<?php

use AsalaeCore\Adapter\DbAcl;
use AsalaeCore\Controller\Component\Reader\ImageReader;
use AsalaeCore\Controller\Component\Reader\OpenDocumentReader;
use AsalaeCore\Controller\Component\Reader\PdfReader;
use AsalaeCore\Driver\Timestamping\TimestampingLocal;
use AsalaeCore\Http\Session\DatabaseSession;
use AsalaeCore\Utility\Antivirus\Clamav;
use AsalaeCore\Worker\TestWorker;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Versae\Controller\ErrorController;
use Versae\Worker\GenerateDepositWorker;
use Versae\Worker\MailWorker;
use Versae\Worker\TransferBuildWorker;
use Versae\Worker\TransferSendWorker;

// Force la locale pour les commandes shell
$encoding = env('APP_ENCODING', 'UTF-8');
$locale = env('APP_DEFAULT_LOCALE', 'fr_FR');
putenv('LC_ALL=' . $locale . '.' . $encoding);
setlocale(LC_ALL, $locale . '.' . $encoding);

return [
    /**
     * Debug Level:
     *
     * Production Mode:
     * false: No error messages, errors, or warnings shown.
     *
     * Development Mode:
     * true: Errors and warnings shown.
     */
    'debug' => false,

    /**
     * Configure basic information about the application.
     *
     * - namespace - The namespace to find app classes under.
     * - defaultLocale - The default locale for translation, formatting currencies and numbers, date and time.
     * - encoding - The encoding used for HTML + database connections.
     * - base - The base directory the app resides in. If false this
     *   will be auto detected.
     * - dir - Name of app directory.
     * - webroot - The webroot directory.
     * - wwwRoot - The file path to webroot.
     * - baseUrl - To configure CakePHP to *not* use mod_rewrite and to
     *   use CakePHP pretty URLs, remove these .htaccess
     *   files:
     *      /.htaccess
     *      /webroot/.htaccess
     *   And uncomment the baseUrl key below.
     * - fullBaseUrl - A base URL to use for absolute links.
     * - imageBaseUrl - Web path to the public images directory under webroot.
     * - cssBaseUrl - Web path to the public css directory under webroot.
     * - jsBaseUrl - Web path to the public js directory under webroot.
     * - paths - Configure paths for non class based resources. Supports the
     *   `plugins`, `templates`, `locales` subkeys, which allow the definition of
     *   paths for plugins, view templates and locale files respectively.
     * - paths.data : directory where the app can create working files
     */
    'App' => [
        'name' => 'versae',
        'version' => VERSAE_VERSION_LONG,
        'namespace' => 'Versae',
        'encoding' => $encoding,
        'defaultLocale' => $locale,
        'base' => false,
        'timezone' => 'Europe/Paris',
        'dir' => 'src',
        'webroot' => 'webroot',
        'wwwRoot' => WWW_ROOT,
        // 'baseUrl' => env('SCRIPT_NAME'),
        'fullBaseUrl' => env('FULL_BASE_URL'),
        'imageBaseUrl' => 'img/',
        'cssBaseUrl' => 'css/',
        'jsBaseUrl' => 'js/',
        'paths' => [
            'plugins' => [ROOT . DS . 'plugins' . DS],
            'templates' => [
                ROOT . DS . 'templates' . DS,
                ROOT . DS . 'vendor' . DS . 'libriciel' . DS . 'asalae-core' . DS . 'templates' . DS,
            ],
            'locales' => [
                RESOURCES . 'locales' . DS,
                ROOT . DS . 'vendor' . DS . 'libriciel' . DS . 'cakephp-filesystem' . DS . 'src' . DS . 'Locale' . DS,
            ],
            'data' => '/data/versae-data',
            'administrators_json' => '/data/config/administrators.json',
            'apis_rules' => RESOURCES . 'apis.json',
            'controllers_rules' => RESOURCES . 'controllers.json',
            'controllers_group' => RESOURCES . 'controllers_group.json',
            'path_to_local_config' => CONFIG . 'path_to_local.php',
            'logs' => LOGS,
        ],
    ],

    /**
     * Acls
     */
    'Acl' => [
        'database' => 'default',
        'classname' => DbAcl::class,
    ],

    /**
     * Security and encryption configuration
     *
     * - salt - A random string used in security hashing methods.
     *   The salt value is also used as the encryption key.
     *   You should treat it as extremely sensitive data.
     */
    'Security' => [
        'salt' => env('SECURITY_SALT', '__SALT__'),
    ],

    /**
     * Pagination
     * Attention, augmenter les valeurs peut avoir un impact négatif sur les
     * performances.
     *
     * - default: nombre de lignes d'un tableau de résultat (par défaut)
     * - options: choix possible du nombre de lignes
     */
    'Pagination' => [
        'default' => 20,
        'options' => '20, 30, 40, 50, 60, 70, 80, 90, 100, 120, 150, 200',
    ],

    /**
     * Apply timestamps with the last modified time to static assets (js, css, images).
     * Will append a querystring parameter containing the time the file was modified.
     * This is useful for busting browser caches.
     *
     * Set to true to apply timestamps when debug is true. Set to 'force' to always
     * enable timestamping regardless of debug value.
     */
    'Asset' => [
        'timestamp' => 'force',
    ],

    /**
     * Valeurs possible:
     *      true - Désactive le cache
     *      false - Le cache est activé
     *      'on_debug' - Le cache est désactivé lorsque debug = true
     */
    'Cache_disabled' => false,

    /**
     * Configure the cache adapters.
     */
    'Cache' => [
        'default' => [
            'className' => 'File',
            'path' => CACHE,
            'url' => env('CACHE_DEFAULT_URL'),
        ],
        'login' => [
            'className' => 'File',
            'prefix' => 'login_',
            'path' => CACHE . 'login/',
            'serialize' => true,
            'duration' => '+60 minutes',
            'mask' => 0660,
        ],

        /**
         * Configure the cache used for general framework caching.
         * Translation cache files are stored with this configuration.
         * Duration will be set to '+2 minutes' in bootstrap.php when debug = true
         * If you set 'className' => 'Null' core cache will be disabled.
         */
        '_cake_core_' => [
            'className' => 'File',
            'prefix' => 'myapp_cake_core_',
            'path' => CACHE . 'persistent/',
            'serialize' => true,
            'duration' => '+1 years',
            'url' => env('CACHE_CAKECORE_URL'),
        ],

        /**
         * Configure the cache for model and datasource caches. This cache
         * configuration is used to store schema descriptions, and table listings
         * in connections.
         * Duration will be set to '+2 minutes' in bootstrap.php when debug = true
         */
        '_cake_model_' => [
            'className' => 'File',
            'prefix' => 'myapp_cake_model_',
            'path' => CACHE . 'models/',
            'serialize' => true,
            'duration' => '+1 years',
            'url' => env('CACHE_CAKEMODEL_URL'),
        ],
    ],

    /**
     * Configure the Error and Exception handlers used by your application.
     *
     * By default errors are displayed using Debugger, when debug is true and logged
     * by Cake\Log\Log when debug is false.
     *
     * In CLI environments exceptions will be printed to stderr with a backtrace.
     * In web environments an HTML page will be displayed for the exception.
     * With debug true, framework errors like Missing Controller will be displayed.
     * When debug is false, framework errors will be coerced into generic HTTP errors.
     *
     * Options:
     *
     * - `errorLevel` - int - The level of errors you are interested in capturing.
     * - `trace` - boolean - Whether or not backtraces should be included in
     *   logged errors/exceptions.
     * - `log` - boolean - Whether or not you want exceptions logged.
     * - `exceptionRenderer` - string - The class responsible for rendering
     *   uncaught exceptions. If you choose a custom class you should place
     *   the file for that class in src/Error. This class needs to implement a
     *   render method.
     * - `skipLog` - array - List of exceptions to skip for logging. Exceptions that
     *   extend one of the listed exceptions will also be skipped for logging.
     *   E.g.:
     *   `'skipLog' => ['Cake\Network\Exception\NotFoundException', 'Cake\Network\Exception\UnauthorizedException']`
     * - `extraFatalErrorMemory` - int - The number of megabytes to increase
     *   the memory limit by when a fatal error is encountered. This allows
     *   breathing room to complete logging or error handling.
     */
    'Error' => [
        'errorLevel' => E_ALL &~ E_DEPRECATED,
        'errorRenderer' => null,
        'exceptionRenderer' => null,
        'errorController' => ErrorController::class,
        'skipLog' => [],
        'log' => true,
        'trace' => true,
        'ignoredDeprecationPaths' => [
            'vendor/cakephp/acl/src/Model/Table/PermissionsTable.php',
        ],
    ],

    /**
     * Email configuration.
     *
     * By defining transports separately from delivery profiles you can easily
     * re-use transport configuration across multiple profiles.
     *
     * You can specify multiple configurations for production, development and
     * testing.
     *
     * Each transport needs a `className`. Valid options are as follows:
     *
     *  Mail   - Send using PHP mail function
     *  Smtp   - Send using SMTP
     *  Debug  - Do not send the email, just return the result
     *
     * You can add custom transports (or override existing transports) by adding the
     * appropriate file to src/Mailer/Transport. Transports should be named
     * 'YourTransport.php', where 'Your' is the name of the transport.
     */
    'EmailTransport' => [
        'default' => [
            'className' => 'Mail',
            // The following keys are used in SMTP transports
            'host' => 'host.docker.internal',
            'port' => 25,
            'timeout' => 30,
            //'username' => null,
            //'password' => null,
            'client' => null,
            'tls' => false,
            'url' => env('EMAIL_TRANSPORT_DEFAULT_URL'),
        ],
    ],

    /**
     * Email delivery profiles
     *
     * Delivery profiles allow you to predefine various properties about email
     * messages from your application and give the settings a name. This saves
     * duplication across your application and makes maintenance and development
     * easier. Each profile accepts a number of keys. See `Cake\Mailer\Email`
     * for more information.
     */
    'Email' => [
        'default' => [
            'transport' => 'default',
            'from' => 'you@localhost',
            //'charset' => 'utf-8',
            //'headerCharset' => 'utf-8',
        ],
    ],

    /**
     * Connection information used by the ORM to connect
     * to your application's datastores.
     * Do not use periods in database name - it may lead to error.
     * See https://github.com/cakephp/cakephp/issues/6471 for details.
     * Drivers include Mysql Postgres Sqlite Sqlserver
     * See vendor\cakephp\cakephp\src\Database\Driver for complete list
     */
    'Datasources' => [
        'default' => [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Postgres',
            'persistent' => false,
            'host' => 'versae_db',
            /**
             * CakePHP will use the default DB port based on the driver selected
             * MySQL on MAMP uses port 8889, MAMP users will want to uncomment
             * the following line and set the port accordingly
             */
            //'port' => 'non_standard_port_number',
            'username' => 'versae',
            'password' => 'versae',
            'database' => 'versae',
            'encoding' => 'utf8',
            'timezone' => 'UTC',
            'flags' => [],
            'cacheMetadata' => true,
            'log' => false,

            /**
             * Set identifier quoting to true if you are using reserved words or
             * special characters in your table or column names. Enabling this
             * setting will result in queries built using the Query Builder having
             * identifiers quoted when creating SQL. It should be noted that this
             * decreases performance because each query needs to be traversed and
             * manipulated before being executed.
             */
            'quoteIdentifiers' => false,

            /**
             * During development, if using MySQL < 5.6, uncommenting the
             * following line could boost the speed at which schema metadata is
             * fetched from the database. It can also be set directly with the
             * mysql configuration directive 'innodb_stats_on_metadata = 0'
             * which is the recommended value in production environments
             */
            //'init' => ['SET GLOBAL innodb_stats_on_metadata = 0'],

            'url' => env('DATABASE_URL'),
        ],

        /**
         * The test connection is used during the test suite.
         */
        'test' => [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Sqlite',
            'persistent' => false,
            'host' => 'localhost',
            //'port' => 'non_standard_port_number',
            'username' => 'my_app',
            'password' => 'secret',
            'database' => ':memory:',
            'encoding' => 'utf8',
            'timezone' => 'UTC',
            'cacheMetadata' => true,
            'quoteIdentifiers' => false,
            'log' => false,
            'url' => env('DATABASE_TEST_URL'),
        ],

        /**
         * Utilisé pour générer les fixtures de permissions
         * Sera supprimé à chaque execution de la commande 'bin/cake fixtures perms'
         */
        'fixtures_perms' => [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Sqlite',
            'database' => sys_get_temp_dir() . DS . 'fixtures.sqlite',
        ],
    ],

    /**
     * Configures logging options
     */
    'Log' => [
        'debug' => [
            'className' => 'Cake\Log\Engine\FileLog',
            'file' => 'debug',
            'levels' => ['notice', 'info', 'debug'],
            'url' => env('LOG_DEBUG_URL'),
        ],
        'error' => [
            'className' => 'Cake\Log\Engine\FileLog',
            'file' => 'error',
            'levels' => ['warning', 'error', 'critical', 'alert', 'emergency'],
            'url' => env('LOG_ERROR_URL'),
        ],
    ],

    /**
     * Session configuration.
     *
     * Contains an array of settings to use for session configuration. The
     * `defaults` key is used to define a default preset to use for sessions, any
     * settings declared here will override the settings of the default config.
     *
     * ## Options
     *
     * - `cookie` - The name of the cookie to use. Defaults to 'CAKEPHP'.
     * - `cookiePath` - The url path for which session cookie is set. Maps to the
     *   `session.cookie_path` php.ini config. Defaults to base path of app.
     * - `timeout` - The time in minutes the session should be valid for.
     *    Pass 0 to disable checking timeout.
     *    Please note that php.ini's session.gc_maxlifetime must be equal to or greater
     *    than the largest Session['timeout'] in all served websites for it to have the
     *    desired effect.
     * - `defaults` - The default configuration set to use as a basis for your session.
     *    There are four built-in options: php, cake, cache, database.
     * - `handler` - Can be used to enable a custom session handler. Expects an
     *    array with at least the `engine` key, being the name of the Session engine
     *    class to use for managing the session. CakePHP bundles the `CacheSession`
     *    and `DatabaseSession` engines.
     * - `ini` - An associative array of additional ini values to set.
     *
     * The built-in `defaults` options are:
     *
     * - 'php' - Uses settings defined in your php.ini.
     * - 'cake' - Saves session files in CakePHP's /tmp directory.
     * - 'database' - Uses CakePHP's database sessions.
     * - 'cache' - Use the Cache class to save sessions.
     *
     * To define a custom session handler, save it at src/Network/Session/<name>.php.
     * Make sure the class implements PHP's `SessionHandlerInterface` and set
     * Session.handler to <name>
     *
     * To use database sessions, load the SQL file located at config/Schema/sessions.sql
     */
    'Session' => [
        'defaults' => 'database',
        'handler' => [
            'engine' => DatabaseSession::class,
            'model' => 'Sessions'
        ]
    ],

    /**
     * Algorithme de hash lors de l'ajout de nouveaux fichiers
     *
     * Algorithmes populaires :
     *
     * - crc32      Très rapide
     * - md5        Très connu
     * - sha1       Très connu
     * - sha256     Lourd
     * - sha512     Très lourd
     * - whirlpool  Ultra lourd (+ secure)
     */
    'hash_algo' => 'sha256',

    /**
     * Configuration du système d'upload
     *
     * @see https://github.com/flowjs/flow.js/blob/master/README.md#full-documentation
     *
     * - chunkSize : Taille de découpage des fichiers, permet la reprise de
     *      l'upload après pause (dernier chunk perdu)
     *      saisir une valeur <= 50% à UPLOAD_MAX_FILESIZE
     *      ou saisir une valeur <= à UPLOAD_MAX_FILESIZE si option forceChunkSize=true
     * - target : chemin vers lequel envoyer les chunks (ne pas modifier)
     * - simultaneousUploads : Nombre de téléchargements simultanés
     *      Influe sur la vitesse de téléchargement et sur la consommation en CPU
     *      Pour des vitesses de téléchargement optimales, augmenter la valeur jusqu'à
     *      obtenir 95%+ sur chaque coeur du processeur
     */
    'Flow' => [
        'chunkSize' => min(67108864, UploadedFile::getMaxFilesize() / 2),// max 64Mo
        'target' => '/upload',
        'simultaneousUploads' => 3,
        'maxChunkRetries' => 3,
        'chunkRetryInterval' => 10000,
    ],

    /**
     * Driver pour le traitement des images
     *
     * Disponible :
     * - gd     (sudo apt-get install php-gd)
     * - imagick (sudo apt-get install php-imagick)
     * - gmagick (sudo apt-get install php-gmagick) (obsolète)
     */
    'DriverImage' => 'gd',

    /**
     * Configuration des workers et jobs beanstalk
     *
     * - client_host : hôte utilisé par les clients beanstalk (application web, workers...)
     * - client_port : port utilisé par les clients beanstalk
     * - server_host : hôte utilisé par le serveur beanstalk, utiliser 0.0.0.0
     *                 pour ouvrir aux autres containers docker
     * - server_port : port utilisé par le serveur beanstalk
     * - worker_auto : lance automatiquement les workers (désactiver pour debug)
     */
    'Beanstalk' => [
        /**
         * Connection au serveur Beanstalkd
         */
        'client_host' => 'versae_beanstalk',
        'client_port' => 11300,
        'server_host' => '0.0.0.0',
        'server_port' => 11300,
        'worker_auto' => true,

        /**
         * Définition des workers pour le manager
         *
         * - classname : Classe PHP du worker
         * - run : commande qui sera exécutée pour lancer le worker
         *      "bin/cake worker <nom_du_worker>" est le fonctionnement par défaut
         *      "docker run -d <nom_de_l_image_docker> <commande>" lancé par docker
         *      "ssh w02.asalae2.fr "/var/www/asalae2/bin worker <nom_du_worker>"" pour du ssh
         *
         * - kill : commande pour forcer l'arrêt d'un worker
         *      "kill {pid}" fonctionnement par défaut
         *      "docker kill {{hostname}}" pour du docker
         *      "ssh {{hostname}} "kill {{pid}}"" pour du ssh
         *
         * - quantity : définit le nombre de workers qui seront lancés avec cette configuration
         *
         * - active : permet d'activer/désactiver cette option
         *
         * Variables pour le kill:
         *
         *  {{hostname}} : nom de l'hôte sur lequel est lancé le worker / ID du container docker
         *  {{pid}} : identifiant unique du processus
         */
        'workers' => [
            'generate-deposit' => [
                'classname' => GenerateDepositWorker::class,
                'run' => 'bin/cake worker generate-deposit',
                'kill' => 'kill {{pid}}',
                'quantity' => 1,
                'active' => true,
            ],
            'mail' => [
                'classname' => MailWorker::class,
                'run' => 'bin/cake worker mail',
                'kill' => 'kill {{pid}}',
                'quantity' => 1,
                'active' => true,
            ],
            'test' => [
                'classname' => TestWorker::class,
                'run' => 'bin/cake worker test',
                'kill' => 'kill {{pid}}',
                'quantity' => 0,
                'active' => true,
            ],
            'transfer-build' => [
                'classname' => TransferBuildWorker::class,
                'run' => 'bin/cake worker transfer-build',
                'kill' => 'kill {{pid}}',
                'quantity' => 1,
                'active' => true,
            ],
            'transfer-send' => [
                'classname' => TransferSendWorker::class,
                'run' => 'bin/cake worker transfer-send',
                'kill' => 'kill {{pid}}',
                'quantity' => 1,
                'active' => true,
            ],
        ],
    ],

    /**
     * Serveur de notifications
     *
     * NOTE : certains proxies client peuvent empêcher l'écoute sur port 8080
     * Pour des solutions altérnatives, allez voir le lien ci-dessous
     *
     * @see http://socketo.me/docs/deploy#serverconfiguration
     *
     * -------------------------------------------------------------------------
     * Pour une utilisation en ssl (Recommandé) :
     * -------------------------------------------------------------------------
     * sudo a2enmod proxy
     * sudo a2enmod proxy_wstunnel
     *
     * Modifier votre VirtualHost et ajoutez :
     * <Location "/wss">
     *      ProxyPass ws://localhost:8080/
     * </location>
     *
     * sudo service apache2 restart
     * -------------------------------------------------------------------------
     *
     * - ping : Permet de vérifier que le serveur tourne
     * - pong_delay : Temps de réponse du client à un ping en secondes
     * - check_timeout : Temps de réponse du navigateur à un ping en millisecondes
     * - connect : Adresse de consultation du WebSocket pour le client
     * - host : Sources autorisé (0.0.0.0 pour autoriser toutes les sources)
     * - listen_port : Port sur lequel écouter
     * - zmq : Client interne, il n'est pas recommendé de modifier cette valeur
     * - verbose : Permet de logger toutes les actions de ratchet
     */
    'Ratchet' => [
        // Client
        'ping' => 'http://versae_ratchet:8080',
        'pong_delay' => 1,
        'check_timeout' => 3000,
        // Serveur
        'host' => '0.0.0.0',
        'listen_port' => 8080,
        'zmq' => [
            'protocol' => 'tcp',
            'domain' => 'versae_ratchet',
            'port' => 5556,
        ],
        'verbose' => false,
    ],

    /**
     * Réglages lié au password
     * Complexité minimale autorisée
     *
     * @see https://www.ssi.gouv.fr/administration/precautions-elementaires/calculer-la-force-dun-mot-de-passe/
     *
     * - PASSWORD_ULTRA_WEAK Mot de passe de 8 caractères dans un alphabet de 70 symboles
     *                       Taille usuelle
     *
     * - PASSWORD_WEAK       Mot de passe de 12 caractères dans un alphabet de 90 symboles
     *                       Taille minimale recommandée par l’ANSSI pour des
     *                       mots de passe ergonomiques ou utilisés de façon locale.
     *
     * - PASSWORD_DEFAULT    Mot de passe de 14 caractères dans un alphabet de 90 symboles
     *                       Taille minimale recommandée par l’ANSSI pour des
     *                       mots de passe ergonomiques ou utilisés de façon locale.
     *
     * - PASSWORD_MEDIUM     Mot de passe de 16 caractères dans un alphabet de 90 symboles
     *                       Taille recommandée par l’ANSSI pour des mots de passe plus sûrs.
     *
     * - PASSWORD_STRONG     Mot de passe de 20 caractères dans un alphabet de 90 symboles
     *                       Force équivalente à la plus petite taille de clé de
     *                       l’algorithme de chiffrement standard AES (128 bits).
     */
    'Password' => [
        'complexity' => PASSWORD_DEFAULT_COMPLEXITY,
        'admin' => [
            'complexity' => PASSWORD_STRONG
        ],
    ],

    /**
     * Réglages de l'utilitaire du système de fichier
     *
     * - useShred : utilise ou pas shred, qui détruit sans possibilité de
     *              récupération les fichiers (suppression beaucoup plus longue)
     */
    'FileSystemUtility' => [
        'useShred' => true,
    ],

    /**
     * Réglages de l'utilitaire de compression des fichiers
     *
     * - encodeFilename :
     *      Permet d'encoder le nom de fichier.
     *      Par défaut, l'encodage est UTF-8, compatible unix
     *      et windows > 8 (un patch existe pour windows 7 sur
     *      @link https://support.microsoft.com/en-us/hotfix/kbhotfix?kbnum=2704299&kbln=en-US )
     *
     *      Si le nom de fichier est mal encodé (selon le systeme),
     *      Des caractères étrange pourrons être visible à la place
     *      des accents.
     *
     *      valeurs conseillés :
     *      - Laisser à false pour une compatibilité tout systèmes récent
     *      - Utiliser la valeur IBM850 pour une compatibilité sur Windows <= 7
     *
     *      valeur déconseillé :
     *      - ASCII//TRANSLIT compatibilité maximale mais remplace les
     *      caractères spéciaux/accents par leurs equivalent : é => e, € => EUR
     */
    'DataCompressorUtility' => [
        'encodeFilename' => false,
    ],

    /**
     * Permet de renseigner des assets à utiliser dans l'application
     *
     * -login
     *      -logo-client : Uri du logo à afficher sur la page de login
     *                     ex: ROOT . DS . 'data' . DS . 'background.jpg'
     *      -background : Couleur (valeur css) du fond derrière le logo-client
     *                    ex : '#D7001A'
     *
     * -global-background : Sera affiché en haut à gauche sur les pages
     *                      Créé afin d'afficher un logo/emblème
     *                      ex: ROOT . DS . 'data' . DS . 'background.jpg'
     *
     * -global-background-position : attribut css background-position
     *                               '50px 50px'
     */
    'Assets' => [
        'login' => [
            'logo-client' => null,
            'background' => null,
        ],
        'global-background' => '',
        'global-background-position' => '',
    ],

    /**
     * Configuration pour les filtres de recherche
     */
    'Filters' => [
        'urls' => [
            'delete' => 'filters/ajax-delete-save',
            'overwrite' => 'filters/ajax-overwrite',
            'new' => 'filters/ajax-new-save',
            'load' => 'filters/ajax-get-filters',
        ]
    ],

    /**
     * Permet d'ajouter des fichiers de configuration supplémentaire
     * Sera fusionné avec un Hash::merge()
     */
    'Config' => [
        'files' => [],
    ],

    /**
     * Permet de désactiver, lorsque le debug est à true, certaines fonctionnalités
     * qui prennent du temps (utile pour le développement)
     */
    'Skip' => [
        'validation' => false,
    ],

    /**
     * Configuration de la validation
     *  -use_domdocument:
     *      Utilisation de la class DOMDocument de php pour effectuer la validation.
     *      Si cette valeur est à false, la commande shell "cmd" sera lancée
     *  -cmd:
     *      Commande à lancer suivi du fichier de validation et du fichier à valider
     *      Sera lancé uniquement si use_domdocument est à false
     *      ex: xmllint --noout --relaxng "/my/validator.rng" "/my/document.xml"
     */
    'Validation' => [
        'relaxng' => [
            'use_domdocument' => false,
//            'cmd' => 'xmllint --noout --relaxng',
//            'cmd' => 'java -jar "'.RESOURCES.'jing.jar"',
            'cmd' => 'java -Dfile.encoding=ISO-8859-1 -jar "' . RESOURCES . 'jing_fr.jar"',
        ],
        'schema' => [
            'use_domdocument' => true,
            'cmd' => 'xmllint --noout --schema'
        ]
    ],

    /**
     * Horodatage
     *
     * drivers : liste les systèmes d'horodatages
     *      name: Nom affiché dans l'application
     *      class: Classe PHP du driver (instance of TimestampingInterface)
     *      fields: Liste les champs paramétrables (l'ordre d'affichage a de l'importance)
     *          type: type de champ html (default: text)
     *          placeholder: indice sur le contenu
     *          help: message d'aide
     *          pattern: expression régulière de validation
     *          default: valeur par défaut
     *          display: affiche ou non le champ dans le paramétrage de l'application (default: true)
     *          read_config: La valeur sera lu ailleur dans la configuration (Chemin.vers.la.conf)
     */
    'Timestamping' => [
        'drivers' => [
            'OPENSIGN' => [
                'name' => 'OpenSign',
                'class' => 'AsalaeCore\Driver\Timestamping\TimestampingOpensign',
                'fields' => [
                    'url' => [
                        'type' => 'url',
                        'label' => 'Url',
                        'placeholder' => 'http://horodatage.local/opensign.wsdl',
                        'required' => 'required',
                    ],
                    'use_proxy' => [
                        'type' => 'checkbox',
                        'label' => 'Use proxy',
                    ],
                    'proxy_host' => [
                        'display' => false,
                        'read_config' => 'Proxy.host',
                    ],
                    'proxy_port' => [
                        'display' => false,
                        'read_config' => 'Proxy.port',
                    ],
                    'proxy_login' => [
                        'display' => false,
                        'read_config' => 'Proxy.login',
                    ],
                    'proxy_password' => [
                        'display' => false,
                        'read_config' => 'Proxy.password',
                        'type' => 'password',
                    ],
                ],
            ],
            'IAIK' => [
                'name' => 'IAIK',
                'class' => 'AsalaeCore\Driver\Timestamping\TimestampingIaik',
                'fields' => [
                    'url' => [
                        'type' => 'url',
                        'label' => 'Url',
                        'placeholder' => 'https://tsp.iaik.tugraz.at/tsp/TspRequest',
                        'required' => 'required',
                    ],
                    'timestamp_format' => [
                        'display' => false,
                        'default' => 'M d H:i:s.u Y T',
                    ],
                    'use_proxy' => [
                        'type' => 'checkbox',
                        'label' => 'Use proxy',
                    ],
                    'proxy_host' => [
                        'display' => false,
                        'read_config' => 'Proxy.host',
                    ],
                    'proxy_port' => [
                        'display' => false,
                        'read_config' => 'Proxy.port',
                    ],
                    'proxy_login' => [
                        'display' => false,
                        'read_config' => 'Proxy.login',
                    ],
                    'proxy_password' => [
                        'display' => false,
                        'read_config' => 'Proxy.password',
                        'type' => 'password',
                    ],
                ],
            ],
            'CRYPTOLOG' => [
                'name' => 'Universign',
                'class' => 'AsalaeCore\Driver\Timestamping\TimestampingGeneric',
                'fields' => [
                    'url' => [
                        'type' => 'url',
                        'label' => 'Url',
                        'required' => 'required',
                    ],
                    'timestamp_format' => [
                        'display' => false,
                        'default' => 'M d H:i:s.u Y T',
                    ],
                    'hash_algo' => [
                        'display' => false,
                        'default' => 'sha256',
                    ],
                    'login' => [
                        'label' => 'Identifiant de connexion au TSA',
                    ],
                    'password' => [
                        'label' => 'Mot de passe de connexion au TSA',
                        'type' => 'password',
                    ],
                    'proxy_host' => [
                        'display' => false,
                        'read_config' => 'Proxy.host',
                    ],
                    'proxy_port' => [
                        'display' => false,
                        'read_config' => 'Proxy.port',
                    ],
                    'proxy_login' => [
                        'display' => false,
                        'read_config' => 'Proxy.login',
                    ],
                    'proxy_password' => [
                        'display' => false,
                        'read_config' => 'Proxy.password',
                        'type' => 'password',
                    ],
                ],
            ],
            'OPENSSL' => [
                'name' => 'Internal OpenSSL TSA',
                'class' => TimestampingLocal::class,
                'fields' => [
                    'ca' => [
                        'label' => '/path/to/root_ca.crt',
                        'required' => 'required',
                    ],
                    'crt' => [
                        'label' => '/path/to/ts.crt',
                        'required' => 'required',
                    ],
                    'pem' => [
                        'label' => '/path/to/ts_pkey.pem',
                        'required' => 'required',
                    ],
                    'cnf' => [
                        'label' => '/path/to/openssl_config.cnf',
                        'required' => 'required',
                    ],
                ]
            ],
        ],
    ],

    /**
     * Volumes
     *
     * drivers : liste les systèmes de volumes
     *      name: Nom affiché dans l'application
     *      class: Classe PHP du driver (instance of TimestampingInterface)
     *      fields: Liste les champs paramétrables (l'ordre d'affichage a de l'importance)
     *          type: type de champ html (default: text)
     *          placeholder: indice sur le contenu
     *          help: message d'aide
     *          pattern: expression régulière de validation
     *          default: valeur par défaut
     *          display: affiche ou non le champ dans le paramétrage de l'application (default: true)
     *          read_config: La valeur sera lu ailleur dans la configuration (Chemin.vers.la.conf)
     */
    'Volumes' => [
        'drivers' => [
            'FILESYSTEM' => [
                'name' => 'Filesystem',
                'class' => AsalaeCore\Driver\Volume\VolumeFilesystem::class,
                'fields' => [
                    'path' => [
                        'label' => 'Dossier',
                        'placeholder' => '/data/Volume01',
                        'required' => 'required',
                    ]
                ],
                'uniqueFields' => ['path'],
                'toString' => ['%s', 'path']
            ],
            'MINIO' => [
                'name' => 'S3 standard object storage',
                'class' => AsalaeCore\Driver\Volume\VolumeS3::class,
                'fields' => [
                    'endpoint' => [
                        'label' => "Url des webservices (endpoint)",
                        'placeholder' => 'http://127.0.0.1:9000',
                        'required' => 'required',
                        'type' => 'url',
                    ],
                    'region' => [
                        'label' => "Region",
                        'default' => 'EU',
                        'required' => 'required',
                    ],
                    'bucket' => [
                        'label' => "Nom de conteneur (Bucket)",
                        'placeholder' => 'volume01',
                        'required' => 'required',
                    ],
                    'credentials_key' => [
                        'label' => "Nom d'utilisateur (credentials key)",
                        'required' => 'required',
                    ],
                    'credentials_secret' => [
                        'label' => "Mot de passe (credentials secret)",
                        'type' => 'password',
                        'required' => 'required',
                    ],
                    'verify' => [
                        'placeholder' => "Optionnel",
                        'class' => 'verify-minio-cert',
                        'help' => "false - Désactive la vérification SSL<br>
                            /path/to/certificate.pem - Indique le chemin vers le certificat public (autosigné)",
                    ],
                    'use_proxy' => [
                        'type' => 'checkbox',
                        'label' => "Utiliser le proxy configuré ?",
                    ],
                    'proxy_host' => [
                        'display' => false,
                        'read_config' => 'Proxy.host',
                    ],
                    'proxy_port' => [
                        'display' => false,
                        'read_config' => 'Proxy.port',
                    ],
                    'proxy_login' => [
                        'display' => false,
                        'read_config' => 'Proxy.login',
                    ],
                    'proxy_password' => [
                        'display' => false,
                        'read_config' => 'Proxy.password',
                    ]
                ],
                'uniqueFields' => ['endpoint', 'bucket'],
                'toString' => ['%s/%s', 'endpoint', 'bucket']
            ],
        ],
    ],

    /**
     * Paramétrage du proxy
     */
    'Proxy' => [
        'host' => null,
        'port' => null,
        'login' => null,
        'password' => null,
    ],

    /**
     * Configuration de la page de login
     */
    'Libriciel' => [
        'login' => [
            'lib-login' => [
                'logo' => '/img/versae-color.svg',
                'forgot-form-action' => '/users/forgotten-password',
            ],
            'lib-footer' => [
                'active' => 'asalae', // ce n'est pas une erreur
            ],
            'css' => [
                'as-bootstrap-4.css',
                '/libriciel-login/forkawesome/css/fork-awesome.css',
                'login.css',
            ],
            'js' => [
                'as-bootstrap-4.js',
                'asalae.login.js',
            ],
        ],
    ],

    /**
     * Antivirus
     */
    'Antivirus' => [
        'classname' => Clamav::class,
        'host' => 'versae_clamav',
        'port' => 3310,
    ],

    /**
     * Permissions
     */
    'Permissions' => [
        'display_technical_name' => false,
    ],

    /**
     * Maintenance
     *
     * - enabled: activation immédiate de l'interruption
     * - scheduled:
     *      - begin: Date et heure du début de la prochaine interruption (ex: 2020-01-01T00:01:30)
     *      - end: Date et heure de fin de la prochaine interruption
     * - periodic:
     *      - begin: Heure de début de l'interruption journalière
     *      - end: Heure de fin de l'interruption journalière
     * - whitelist_file: Liste des urls accessibles lors de l'interruption (les urls d'administration)
     * - enable_whitelist: Permet aux requêtes possédant un entête précis de passer outre l'interruption
     * - whitelist_headers: Liste d'entêtes HTTP autorisées pendant une interruption de service
     * - message: Message destiné aux utilisateurs lors de l'interruption
     * - message_periodic: Message lors de l'interruption journalière
     */
    'Interruption' => [
        'enabled' => false,
        'scheduled' => [
            'begin' => null,
            'end' => null,
        ],
        'periodic' => [
            'begin' => null,
            'end' => null,
        ],
        'whitelist_file' => RESOURCES . 'interruption_whitelist.conf',
        'enable_whitelist' => true,
        'whitelist_headers' => [
            'X-Asalae-Webservice' => true,
        ],
        'message' => null,
        'message_periodic' => null,
        'enable_workers' => false,
    ],

    /**
     * - probe: affichage de l'état du serveur
     *      - loadavg: load average
     *      - mem: RAM utilisé / disponible
     */
    'Admins' => [
        'probe' => [
            'loadavg' => [
                'low' => 0.7, // vert
                'medium' => 1.2, // bleu
                'high' => 1.6, // jaune
                'max' => 5, // rouge
            ],
            'mem' => [
                'low' => 25, // vert
                'medium' => 50, // bleu
                'high' => 75, // jaune
                'max' => 100, // rouge
            ],
            'swap' => [
                'low' => 25, // vert
                'medium' => 50, // bleu
                'high' => 75, // jaune
                'max' => 100, // rouge
            ],
        ],
    ],

    /**
     * Liste les readers disponibles
     */
    'Readers' => [
        'pdf' => PdfReader::class,
        'open' => OpenDocumentReader::class,
        'image' => ImageReader::class,
    ],

    /**
     * Configuration OAI-PMH
     */
    'OAIPMH' => [
        'maxListSize' => 500, // items pagination
        'expiration' => 3600,  // seconds
    ],

    /**
     * Gestion des téléchargements
     *
     * - asyncFileCreationTimeLimit :
     *      seconds, temps limite à partir duquel on propose l'envoit d'un mail au
     *      lieu de lancer le téléchargement (estimation)
     *
     * - tempFileConservationTimeLimit :
     *      hours, temps de conservation des fichiers disponibles au téléchargement
     */
    'Downloads' => [
        'asyncFileCreationTimeLimit' => 30,
        'tempFileConservationTimeLimit' => 48,
    ],

    /**
     * Configuration commune entre les crons
     */
    'Crons' => [
        'emails' => [
            'min_interval' => '1 day'
        ]
    ],

    /**
     * Filtre les ips qui peuvent accéder à l'interface d'administration technique
     *
     * - enable_whitelist: active le filtrage des ips
     * - whitelist: liste des ips autorisés. Supporte le wildcard *
     *      ex:
     *          - 192.168.*.*
     *          - 10.0.0.*
     *          - fe80::*:*:*:*
     */
    'Ip' => [
        'enable_whitelist' => false,
        'whitelist' => [
            '127.0.0.1',
            '::1',
        ],
    ],

    /**
     * Keycloak
     * - enabled: active l'authentification via keycloak
     * - realm: nom du royaume keycloak
     * - client_id: nom du client keycloak
     * - client_secret: secret du client
     * - clientParams: attributs supplémentaire pour le client HTTP. liste non exaustive:
     *     - ssl_verify_peer
     *     - ssl_verify_peer_name
     *     - ssl_verify_host
     *     - ssl_verify_depth
     *     - ssl_cafile
     *     - redirect
     * - base_url: url du keycloak
     * - username: champ keyclock utilisé pour l'identification de l'utilisateur
     */
    'Keycloak' => [
        'enabled' => false,
        'realm' => 'master',
        'client_id' => 'versae',
        'base_url' => 'http://localhost:8080',
        'username' => 'preferred_username',
    ],

    /**
     * Pour connecteurs autre que keycloak (note: mais fonctionne avec keycloak)
     * - enabled: active l'authentification via openid-connect
     * - client_id: id du client
     * - client_secret: secret du client
     * - clientParams: attributs supplémentaire pour le client HTTP. liste non exaustive:
     *     - ssl_verify_peer
     *     - ssl_verify_peer_name
     *     - ssl_verify_host
     *     - ssl_verify_depth
     *     - ssl_cafile
     *     - redirect
     * - base_url: url du service (/.well-known/openid-configuration doit exister)
     * - username: champ utilisé pour l'identification de l'utilisateur
     * - redirect_uri: redirige sur cette url (doit correspondre à App.fullBaseUrl)
     *      si valeur auto => App.fullBaseUrl est utilisé
     * - scope: si besoin on peut fixer un scope
     */
    'OpenIDConnect' => [
        'enabled' => false,
        'client_id' => 'versae',
        'base_url' => 'http://localhost:8080/auth/realms/master',
        'username' => 'sub',
        'scope' => 'openid',
        'redirect_uri' => 'auto',
    ],

    /**
     * Apparence des mails
     */
    'AsalaeCore' => [
        'Template' => [
            'Layout' => [
                'Email' => [
                    'html' => [
                        'default' => [
                            'logoUrl' => 'https://ressources.libriciel.fr/public/versae/img/versae-color-white.svg',
                            'logoAlt' => 'versae',
                        ],
                    ],
                ],
            ],
        ],
    ],

    /**
     * Liste des binaries dans les prérequis affichés dans /admins
     */
    'Check' => [
        'prerequis' => [
            'bzip2',
            'gzip',
            'rar',
            'sf',
            'zip',
        ],
    ],

    /**
     * Seda generator
     */
    'SedaGenerator' => [
        'url' => 'http://versae_sedagen',
        'ssl_verify_depth' => 0,
    ],

    /**
     * Permet d'empêcher un trop grand nombre d'essais de mot de passe infructueux
     *
     * - attempt_limit : limite le nombre de tentatives de login sur un utilisateur en particulier sur une période
     *      - enabled : si valeur true, la limite est activée
     *      - max_amount : si le nombre d'essais dépasse cette valeur, une erreur 429 sera envoyée
     *      - cache_duration : durée en secondes pendant laquelle on conserve les tentatives infructueuses (max 1h)
     */
    'Login' => [
        'attempt_limit' => [
            'enabled' => true,
            'max_amount' => 5,
            'cache_duration' => 300,
        ],
    ],
];
