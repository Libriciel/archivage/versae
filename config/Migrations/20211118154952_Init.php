<?php
/**
 * @noinspection PhpCSValidationInspection
 */
declare(strict_types=1);

use Migrations\AbstractMigration;

/**
 * 1ere migration
 */
class Init extends AbstractMigration
{
    /**
     * @var array Liste de champ name de la table roles
     */
    const ROLES = [
        "Archiviste" => 'AA',
        "Référent Archives" => 'ARA',
        "Versant" => 'AV',
    ];

    /**
     * @var array Champ name en clef, name[] des roles liés en valeur
     */
    const TYPES = [
        [
            'name' => "Service d'Exploitation",
            'code' => 'SE',
            'roles' => [
            ],
        ],
        [
            'name' => "Service d'Archives",
            'code' => 'SA',
            'roles' => [
                "Archiviste",
            ],
        ],
        [
            'name' => "Service Versant",
            'code' => 'SV',
            'roles' => [
                "Référent Archives",
                "Versant",
            ],
        ],
        [
            'name' => "Organisationnel",
            'code' => 'SO',
            'roles' => [
                "Référent Archives",
                "Versant",
            ],
        ],
    ];

    public function up()
    {
        $this->table('acos')
            ->addColumn(
                'parent_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'model',
                'string',
                [
                    'default' => '',
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'foreign_key',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'alias',
                'string',
                [
                    'default' => '',
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'lft',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'rght',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'parent_id',
                ]
            )
            ->addIndex(
                [
                    'alias',
                ]
            )
            ->addIndex(
                [
                    'lft',
                    'rght',
                ]
            )
            ->create();

        $this->table('archiving_systems')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'url',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'username',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'password',
                'binary',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'use_proxy',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'ssl_verify_peer',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'ssl_verify_peer_name',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'ssl_verify_depth',
                'integer',
                [
                    'default' => '5',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'ssl_verify_host',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'ssl_cafile',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'chunk_size',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('aros')
            ->addColumn(
                'parent_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'model',
                'string',
                [
                    'default' => '',
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'foreign_key',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'alias',
                'string',
                [
                    'default' => '',
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'lft',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'rght',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'alias',
                ]
            )
            ->addIndex(
                [
                    'lft',
                    'rght',
                ]
            )
            ->create();

        $this->table('aros_acos')
            ->addColumn(
                'aro_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'aco_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                '_create',
                'string',
                [
                    'default' => '0',
                    'limit' => 2,
                    'null' => false,
                ]
            )
            ->addColumn(
                '_read',
                'string',
                [
                    'default' => '0',
                    'limit' => 2,
                    'null' => false,
                ]
            )
            ->addColumn(
                '_update',
                'string',
                [
                    'default' => '0',
                    'limit' => 2,
                    'null' => false,
                ]
            )
            ->addColumn(
                '_delete',
                'string',
                [
                    'default' => '0',
                    'limit' => 2,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'aco_id',
                ]
            )
            ->addIndex(
                [
                    'aro_id',
                ]
            )
            ->create();

        $this->table('auth_sub_urls')
            ->addColumn(
                'auth_url_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'url',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'auth_url_id',
                ]
            )
            ->create();

        $this->table('auth_urls')
            ->addColumn(
                'code',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'url',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'expire',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'created',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'expire',
                ]
            )
            ->create();

        $this->table('beanstalk_jobs')
            ->addColumn(
                'jobid',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'tube',
                'string',
                [
                    'default' => 'default',
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'priority',
                'integer',
                [
                    'default' => '1024',
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'last_status',
                'string',
                [
                    'default' => 'ready',
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'delay',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'ttr',
                'integer',
                [
                    'default' => '60',
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'errors',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('beanstalk_workers')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'tube',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'path',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'pid',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'last_launch',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'hostname',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->create();

        $this->table('configurations')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'setting',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                    'name',
                ]
            )
            ->create();

        $this->table('counters')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'definition_mask',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'sequence_reset_mask',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'sequence_reset_value',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'sequence_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                    'identifier',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->addIndex(
                [
                    'sequence_id',
                ]
            )
            ->create();

        $this->table('cron_executions')
            ->addColumn(
                'cron_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'date_begin',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'date_end',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'report',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'last_update',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'pid',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'cron_id',
                ]
            )
            ->create();

        $this->table('crons')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'classname',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'locked',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'frequency',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'next',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'app_meta',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'parallelisable',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'max_duration',
                'string',
                [
                    'default' => 'PT1H',
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'last_email',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->create();

        $this->table('deposit_values')
            ->addColumn(
                'deposit_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'form_input_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'input_value',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'deposit_id',
                ]
            )
            ->addIndex(
                [
                    'form_input_id',
                ]
            )
            ->create();

        $this->table('deposits')
            ->addColumn(
                'archival_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'transferring_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'form_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'last_state_update',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'states_history',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created_user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'path_token',
                'string',
                [
                    'default' => null,
                    'limit' => 8,
                    'null' => false,
                ]
            )
            ->addColumn(
                'error_message',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'archival_agency_id',
                ]
            )
            ->addIndex(
                [
                    'created_user_id',
                ]
            )
            ->addIndex(
                [
                    'form_id',
                ]
            )
            ->addIndex(
                [
                    'transferring_agency_id',
                ]
            )
            ->create();

        $this->table('file_extensions')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'name',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('file_extensions_pronoms')
            ->addColumn(
                'file_extension_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'pronom_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'file_extension_id',
                ]
            )
            ->addIndex(
                [
                    'pronom_id',
                ]
            )
            ->create();

        $this->table('fileuploads')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'path',
                'string',
                [
                    'default' => null,
                    'limit' => 2048,
                    'null' => true,
                ]
            )
            ->addColumn(
                'size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => true,
                ]
            )
            ->addColumn(
                'hash',
                'string',
                [
                    'default' => null,
                    'limit' => 4000,
                    'null' => true,
                ]
            )
            ->addColumn(
                'hash_algo',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'valid',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 50,
                    'null' => true,
                ]
            )
            ->addColumn(
                'mime',
                'string',
                [
                    'default' => null,
                    'limit' => 128,
                    'null' => true,
                ]
            )
            ->addColumn(
                'locked',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('filters')
            ->addColumn(
                'saved_filter_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'key',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'value',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'saved_filter_id',
                ]
            )
            ->create();

        $this->table('form_calculators')
            ->addColumn(
                'form_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'form_variable_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'multiple',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'form_id',
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'form_id',
                ]
            )
            ->addIndex(
                [
                    'form_variable_id',
                ]
            )
            ->create();

        $this->table('form_calculators_form_variables')
            ->addColumn(
                'form_calculator_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'form_variable_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'form_calculator_id',
                ]
            )
            ->addIndex(
                [
                    'form_variable_id',
                ]
            )
            ->create();

        $this->table('form_extractors')
            ->addColumn(
                'form_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'form_input_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'file_selector',
                'string',
                [
                    'default' => null,
                    'limit' => 2048,
                    'null' => true,
                ]
            )
            ->addColumn(
                'data_format',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'data_path',
                'string',
                [
                    'default' => null,
                    'limit' => 2048,
                    'null' => false,
                ]
            )
            ->addColumn(
                'app_meta',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'multiple',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'form_id',
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'form_id',
                ]
            )
            ->addIndex(
                [
                    'form_input_id',
                ]
            )
            ->create();

        $this->table('form_extractors_form_variables')
            ->addColumn(
                'form_extractor_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'form_variable_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'form_extractor_id',
                ]
            )
            ->addIndex(
                [
                    'form_variable_id',
                ]
            )
            ->create();

        $this->table('form_fieldsets')
            ->addColumn(
                'form_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'legend',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'ord',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'disable_expression',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'form_id',
                    'ord',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'form_id',
                ]
            )
            ->create();

        $this->table('form_inputs')
            ->addColumn(
                'form_fieldset_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'ord',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'label',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'default_value',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'required',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'readonly',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'disable_expression',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'pattern',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'placeholder',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'help',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'app_meta',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'form_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'multiple',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'form_fieldset_id',
                    'ord',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'name',
                    'form_id',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'form_fieldset_id',
                ]
            )
            ->addIndex(
                [
                    'form_id',
                ]
            )
            ->create();

        $this->table('form_inputs_form_variables')
            ->addColumn(
                'form_input_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'form_variable_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'form_input_id',
                ]
            )
            ->addIndex(
                [
                    'form_variable_id',
                ]
            )
            ->create();

        $this->table('form_transfer_headers')
            ->addColumn(
                'form_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'form_variable_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'form_id',
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'form_id',
                ]
            )
            ->addIndex(
                [
                    'form_variable_id',
                ]
            )
            ->create();

        $this->table('form_unit_contents')
            ->addColumn(
                'form_unit_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'form_variable_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'form_unit_id',
                ]
            )
            ->addIndex(
                [
                    'form_variable_id',
                ]
            )
            ->create();

        $this->table('form_unit_headers')
            ->addColumn(
                'form_unit_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'form_variable_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'form_unit_id',
                ]
            )
            ->addIndex(
                [
                    'form_variable_id',
                ]
            )
            ->create();

        $this->table('form_unit_keyword_details')
            ->addColumn(
                'form_unit_keyword_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'form_variable_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'form_unit_keyword_id',
                ]
            )
            ->addIndex(
                [
                    'form_variable_id',
                ]
            )
            ->create();

        $this->table('form_unit_keywords')
            ->addColumn(
                'form_unit_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'multiple_with',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'form_unit_id',
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'form_unit_id',
                ]
            )
            ->create();

        $this->table('form_unit_managements')
            ->addColumn(
                'form_unit_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'form_variable_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'form_unit_id',
                ]
            )
            ->addIndex(
                [
                    'form_variable_id',
                ]
            )
            ->create();

        $this->table('form_units')
            ->addColumn(
                'form_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'parent_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'lft',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'rght',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'form_input_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'presence_condition_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'search_expression',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'presence_required',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'form_id',
                ]
            )
            ->addIndex(
                [
                    'form_input_id',
                ]
            )
            ->addIndex(
                [
                    'parent_id',
                ]
            )
            ->addIndex(
                [
                    'presence_condition_id',
                ]
            )
            ->addIndex(
                [
                    'lft',
                    'rght',
                ]
            )
            ->create();

        $this->table('form_variables')
            ->addColumn(
                'form_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'twig',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'app_meta',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'form_id',
                ]
            )
            ->create();

        $this->table('forms')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'archiving_system_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'version_number',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'version_note',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'last_state_update',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'states_history',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'user_guide',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'seda_version',
                'string',
                [
                    'default' => 'seda2.1',
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'tested',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'identifier',
                    'version_number',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'archiving_system_id',
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('forms_transferring_agencies')
            ->addColumn(
                'form_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'form_id',
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('keyword_lists')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'version',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                    'identifier',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('keywords')
            ->addColumn(
                'keyword_list_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'version',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'code',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'app_meta',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'parent_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'lft',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'rght',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'exact_match',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'change_note',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'keyword_list_id',
                ]
            )
            ->addIndex(
                [
                    'parent_id',
                ]
            )
            ->create();

        $this->table('ldaps')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'host',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'port',
                'integer',
                [
                    'default' => '389',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'user_query_login',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'user_query_password',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'ldap_root_search',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'user_login_attribute',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => false,
                ]
            )
            ->addColumn(
                'ldap_users_filter',
                'string',
                [
                    'default' => null,
                    'limit' => 65535,
                    'null' => true,
                ]
            )
            ->addColumn(
                'account_prefix',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'account_suffix',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'use_proxy',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'use_ssl',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'use_tls',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'user_name_attribute',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'user_mail_attribute',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'schema',
                'string',
                [
                    'default' => 'Adldap\\Schemas\\ActiveDirectory',
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'follow_referrals',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'version',
                'integer',
                [
                    'default' => '3',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'timeout',
                'integer',
                [
                    'default' => '5',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'custom_options',
                'text',
                [
                    'default' => '[]',
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'user_username_attribute',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('mails')
            ->addColumn(
                'serialized',
                'binary',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->create();

        $this->table('mediainfo_audios')
            ->addColumn(
                'mediainfo_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format_info',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'mode_extension',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format_settings_endianness',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'codec_id',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'duration',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'bit_rate_mode',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'bit_rate',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'channel_s_',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'channel_positions',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'sampling_rate',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'frame_rate',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'bit_depth',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'compression_mode',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'delay_relative_to_video',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'stream_size',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'title',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'language',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                '_default',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'forced',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'mediainfo_id',
                ]
            )
            ->create();

        $this->table('mediainfo_images')
            ->addColumn(
                'mediainfo_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format_info',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'width',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'height',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'bit_depth',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'compression_mode',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'stream_size',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'mediainfo_id',
                ]
            )
            ->create();

        $this->table('mediainfo_texts')
            ->addColumn(
                'mediainfo_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'codec_id',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'codec_id_info',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'duration',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'bit_rate',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'count_of_elements',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'stream_size',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'title',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'language',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                '_default',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'forced',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'mediainfo_id',
                ]
            )
            ->create();

        $this->table('mediainfo_videos')
            ->addColumn(
                'mediainfo_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format_info',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format_profile',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format_settings_cabac',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format_settings_reframes',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'codec_id',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'duration',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'bit_rate',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'width',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'height',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'display_aspect_ratio',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'frame_rate_mode',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'frame_rate',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'color_space',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'chroma_subsampling',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'bit_depth',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'scan_type',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'bits_pixel_frame_',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'stream_size',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'title',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'writing_library',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'encoding_settings',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'language',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                '_default',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'forced',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'color_range',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'color_primaries',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'transfer_characteristics',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'matrix_coefficients',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'mediainfo_id',
                ]
            )
            ->create();

        $this->table('mediainfos')
            ->addColumn(
                'fileupload_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'unique_id',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'complete_name',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format_version',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'file_size',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'duration',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'overall_bit_rate',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'movie_name',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'encoded_date',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'writing_application',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'writing_library',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'fileupload_id',
                ]
            )
            ->create();

        $this->table('notifications')
            ->addColumn(
                'user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'text',
                'string',
                [
                    'default' => null,
                    'limit' => 2048,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'css_class',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('org_entities')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'parent_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'lft',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'rght',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'type_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archival_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'is_main_archival_agency',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'max_disk_usage',
                'biginteger',
                [
                    'default' => '0',
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'disk_usage',
                'biginteger',
                [
                    'default' => '0',
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'short_name',
                'string',
                [
                    'default' => null,
                    'limit' => 40,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'identifier',
                    'archival_agency_id',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'archival_agency_id',
                ]
            )
            ->addIndex(
                [
                    'parent_id',
                ]
            )
            ->addIndex(
                [
                    'type_entity_id',
                ]
            )
            ->addIndex(
                [
                    'id',
                    'name',
                ]
            )
            ->addIndex(
                [
                    'lft',
                    'rght',
                ]
            )
            ->create();

        $this->table('pronoms')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'puid',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'mime',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'puid',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('roles')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'code',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'parent_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'lft',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'rght',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'hierarchical_view',
                'boolean',
                [
                    'comment' => 'autorise ce rôle à voir les éléments de ses sous-entités',
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'agent_type',
                'string',
                [
                    'default' => 'person',
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'code',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->addIndex(
                [
                    'parent_id',
                ]
            )
            ->create();

        $this->table('roles_type_entities')
            ->addColumn(
                'role_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'type_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'role_id',
                ]
            )
            ->addIndex(
                [
                    'type_entity_id',
                ]
            )
            ->create();

        $this->table('saved_filters')
            ->addColumn(
                'user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'controller',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'action',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('sequences')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'value',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('sessions', ['id' => false, 'primary_key' => ['id']])
            ->addColumn(
                'id',
                'string',
                [
                    'default' => null,
                    'limit' => 40,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'data',
                'binary',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'expires',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'token',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'token',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('siegfrieds')
            ->addColumn(
                'fileupload_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'pronom',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'version',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'mime',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'basis',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'warning',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'fileupload_id',
                ]
            )
            ->create();

        $this->table('transfers')
            ->addColumn(
                'deposit_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'comment',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'path_token',
                'string',
                [
                    'default' => null,
                    'limit' => 8,
                    'null' => false,
                ]
            )
            ->addColumn(
                'data_count',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'data_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => true,
                ]
            )
            ->addColumn(
                'files_deleted',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'error_message',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'last_comm_date',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'last_comm_code',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'last_comm_message',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'created_user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'created_user_id',
                ]
            )
            ->addIndex(
                [
                    'deposit_id',
                ]
            )
            ->create();

        $this->table('type_entities')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'code',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'code',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('users')
            ->addColumn(
                'username',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'password',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'cookies',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'menu',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'high_contrast',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'role_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'email',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'ldap_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'agent_type',
                'string',
                [
                    'default' => 'person',
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'ldap_login',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'username',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'ldap_id',
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->addIndex(
                [
                    'role_id',
                ]
            )
            ->create();

        $this->table('versions')
            ->addColumn(
                'subject',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'version',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'datetime',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'subject',
                    'version',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('acos')
            ->addForeignKey(
                'parent_id',
                'acos',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('archiving_systems')
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('aros_acos')
            ->addForeignKey(
                'aco_id',
                'acos',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'aro_id',
                'aros',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('auth_sub_urls')
            ->addForeignKey(
                'auth_url_id',
                'auth_urls',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('beanstalk_jobs')
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'SET_NULL',
                ]
            )
            ->update();

        $this->table('configurations')
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('counters')
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'sequence_id',
                'sequences',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('cron_executions')
            ->addForeignKey(
                'cron_id',
                'crons',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('deposit_values')
            ->addForeignKey(
                'deposit_id',
                'deposits',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'form_input_id',
                'form_inputs',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('deposits')
            ->addForeignKey(
                'archival_agency_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'created_user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'SET_NULL',
                ]
            )
            ->addForeignKey(
                'form_id',
                'forms',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'transferring_agency_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('file_extensions_pronoms')
            ->addForeignKey(
                'file_extension_id',
                'file_extensions',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'pronom_id',
                'pronoms',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('fileuploads')
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'SET_NULL',
                ]
            )
            ->update();

        $this->table('filters')
            ->addForeignKey(
                'saved_filter_id',
                'saved_filters',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('form_calculators')
            ->addForeignKey(
                'form_id',
                'forms',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'form_variable_id',
                'form_variables',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('form_calculators_form_variables')
            ->addForeignKey(
                'form_calculator_id',
                'form_calculators',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'form_variable_id',
                'form_variables',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('form_extractors')
            ->addForeignKey(
                'form_id',
                'forms',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'form_input_id',
                'form_inputs',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('form_extractors_form_variables')
            ->addForeignKey(
                'form_extractor_id',
                'form_extractors',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'form_variable_id',
                'form_variables',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('form_fieldsets')
            ->addForeignKey(
                'form_id',
                'forms',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('form_inputs')
            ->addForeignKey(
                'form_fieldset_id',
                'form_fieldsets',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'form_id',
                'forms',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('form_inputs_form_variables')
            ->addForeignKey(
                'form_input_id',
                'form_inputs',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'form_variable_id',
                'form_variables',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('form_transfer_headers')
            ->addForeignKey(
                'form_id',
                'forms',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'form_variable_id',
                'form_variables',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('form_unit_contents')
            ->addForeignKey(
                'form_unit_id',
                'form_units',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'form_variable_id',
                'form_variables',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('form_unit_headers')
            ->addForeignKey(
                'form_unit_id',
                'form_units',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'form_variable_id',
                'form_variables',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('form_unit_keyword_details')
            ->addForeignKey(
                'form_unit_keyword_id',
                'form_unit_keywords',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'form_variable_id',
                'form_variables',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('form_unit_keywords')
            ->addForeignKey(
                'form_unit_id',
                'form_units',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('form_unit_managements')
            ->addForeignKey(
                'form_unit_id',
                'form_units',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'form_variable_id',
                'form_variables',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('form_units')
            ->addForeignKey(
                'form_id',
                'forms',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'form_input_id',
                'form_inputs',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'parent_id',
                'form_units',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'presence_condition_id',
                'form_variables',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('form_variables')
            ->addForeignKey(
                'form_id',
                'forms',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('forms')
            ->addForeignKey(
                'archiving_system_id',
                'archiving_systems',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('forms_transferring_agencies')
            ->addForeignKey(
                'form_id',
                'forms',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('keyword_lists')
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('keywords')
            ->addForeignKey(
                'keyword_list_id',
                'keyword_lists',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'parent_id',
                'keywords',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('ldaps')
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('mediainfo_audios')
            ->addForeignKey(
                'mediainfo_id',
                'mediainfos',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('mediainfo_images')
            ->addForeignKey(
                'mediainfo_id',
                'mediainfos',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('mediainfo_texts')
            ->addForeignKey(
                'mediainfo_id',
                'mediainfos',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('mediainfo_videos')
            ->addForeignKey(
                'mediainfo_id',
                'mediainfos',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('mediainfos')
            ->addForeignKey(
                'fileupload_id',
                'fileuploads',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('notifications')
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('org_entities')
            ->addForeignKey(
                'archival_agency_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'parent_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'type_entity_id',
                'type_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'SET_NULL',
                ]
            )
            ->update();

        $this->table('roles')
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'parent_id',
                'roles',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->update();

        $this->table('roles_type_entities')
            ->addForeignKey(
                'role_id',
                'roles',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'type_entity_id',
                'type_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('saved_filters')
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('sequences')
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('sessions')
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('siegfrieds')
            ->addForeignKey(
                'fileupload_id',
                'fileuploads',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('transfers')
            ->addForeignKey(
                'created_user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'SET_NULL',
                ]
            )
            ->addForeignKey(
                'deposit_id',
                'deposits',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();

        $this->table('users')
            ->addForeignKey(
                'ldap_id',
                'ldaps',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'NO_ACTION',
                ]
            )
            ->addForeignKey(
                'org_entity_id',
                'org_entities',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->addForeignKey(
                'role_id',
                'roles',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();
    }

    /**
     * Down Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-down-method
     * @return void
     */
    public function down()
    {
        $this->table('acos')
            ->dropForeignKey(
                'parent_id'
            )->save();

        $this->table('archiving_systems')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('aros_acos')
            ->dropForeignKey(
                'aco_id'
            )
            ->dropForeignKey(
                'aro_id'
            )->save();

        $this->table('auth_sub_urls')
            ->dropForeignKey(
                'auth_url_id'
            )->save();

        $this->table('beanstalk_jobs')
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('configurations')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('counters')
            ->dropForeignKey(
                'org_entity_id'
            )
            ->dropForeignKey(
                'sequence_id'
            )->save();

        $this->table('cron_executions')
            ->dropForeignKey(
                'cron_id'
            )->save();

        $this->table('deposit_values')
            ->dropForeignKey(
                'deposit_id'
            )
            ->dropForeignKey(
                'form_input_id'
            )->save();

        $this->table('deposits')
            ->dropForeignKey(
                'archival_agency_id'
            )
            ->dropForeignKey(
                'created_user_id'
            )
            ->dropForeignKey(
                'form_id'
            )
            ->dropForeignKey(
                'transferring_agency_id'
            )->save();

        $this->table('file_extensions_pronoms')
            ->dropForeignKey(
                'file_extension_id'
            )
            ->dropForeignKey(
                'pronom_id'
            )->save();

        $this->table('fileuploads')
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('filters')
            ->dropForeignKey(
                'saved_filter_id'
            )->save();

        $this->table('form_calculators')
            ->dropForeignKey(
                'form_id'
            )
            ->dropForeignKey(
                'form_variable_id'
            )->save();

        $this->table('form_calculators_form_variables')
            ->dropForeignKey(
                'form_calculator_id'
            )
            ->dropForeignKey(
                'form_variable_id'
            )->save();

        $this->table('form_extractors')
            ->dropForeignKey(
                'form_id'
            )
            ->dropForeignKey(
                'form_input_id'
            )->save();

        $this->table('form_extractors_form_variables')
            ->dropForeignKey(
                'form_extractor_id'
            )
            ->dropForeignKey(
                'form_variable_id'
            )->save();

        $this->table('form_fieldsets')
            ->dropForeignKey(
                'form_id'
            )->save();

        $this->table('form_inputs')
            ->dropForeignKey(
                'form_fieldset_id'
            )
            ->dropForeignKey(
                'form_id'
            )->save();

        $this->table('form_inputs_form_variables')
            ->dropForeignKey(
                'form_input_id'
            )
            ->dropForeignKey(
                'form_variable_id'
            )->save();

        $this->table('form_transfer_headers')
            ->dropForeignKey(
                'form_id'
            )
            ->dropForeignKey(
                'form_variable_id'
            )->save();

        $this->table('form_unit_contents')
            ->dropForeignKey(
                'form_unit_id'
            )
            ->dropForeignKey(
                'form_variable_id'
            )->save();

        $this->table('form_unit_headers')
            ->dropForeignKey(
                'form_unit_id'
            )
            ->dropForeignKey(
                'form_variable_id'
            )->save();

        $this->table('form_unit_keyword_details')
            ->dropForeignKey(
                'form_unit_keyword_id'
            )
            ->dropForeignKey(
                'form_variable_id'
            )->save();

        $this->table('form_unit_keywords')
            ->dropForeignKey(
                'form_unit_id'
            )->save();

        $this->table('form_unit_managements')
            ->dropForeignKey(
                'form_unit_id'
            )
            ->dropForeignKey(
                'form_variable_id'
            )->save();

        $this->table('form_units')
            ->dropForeignKey(
                'form_id'
            )
            ->dropForeignKey(
                'form_input_id'
            )
            ->dropForeignKey(
                'parent_id'
            )
            ->dropForeignKey(
                'presence_condition_id'
            )->save();

        $this->table('form_variables')
            ->dropForeignKey(
                'form_id'
            )->save();

        $this->table('forms')
            ->dropForeignKey(
                'archiving_system_id'
            )
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('forms_transferring_agencies')
            ->dropForeignKey(
                'form_id'
            )
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('keyword_lists')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('keywords')
            ->dropForeignKey(
                'keyword_list_id'
            )
            ->dropForeignKey(
                'parent_id'
            )->save();

        $this->table('ldaps')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('mediainfo_audios')
            ->dropForeignKey(
                'mediainfo_id'
            )->save();

        $this->table('mediainfo_images')
            ->dropForeignKey(
                'mediainfo_id'
            )->save();

        $this->table('mediainfo_texts')
            ->dropForeignKey(
                'mediainfo_id'
            )->save();

        $this->table('mediainfo_videos')
            ->dropForeignKey(
                'mediainfo_id'
            )->save();

        $this->table('mediainfos')
            ->dropForeignKey(
                'fileupload_id'
            )->save();

        $this->table('notifications')
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('org_entities')
            ->dropForeignKey(
                'archival_agency_id'
            )
            ->dropForeignKey(
                'parent_id'
            )
            ->dropForeignKey(
                'type_entity_id'
            )->save();

        $this->table('roles')
            ->dropForeignKey(
                'org_entity_id'
            )
            ->dropForeignKey(
                'parent_id'
            )->save();

        $this->table('roles_type_entities')
            ->dropForeignKey(
                'role_id'
            )
            ->dropForeignKey(
                'type_entity_id'
            )->save();

        $this->table('saved_filters')
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('sequences')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('sessions')
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('siegfrieds')
            ->dropForeignKey(
                'fileupload_id'
            )->save();

        $this->table('transfers')
            ->dropForeignKey(
                'created_user_id'
            )
            ->dropForeignKey(
                'deposit_id'
            )->save();

        $this->table('users')
            ->dropForeignKey(
                'ldap_id'
            )
            ->dropForeignKey(
                'org_entity_id'
            )
            ->dropForeignKey(
                'role_id'
            )->save();

        $this->table('acos')->drop()->save();
        $this->table('archiving_systems')->drop()->save();
        $this->table('aros')->drop()->save();
        $this->table('aros_acos')->drop()->save();
        $this->table('auth_sub_urls')->drop()->save();
        $this->table('auth_urls')->drop()->save();
        $this->table('beanstalk_jobs')->drop()->save();
        $this->table('beanstalk_workers')->drop()->save();
        $this->table('configurations')->drop()->save();
        $this->table('counters')->drop()->save();
        $this->table('cron_executions')->drop()->save();
        $this->table('crons')->drop()->save();
        $this->table('deposit_values')->drop()->save();
        $this->table('deposits')->drop()->save();
        $this->table('file_extensions')->drop()->save();
        $this->table('file_extensions_pronoms')->drop()->save();
        $this->table('fileuploads')->drop()->save();
        $this->table('filters')->drop()->save();
        $this->table('form_calculators')->drop()->save();
        $this->table('form_calculators_form_variables')->drop()->save();
        $this->table('form_extractors')->drop()->save();
        $this->table('form_extractors_form_variables')->drop()->save();
        $this->table('form_fieldsets')->drop()->save();
        $this->table('form_inputs')->drop()->save();
        $this->table('form_inputs_form_variables')->drop()->save();
        $this->table('form_transfer_headers')->drop()->save();
        $this->table('form_unit_contents')->drop()->save();
        $this->table('form_unit_headers')->drop()->save();
        $this->table('form_unit_keyword_details')->drop()->save();
        $this->table('form_unit_keywords')->drop()->save();
        $this->table('form_unit_managements')->drop()->save();
        $this->table('form_units')->drop()->save();
        $this->table('form_variables')->drop()->save();
        $this->table('forms')->drop()->save();
        $this->table('forms_transferring_agencies')->drop()->save();
        $this->table('keyword_lists')->drop()->save();
        $this->table('keywords')->drop()->save();
        $this->table('ldaps')->drop()->save();
        $this->table('mails')->drop()->save();
        $this->table('mediainfo_audios')->drop()->save();
        $this->table('mediainfo_images')->drop()->save();
        $this->table('mediainfo_texts')->drop()->save();
        $this->table('mediainfo_videos')->drop()->save();
        $this->table('mediainfos')->drop()->save();
        $this->table('notifications')->drop()->save();
        $this->table('org_entities')->drop()->save();
        $this->table('pronoms')->drop()->save();
        $this->table('roles')->drop()->save();
        $this->table('roles_type_entities')->drop()->save();
        $this->table('saved_filters')->drop()->save();
        $this->table('sequences')->drop()->save();
        $this->table('sessions')->drop()->save();
        $this->table('siegfrieds')->drop()->save();
        $this->table('transfers')->drop()->save();
        $this->table('type_entities')->drop()->save();
        $this->table('users')->drop()->save();
        $this->table('versions')->drop()->save();
    }
}
