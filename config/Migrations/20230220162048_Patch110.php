<?php
declare(strict_types=1);

use AsalaeCore\Factory\Utility;
use Migrations\AbstractMigration;

class Patch110 extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('beanstalk_jobs');
        $table->addColumn(
            'data',
            'binary',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'beanstalk_worker_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->addColumn(
            'object_model',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->addColumn(
            'object_foreign_key',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->addColumn(
            'job_state',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->addColumn(
            'last_state_update',
            'datetime',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'states_history',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addForeignKey(
            'beanstalk_worker_id',
            'beanstalk_workers',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'SET NULL'
            ]
        );
        $table->update();

        $table = $this->table('beanstalk_jobs');
        $table->removeColumn('jobid');
        $table->removeColumn('last_status');
        $table->update();

        $table = $this->table('form_units');
        $table->addColumn(
            'storage_calculation',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('form_fieldsets');
        $table->addColumn(
            'repeatable',
            'boolean',
            [
                'default' => false,
                'null' => false,
            ]
        );
        $table->addColumn(
            'required',
            'boolean',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'cardinality',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('form_units');
        $table->addColumn(
            'repeatable_fieldset_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->addForeignKey(
            'repeatable_fieldset_id',
            'form_fieldsets',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'NO_ACTION'
            ]
        );
        $table->update();

        $table = $this->table('form_fieldsets');
        $table->addColumn(
            'button_name',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('deposit_values');
        $table->addColumn(
            'fieldset_index',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('org_entities');

        // sqlite ne supporte pas le drop des foreign_key
        $adapter = $this->getAdapter();
        if ($adapter->getAdapterType() !== 'sqlite') {
            $table->removeColumn('archival_agency_id');
        } else {
            $table = $this->table('new_org_entities');
            $table->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
                ->addColumn(
                    'identifier',
                    'string',
                    [
                        'default' => null,
                        'limit' => 255,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'parent_id',
                    'integer',
                    [
                        'default' => null,
                        'limit' => 10,
                        'null' => true,
                    ]
                )
                ->addColumn(
                    'lft',
                    'integer',
                    [
                        'default' => null,
                        'limit' => 10,
                        'null' => true,
                    ]
                )
                ->addColumn(
                    'rght',
                    'integer',
                    [
                        'default' => null,
                        'limit' => 10,
                        'null' => true,
                    ]
                )
                ->addColumn(
                    'created',
                    'datetime',
                    [
                        'default' => null,
                        'limit' => null,
                        'null' => false,
                        'precision' => 6,
                        'scale' => 6,
                    ]
                )
                ->addColumn(
                    'modified',
                    'datetime',
                    [
                        'default' => null,
                        'limit' => null,
                        'null' => false,
                        'precision' => 6,
                        'scale' => 6,
                    ]
                )
                ->addColumn(
                    'type_entity_id',
                    'integer',
                    [
                        'default' => null,
                        'limit' => 10,
                        'null' => true,
                    ]
                )
                ->addColumn(
                    'active',
                    'boolean',
                    [
                        'default' => true,
                        'limit' => null,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'is_main_archival_agency',
                    'boolean',
                    [
                        'default' => null,
                        'limit' => null,
                        'null' => true,
                    ]
                )
                ->addColumn(
                    'description',
                    'text',
                    [
                        'default' => null,
                        'limit' => null,
                        'null' => true,
                    ]
                )
                ->addColumn(
                    'max_disk_usage',
                    'biginteger',
                    [
                        'default' => '0',
                        'limit' => 20,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'disk_usage',
                    'biginteger',
                    [
                        'default' => '0',
                        'limit' => 20,
                        'null' => false,
                    ]
                )
                ->addColumn(
                    'short_name',
                    'string',
                    [
                        'default' => null,
                        'limit' => 40,
                        'null' => true,
                    ]
                )
                ->addIndex(
                    [
                        'parent_id',
                    ]
                )
                ->addIndex(
                    [
                        'type_entity_id',
                    ]
                )
                ->addIndex(
                    [
                        'id',
                        'name',
                    ]
                )
                ->addIndex(
                    [
                        'lft',
                        'rght',
                    ]
                );
            $table->create();
            $org_entities = $adapter->getQueryBuilder()
                ->select('*')
                ->from('org_entities')
                ->orderAsc('id')
                ->execute()
                ->fetchAll('assoc');
            foreach ($org_entities as $org_entitie) {
                $table->insert($org_entitie);
            }
            $table->update();

            $table = $this->table('org_entities');
            $table->drop();
            $table->update();

            $table = $this->table('new_org_entities');
            $table->rename('org_entities');
        }
        $table->update();

        $table = $this->table('deposits');
        $table->addColumn(
            'request_data',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('rgpd_infos');
        $table->addColumn(
            'org_entity_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->addColumn(
            'org_name',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'org_address',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'org_siret',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->addColumn(
            'org_ape',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->addColumn(
            'org_phone',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->addColumn(
            'org_email',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->addColumn(
            'ceo_name',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->addColumn(
            'ceo_function',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->addColumn(
            'dpo_name',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->addColumn(
            'dpo_email',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->addColumn(
            'comment',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addForeignKey(
            'org_entity_id',
            'org_entities',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'CASCADE',
            ]
        );
        $table->create();

        $table = $this->table('change_entity_requests');
        $table->addColumn(
            'base_archival_agency_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'target_archival_agency_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'user_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ]
        );
        $table->addColumn(
            'reason',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'created',
            'datetime',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addForeignKey(
            'base_archival_agency_id',
            'org_entities',
            'id',
            [
                'delete' => 'CASCADE',
                'update' => 'CASCADE',
            ]
        );
        $table->addForeignKey(
            'target_archival_agency_id',
            'org_entities',
            'id',
            [
                'delete' => 'CASCADE',
                'update' => 'CASCADE',
            ]
        );
        $table->addForeignKey(
            'user_id',
            'users',
            'id',
            [
                'delete' => 'CASCADE',
                'update' => 'CASCADE',
            ]
        );
        $table->create();

        $table = $this->table('deposits');
        $table->addColumn(
            'originating_agency_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ]
        );
        $table->addForeignKey(
            'originating_agency_id',
            'org_entities',
            'id',
            [
                'delete' => 'CASCADE',
                'update' => 'CASCADE',
            ]
        );
        $table->update();

        $this->table('deposits')
            ->addColumn(
                'validated',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->update();

        if (!defined('TMP_TESTDIR')) {
            $Exec = Utility::get('Exec');
            $Exec->setDefaultStdout(LOGS . 'patch110.log');
            $Exec->async("sleep 5 && " . CAKE_SHELL, 'patch110');
        }

        $this->table('deposits')
            ->changeColumn(
                'validated',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->renameColumn('validated', 'bypass_validation')
            ->update();
    }

    public function down()
    {
        $table = $this->table('beanstalk_jobs');
        $table->removeColumn('data');
        $table->removeColumn('beanstalk_worker_id');
        $table->removeColumn('object_model');
        $table->removeColumn('object_foreign_key');
        $table->removeColumn('job_state');
        $table->removeColumn('last_state_update');
        $table->removeColumn('states_history');
        $table->update();

        $table = $this->table('beanstalk_jobs');
        $table->addColumn(
            'jobid',
            'integer',
            [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ]
        );
        $table->addColumn(
            'last_status',
            'string',
            [
                'default' => 'ready',
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('form_units');
        $table->removeColumn('storage_calculation');
        $table->update();

        $table = $this->table('form_fieldsets');
        $table->removeColumn('repeatable');
        $table->removeColumn('required');
        $table->removeColumn('cardinality');
        $table->update();

        $table = $this->table('form_units');
        $table->removeColumn('repeatable_fieldset_id');
        $table->update();

        $table = $this->table('form_fieldsets');
        $table->removeColumn('button_name');
        $table->update();

        $table = $this->table('deposit_values');
        $table->removeColumn('fieldset_index');
        $table->update();

        $table = $this->table('org_entities');
        $table->addColumn(
            'archival_agency_id',
            'integer',
            [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ]
        );
        $table->update();

        $table = $this->table('deposits');
        $table->removeColumn('request_data');
        $table->update();

        $table = $this->table('rgpd_infos');
        $table->drop();
        $table->update();

        $table = $this->table('change_entity_requests');
        $table->drop();
        $table->update();

        $table = $this->table('deposits');
        $table->removeColumn('originating_agency_id');
        $table->update();

        $this->table('deposits')
            ->removeColumn('validated')
            ->update();

        $this->table('deposits')
            ->changeColumn(
                'bypass_validation',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->renameColumn('bypass_validation', 'validated')
            ->update();
    }
}
