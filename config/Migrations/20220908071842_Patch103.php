<?php
declare(strict_types=1);

use Migrations\AbstractMigration;
use Versae\Model\Table\FormInputsTable;

class Patch103 extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('archiving_systems');
        $table->addIndex(['org_entity_id', 'name'], ['unique' => true]);
        $table->update();

        $adapter = $this->getAdapter();
        $inputs = $adapter->getQueryBuilder()
            ->select(
                [
                    'id',
                    'app_meta',
                ]
            )
            ->from('form_inputs')
            ->where(['type' => FormInputsTable::TYPE_FILE])
            ->execute()
            ->fetchAll('assoc');

        foreach ($inputs as $input) {
            $app_meta = json_decode($input['app_meta'] ?? '', true);
            if (isset($app_meta['formats'])) {
                $app_meta['formats'] = $app_meta['formats'] === 'all'
                    ? []
                    : [$app_meta['formats']];

                $adapter->getQueryBuilder()
                    ->update('form_inputs')
                    ->set(['app_meta' => json_encode($app_meta, JSON_UNESCAPED_SLASHES)])
                    ->where(['id' => $input['id']])
                    ->execute();
            }
        }
    }

    public function down()
    {
        $table = $this->table('archiving_systems');
        $table->removeIndex(['org_entity_id', 'name']);
        $table->update();

        $adapter = $this->getAdapter();
        $inputs = $adapter->getQueryBuilder()
            ->select(
                [
                    'id',
                    'app_meta',
                ]
            )
            ->from('form_inputs')
            ->where(['type' => FormInputsTable::TYPE_FILE])
            ->execute()
            ->fetchAll('assoc');

        foreach ($inputs as $input) {
            $app_meta = json_decode($input['app_meta'] ?? '', true);
            if (isset($app_meta['formats'])) {
                $app_meta['formats'] = $app_meta['formats'] === []
                    ? 'all'
                    : current($app_meta['formats']);

                $adapter->getQueryBuilder()
                    ->update('form_inputs')
                    ->set(['app_meta' => json_encode($app_meta, JSON_UNESCAPED_SLASHES)])
                    ->where(['id' => $input['id']])
                    ->execute();
            }
        }
    }
}
