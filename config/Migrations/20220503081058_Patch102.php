<?php
/**
 * @noinspection PhpCSValidationInspection
 */
declare(strict_types=1);

use Migrations\AbstractMigration;

class Patch102 extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('deposit_values');
        $table->addColumn(
            'hidden',
            'boolean',
            [
                'default' => false,
                'null' => false,
            ]
        );
        $table->changeColumn(
            'input_value',
            'text',
            [
                'default' => null,
                'limit' => null,
                'null' => true,
            ]
        );
        $table->update();
    }

    public function down()
    {
        $table = $this->table('deposit_values');
        $table->removeColumn('hidden');
        $table->changeColumn(
            'input_value',
            'text',
            [
                'default' => null,
                'limit' => null,
                'null' => false,
            ]
        );
        $table->update();
    }
}
