<?php
/**
 * @noinspection PhpCSValidationInspection
 */
declare(strict_types=1);

use Migrations\AbstractMigration;

class Patch101 extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('roles');
        $table->removeIndex(['name']);
        $table->addIndex(['org_entity_id', 'name'], ['unique' => true]);
        $table->update();

        $this->table('deposits')
            ->changeColumn(
                'created_user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->save();

        $this->table('transfers')
            ->changeColumn(
                'created_user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->save();
    }

    public function down()
    {
        $table = $this->table('roles');
        $table->removeIndex(['name']);
        $table->addIndex(['name'], ['unique' => true]);
        $table->update();
    }
}
