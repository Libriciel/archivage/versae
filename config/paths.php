<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         3.0.0
 * @license       MIT License (https://opensource.org/licenses/mit-license.php)
 */

/**
 * Use the DS to separate the directories in other defines
 */
if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

/**
 * These defines should only be edited if you have cake installed in
 * a directory layout other than the way it is distributed.
 * When using custom settings be sure to use the DS and do not add a trailing DS.
 */

/**
 * The full path to the directory which holds "src", WITHOUT a trailing DS.
 */
define('ROOT', dirname(__DIR__));

/**
 * The actual directory name for the application directory. Normally
 * named 'src'.
 */
define('APP_DIR', 'src');

/**
 * Path to the application's directory.
 */
define('APP', ROOT . DS . APP_DIR . DS);

/**
 * Path to the config directory.
 */
define('CONFIG', ROOT . DS . 'config' . DS);

/**
 * File path to the webroot directory.
 */
define('WWW_ROOT', ROOT . DS . 'webroot' . DS);

/**
 * Path to the tests directory.
 */
define('TESTS', ROOT . DS . 'tests' . DS);
define('TEST_DATA', ROOT . DS . 'tests' . DS . 'Data' . DS);

/**
 * Path to the temporary files directory.
 */
define('TMP', ROOT . DS . 'tmp' . DS);

/**
 * Path to the logs directory.
 */
define('LOGS', ROOT . DS . 'logs' . DS);

/**
 * Path to the cache files directory. It can be shared between hosts in a multi-server setup.
 */
define('CACHE', TMP . 'cache' . DS);

/**
 * Path to the resources directory.
 */
define('RESOURCES', ROOT . DS . 'resources' . DS);

/**
 * Nom du dossier utilisé par les fichiers compilés (javascript)
 */
define('COMPILED_DIR', 'compiled');

/**
 * Chemin vers les fichiers javascript compilés
 */
define('COMPILED_JS', WWW_ROOT . 'js' . DS . COMPILED_DIR . DS);

/**
 * The absolute path to the "cake" directory, WITHOUT a trailing DS.
 *
 * CakePHP should always be installed with composer, so look there.
 */
define('CAKE_CORE_INCLUDE_PATH', ROOT . DS . 'vendor' . DS . 'cakephp' . DS . 'cakephp');

/**
 * Path to the cake directory.
 */
define('CORE_PATH', CAKE_CORE_INCLUDE_PATH . DS);
define('CAKE', CORE_PATH . 'src' . DS);

/**
 * Chemin vers le coeur
 */
define('ASALAE_CORE_INCLUDE_PATH', ROOT . DS . 'vendor' . DS . 'libriciel' . DS . 'asalae-core');

/**
 * Asalae-core src
 */
define('ASALAE_CORE', ASALAE_CORE_INCLUDE_PATH . DS . 'src');
define('ASALAE_CORE_TESTS', ASALAE_CORE_INCLUDE_PATH . DS . 'tests');
define('ASALAE_CORE_TEST_DATA', ASALAE_CORE_TESTS . DS . 'Data');

/**
 * Chemin vers le fichier XSL SEDAv0.2
 */
define('SEDA_V02_XSL', WWW_ROOT . 'seda2html' . DS . 'seda2html_v02' . DS . 'seda.xsl');
define('SEDA_ARCHIVE_V02_XSL', WWW_ROOT . 'seda2html' . DS . 'seda2html_v02' . DS . 'sedaArchive2html_v02.xsl');

/**
 * Chemin vers le fichier XSD SEDAv0.2
 */
define('SEDA_V02_XSD', WWW_ROOT . 'xmlSchemas' . DS . 'seda_v0-2' . DS . 'seda_v0-2.xsd');
define(
    'SEDA_ARCHIVE_V02_XSD',
    WWW_ROOT . 'xmlSchemas' . DS . 'seda_v0-2' . DS . 'v02' . DS . 'archives_echanges_v0-2_archive.xsd'
);
define('SEDA_V02_CODES', WWW_ROOT . 'xmlSchemas' . DS . 'seda_v0-2' . DS . 'v02' . DS . 'codes');
define(
    'SEDA_V02_CODES_FILES',
    [
        'lang' => 'archives_echanges_v0-2_language_code.xsd',
        'access_code' => 'archives_echanges_v0-2_accessrestriction_code.xsd',
        'keywordtype' => 'archives_echanges_v0-2_keywordtype_code.xsd',
        'unit' => 'UNECE_MeasurementUnitCommonCode_5.xsd',
        'filetype' => 'archives_echanges_v0-2_filetype_code.xsd',
        'mime' => 'IANA_MIMEMediaType_20081112.xsd',
        'encoding' => 'UNECE_CharacterSetEncodingCode_40106.xsd',
        'charset' => 'IANA_CharacterSetCode_20070514.xsd',
        'type' => 'archives_echanges_v0-2_documenttype_code.xsd',
    ]
);

/**
 * Chemin vers le fichier XSL SEDAv1.0
 */
define('SEDA_V10_XSL', WWW_ROOT . 'seda2html' . DS . 'seda2html_v10' . DS . 'seda2html_v10.xsl');

/**
 * Chemin vers le fichier XSD SEDAv1.0
 */
define('SEDA_V10_XSD', WWW_ROOT . 'xmlSchemas' . DS . 'seda_v1-0' . DS . 'seda_v1-0.xsd');
define('SEDA_V10_CODES', WWW_ROOT . 'xmlSchemas' . DS . 'seda_v1-0' . DS . 'v10' . DS . 'codes');
define(
    'SEDA_V10_CODES_FILES',
    [
        'agencyid' => 'UNECE_AgencyIdentificationCode_D10A.xsd',
        'lang' => 'seda_v1-0_language_code.xsd',
        'access_code' => 'seda_v1-0_accessrestriction_code.xsd',
        'appraisal_code' => 'seda_v1-0_appraisal_code.xsd',
        'keywordtype' => 'seda_v1-0_keywordtype_code.xsd',
        'filetype' => 'seda_v1-0_filetype_code.xsd',
        'mime' => 'IANA_MIMEMediaType_20110216.xsd',
        'encoding' => 'UNECE_CharacterSetEncodingCode_40106.xsd',
        'charset' => 'IANA_CharacterSetCode_20101104.xsd',
        'type' => 'seda_v1-0_documenttype_code.xsd',
        'level' => 'seda_v1-0_descriptionlevel_code.xsd',
    ]
);

/**
 * Chemin vers le fichier XSL SEDAv2.0
 */
define('SEDA_V20_XSL', WWW_ROOT . 'seda2html' . DS . 'seda2html_v20' . DS . 'seda2html_v20.xsl');

/**
 * Chemin vers le fichier XSD SEDAv2.0
 */
define('SEDA_V20_XSD', WWW_ROOT . 'xmlSchemas' . DS . 'seda_v2-0' . DS . 'seda-2.0-main.xsd');

/**
 * Chemin vers le fichier XSD SEDAv2.0
 */
define('SEDA_V21_XSD', WWW_ROOT . 'xmlSchemas' . DS . 'seda_v2-1' . DS . 'seda-2.1-main.xsd');
define('SEDA_ARCHIVE_V21_XSD', WWW_ROOT . 'xmlSchemas' . DS . 'seda_v2-1' . DS . 'asalae-seda-2.1-archive.xsd');

/**
 * Chemin vers le fichier XSD SEDAv2.2
 */
define('SEDA_V22_XSD', WWW_ROOT . 'xmlSchemas' . DS . 'seda_v2-2' . DS . 'seda-2.2-main.xsd');
define('SEDA_ARCHIVE_V22_XSD', WWW_ROOT . 'xmlSchemas' . DS . 'seda_v2-2' . DS . 'asalae-seda-2.2-archive.xsd');

/**
 * Chemin vers le fichier XSD OAI-PMHv2.0
 */
define('OAIPMH_V20_XSD', WWW_ROOT . 'xmlSchemas' . DS . 'oai_pmh' . DS . 'OAI_PMH_2.0.xsd');

/**
 * Chemin vers le fichier XSD OAI-PMHv2.0-lax
 */
define('OAIPMH_V20_XSD_LAX', WWW_ROOT . 'xmlSchemas' . DS . 'oai_pmh' . DS . 'OAI_PMH_2.0-lax.xsd');

/**
 * Chemin vers le fichier XSD de la norme EAC-CPF
 */
define('EAC_CPF_XSD', WWW_ROOT . 'xmlSchemas' . DS . 'EAC-CPF' . DS . 'cpf.xsd');

/**
 * Chemin vers le fichier premis.xsd
 */
define('PREMIS_V3', WWW_ROOT . 'xmlSchemas' . DS . 'premis' . DS . 'premis.xsd');

/**
 * Chemin vers le shell cake
 */
define('CAKE_SHELL', ROOT . DS . 'bin' . DS . 'cake');

/**
 * Chemin vers le fichier VERSION.txt
 */
define('APP_VERSION', ROOT . DS . 'VERSION.txt');

/**
 * Chemin vers le fichier XSL SEDAv2.1
 */
define('SEDA_V21_XSL', WWW_ROOT . 'seda2html' . DS . 'seda2html_v21' . DS . 'seda2html_v21.xsl');

/**
 * Chemin vers le fichier XSL SEDAv2.2
 */
define('SEDA_V22_XSL', WWW_ROOT . 'seda2html' . DS . 'seda2html_v22' . DS . 'seda2html_v22.xsl');
define('SEDA_ARCHIVE_V22_XSL', SEDA_V22_XSL);
