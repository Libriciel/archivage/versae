<?php
/**
 * @noinspection PhpCSValidationInspection
 */
declare(strict_types=1);

use Cake\Core\Configure;
use Cake\I18n\FrozenTime;
use Cake\ORM\TableRegistry;
use Migrations\AbstractSeed;
use Versae\Model\Table\VersionsTable;

/**
 * Patch004v110Seed seed.
 */
class Patch004v110Seed extends AbstractSeed
{
    const VERSION = '1.1.0';

    public function run(): void
    {
        $appName = Configure::read('App.name', 'versae');
        /** @var VersionsTable $Versions */
        $Versions = TableRegistry::getTableLocator()->get('Versions');
        if ($Versions->find()->where(['subject' => $appName, 'version' => self::VERSION])->count()) {
            return;
        }

        $this->insert(
            'versions',
            [
                'subject' => $appName,
                'version' => self::VERSION,
                'created' => (new FrozenTime)->format(DATE_RFC3339),
            ]
        );
    }

    public function getDependencies(): array
    {
        return [
            Patch003v103Seed::class,
        ];
    }
}
