<?php
/**
 * @noinspection PhpCSValidationInspection
 */
declare(strict_types=1);

use AsalaeCore\Cron\Pronom;
use AsalaeCore\Cron\Unlocker;
use Versae\Cron\Check;
use Versae\Cron\DeleteExpiredData;
use Versae\Cron\TransferTracking;
use Cake\Core\Configure;
use Cake\I18n\FrozenTime as Time;
use Cake\ORM\Entity;
use Migrations\AbstractSeed;

/**
 * Initial seed.
 *
 * 1er Seed
 * @noinspection PhpCSValidationInspection
 */
class Patch000InitialSeed extends AbstractSeed
{
    /**
     * @var Entity[] Mémorise les entités créé
     */
    private $roles;

    const R_ARCHIVISTE = "Archiviste";
    const R_REF_ARCHIVE = "Référent Archives";
    const R_VERSANT = "Versant";

    /**
     * @var array Liste de champ name de la table roles
     */
    const ROLES = [
        self::R_ARCHIVISTE => 'AA',
        self::R_REF_ARCHIVE => 'ARA',
        self::R_VERSANT => 'AV',
    ];

    const ROLES_DESC = [
        self::R_ARCHIVISTE => "Ce rôle utilisateur est réservé aux archivistes"
            ." du Service d'Archives et autorise l'accès à toutes les "
            ."fonctions de l'application. Ce rôle permet de réaliser le "
            ."paramétrage fonctionnel ainsi que la gestion des formulaires."
            ." Il permet également de faire des versements pour le compte des services versants.",
        self::R_REF_ARCHIVE => "Ce rôle correspond à un agent des services qui"
            ." est en relation avec le Service d'Archives. Il a la"
            ." possibilité de faire des versements pour le compte"
            ." d'autres services versants.",
        self::R_VERSANT => "Ce rôle utilisateur correspond à un agent d'un "
            ."service versant. Il permet de faire et de suivre des versements pour son service.",
    ];

    /**
     * @var array Champ name en clef, name[] des roles liés en valeur
     */
    const TYPES = [
        [
            'name' => "Service d'Exploitation",
            'code' => 'SE',
            'roles' => [],
        ],
        [
            'name' => "Service d'Archives",
            'code' => 'SA',
            'roles' => [
                self::R_ARCHIVISTE,
            ],
        ],
        [
            'name' => "Service Versant",
            'code' => 'SV',
            'roles' => [
                self::R_REF_ARCHIVE,
                self::R_VERSANT,
            ],
        ],
        [
            'name' => "Organisationnel",
            'code' => 'SO',
            'roles' => [
                self::R_REF_ARCHIVE,
                self::R_VERSANT,
            ],
        ],
    ];

    public function run(): void
    {
        $conn = $this->getAdapter()->getConnection();
        try {
            $statement = $conn->query('SELECT COUNT(*) FROM versions');
            $count = $statement->fetchColumn();
        } catch (PDOException $e) {
            $count = 0;
        }

        // Si il y a des entrées dans version, c'est qu'il n'y a pas besoin de ce seed
        if ($count) {
            return;
        }

        $i = 1;
        foreach (self::ROLES as $role => $code) {
            $this->table('roles')
                ->insert(
                    [
                        'name' => $role,
                        'code' => $code,
                        'active' => true,
                        'lft' => $i++,
                        'rght' => $i++,
                        'hierarchical_view' => (int)in_array($code, ['AA', 'AC', 'ARA']),
                        'created' => (new DateTime)->format(DATE_RFC3339),
                        'modified' => (new DateTime)->format(DATE_RFC3339),
                        'description' => self::ROLES_DESC[$role],
                    ]
                )
                ->saveData();
            $this->roles[$role] = $conn->lastInsertId();
        }
        foreach (self::TYPES as $values) {
            $this->table('type_entities')
                ->insert(
                    [
                        'name' => $values['name'],
                        'code' => $values['code'],
                        'active' => true,
                        'created' => (new DateTime)->format(DATE_RFC3339),
                        'modified' => (new DateTime)->format(DATE_RFC3339),
                    ]
                )
                ->saveData();
            $type_entity_id = $conn->lastInsertId();

            foreach ($values['roles'] as $role) {
                $this->table('roles_type_entities')
                    ->insert(
                        [
                            'role_id' => $this->roles[$role],
                            'type_entity_id' => $type_entity_id,
                        ]
                    )
                    ->saveData();
            }
        }

        $data = [
            [
                'name' => "Vérification du fonctionnement de versae",
                'description' => "<p>La tâche vérifie que versae fonctionne dans les conditions nominales et qu'il
est en mesure d'assurer le service demandé.</p>
<p>Les vérifications portent sur :
<ul>
<li>l'installation de l'application : vérification du serveur, de php
(version et modules supplémentaires), de postgres,
des sources de l'application (version, plugin, ...),
de la version du schéma de la base de données</li>

<li>les outils et services externes : vérification des outils de détection du"
                    ." format des fichiers, de l'antivirus, ...</li>

</ul>
</p>",
                'classname' => Check::class,
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'app_meta' => null,
                'parallelisable' => true,
            ],
            [
                'name' => "Déverrouillage des tâches planifiées bloquées",
                'description' => "Lorsqu'une tâche planifiée reste verrouillée, "
                    ."cette tâche permet de la déverrouiller après un délai d'une heure par défaut",
                'classname' => Unlocker::class,
                'active' => true,
                'locked' => false,
                'frequency' => 'PT1H',
                'app_meta' => null,
                'parallelisable' => true,
            ],
            [
                'name' => 'Mise à jour de la liste des pronoms',
                'description' => 'Fait appel à un webservice pour mettre à jour à table des pronoms (fmt)',
                'classname' => Pronom::class,
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'app_meta' => '{"wsdl":"https:\\/\\/www.nationalarchives.gov.uk\\'
                    .'/PRONOM\\/Services\\/Contract\\/PRONOM.wsdl","endpoint":'
                    .'"https:\\/\\/www.nationalarchives.gov.uk\\/PRONOM\\/service.asmx",'
                    .'"action_version":"getSignatureFileVersionV1","action_data":'
                    .'"getSignatureFileV1","extract_version":"Version.Version",'.
                    '"extract_data":"SignatureFile.FFSignatureFile.FileFormatCollection.FileFormat","use_proxy":true}',
                'parallelisable' => true,
            ],
            [
                'name' => 'Suppression des données expirées',
                'description' => 'Suppression des données expirées en base de données et sur le serveur',
                'classname' => DeleteExpiredData::class,
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'app_meta' => null,
                'parallelisable' => true,
            ],
            [
                'name' => __("Transferts : suivi"),
                'description' => __(
                    "Lecture des accusés de réception et des réponses aux transferts en cours"
                ),
                'classname' => TransferTracking::class,
                'active' => true,
                'locked' => false,
                'frequency' => 'PT5M',
                'next' => (new DateTime('02:00:00'))
                    ->sub(new DateInterval('P1D'))
                    ->format('Y-m-dTH:i:s'),
                'app_meta' => null,
                'parallelisable' => false,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ]
        ];

        foreach ($data as $cron) {
            $this->table('crons')
                ->insert(
                    $cron + [
                        'next' => (new DateTime('02:00:00'))
                            ->sub(new DateInterval('P1D'))
                            ->format(DATE_RFC3339),
                    ]
                )
                ->saveData();
        }

        $this->table('versions')
            ->insert(
                [
                    'subject' => Configure::read('App.name', 'versae'),
                    'version' => '1.0.0',
                    'created' => (new Time)->format(DATE_RFC3339)
                ]
            )
            ->saveData();
    }
}
