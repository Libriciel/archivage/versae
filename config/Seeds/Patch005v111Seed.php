<?php
/**
 * @noinspection PhpCSValidationInspection
 */
declare(strict_types=1);

use Cake\Core\Configure;
use Cake\I18n\FrozenTime;
use Cake\ORM\TableRegistry;
use Migrations\AbstractSeed;
use Versae\Cron\JobMaker;
use Versae\Model\Table\VersionsTable;

/**
 * Patch005v111Seed seed.
 */
class Patch005v111Seed extends AbstractSeed
{
    const VERSION = '1.1.1';

    public function run(): void
    {
        $appName = Configure::read('App.name', 'versae');
        /** @var VersionsTable $Versions */
        $Versions = TableRegistry::getTableLocator()->get('Versions');
        if ($Versions->find()->where(['subject' => $appName, 'version' => self::VERSION])->count()) {
            return;
        }

        $this->insert(
            'versions',
            [
                'subject' => $appName,
                'version' => self::VERSION,
                'created' => (new FrozenTime)->format(DATE_RFC3339),
            ]
        );

        $this->insert(
            'crons',
            [
                'name' => __("Créateur de jobs"),
                'description' => __(
                    "Recherche des cas bloqués dans un état intermédiaire " .
                    "afin de lancer les jobs qui pourraient le débloquer"
                ),
                'classname' => JobMaker::class,
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'next' => (new DateTime('02:30:00'))
                    ->sub(new DateInterval('P1D'))
                    ->format(DATE_RFC3339),
                'app_meta' => null,
                'parallelisable' => true,
                'max_duration' => 'PT1H',
                'last_email' => null,
            ]
        );
    }

    public function getDependencies(): array
    {
        return [
            Patch004v110Seed::class,
        ];
    }
}
