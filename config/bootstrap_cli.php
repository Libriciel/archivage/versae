<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         3.0.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @noinspection  PhpIgnoredClassAliasDeclaration utilisé dans PsyShell
 */

use AsalaeCore\Utility\DOMUtility;
use Cake\Core\Configure;

/**
 * Additional bootstrapping and configuration for CLI environments should
 * be put here.
 */

// Set logs to different files so they don't have permission conflicts.
Configure::write('Log.debug.file', 'cli-debug');
Configure::write('Log.error.file', 'cli-error');

class_alias(AsalaeCore\MinkSuite\MinkBeanstalk::class, 'MinkBeanstalk');
class_alias(AsalaeCore\Utility\Config::class, 'Config');
class_alias(Cake\Core\Configure::class, 'Configure');
class_alias(Cake\Datasource\ConnectionManager::class, 'ConnectionManager');
class_alias(Cake\Datasource\EntityInterface::class, 'EntityInterface');
class_alias(Cake\ORM\Entity::class, 'Entity');
class_alias(Cake\ORM\TableRegistry::class, 'T');
class_alias(Cake\ORM\TableRegistry::class, 'TableRegistry');
class_alias(DOMUtility::class, 'DOMUtility');
class_alias(Libriciel\Filesystem\Utility\Filesystem::class, 'Filesystem');
class_alias(Versae\MinkSuite\MinkCase::class, 'MinkCase');
