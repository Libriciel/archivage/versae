<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.8
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @noinspection PhpCSValidationInspection PhpInternalEntityUsedInspection
 */

// You can remove this if you are confident that your PHP version is sufficient.
if (version_compare(PHP_VERSION, '7.2.0') < 0) {
    trigger_error('Your PHP version must be equal or higher than 7.2.0', E_USER_ERROR);
}

/*
 *  You can remove this if you are confident you have intl installed.
 */
if (!extension_loaded('intl')) {
    trigger_error('You must enable the intl extension to use CakePHP.', E_USER_ERROR);
}

/*
 * You can remove this if you are confident you have mbstring installed.
 */
if (!extension_loaded('mbstring')) {
    trigger_error('You must enable the mbstring extension to use CakePHP.', E_USER_ERROR);
}

/*
 * Configure paths required to find CakePHP + general filepath
 * constants
 */
require __DIR__ . '/paths.php';
require __DIR__ . '/constants.php';

/**
 * Ajoute/remplace des fonctions basics de Cakephp
 */
if (is_file(ASALAE_CORE . DS . 'basics.php')) {
    require ASALAE_CORE . DS . 'basics.php';
}
if (is_file(APP . 'basics.php')) {
    require APP . 'basics.php';
}

/*
 * Bootstrap CakePHP.
 *
 * Does the various bits of setup that CakePHP needs to do.
 * This includes:
 *
 * - Registering the CakePHP autoloader.
 * - Setting the default application paths.
 */
require CORE_PATH . 'config' . DS . 'bootstrap.php';

use AsalaeCore\Database\Type\DateTimeFractionalType;
use AsalaeCore\Database\Type\DateTimeType;
use AsalaeCore\Database\Type\DateType;
use AsalaeCore\Error\Debug\HtmlFormatter;
use AsalaeCore\Error\ErrorTrap;
use AsalaeCore\Error\ExceptionTrap;
use AsalaeCore\Factory;
use AsalaeCore\ORM\Locator\TableLocator;
use AsalaeCore\Validation\RulesProvider;
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
use Cake\Database\TypeFactory;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\FactoryLocator;
use Cake\Error\Debug\ConsoleFormatter;
use Cake\Error\Debug\TextFormatter;
use Cake\Error\Debugger;
use Cake\Http\ServerRequest;
use Cake\Log\Log;
use Cake\Mailer\Mailer;
use Cake\Mailer\TransportFactory;
use Cake\Routing\Router;
use Cake\Utility\Security;
use Cake\Validation\Validator;
use Detection\MobileDetect;

/**
 * Force des mot de passe
 * @see https://www.ssi.gouv.fr/administration/precautions-elementaires/calculer-la-force-dun-mot-de-passe/
 */
/**
 * Mot de passe de 8 caractères dans un alphabet de 70 symboles
 * Taille usuelle
 */
define('PASSWORD_ULTRA_WEAK', 49);
/**
 * Mot de passe de 12 caractères dans un alphabet de 90 symboles
 * Taille minimale recommandée par l’ANSSI pour des mots de passe ergonomiques ou utilisés de façon locale.
 */
define('PASSWORD_WEAK', 78);
/**
 * Mot de passe de 14 caractères dans un alphabet de 90 symboles
 * Taille minimale recommandée par l’ANSSI pour des mots de passe ergonomiques ou utilisés de façon locale.
 */
define('PASSWORD_DEFAULT_COMPLEXITY', 80);
/**
 * Mot de passe de 16 caractères dans un alphabet de 36 symboles
 * Taille recommandée par l’ANSSI pour des mots de passe plus sûrs.
 */
define('PASSWORD_MEDIUM', 82);
/**
 * Mot de passe de 20 caractères dans un alphabet de 90 symboles
 * Force équivalente à la plus petite taille de clé de l’algorithme de chiffrement standard AES (128 bits).
 */
define('PASSWORD_STRONG', 130);

/*
 * Read configuration file and inject configuration into various
 * CakePHP classes.
 *
 * By default there is only one configuration file. It is often a good
 * idea to create multiple configuration files, and separate the configuration
 * that changes from configuration that does not. This makes deployment simpler.
 */
try {
    Configure::config('default', new PhpConfig());
    Configure::load('app_default', 'default', false);
} catch (\Exception $e) {
    exit($e->getMessage() . "\n");
}

/*
 * Load an environment local configuration file.
 * You can use a file like app_local.php to provide local overrides to your
 * shared configuration.
 */
$pathToLocalConfig = Configure::read('App.paths.path_to_local_config');
$write = false;
if (is_readable($pathToLocalConfig)) {
    $json = require $pathToLocalConfig;
} elseif (is_readable('/data/config/app_local.json')) {
    $json = '/data/config/app_local.json';
    $write = is_writable(dirname($pathToLocalConfig));
} elseif (is_readable(CONFIG . 'app_local.json')) {
    $json = CONFIG . 'app_local.json';
    $write = is_writable(dirname($pathToLocalConfig));
}
if ($write) {
    $escaped = addcslashes($json, "'");
    file_put_contents($pathToLocalConfig, "<?php return '$escaped';?>");
}
if (!empty($json) && is_file($json)) {
    $local = json_decode(file_get_contents($json), true);
    $configs = [
        Configure::read(),
        $local
    ];
    $workdir = getcwd();
    chdir(__DIR__);
    foreach ($local['Config']['files'] ?? [] as $path) {
        if (!is_file($path)) {
            throw new Exception("Config file not found");
        }
        $configs[] = require $path;
    }
    $merged = call_user_func_array(["Cake\Utility\Hash", "merge"], $configs);
    Configure::write($merged);
    chdir($workdir);
}
if (empty(Configure::read('App.paths.administrators_json'))) {
    Configure::write(
        'App.paths.administrators_json',
        rtrim(Configure::read('App.paths.data'), DS) . DS . 'administrateurs.json'
    );
}

/*
 * When debug = true the metadata cache should only last
 * for a short time.
 */
if (Configure::read('debug')) {
    Configure::write('Cache._cake_model_.duration', '+2 minutes');
    Configure::write('Cache._cake_core_.duration', '+2 minutes');
}

/*
 * Set server timezone to UTC. You can change it to another timezone of your
 * choice but using UTC makes time calculations / conversions easier.
 */
date_default_timezone_set(Configure::read('App.timezone', 'UTC'));

/*
 * Configure the mbstring extension to use the correct encoding.
 */
mb_internal_encoding(Configure::read('App.encoding'));

/*
 * Set the default locale. This controls how dates, number and currency is
 * formatted and sets the default language to use for translations.
 */
ini_set('intl.default_locale', Configure::read('App.defaultLocale'));

/*
 * Register application error and exception handlers.
 */
$error = Configure::read('Error');
(new ErrorTrap($error))->register();
(new ExceptionTrap($error))->register();

/*
 * Include the CLI bootstrap overrides.
 */
if (PHP_SAPI === 'cli') {
    require __DIR__ . '/bootstrap_cli.php';
}

/*
 * Set the full base URL.
 * This URL is used as the base of all absolute links.
 *
 * If you define fullBaseUrl in your config file you can remove this.
 */
$fullBaseUrl = Configure::read('App.fullBaseUrl');
if (!$fullBaseUrl) {
    $s = null;
    if (env('HTTPS')) {
        $s = 's';
    }

    $httpHost = env('HTTP_HOST');
    if (isset($httpHost)) {
        $fullBaseUrl = 'http' . $s . '://' . $httpHost;
        Configure::write('App.fullBaseUrl', $fullBaseUrl);
    }
    unset($httpHost, $s);
}
if ($fullBaseUrl) {
    Router::fullBaseUrl($fullBaseUrl);
}
unset($fullBaseUrl);

$cacheDisabled = Configure::consume('Cache_disabled');
Cache::setConfig(Configure::consume('Cache'));
if ($cacheDisabled === true || ($cacheDisabled === 'on_debug' && Configure::read('debug'))) {
    Cache::disable();
}
ConnectionManager::setConfig(Configure::consume('Datasources'));
TransportFactory::setConfig(Configure::consume('EmailTransport'));
Mailer::setConfig(Configure::consume('Email'));
$logPath = Configure::read('App.paths.logs');
if ($logPath[-1] !== DS) {
    $logPath = $logPath.DS;
    Configure::write('App.paths.logs', $logPath);
}
$configLog = Configure::consume('Log');
$configLog['debug']['path'] = $logPath;
$configLog['error']['path'] = $logPath;
Log::setConfig($configLog);
Security::setSalt(Configure::consume('Security.salt'));

/*
 * The default crypto extension in 3.0 is OpenSSL.
 * If you are migrating from 2.x uncomment this code to
 * use a more compatible Mcrypt based implementation
 */
//Security::engine(new \Cake\Utility\Crypto\Mcrypt());

/*
 * Setup detectors for mobile and tablet.
 */
ServerRequest::addDetector(
    'mobile',
    function () {
        $detector = new MobileDetect();

        return $detector->isMobile();
    }
);
ServerRequest::addDetector(
    'tablet',
    function () {
        $detector = new MobileDetect();

        return $detector->isTablet();
    }
);

/*
 * Enable immutable time objects in the ORM.
 *
 * You can enable default locale format parsing by adding calls
 * to `useLocaleParser()`. This enables the automatic conversion of
 * locale specific date formats. For details see
 * @phpcs:ignore
 * @link http://book.cakephp.org/3.0/en/core-libraries/internationalization-and-localization.html#parsing-localized-datetime-data
 */
TypeFactory::set('datetime', new DateTimeType);
TypeFactory::set('timestamp', new DateTimeType);
TypeFactory::set('datetimefractional', new DateTimeFractionalType);
TypeFactory::set('timestampfractional', new DateTimeFractionalType);
TypeFactory::set('date', new DateType);
$dateTypes = ['time', 'date', 'datetime', 'timestamp', 'datetimefractional', 'timestampfractional'];
foreach ($dateTypes as $name) {
    /** @var DateTimeType $timer */
    $timer = TypeFactory::build($name);
    $timer->useLocaleParser();
    $timer->setDatabaseTimezone(Configure::read('App.timezone'));
}

Factory\Utility::addNamespace('Beanstalk\Utility');
Factory\Utility::addNamespace('Libriciel\Filesystem\Utility');

putenv('XDG_CONFIG_HOME='.(getenv('XDG_CONFIG_HOME') ?: TMP.'.config'));

/**
 * Validation custom
 */
Validator::addDefaultProvider('default', new RulesProvider);

/**
 * Affichage custom du debug
 */
if (ConsoleFormatter::environmentMatches()) {
    $class = ConsoleFormatter::class;
} elseif (HtmlFormatter::environmentMatches()) {
    $class = HtmlFormatter::class;
} else {
    $class = TextFormatter::class;
}
Debugger::getInstance()->setConfig('exportFormatter', $class);

FactoryLocator::add('Table', new TableLocator);
