<?php
/**
 * @noinspection HttpUrlsUsage
 * @noinspection PhpCSValidationInspection
 */

$versionFile = file(ROOT.DS.'VERSION.txt');
$version = trim(array_pop($versionFile));

/**
* Version de versae
*/
define('VERSAE_VERSION_LONG', $version);

/**
* Version de versae
*/
define('VERSAE_VERSION_SHORT', $version);

/**
* Editeur
*/
define('VENDOR_NAME', 'LIBRICIEL SCOP');
define('VENDOR_ADDRESS', '140 Rue Aglaonice de Thessalie, 34170 Castelnau-le-Lez');
define('VENDOR_SIRET', '491 011 698 00025');
define('CNIL_INFO_URL', 'https://www.cnil.fr/fr/les-droits-pour-maitriser-vos-donnees-personnelles');
define('CNIL_COMPLAINT_URL', 'https://www.cnil.fr/fr/plaintes');

/**
 * Namespace du seda
 */
define('NAMESPACE_SEDA_02', 'fr:gouv:ae:archive:draft:standard_echange_v0.2');
define('NAMESPACE_SEDA_10', 'fr:gouv:culture:archivesdefrance:seda:v1.0');
define('NAMESPACE_SEDA_20', 'fr:gouv:culture:archivesdefrance:seda:v2.0');
define('NAMESPACE_SEDA_21', 'fr:gouv:culture:archivesdefrance:seda:v2.1');
define('NAMESPACE_SEDA_22', 'fr:gouv:culture:archivesdefrance:seda:v2.2');

/**
 * Namespace du premis
 */
define('NAMESPACE_PREMIS_V3', 'http://www.loc.gov/premis/v3');

/**
 * change la limite de 65535 lignes d'un xml si $dom->loadXML($xml, XML_PARSE_BIG_LINES);
 */
if (!defined('XML_PARSE_BIG_LINES')) {
    define('XML_PARSE_BIG_LINES', 4194304);
}
