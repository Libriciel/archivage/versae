#!/bin/bash

_mon_script_completions() {
    local cur_word
    COMPREPLY=()
    cur_word="${COMP_WORDS[COMP_CWORD]}"

    if [ "${#COMP_WORDS[@]}" -gt 2 ]; then
        if [[ $cur_word = "" ]] ; then
            _filedir
            return 0
        fi
        mapfile -t COMPREPLY < <(compgen -f -- "$cur_word")
        return 0
    fi

    # Liste des suggestions
    linenum=$(grep -n "^# BEGIN COMMAND EXTRACTOR$" "${COMP_WORDS[0]}" | head -n 1 | cut -d ':' -f 1)
    options=$(sed -n "$linenum,\$p" "${COMP_WORDS[0]}" | grep -B1 ";;" | grep -v ";;" | grep -v '\-\-' | grep -v echo | sort)
    texte_reformate=$(sed ':a;N;$!ba;s/\n/ /g' <<< "$options")
    options="${texte_reformate//^[[:space:]]*/}"

    mapfile -t COMPREPLY < <(compgen -W "${options}" -- "${cur_word}")
}

complete -F _mon_script_completions versae
