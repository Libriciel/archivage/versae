# Changelog

## \[1.1.3\] - 13/05/2025

### Évolutions

- Changement des réglages de limite de connexion par défaut à 5 essais toutes les 5 minutes #747
- Paramétrage possible du php.ini #753

### Corrections

- Mise à jour du seda_generator #724
- Sauvegarde des conditions de masquage sur les inputs #740
- Affichage des métadonnées répétables (Language en double) #744
- Édition des transferts avec champs texte répétables #741
- Versioning des formulaires avec mots-clés à champ multiple #742
- Correction erreur 500 sur le récapitulatif du transfert #738
- Configuration de l'upload dans les sections répétables #752
- Optimisation du temps de chargement lors de l'édition d'un versement #755
- Ajout d'une icône de chargement sur le curseur #755
- Les uploads sont initialisés avec required: false si des données existent #755
- Correction de la réapparition des fichiers après suppression #754


## \[1.1.2\] - 04/12/2024

### Évolutions

- Définition du paramétrage proxy dans les variables d'environnement #720
- Champs multiple possible pour certains éléments du contenu de la description #687
- Check des SAE en asynchrone #489

### Corrections

- Optimisation du chargement de l'édition d'un input avec plusieurs milliers d'options #721
- Retrait possible de certains champs spéciaux (comme min, max, formats etc...) dans les champs de formulaire #711
- Retrait de l'onglet du SEDA generator dans l'éditeur de configuration #723
- Champ avec condition de masquage sans icône dans l'édition du formulaire  #729
- Rechargement des droits suite à l'édition d'un utilisateur #569 


## \[1.1.1\] - 03/10/2024

### Nouveautés

- Ajout d'un créateur de job #673
- Ajout de la métadonnée `count` sur les sections répétables #659

### Évolutions

- Input date : date du jour par défaut #667
- Tableau de correspondance (SWITCH) : possibilité de modifier des valeurs importées depuis une liste de mots-clés #674
- Condition de présence sur les unités d'archives répétables #688

### Corrections

- Optimisation du Sedagenerator #686
- Versement bloqué en ready_to_prepare #683
- Input date : date min et date max non enregistrées #664
- Condition SI/SINON : décalage d'affichage dans le select Comparer #661
- Amélioration de l'UI/UX pour la suppression des connecteurs vers les SAE #691
- Pré-remplissage du champ "Envoi par morceaux : Taille des morceaux en octets" #699
- Test d'un formulaire avec sections répétables : perte des informations saisies en cas d'erreur SEDA #670
- Affichage erroné dans la balise dédiée à la communicabilité dans "Prévisualiser l'arborescence" en SEDA 1.0 #685
- Affichage du menu utilisateur avec OpenID #701
- Règles de gestion d'une UA en SEDA v2.1 : problème lorsque toutes les règles sont ajoutées #695
- Extracteurs dans les sections répétables #671
- Correction de la validation PHP des patterns #675
- Métadonnées manquantes/en trop dans l'arborescence #694 #698 #700
- Métadonnées additionnelles partiellement remplies = impossible de reprendre la saisie #703
- Paramétrages admin tech limités à la configuration Docker #668
- Case à cocher cochée par défaut #704
- Possibilité de désassigner un SAE d'un SA #702
- Champ caché dans les sections répétables : bug après enregistrement et réouverture #705
- Initialisation de l'antivirus Clamav #709
- Rendre impossible la suppression de son propre utilisateur #713


## \[1.1.0\] - 11/09/2023

### Nouveautés

- Import / Export des formulaires #386
- Choix du rangement des fichiers d'un versement #508
- Champ de texte multiple #417
- Affichage des liens entre éléments de formulaire, extracteurs et calculateurs
- Seda 2.2 #503
- Duplication de champs de formulaire #414
- Duplication des éléments de formulaire #484
- Prévisualisation de l'arborescence d'un formulaire #494
- Changement d'entités d'un utilisateur #466
- Variables sur l'utilisateur connecté, son entité et le service versant sélectionné #416 #457
- Limitation du nombre de tentatives de login #467
- Coche "Utiliser le nom à la place du code" pour le chargement d'options #480
- Tri possible sur les options par valeur ou par texte affiché #531
- Mapping "switch" à partir d'une liste de mots-clés #480
- Ajout libre de métadonnées dans ManagementMetadata #526
- Possibilité d'enregistrer un versement incomplet #524

### Évolutions

- Nouvelle gestion des fichiers #502
- Modifications des listes de formulaires #505
- Augmentation de la force minimum des mots de passe #501
- Amélioration de l'interface de saisie des conditions #418
- Meilleure interface de conditions de masquage de champs
- Possibilité de choisir plusieurs valeurs pour le masquage de champs #284
- Préparation des versements en asynchrone #355
- Gestion de la page de politique de confidentialité (RGPD) #127
- Conversion automatique des CSV pour l'extracteur #425
- Utilisateurs supprimables #127
- Amélioration de la saisie et édition possible des options de listes déroulantes #511
- Affichage des métadonnées "Autre" suite à un test de formulaire #416
- Ajout du service producteur pour les versements et les formulaires #520
- Affichage du type de champ dans le titre de la modale
- Ajout de ManagementMetadata et CodeListVersions #526
- Ajout de métadonnées de gestion et de contenu #530
- Unicité des noms de formulaires #353
- Format de la date par popup #496
- Alerte en cas d'envoi d'un fichier en double #561
- Suppression automatique des métadonnées incomplètes #595
- Saisie possible de plage de coordonnées pour les extracteurs CSV #390

### Corrections

- Affichage des dates au format court FR #496
- Timeout sur versements lourds #500
- Recherche dans une arborescence créant des archives d'unités de documents.
- Affichage des métadonnées liées aux documents #475
- Problème d'import d'un logo #522
- Problème à la suppression d'un fichier dans un versement sans sauvegarde de ce dernier #528
- Nouveaux filtres Twig #523
- Correction de la visualisation d'un formulaire sans droits ajouter #529
- Changement d'onglet automatique en cas d'erreur du formulaire #535
- Correction sécurité #612
- Ordre des unités d'archives dans une arborescence issue d'un fichier compressé #639
- MemoryLimit et Timelimit pour les très grands transferts #642


## \[1.0.3\] - 09/12/2022

### Nouveautés

- Ajout d'un bouton de test de notification
- Possibilité de choisir plusieurs formats de fichier pour un champ Fichier #420
- Possibilité de rendre obligatoire les cases à cocher multiples #454
- Saisie possible de dates dans les bornes min et max des champs Dates #456
- Nouveau filtre Twig pour le retrait des accents #479

### Évolutions

- Mise à jour du socle technique au niveau d'Asalae 2.1.9
- Les Systèmes d'Archivage Électroniques inactifs ne sont plus vérifiés sur le System Check #488

### Corrections

- SEDA 2.1 à corriger - balises de gestion apparaissent aux deux endroits #398
- Meilleure validation sur les champs de type fichier #429
- Utilisation des formulaires par les entités mères #447
- Bug de suppression logo dans la config administrateur technique #449
- Limitation du filesystem linux - taille des noms sur mkdir() #452
- Zones de saisie : bug d'affichage après une suppression d'une option liste déroulante #455
- Conditions sinon si dans twig : informations perdues lors de la modification #458
- Un utilisateur connecté n'est plus supprimable #471
- Unicité des noms des Systèmes d'archivage électronique au sein d'un même Service d'archives #478
- Suppression de mot-clef ou règle de gestion vide #459
- Profil d'archives et niveau de service en seda 2.1 #460
- L'entité d'appartenance n'apparait pas dans la liste juste après la création d'un utilisateur #472
- Liste déroulante multiple avec moteur de recherche #473
- Mots de passe avec caractères spéciaux pour postgres et admins #477
- Fresh install répétable en cas d'échec #453


## \[1.0.2\] - 18/05/2022

### Nouveautés

- feedback lors de la génération du transfert #356

### Corrections

- Affichage conditionnel du guide utilisateur
- On ne propose plus les formulaires des SAE désactivés #359
- Erreur de gestion de droit preedit sur versement #405
- Validations des champs cachés #409
- Suppression des fichiers de test de formulaire #411
- Formulaires : Ajout d'un message d'aide pour tous les types de champs #413
- Versements supprimables depuis l'état "erreur d'envoi" #415
- Mise à jour auto de l'attribut 'name' du champ (identifiant) quand on modifie le nom du champ #421
- Correction sur les conditions de masquage des select et radio #406 #426
- Envoi des transferts en .tar.gz pour contourner le problème sur les noms d'archives finissant par un point #427
- Problème de TransferringAgency vide dans un versement après son édition #428
- Correction sur les champs d'un versement qui pouvaient changer d'ordre d'affichage #430
- Retour en edition sur champs à fichiers multiples #431
- Extracteurs sur champs multiples #433
- Les champs d'une section de formulaire ne sont plus sélectionnables pour ses propres conditions de masquage #445
- La valeur de "Message lorsque le champ select est vide" perdue lors de la duplication #450
- Caractères spéciaux dans les noms de fichier du transfert


## \[1.0.1\] - 22/03/2022

### Nouveautés

- Limitation du nombre de tentatives de connexion sur un même login #380
- Affichage de l'interruption de service en json pour les webservices
- Suppression d'un utilisateur possible même s'il possède des versements en cours #256

### Corrections

- DescriptionLevel en seda2.1 #397
- L'ajout d'un mot-clé sur une unité d'archives supprime le contenu défini #397
- Blocage/déblocage des champs utilisés suite à une modification d'une unité d'archives #393
- Édition d'un versement en cours erreur en cas de requête trop lente #387
- Correction de la disparition des logos (ajout d'un volume persistant) #381 
- Données sur la DUA absentes dans la visualisation d'un test de formulaire #399
- Retrait des infobulles en bas de page #403
- Ajout de validation sur les noms de fichiers #396


## \[1.0.0\] - 19/01/2022

### Nouveautés

- Versements : création, envoi, suivi, historique
- Formulaires : création, publication, gestion des versions
- Administration fonctionnelle : entités, utilisateurs, rôles
- Administration technique : tenants (Services d'Archives), LDAPs, SAEs, workers, crons


