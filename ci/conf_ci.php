<?php

$config = [];

if (getenv('TEST_TOKEN') !== false) {  // Using paratest
    if (!is_dir('/tmp/testdb')) {
        mkdir('/tmp/testdb');
    }
    $config['Datasources'] = [
        'test' => [
            'database' => $db = '/tmp/testdb/' . getenv('TEST_TOKEN') . '.sqlite',
        ]
    ];
    if (!defined('FIRST_UNLINK_DB')) {
        define('FIRST_UNLINK_DB', true);
        if (file_exists($db)) {
            unlink($db);
        }
    }
}

return $config;
