#!/usr/bin/php
<?php
$s = DIRECTORY_SEPARATOR;
$dir = dirname(__DIR__).$s;
$config = $dir.'config'.$s.'app_local.json';
if (is_file($config)) {
    throw new Exception('config file already exists!');
}
$data = json_decode(file_get_contents(__DIR__.$s.'app_local_ci.json'), true);
$data['Config']['files'] = [$dir.'ci'.$s.'conf_ci.php'];

if (!file_put_contents($config, json_encode($data, JSON_PRETTY_PRINT))) {
    throw new Exception('Unable to put config file');
}
