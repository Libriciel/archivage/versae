# LANGUAGE translation of CakePHP Application
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2020-12-03 14:46+0100\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: View/Helper/TranslateHelper.php:40
#: asalae-core/src/View/Helper/TranslateHelper.php:45
msgid "Accords de versement"
msgstr ""

#: View/Helper/TranslateHelper.php:42
msgid "Registre des entrées"
msgstr ""

#: View/Helper/TranslateHelper.php:44
msgid "Mots clés d'archives"
msgstr ""

#: View/Helper/TranslateHelper.php:46
msgid "Unités d'archives"
msgstr ""

#: View/Helper/TranslateHelper.php:48
msgid "Compteurs"
msgstr ""

#: View/Helper/TranslateHelper.php:50
msgid "Demandes de communication"
msgstr ""

#: View/Helper/TranslateHelper.php:52
msgid "Communications"
msgstr ""

#: View/Helper/TranslateHelper.php:54
msgid "DestructionNotifications"
msgstr ""

#: View/Helper/TranslateHelper.php:56
msgid "DestructionRequests"
msgstr ""

#: View/Helper/TranslateHelper.php:58
msgid "Mots-clés"
msgstr ""

#: View/Helper/TranslateHelper.php:60
msgid "Entités"
msgstr ""

#: View/Helper/TranslateHelper.php:62
msgid "Utilisateurs"
msgstr ""

#: View/Helper/TranslateHelper.php:64
msgid "Rôles utilisateur"
msgstr ""

#: View/Helper/TranslateHelper.php:66
msgid "Profils d'archives"
msgstr ""

#: View/Helper/TranslateHelper.php:68
msgid "Niveaux de service"
msgstr ""

#: View/Helper/TranslateHelper.php:70
msgid "Webservice versae v1"
msgstr ""

#: View/Helper/TranslateHelper.php:72
msgid "Transferts"
msgstr ""

#: View/Helper/TranslateHelper.php:74
msgid "Jobs en cours"
msgstr ""

#: View/Helper/TranslateHelper.php:76
msgid "Circuits de validation"
msgstr ""

#: View/Helper/TranslateHelper.php:78
msgid "Validation"
msgstr ""

#: View/Helper/TranslateHelper.php:80
msgid "Webservices"
msgstr ""

#: View/Helper/TranslateHelper.php:223
#: asalae-core/src/View/Helper/TranslateHelper.php:66
msgid "Ajouter"
msgstr ""

#: View/Helper/TranslateHelper.php:225
#: asalae-core/src/View/Helper/TranslateHelper.php:68
msgid "Supprimer"
msgstr ""

#: View/Helper/TranslateHelper.php:227
#: asalae-core/src/View/Helper/TranslateHelper.php:70
msgid "Modifier"
msgstr ""

#: View/Helper/TranslateHelper.php:229
#: asalae-core/src/View/Helper/TranslateHelper.php:72
msgid "Lister"
msgstr ""

#: View/Helper/TranslateHelper.php:231
#: asalae-core/src/View/Helper/TranslateHelper.php:74
msgid "Visualiser"
msgstr ""

#: View/Helper/TranslateHelper.php:233
msgid "Télécharger"
msgstr ""

#: View/Helper/TranslateHelper.php:235
msgid "Envoyer"
msgstr ""

#: View/Helper/TranslateHelper.php:237
#: asalae-core/src/View/Helper/TranslateHelper.php:76
msgid "Création via Web service"
msgstr ""

#: View/Helper/TranslateHelper.php:239
#: asalae-core/src/View/Helper/TranslateHelper.php:78
msgid "Lecture via Web service"
msgstr ""

#: View/Helper/TranslateHelper.php:241
#: asalae-core/src/View/Helper/TranslateHelper.php:80
msgid "Mise à jour via Web service"
msgstr ""

#: View/Helper/TranslateHelper.php:243
#: asalae-core/src/View/Helper/TranslateHelper.php:82
msgid "Suppression via Web service"
msgstr ""

#: View/Helper/TranslateHelper.php:87
msgctxt "Archives"
msgid "description"
msgstr ""

#: View/Helper/TranslateHelper.php:89
msgctxt "Archives"
msgid "search"
msgstr ""

#: View/Helper/TranslateHelper.php:91
msgctxt "Archives"
msgid "checkIntegrity"
msgstr ""

#: View/Helper/TranslateHelper.php:93
msgctxt "Archives"
msgid "lifecycle"
msgstr ""

#: View/Helper/TranslateHelper.php:99
msgctxt "ArchiveUnits"
msgid "catalog"
msgstr ""

#: View/Helper/TranslateHelper.php:101
msgctxt "ArchiveUnits"
msgid "duaExpired"
msgstr ""

#: View/Helper/TranslateHelper.php:103
msgctxt "ArchiveUnits"
msgid "returnable"
msgstr ""

#: View/Helper/TranslateHelper.php:109
msgctxt "Deliveries"
msgid "acquit"
msgstr ""

#: View/Helper/TranslateHelper.php:111
msgctxt "Deliveries"
msgid "acquittal"
msgstr ""

#: View/Helper/TranslateHelper.php:113
msgctxt "Deliveries"
msgid "retrieval"
msgstr ""

#: View/Helper/TranslateHelper.php:119
msgctxt "DeliveryRequests"
msgid "indexPreparating"
msgstr ""

#: View/Helper/TranslateHelper.php:133
msgctxt "DestructionRequests"
msgid "indexPreparating"
msgstr ""

#: View/Helper/TranslateHelper.php:181
msgctxt "Transfers"
msgid "indexPreparating"
msgstr ""

#: View/Helper/TranslateHelper.php:121
msgctxt "DeliveryRequests"
msgid "indexMy"
msgstr ""

#: View/Helper/TranslateHelper.php:129
msgctxt "DestructionRequests"
msgid "indexMy"
msgstr ""

#: View/Helper/TranslateHelper.php:173
msgctxt "Transfers"
msgid "indexMy"
msgstr ""

#: View/Helper/TranslateHelper.php:123
msgctxt "DeliveryRequests"
msgid "indexAll"
msgstr ""

#: View/Helper/TranslateHelper.php:131
msgctxt "DestructionRequests"
msgid "indexAll"
msgstr ""

#: View/Helper/TranslateHelper.php:191
msgctxt "Transfers"
msgid "indexAll"
msgstr ""

#: View/Helper/TranslateHelper.php:139
msgctxt "KeywordLists"
msgid "newVersion"
msgstr ""

#: View/Helper/TranslateHelper.php:145
msgctxt "Ldaps"
msgid "importUsers"
msgstr ""

#: View/Helper/TranslateHelper.php:151
msgctxt "Restservices"
msgid "sedaMessages"
msgstr ""

#: View/Helper/TranslateHelper.php:157
msgctxt "Tasks"
msgid "admin"
msgstr ""

#: View/Helper/TranslateHelper.php:159
msgctxt "Tasks"
msgid "adminJobInfo"
msgstr ""

#: View/Helper/TranslateHelper.php:161
msgctxt "Tasks"
msgid "ajaxCancel"
msgstr ""

#: View/Helper/TranslateHelper.php:163
msgctxt "Tasks"
msgid "ajaxPause"
msgstr ""

#: View/Helper/TranslateHelper.php:165
msgctxt "Tasks"
msgid "ajaxResume"
msgstr ""

#: View/Helper/TranslateHelper.php:171
msgctxt "Transfers"
msgid "analyse"
msgstr ""

#: View/Helper/TranslateHelper.php:175
msgctxt "Transfers"
msgid "indexMyAccepted"
msgstr ""

#: View/Helper/TranslateHelper.php:177
msgctxt "Transfers"
msgid "indexMyEntity"
msgstr ""

#: View/Helper/TranslateHelper.php:179
msgctxt "Transfers"
msgid "indexMyRejected"
msgstr ""

#: View/Helper/TranslateHelper.php:183
msgctxt "Transfers"
msgid "indexSent"
msgstr ""

#: View/Helper/TranslateHelper.php:185
msgctxt "Transfers"
msgid "send"
msgstr ""

#: View/Helper/TranslateHelper.php:187
msgctxt "Transfers"
msgid "indexAccepted"
msgstr ""

#: View/Helper/TranslateHelper.php:189
msgctxt "Transfers"
msgid "indexRejected"
msgstr ""

#: View/Helper/TranslateHelper.php:197
msgctxt "Users"
msgid "editByAdmin"
msgstr ""

#: View/Helper/TranslateHelper.php:203
msgctxt "ValidationProcesses"
msgid "myTransfers"
msgstr ""

#: View/Helper/TranslateHelper.php:205
msgctxt "ValidationProcesses"
msgid "processMyTransfer"
msgstr ""

#: View/Helper/TranslateHelper.php:207
msgctxt "ValidationProcesses"
msgid "transfers"
msgstr ""

#: View/Helper/TranslateHelper.php:209
msgctxt "ValidationProcesses"
msgid "destructionRequests"
msgstr ""

#: View/Helper/TranslateHelper.php:211
msgctxt "ValidationProcesses"
msgid "deliveryRequests"
msgstr ""

#: View/Helper/TranslateHelper.php:213
msgctxt "ValidationProcesses"
msgid "processDeliveryRequest"
msgstr ""

#: View/Helper/TranslateHelper.php:215
msgctxt "ValidationProcesses"
msgid "processDestructionRequest"
msgstr ""

#: asalae-core/src/View/Helper/TranslateHelper.php:52
msgctxt "Download"
msgid "file"
msgstr ""

#: asalae-core/src/View/Helper/TranslateHelper.php:54
msgctxt "Download"
msgid "open"
msgstr ""

#: asalae-core/src/View/Helper/TranslateHelper.php:56
msgctxt "Download"
msgid "thumbnailImage"
msgstr ""

#: asalae-core/src/View/Helper/TranslateHelper.php:58
msgctxt "Download"
msgid "zip"
msgstr ""

