# LANGUAGE translation of CakePHP Application
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2017-04-05 13:53+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: debug_kit/src/DebugTimer.php:135
msgid "Core Processing (Derived from $_SERVER[\"REQUEST_TIME\"])"
msgstr ""

#: debug_kit/src/Panel/TimerPanel.php:53
msgid "Controller initialization"
msgstr ""

#: debug_kit/src/Panel/TimerPanel.php:62
msgid "Controller action start"
msgstr ""

#: debug_kit/src/Panel/TimerPanel.php:63;68
msgid "Controller action"
msgstr ""

#: debug_kit/src/Panel/TimerPanel.php:73;74;94
msgid "View Render start"
msgstr ""

#: debug_kit/src/Panel/TimerPanel.php:83;88
msgid "Render {0}"
msgstr ""

#: debug_kit/src/Panel/TimerPanel.php:95
msgid "Controller shutdown"
msgstr ""

#: debug_kit/src/Shell/BenchmarkShell.php:45
msgid "-> Testing :url"
msgstr ""

#: debug_kit/src/Shell/BenchmarkShell.php:72
msgid "Total Requests made: :requests"
msgstr ""

#: debug_kit/src/Shell/BenchmarkShell.php:73
msgid "Total Time elapsed: :duration (seconds)"
msgstr ""

#: debug_kit/src/Shell/BenchmarkShell.php:77
msgid "Requests/Second: :rps req/sec"
msgstr ""

#: debug_kit/src/Shell/BenchmarkShell.php:81
msgid "Average request time: :average-time seconds"
msgstr ""

#: debug_kit/src/Shell/BenchmarkShell.php:85
msgid "Standard deviation of average request time: :std-dev"
msgstr ""

#: debug_kit/src/Shell/BenchmarkShell.php:89
msgid "Longest/shortest request: :longest sec/:shortest sec"
msgstr ""

#: debug_kit/src/Shell/BenchmarkShell.php:147
msgid "Allows you to obtain some rough benchmarking statisticsabout a fully qualified URL."
msgstr ""

#: debug_kit/src/Shell/BenchmarkShell.php:153
msgid "The URL to request."
msgstr ""

#: debug_kit/src/Shell/BenchmarkShell.php:158
msgid "Number of iterations to perform."
msgstr ""

#: debug_kit/src/Shell/BenchmarkShell.php:162
msgid "Maximum total time for all iterations, in seconds.If a single iteration takes more than the timeout, only one request will be made"
msgstr ""

#: debug_kit/src/Shell/BenchmarkShell.php:168
msgid "Example Use: `cake benchmark --n 10 --t 100 http://localhost/testsite`. <info>Note:</info> this benchmark does not include browser render times."
msgstr ""

#: debug_kit/src/Template/Element/cache_panel.ctp:21
msgid "There were no cache operations this request."
msgstr ""

#: debug_kit/src/Template/Element/cache_panel.ctp:25
msgid "{0} Metrics"
msgstr ""

#: debug_kit/src/Template/Element/cache_panel.ctp:31
msgid "Metric"
msgstr ""

#: debug_kit/src/Template/Element/cache_panel.ctp:32
msgid "Total"
msgstr ""

#: debug_kit/src/Template/Element/environment_panel.ctp:28
msgid "Application Constants"
msgstr ""

#: debug_kit/src/Template/Element/environment_panel.ctp:49
msgid "No application environment available."
msgstr ""

#: debug_kit/src/Template/Element/environment_panel.ctp:53
msgid "CakePHP Constants"
msgstr ""

#: debug_kit/src/Template/Element/environment_panel.ctp:74
msgid "CakePHP environment unavailable."
msgstr ""

#: debug_kit/src/Template/Element/environment_panel.ctp:78
msgid "PHP Environment"
msgstr ""

#: debug_kit/src/Template/Element/environment_panel.ctp:99
msgid "PHP environment unavailable."
msgstr ""

#: debug_kit/src/Template/Element/environment_panel.ctp:104
msgid "Hidef Environment"
msgstr ""

#: debug_kit/src/Template/Element/environment_panel.ctp:125
msgid "No Hidef environment available."
msgstr ""

#: debug_kit/src/Template/Element/history_panel.ctp:23
msgid "No previous requests logged."
msgstr ""

#: debug_kit/src/Template/Element/history_panel.ctp:25
msgid "previous requests available"
msgstr ""

#: debug_kit/src/Template/Element/history_panel.ctp:29;53
msgid "Back to current request"
msgstr ""

#: debug_kit/src/Template/Element/log_panel.ctp:21
msgid "There were no log entries made this request"
msgstr ""

#: debug_kit/src/Template/Element/log_panel.ctp:24
msgid "{0} Messages"
msgstr ""

#: debug_kit/src/Template/Element/log_panel.ctp:28
msgid "Time"
msgstr ""

#: debug_kit/src/Template/Element/log_panel.ctp:29
#: debug_kit/src/Template/Element/timer_panel.ctp:32
msgid "Message"
msgstr ""

#: debug_kit/src/Template/Element/packages_panel.ctp:22
msgid "{0} not found"
msgstr ""

#: debug_kit/src/Template/Element/packages_panel.ctp:27
msgid "Direct dependencies only"
msgstr ""

#: debug_kit/src/Template/Element/packages_panel.ctp:32
msgid "Requirements ({0})"
msgstr ""

#: debug_kit/src/Template/Element/packages_panel.ctp:58
msgid "Dev Requirements ({0})"
msgstr ""

#: debug_kit/src/Template/Element/request_panel.ctp:31
msgid "Headers already sent at file {0} and line {1}."
msgstr ""

#: debug_kit/src/Template/Element/request_panel.ctp:40
msgid "No post data."
msgstr ""

#: debug_kit/src/Template/Element/request_panel.ctp:49
msgid "No querystring data."
msgstr ""

#: debug_kit/src/Template/Element/request_panel.ctp:59
msgid "No Cookie data."
msgstr ""

#: debug_kit/src/Template/Element/routes_panel.ctp:11
msgid "Route name"
msgstr ""

#: debug_kit/src/Template/Element/routes_panel.ctp:12
msgid "URI template"
msgstr ""

#: debug_kit/src/Template/Element/routes_panel.ctp:13
msgid "Defaults"
msgstr ""

#: debug_kit/src/Template/Element/sql_log_panel.ctp:58
msgid "Total Time: {0} ms &mdash; Total Queries: {1} &mdash; Total Rows: {2}"
msgstr ""

#: debug_kit/src/Template/Element/sql_log_panel.ctp:71
msgid "Query"
msgstr ""

#: debug_kit/src/Template/Element/sql_log_panel.ctp:72
msgid "Rows"
msgstr ""

#: debug_kit/src/Template/Element/sql_log_panel.ctp:73
msgid "Took (ms)"
msgstr ""

#: debug_kit/src/Template/Element/sql_log_panel.ctp:91
msgid "No active database connections"
msgstr ""

#: debug_kit/src/Template/Element/timer_panel.ctp:23
msgid "Memory"
msgstr ""

#: debug_kit/src/Template/Element/timer_panel.ctp:25
msgid "Peak Memory Use:"
msgstr ""

#: debug_kit/src/Template/Element/timer_panel.ctp:33
msgid "Memory Use"
msgstr ""

#: debug_kit/src/Template/Element/timer_panel.ctp:48
msgid "Timers"
msgstr ""

#: debug_kit/src/Template/Element/timer_panel.ctp:50
msgid "Total Request Time:"
msgstr ""

#: debug_kit/src/Template/Element/timer_panel.ctp:57
msgid "Event"
msgstr ""

#: debug_kit/src/Template/Element/timer_panel.ctp:58
msgid "Time in ms"
msgstr ""

#: debug_kit/src/Template/Element/timer_panel.ctp:59
msgid "Timeline"
msgstr ""

#: debug_kit/src/Template/Element/variables_panel.ctp:28
msgid "Sort variables by name"
msgstr ""

#: debug_kit/src/View/Helper/SimpleGraphHelper.php:76
msgid "Starting {0}ms into the request, taking {1}ms"
msgstr ""

#: debug_kit/src/View/Helper/TidyHelper.php:110
msgid "No markup errors found"
msgstr ""

