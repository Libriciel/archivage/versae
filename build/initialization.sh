#!/bin/bash

failed_with_error () {
    echo "$1" >&2
    exit 1
}
# appli env config
configure() {
    sudo -u "$APACHE_RUN_USER" /var/www/versae/bin/cake configuration set "$1" "$2"
}
# initialisation d'un nouveau volume
if [ ! -f /etc/apache2/sites-available/versae.conf ]; then
    cp /data-entrypoint/versae.conf /etc/apache2/sites-available/versae.conf
    sed -i'' -E \
        -e "s/\%\{HTTP_HOST\}/${HTTP_HOST}/" \
        /etc/apache2/sites-available/versae.conf
    sed -i'' -E \
        -e "s/\%\{APACHE_ADD_TO_VHOST\}/${APACHE_ADD_TO_VHOST}/" \
        /etc/apache2/sites-available/versae.conf
elif [ -f /etc/apache2/sites-available/versae.ssl.conf ]; then
    sed -i'' -e 's|/var/www/html|/var/www/versae|g' /etc/apache2/sites-available/versae.ssl.conf
    sed -i'' -e 's|ws://ratchet:8080/|ws://versae_ratchet:8080/|g' /etc/apache2/sites-available/versae.ssl.conf
    sed -i'' -e 's|ErrorLog /var/log/apache2/versae-error.log|ErrorLog /dev/stderr|' \
        -e 's|CustomLog /var/log/apache2/versae-access.log combined|TransferLog /dev/stdout|' \
        /etc/apache2/sites-available/versae.ssl.conf
    if ! grep -q "ErrorLog" /etc/apache2/sites-available/versae.ssl.conf; then
        sed -i'' -e '/<\/VirtualHost>/i\
\
    ErrorLog /dev/stderr\
    TransferLog /dev/stdout' /etc/apache2/sites-available/versae.ssl.conf
    fi
    if ! grep -q 'SetEnv HOME' /etc/apache2/sites-available/versae.ssl.conf; then
        sed -i'' -e '/<\/VirtualHost>/i\
\
    SetEnv HOME "/home/versae"' /etc/apache2/sites-available/versae.ssl.conf
    fi
    cat /etc/apache2/sites-available/versae.ssl.conf >> /etc/apache2/sites-available/versae.conf \
        || failed_with_error "failed to merge versae.conf files"
    rm /etc/apache2/sites-available/versae.ssl.conf
fi
if [ ! -f /etc/apache2/ssl/server.crt ]; then
    cp /data-entrypoint/server.crt /etc/apache2/ssl/server.crt
fi
if [ ! -f /etc/apache2/ssl/server.key ]; then
    cp /data-entrypoint/server.key /etc/apache2/ssl/server.key
fi

# Ajout de la conf php
PHP_INI="/etc/php/8.1/apache2/php.ini"
COMMENT="; Ajouté par le .env versae"
if grep -qF "$COMMENT" "$PHP_INI"; then
    sed -i "/$COMMENT/,\$d" "$PHP_INI"
fi
if [ -n "$APACHE_ADD_TO_PHP_INI" ]; then
    echo "Ajout de la conf php.ini"
    echo "
$COMMENT
$APACHE_ADD_TO_PHP_INI" >> "$PHP_INI"
fi

# Copie des fichiers de l'image vers le volume
cd /var/www/versae/ || exit 1
xargs -I % sh -c 'rm -rf "/var/www/versae-volume/%" && cp -aR "%" "/var/www/versae-volume/" 2>/dev/null' < files_to_override_on_init.txt
cd /var/www/versae-volume/ || exit 1
mkdir -p /var/www/versae-volume/tmp /var/www/versae-volume/logs
chown "$APACHE_RUN_USER" /var/www/versae-volume/tmp /var/www/versae-volume/logs

INI_FILE=/tmp/install.ini
CONFIG_DIR=/data/config
CONFIG_FILE=$CONFIG_DIR/app_local.json
RATCHET_HOST=${RATCHET_HOST:-versae_ratchet}
POSTGRES_DBHOST=${POSTGRES_DBHOST:-versae_db}
mkdir -p $CONFIG_DIR
mkdir -p /etc/apache2/ssl

# chown des volumes
chown "$APACHE_RUN_USER" -Rf \
    /etc/apache2/sites-available \
    /etc/apache2/ssl \
    /var/www/versae/tmp \
    /var/www/versae/logs \
    /var/www/versae/webroot/org-entity-data

chown "$APACHE_RUN_USER" /data /data/config

URL=${HTTP_HOST:-localhost}${HTTP_PORT:-}${HTTP_BASEURL:-}

if [ ! -f "$CONFIG_FILE" ]
then

    echo "
[config]
config_file = $CONFIG_FILE
data_dir = /data
admin_file = $CONFIG_DIR/administrators.json
debug = $DEBUG

[app]
locale = fr_FR
timezone = Europe/Paris
url = https://$URL

[ratchet]
url_ws = wss://$URL/wss

[database]
host = $POSTGRES_DBHOST
port = $POSTGRES_DBPORT
username = $POSTGRES_USER
password = \"$POSTGRES_PASSWORD\"
database = $POSTGRES_DB

[security]
type = random
value =

[email]
from = $MAIL_FROM
method = smtp
host = $POSTFIX_RELAYHOST
port = $POSTFIX_PORT
username = $POSTFIX_RELAYHOST_USERNAME
password = \"$POSTFIX_RELAYHOST_PASSWORD\"

[admins]
user1[username] = $ADMIN_USER
user1[email] = $ADMIN_MAIL
user1[password] = \"$ADMIN_PASSWORD\"

[exploitation_service]
name = \"Service d'Exploitation\"
identifier = se

[proxy]
host = $PROXY_HOST
port = $PROXY_PORT
username = $PROXY_USER
password = \"$PROXY_PASSWORD\"

" > $INI_FILE

    if [ -n "$SA_NAME" ] && [ -n "$SA_IDENTIFIER" ]; then
        echo "
[archival_agencies]
sa[name] = \"$SA_NAME\"
sa[short_name] =
sa[identifier] = \"$SA_IDENTIFIER\"
sa[archiving_systems] =
sa[max_disk_usage] = 10
sa[is_main_archival_agency] = 1

" >> $INI_FILE
    fi

    echo "Pas de configuration existante, initialisation"
    echo " "
    (sudo -u "$APACHE_RUN_USER" /var/www/versae/bin/cake install config "$INI_FILE" \
        && rm "$INI_FILE") \
        || (rm -f "$CONFIG_FILE" && failed_with_error "install failed")
    echo ""
else
    echo ""
    echo "Configuration existante "
    echo ""

    # force override hosts
    configure Datasources.default.host "${POSTGRES_DBHOST:-versae_db}"
    configure SedaGenerator.url "http://${SEDAGEN_HOST:-versae_sedagen}"
fi

/usr/bin/php /var/www/versae/bin/cake.php migrations migrate || exit 1
/usr/bin/php /var/www/versae/bin/cake.php migrations seed || exit 1
rm /var/www/versae/tmp/cache -r
/usr/bin/php /var/www/versae/bin/cake.php update || exit 1
/usr/bin/php /var/www/versae/bin/cake.php roles_perms import --keep /var/www/versae/resources/export_roles.json || exit 1

sudo -u "$APACHE_RUN_USER" timeout 60 sf -update

# config pour bin/cake console
APACHE_HOME_DIR=$(bash -c "cd ~$(printf %q "$APACHE_RUN_USER") && pwd")
sudo -u "$APACHE_RUN_USER" mkdir -p "$APACHE_HOME_DIR/.config/psysh"
sudo -u "$APACHE_RUN_USER" cp config/psych.php "$APACHE_HOME_DIR/.config/psysh/config.php"

if [ -f "/var/www/versae-volume/config/path_to_local.php" ]
then
    if [ -f "/var/www/versae/config/path_to_local.php" ]
    then
      echo "<?php return '$CONFIG_FILE';?>" > /var/www/versae/config/path_to_local.php
    fi

    cp /var/www/versae-volume/config/path_to_local.php /var/www/versae/config/path_to_local.php
else
    echo "<?php return '$CONFIG_FILE';?>"
fi
chown "$APACHE_RUN_USER":"$APACHE_RUN_GROUP" /var/www/versae/config/path_to_local.php

# permissions
echo "setting permissions for $APACHE_RUN_USER:$APACHE_RUN_GROUP"
chown "$APACHE_RUN_USER":"$APACHE_RUN_GROUP" /data -Rf
PATH_TO_LOCAL=$(php -r 'echo include "/var/www/versae/config/path_to_local.php";')
chmod 600 "$PATH_TO_LOCAL"
chmod 600 "$CONFIG_DIR/administrators.json"

# chown des volumes
chown "$APACHE_RUN_USER" -Rf \
    /etc/apache2/sites-available \
    /etc/apache2/ssl \
    /var/www/versae/tmp \
    /var/www/versae/logs \
    /var/www/versae/webroot/org-entity-data

configure Beanstalk.client_host "${BEANSTALK_CLIENT_HOST:-versae_beanstalk}"
if [ -n "$BEANSTALK_CLIENT_PORT" ]; then
    configure Beanstalk.client_port "$BEANSTALK_CLIENT_PORT"
fi
configure Ratchet.ping "${RATCHET_PING:-http://versae_ratchet:8080}"
configure Ratchet.connect "wss://${HTTP_HOST}/wss"
if [ -n "$POSTGRES_DBPORT" ]; then
    configure Datasources.default.port "$POSTGRES_DBPORT"
fi
configure Antivirus.host "${CLAMAV_HOST:-versae_clamav}"
if [ -n "$CLAMAV_PORT" ]; then
    configure Antivirus.port "$CLAMAV_PORT"
fi

configure Proxy.host "${PROXY_HOST:-null}"
configure Proxy.port "${PROXY_PORT:-null}"
configure Proxy.username "${PROXY_USER:-null}"
configure Proxy.password "${PROXY_PASSWORD:-null}"

echo "initialization completed"
