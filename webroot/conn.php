<?php /** @noinspection PhpCSValidationInspection */
/**
 * Ce fichier permet de savoir si l'utilisateur est connecté et si sa session
 * n'a pas expiré, sans la prolonger
 */

use Cake\Database\Connection;
use Cake\Datasource\ConnectionManager;
use Cake\Core\Configure;
use Versae\Http\ConnSession;

ob_start();
require dirname(__DIR__) . '/vendor/autoload.php';
require realpath(dirname(__DIR__).'/config/bootstrap.php');

$defaultSession = [
    'cookie' => 'PHPSESSID',
    'ini' => [
        'session.use_trans_sid' => (int) 0,
        'session.cookie_secure' => (int) 1,
        'session.name' => 'PHPSESSID',
        'session.cookie_httponly' => (int) 1
    ],
    'defaults' => 'php',
    'cookiePath' => '/'
];
try {
    /** @var Connection $conn */
    $conn = ConnectionManager::get('default');
    $conn->query('select 1');
    $config = Configure::read('Session') + $defaultSession;
} catch (\Exception $e) {
    $config = $defaultSession;
}
$session = new ConnSession($config);

$userData = !empty($_GET['admin']) ? $session->read('Admin.tech') : $session->read('Auth');

if ($session->timedOut()) {
    @$session->destroy();
}
ob_end_clean();
// evite le parse error suite à l'affichage d'un "session_write_close(): Failed to write session"
error_reporting(0);
ini_set("display_errors", 0);

header('Content-Type: application/json');
echo json_encode(
    [
        'connected' => !$session->timedOut() && $userData,
        'remaining' => $session->timeLeft()
    ]
);
