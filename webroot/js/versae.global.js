/**
 * Supprime les icones sur firefox des selects qui ne sont pas en select2
 */
if (navigator.userAgent.indexOf("Firefox") !== -1) {
    let filterIcons = ["\uf1b3"];
    $(document).on(
        'ajax.complete.browserfix browserfix.browserfix',
        function () {
            let selects = $('select:not(.select2-hidden-accessible)');
            setTimeout(
                () => selects.find('option').each(
                    function () {
                        $(this).contents()
                        .filter(
                            function () {
                                return this.nodeType === Node.TEXT_NODE;
                            }
                        )
                        .each(
                            function () {
                                for (let i = 0; i < filterIcons.length; i++) {
                                    this.nodeValue = this.nodeValue.replace(filterIcons[i], '');
                                }
                            }
                        );
                    }
                ),
                10
            );
            setTimeout(
                () => selects.find('optgroup').each(
                    function () {
                        for (let i = 0; i < filterIcons.length; i++) {
                            this.label = this.label.replace(filterIcons[i], '');
                        }
                    }
                ),
                10
            );
        }
    );
}
