<?xml version="1.0" encoding="UTF-8" ?>
<!--
	SEDA v0.2 XSLT display HTML
	Modifié pour les besoins d'asalae
-->

<xsl:stylesheet version="1.0"
				xmlns:seda="fr:gouv:ae:archive:draft:standard_echange_v0.2"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:xsd="http://www.w3.org/2001/XMLSchema"
				xmlns:ccts="urn:un:unece:uncefact:documentation:standard:CoreComponentsTechnicalSpecification:2"
				exclude-result-prefixes="seda xsl xsd ccts">

	<xsl:output method="html"
				indent="yes"
				media-type="text/html"
				encoding="UTF-8"
				doctype-public="-//W3C//DTD HTML 4.01//EN"
				doctype-system="http://www.w3.org/TR/html4/strict.dtd" />

	<!-- Organization -->
	<xsl:template match="Organization">
		<fieldset class="{local-name()}">
			<legend>
				<xsl:call-template name="traduction">
					<xsl:with-param name="term" select="local-name()"/>
				</xsl:call-template>
			</legend>
			<div style="display: block;">
				<xsl:apply-templates select="BusinessType"/>
				<xsl:apply-templates select="Description"/>
				<xsl:apply-templates select="District"/>
				<xsl:apply-templates select="Identification"/>
				<xsl:apply-templates select="LegalClassification"/>
				<xsl:apply-templates select="Name"/>
				<xsl:apply-templates select="TaxRegistration"/>
				<xsl:apply-templates select="Contact"/>
				<xsl:apply-templates select="Address"/>
				<xsl:apply-templates select="Communication"/>
			</div>
		</fieldset>
	</xsl:template>

	<!-- Contact -->
	<xsl:template match="Contact">
		<fieldset class="{local-name()}">
			<legend>
				<xsl:call-template name="traduction">
					<xsl:with-param name="term" select="local-name()"/>
				</xsl:call-template>
				<xsl:apply-templates select="@*"/>
			</legend>
			<div class="{local-name()}">
				<div class="code-value">
					<xsl:apply-templates select="PersonName"/>
					<xsl:apply-templates select="Identification"/>
					<xsl:apply-templates select="Responsibility"/>
					<xsl:apply-templates select="DepartmentName"/>
					<xsl:apply-templates select="Address"/>
					<xsl:apply-templates select="Communication"/>
				</div>
			</div>
		</fieldset>
	</xsl:template>

	<!-- Address -->
	<xsl:template match="Address">
		<fieldset class="{local-name()}">
			<legend>
				<xsl:call-template name="traduction">
					<xsl:with-param name="term" select="local-name()"/>
				</xsl:call-template>
				<xsl:apply-templates select="@*"/>
			</legend>
			<div class="{local-name()}">
				<div class="code-value">
					<xsl:apply-templates select="BlockName"/>
					<xsl:apply-templates select="BuildingName"/>
					<xsl:apply-templates select="BuildingNumber"/>
					<xsl:apply-templates select="StreetName"/>
					<xsl:apply-templates select="Postcode"/>
					<xsl:apply-templates select="CityName"/>
					<xsl:apply-templates select="CitySub-DivisionName"/>
					<xsl:apply-templates select="Country"/>
					<xsl:apply-templates select="FloorIdentification"/>
					<xsl:apply-templates select="PostOfficeBox"/>
					<xsl:apply-templates select="RoomIdentification"/>
				</div>
			</div>
		</fieldset>
	</xsl:template>

	<!-- Communication -->
	<xsl:template match="Communication">
		<fieldset class="{local-name()}">
			<legend>
				<xsl:call-template name="traduction">
					<xsl:with-param name="term" select="local-name()"/>
				</xsl:call-template>
				<xsl:apply-templates select="@*"/>
			</legend>
			<div class="{local-name()}">
				<div class="code-value">
					<xsl:apply-templates select="Channel"/>
					<xsl:apply-templates select="CompleteNumber"/>
					<xsl:apply-templates select="URI"/>
				</div>
			</div>
		</fieldset>
	</xsl:template>

	<!-- Divers -->
	<xsl:template match="
	Description[(. != '') or (@* !='')]
	|BusinessType[(. != '') or (@* !='')]
	|District[(. != '') or (@* !='')]
	|Identification[(. != '') or (@* !='')]
	|LegalClassification[(. != '') or (@* !='')]
	|Name[(. != '') or (@* !='')]
	|TaxRegistration[(. != '') or (@* !='')]
	|PersonName[(. != '') or (@* !='')]
	|Responsibility[(. != '') or (@* !='')]
	|BlockName[(. != '') or (@* !='')]
	|BuildingName[(. != '') or (@* !='')]
	|BuildingNumber[(. != '') or (@* !='')]
	|CityName[(. != '') or (@* !='')]
	|CitySub-DivisionName[(. != '') or (@* !='')]
	|Country[(. != '') or (@* !='')]
	|FloorIdentification[(. != '') or (@* !='')]
	|Postcode[(. != '') or (@* !='')]
	|PostOfficeBox[(. != '') or (@* !='')]
	|RoomIdentification[(. != '') or (@* !='')]
	|StreetName[(. != '') or (@* !='')]
	|Channel[(. != '') or (@* !='')]
	|CompleteNumber[(. != '') or (@* !='')]
	|URI[(. != '') or (@* !='')]">
		<div class="{local-name()}">
			<xsl:call-template name="traduction"><xsl:with-param name="term" select="local-name()"/></xsl:call-template> :
			<xsl:apply-templates/>
			<xsl:apply-templates select="@*"/>
		</div>
	</xsl:template>

	<!-- cas particuliers -->
	<xsl:template match="Contact/DepartmentName[(. != '') or (@* !='')]">
		<div class="{local-name()}">
			<xsl:call-template name="traduction"><xsl:with-param name="term">Contact/DepartmentName</xsl:with-param></xsl:call-template> :
			<xsl:apply-templates/>
			<xsl:apply-templates select="@*"/>
		</div>
	</xsl:template>

	<xsl:template match="Country[@listVersionID='second edition 2006']/text()">
		<xsl:variable name="value" select="."/>
		<xsl:variable name="table">codes/ISO_ISOTwoletterCountryCode_SecondEdition2006VI-3.xsd</xsl:variable>
		<xsl:value-of select="document($table)//xsd:enumeration[@value=$value]//xsd:annotation/xsd:documentation/ccts:Name"/>
	</xsl:template>


	<!-- Regle par défaut pour mettre en rouge tout ce qui a été oublié -->
	<xsl:template match="*">
		<span style="background-color:red">
			<xsl:apply-templates/>
		</span>
	</xsl:template>


	<!-- Formatage des Attributs -->
	<xsl:template match="@*[1]">
		<xsl:if test=". != ''">
			<a href="javascript:void(0)" class="info" title="">
				<span class="attributs">
					<xsl:for-each select="../@*[.!='']">
						<xsl:if test="local-name()!='filename'">
							<p><strong><xsl:value-of select="local-name()"/></strong>="<xsl:value-of select="translate(normalize-space(.), ' ', '&#160;')"/>"</p>
						</xsl:if>
					</xsl:for-each>
				</span>
				<i class="icon-tag"></i>
			</a>
		</xsl:if>
	</xsl:template>
	<xsl:template match="@*"/>



	<!-- Formatage des noms -->
	<xsl:template name="name">
		<xsl:param name="nom"></xsl:param>
		<xsl:choose>
			<xsl:when test="($nom != '')"><xsl:value-of select="$nom"/></xsl:when>
			<xsl:otherwise>NO_NAME</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Traduction de termes en français -->
	<xsl:template name="traduction">
		<xsl:param name="term"></xsl:param>
		<xsl:choose>
			<xsl:when test="($term = 'Communication')">Moyen de communication</xsl:when>
			<xsl:when test="($term = 'Organization')">Organisation</xsl:when>
			<xsl:when test="($term = 'Channel')">Canal</xsl:when>
			<xsl:when test="($term = 'CompleteNumber')">Numéro</xsl:when>
			<xsl:when test="($term = 'URI')">Identifiant ressource</xsl:when>
			<xsl:when test="($term = 'Name')">Nom</xsl:when>
			<xsl:when test="($term = 'Address')">Adresse</xsl:when>
			<xsl:when test="($term = 'Contact')">Contact</xsl:when>
			<xsl:when test="($term = 'Country')">Pays</xsl:when>
			<xsl:when test="($term = 'Identification')">Identifiant</xsl:when>
			<xsl:when test="($term = 'Description')">Description</xsl:when>
			<xsl:when test="($term = 'BlockName')">Quartier</xsl:when>
			<xsl:when test="($term = 'BuildingName')">Bâtiment</xsl:when>
			<xsl:when test="($term = 'BuildingNumber')">Numéro</xsl:when>
			<xsl:when test="($term = 'CityName')">Localité</xsl:when>
			<xsl:when test="($term = 'CitySub-DivisionName')">Arrondissement/quartier</xsl:when>
			<xsl:when test="($term = 'BusinessType')">Code de l'activité</xsl:when>
			<xsl:when test="($term = 'Contact/DepartmentName')">Service</xsl:when>
			<xsl:when test="($term = 'District')">Ressort territorial</xsl:when>
			<xsl:when test="($term = 'FloorIdentification')">Etage</xsl:when>
			<xsl:when test="($term = 'LegalClassification')">Code de la catégorie juridique</xsl:when>
			<xsl:when test="($term = 'PersonName')">Nom</xsl:when>
			<xsl:when test="($term = 'PostOfficeBox')">Boite postale</xsl:when>
			<xsl:when test="($term = 'Postcode')">Code postal</xsl:when>
			<xsl:when test="($term = 'Responsibility')">Attributions</xsl:when>
			<xsl:when test="($term = 'RoomIdentification')">Pièce</xsl:when>
			<xsl:when test="($term = 'StreetName')">Voie</xsl:when>

			<xsl:otherwise><xsl:value-of select="$term"/></xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
