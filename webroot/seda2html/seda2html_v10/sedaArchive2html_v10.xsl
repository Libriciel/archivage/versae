<?xml version="1.0" encoding="UTF-8" ?>
<!--
        SEDA v1.0 XSLT display HTML
        Modifié pour les besoins d'asalae
        Archive
-->

<xsl:stylesheet version="1.0"
                xmlns:seda="fr:gouv:culture:archivesdefrance:seda:v1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:ccts="urn:un:unece:uncefact:documentation:standard:CoreComponentsTechnicalSpecification:2"
                exclude-result-prefixes="seda xsl xsd ccts">

    <xsl:output
        method="html"
        indent="yes"
        media-type="text/html"
        encoding="UTF-8"
        doctype-public="-//W3C//DTD HTML 4.01//EN"
        doctype-system="http://www.w3.org/TR/html4/strict.dtd" />

    <xsl:param name="afficheDocument" select="defaultstring"/>
    <xsl:param name="ratchetUrl" select="defaultstring"/>

    <xsl:template match="/">
        <html>
            <head>
                <link href="../../favicon.ico" type="image/x-icon" rel="icon" />
                <link href="../../favicon.ico" type="image/x-icon" rel="shortcut icon" />
                <link rel="stylesheet" href="../../css/font-awesome.min.css"/>
                <link rel="stylesheet" href="../../css/jstree/style.min.css"/>
                <link rel="stylesheet" href="../../css/descriptionArchive.css"/>
                <link rel="stylesheet" href="../../css/jquery-ui.min.css"/>
                <script type="text/javascript" src="../../js/autobahn.min.js"></script>
                <script type="text/javascript" src="../../js/uuid.js"></script>
                <script type="text/javascript" src="../../js/jquery-1.11.1.min.js"></script>
                <script type="text/javascript" src="../../js/jquery-ui.min.js"></script>
                <script type="text/javascript" src="../../js/jstree/jstree.min.js"></script>
                <script type="text/javascript" src="../../js/asalae.descriptionArchive.js"></script>
                <script type="text/javascript" src="../../js/asalae.arborescence.js"></script>
                <script type="text/javascript" src="../../js/asalae.seda2html-common.js"></script>
                <script type="text/javascript" src="../../js/asalae.global-top.js"></script>
                <title>Asalae : Entrées : Description</title>
            </head>
            <body>
                <div id="navigation">
                    <div id="headnavigation">
                        <h4>NAVIGATION</h4>
                        <p id="minimize">
                            <i class="icon-chevron-up" title="masquer/afficher"></i>
                        </p>
                    </div>
                    <div id="arbonavigation">
                        <xsl:call-template name="menuarbo"/>
                    </div>
                </div>
                <div id="main">
                    <xsl:call-template name="first_level"/>
                </div>
                <script id="download-script" data-ratchet="{$ratchetUrl}">
                    var ratchetUrl = $('#download-script').attr('data-ratchet');
                    AsalaeDownloads.getInstance(ratchetUrl).handleDownloads();
                </script>
                <script>
                    <![CDATA[
                    $(document).on('keydown', function(event) {
                        if (event.ctrlKey && event.key === 'Delete') {
                            var url = '' + window.location;
                            url = url.replace(/\?.*/, '');
                            window.location = url + '?renew=true';
                        }
                    });
                    ]]>
                </script>
            </body>
        </html>
    </xsl:template>

    <!-- Arbo -->
    <xsl:template name="menuarbo">
        <xsl:apply-templates select="seda:Archive" mode="arbo" />
    </xsl:template>

    <xsl:template match="seda:Archive" mode="arbo">
        <ul>
            <li>
                <a href="#{generate-id()}">
                    <xsl:value-of select="seda:Name" />
                </a>
                <xsl:apply-templates select="seda:ArchiveObject" mode="arbo" />
            </li>
        </ul>
    </xsl:template>

    <xsl:template match="seda:ArchiveObject" mode="arbo">
        <ul>
            <li>
                <a href="#{generate-id()}">
                    <xsl:value-of select="seda:Name" />
                </a>
                <xsl:apply-templates select="seda:ArchiveObject" mode="arbo" />
            </li>
        </ul>
    </xsl:template>

    <xsl:template name="first_level">
        <h1 class="message">
            <xsl:if test="/seda:Archive">
            Message d'Archive - SEDA v1.0
            </xsl:if>
            <xsl:if test="/seda:ArchiveObject">
            Message d'Unité documentaire - SEDA v1.0
            </xsl:if>
            <xsl:if test="/seda:Document">
            Message de Document - SEDA v1.0
            </xsl:if>
            <xsl:apply-templates select="@*"/>
            <small><a href="javascript:invertDataName()" class="pull-right">Termes SEDA / Termes Français</a></small>
        </h1>
        <fieldset>
            <legend>Contenu</legend>
            <div id="content" style="display: block;">
                <div id="commands">
                    <span>
                        <a href="javascript:expandAll(true)">tout déplier</a>
                        |
                        <a href="javascript:expandAll(false)">tout replier</a>
                    </span>
                </div>
                <xsl:apply-templates/>
            </div>
        </fieldset>
    </xsl:template>

    <!-- Archive -->
    <xsl:template match="seda:Archive">
        <h2 id="{generate-id()}">
            <xsl:call-template name="name">
                <xsl:with-param name="nom" select="seda:ArchivalAgencyArchiveIdentifier"/>
            </xsl:call-template>
            :
            <xsl:call-template name="name">
                <xsl:with-param name="nom" select="seda:Name"/>
            </xsl:call-template>
            <xsl:apply-templates select="@*"/>
        </h2>
        <xsl:call-template name="archive"/>
    </xsl:template>

    <xsl:template name="archive">
        <div>
            <xsl:apply-templates select="seda:AccessRestrictionRule"/>
            <xsl:apply-templates select="seda:AppraisalRule"/>
            <xsl:apply-templates select="seda:ArchivalAgreement"/>
            <xsl:apply-templates select="seda:ArchivalProfile"/>
            <xsl:apply-templates select="seda:ContentDescription"/>
            <xsl:apply-templates select="seda:DescriptionLanguage"/>
            <xsl:apply-templates select="seda:OriginatingAgencyArchiveIdentifier"/>
            <xsl:apply-templates select="seda:ServiceLevel"/>
            <xsl:apply-templates select="seda:TransferringAgencyArchiveIdentifier"/>
            <xsl:if test="seda:Document">
                <fieldset class="Documents">
                    <legend>
                        <span class="text">
                            <xsl:text>Documents de l'archive</xsl:text>
                        </span>
                    </legend>
                    <ul>
                    <xsl:for-each select="seda:Document">
                        <li>
                            <xsl:apply-templates select="."/>
                        </li>
                    </xsl:for-each>
                </ul>
                </fieldset>
            </xsl:if>
            <xsl:if test="seda:ArchiveObject">
                <fieldset class="Objects">
                    <legend>
                        <xsl:text>Objets d'archive</xsl:text>
                    </legend>
                    <ul>
                        <xsl:for-each select="seda:ArchiveObject">
                            <li>
                                <xsl:apply-templates select="."/>
                            </li>
                        </xsl:for-each>
                    </ul>
                </fieldset>
            </xsl:if>
        </div>
    </xsl:template>

    <!-- ArchiveObject -->
    <xsl:template match="seda:ArchiveObject">
        <xsl:variable name="depth" select="count(ancestor::*)"/>
        <xsl:if test="$depth > 0">
            <div class="navigationRetour">
                <a class="backtotop" href="#" title="Revenir en haut">
                    <i class="icon-circle-arrow-up"></i>
                </a>
            </div>
            <fieldset class="ArchiveObject folder" id="{generate-id()}">
                <legend class="ArchiveObject-name">
                    <xsl:call-template name="name">
                        <xsl:with-param name="nom" select="seda:Name"/>
                    </xsl:call-template>
                    <xsl:apply-templates select="@*"/>
                </legend>
                <div>
                    <xsl:apply-templates select="seda:ArchivalAgencyObjectIdentifier"/>
                    <xsl:apply-templates select="seda:TransferringAgencyObjectIdentifier"/>
                    <xsl:apply-templates select="seda:OriginatingAgencyObjectIdentifier"/>
                    <xsl:apply-templates select="seda:AppraisalRule"/>
                    <xsl:apply-templates select="seda:AccessRestrictionRule"/>

                    <xsl:apply-templates select="seda:ContentDescription"/>
                    <xsl:if test="seda:ArchiveObject">
                        <ul>
                            <xsl:for-each select="seda:ArchiveObject">
                                <li>
                                    <xsl:apply-templates select="."/>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </xsl:if>
                    <xsl:if test="seda:Document">
                        <fieldset class="Documents">
                            <legend>
                                <xsl:text>Documents de l'objet d'archive</xsl:text>
                            </legend>
                            <ul>
                                <xsl:for-each select="seda:Document">
                                    <li>
                                        <xsl:apply-templates select="."/>
                                    </li>
                                </xsl:for-each>
                            </ul>
                        </fieldset>
                    </xsl:if>
                </div>
            </fieldset>
        </xsl:if>
        <xsl:if test="$depth = 0">
            <h2 id="{generate-id()}">
                <xsl:call-template name="name">
                    <xsl:with-param name="nom" select="seda:ArchivalAgencyObjectIdentifier"/>
                </xsl:call-template>
                :
                <xsl:call-template name="name">
                    <xsl:with-param name="nom" select="seda:Name"/>
                </xsl:call-template>
                <xsl:apply-templates select="@*"/>
            </h2>
            <xsl:call-template name="archive"/>
        </xsl:if>
    </xsl:template>

    <!-- Document -->
    <xsl:template match="seda:Document">
        <xsl:variable name="depth" select="count(ancestor::*)"/>
        <xsl:if test="$depth > 0">
            <fieldset class="{local-name()} file">
                <legend class="Document-name">
    <!--                <xsl:call-template name="toString"/>-->
                    <xsl:call-template name="name">
                        <xsl:with-param name="nom" select="seda:Attachment/@filename"/>
                    </xsl:call-template>
                    <xsl:apply-templates select="@*"/>
                </legend>
                <div>
                    <xsl:apply-templates select="seda:Attachment"/>
                    <xsl:apply-templates select="seda:ArchivalAgencyDocumentIdentifier"/>
                    <xsl:apply-templates select="seda:OriginatingAgencyDocumentIdentifier"/>
                    <xsl:apply-templates select="seda:TransferringAgencyDocumentIdentifier"/>
                    <xsl:apply-templates select="seda:Control"/>
                    <xsl:apply-templates select="seda:Copy"/>
                    <xsl:apply-templates select="seda:Creation"/>
                    <xsl:apply-templates select="seda:Description"/>
                    <xsl:apply-templates select="seda:Issue"/>
                    <xsl:apply-templates select="seda:Language"/>
                    <xsl:apply-templates select="seda:Purpose"/>
                    <xsl:apply-templates select="seda:Receipt"/>
                    <xsl:apply-templates select="seda:Response"/>
                    <xsl:apply-templates select="seda:Size"/>
                    <xsl:apply-templates select="seda:Status"/>
                    <xsl:apply-templates select="seda:Submission"/>
                    <xsl:apply-templates select="seda:Type"/>
                    <xsl:apply-templates select="seda:Integrity"/>
                    <xsl:apply-templates select="seda:RelatedData"/>
                    <xsl:apply-templates select="seda:OtherMetadata"/>
                </div>
            </fieldset>
        </xsl:if>
        <xsl:if test="$depth = 0">
            <h2 id="{generate-id()}">
                <xsl:call-template name="name">
                    <xsl:with-param name="nom" select="seda:ArchivalAgencyDocumentIdentifier"/>
                </xsl:call-template>
                :
                <xsl:call-template name="name">
                    <xsl:with-param name="nom" select="seda:Attachment/@filename"/>
                </xsl:call-template>
                <xsl:apply-templates select="@*"/>
            </h2>
            <xsl:apply-templates/>
        </xsl:if>
    </xsl:template>

    <!-- Attachment -->
    <!-- on n'affiche pas le contenu qui peut être en base64Binary -->
    <xsl:template match="seda:Attachment">
        <div class="{local-name()}">
            <label id="{generate-id()}" data-name="{local-name()}">
                <span class="text">
                    <xsl:call-template name="traduction">
                        <xsl:with-param name="term" select="local-name()"/>
                    </xsl:call-template>
                </span>
                :
            </label>
        </div>
        <xsl:apply-templates select="@*"/>
    </xsl:template>

    <!-- Formatage des href pour le pieces attachées -->
    <xsl:template match="seda:Attachment/@filename">
        <xsl:choose>
            <xsl:when test="$files_exists">
                <a>
                    <xsl:attribute name="download">
                        <xsl:value-of select="../@basename"/>
                    </xsl:attribute>
                    <xsl:attribute name="href">
                        <xsl:value-of select="$afficheDocument"/>
                        <xsl:value-of select="../@b64filename"/>
                    </xsl:attribute>
                    <xsl:value-of select="."/>
                </a>
            </xsl:when>
            <xsl:otherwise>
                <span class="text">
                    <xsl:value-of select="../@basename"/>
                </span>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Content description -->
    <xsl:template match="seda:ContentDescription">
        <fieldset class="ContentDescription">
            <legend class="ContentDescription-name" data-name="{local-name()}">
                <span class="text">
                    <xsl:call-template name="traduction">
                        <xsl:with-param name="term" select="local-name()"/>
                    </xsl:call-template>
                </span>
                <xsl:apply-templates select="@*"/>
            </legend>
            <div class="ContentDescriptionContent">
                <!-- <label><xsl:call-template name="traduction"><xsl:with-param name="term" select="local-name()"/></xsl:call-template>: </label> -->
                <xsl:apply-templates select="seda:Language"/>
                <xsl:apply-templates select="seda:Description"/>
                <xsl:call-template name="dates_extremes">
                    <xsl:with-param name="lang" select="'fr'"/>
                </xsl:call-template>
                <xsl:apply-templates select="seda:DescriptionLevel"/>
                <xsl:apply-templates select="seda:FilePlanPosition"/>
                <xsl:apply-templates select="seda:OtherDescriptiveData"/>
                <xsl:apply-templates select="seda:OriginatingAgency"/>
                <xsl:apply-templates select="seda:Repository"/>
                <xsl:apply-templates select="seda:CustodialHistory"/>
                <xsl:apply-templates select="seda:RelatedObjectReference"/>
                <xsl:apply-templates select="seda:Size"/>
                <xsl:apply-templates select="seda:Keyword"/>
                <xsl:apply-templates select="seda:AccessRestrictionRule"/>
                <xsl:apply-templates select="seda:OtherMetadata"/>
            </div>
        </fieldset>
    </xsl:template>

    <!-- Keyword -->
    <xsl:template match="seda:Keyword">

        <fieldset class="Keyword">
            <legend class="Keyword-name" data-name="{local-name()}">
                <span class="text">
                    <xsl:call-template name="traduction">
                        <xsl:with-param name="term" select="local-name()"/>
                    </xsl:call-template>
                </span>
                <xsl:text> : </xsl:text>
                <xsl:call-template name="name">
                    <xsl:with-param name="nom" select="seda:KeywordContent"/>
                </xsl:call-template>
                <xsl:apply-templates select="@*"/>
            </legend>
            <div class="{local-name()}">
                <div class="code-value">
                    <xsl:apply-templates select="seda:KeywordContent"/>
                    <xsl:apply-templates select="seda:KeywordReference"/>
                    <xsl:apply-templates select="seda:KeywordType"/>
                    <xsl:apply-templates select="seda:AccessRestrictionRule"/>
                </div>
            </div>
        </fieldset>
    </xsl:template>

    <!-- Regles -->
    <xsl:template match="seda:AccessRestrictionRule|seda:AppraisalRule">
        <xsl:call-template name="RulesType"/>
    </xsl:template>

    <!-- Organisations (services d'archives, service versant, service producteur) -->
    <xsl:template
        match="seda:TransferringAgency|seda:ArchivalAgency|seda:OriginatingAgency|seda:Repository|seda:Requester|seda:Receiver|seda:Sender">
        <xsl:variable name="myid" select="generate-id()"/>
        <fieldset class="Organisation">
            <legend data-name="{local-name()}">
                <span class="text">
                    <xsl:call-template name="traduction">
                        <xsl:with-param name="term" select="local-name()"/>
                    </xsl:call-template>
                </span>
                <xsl:apply-templates select="@*"/>
            </legend>
            <div id="{$myid}" style="display: block;">
                <xsl:apply-templates select="*"/>
            </div>
        </fieldset>
    </xsl:template>

    <!-- Contact -->
    <xsl:template match="seda:Contact">
        <fieldset class="{local-name()}">
            <legend data-name="{local-name()}">
                <span class="text">
                    <xsl:call-template name="traduction">
                        <xsl:with-param name="term" select="local-name()"/>
                    </xsl:call-template>
                </span>
                <xsl:apply-templates select="@*"/>
            </legend>
            <div>
                <div class="code-value">
                    <xsl:apply-templates select="seda:PersonName"/>
                    <xsl:apply-templates select="seda:Identification"/>
                    <xsl:apply-templates select="seda:Responsibility"/>
                    <xsl:apply-templates select="seda:DepartmentName"/>
                    <xsl:apply-templates select="seda:Address"/>
                    <xsl:apply-templates select="seda:Communication"/>
                </div>
            </div>
        </fieldset>
    </xsl:template>

    <!-- Address -->
    <xsl:template match="seda:Address">
        <fieldset class="{local-name()}">
            <legend data-name="{local-name()}">
                <span class="text">
                    <xsl:call-template name="traduction">
                        <xsl:with-param name="term" select="local-name()"/>
                    </xsl:call-template>
                </span>
                <xsl:apply-templates select="@*"/>
            </legend>
            <div>
                <div class="code-value">
                    <xsl:apply-templates select="seda:BlockName"/>
                    <xsl:apply-templates select="seda:BuildingName"/>
                    <xsl:apply-templates select="seda:BuildingNumber"/>
                    <xsl:apply-templates select="seda:StreetName"/>
                    <xsl:apply-templates select="seda:Postcode"/>
                    <xsl:apply-templates select="seda:CityName"/>
                    <xsl:apply-templates select="seda:CitySub-DivisionName"/>
                    <xsl:apply-templates select="seda:Country"/>
                    <xsl:apply-templates select="seda:FloorIdentification"/>
                    <xsl:apply-templates select="seda:PostOfficeBox"/>
                    <xsl:apply-templates select="seda:RoomIdentification"/>
                </div>
            </div>
        </fieldset>
    </xsl:template>

    <!-- Blocs dont l'ordre n'a pas besoin d'etre reorganise -->
    <xsl:template match="seda:Communication|seda:CustodialHistory|seda:RelatedData|seda:RelatedObjectReference">
        <fieldset class="{local-name()}">
            <legend data-name="{local-name()}">
                <span class="text">
                    <xsl:call-template name="traduction">
                        <xsl:with-param name="term" select="local-name()"/>
                    </xsl:call-template>
                </span>
                <xsl:apply-templates select="@*"/>
            </legend>
            <div>
                <div class="code-value">
                    <xsl:apply-templates select="*"/>
                </div>
            </div>
        </fieldset>
    </xsl:template>

    <!-- Signature -->
    <xsl:template match="seda:NonRepudiation">
        <fieldset class="{local-name()}">
            <legend data-name="{local-name()}">
                <span class="text">
                    <xsl:call-template name="traduction">
                        <xsl:with-param name="term" select="local-name()"/>
                    </xsl:call-template>
                </span>
                <xsl:apply-templates select="@*"/>
            </legend>
            <div>
                <code>
                    <xsl:call-template name="code"/>
                </code>
            </div>
        </fieldset>
    </xsl:template>

    <!-- Empreinte -->
    <xsl:template match="seda:Integrity">
        <fieldset class="{local-name()}">
            <legend data-name="{local-name()}">
                <span class="text">
                    <xsl:call-template name="traduction">
                        <xsl:with-param name="term" select="local-name()"/>
                    </xsl:call-template>
                </span>
                <xsl:apply-templates select="@*"/>
            </legend>
            <div>
                <xsl:apply-templates/>
            </div>
        </fieldset>
    </xsl:template>

    <!-- Divers: etiquettes qui ne contienent pas d'autre sous-etiquettes  -->
    <xsl:template match="seda:*[count(.//seda:*) = 0][(. != '') or (@* !='')]">
        <div class="{local-name()}">
            <label id="{generate-id()}" data-name="{local-name()}">
                <span class="text">
                    <xsl:call-template name="traduction">
                        <xsl:with-param name="term" select="local-name()"/>
                    </xsl:call-template>
                </span>
                :
            </label>
            <xsl:apply-templates/>
            <xsl:apply-templates select="@*"/>
        </div>
    </xsl:template>

    <!-- cas particuliers -->
    <xsl:template match="seda:AppraisalRule/seda:Code[(. != '') or (@* !='')]">
        <div class="{local-name()}">
            <label id="{generate-id()}" data-name="{local-name()}">
                <span class="text">
                    <xsl:call-template name="traduction">
                        <xsl:with-param name="term">AppraisalRule/Code</xsl:with-param>
                    </xsl:call-template>
                </span>
                :
            </label>
            <xsl:apply-templates/>
            <xsl:apply-templates select="@*"/>
        </div>
    </xsl:template>
    <xsl:template match="seda:AccessRestrictionRule/seda:Code[(. != '') or (@* !='')]">
        <div class="{local-name()}">
            <label id="{generate-id()}" data-name="{local-name()}">
                <span class="text">
                    <xsl:call-template name="traduction">
                        <xsl:with-param name="term">AccessRestrictionRule/Code</xsl:with-param>
                    </xsl:call-template>
                </span>
                :
            </label>
            <xsl:apply-templates/>
            <xsl:apply-templates select="@*"/>
        </div>
    </xsl:template>
    <xsl:template match="seda:Contact/seda:DepartmentName[(. != '') or (@* !='')]">
        <div class="{local-name()}">
            <label id="{generate-id()}" data-name="{local-name()}">
                <span class="text">
                    <xsl:call-template name="traduction">
                        <xsl:with-param name="term">Contact/DepartmentName</xsl:with-param>
                    </xsl:call-template>
                </span>
                :
            </label>
            <xsl:apply-templates/>
            <xsl:apply-templates select="@*"/>
        </div>
    </xsl:template>

    <xsl:template match="seda:URIID[(. != '') or (@* !='')]">
        <a>
            <xsl:choose>
                <xsl:when test="contains(., '@')">
                    <xsl:attribute name="href">mailto:
                        <xsl:apply-templates/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="href">
                        <xsl:apply-templates/>
                    </xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates/>
        </a>
    </xsl:template>

    <xsl:template match="seda:Copy[(. != '') or (@* !='')]/text()">
        <xsl:choose>
            <xsl:when test=".='true'">copie</xsl:when>
            <xsl:when test=".='false'">original</xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="seda:Control">
        <xsl:choose>
            <xsl:when test=".='true'">vrai</xsl:when>
            <xsl:when test=".='false'">faux</xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="seda:Duration/text()">
        <xsl:param name="periode" select="substring-after(.,'P')"/>
        <xsl:variable name="ans">
            <xsl:if test="contains($periode, 'Y')">
                <xsl:value-of select="substring-before($periode, 'Y')"/>
            </xsl:if>
        </xsl:variable>
        <xsl:variable name="mois">
            <xsl:choose>
                <xsl:when test="contains($periode, 'Y')">
                    <xsl:value-of select="substring-before(substring-after($periode, 'Y'), 'M')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="substring-before($periode, 'M')"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="jours">
            <xsl:choose>
                <xsl:when test="contains($periode, 'M')">
                    <xsl:value-of select="substring-before(substring-after($periode, 'M'), 'D')"/>
                </xsl:when>
                <xsl:when test="contains($periode, 'Y')">
                    <xsl:value-of select="substring-before(substring-after($periode, 'Y'), 'D')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="substring-before($periode, 'D')"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:if test="($ans!='')">
            <xsl:value-of select="$ans"/> an<xsl:if test="(number($ans)>1)">s</xsl:if>
        </xsl:if>
        <xsl:if test="($mois!='')">
            <xsl:value-of select="$mois"/> moi<xsl:if test="(number($mois)>1)">s</xsl:if>
        </xsl:if>
        <xsl:if test="($jours!='')">
            <xsl:value-of select="$jours"/> jour<xsl:if test="(number($jours)>1)">s</xsl:if>
        </xsl:if>
    </xsl:template>

    <xsl:template match="seda:ReplyCode/text()">
        <xsl:value-of select="."/>
    </xsl:template>
    <xsl:template match="seda:DescriptionLevel/text()">
        <xsl:variable name="value" select="."/>
        <xsl:variable name="table">codes/seda_v1-0_descriptionlevel_code.xsd</xsl:variable>
        <xsl:value-of
            select="document($table)//xsd:enumeration[@value=$value]//xsd:annotation/xsd:documentation/ccts:Name"/>
    </xsl:template>
    <xsl:template match="seda:Type/text()">
        <xsl:variable name="value" select="."/>
        <xsl:variable name="table">codes/seda_v1-0_documenttype_code.xsd</xsl:variable>
        <xsl:value-of
            select="document($table)//xsd:enumeration[@value=$value]//xsd:annotation/xsd:documentation[@xml:lang='fr']/ccts:Name"/>
    </xsl:template>
    <xsl:template match="seda:DescriptionLanguage/text()|seda:Language/text()">
        <xsl:value-of select="."/>
    </xsl:template>
    <xsl:template match="seda:AppraisalRule/seda:Code/text()">
        <xsl:variable name="value" select="."/>
        <xsl:variable name="table">codes/seda_v1-0_appraisal_code.xsd</xsl:variable>
        <xsl:value-of
            select="document($table)//xsd:enumeration[@value=$value]//xsd:annotation/xsd:documentation/ccts:Name"/>
    </xsl:template>
    <xsl:template match="seda:Size[@unitCode!='']/text()">
        <xsl:variable name="value">
            <xsl:value-of select="../@unitCode"/>
        </xsl:variable>
        <xsl:variable name="table">codes/UNECE_MeasurementUnitCommonCode_7.xsd</xsl:variable>
        <xsl:value-of select="."/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="document($table)//xsd:enumeration[@value=$value]//xsd:annotation/xsd:documentation/ccts:Name"/>
    </xsl:template>
    <xsl:template match="seda:Country/text()">
        <xsl:variable name="value" select="."/>
        <xsl:variable name="table">codes/ISO_ISOTwoletterCountryCode_SecondEdition2006VI-8.xsd</xsl:variable>
        <xsl:value-of
            select="document($table)//xsd:enumeration[@value=$value]//xsd:annotation/xsd:documentation/ccts:Name"/>
    </xsl:template>
    <xsl:template match="seda:AccessRestrictionRule/seda:Code/text()">
        <xsl:variable name="value" select="."/>
        <xsl:variable name="table">codes/seda_v1-0_accessrestriction_code.xsd</xsl:variable>
        <xsl:variable name="meta">
            <xsl:value-of
                select="document($table)//xsd:enumeration[@value=$value]//xsd:annotation/xsd:documentation/ccts:Name"/>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$meta='0 an'">Immédiat</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$meta"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="seda:KeywordType/text()">
        <xsl:variable name="value" select="."/>
        <xsl:variable name="table">codes/seda_v1-0_keywordtype_code.xsd</xsl:variable>
        <xsl:value-of select="document($table)//xsd:enumeration[@value=$value]//xsd:annotation/xsd:documentation"/>
    </xsl:template>


    <!-- Regle par défaut pour mettre en rouge tout ce qui a été oublié -->
    <xsl:template match="*">
        <span style="background-color:red">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <!-- Formatage des Attributs -->
    <xsl:template match="@*[1]">
        <xsl:if test=". != ''">
            <a href="javascript:void(0)" class="info" title="">
                <span class="attributs">
                    <xsl:for-each select="../@*[.!='']">
                        <xsl:if test="local-name()!='filename'">
                            <p>
                                <strong>
                                    <xsl:value-of select="local-name()"/>
                                </strong>="<xsl:value-of select="translate(normalize-space(.), ' ', '&#160;')"/>"</p>
                        </xsl:if>
                    </xsl:for-each>
                </span>
                <i aria-hidden="true" class="fa fa-tag"></i>
            </a>
        </xsl:if>
    </xsl:template>
    <xsl:template match="@*"/>

    <xsl:template name="RulesType">
        <fieldset class="rules">
            <legend data-name="{local-name()}">
                <span class="text">
                    <xsl:call-template name="traduction">
                        <xsl:with-param name="term" select="local-name()"/>
                    </xsl:call-template>
                </span>
                <xsl:apply-templates select="@*"/>
                <xsl:text> : </xsl:text>
                <xsl:call-template name="toString"/>
            </legend>

            <div class="{local-name()}">
                <div class="code-value">
                    <xsl:apply-templates select="seda:Duration"/>
                    <xsl:apply-templates select="seda:Code"/>
                    <xsl:apply-templates select="seda:StartDate"/>
                </div>
            </div>
        </fieldset>
    </xsl:template>


    <!-- Formatage des dates extremes -->
    <xsl:template name="dates_extremes">
        <xsl:param name="lang"></xsl:param>
        <xsl:if test="(seda:LatestDate != '') or (seda:OldestDate != '')">
            <div>
                <xsl:choose>
                    <xsl:when test="($lang = 'fr')">
                        <label>Dates extrêmes :</label>
                        <xsl:if test="(seda:OldestDate != '')">
                            <xsl:text> du </xsl:text>
                            <xsl:apply-templates select="seda:OldestDate"/>
                        </xsl:if>
                        <xsl:if test="(seda:LatestDate != '')">
                            <xsl:text> jusqu'au </xsl:text>
                            <xsl:apply-templates select="seda:LatestDate"/>
                        </xsl:if>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates/>
                    </xsl:otherwise>
                </xsl:choose>
            </div>
        </xsl:if>
    </xsl:template>

    <!-- Formatage des noms -->
    <xsl:template name="name">
        <xsl:param name="nom"></xsl:param>
        <xsl:choose>
            <xsl:when test="($nom != '')">
                <xsl:value-of select="$nom"/>
            </xsl:when>
            <xsl:otherwise>NO_NAME</xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template
        match="seda:LatestDate|seda:OldestDate|seda:StartDate/text()|seda:Date/text()|seda:Receipt/text()|seda:Response/text()|seda:Submission/text()|seda:Issue/text()|seda:Creation/text()"
        priority="1">
        <xsl:call-template name="date">
            <xsl:with-param name="date" select="."/>
        </xsl:call-template>
    </xsl:template>

    <!-- Formatage d'une date -->
    <xsl:template name="date">
        <xsl:param name="date"></xsl:param>
        <xsl:variable name="year">
            <xsl:value-of select="substring($date, 1, 4)"/>
        </xsl:variable>
        <xsl:variable name="mm">
            <xsl:value-of select="substring($date, 6, 2)"/>
        </xsl:variable>
        <xsl:variable name="month">
            <xsl:choose>
                <xsl:when test="$mm='01'">janvier</xsl:when>
                <xsl:when test="$mm='02'">février</xsl:when>
                <xsl:when test="$mm='03'">mars</xsl:when>
                <xsl:when test="$mm='04'">avril</xsl:when>
                <xsl:when test="$mm='05'">mai</xsl:when>
                <xsl:when test="$mm='06'">juin</xsl:when>
                <xsl:when test="$mm='07'">juillet</xsl:when>
                <xsl:when test="$mm='08'">août</xsl:when>
                <xsl:when test="$mm='09'">septembre</xsl:when>
                <xsl:when test="$mm='10'">octobre</xsl:when>
                <xsl:when test="$mm='11'">novembre</xsl:when>
                <xsl:when test="$mm='12'">décembre</xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="day">
            <xsl:value-of select="substring($date, 9, 2)"/>
        </xsl:variable>
        <xsl:value-of select="$day"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="$month"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="$year"/>
        <!-- <xsl:text> à </xsl:text>
        <xsl:value-of select="substring-after($date, 'T')"/> -->
    </xsl:template>


    <!-- Formatage des code xml -->
    <xsl:template name="code">
        <xsl:if test="node()">
            <xsl:text>&lt;</xsl:text>
            <xsl:value-of select="local-name()"/>
            <xsl:text> xmlns="</xsl:text>
            <xsl:value-of select="namespace-uri()"/>
            <xsl:text>" </xsl:text>
            <xsl:for-each select="@*">
                <xsl:text> </xsl:text>
                <xsl:value-of select="local-name()"/>
                <xsl:text>="</xsl:text>
                <xsl:value-of
                    select="."/>
                <xsl:text>"</xsl:text>
            </xsl:for-each>
            <xsl:text>&gt;</xsl:text>
        </xsl:if>

        <xsl:value-of select="normalize-space(text())"/>

        <xsl:for-each select="text()|*">
            <xsl:call-template name="code"/>
        </xsl:for-each>

        <xsl:if test="node()">
            <xsl:text>&lt;/</xsl:text>
            <xsl:value-of select="local-name()"/>
            <xsl:text>&gt;</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- Traduction de termes en français -->
    <xsl:template name="traduction">
        <xsl:param name="term"></xsl:param>
        <xsl:choose>
            <!-- schema archive_transfer -->
            <xsl:when test="($term = 'ArchiveTransfer')">Transfert</xsl:when>
            <xsl:when test="($term = 'Comment')">Commentaire</xsl:when>
            <xsl:when test="($term = 'Date')">Date</xsl:when>
            <xsl:when test="($term = 'RelatedTransferReference')">Référence à un autre transfert</xsl:when>
            <xsl:when test="($term = 'TransferIdentifier')">Identifiant du transfert</xsl:when>
            <xsl:when test="($term = 'TransferRequestReplyIdentifier')">Identifiant de la réponse à la demande de
                transfert
            </xsl:when>
            <xsl:when test="($term = 'ArchivalAgency')">Service d'archives</xsl:when>
            <xsl:when test="($term = 'TransferringAgency')">Service versant</xsl:when>
            <xsl:when test="($term = 'NonRepudiation')">Signature</xsl:when>
            <!-- schema archive -->
            <xsl:when test="($term = 'AccessRestrictionRule')">Règle de restriction d'accès</xsl:when>
            <xsl:when test="($term = 'AppraisalRule')">Règle de sort final</xsl:when>
            <xsl:when test="($term = 'ArchivalAgencyArchiveIdentifier')">Identifiant attribué par le service d'archives</xsl:when>
            <xsl:when test="($term = 'ArchivalAgencyDocumentIdentifier')">Identifiant attribué par le service d'archives</xsl:when>
            <xsl:when test="($term = 'ArchivalAgencyObjectIdentifier')">Identifiant attribué par le service d'archives</xsl:when>
            <xsl:when test="($term = 'ArchivalAgreement')">Accord de versement</xsl:when>
            <xsl:when test="($term = 'ArchivalProfile')">Profil d'archive</xsl:when>
            <xsl:when test="($term = 'Archive')">Archive</xsl:when>
            <xsl:when test="($term = 'ArchiveObject')">Unité documentaire</xsl:when>
            <xsl:when test="($term = 'Attachment')">Pièce jointe</xsl:when>
            <xsl:when test="($term = 'Code')">Code</xsl:when>
            <xsl:when test="($term = 'ContentDescription')">Description du contenu</xsl:when>
            <xsl:when test="($term = 'Keyword')">Mot-clé</xsl:when>
            <xsl:when test="($term = 'Control')">Exigences de contrôle</xsl:when>
            <xsl:when test="($term = 'Copy')">Exemplaire</xsl:when>
            <xsl:when test="($term = 'Creation')">Date de création</xsl:when>
            <xsl:when test="($term = 'CustodialHistory')">Historique de conservation</xsl:when>
            <xsl:when test="($term = 'CustodialHistoryItem')">Evénement</xsl:when>
            <xsl:when test="($term = 'Data')">Données binaires</xsl:when>
            <xsl:when test="($term = 'Description')">Description</xsl:when>
            <xsl:when test="($term = 'DescriptionLanguage')">Langue de la description</xsl:when>
            <xsl:when test="($term = 'DescriptionLevel')">Niveau de description</xsl:when>
            <xsl:when test="($term = 'Document')">Document</xsl:when>
            <xsl:when test="($term = 'Duration')">Durée d'utilité Administrative</xsl:when>
            <xsl:when test="($term = 'FilePlanPosition')">Position dans le plan de classement</xsl:when>
            <xsl:when test="($term = 'Integrity')">Empreinte</xsl:when>
            <xsl:when test="($term = 'Issue')">Date d'émission</xsl:when>
            <xsl:when test="($term = 'KeywordContent')">Libellé</xsl:when>
            <xsl:when test="($term = 'KeywordReference')">Identifiant dans le référentiel associé</xsl:when>
            <xsl:when test="($term = 'KeywordType')">Type</xsl:when>
            <xsl:when test="($term = 'Language')">Langue du contenu</xsl:when>
            <xsl:when test="($term = 'LatestDate')">Date de fin</xsl:when>
            <xsl:when test="($term = 'Name')">Nom</xsl:when>
            <xsl:when test="($term = 'OldestDate')">Date de début</xsl:when>
            <xsl:when test="($term = 'OriginatingAgency')">Service producteur</xsl:when>
            <xsl:when test="($term = 'OriginatingAgencyArchiveIdentifier')">Identifiant attribué par le service producteur</xsl:when>
            <xsl:when test="($term = 'OriginatingAgencyDocumentIdentifier')">Identifiant attribué par le service producteur</xsl:when>
            <xsl:when test="($term = 'OriginatingAgencyObjectIdentifier')">Identifiant attribué par le service producteur</xsl:when>
            <xsl:when test="($term = 'OtherDescriptiveData')">Autres informations</xsl:when>
            <xsl:when test="($term = 'OtherMetadata')">Autres métadonnées</xsl:when>
            <xsl:when test="($term = 'Purpose')">Objet</xsl:when>
            <xsl:when test="($term = 'Receipt')">Date de réception</xsl:when>
            <xsl:when test="($term = 'RelatedData')">Données liées</xsl:when>
            <xsl:when test="($term = 'RelatedObjectIdentifier')">Identifiant</xsl:when>
            <xsl:when test="($term = 'RelatedObjectReference')">Référence complémentaire</xsl:when>
            <xsl:when test="($term = 'Relation')">Nature de la relation</xsl:when>
            <xsl:when test="($term = 'Repository')">Service d'archives</xsl:when>
            <xsl:when test="($term = 'Response')">Date de réponse</xsl:when>
            <xsl:when test="($term = 'ServiceLevel')">Niveau de service demandé</xsl:when>
            <xsl:when test="($term = 'Size')">Taille</xsl:when>
            <xsl:when test="($term = 'StartDate')">Date de départ du calcul</xsl:when>
            <xsl:when test="($term = 'Status')">Etat</xsl:when>
            <xsl:when test="($term = 'Submission')">Date de soumission</xsl:when>
            <xsl:when test="($term = 'TransferringAgencyArchiveIdentifier')">Identifiant attribué par le service versant</xsl:when>
            <xsl:when test="($term = 'TransferringAgencyDocumentIdentifier')">Identifiant attribué par le service versant</xsl:when>
            <xsl:when test="($term = 'TransferringAgencyObjectIdentifier')">Identifiant attribué par le service versant</xsl:when>
            <xsl:when test="($term = 'Type')">Type</xsl:when>
            <!-- schema Organization -->
            <xsl:when test="($term = 'Address')">Adresse</xsl:when>
            <xsl:when test="($term = 'BlockName')">Quartier</xsl:when>
            <xsl:when test="($term = 'BuildingName')">Bâtiment</xsl:when>
            <xsl:when test="($term = 'BuildingNumber')">Numéro</xsl:when>
            <xsl:when test="($term = 'BusinessType')">Code de l'activité</xsl:when>
            <xsl:when test="($term = 'Channel')">Type/Outil de communication</xsl:when>
            <xsl:when test="($term = 'CityName')">Localité</xsl:when>
            <xsl:when test="($term = 'CitySub-DivisionName')">Arrondissement / quartier</xsl:when>
            <xsl:when test="($term = 'Communication')">Moyen de communication</xsl:when>
            <xsl:when test="($term = 'CompleteNumber')">
                <i class="icon-phone-sign"></i>
            </xsl:when>
            <xsl:when test="($term = 'Contact')">Contact</xsl:when>
            <xsl:when test="($term = 'Country')">Pays</xsl:when>
            <xsl:when test="($term = 'Contact/DepartmentName')">Service</xsl:when>
            <xsl:when test="($term = 'FloorIdentification')">Etage</xsl:when>
            <xsl:when test="($term = 'Identification')">Identifiant</xsl:when>
            <xsl:when test="($term = 'LegalClassification')">Code de la catégorie juridique</xsl:when>
            <xsl:when test="($term = 'PersonName')">Nom</xsl:when>
            <xsl:when test="($term = 'PostOfficeBox')">Boite postale</xsl:when>
            <xsl:when test="($term = 'Postcode')">Code postal</xsl:when>
            <xsl:when test="($term = 'Responsibility')">Attributions</xsl:when>
            <xsl:when test="($term = 'RoomIdentification')">Pièce</xsl:when>
            <xsl:when test="($term = 'StreetName')">Voie</xsl:when>
            <xsl:when test="($term = 'URIID')">Identifiant</xsl:when>

            <xsl:when test="($term = 'AccessRestrictionRule/Code')">Délai de communicabilité</xsl:when>
            <xsl:when test="($term = 'AppraisalRule/Code')">Sort final</xsl:when>
            <xsl:when test="($term = 'Keywords')">Mot-clé</xsl:when>

            <!-- autres messages -->
            <xsl:when test="($term = 'ReplyCode')">Code retour</xsl:when>
            <xsl:when test="($term = 'GrantDate')">Date du transfert de responsabilité</xsl:when>
            <xsl:when test="($term = 'TransferReplyIdentifier')">Identifiant de la réponse au transfert</xsl:when>
            <xsl:when test="($term = 'AcknowledgementIdentifier')">Identifiant de l'accusé de réception</xsl:when>
            <xsl:when test="($term = 'MessageReceivedIdentifier')">Identifiant du message dont on accuse réception
            </xsl:when>
            <xsl:when test="($term = 'Receiver')">Service qui accuse réception</xsl:when>
            <xsl:when test="($term = 'Sender')">Expéditeur du message dont on accuse réception</xsl:when>
            <xsl:when test="($term = 'DeliveryRequestIdentifier')">Identifiant de la demande de communication</xsl:when>
            <xsl:when test="($term = 'Derogation')">Demande de dérogation</xsl:when>
            <xsl:when test="($term = 'UnitIdentifier')">Identifiant</xsl:when>
            <xsl:when test="($term = 'Requester')">Demandeur</xsl:when>
            <xsl:when test="($term = 'AuthorizationRequestReplyIdentifier')">Identifiant de l'autorisation de
                communication du service de contrôle
            </xsl:when>
            <xsl:when test="($term = 'DeliveryRequestIdentifier')">Identifiant de la demande de communication</xsl:when>
            <xsl:when test="($term = 'DeliveryRequestReplyIdentifier')">Identifiant de la réponse à la demande de
                communcation
            </xsl:when>

            <xsl:otherwise>
                <xsl:value-of select="$term"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="toString">
        <!--<xsl:value-of select="local-name()"/>-->
        <xsl:choose>
            <xsl:when test="local-name()='AppraisalRule'">
                <strong>
                    <xsl:apply-templates select="seda:Code/text()"/>
                </strong>
                <xsl:text>, DUA : </xsl:text>
                <strong>
                    <xsl:apply-templates select="seda:Duration/text()"/>
                </strong>
            </xsl:when>
            <xsl:when test="local-name()='AccessRestrictionRule'">
                <strong>
                    <xsl:value-of select="seda:Code/text()"/>
                </strong>
                <xsl:text>, délai de communicabilité </xsl:text>
                <strong>
                    <xsl:apply-templates select="seda:Code/text()"/>
                </strong>
            </xsl:when>
            <xsl:when test="local-name()='OriginatingAgency'">
                <xsl:value-of select="seda:Name"/>
                <xsl:text> (</xsl:text>
                <xsl:value-of select="seda:Identification"/>
                <xsl:text>)</xsl:text>
            </xsl:when>
            <xsl:when test="local-name()='Address'">
                <xsl:value-of select="seda:StreetName"/>, <xsl:value-of select="seda:Postcode"/>
                <xsl:value-of select="seda:CityName"/>
            </xsl:when>
            <xsl:when test="local-name()='Communication'">
                <xsl:value-of select="seda:CompleteNumber"/>
            </xsl:when>
            <xsl:when test="local-name()='Contact'">
                <xsl:value-of select="seda:PersonName"/>
            </xsl:when>
            <xsl:when test="local-name()='Document'">
                <xsl:if test="seda:Description!=''">
                    <xsl:value-of select="seda:Description"/>
                    <xsl:text> : </xsl:text>
                </xsl:if>
                <xsl:value-of select="seda:Attachment/@filename"/>
            </xsl:when>
            <xsl:otherwise>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Affichage du pied de page -->
    <xsl:template name="pied_de_page">
        <div id="footer">SEDA V1.0 -
            <a href="http://www.archivesdefrance.culture.gouv.fr/gerer/archives-electroniques/standard/seda/">Standard
                d'échange de données pour l'archivage
            </a>
        </div>
    </xsl:template>

</xsl:stylesheet>
