<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
    xmlns:seda="fr:gouv:culture:archivesdefrance:seda:v2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:ccts="urn:un:unece:uncefact:documentation:standard:CoreComponentsTechnicalSpecification:2"
    exclude-result-prefixes="seda xsl xsd ccts">

    <xsl:output
        method="html"
        indent="yes"
        media-type="text/html"
        encoding="UTF-8"
        doctype-public="-//W3C//DTD HTML 4.01//EN"
        doctype-system="http://www.w3.org/TR/html4/strict.dtd" />

    <xsl:param name="afficheDocument" select="defaultstring"/>
    <xsl:param name="ratchetUrl" select="defaultstring"/>

    <xsl:template match="/">
        <html>
            <head>
                <link href="../../favicon.ico" type="image/x-icon" rel="icon" />
                <link href="../../favicon.ico" type="image/x-icon" rel="shortcut icon" />
                <link rel="stylesheet" href="../../css/font-awesome.min.css"/>
                <link rel="stylesheet" href="../../css/jstree/style.min.css"/>
                <link rel="stylesheet" href="../../css/descriptionArchive.css"/>
                <link rel="stylesheet" href="../../css/jquery-ui.min.css"/>
                <script type="text/javascript" src="../../js/autobahn.min.js"></script>
                <script type="text/javascript" src="../../js/uuid.js"></script>
                <script type="text/javascript" src="../../js/jquery-1.11.1.min.js"></script>
                <script type="text/javascript" src="../../js/jquery-ui.min.js"></script>
                <script type="text/javascript" src="../../js/jstree/jstree.min.js"></script>
                <script type="text/javascript" src="../../js/asalae.descriptionArchive.js"></script>
                <script type="text/javascript" src="../../js/asalae.arborescence.js"/>
                <script type="text/javascript" src="../../js/asalae.seda2html-common.js"/>
                <script type="text/javascript" src="../../js/asalae.global-top.js"></script>
                <title>Standard d'Echange de Données pour l'Archivage (SEDA)</title>
            </head>
            <body>
                <div id="navigation">
                    <div id="headnavigation">
                        <h4>NAVIGATION</h4>
                        <p id="minimize">
                            <i class="icon-chevron-up" title="masquer/afficher"></i>
                        </p>
                    </div>
                    <div id="arbonavigation">
                        <xsl:call-template name="menuarbo"/>
                    </div>
                </div>
                <div id="main">
                    <xsl:apply-templates/>
                </div>
                <script id="download-script" data-ratchet="{$ratchetUrl}">
                    var ratchetUrl = $('#download-script').attr('data-ratchet');
                    AsalaeDownloads.getInstance(ratchetUrl).handleDownloads();
                </script>
            </body>
        </html>
    </xsl:template>

    <!-- Arbo -->
    <xsl:template name="menuarbo">
        <xsl:apply-templates select="seda:ArchiveTransfer|seda:Archive" mode="arbo" />
    </xsl:template>

    <xsl:template match="seda:DescriptiveMetadata" mode="arbo">
        <xsl:apply-templates  select="seda:ArchiveUnit" mode="arbo" />
    </xsl:template>

    <xsl:template match="seda:ArchiveUnit" mode="arbo">
        <ul>
            <li>
                <a href="#{generate-id()}">
                    <xsl:value-of select="seda:Content/seda:Title" />
                </a>
                <xsl:apply-templates  select="seda:ArchiveUnit" mode="arbo" />
            </li>
        </ul>
    </xsl:template>

    <!-- Archive -->
    <xsl:template match="seda:ArchiveTransfer">
        <h1 class="message">
            Message de
            <xsl:call-template name="traduction">
                <xsl:with-param name="term" select="local-name()"/>
            </xsl:call-template>
            <xsl:text> - SEDA v2.0</xsl:text>
            <xsl:apply-templates select="@*"/>
            <small><a href="javascript:invertDataName()" class="pull-right">Termes SEDA / Termes Français</a></small>
        </h1>
        <fieldset>
            <legend>En-tête</legend>
            <div id="entete" style="display: block;">
                <div class="info-generales">
                    <xsl:apply-templates select="seda:Comment|seda:Date|seda:MessageIdentifier|seda:ArchivalAgreement|seda:RelatedTransferReference|seda:TransferRequestReplyIdentifier"/>
                </div>
                <div class="acteurs">
                    <div class="acteur">
                        <xsl:apply-templates select="seda:TransferringAgency"/>
                    </div>
                    <div class="acteur">
                        <xsl:apply-templates select="seda:ArchivalAgency"/>
                    </div>
                </div>
                <xsl:apply-templates select="seda:CodeListVersions"/>
            </div>
        </fieldset>
        <fieldset>
            <legend>Contenu</legend>
            <div id="content" style="display: block;">
                <div id="commands">
                    <span>
                        <a href="javascript:expandAll(true)">tout déplier</a>
                        |
                        <a href="javascript:expandAll(false)">tout replier</a>
                    </span>
                </div>
                <xsl:apply-templates select="seda:DataObjectPackage"/>
            </div>
        </fieldset>
        <xsl:call-template name="pied_de_page"/>
        <div style="display: none;"><xsl:apply-templates/></div>
    </xsl:template>

    <!-- Organisations (services d'archives, service versant, service producteur) -->
    <xsl:template match="seda:TransferringAgency|seda:ArchivalAgency">
        <xsl:variable name="myid" select="generate-id()"/>
        <fieldset class="Organisation">
            <legend data-name="{local-name()}">
                <span class="text">
                    <xsl:call-template name="traduction">
                        <xsl:with-param name="term" select="local-name()"/>
                    </xsl:call-template>
                </span>
                <xsl:call-template name="documentation">
                    <xsl:with-param name="term" select="local-name()"/>
                </xsl:call-template>
                <xsl:apply-templates select="@*"/>
            </legend>
            <div id="{$myid}" style="display: block;">
                <xsl:apply-templates/>
            </div>
        </fieldset>
    </xsl:template>

    <!-- GENERIC TYPE FIELDSET -->
    <xsl:template match="seda:*[count(.//seda:*) > 0]" priority="0">
        <xsl:variable name="tagName" select="local-name()"></xsl:variable>
        <xsl:variable name="cssClass">
            <xsl:choose>
                <xsl:when test="$tagName = 'DataObjectPackage'">dark-blue</xsl:when>
                <xsl:when test="$tagName = 'Content'">blue</xsl:when>
                <xsl:when test="$tagName = 'CustodialHistory'">deep-blue</xsl:when>
                <xsl:when test="$tagName = 'Keyword'">green</xsl:when>
                <xsl:otherwise>light-blue</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <fieldset class="colored-fieldset" id="{generate-id()}">
            <legend class="{$cssClass}" data-name="{local-name()}">
                <span class="text">
                    <xsl:call-template name="traduction">
                        <xsl:with-param name="term" select="local-name()"/>
                    </xsl:call-template>
                </span>
                <xsl:call-template name="documentation">
                    <xsl:with-param name="term" select="local-name()"/>
                </xsl:call-template>
                <xsl:apply-templates select="@*"/>
            </legend>
            <div>
                <xsl:apply-templates/>
            </div>
        </fieldset>
    </xsl:template>

    <!-- GENERIC TYPE LABEL + TAG + VALUE  -->
    <xsl:template match="seda:*[count(.//seda:*) = 0][(. != '') or (@* !='')]" priority="0">
        <div class="{local-name()}">
            <label id="{generate-id()}" data-name="{local-name()}">
                <span class="text">
                    <xsl:call-template name="traduction">
                        <xsl:with-param name="term" select="local-name()"/>
                    </xsl:call-template>
                </span>
                <xsl:call-template name="documentation">
                    <xsl:with-param name="term" select="local-name()"/>
                </xsl:call-template>
                :
            </label>
            <xsl:apply-templates/>
            <xsl:apply-templates select="@*"/>
        </div>
    </xsl:template>

    <!-- Formatage des Attributs -->
    <xsl:template match="@*[1]">
        <xsl:if test=". != ''">
            <a href="javascript:void(0)" class="info" title="">
                <span class="attributs">
                    <xsl:for-each select="../@*[.!='']">
                        <xsl:if test="local-name()!='filename'">
                            <p>
                                <strong>
                                    <xsl:value-of select="local-name()"/>
                                </strong>="<xsl:value-of select="translate(normalize-space(.), ' ', '&#160;')"/>"
                            </p>
                        </xsl:if>
                    </xsl:for-each>
                </span>
                <i aria-hidden="true" class="fa fa-tag"></i>
            </a>
        </xsl:if>
    </xsl:template>
    <xsl:template match="@*"/>

    <!-- Formatage d'une date -->
    <xsl:template name="date">
        <xsl:param name="date"></xsl:param>
        <xsl:variable name="year">
            <xsl:value-of select="substring($date, 1, 4)"/>
        </xsl:variable>
        <xsl:variable name="mm">
            <xsl:value-of select="substring($date, 6, 2)"/>
        </xsl:variable>
        <xsl:variable name="month">
            <xsl:choose>
                <xsl:when test="$mm='01'">janvier</xsl:when>
                <xsl:when test="$mm='02'">février</xsl:when>
                <xsl:when test="$mm='03'">mars</xsl:when>
                <xsl:when test="$mm='04'">avril</xsl:when>
                <xsl:when test="$mm='05'">mai</xsl:when>
                <xsl:when test="$mm='06'">juin</xsl:when>
                <xsl:when test="$mm='07'">juillet</xsl:when>
                <xsl:when test="$mm='08'">août</xsl:when>
                <xsl:when test="$mm='09'">septembre</xsl:when>
                <xsl:when test="$mm='10'">octobre</xsl:when>
                <xsl:when test="$mm='11'">novembre</xsl:when>
                <xsl:when test="$mm='12'">décembre</xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="day">
            <xsl:value-of select="substring($date, 9, 2)"/>
        </xsl:variable>
        <xsl:value-of select="$day"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="$month"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="$year"/>
    </xsl:template>

    <xsl:template
        match="seda:LatestDate|seda:OldestDate|seda:StartDate/text()|seda:Date/text()|seda:Receipt/text()|seda:Response/text()|seda:Submission/text()|seda:Issue/text()|seda:Creation/text()"
        priority="1">
        <xsl:call-template name="date">
            <xsl:with-param name="date" select="."/>
        </xsl:call-template>
    </xsl:template>

    <!-- Affichage du pied de page -->
    <xsl:template name="pied_de_page">
        <div id="footer">SEDA V2.0 -
            <a href="http://www.archivesdefrance.culture.gouv.fr/gerer/archives-electroniques/standard/seda/">Standard
                d'échange de données pour l'archivage
            </a>
        </div>
    </xsl:template>

    <xsl:template name="documentation">
        <xsl:param name="term"></xsl:param>
        <div class="doc-block">
            <span class="attributs">
                <strong><xsl:value-of select="$term"/> :<br/></strong>
                <xsl:choose>
                    <xsl:when test="$term = 'AccessRule'">Gestion de la communicabilité.</xsl:when>
                    <xsl:when test="$term = 'AccessRuleCodeListVersion'">Version des listes de codes pour les règles de communicabilité.</xsl:when>
                    <xsl:when test="$term = 'Acknowledgement'">Accusé de réception d'un message.</xsl:when>
                    <xsl:when test="$term = 'AcquiredDate'">Date de numérisation.</xsl:when>
                    <xsl:when test="$term = 'Activity'">En plus des balises Tag et Keyword, il est possible d'indexer les objets avec des éléments pré-définis : Activité.</xsl:when>
                    <xsl:when test="$term = 'Address'">En plus des balises Tag et Keyword, il est possible d'indexer les objets avec des éléments pré-définis : Adresse.</xsl:when>
                    <xsl:when test="$term = 'Addressee'">Destinataire pour action. Utilisé pour indiquer le nom du destinatire par exemple dans un courrier électronique.</xsl:when>
                    <xsl:when test="$term = 'AppraisalRule'">Gestion de la durée d’utilité administrative.</xsl:when>
                    <xsl:when test="$term = 'AppraisalRuleCodeListVersion'">Version des listes de codes pour les règles de durée d'utilité administrative.</xsl:when>
                    <xsl:when test="$term = 'ArchivalAgency'">Service d'archives responsable.</xsl:when>
                    <xsl:when test="$term = 'ArchivalAgencyArchiveUnitIdentifier'">Identifiant métier attribué à l'ArchiveUnit par le service d'archives. Peut être comparé à une cote.</xsl:when>
                    <xsl:when test="$term = 'ArchivalAgreement'">Accord de service.</xsl:when>
                    <xsl:when test="$term = 'ArchivalProfile'">Profil d’archivage applicable aux ArchiveUnit.</xsl:when>
                    <xsl:when test="$term = 'ArchiveDeliveryRequest'">Demande de communication d'archives.</xsl:when>
                    <xsl:when test="$term = 'ArchiveDeliveryRequestReply'">Réponse à une demande de communication d'archives.</xsl:when>
                    <xsl:when test="$term = 'ArchiveDestructionNotification'">Notification d'élimination d'archives.</xsl:when>
                    <xsl:when test="$term = 'ArchiveModificationNotification'">Notification de modification d'archives (format ou métadonnées).</xsl:when>
                    <xsl:when test="$term = 'ArchiveRestitutionRequest'">Demande de restitution d'archives.</xsl:when>
                    <xsl:when test="$term = 'ArchiveRestitutionRequestReply'">Réponse à une demande de restitution d'archives.</xsl:when>
                    <xsl:when test="$term = 'ArchiveTransfer'">Transfert d'archives.</xsl:when>
                    <xsl:when test="$term = 'ArchiveTransferReply'">Réponse à un transfert d'archives (acceptation, rejet, anomalie..).</xsl:when>
                    <xsl:when test="$term = 'ArchiveTransferRequest'">Demande de transfert d’archives.</xsl:when>
                    <xsl:when test="$term = 'ArchiveTransferRequestReply'">Réponse à une demande de transfert d’archives.</xsl:when>
                    <xsl:when test="$term = 'ArchiveUnit'">Correspond à la notion de composant en ISAD(G). ArchiveUnit permet à la fois de gérer la hiérarchie intellectuelle, tout en contenant les métadonnées de description et de gestion propres à chaque niveau de description archivistique.</xsl:when>
                    <xsl:when test="$term = 'ArchiveUnitProfile'">Référence à une partie d'un profil d’archivage applicable à un ArchiveUnit en particulier. Permet par exemple de faire référence à une typologie documentaire dans un profil d'archivage.</xsl:when>
                    <xsl:when test="$term = 'ArchiveUnitRefId'">Permet de faire une référence à d'autres ArchiveUnit dans la même transaction.</xsl:when>
                    <xsl:when test="$term = 'ArchiveUnitReferenceAbstract'">Contient les requêtes nécessaires pour trouver un ArchiveUnit et pointer sur lui dans un prochain ArchiveUnit. Permet de référencer un noeud déjà existant dans un arbre à partir d'un transfert précédent.</xsl:when>
                    <xsl:when test="$term = 'Attachment'">Objet-données (contenu binaire ou fichier joint).</xsl:when>
                    <xsl:when test="$term = 'Audio'">Métadonnées pour un objet-données de type audio.</xsl:when>
                    <xsl:when test="$term = 'AuthorizationControlAuthorityRequest'">Demande d'autorisation au service de contrôle.</xsl:when>
                    <xsl:when test="$term = 'AuthorizationControlAuthorityRequestReply'">Réponse donnée à une demande d'autorisation au service de contrôle.</xsl:when>
                    <xsl:when test="$term = 'AuthorizationOriginatingAgencyRequest'">Demande d'autorisation au service producteur.</xsl:when>
                    <xsl:when test="$term = 'AuthorizationOriginatingAgencyRequestReply'">Réponse donnée à une demande d'autorisation au service producteur.</xsl:when>
                    <xsl:when test="$term = 'AuthorizationReason'">Motif de l'autorisation.</xsl:when>
                    <xsl:when test="$term = 'AuthorizationReasonCodeListVersion'">Version de la liste de codes d'autorisation.</xsl:when>
                    <xsl:when test="$term = 'AuthorizationRequestContent'">Demande d’autorisation.</xsl:when>
                    <xsl:when test="$term = 'AuthorizationRequestReply'">Réponse à la demande d’autorisation.</xsl:when>
                    <xsl:when test="$term = 'AuthorizationRequestReplyIdentifier'">Identifiant de la réponse à une demande d’autorisation.</xsl:when>
                    <xsl:when test="$term = 'AuthorizedAgent'">Titulaire des droits de propriété intellectuelle.</xsl:when>
                    <xsl:when test="$term = 'BinaryDataObject'">Bloc de métadonnées techniques des objets-données numériques. Le caractère facultatif est requis afin de permettre le transfert d'un plan de classement sans DataObject joint.</xsl:when>
                    <xsl:when test="$term = 'BirthDate'">Date de naissance de la personne.</xsl:when>
                    <xsl:when test="$term = 'BirthName'">Nom de naissance d'une personne.</xsl:when>
                    <xsl:when test="$term = 'BirthPlace'">Lieu de naissance de la personne.</xsl:when>
                    <xsl:when test="$term = 'City'">En plus des balises Tag et Keyword, il est possible d'indexer les objets avec des éléments pré-définis : Ville.</xsl:when>
                    <xsl:when test="$term = 'ClassificationLevel'">Référence au niveau de classification.</xsl:when>
                    <xsl:when test="$term = 'ClassificationOwner'">Propriétaire de la classification. Service émetteur au sens de l’IGI 1300.</xsl:when>
                    <xsl:when test="$term = 'ClassificationReassessingDate'">Date de réévaluation de la classification.</xsl:when>
                    <xsl:when test="$term = 'ClassificationRule'">Gestion de la classification.</xsl:when>
                    <xsl:when test="$term = 'ClassificationRuleCodeListVersion'">Version des listes de codes pour les règles de classification.</xsl:when>
                    <xsl:when test="$term = 'CodeListVersions'">Listes de codes de références utilisés dans le message.</xsl:when>
                    <xsl:when test="$term = 'Comment'">Commentaire.</xsl:when>
                    <xsl:when test="$term = 'Compressed'">Indique si l’objet-données est compressé et doit être décompressé.</xsl:when>
                    <xsl:when test="$term = 'CompressionAlgorithmCodeListVersion'">Version de la liste de code de l'algorithme de compression.</xsl:when>
                    <xsl:when test="$term = 'Content'">Métadonnées de description associées à un ArchiveUnit.</xsl:when>
                    <xsl:when test="$term = 'ControlAuthority'">Autorité de contrôle.</xsl:when>
                    <xsl:when test="$term = 'Corpname'">Nom d'une entité.</xsl:when>
                    <xsl:when test="$term = 'Country'">En plus des balises Tag et Keyword, il est possible d'indexer les objets avec des éléments pré-définis : Pays.</xsl:when>
                    <xsl:when test="$term = 'Coverage'">Couverture spatiale, temporelle ou juridictionnelle de l’ArchiveUnit</xsl:when>
                    <xsl:when test="$term = 'CreatedDate'">Date de création.</xsl:when>
                    <xsl:when test="$term = 'CreatingApplicationName'">Nom de l'application utilisée pour créer le fichier.</xsl:when>
                    <xsl:when test="$term = 'CreatingApplicationVersion'">Version de l'application utilisée pour créer le fichier.</xsl:when>
                    <xsl:when test="$term = 'CreatingOs'">Système d’exploitation utilisé pour créer le fichier.</xsl:when>
                    <xsl:when test="$term = 'CreatingOsVersion'">Version du système d'exploitation utilisé pour créer le fichier.</xsl:when>
                    <xsl:when test="$term = 'CustodialHistory'">Énumère les changements successifs de propriété, de responsabilité et de conservation des ArchiveUnit avant leur entrée dans le lieu de conservation. On peut notamment y indiquer comment s'est effectué le passage de l'application d'origine au fichier archivable. Correspond à l'historique de la conservation en ISAD(G).</xsl:when>
                    <xsl:when test="$term = 'CustodialHistoryFile'">Référence à un fichier de journalisation externe.</xsl:when>
                    <xsl:when test="$term = 'CustodialHistoryItem'">Description d'une période ou d'un événement précis dans l'historique.</xsl:when>
                    <xsl:when test="$term = 'DataObjectGroupId'">Référence à un groupe d'objets-données listé dans les métadonnées de transport.</xsl:when>
                    <xsl:when test="$term = 'DataObjectGroupReferenceId'">Identifiant du groupe d'objets-données DataObjectVersionGroup.</xsl:when>
                    <xsl:when test="$term = 'DataObjectId'">Référence à un objet-données listé dans les métadonnées de transport.</xsl:when>
                    <xsl:when test="$term = 'DataObjectPackage'">Objets-données échangés dans le message.</xsl:when>
                    <xsl:when test="$term = 'DataObjectReference'">Permet de faire référence à un objet-donnée binaire ou physique déjà présent dans les métadonnées du bordereau.</xsl:when>
                    <xsl:when test="$term = 'DataObjectVersion'">Version d’un objet-données (par exemple : original papier, conservation, diffusion, vignette, txt, …).</xsl:when>
                    <xsl:when test="$term = 'DataObjectVersionCodeListVersion'">Liste de codes correspondant aux diverses versions d'un objet-données au sein d’un groupe d'objets-données (ex. original papier, conservation, diffusion, vignette, txt).</xsl:when>
                    <xsl:when test="$term = 'Date'">Date du message.</xsl:when>
                    <xsl:when test="$term = 'DateCreatedByApplication'">Date de création du fichier.</xsl:when>
                    <xsl:when test="$term = 'DateSignature'">Date de l'objet signature.</xsl:when>
                    <xsl:when test="$term = 'DeathDate'">Date de décès d'une personne.</xsl:when>
                    <xsl:when test="$term = 'DeathPlace'">Lieu de décès d'une personne.</xsl:when>
                    <xsl:when test="$term = 'Depth'">Métadonnée de dimension physique : profondeur.</xsl:when>
                    <xsl:when test="$term = 'Derogation'">Indique si une procédure de dérogation est nécessaire avant de communiquer l’unité documentaire ArchiveUnit.</xsl:when>
                    <xsl:when test="$term = 'Description'">Description détaillée de l’ArchiveUnit. Correspond à la présentation du contenu au sens de la norme ISAD(G).</xsl:when>
                    <xsl:when test="$term = 'DescriptionLanguage'">Langue utilisée pour les informations de représentation et de pérennisation.</xsl:when>
                    <xsl:when test="$term = 'DescriptionLevel'">Niveau de description au sens de la norme ISAD (G). Indique si l’ArchiveUnit correspond à un fonds, à un sous-fonds, à une classe, à une série organique, à une sous-série organique, à un dossier, à un sous-dossier ou à une pièce.</xsl:when>
                    <xsl:when test="$term = 'DescriptiveMetadata'">Bloc de métadonnées descriptives des objets-données.</xsl:when>
                    <xsl:when test="$term = 'Diameter'">Métadonnée de dimension physique : diamètre.</xsl:when>
                    <xsl:when test="$term = 'DisseminationRule'">Gestion de la diffusion.</xsl:when>
                    <xsl:when test="$term = 'DisseminationRuleCodeListVersion'">Version des listes de codes pour les règles de diffusion.</xsl:when>
                    <xsl:when test="$term = 'Document'">Métadonnées pour un objet-données de type document.</xsl:when>
                    <xsl:when test="$term = 'DocumentType'">Type de document au sens diplomatique du terme (ex. compte-rendu de réunion, note, correspondance, etc.). Ne pas confondre avec Type.</xsl:when>
                    <xsl:when test="$term = 'Encoding'">Encodage du fichier tel que défini dans EncodingIdCodeList.</xsl:when>
                    <xsl:when test="$term = 'EncodingCodeListVersion'">Version de la liste de code d'encodage du fichier.</xsl:when>
                    <xsl:when test="$term = 'EndDate'">Date de fermeture / Date de fin.</xsl:when>
                    <xsl:when test="$term = 'Event'">Informations décrivant un événement survenu au cours d’une procédure (ex. publication d’un marché, notification d’un marché, recueil d’un avis administratif, etc.).</xsl:when>
                    <xsl:when test="$term = 'EventDateTime'">Date et heure de l'événement.</xsl:when>
                    <xsl:when test="$term = 'EventDetail'">Détail sur l'événement.</xsl:when>
                    <xsl:when test="$term = 'EventIdentifier'">Identifiant de l'événement.</xsl:when>
                    <xsl:when test="$term = 'EventType'">Type d'événement.</xsl:when>
                    <xsl:when test="$term = 'FileFormatCodeListVersion'">Version de la liste de code d'identification du format.</xsl:when>
                    <xsl:when test="$term = 'FileInfo'">Propriétés techniques génériques du fichier (nom d’origine, logiciel de création, système d’exploitation de création).</xsl:when>
                    <xsl:when test="$term = 'FilePlanPosition'">Position de l’ArchiveUnit dans le plan de classement du service producteur.</xsl:when>
                    <xsl:when test="$term = 'Filename'">Nom du fichier d'origine.</xsl:when>
                    <xsl:when test="$term = 'FinalAction'">Action à mettre en œuvre au terme de la durée de gestion.</xsl:when>
                    <xsl:when test="$term = 'FirstName'">Prénom d'une personne.</xsl:when>
                    <xsl:when test="$term = 'FormatId'">Type spécifique du format tel que défini dans FormatIdCodeList.</xsl:when>
                    <xsl:when test="$term = 'FormatIdentification'">Identification du format de l'objet-données.</xsl:when>
                    <xsl:when test="$term = 'FormatLitteral'">Forme littérale du nom du format.</xsl:when>
                    <xsl:when test="$term = 'Function'">En plus des balises Tag et Keyword, il est possible d'indexer les objets avec des éléments pré-définis : Fonction.</xsl:when>
                    <xsl:when test="$term = 'Gender'">Sexe de la personne.</xsl:when>
                    <xsl:when test="$term = 'Geogname'">En plus des balises Tag et Keyword, il est possible d'indexer les objets avec des éléments pré-définis : Nom géographique.</xsl:when>
                    <xsl:when test="$term = 'GivenName'">Nom d'usage d'une personne.</xsl:when>
                    <xsl:when test="$term = 'Gps'">Coordonnées gps complétées ou vérifiées par un utilisateur. Fait référence à des coordonnées traitées par un utilisateur et non à des coordonnées captées.</xsl:when>
                    <xsl:when test="$term = 'GpsAltitude'">Altitude de la position GPS.</xsl:when>
                    <xsl:when test="$term = 'GpsAltitudeRef'">0 (niveau de la mer) / 1 (référence au niveau de la mer - valeur négative -).</xsl:when>
                    <xsl:when test="$term = 'GpsDateStamp'">Heure et Date de la position GPS.</xsl:when>
                    <xsl:when test="$term = 'GpsLatitude'">N (Nord) / S (Sud).</xsl:when>
                    <xsl:when test="$term = 'GpsLongitude'">Latitude de la position GPS.</xsl:when>
                    <xsl:when test="$term = 'GpsLongitudeRef'">E (Est) / W (Ouest).</xsl:when>
                    <xsl:when test="$term = 'GpsVersionID'">Identifiant de la version du GPS.</xsl:when>
                    <xsl:when test="$term = 'GrantDate'">Date de prise en charge effective du transfert.</xsl:when>
                    <xsl:when test="$term = 'Height'">Métadonnée de dimension physique : hauteur.</xsl:when>
                    <xsl:when test="$term = 'Identifier'">Identifiant.</xsl:when>
                    <xsl:when test="$term = 'Image'">Métadonnées pour un objet-données de type image.</xsl:when>
                    <xsl:when test="$term = 'IsPartOf'">Est une partie de. Cette relation permet d'indique qu'un objet est une partie d'un autre.</xsl:when>
                    <xsl:when test="$term = 'IsVersionOf'">Est une version de. Edition, adaptation, traduction. Cette relation permet d'indiquer les modifications dans le contenu.</xsl:when>
                    <xsl:when test="$term = 'Juridictional'">Juridiction administrative ou ressort administratif.</xsl:when>
                    <xsl:when test="$term = 'Keyword'">Mots-clef avec contexte inspiré du SEDA 1.0. En ce qui concerne l'indexation, on pourra utiliser Tag ou Keyword en fonction de ce que l'on souhaite décrire.</xsl:when>
                    <xsl:when test="$term = 'KeywordContent'">Valeur du mot-clé. A utiliser avec Keyword.</xsl:when>
                    <xsl:when test="$term = 'KeywordReference'">Identifiant du mot clé dans un référentiel donné. Par exemple, pour un lieu, il pourrait s'agir de son code officiel géographique selon l'INSEE.</xsl:when>
                    <xsl:when test="$term = 'KeywordType'">Type de mot clé.</xsl:when>
                    <xsl:when test="$term = 'Language'">Langue du contenu des objets-données.</xsl:when>
                    <xsl:when test="$term = 'LastModified'">Date de la dernière modification du fichier.</xsl:when>
                    <xsl:when test="$term = 'Length'">Métadonnée de dimension physique : longueur.</xsl:when>
                    <xsl:when test="$term = 'Management'">Métadonnées de gestion applicables à l’ArchiveUnit concernée et à ses héritiers.</xsl:when>
                    <xsl:when test="$term = 'ManagementMetadata'">Bloc des métadonnées de gestion par défaut des objets-données.</xsl:when>
                    <xsl:when test="$term = 'Masterdata'">Référentiel des personnes et des organisations au moment de la vérification de la signature et de sa validation.</xsl:when>
                    <xsl:when test="$term = 'MessageDigest'">Empreinte de l'objet-données.</xsl:when>
                    <xsl:when test="$term = 'MessageDigestAlgorithmCodeListVersion'">Liste de l'algorithme de hachage utilisé dans le message.</xsl:when>
                    <xsl:when test="$term = 'MessageIdentifier'">Identifiant du message.</xsl:when>
                    <xsl:when test="$term = 'MessageReceivedIdentifier'">Identifiant du message reçu.</xsl:when>
                    <xsl:when test="$term = 'MessageRequestIdentifier'">Identifiant de la demande.</xsl:when>
                    <xsl:when test="$term = 'Metadata'">Propriétés techniques spécifiques du fichier en fonction de sa nature technique (texte, document, image, audio, vidéo, etc.).</xsl:when>
                    <xsl:when test="$term = 'MimeType'">Type Mime associé, potentiellement stable mais pas assez précis.</xsl:when>
                    <xsl:when test="$term = 'MimeTypeCodeListVersion'">Version de la liste de code du type Mime.</xsl:when>
                    <xsl:when test="$term = 'Nationality'">Nationalité d'une personne.</xsl:when>
                    <xsl:when test="$term = 'NeedAuthorization'">Indique si une autorisation humaine est nécessaire pour vérifier ou valider les opérations de gestion des ArchiveUnit.</xsl:when>
                    <xsl:when test="$term = 'NeedReassessingAuthorization'">Indique si une autorisation humaine est nécessaire pour réévaluer la classification.</xsl:when>
                    <xsl:when test="$term = 'NumberOfPage'">Métadonnée de dimension physique : nombre de pages.</xsl:when>
                    <xsl:when test="$term = 'ObjectGroupExtenstionAbstract'">Permet d'étendre ObjectGroup avec d'autres métadonnées descriptives.</xsl:when>
                    <xsl:when test="$term = 'OrganizationDescriptiveMetadata'">Métadonnées de description de l'organisation.</xsl:when>
                    <xsl:when test="$term = 'OriginatingAgency'">Service producteur.</xsl:when>
                    <xsl:when test="$term = 'OriginatingAgencyArchiveUnitIdentifier'">Identifiant métier attribué à l’ArchiveUnit par le service producteur.</xsl:when>
                    <xsl:when test="$term = 'OriginatingSystemId'">Identifiant système attribué à l’ArchiveUnit par l’application du service producteur.</xsl:when>
                    <xsl:when test="$term = 'OtherCodeListAbstract'">Permet d'ajouter de nouvelles listes de codes si l'ajout d'autres métadonnées l'impose.</xsl:when>
                    <xsl:when test="$term = 'OtherCoreTechnicalMetadataAbstract'">Contient toutes les métadonnées techniques de base pour d'autres types.</xsl:when>
                    <xsl:when test="$term = 'OtherDimensionsAbstract'">Permet d'étendre OtherDimensions avec d'autres métadonnées de description des objets-données physiques.</xsl:when>
                    <xsl:when test="$term = 'OtherManagementAbstract'">Utilisé par exemple pour manipuler un ArchiveUnit déjà existant dans le système d'archivage électronique.</xsl:when>
                    <xsl:when test="$term = 'OtherMetadata'">Autres métadonnées techniques si celles définies précédemment ne suffisent pas.</xsl:when>
                    <xsl:when test="$term = 'PhysicalDataObject'">Bloc de métadonnées techniques des objets-données physiques.</xsl:when>
                    <xsl:when test="$term = 'PhysicalDimensions'">Dimensions d'un objet-données physique.</xsl:when>
                    <xsl:when test="$term = 'PhysicalId'">Identifiant physique d’un objet-données physique, externe à celui-ci (ex. code-barres).</xsl:when>
                    <xsl:when test="$term = 'Position'">Intitulé du poste de travail occupé par la personne.</xsl:when>
                    <xsl:when test="$term = 'PostalCode'">En plus des balises Tag et Keyword, il est possible d'indexer les objets avec des éléments pré-définis : Code postal.</xsl:when>
                    <xsl:when test="$term = 'PreventInheritance'">Indique si les règles de gestion héritées des ArchiveUnit parents doivent être ignorées pour l’ArchiveUnit concerné.</xsl:when>
                    <xsl:when test="$term = 'ReceivedDate'">Date de réception.</xsl:when>
                    <xsl:when test="$term = 'Receiver'">Destinataire du message.</xsl:when>
                    <xsl:when test="$term = 'Recipient'">Destinataire pour information. Utilisé pour indiquer le nom du destinatire en copie, pour information, par exemple dans un courrier électronique.</xsl:when>
                    <xsl:when test="$term = 'RefNonRuleId'">Identifiant de la règle à désactiver à partir de cette ArchiveUnit.</xsl:when>
                    <xsl:when test="$term = 'ReferencedObject'">Référence à l'objet signé.</xsl:when>
                    <xsl:when test="$term = 'References'">Référence. Cette relation permet d'indiquer qu'un objet en référence un autre.</xsl:when>
                    <xsl:when test="$term = 'Region'">En plus des balises Tag et Keyword, il est possible d'indexer les objets avec des éléments pré-définis : Région.</xsl:when>
                    <xsl:when test="$term = 'RegisteredDate'">Date d'enregistrement.</xsl:when>
                    <xsl:when test="$term = 'RelatedObjectReference'">Référence à un objet faisant ou ne faisant pas partie du présent paquet d'information.</xsl:when>
                    <xsl:when test="$term = 'RelatedTransferReference'">Référence à un transfert d'archives lié.</xsl:when>
                    <xsl:when test="$term = 'Relationship'">Permet de spécifier un lien technique entre un objet-données et une signature.</xsl:when>
                    <xsl:when test="$term = 'RelationshipCodeListVersion'">Version de la liste de codes des relations.</xsl:when>
                    <xsl:when test="$term = 'Replaces'">Remplace. Cette relation permet d'indiquer les objets remplacés par le niveau courant de description.</xsl:when>
                    <xsl:when test="$term = 'ReplyCode'">Code de la réponse.</xsl:when>
                    <xsl:when test="$term = 'ReplyCodeListVersion'">Liste des codes de réponses à utiliser.</xsl:when>
                    <xsl:when test="$term = 'RepositoryArchiveUnitPID'">Référence à un ArchiveUnit déjà conservé dans un système d'archivage.</xsl:when>
                    <xsl:when test="$term = 'RepositoryObjectPID'">Référence à un un objet-données ou à un groupe d'objets-données déjà conservé(s) dans un système d'archivage.</xsl:when>
                    <xsl:when test="$term = 'RequestDate'">Date de la demande d'autorisation.</xsl:when>
                    <xsl:when test="$term = 'Requester'">Demandeur.</xsl:when>
                    <xsl:when test="$term = 'Requires'">Requiert. Cette relation permet d'indiquer les objets nécessaire à la compréhension du niveau courant de description.</xsl:when>
                    <xsl:when test="$term = 'ReuseRule'">Gestion de la réutilisation.</xsl:when>
                    <xsl:when test="$term = 'ReuseRuleCodeListVersion'">Version des listes de codes pour les règles de réutilisation.</xsl:when>
                    <xsl:when test="$term = 'Role'">Droits avec lesquels un utilisateur a réalisé une opération, notamment dans une application.</xsl:when>
                    <xsl:when test="$term = 'Rule'">Référence à la règle.</xsl:when>
                    <xsl:when test="$term = 'RuleId'">Identifiant de la règle</xsl:when>
                    <xsl:when test="$term = 'Sender'">Expéditeur du message.</xsl:when>
                    <xsl:when test="$term = 'SentDate'">Date d'envoi.</xsl:when>
                    <xsl:when test="$term = 'ServiceLevel'">Niveau de service applicable aux unités d’archives.</xsl:when>
                    <xsl:when test="$term = 'Shape'">Métadonnée de dimension physique : forme.</xsl:when>
                    <xsl:when test="$term = 'Signature'">Contient toutes les informations relatives à la signature.</xsl:when>
                    <xsl:when test="$term = 'SignedObjectDigest'">Empreinte obligatoire jusqu'au processus de versement pour assurer la portabilité de la valeur probante. Le SAE peut ne pas la conserver si l'on considère que l'identifiant de l'objet correspondant suffit. Ce procédé permet de résister au temps lorsque les informations binaires du paquet seront converties au gré des opérations de préservation de la lisibilité des formats. Au cours de ces opérations, l'identifiant ne changera pas, contrairement au format dufichier et donc à son empreinte.</xsl:when>
                    <xsl:when test="$term = 'SignedObjectId'">Identifiant de l'objet-données signé.</xsl:when>
                    <xsl:when test="$term = 'Signer'">Signataire(s) de la transaction ou de l'objet.</xsl:when>
                    <xsl:when test="$term = 'SigningTime'">Date de signature.</xsl:when>
                    <xsl:when test="$term = 'Size'">Permet de spécifier la taille de l'objet-données en octet.</xsl:when>
                    <xsl:when test="$term = 'Source'">En cas de substitution numérique, permet de faire référence au papier.</xsl:when>
                    <xsl:when test="$term = 'Spatial'">Couverture spatiale ou couverture géographique.</xsl:when>
                    <xsl:when test="$term = 'StartDate'">Date de départ de calcul.</xsl:when>
                    <xsl:when test="$term = 'Status'">Etat de l'objet-données (par rapport avec son cycle de vie). Permet par exemple d'indiquer si la signature du fichier a été vérifiée avant le transfert aux archives.</xsl:when>
                    <xsl:when test="$term = 'StorageRule'">Gestion de la durée d’utilité courante.</xsl:when>
                    <xsl:when test="$term = 'StorageRuleCodeListVersion'">Version des listes de codes pour les règles de durée d'utilité courante.</xsl:when>
                    <xsl:when test="$term = 'SubmissionAgency'">Service versant responsable du transfert des données.</xsl:when>
                    <xsl:when test="$term = 'SystemId'">Identifiant attribué aux objets. Il est attribué par le SAE et correspond à un identifiant interne.</xsl:when>
                    <xsl:when test="$term = 'Tag'">Mots-clés ou liste de mots-clés génériques. En ce qui concerne l'indexation, on pourra utiliser Tag ou Keyword en fonction de ce que l'on souhaite décrire.</xsl:when>
                    <xsl:when test="$term = 'Temporal'">Couverture temporelle.</xsl:when>
                    <xsl:when test="$term = 'Text'">Métadonnées pour un objet-données de type textuel.</xsl:when>
                    <xsl:when test="$term = 'Thickness'">Métadonnée de dimension physique : épaisseur.</xsl:when>
                    <xsl:when test="$term = 'Title'">Intitulé de l'ArchiveUnit.</xsl:when>
                    <xsl:when test="$term = 'TransactedDate'">Date de la transaction.</xsl:when>
                    <xsl:when test="$term = 'TransferDate'">Date de transfert.</xsl:when>
                    <xsl:when test="$term = 'TransferRequestReplyIdentifier'">Identifiant de la réponse à une demande de transfert.</xsl:when>
                    <xsl:when test="$term = 'TransferringAgency'">Service versant.</xsl:when>
                    <xsl:when test="$term = 'TransferringAgencyArchiveUnitIdentifier'">Identifiant attribué à l'ArchiveUnit par le service versant.</xsl:when>
                    <xsl:when test="$term = 'Type'">Type d’information au sens de l’OAIS (information de représentation, information de pérennisation, etc.).</xsl:when>
                    <xsl:when test="$term = 'UnitIdentifier'">Identifiant de l'unité documentaire.</xsl:when>
                    <xsl:when test="$term = 'Uri'">L'URI spécifie où se trouve l'objet-données numérique. Peut correspondre à un chemin relatif.</xsl:when>
                    <xsl:when test="$term = 'ValidationTime'">Date de la validation de la signature.</xsl:when>
                    <xsl:when test="$term = 'Validator'">Validateur de la signature.</xsl:when>
                    <xsl:when test="$term = 'Version'">Permet d'indiquer quelle est la version du document.</xsl:when>
                    <xsl:when test="$term = 'Video'">Métadonnées pour un objet-données de type vidéo.</xsl:when>
                    <xsl:when test="$term = 'Weight'">Métadonnée de dimension physique : poids.</xsl:when>
                    <xsl:when test="$term = 'Width'">Métadonnée de dimension physique : largeur.</xsl:when>
                    <xsl:when test="$term = 'Writer'">Rédacteur Rédacteur de l’ArchiveUnit.</xsl:when>
                    <xsl:otherwise>
                        Pas de documentation sur <xsl:value-of select="$term"/>
                    </xsl:otherwise>
                </xsl:choose>
            </span>
        </div>
    </xsl:template>

    <xsl:template name="traduction">
        <xsl:param name="term"></xsl:param>
        <xsl:choose>
            <xsl:when test="$term = 'AccessRule'">Gestion de la communicabilité</xsl:when>
            <xsl:when test="$term = 'AccessRuleCodeListVersion'">Version codes communicabilité</xsl:when>
            <xsl:when test="$term = 'Acknowledgement'">Accusé de réception</xsl:when>
            <xsl:when test="$term = 'AcquiredDate'">Date de numérisation.</xsl:when>
            <xsl:when test="$term = 'Activity'">Activité</xsl:when>
            <xsl:when test="$term = 'Address'">Adresse</xsl:when>
            <xsl:when test="$term = 'Addressee'">Destinataire pour action</xsl:when>
            <xsl:when test="$term = 'AppraisalRule'">Gestion de la durée d’utilité administrative</xsl:when>
            <xsl:when test="$term = 'AppraisalRuleCodeListVersion'">Version codes durée d'utilité administrative</xsl:when>
            <xsl:when test="$term = 'ArchivalAgency'">Service d'archives</xsl:when>
            <xsl:when test="$term = 'ArchivalAgencyArchiveUnitIdentifier'">Identifiant métier</xsl:when>
            <xsl:when test="$term = 'ArchivalAgreement'">Accord de service</xsl:when>
            <xsl:when test="$term = 'ArchivalProfile'">Profil d’archivage</xsl:when>
            <xsl:when test="$term = 'ArchiveDeliveryRequest'">Demande de communication d'archives</xsl:when>
            <xsl:when test="$term = 'ArchiveDeliveryRequestReply'">Réponse à une demande de communication d'archives</xsl:when>
            <xsl:when test="$term = 'ArchiveDestructionNotification'">Notification d'élimination d'archives</xsl:when>
            <xsl:when test="$term = 'ArchiveModificationNotification'">Notification de modification d'archives</xsl:when>
            <xsl:when test="$term = 'ArchiveRestitutionRequest'">Demande de restitution</xsl:when>
            <xsl:when test="$term = 'ArchiveRestitutionRequestReply'">Réponse à une demande de restitution d'archives</xsl:when>
            <xsl:when test="$term = 'ArchiveTransfer'">Transfert d'archives</xsl:when>
            <xsl:when test="$term = 'ArchiveTransferReply'">Réponse à un transfert d'archives</xsl:when>
            <xsl:when test="$term = 'ArchiveTransferRequest'">Demande de transfert d’archives</xsl:when>
            <xsl:when test="$term = 'ArchiveTransferRequestReply'">Réponse à une demande de transfert d’archives</xsl:when>
            <xsl:when test="$term = 'ArchiveUnit'">Archive</xsl:when>
            <xsl:when test="$term = 'ArchiveUnitProfile'">Référence à une partie d'un profil d’archivage</xsl:when>
            <xsl:when test="$term = 'ArchiveUnitRefId'">Référence à d'autres archives</xsl:when>
            <xsl:when test="$term = 'Attachment'">Fichier joint</xsl:when>
            <xsl:when test="$term = 'Audio'">Métadonnées audio</xsl:when>
            <xsl:when test="$term = 'AuthorizationControlAuthorityRequest'">Demande d'autorisation au service de contrôle</xsl:when>
            <xsl:when test="$term = 'AuthorizationControlAuthorityRequestReply'">Réponse à une demande d'autorisation au service de contrôle</xsl:when>
            <xsl:when test="$term = 'AuthorizationOriginatingAgencyRequest'">Demande d'autorisation au service producteur</xsl:when>
            <xsl:when test="$term = 'AuthorizationOriginatingAgencyRequestReply'">Réponse à une demande d'autorisation au service producteur</xsl:when>
            <xsl:when test="$term = 'AuthorizationReason'">Motif de l'autorisation</xsl:when>
            <xsl:when test="$term = 'AuthorizationReasonCodeListVersion'">Version de la liste de codes d'autorisation</xsl:when>
            <xsl:when test="$term = 'AuthorizationRequestContent'">Demande d’autorisation</xsl:when>
            <xsl:when test="$term = 'AuthorizationRequestReply'">Réponse à la demande d’autorisation</xsl:when>
            <xsl:when test="$term = 'AuthorizationRequestReplyIdentifier'">Identifiant de la réponse à une demande d’autorisation</xsl:when>
            <xsl:when test="$term = 'AuthorizedAgent'">Titulaire des droits de propriété intellectuelle</xsl:when>
            <xsl:when test="$term = 'BinaryDataObject'">Métadonnées techniques numériques</xsl:when>
            <xsl:when test="$term = 'BirthDate'">Date de naissance</xsl:when>
            <xsl:when test="$term = 'BirthName'">Nom de naissance</xsl:when>
            <xsl:when test="$term = 'BirthPlace'">Lieu de naissance</xsl:when>
            <xsl:when test="$term = 'City'">Ville</xsl:when>
            <xsl:when test="$term = 'ClassificationLevel'">Référence au niveau de classification</xsl:when>
            <xsl:when test="$term = 'ClassificationOwner'">Propriétaire de la classification</xsl:when>
            <xsl:when test="$term = 'ClassificationReassessingDate'">Date de réévaluation de la classification</xsl:when>
            <xsl:when test="$term = 'ClassificationRule'">Gestion de la classification</xsl:when>
            <xsl:when test="$term = 'ClassificationRuleCodeListVersion'">Version des listes de codes pour les règles de classification</xsl:when>
            <xsl:when test="$term = 'CodeListVersions'">Codes de références</xsl:when>
            <xsl:when test="$term = 'Comment'">Commentaire</xsl:when>
            <xsl:when test="$term = 'Compressed'">Est compressé</xsl:when>
            <xsl:when test="$term = 'CompressionAlgorithmCodeListVersion'">Version code de l'algorithme de compression</xsl:when>
            <xsl:when test="$term = 'Content'">Métadonnées de description</xsl:when>
            <xsl:when test="$term = 'ControlAuthority'">Autorité de contrôle</xsl:when>
            <xsl:when test="$term = 'Corpname'">Nom d'une entité</xsl:when>
            <xsl:when test="$term = 'Country'">Pays</xsl:when>
            <xsl:when test="$term = 'Coverage'">Couverture spatiale, temporelle ou juridictionnelle</xsl:when>
            <xsl:when test="$term = 'CreatedDate'">Date de création</xsl:when>
            <xsl:when test="$term = 'CreatingApplicationName'">Application utilisée pour créer le fichier</xsl:when>
            <xsl:when test="$term = 'CreatingApplicationVersion'">Version de l'application utilisée pour créer le fichier</xsl:when>
            <xsl:when test="$term = 'CreatingOs'">Système d’exploitation utilisé pour créer le fichier</xsl:when>
            <xsl:when test="$term = 'CreatingOsVersion'">Version du système d'exploitation utilisé pour créer le fichier</xsl:when>
            <xsl:when test="$term = 'CustodialHistory'">Énumère les changements successifs de propriété</xsl:when>
            <xsl:when test="$term = 'CustodialHistoryFile'">Référence à un fichier de journalisation externe</xsl:when>
            <xsl:when test="$term = 'CustodialHistoryItem'">Description d'un événement précis dans l'historique</xsl:when>
            <xsl:when test="$term = 'DataObjectGroupId'">Référence à un groupe</xsl:when>
            <xsl:when test="$term = 'DataObjectGroupReferenceId'">Identifiant du groupe</xsl:when>
            <xsl:when test="$term = 'DataObjectId'">Référence</xsl:when>
            <xsl:when test="$term = 'DataObjectPackage'">Objets-données échangés dans le message</xsl:when>
            <xsl:when test="$term = 'DataObjectReference'">Référence à un objet-donnée</xsl:when>
            <xsl:when test="$term = 'DataObjectVersion'">Version d’un objet-données</xsl:when>
            <xsl:when test="$term = 'DataObjectVersionCodeListVersion'">Liste de codes versions d'un objet-données</xsl:when>
            <xsl:when test="$term = 'Date'">Date</xsl:when>
            <xsl:when test="$term = 'DateCreatedByApplication'">Date de création du fichier</xsl:when>
            <xsl:when test="$term = 'DateSignature'">Date de l'objet signature</xsl:when>
            <xsl:when test="$term = 'DeathDate'">Date de décès</xsl:when>
            <xsl:when test="$term = 'DeathPlace'">Lieu de décès</xsl:when>
            <xsl:when test="$term = 'Depth'">Profondeur</xsl:when>
            <xsl:when test="$term = 'Derogation'">Procédure de dérogation nécessaire</xsl:when>
            <xsl:when test="$term = 'Description'">Description</xsl:when>
            <xsl:when test="$term = 'DescriptionLanguage'">Langue utilisée</xsl:when>
            <xsl:when test="$term = 'DescriptionLevel'">Niveau de description</xsl:when>
            <xsl:when test="$term = 'DescriptiveMetadata'">Métadonnées descriptives</xsl:when>
            <xsl:when test="$term = 'Diameter'">Diamètre</xsl:when>
            <xsl:when test="$term = 'DisseminationRule'">Gestion de la diffusion</xsl:when>
            <xsl:when test="$term = 'DisseminationRuleCodeListVersion'">Version codes règles de diffusion</xsl:when>
            <xsl:when test="$term = 'Document'">Métadonnées document</xsl:when>
            <xsl:when test="$term = 'DocumentType'">Type de document</xsl:when>
            <xsl:when test="$term = 'Encoding'">Encodage du fichier</xsl:when>
            <xsl:when test="$term = 'EncodingCodeListVersion'">Version code d'encodage du fichier</xsl:when>
            <xsl:when test="$term = 'EndDate'">Date de fin</xsl:when>
            <xsl:when test="$term = 'Event'">Evénement survenu au cours d’une procédure</xsl:when>
            <xsl:when test="$term = 'EventDateTime'">Date et heure de l'événement</xsl:when>
            <xsl:when test="$term = 'EventDetail'">Détail sur l'événement</xsl:when>
            <xsl:when test="$term = 'EventIdentifier'">Identifiant de l'événement</xsl:when>
            <xsl:when test="$term = 'EventType'">Type d'événement</xsl:when>
            <xsl:when test="$term = 'FileFormatCodeListVersion'">Version code d'identification du format</xsl:when>
            <xsl:when test="$term = 'FileInfo'">Propriétés techniques du fichier</xsl:when>
            <xsl:when test="$term = 'FilePlanPosition'">Position de l’ArchiveUnit dans le plan de classement du service producteur</xsl:when>
            <xsl:when test="$term = 'Filename'">Nom du fichier d'origine</xsl:when>
            <xsl:when test="$term = 'FinalAction'">Action terme de la durée de gestion</xsl:when>
            <xsl:when test="$term = 'FirstName'">Prénom</xsl:when>
            <xsl:when test="$term = 'FormatId'">Type du format</xsl:when>
            <xsl:when test="$term = 'FormatIdentification'">Identification du format</xsl:when>
            <xsl:when test="$term = 'FormatLitteral'">Nom du format</xsl:when>
            <xsl:when test="$term = 'Function'">Fonction</xsl:when>
            <xsl:when test="$term = 'Gender'">Sexe</xsl:when>
            <xsl:when test="$term = 'Geogname'">Nom géographique</xsl:when>
            <xsl:when test="$term = 'GivenName'">Nom d'usage</xsl:when>
            <xsl:when test="$term = 'Gps'">Coordonnées gps</xsl:when>
            <xsl:when test="$term = 'GpsAltitude'">Altitude GPS</xsl:when>
            <xsl:when test="$term = 'GpsAltitudeRef'">Référence au niveau de la mer</xsl:when>
            <xsl:when test="$term = 'GpsDateStamp'">Heure et Date de la position GPS</xsl:when>
            <xsl:when test="$term = 'GpsLatitude'">N (Nord) / S (Sud)</xsl:when>
            <xsl:when test="$term = 'GpsLongitude'">Latitude GPS</xsl:when>
            <xsl:when test="$term = 'GpsLongitudeRef'">E (Est) / W (Ouest)</xsl:when>
            <xsl:when test="$term = 'GpsVersionID'">Version du GPS</xsl:when>
            <xsl:when test="$term = 'GrantDate'">Date de prise en charge effective du transfert</xsl:when>
            <xsl:when test="$term = 'Height'">Hauteur</xsl:when>
            <xsl:when test="$term = 'Identifier'">Identifiant</xsl:when>
            <xsl:when test="$term = 'Image'">Métadonnées image</xsl:when>
            <xsl:when test="$term = 'IsPartOf'">Est une partie de</xsl:when>
            <xsl:when test="$term = 'IsVersionOf'">Est une version de</xsl:when>
            <xsl:when test="$term = 'Juridictional'">Juridiction administrative</xsl:when>
            <xsl:when test="$term = 'Keyword'">Mots-clef avec contexte</xsl:when>
            <xsl:when test="$term = 'KeywordContent'">Valeur du mot-clé</xsl:when>
            <xsl:when test="$term = 'KeywordReference'">Identifiant du mot clé</xsl:when>
            <xsl:when test="$term = 'KeywordType'">Type de mot clé</xsl:when>
            <xsl:when test="$term = 'Language'">Langue du contenu</xsl:when>
            <xsl:when test="$term = 'LastModified'">Date de la dernière modification</xsl:when>
            <xsl:when test="$term = 'Length'">Longueur</xsl:when>
            <xsl:when test="$term = 'Management'">Métadonnées de gestion</xsl:when>
            <xsl:when test="$term = 'ManagementMetadata'">Métadonnées de gestion</xsl:when>
            <xsl:when test="$term = 'Masterdata'">Référentiel</xsl:when>
            <xsl:when test="$term = 'MessageDigest'">Empreinte</xsl:when>
            <xsl:when test="$term = 'MessageDigestAlgorithmCodeListVersion'">Liste de l'algorithme de hachage</xsl:when>
            <xsl:when test="$term = 'MessageIdentifier'">Identifiant du message</xsl:when>
            <xsl:when test="$term = 'MessageReceivedIdentifier'">Identifiant du message reçu</xsl:when>
            <xsl:when test="$term = 'MessageRequestIdentifier'">Identifiant de la demande</xsl:when>
            <xsl:when test="$term = 'Metadata'">Propriétés techniques</xsl:when>
            <xsl:when test="$term = 'MimeType'">Type Mime</xsl:when>
            <xsl:when test="$term = 'MimeTypeCodeListVersion'">Version code du type Mime</xsl:when>
            <xsl:when test="$term = 'Nationality'">Nationalité</xsl:when>
            <xsl:when test="$term = 'NeedAuthorization'">Autorisation humaine nécessaire</xsl:when>
            <xsl:when test="$term = 'NeedReassessingAuthorization'">Autorisation humaine nécessaire</xsl:when>
            <xsl:when test="$term = 'NumberOfPage'">Nombre de pages</xsl:when>
            <xsl:when test="$term = 'OrganizationDescriptiveMetadata'">Métadonnées de description de l'organisation</xsl:when>
            <xsl:when test="$term = 'OriginatingAgency'">Service producteur</xsl:when>
            <xsl:when test="$term = 'OriginatingAgencyArchiveUnitIdentifier'">Identifiant métier attribué par le service producteur</xsl:when>
            <xsl:when test="$term = 'OriginatingSystemId'">Identifiant système attribué l’application du service producteur</xsl:when>
            <xsl:when test="$term = 'OtherMetadata'">Autres métadonnées techniques</xsl:when>
            <xsl:when test="$term = 'PhysicalDataObject'">Métadonnées techniques physiques</xsl:when>
            <xsl:when test="$term = 'PhysicalDimensions'">Dimensions</xsl:when>
            <xsl:when test="$term = 'PhysicalId'">Identifiant physique</xsl:when>
            <xsl:when test="$term = 'Position'">Intitulé du poste de travail</xsl:when>
            <xsl:when test="$term = 'PostalCode'">Code postal</xsl:when>
            <xsl:when test="$term = 'PreventInheritance'">Les règles héritées doivent être ignorées</xsl:when>
            <xsl:when test="$term = 'ReceivedDate'">Date de réception</xsl:when>
            <xsl:when test="$term = 'Receiver'">Destinataire (A)</xsl:when>
            <xsl:when test="$term = 'Recipient'">Destinataire pour information (CC)</xsl:when>
            <xsl:when test="$term = 'RefNonRuleId'">Identifiant de la règle à désactiver</xsl:when>
            <xsl:when test="$term = 'ReferencedObject'">Référence à l'objet signé</xsl:when>
            <xsl:when test="$term = 'References'">Référence</xsl:when>
            <xsl:when test="$term = 'Region'">Région</xsl:when>
            <xsl:when test="$term = 'RegisteredDate'">Date d'enregistrement</xsl:when>
            <xsl:when test="$term = 'RelatedObjectReference'">Référence à un objet</xsl:when>
            <xsl:when test="$term = 'RelatedTransferReference'">Référence à un transfert d'archives lié</xsl:when>
            <xsl:when test="$term = 'Relationship'">Lien technique</xsl:when>
            <xsl:when test="$term = 'RelationshipCodeListVersion'">Version codes des relations</xsl:when>
            <xsl:when test="$term = 'Replaces'">Remplace</xsl:when>
            <xsl:when test="$term = 'ReplyCode'">Code réponse</xsl:when>
            <xsl:when test="$term = 'ReplyCodeListVersion'">Codes de réponses à utiliser</xsl:when>
            <xsl:when test="$term = 'RepositoryArchiveUnitPID'">Référence à un ArchiveUnit déjà conservé dans un système d'archivage.</xsl:when>
            <xsl:when test="$term = 'RepositoryObjectPID'">Référence à un un objet-données ou à un groupe d'objets-données déjà conservé(s) dans un système d'archivage</xsl:when>
            <xsl:when test="$term = 'RequestDate'">Date de la demande d'autorisation</xsl:when>
            <xsl:when test="$term = 'Requester'">Demandeur</xsl:when>
            <xsl:when test="$term = 'Requires'">Requiert</xsl:when>
            <xsl:when test="$term = 'ReuseRule'">Gestion de la réutilisation</xsl:when>
            <xsl:when test="$term = 'ReuseRuleCodeListVersion'">Version codes règles de réutilisation</xsl:when>
            <xsl:when test="$term = 'Rule'">Règle</xsl:when>
            <xsl:when test="$term = 'RuleId'">Identifiant de la règle</xsl:when>
            <xsl:when test="$term = 'Sender'">Expéditeur</xsl:when>
            <xsl:when test="$term = 'SentDate'">Date d'envoi</xsl:when>
            <xsl:when test="$term = 'ServiceLevel'">Niveau de service</xsl:when>
            <xsl:when test="$term = 'Shape'">Forme</xsl:when>
            <xsl:when test="$term = 'Signature'">Signature</xsl:when>
            <xsl:when test="$term = 'SignedObjectDigest'">Empreinte</xsl:when>
            <xsl:when test="$term = 'SignedObjectId'">Identifiant</xsl:when>
            <xsl:when test="$term = 'Signer'">Signataire(s)</xsl:when>
            <xsl:when test="$term = 'SigningTime'">Date de signature</xsl:when>
            <xsl:when test="$term = 'Size'">Taille en octet</xsl:when>
            <xsl:when test="$term = 'Source'">Référence au papier</xsl:when>
            <xsl:when test="$term = 'Spatial'">Couverture spatiale</xsl:when>
            <xsl:when test="$term = 'StartDate'">Date de départ</xsl:when>
            <xsl:when test="$term = 'Status'">Etat</xsl:when>
            <xsl:when test="$term = 'StorageRule'">Gestion de la durée d’utilité courante</xsl:when>
            <xsl:when test="$term = 'StorageRuleCodeListVersion'">Version codes règles de durée d'utilité courante</xsl:when>
            <xsl:when test="$term = 'SubmissionAgency'">Service versant</xsl:when>
            <xsl:when test="$term = 'SystemId'">Identifiant attribué aux objets</xsl:when>
            <xsl:when test="$term = 'Tag'">Mots-clés</xsl:when>
            <xsl:when test="$term = 'Temporal'">Couverture temporelle</xsl:when>
            <xsl:when test="$term = 'Text'">Métadonnées textuel</xsl:when>
            <xsl:when test="$term = 'Thickness'">Epaisseur</xsl:when>
            <xsl:when test="$term = 'Title'">Intitulé</xsl:when>
            <xsl:when test="$term = 'TransactedDate'">Date de la transaction</xsl:when>
            <xsl:when test="$term = 'TransferDate'">Date de transfert</xsl:when>
            <xsl:when test="$term = 'TransferRequestReplyIdentifier'">Identifiant réponse à une demande de transfert</xsl:when>
            <xsl:when test="$term = 'TransferringAgency'">Service versant</xsl:when>
            <xsl:when test="$term = 'TransferringAgencyArchiveUnitIdentifier'">Identifiant attribué à l'ArchiveUnit par le service versant</xsl:when>
            <xsl:when test="$term = 'Type'">Type d’information</xsl:when>
            <xsl:when test="$term = 'UnitIdentifier'">Identifiant document</xsl:when>
            <xsl:when test="$term = 'Uri'">URI</xsl:when>
            <xsl:when test="$term = 'ValidationTime'">Date validation de la signature</xsl:when>
            <xsl:when test="$term = 'Validator'">Validateur de la signature</xsl:when>
            <xsl:when test="$term = 'Version'">Version du document</xsl:when>
            <xsl:when test="$term = 'Video'">Métadonnées vidéo</xsl:when>
            <xsl:when test="$term = 'Weight'">Métadonnée poids</xsl:when>
            <xsl:when test="$term = 'Width'">Métadonnée largeur</xsl:when>
            <xsl:when test="$term = 'Writer'">Rédacteur</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$term"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>