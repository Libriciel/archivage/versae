<?php

/**
 * Versae\Utility\Check
 */

namespace Versae\Utility;

use AsalaeCore\Factory\Utility;
use AsalaeCore\Form\ConfigurationForm;
use AsalaeCore\Http\Client;
use AsalaeCore\Utility\Check as CoreCheck;
use AsalaeCore\Utility\Config;
use AsalaeCore\Utility\LimitBreak;
use Cake\Core\App;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Exception;
use Versae\Model\Entity\ArchivingSystem;

/**
 * Vérification de l'application
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Check extends CoreCheck
{
    /**
     * @var array
     */
    protected static $archivingSystems;
    /**
     * @var array
     */
    protected static $treeSanity;

    /**
     * Vrai si la configuration (app_local.json) est valide
     * @return array
     */
    public static function configurationErrors(): array
    {
        $form = new ConfigurationForm();
        $map = array_flip($form::CONFIG_MAP);
        $config = Hash::flatten(Config::readLocal());
        $data = ['config' => file_get_contents(Config::getPathToLocal())];
        foreach ($config as $key => $value) {
            if (isset($map[$key])) {
                $data[$map[$key]] = $value;
            }
        }
        return $form->validate($data) ? [] : $form->getErrors();
    }

    /**
     * Remise à zéro du cache pour les tests
     */
    public static function reset(): void
    {
        static::$archivingSystems = null;
    }

    /**
     * Liste des workers et comparaison avec la quantité en base
     * @return array
     * @throws Exception
     * @deprecated
     */
    public static function workers(): array
    {
        return [];
    }

    /**
     * Faux si le seda generator ne répond pas au ping
     * ou n'est pas configuré
     * @return array
     * @throws Exception
     */
    public static function sedaGeneratorStatus(): array
    {
        if (empty(Configure::read('SedaGenerator.url'))) {
            return [
                'success' => false,
                'warning' => true,
                'info' => __("Seda Generator non configuré"),
            ];
        }

        /** @var Client $client */
        $client = Utility::get(Client::class);
        try {
            $response = $client->get(Configure::read('SedaGenerator.url') . '/ping');
            $data = json_decode($response->getStringBody(), true);
            return [
                'success' => $data['success'],
                'warning' => false,
                'info' => '',
            ];
        } catch (Exception $e) {
            return [
                'success' => false,
                'warning' => false,
                'info' => $e->getMessage(),
            ];
        }
    }

    /**
     * Vérifie la réponse des SAE utilisés dans des forms actifs
     * @return array
     */
    public static function archivingSystems(): array
    {
        if (isset(static::$archivingSystems)) {
            return static::$archivingSystems;
        }

        static::$archivingSystems = TableRegistry::getTableLocator()->get('ArchivingSystems')->find()
            ->where(['active' => true])
            ->all()
            ->each(
                function (EntityInterface $entity) {
                    $success = true;
                    try {
                        /** @var ArchivingSystem $entity */
                        $entity->whoami();
                    } catch (Exception) {
                        $success = false;
                    }
                    $entity->set('success', $success);
                    return $entity;
                }
            )
            ->toArray();

        return static::$archivingSystems;
    }

    /**
     * Faux si un des SAE ne répond
     * @return bool
     */
    public static function archivingSystemsOK(): bool
    {
        foreach (self::archivingSystems() as $as) {
            if (!$as->get('success')) {
                return false;
            }
        }
        return true;
    }

    /**
     * Vrai si les lft et rght sont cohérents
     * @return array
     */
    public static function treeSanity(): array
    {
        if (!isset(static::$treeSanity)) {
            static::$treeSanity = static::getTreeSanity();
        }
        return static::$treeSanity;
    }

    /**
     * Vrai si les lft et rght sont cohérents
     * @return array
     */
    private static function getTreeSanity(): array
    {
        $loc = TableRegistry::getTableLocator();
        $paths = App::classPath('Model/Table');
        $models = glob($paths[0] . '*Table.php');
        $results = [];
        foreach ($models as $file) {
            $name = basename($file, 'Table.php');
            $model = $loc->get($name);
            if ($model && $model->hasBehavior('Tree')) {
                $tree = $model->getBehavior('Tree');
                $table = $model->getTable();
                $alias = $model->getAlias();
                $lft = $tree->getConfig('left');
                $rght = $tree->getConfig('right');
                $count = $model->find()->count();
                $requiredMemory = $count * 300; // 261977704 pour 1.3M de lignes, soit 200 * 1.3M, 300 on est large
                LimitBreak::setMemoryLimit($requiredMemory);

                $conn = $model->getConnection();
                $stmt = $conn->prepare("SELECT $lft, $rght FROM $table ORDER BY $lft");
                $stmt->execute();

                $lftsRghts = [];
                while ($data = $stmt->fetch('assoc')) {
                    if (isset($lftsRghts[$data[$lft]])) {
                        $results[$alias] = [
                            'success' => false,
                            'message' => __("lft en double ({0})", $data[$lft]),
                        ];
                        continue 2;
                    }
                    if (isset($lftsRghts[$data[$rght]])) {
                        $results[$alias] = [
                            'success' => false,
                            'message' => __("rght en double ({0})", $data[$rght]),
                        ];
                        continue 2;
                    }
                    if ($data[$lft] >= $data[$rght]) {
                        $results[$alias] = [
                            'success' => false,
                            'message' => __("lft / rght inversés, ({0}, {1})", $data[$lft], $data[$rght]),
                        ];
                        continue 2;
                    }
                    $lftsRghts[$data[$lft]] = true;
                    $lftsRghts[$data[$rght]] = true;
                }
                $results[$alias] = [
                    'success' => true,
                    'message' => __("OK"),
                ];
            }
        }
        return $results;
    }

    /**
     * Vérifi la cohérence des données issue d'un treeBehavior
     * @return bool
     */
    public static function treeSanityOk(): bool
    {
        foreach (self::treeSanity() as $arr) {
            if (!$arr['success']) {
                return false;
            }
        }
        return true;
    }
}
