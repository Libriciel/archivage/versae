<?php

/**
 * Versae\Utility\Twig
 */

namespace Versae\Utility;

use AsalaeCore\Utility\Translit;
use buzzingpixel\twigswitch\SwitchTwigExtension;
use DateTime;
use Throwable;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Extension\SandboxExtension;
use Twig\Extra\Intl\IntlExtension;
use Twig\Loader\ArrayLoader;
use Twig\Sandbox\SecurityPolicy;
use Twig\Source;
use Twig\TwigFilter;
use Versae\Error\Debug\HtmlFormatter;

/**
 * Manipulation de twig
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Twig
{
    public const SANDBOX_TAGS = ['if', 'for', 'switch'];
    public const SANDBOX_FILTERS = [
        'abs',
        'capitalize',
        'convert_encoding',
        'date',
        'date_modify',
        'default',
        'escape',
        'first',
        'format_datetime',
        'format_number',
        'join',
        'last',
        'length',
        'lower',
        'remove_accent',
        'round',
        'slice',
        'title',
        'trim',
        'upper',
    ];
    public const SANDBOX_METHODS = [/*'GetThings' => ['doStuff']*/];
    public const SANDBOX_PROPERTIES = [/*'GetThings' => ['id', 'data']*/];
    public const SANDBOX_FUNCTIONS = [/*'range'*/];

    public const KEY_TAGS = 'allowedTags';
    public const KEY_FILTERS = 'allowedFilters';
    public const KEY_METHODS = 'allowedMethods';
    public const KEY_PROPERTIES = 'allowedProperties';
    public const KEY_FUNCTIONS = 'allowedFunctions';

    public const TOKEN_PROG_LIST = [
        'if', 'for', 'switch', 'else', 'elseif', 'endif', 'endfor', 'case',
        'default', 'endswitch', 'true', 'false', 'null'
    ];
    public const NL_REPLACE = '§-%§%-§';

    /**
     * Donne l'instance de twig
     * @param string $tmpl
     * @param bool   $sandbox
     * @param array  $sandboxPolicy
     * @return Environment
     */
    public static function getTwig(
        string $tmpl,
        bool $sandbox = true,
        array $sandboxPolicy = []
    ): Environment {
        $loader = new ArrayLoader(['index' => $tmpl]);
        $twig = new Environment($loader);
        $twig->addExtension(new IntlExtension());
        $twig->addExtension(new SwitchTwigExtension());
        $twig->addFilter(
            new TwigFilter('remove_accent', fn ($s) => Translit::ascii($s ?: ''))
        );
        $twig->addFilter(
            new TwigFilter('abs', fn ($s) => is_numeric($s) ? abs($s) : 0)
        );
        $twig->addFilter( // round plus permissif que celui de twig
            new TwigFilter(
                'round',
                function ($value, ...$args) {
                    if (!is_numeric($value)) {
                        return 0;
                    }
                    $precision = $args[0] ?? 0;
                    $method = in_array($args[1] ?? null, ['ceil', 'floor']) ? $args[1] : 'common';

                    // ↓↓↓ copie de la méthode @internal: Twig\Extension\CoreExtension::round() ↓↓↓
                    $value = (float) $value;

                    if ('common' === $method) {
                        return round($value, $precision);
                    }

                    if ('ceil' !== $method && 'floor' !== $method) {
                        throw new RuntimeError(
                            'The round filter only supports the "common", "ceil", and "floor" methods.'
                        );
                    }

                    return $method($value * 10 ** $precision) / 10 ** $precision;
                }
            )
        );

        if ($sandbox) {
            $policy = new SecurityPolicy(
                $sandboxPolicy[self::KEY_TAGS] ?? self::SANDBOX_TAGS,
                $sandboxPolicy[self::KEY_FILTERS] ?? self::SANDBOX_FILTERS,
                $sandboxPolicy[self::KEY_METHODS] ?? self::SANDBOX_METHODS,
                $sandboxPolicy[self::KEY_PROPERTIES] ?? self::SANDBOX_PROPERTIES,
                $sandboxPolicy[self::KEY_FUNCTIONS] ?? self::SANDBOX_FUNCTIONS
            );
            $twig->addExtension(new SandboxExtension($policy, true));
        }
        return $twig;
    }

    /**
     * Effectue le rendu d'un template twig
     * @param string $tmpl
     * @param array  $vars
     * @param bool   $sandbox
     * @param array  $sandboxPolicy
     * @return string
     * @throws LoaderError|RuntimeError|SyntaxError
     */
    public static function render(
        string $tmpl,
        array $vars = [],
        bool $sandbox = true,
        array $sandboxPolicy = []
    ): string {
        foreach ($vars as $key => $value) {
            if ($value instanceof DateTime) {
                $vars[$key] = $value->format(DATE_RFC3339);
            }
        }
        $headerSent = HtmlFormatter::getOutputHeader();
        $twig = self::getTwig($tmpl, $sandbox, $sandboxPolicy);
        $rendered = $twig->render('index', $vars);
        // $twig->render() peut définir HtmlFormatter::$outputHeader à true,
        // ce qui compromet l'affichage des debug/erreurs par la suite
        HtmlFormatter::setOutputHeader($headerSent);
        return $rendered;
    }

    /**
     * Valide un template, interdit les accolades dans la réponse
     * @param string $tmpl
     * @param bool   $sandbox
     * @param array  $sandboxPolicy
     * @return bool
     */
    public static function validate(
        string $tmpl,
        bool $sandbox = true,
        array $sandboxPolicy = []
    ): bool {
        try {
            $rendered = self::render($tmpl, [], $sandbox, $sandboxPolicy);
            return preg_match('/[{}]/', $rendered) === 0;
        } catch (Throwable) {
            return false;
        }
    }

    /**
     * Donne une liste de tokens issue d'un template
     * @param string $tmpl
     * @return array
     * @throws SyntaxError
     */
    public static function tokenize(string $tmpl): array
    {
        $twig = self::getTwig($tmpl, false);
        $tokens = [];

        foreach (explode("\n", (string)$twig->tokenize(new Source($tmpl, 'index'))) as $token) {
            if (preg_match('/^(\w+)\((.*)\)$/', $token, $m)) {
                [, $tokenName, $content] = $m;
                if ($tokenName === 'NAME_TYPE') {
                    $tokens[] = [
                        'name' => $tokenName,
                        'type' => in_array(strtolower($content), self::TOKEN_PROG_LIST)
                            ? 'PROG'
                            : 'VAR',
                        'value' => $content,
                    ];
                } elseif ($content) {
                    $tokens[] = [
                        'name' => $tokenName,
                        'value' => $content,
                    ];
                } else {
                    $tokens[] = ['name' => $tokenName];
                }
            }
        }
        return $tokens;
    }

    /**
     * Donne la liste des variables utilisés dans un template
     * @param string $tmpl
     * @return array
     * @throws SyntaxError
     */
    public static function extractVars(string $tmpl): array
    {
        $vars = [];
        $lastVar = '';
        $isFilter = false;
        $filterTokens = [
            'NAME_TYPE',
            'PUNCTUATION_TYPE',
            'STRING_TYPE',
            'NUMBER_TYPE',
        ];
        foreach (self::tokenize($tmpl) as $token) {
            if ($token['name'] === 'PUNCTUATION_TYPE' && $token['value'] === '|') {
                $isFilter = true;
                if ($lastVar) {
                    $vars[] = $lastVar;
                    $lastVar = '';
                }
            }
            if ($isFilter) {
                if (!in_array($token['name'], $filterTokens)) {
                    $isFilter = false;
                }
            } else {
                if (
                    ($token['type'] ?? '') === 'VAR'
                    || ($token['name'] === 'PUNCTUATION_TYPE'
                    && $token['value'] === '.')
                ) {
                    $lastVar .= $token['value'];
                } elseif ($lastVar) {
                    $vars[] = $lastVar;
                    $lastVar = '';
                }
            }
        }
        return array_values(array_unique($vars));
    }

    /**
     * Remplace une variable par une autre (attention, ne distinge pas les textes)
     * exemple: $tmpl='{{ foo.bar|trim }}{{ foo.baron }}';$seach='foo.bar';$replace='foo.biz';
     *         -> '{{ foo.biz|trim }}{{ foo.baron }}'
     * @param string $tmpl
     * @param string $search
     * @param string $replace
     * @return string
     * @throws SyntaxError
     */
    public static function replaceVar(string $tmpl, string $search, string $replace): string
    {
        if (!in_array($search, self::extractVars($tmpl)) || $search === $replace) {
            return $tmpl;
        }
        $ntmpl = str_replace("\n", self::NL_REPLACE, $tmpl);
        $search = preg_quote($search, '/');
        while (preg_match("/^(.*\W)$search(\W.*)$/", $ntmpl, $m)) {
            $ntmpl = $m[1] . $replace . $m[2];
        }
        return str_replace(self::NL_REPLACE, "\n", $ntmpl);
    }

    /**
     * Aide pour chaque filtre Twig
     * @param string $filter
     * @return string
     */
    public static function getFiltersHelp(string $filter): string
    {
        return match ($filter) {
            'abs' => __("Valeur absolue. exemple : {{ (-1)|abs }}"),
            'capitalize' => __("Met le premier caractère en majuscule"),
            'convert_encoding' => __(
                "Change l'encodage. exemple : {{ data|convert_encoding('UTF-8', 'iso-2022-jp') }}"
                . " convertit depuis 'iso-2022-jp' vers 'UTF-8'"
            ),
            'date' => __("Formatage de date. exemple : {{ data|date('m/d/Y') }}"),
            'date_modify' => __(
                "Modifie une date, à utiliser avec le filtre date pour le formatage. "
                    . "exemple : {{ data|date_modify('+1 day')|date(\"m/d/Y\") }}"
            ),
            'default' => __(
                "Donne une valeur par défaut si la variable est non définie ou vide. exemple : {{ data|default(0) }}"
            ),
            'escape' => __("Échappement html"),
            'first' => __("Premier élément d'une liste ou d'une chaîne de caratères. exemple : {{ 'abc'|first }}"),
            'format_datetime' => __("Formatage de date en toutes lettres. exemple : {{ data|format_datetime }}"),
            'format_number' => __("Formatage de nombre"),
            'join' => __("Concaténation des éléments d'une liste. exemple : {{ [1, 2, 3]|join(', ') }}"),
            'last' => __("Dernier élément d'une liste ou d'une chaîne de caratères. exemple : {{ 'abc'|last }}"),
            'length' => __("Longueur d'une liste ou d'une chaine de caractères"),
            'lower' => __("Mise en minuscule"),
            'remove_accent' => __("Retrait des accents"),
            'round' => __(
                "Arrondi d'une valeur numérique. exemples : {{ 42.55|round }}, {{ 42.55|round(1, 'floor') }}"
            ),
            'slice' => __(
                "Extrait une sous partie d'une liste ou d'une chaine de caractères."
                . " exemple : {{ ['a', 'b', 'c']|slice(1, 2) }} donne ['b', 'c']"
            ),
            'title' => __("Met le premier caractère de chaque mot en majuscule"),
            'trim' => __("Retrait des espaces en début ou fin. exemple : {{ '    test   '|trim }}"),
            'upper' => __("Met tout en majuscule"),
        default => '',
        };
    }
}
