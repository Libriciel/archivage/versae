<?php

/**
 * Versae\Utility\Csv
 */

namespace Versae\Utility;

use Versae\Exception\GenericException;

/**
 * Manipulation de csv
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Csv
{
    /**
     * @var false|string[][] code csv (utilisé à la place de $this->file)
     */
    private $str;
    /**
     * Constructeur
     * @param string $csv       chemin vers le fichier ou str
     * @param string $separator si valeur auto, on cherche à découvrir si , ou ;
     * @param string $enclosure
     * @param string $escape
     * @param bool   $forceUtf8
     */
    public function __construct(
        string $csv,
        string $separator = 'auto',
        string $enclosure = '"',
        string $escape = '\\',
        bool $forceUtf8 = true
    ) {
        if (is_file($csv)) {
            $csv = file_get_contents($csv);
        }
        if ($separator === 'auto') {
            $separator = self::detectSeparator($csv);
        }
        if ($forceUtf8) {
            $csv = self::csvToUtf8($csv);
        }
        $this->str = self::parse($csv, $separator, $enclosure, $escape);
    }

    /**
     * Donne une coordonnée type B25 en son equivalent numérique : [1,24]
     * @param string $coords
     * @return array
     */
    public static function coords(string $coords): array
    {
        if (preg_match('/^([a-z]+)(\d+)$/i', $coords, $matches)) {
            $level = strlen($matches[1]);
            $matches[1] = array_reduce(
                str_split(strtoupper($matches[1])),
                function ($result, $letter) use (&$level) {
                    return $result + (ord($letter) - 64) * pow(26, --$level);
                }
            );
            $result = array_splice($matches, 1);
            return [$result[0] - 1, $result[1] - 1];
        } elseif (strpos($coords, ',')) {
            $result = explode(',', $coords, 2);
            return [$result[0] - 1, $result[1] - 1];
        } else {
            throw new GenericException(__("Coordonnées invalides"));
        }
    }

    /**
     * Donne les coordonnées selon le numéro de ligne et de colonne
     * ex: (0, 0) = 'A1' ; (3, 6) = 'D7' ; (26,26) = 'AA27'
     * @param int $col
     * @param int $row
     * @return string
     */
    public static function colRowToCoords(int $col, int $row): string
    {
        $letters = '';
        do {
            $numeric = $row % 26;
            $letters = chr(65 + $numeric) . $letters;
            $row = intval($row / 26) - 1;
        } while ($row > -1);
        return $letters . ($col + 1);
    }

    /**
     * Donne le contenu d'une case selon sa coordonnée
     * @param string $coords
     * @return string|array
     */
    public function get(string $coords)
    {
        if (strpos($coords, ':')) {
            $result = [];
            foreach (self::coordRange($coords) as $newCoords) {
                [$col, $row] = $newCoords;
                if (isset($this->str[$row][$col])) {
                    $result[] = $this->str[$row][$col];
                }
            }
            return $result;
        }
        [$col, $row] = self::coords($coords);
        if (!isset($this->str[$row])) {
            return '';
        }
        return $this->str[$row][$col] ?? '';
    }

    /**
     * Donne les données parsés
     * @return string[][]
     */
    public function getData(): array
    {
        return $this->str;
    }

    /**
     * Recherche du separator, si il y a plus de ";" que de "," alors on estime
     * que ";" est le séparateur, "," restant la solution par défaut
     * @param string $csvstr
     * @return string
     */
    public static function detectSeparator(string $csvstr): string
    {
        $commaCount = substr_count($csvstr, ',');
        $semicolonCount = substr_count($csvstr, ';');
        return $commaCount > $semicolonCount ? ',' : ';';
    }

    /**
     * Essaye de convertir un document en utf8
     * @param string $csvstr
     * @return string
     */
    public static function csvToUtf8(string $csvstr): string
    {
        if (!mb_check_encoding($csvstr, 'UTF-8')) {
            $csvstr = iconv(
                mb_detect_encoding($csvstr, ['ISO-8859-1', 'ASCII', 'CP850']),
                "UTF-8",
                $csvstr
            );
        }
        return $csvstr;
    }

    /**
     * Converti un csv string en array de 2 dimentions
     * [0 => [0 => 'col1', 1 => 'col2']]
     * @param string $csvstr
     * @param string $separator
     * @param string $enclosure
     * @param string $escape
     * @return array
     */
    public static function parse(
        string $csvstr,
        string $separator = ',',
        string $enclosure = '"',
        string $escape = '\\'
    ): array {
        $tmp = tmpfile();
        fwrite($tmp, $csvstr);
        rewind($tmp);
        $parsed = [];
        do {
            $getcsv = fgetcsv(
                $tmp,
                0,
                $separator,
                $enclosure,
                $escape
            );
            if ($getcsv) {
                $parsed[] = $getcsv;
            }
        } while ($getcsv);
        fclose($tmp);
        return $parsed;
    }

    /**
     * Converti une plage de coordonnées comme B2:B150 en liste de coordonnées
     * @param string $coord
     * @return array
     */
    public static function coordRange(string $coord): array
    {
        $range = [];
        if (substr_count($coord, ':') === 1) {
            [$begin, $end] = explode(':', $coord);
            $beginCoord = self::coords($begin);
            $endCoord = self::coords($end);
            if ($beginCoord[0] === $endCoord[0] && $beginCoord[1] < $endCoord[1]) {
                for ($i = $beginCoord[1]; $i <= $endCoord[1]; $i++) {
                    $range[] = [$beginCoord[0], $i];
                }
            } elseif ($beginCoord[1] === $endCoord[1] && $beginCoord[0] < $endCoord[0]) {
                for ($i = $beginCoord[0]; $i <= $endCoord[0]; $i++) {
                    $range[] = [$i, $beginCoord[1]];
                }
            } else {
                throw new GenericException(__("Plage de coordonnées non valide"));
            }
        }
        return $range;
    }
}
