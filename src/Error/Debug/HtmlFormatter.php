<?php

/**
 * Versae\Error\Debug\HtmlFormatter
 * @noinspection PhpInternalEntityUsedInspection CakeHtmlFormatter
 */

namespace Versae\Error\Debug;

use Cake\Error\Debug\HtmlFormatter as CakeHtmlFormatter;

/**
 * Permet de marquer comme header envoyé false"
 *
 * @category Exception
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class HtmlFormatter extends CakeHtmlFormatter
{
    /**
     * Getter
     * @return bool
     */
    public static function getOutputHeader(): bool
    {
        return static::$outputHeader;
    }

    /**
     * Setter
     * @param bool $outputHeader
     */
    public static function setOutputHeader(bool $outputHeader)
    {
        static::$outputHeader = $outputHeader;
    }
}
