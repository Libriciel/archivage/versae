<?php

/**
 * Versae\View\AppView
 */

namespace Versae\View;

use AsalaeCore\View\AppView as CoreView;
use AsalaeCore\View\Helper\InputHelper;

/**
 * Application View
 *
 * Your application’s default view class
 *
 * @category View
 *
 * @link http://book.cakephp.org/3.0/en/views.html#the-app-view
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property InputHelper Input
 */
class AppView extends CoreView
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading helpers.
     *
     * e.g. `$this->loadHelper('Html');`
     */
    public function initialize(): void
    {
        parent::initialize();
        $registry = $this->helpers();
        $helpers = $registry->normalizeArray(
            [
                'Acl' => ['className' => 'AsalaeCore.Acl'],
                'AjaxPaginator' => ['className' => 'AsalaeCore.AjaxPaginator'],
                'Breadcrumbs' => ['className' => 'Bootstrap.Breadcrumbs'],
                'Date' => ['className' => 'AsalaeCore.Date'],
                'Fa' => [
                    'className' => 'AsalaeCore.Fa',
                    'v5' => true,
                ],
                'Filter' => ['className' => 'AsalaeCore.Filter'],
                'Form' => ['className' => 'AsalaeCore.Form'],
                'Html' => ['className' => 'AsalaeCore.Html'],
                'Input' => ['className' => 'AsalaeCore.Input'],
                'LiberSign' => ['className' => 'AsalaeCore.LiberSign'],
                'Modal' => [
                    'className' => 'Bootstrap.Modal',
                    'templates' => [
                        'modalTitle' => '<h3 class="h4 modal-title{{attrs.class}}"{{attrs}}>{{content}}</h3>',
                        'modalContentEnd' => "<div class=\"ui-draggable-handle bottom-handle\"></div>\n</div>\n",
                        'modalEnd' => "{{contentEnd}}{{dialogEnd}}</div>\n",
                        'modalDialogEnd' => "</div>\n",
                        'headerEnd' => "</div>\n",
                        'bodyEnd' => "</div>\n",
                        'footerEnd' => "</div>\n",
                        'modalHeaderCloseButton' =>
                        '<button type="button"
                             title="' . __("Close") . '"
                             class="close{{attrs.class}}"
                             type="button"
                             onclick="$(this).closest(\'.modal\').modal(\'hide\')"
                             aria-label="{{label}}"{{attrs}}>{{content}}</button>',
                    ]
                ],
                'ModalForm' => ['className' => 'AsalaeCore.ModalForm'],
                'ModalView' => ['className' => 'AsalaeCore.ModalView'],
                'MultiStepForm' => ['className' => 'AsalaeCore.MultiStepForm'],
                'Navbar' => ['className' => 'Bootstrap.Navbar'],
                'Paginator' => ['className' => 'AsalaeCore.Paginator'],
                'Panel' => ['className' => 'Bootstrap.Panel'],
                'SearchEngine' => ['className' => 'SearchEngine'],
                'Table' => ['className' => 'AsalaeCore.Table'],
                'Tabs' => ['className' => 'AsalaeCore.Tabs'],
                'Translate' => ['className' => 'Translate'],
                'Upload' => ['className' => 'AsalaeCore.Upload'],
                'ViewTable' => ['className' => 'AsalaeCore.ViewTable'],
            ]
        );

        foreach ($helpers as $properties) {
            $this->loadHelper($properties['class'], $properties['config']);
        }

        if (isset($this->viewVars['pageTitle'])) {
            $this->assign('title', $this->viewVars['pageTitle']);
        }
    }
}
