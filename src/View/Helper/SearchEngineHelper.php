<?php

/**
 * Versae\View\Helper\SearchEngineHelper
 */

namespace Versae\View\Helper;

use AsalaeCore\View\Helper\FaHelper;
use AsalaeCore\View\Helper\FormHelper;
use AsalaeCore\View\Helper\HtmlHelper;
use Cake\View\Helper;

/**
 * Génère un input de moteur de recherche
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property FaHelper Fa
 * @property FormHelper Form
 * @property HtmlHelper Html
 */
class SearchEngineHelper extends Helper
{
    /**
     * @var array helpers
     */
    protected $helpers = [
        'AsalaeCore.Form',
        'AsalaeCore.Fa',
        'AsalaeCore.Html',
    ];

    /**
     * @var array config par défaut
     */
    protected $_defaultConfig = [
        'form' => [
            'class' => 'container',
            'method' => 'get',
            'type' => 'get',
        ],
        'input' => [
            'aria-autocomplete' => 'both',
            'aria-haspopup' => false,
            'autocapitalize' => 'off',
            'autocomplete' => 'off',
            'autocorrect' => 'off',
            'role' => 'combobox',
            'spellcheck' => false,
        ],
        'script' => [
            'delay' => 250, // ms
        ]
    ];

    /**
     * Initialise un formulaire de recherche
     * @param mixed $context The context for which the form is being defined.
     *                       Can be a ContextInterface instance, ORM entity, ORM resultset, or an
     *                       array of meta data. You can use `null` to make a context-less form.
     * @param array $options An array of html attributes and options.
     * @return string An formatted opening FORM tag.
     * @link https://book.cakephp.org/4/en/views/helpers/form.html#Cake\View\Helper\FormHelper::create
     */
    public function form($context, array $options)
    {
        return $this->Form->create($context, $options + $this->getConfig('form'));
    }

    /**
     * Donne le input/label du champ de recherche
     * @param string $field
     * @param array  $params
     * @return string
     */
    public function input(string $field, array $params): string
    {
        $button = $this->Html->tag(
            'button',
            $this->Fa->i('fa-search')
            . $this->Html->tag(
                'span.sr-only',
                __("Rechercher")
            ),
            [
                'type' => 'submit',
                'class' => 'btn btn-primary',
                'title' => __("Rechercher"),
            ]
        );
        return $this->Form->control(
            $field,
            $params
            + $this->getConfig('input')
            + [
                'label' => __("Moteur de recherche"),
                'append' => [$button],
            ]
        );
    }

    /**
     * Donne le script lié à un moteur de recherche
     * @param string $inputId
     * @param string $currentUrl
     * @param string $searchUrl
     * @param array  $table
     * @return string
     */
    public function script(string $inputId, string $currentUrl, string $searchUrl, array $table)
    {
        $delay = $this->getConfig('script.delay', 250);
        $table = json_encode($table);
        return $this->Html->tag(
            'script',
            "new AsalaeSearchEngine('#$inputId', '$currentUrl', '$searchUrl', $table, $delay);"
        );
    }
}
