<?php

/**
 * Versae\View\Helper\HtmlHelper
 */

namespace Versae\View\Helper;

use Bootstrap\View\Helper\HtmlHelper as BootstrapHtmlHelper;

/**
 * Ajoute la possibilité de remplacer:
 *
 * $this->Html->tag('div', 'foo', ['id' => 'foo', 'class' => 'bar baz'])
 *
 * par beaucoup plus court et lisible:
 *
 * $this->Html->tag('div#foo.bar.baz', 'foo')
 *
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class HtmlHelper extends BootstrapHtmlHelper
{
    /**
     * Returns a formatted block tag, i.e DIV, SPAN, P.
     *
     * ### Options
     *
     * - `escape` Whether or not the contents should be html_entity escaped.
     *
     * @param string      $name    Tag name.
     * @param string|null $text    String content that will appear inside the div element.
     *                             If null, only a start tag will be printed
     * @param array       $options Additional HTML attributes of the DIV tag, see above.
     * @return string The formatted tag element
     */
    public function tag($name, ?string $text = null, array $options = []): string
    {
        if (preg_match('/#([\w-]+)/', $name, $match, PREG_OFFSET_CAPTURE)) {
            $name = substr($name, 0, $match[0][1]) . substr($name, $match[0][1] + strlen($match[0][0]));
            $options['id'] = $match[1][0];
        }
        $classes = [];
        while (preg_match('/\.([\w-]+)/', $name, $match, PREG_OFFSET_CAPTURE)) {
            $name = substr($name, 0, $match[0][1]) . substr($name, $match[0][1] + strlen($match[0][0]));
            $classes[] = $match[1][0];
        }
        if (!empty($classes)) {
            $options['class'] = implode(' ', $classes);
        }
        return parent::tag($name, $text, $options);
    }
}
