<?php

/**
 * Versae\View\Helper\TranslateHelper
 */

namespace Versae\View\Helper;

use AsalaeCore\View\Helper\TranslateHelper as CoreTranslateHelper;
use Cake\I18n\I18n;
use Cake\View\Helper\HtmlHelper;
use Exception;

/**
 * Centralise certaines traductions
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property HtmlHelper $Html
 */
class TranslateHelper extends CoreTranslateHelper
{

    /**
     * Permet d'obtenir la traduction pour un controller / action dans un
     * contexte de droits
     *
     * @param string      $controller
     * @param string|null $action
     * @return string
     * @throws Exception
     */
    public function permission(string $controller, string $action = null): string
    {
        if ($action === null) {
            return $this->translateController($controller);
        }

        if ($controller === 'KeywordLists' && $action === 'newVersion') {
            return __dx('droit', 'KeywordLists', "Nouvelle version");
        }
        if ($controller === 'Users' && $action === 'editByAdmin') {
            return __dx('droit', 'Users', "Modifier (admin. tech.)");
        }
        if ($controller === 'Forms') {
            switch ($action) {
                case 'indexEditing':
                    return __dx('droit', 'Forms', "Tous les formulaires");
                case 'indexPublished':
                    return __dx('droit', 'Forms', "Formulaires publiés");
                case 'indexAll':
                    return __dx('droit', 'Forms', "Formulaires toutes versions");
                case 'deactivate':
                    return __dx('droit', 'Forms', "Désactiver");
                case 'duplicate':
                    return __dx('droit', 'Forms', "Dupliquer");
                case 'activate':
                    return __dx('droit', 'Forms', "Activer");
                case 'publish':
                    return __dx('droit', 'Forms', "Publier");
                case 'test':
                    return __dx('droit', 'Forms', "Tester");
                case 'unpublish':
                    return __dx('droit', 'Forms', "Dépublier");
                case 'version':
                    return __dx('droit', 'Forms', "Versionner");
                case 'import':
                    return __dx('droit', 'Forms', "Importer");
                case 'export':
                    return __dx('droit', 'Forms', "Exporter");
            }
        }
        if ($controller === 'Deposits') {
            return $this->translateDeposit($action);
        }

        if ($controller === 'Ldaps' && $action === 'importUsers') {
            return __dx('droit', 'Ldaps', "Importer des utilisateurs");
        }

        if ($controller === 'Users') {
            return $this->translateUser($action);
        }

        return $this->translateAction($controller, $action);
    }

    /**
     * Donne la traduction d'un controlleur, sans action
     * @param string $controller
     * @return string
     */
    private function translateController(string $controller): string
    {
        $translation = [
            'Counters' => __d('permissions', "Compteurs"),
            'KeywordLists' => __d('permissions', "Mots-clés"),
            'OrgEntities' => __d('permissions', "Entités"),
            'Users' =>  __d('permissions', "Utilisateurs"),
            'Roles' =>  __d('permissions', "Rôles utilisateur"),
            'Forms' =>  __d('permissions', "Formulaires"),
            'Deposits' => __d('permissions', "Versements"),
            'Tasks' =>  __d('permissions', "Jobs en cours"),
        ];
        return $translation[$controller] ?? I18n::getTranslator('permission')->translate($controller);
    }

    /**
     * Traduction d'une action (non spécifique au controlleur)
     * @param string $controller
     * @param string $action
     * @return string
     */
    private function translateAction(string $controller, string $action)
    {
        switch ($action) {
            case 'add':
            case 'add1':
                return __d('permissions', "Ajouter");
            case 'delete':
                return __d('permissions', "Supprimer");
            case 'edit':
                return __d('permissions', "Modifier");
            case 'index':
                return __d('permissions', "Lister");
            case 'view':
                return __d('permissions', "Visualiser");
            case 'api.create':
                return __d('permissions', "Création via Web service");
            case 'api.read':
                return __d('permissions', "Lecture via Web service");
            case 'api.update':
                return __d('permissions', "Mise à jour via Web service");
            case 'api.delete':
                return __d('permissions', "Suppression via Web service");
        }
        return I18n::getTranslator('permission')->translate(
            $action,
            ['_context' => $controller]
        );
    }

    /**
     * Traductions du controlleur Deposits
     * @param string $action
     * @return string
     */
    private function translateDeposit(string $action): string
    {
        switch ($action) {
            case 'indexInProgress':
                return __dx('droit', 'Deposits', "Mes versements en cours");
            case 'indexAll':
                return __dx('droit', 'Deposits', "Tous mes versements");
            case 'indexMyEntity':
                return __dx('droit', 'Deposits', "Versements de mon service");
            case 'reedit':
                return __dx('droit', 'Deposits', "Rééditer");
            case 'send':
                return __dx('droit', 'Deposits', "Envoyer");
            case 'cancel':
                return __dx('droit', 'Deposits', "Annuler");
            case 'resend':
                return __dx('droit', 'Deposits', "Renvoyer");
        }

        return $this->translateAction('Deposits', $action);
    }

    /**
     * Traductions du controlleur Users
     * @param string $action
     * @return string
     */
    private function translateUser(string $action)
    {
        switch ($action) {
            case 'acceptUser':
                return __dx('droit', 'Users', "Accepter l'utilisateur d'un autre service d'archives");
            case 'changeEntity':
                return __dx('droit', 'Users', "Modifier l'entité de l'utilisateur");
        }

        return $this->translateAction('Users', $action);
    }
}
