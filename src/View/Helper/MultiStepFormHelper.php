<?php

/**
 * Versae\View\Helper\MultiStepFormHelper
 */

namespace Versae\View\Helper;

use Cake\View\Helper;
use Cake\View\Helper\HtmlHelper;

/**
 * Permet d'afficher le niveau d'avancement dans un formulaire en plusieurs étapes
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property HtmlHelper $Html
 */
class MultiStepFormHelper extends Helper
{
    /**
     * @var null|string Nom du template
     */
    public $template = null;

    /**
     * List of helpers used by this helper
     *
     * @var array
     */
    public $helpers = ['Html'];

    /**
     * Défini le template
     * @param string $template
     * @return MultiStepFormHelper
     */
    public function template(string $template): self
    {
        $this->template = $template;
        return $this;
    }

    /**
     * Effectue le rendu de la barre de progression
     * @param int $step
     * @return string
     */
    public function step(int $step): string
    {
        return $this->_View->element($this->template, ['step' => $step]);
    }

    /**
     * Permet de créer un template
     * @param array $steps
     * @param int   $step
     * @return string
     */
    public function create(array $steps, int $step): string
    {
        $i = 0;
        $output = $this->Html->tag('div', null, ['class' => 'multi-step-form-progress-bar']);
        $output .= $this->Html->tag('ol');

        foreach ($steps as $message) {
            $i++;
            $class = '';
            if ($step === $i) {
                $class = 'active';
            } elseif ($step > $i) {
                $class = 'success';
            }
            $output .= $this->Html->tag(
                'li',
                $this->Html->tag('div', '', ['class' => 'bar'])
                . $this->Html->tag('span', $message, ['class' => 'title']),
                ['class' => $class]
            );
        }

        $output .= $this->Html->tag('/ol');
        $output .= $this->Html->tag('/div');
        return $output;
    }
}
