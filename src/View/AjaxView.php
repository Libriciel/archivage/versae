<?php

/**
 * Versae\View\AjaxView
 */

namespace Versae\View;

use Cake\Event\EventManager;
use Cake\Http\Response;
use Cake\Http\ServerRequest;

/**
 * A view class that is used for AJAX responses.
 * Currently only switches the default layout and sets the response type - which just maps to
 * text/html by default.
 *
 * @category View
 *
 * @link http://book.cakephp.org/3.0/en/views.html#the-app-view
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AjaxView extends AppView
{
    /**
     * @var string
     */
    public $layout = 'ajax';

    /**
     * Constructor
     *
     * @param \Cake\Http\ServerRequest|null $request      The request object.
     * @param \Cake\Http\Response|null      $response     The response object.
     * @param \Cake\Event\EventManager|null $eventManager Event manager object.
     * @param array                         $viewOptions  View options.
     */
    public function __construct(
        ServerRequest $request = null,
        Response $response = null,
        EventManager $eventManager = null,
        array $viewOptions = []
    ) {
        if ($response instanceof Response) {
            $response = $response->withType('ajax');
        }

        parent::__construct($request, $response, $eventManager, $viewOptions);
    }
}
