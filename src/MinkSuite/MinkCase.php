<?php

/**
 * AsalaeCore\MinkSuite\MinkCase
 */

namespace Versae\MinkSuite;

use AsalaeCore\MinkSuite\MinkCase as CoreMinkCase;

/**
 * Surcharge de Core/MinkCase pour adapter à Versae
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MinkCase extends CoreMinkCase
{
    /**
     * @inheritdoc
     */
    protected $menuUserSelector = '#user-menu-dropdown';
    /**
     * @inheritdoc
     */
    protected $menuUserElementSelector = '//*[@id="user-menu-dropdown"]/div/*[self::button or self::a]';
    /**
     * @inheritdoc
     */
    protected $menuSelector = '//*[@id="sidebar-container"]/ul/li/div/a/div/span';
    /**
     * @inheritdoc
     */
    protected $submenuSelector = '../../div/a';
}
