<?php

/**
 * Versae\Worker\TransferBuildWorker
 */

namespace Versae\Worker;

use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\Antivirus\AntivirusInterface;
use AsalaeCore\Utility\Antivirus\Clamav;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Worker\AbstractAsalaeWorkerV4;
use Beanstalk\Exception\CantWorkException;
use Cake\Console\ConsoleIo;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Composer\Util\Filesystem as ComposerFilesystem;
use Datacompressor\Utility\DataCompressor;
use DOMDocument;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Pheanstalk\PheanstalkInterface;
use Versae\Exception\GenericException;
use Versae\Model\Table\DepositsTable;
use Versae\Model\Table\TransfersTable;

/**
 * Création du transfert
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TransferBuildWorker extends AbstractAsalaeWorkerV4
{
    /**
     * @var DepositsTable
     */
    private $Deposits;
    /**
     * @var TransfersTable
     */
    private $Transfers;

    /**
     * DestructionWorker constructor.
     * @param EntityInterface $workerEntity
     * @param ConsoleIo       $io
     * @noinspection PhpDocSignatureInspection v4
     */
    public function __construct(EntityInterface $workerEntity, ConsoleIo $io = null)
    {
        parent::__construct($workerEntity, $io);
        $loc = TableRegistry::getTableLocator();
        $this->Deposits = $loc->get('Deposits');
        $this->Transfers = $loc->get('Transfers');
    }

    /**
     * Effectue le travail voulu
     * @param mixed $data
     * @throws Exception
     */
    public function work($data): void
    {
        if (empty($data['transfer_id'])) {
            $this->out(__("Job vide"));
            throw new CantWorkException();
        }
        $transfer = $this->Transfers->find()
            ->where(['Transfers.id' => $data['transfer_id']])
            ->contain(
                [
                    'Deposits' => [
                        'Forms',
                        'DepositValues' => ['FormInputs'],
                    ],
                ]
            )
            ->firstOrFail();
        $deposit = $transfer->get('deposit');
        if (
            !in_array(
                $deposit->get('state'),
                [DepositsTable::S_READY_TO_PREPARE, DepositsTable::S_PREPARING_ERROR]
            )
        ) {
            $this->out(
                $msg = __(
                    "Etat du versement : {0}. Annulation de l'envoi.",
                    $deposit->get('state')
                )
            );
            throw new CantWorkException($msg);
        }

        // S'il est en erreur, on évite qu'un utilisateur puisse le repasser en editing pendant qu'on travaille
        if ($deposit->get('state') === DepositsTable::S_PREPARING_ERROR) {
            $this->Deposits->transitionOrFail($deposit, DepositsTable::T_RETRY);
            $this->Deposits->saveOrFail($deposit);
        }

        // que toutes les UA ont Content.title présent et non vide
        $this->checkAllUnitsHaveTitle($transfer);

        $tmpPath = Hash::get($transfer, 'deposit.path') . DS .  'tmp';
        $uncompressedPath = Hash::get($transfer, 'deposit.path') . DS .  'uncompressed';
        $this->setTransferIdentifier($transfer);
        $zip = Hash::get($transfer, 'zip');
        if (!is_dir($tmpPath)) {
            // pour corriger cette erreur :
            // editer le versement puis enregistrer (ça devrait recréer le dossier)
            $msg = __("Le dossier lié au versement id={0} n'existe pas", $transfer->id);
            throw $this->getException($transfer, $msg);
        }

        $composerFilesystem = new ComposerFilesystem();
        if ($composerFilesystem->size($tmpPath) > disk_free_space($tmpPath)) {
            $msg = __("Pas assez de place disponible sur le volume d'échange");
            throw $this->getException($transfer, $msg);
        }

        $this->out(__("Analyse antivirus..."));
        try {
            /** @var Clamav|AntivirusInterface $Antivirus */
            $Antivirus = Utility::get(Configure::read('Antivirus.classname'));
            $viruses = $Antivirus->scan($tmpPath);
        } catch (Exception $e) {
            $msg = __("Erreur lors de l'analyse antivirus");
            throw $this->getException($transfer, $msg, $e);
        }
        if ($viruses) {
            $file = array_keys($viruses)[0];
            $msg = __(
                "Virus détecté dans le fichier {0} ({1})",
                basename($file),
                $viruses[$file]
            );
            Filesystem::remove($tmpPath);
            throw $this->getException($transfer, $msg);
        }

        $this->out(__("Compression des données..."));
        if (!DataCompressor::compress($tmpPath, $zip)) {
            $msg = __("Erreur lors de la création du zip");
            throw $this->getException($transfer, $msg);
        }
        $conn = $this->Transfers->getConnection();
        try {
            $conn->begin();
            $this->Deposits->transitionOrFail($deposit, DepositsTable::T_PREPARE);
            $files = Filesystem::listFiles($tmpPath . '/original_data');
            $size = array_reduce(
                $files,
                fn($size, $a) => $size + filesize($a)
            );
            $transfer->set('data_count', count($files));
            $transfer->set('data_size', $size);
            $this->Deposits->saveOrFail($deposit);
            $this->Transfers->saveOrFail($transfer);
            $this->emit(
                'transfer-send',
                ['transfer_id' => $transfer->id, 'deposit_id' => $deposit->id, 'user_id' => $this->userId],
                PheanstalkInterface::DEFAULT_PRIORITY,
                1 // le temps de cloturer la transaction
            );
            Filesystem::remove($tmpPath);
            if (is_dir($uncompressedPath)) {
                Filesystem::remove($uncompressedPath);
            }
            $conn->commit();
        } catch (Exception $e) {
            unlink($zip);
            $conn->rollback();
            $this->Deposits->transition($deposit, DepositsTable::T_ERROR);
            $this->Deposits->save($deposit);

            throw $e;
        }
    }

    /**
     * Envoi une erreur
     * @param EntityInterface $transfer
     * @param string          $message
     * @param Exception|null  $prev
     * @return GenericException
     */
    private function getException(EntityInterface $transfer, string $message, Exception $prev = null): GenericException
    {
        $transfer->set('error_message', $message);
        $this->Transfers->saveOrFail($transfer);

        $deposit = $transfer->get('deposit');
        $this->Deposits->transitionOrFail($deposit, DepositsTable::T_ERROR);
        $this->Deposits->saveOrFail($deposit);

        return new GenericException($message, null, $prev);
    }

    /**
     * Modifie l'identifiant temporaire par son identifiant final
     * @param EntityInterface $transfer
     * @throws Exception
     */
    private function setTransferIdentifier(EntityInterface $transfer): void
    {
        $path = Hash::get($transfer, 'deposit.path');
        $transferXmlPath = rtrim($path, DS) . '/transfer.xml';
        $tmpXmlPath = rtrim($path, DS) . '/tmp/management_data/transfer.xml';
        $util = DOMUtility::load($transferXmlPath);
        $identifier = $util->node('ns:MessageIdentifier|ns:TransferIdentifier');
        if ($identifier->nodeValue !== $transfer->get('identifier')) {
            $util::setValue($identifier, $transfer->get('identifier'));
            $util->dom->save($transferXmlPath);
        }
        if (is_file($tmpXmlPath)) {
            unlink($tmpXmlPath);
        }
        symlink($transferXmlPath, $tmpXmlPath);
    }

    /**
     * Chaque UA doit avoir un Title
     * @param EntityInterface $transfer
     * @return void
     * @throws Exception|GenericException
     */
    private function checkAllUnitsHaveTitle(EntityInterface $transfer): void
    {
        $dom = new DOMDocument();
        $dom->load($transfer->get('path') . DS . 'transfer.xml');
        $util = new DOMUtility($dom);

        switch ($util->namespace) {
            case NAMESPACE_SEDA_10:
                $path = '//ns:Archive|//ns:ArchiveObject';
                $name = 'ns:Name';
                break;
            case NAMESPACE_SEDA_21:
            case NAMESPACE_SEDA_22:
                $path = '//ns:ArchiveUnit/ns:Content';
                $name = 'ns:Title';
                break;
            default:
                throw new Exception('unsuported namespace: ' . $util->namespace);
        }

        foreach ($util->xpath->query($path) as $unit) {
            if (!$util->node($name, $unit)->nodeValue) {
                $msg = __("Pas de Title dans l'Unité d'archives");
                throw $this->getException($transfer, $msg);
            }
        }
    }
}
