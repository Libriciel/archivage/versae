<?php

/**
 * Versae\Worker\GenerateDepositWorker
 */

namespace Versae\Worker;

use AsalaeCore\Utility\FormatError;
use AsalaeCore\Worker\AbstractAsalaeWorkerV4;
use Beanstalk\Exception\CantWorkException;
use Cake\Console\ConsoleIo;
use Cake\Datasource\EntityInterface;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use ErrorException;
use Exception;
use Versae\Form\FormMakerForm;
use Versae\Model\Table\DepositsTable;

/**
 * Création du transfert
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2023, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class GenerateDepositWorker extends AbstractAsalaeWorkerV4
{
    /**
     * @var DepositsTable
     */
    private $Deposits;

    /**
     * DestructionWorker constructor.
     * @param EntityInterface $workerEntity
     * @param ConsoleIo       $io
     * @noinspection PhpDocSignatureInspection v4
     */
    public function __construct(EntityInterface $workerEntity, ConsoleIo $io = null)
    {
        parent::__construct($workerEntity, $io);
        $loc = TableRegistry::getTableLocator();
        $this->Deposits = $loc->get('Deposits');
    }

    /**
     * Effectue le travail voulu
     * @param mixed $data
     * @throws Exception
     */
    public function work($data): void
    {
        if (empty($data['deposit_id'])) {
            $this->out(__("Job vide"));
            throw new CantWorkException();
        }
        $deposit = $this->Deposits->find()
            ->where(['Deposits.id' => $data['deposit_id']])
            ->contain(
                [
                    'Forms' => [
                        'FormFieldsets' => [
                            'sort' => 'ord',
                            'FormInputs' => [
                                'sort' => 'ord',
                            ],
                        ],
                    ],
                    'DepositValues' => [
                        'sort' => 'fieldset_index',
                        'FormInputs' => ['FormFieldsets']
                    ],
                ]
            )
            ->firstOrFail();
        if ($deposit->get('state') !== DepositsTable::S_GENERATING) {
            $this->out(
                $msg = __(
                    "État du versement : {0}. Annulation de l'envoi.",
                    $deposit->get('state')
                )
            );
            throw new CantWorkException($msg);
        }

        $this->out(__("Initialization du FormMaker pour le versement {0}", $deposit->get('id')));
        $requestData = unserialize($deposit->get('request_data'));
        if (!$requestData) {
            $this->error(
                $msg = __("Échec lors du unserialize du request data")
            );
            $this->Deposits->transition($deposit, DepositsTable::T_ERROR);
            $this->Deposits->save($deposit);
            throw new CantWorkException($msg);
        }
        if (empty($requestData['data']['inputs'])) {
            $requestData['data']['inputs'] = []; // empeche un bug en cas de formulaire vide
        }

        set_error_handler(
            function ($errno, $errstr, $errfile, $errline) {
                throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
            }
        );
        try {
            $formEntity = $deposit->get('form');
            $form = FormMakerForm::create($formEntity->id, $this->userId, $deposit);

            $inputs = $requestData['data']['inputs']
                + $this->Deposits->getDepositFormData($deposit);
            $form->setData($inputs);
            $formData = $requestData['data']['inputs'] + $requestData['additionnalData'];
        } catch (Exception $e) {
            $file = str_replace(ROOT, 'ROOT', $e->getFile());
            $data['error'] = isset($form) ? FormatError::logFormErrors($form, true, false) : '';
            $data['error'] .= ' ' . ($file . ':' . $e->getLine() . ' ' . $e->getMessage());
            $this->error(__("Échec lors de la génération du XML:\n{0}", trim($data['error'])));
            $deposit->set('request_data');
            $deposit->set('error_message', $data['error']);
            $this->Deposits->transition($deposit, DepositsTable::T_ERROR);
            $this->Deposits->save($deposit);
            $this->websocketMessage('generate_deposit_failed', $data);
            $this->error((string)$e);
            return;
        } finally {
            restore_error_handler();
        }
        
        unset($requestData['data']['state']);
        try {
            $this->out(__("Génération du XML"));
            $success = $form->execute($formData);
            $this->Deposits->updateDeposit($deposit, $requestData['data']);

            if (!$success) {
                throw new Exception(__("Échec lors de la génération du bordereau"));
            }

            $form->exportFiles();
            $this->Deposits->transitionOrFail($deposit, DepositsTable::T_DONE);
            $this->Deposits->saveOrFail($deposit);
            $data['sendable'] = $deposit->get('sendable');
            $this->websocketMessage('generate_deposit_success', $data);
        } catch (Exception $e) {
            $file = str_replace(ROOT, 'ROOT', $e->getFile());
            $formatedErrors = FormatError::logFormErrors($form, true, false);
            $workerError = $formatedErrors
                ?: ($file . ':' . $e->getLine() . ' ' . $e->getMessage());
            $this->error(__("Échec lors de la génération du XML:\n{0}", $workerError));
            $data['error'] = sprintf(
                '%s %s:%d %s',
                $formatedErrors,
                $file,
                $e->getLine(),
                $e->getMessage()
            );
            $this->error(__("Échec lors de la génération du XML:\n{0}", trim($data['error'])));
            $deposit->set('request_data');
            if (isset($data['xml'])) {
                Log::debug($data['xml']);
            }
            $deposit->set('error_message', $data['error']);
            $this->Deposits->transition($deposit, DepositsTable::T_ERROR);
            $this->Deposits->save($deposit);
            $this->websocketMessage('generate_deposit_failed', $data);
            $this->error((string)$e);
        }
    }
}
