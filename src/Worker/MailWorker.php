<?php

/**
 * Versae\Worker\MailWorker
 */

namespace Versae\Worker;

use AsalaeCore\Worker\AbstractAsalaeWorkerV4;
use Cake\Console\ConsoleIo;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Exception;
use Versae\Model\Entity\Mail;
use Versae\Model\Table\MailsTable;

/**
 * Envoi des mails
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MailWorker extends AbstractAsalaeWorkerV4
{
    /**
     * @var MailsTable
     */
    private $Mails;

    /**
     * DestructionWorker constructor.
     * @param EntityInterface $workerEntity
     * @param ConsoleIo       $io
     * @noinspection PhpDocSignatureInspection v4
     */
    public function __construct(EntityInterface $workerEntity, ConsoleIo $io = null)
    {
        parent::__construct($workerEntity, $io);
        $loc = TableRegistry::getTableLocator();
        $this->Mails = $loc->get('Mails');
    }

    /**
     * Effectue le travail voulu
     * @param mixed $data
     * @throws Exception
     */
    public function work($data)
    {
        /** @var Mail $mail */
        $mail = $this->Mails->get($data['mail_id']);
        $mail->send();
    }

    /**
     * Lancé après work()
     * @param mixed $data
     * @throws Exception
     */
    public function afterWork($data)
    {
        parent::afterWork($data);
        $cooldown = $this->params['cooldown'] ?? 1;
        sleep($cooldown);//NOSONAR
    }
}
