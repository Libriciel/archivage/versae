<?php

/**
 * Versae\Worker\TransferSendWorker
 */

namespace Versae\Worker;

use AsalaeCore\Worker\AbstractAsalaeWorkerV4;
use Beanstalk\Exception\CantWorkException;
use Cake\Console\ConsoleIo;
use Cake\Datasource\EntityInterface;
use Cake\Http\Client\Response;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DateTime;
use Exception;
use Versae\Exception\GenericException;
use Versae\Model\Entity\ArchivingSystem;
use Versae\Model\Table\DepositsTable;
use Versae\Model\Table\TransfersTable;

/**
 * Envoi du transfert sur le SAE
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TransferSendWorker extends AbstractAsalaeWorkerV4
{
    public const TOUCH_INTERVAL = 10.0;
    /**
     * @var TransfersTable
     */
    private $Transfers;
    /**
     * @var DepositsTable
     */
    private $Deposits;
    /**
     * @var DepositsTable
     */
    private static $lastTouch;

    /**
     * DestructionWorker constructor.
     * @param EntityInterface $workerEntity
     * @param ConsoleIo       $io
     * @noinspection PhpDocSignatureInspection v4
     */
    public function __construct(EntityInterface $workerEntity, ConsoleIo $io = null)
    {
        parent::__construct($workerEntity, $io);
        $loc = TableRegistry::getTableLocator();
        $this->Transfers = $loc->get('Transfers');
        $this->Deposits = $loc->get('Deposits');
    }

    /**
     * Effectue le travail voulu
     * @param mixed $data
     * @throws Exception
     */
    public function work($data)
    {
        if (empty($data['transfer_id'])) {
            $this->out(__("Job vide"));
            throw new CantWorkException();
        }
        $transfer = $this->Transfers->find()
            ->where(['Transfers.id' => $data['transfer_id']])
            ->contain(
                [
                    'Deposits' => [
                        'Forms' => ['ArchivingSystems'],
                        'DepositValues' => ['FormInputs'],
                    ],
                ]
            )
            ->firstOrFail();
        $deposit = $transfer->get('deposit');
        if (
            !in_array(
                $deposit->get('state'),
                [DepositsTable::S_READY_TO_SEND, DepositsTable::S_SENDING_ERROR]
            )
        ) {
            $this->out(
                __(
                    "Etat du versement : {0}. Annulation de l'envoi.",
                    $deposit->get('state')
                )
            );
            throw new CantWorkException();
        }

        // S'il est en erreur, on évite qu'un utilisateur puisse le repasser en editing pendant qu'on travaille
        if ($deposit->get('state') === DepositsTable::S_SENDING_ERROR) {
            $this->Deposits->transitionOrFail($deposit, DepositsTable::T_RETRY);
            $this->Deposits->saveOrFail($deposit);
        }

        if (!is_file($transfer->get('zip'))) {
            // editer le versement puis enregistrer devrai recréer le dossier
            $msg = __("Le fichier zip lié au versement id={0} n'existe pas", $transfer->id);
            throw $this->getException($transfer, $msg);
        }

        /** @var ArchivingSystem $archivingSystem */
        $archivingSystem = Hash::get($transfer, 'deposit.form.archiving_system');
        $this->out(
            __(
                "Envoi du versement id={0}: sur {1} avec l'utilisateur {2}",
                $data['transfer_id'],
                $archivingSystem->get('url'),
                $archivingSystem->get('username')
            )
        );
        try {
            self::$lastTouch = microtime(true);
            $response = $archivingSystem->postOutgoingTransfer(
                $transfer,
                true,
                $this->touchCallback(...)
            );
        } catch (Exception $e) {
            $msg = __(
                "Erreur lors de l'envoi du versement id={0}: {1}",
                $transfer->id,
                $e->getMessage()
            );
            throw $this->getException($transfer, $msg);
        }
        $this->saveResponse($transfer, $response);
        $response = $response->getJson();
        if (is_array($response)) {
            if (isset($response['success']) && $response['success']) {
                $this->out(__("Versement effectué avec succès !"));
                $this->Deposits->transitionOrFail($deposit, DepositsTable::T_SEND);
                $this->Deposits->saveOrFail($deposit);
                return;
            } else {
                if (isset($response['error'])) {
                    $msg = is_array($response['error'])
                        ? var_export($response['error'], true)
                        : $response['error'];
                    throw $this->getException($transfer, $msg);
                } elseif (isset($response['message'])) {
                    throw $this->getException(
                        $transfer,
                        'Error code ' . $response['code'] . ' ' . $response['message']
                    );
                } elseif (isset($response['code'])) {
                    throw $this->getException($transfer, 'Error code ' . $response['code']);
                }
                /** @noinspection PhpConditionAlreadyCheckedInspection */
                if (!empty($response['message'])) {
                    throw $this->getException($transfer, $response['message']);
                }
            }
        }
        throw $this->getException($transfer, __("Erreur lors du versement"));
    }

    /**
     * Envoi une erreur
     * @param EntityInterface $transfer
     * @param string          $message
     * @param Exception|null  $prev
     * @return GenericException
     */
    private function getException(EntityInterface $transfer, string $message, Exception $prev = null)
    {
        $deposit = $transfer->get('deposit');
        $this->Deposits->transitionOrFail($deposit, DepositsTable::T_ERROR);
        $this->Deposits->save($deposit);
        $transfer->set('error_message', $message);
        $this->Transfers->save($transfer);

        return new GenericException($message, null, $prev);
    }

    /**
     * Mémorise la réponse dans le transfert
     * @param EntityInterface $transfer
     * @param Response        $response
     */
    private function saveResponse(EntityInterface $transfer, Response $response)
    {
        $this->Transfers->patchEntity(
            $transfer,
            [
                'last_comm_date' => new DateTime(),
                'last_comm_code' => $response->getStatusCode(),
                'last_comm_message' => $response->getBody()->getContents(),
            ],
            ['validate' => false]
        );
    }

    /**
     * Feedback de l'upload en cours toutes les 10s
     * @param array $params
     * @return void
     */
    public function touchCallback(array $params)
    {
        if (
            !empty($params['finalization'])
            && !empty($params['transfer'])
            && !empty($params['url'])
        ) {
            $transfer = is_resource($params['transfer'])
                ? '<resource>'
                : $params['transfer'];
            $this->out(
                sprintf(
                    'finalization: %s (transfer=%s)',
                    $params['url'],
                    $transfer
                )
            );
            return;
        }
        if (empty($params['currentChunk']) || empty($params['totalChunks'])) {
            return;
        }
        $mtime = microtime(true);
        if (
            $mtime - self::$lastTouch > self::TOUCH_INTERVAL
            || $params['currentChunk'] === $params['totalChunks']
        ) {
            $this->out(sprintf('%d / %d', $params['currentChunk'], $params['totalChunks']));
            self::$lastTouch = $mtime;
        }
    }
}
