<?php

/**
 * Versae\Http\ConnSession
 */

namespace Versae\Http;

use Cake\Http\Session;

/**
 * Surcharge de la classe de Session
 */
class ConnSession extends Session
{
    /**
     * @override Retire le Session::write('Config.time')
     *
     * Returns true if the session is no longer valid because the last time it was
     * accessed was after the configured timeout.
     *
     * @return bool
     */
    protected function _timedOut(): bool
    {
        $time = $this->read('Config.time');
        $result = false;

        $checkTime = $time !== null && $this->_lifetime > 0;
        if ($checkTime && (time() - $time > $this->_lifetime)) {
            $result = true;
        }

        return $result;
    }

    /**
     * Rend public _timedOut
     *
     * @return bool
     */
    public function timedOut(): bool
    {
        return $this->_timedOut();
    }

    /**
     * Temps restant avant timeout
     *
     * @return int
     */
    public function timeLeft(): int
    {
        return $this->_lifetime - (time() - $this->read('Config.time'));
    }
}
