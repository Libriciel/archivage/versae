<?php

/**
 * Versae\Cron\UpdateLDAPUsers
 */

namespace Versae\Cron;

use Adldap\Adldap;
use Adldap\Connections\ProviderInterface;
use Adldap\Query\Builder;
use Adldap\Query\Factory;
use AsalaeCore\Console\ConsoleLogTrait;
use AsalaeCore\Cron\CronInterface;
use AsalaeCore\Factory\Utility;
use Cake\Console\ConsoleOutput as CakeConsoleOutput;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Exception;
use Throwable;
use Versae\Model\Entity\Ldap;

/**
 * Synchronise les informations du LDAP avec la base de l'application
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class UpdateLDAPUsers implements CronInterface
{
    use ConsoleLogTrait;

    /**
     * Constructeur de la classe du cron
     * @param array                  $params paramètres additionnels du cron
     * @param CakeConsoleOutput|null $out
     * @param CakeConsoleOutput|null $err
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->initializeOutput($out, $err);
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     * @throws Exception
     */
    public function work(EntityInterface $exec = null, EntityInterface $cron = null): string
    {
        $loc = TableRegistry::getTableLocator();
        $Ldaps = $loc->get('Ldaps');

        /** @var Ldap $ldap */
        foreach ($Ldaps->find()->all() as $ldap) {
            $config = $ldap->getLdapConfig();
            /** @var Adldap $ad */
            $ad = Utility::get(Adldap::class);
            $ad->addProvider($config);
            try {
                $provider = $ad->connect();
                $this->updateUsers($ldap, $provider);
            } catch (Throwable $e) {
                $this->err('<error>' . ((string)$e) . '</error>');
                $state = 'warning';
            }
        }
        return $state ?? 'success';
    }

    /**
     * @inheritDoc
     */
    public static function getVirtualFields(): array
    {
        return [];
    }

    /**
     * Met à jour les utilisateurs d'un LDAP
     * @param EntityInterface   $ldap
     * @param ProviderInterface $provider
     */
    private function updateUsers(EntityInterface $ldap, ProviderInterface $provider)
    {
        $loc = TableRegistry::getTableLocator();
        $Users = $loc->get('Users');
        $query = $Users->find()
            ->where(['ldap_id' => $ldap->id]);
        $this->out(__("Mise à jour de {0} utilisateur(s) du LDAP {1}", $query->count(), $ldap->get('name')));
        /** @var EntityInterface $user */
        foreach ($query as $user) {
            /** @var Factory $search */
            $search = $provider->search();
            /** @var Builder $users */
            $users = $search->users();
            $ldapUser = $users->findBy($ldap->get('user_login_attribute'), $user->get('username'));
            if (!empty($ldapUser)) {
                $name = $ldapUser->getAttribute($ldap->get('user_name_attribute'));
                $mail = $ldapUser->getAttribute($ldap->get('user_mail_attribute'));
                $data = [];
                if ($name) {
                    $data['name'] = current($name);
                }
                if ($mail) {
                    $data['email'] = current($mail);
                }
                $Users->patchEntity($user, $data);
                if ($user->isDirty()) {
                    $Users->saveOrFail($user);
                    $this->out(__("Mis à jour de l'utilisateur {0}", $user->get('username')));
                }
            } elseif ($user->get('active') === true) {
                $this->out(__("L'utilisateur {0} n'a pas été trouvé dans le LDAP", $user->get('username')));
            }
        }
    }
}
