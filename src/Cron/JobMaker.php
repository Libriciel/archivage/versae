<?php

/**
 * Versae\Cron\JobMaker
 */

namespace Versae\Cron;

use AsalaeCore\Console\ConsoleLogTrait;
use AsalaeCore\Cron\ColoredCronTrait;
use AsalaeCore\Cron\CronInterface;
use AsalaeCore\Factory\Utility;
use Beanstalk\Model\Table\BeanstalkJobsTable;
use Beanstalk\Utility\Beanstalk;
use Cake\Console\ConsoleOutput as CakeConsoleOutput;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Exception;
use Versae\Model\Table\DepositsTable;
use Versae\Model\Table\TransfersTable;
use ZMQSocketException;

/**
 * Vérifie l'application
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2024, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class JobMaker implements CronInterface
{
    use ConsoleLogTrait;
    use ColoredCronTrait;

    private Beanstalk $Beanstalk;
    private TransfersTable | Table $Transfers;
    private DepositsTable | Table $Deposits;
    private BeanstalkJobsTable | Table $BeanstalkJobs;

    /**
     * Constructeur de la classe du cron
     * @param array                  $params paramètres additionnels du cron
     * @param CakeConsoleOutput|null $out
     * @param CakeConsoleOutput|null $err
     * @throws Exception
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->initializeOutput($out, $err);
        $this->Beanstalk = Utility::get('Beanstalk');
        $loc = TableRegistry::getTableLocator();
        $this->Transfers = $loc->get('Transfers');
        $this->Deposits = $loc->get('Deposits');
        $this->BeanstalkJobs = $loc->get('BeanstalkJobs');
    }

    /**
     * @inheritDoc
     */
    public static function getVirtualFields(): array
    {
        return [];
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     * @throws Exception
     */
    public function work(EntityInterface $exec = null, EntityInterface $cron = null): string
    {
        $success = $this->checkDeposits();
        $success = $this->checkTransfers() && $success;
        return $success ? 'success' : 'warning';
    }

    /**
     * checkDeposits
     * @return bool
     * @throws ZMQSocketException
     */
    private function checkDeposits(): bool
    {
        $query = $this->Deposits->find()
            ->where(
                [
                    'Deposits.state IN' => [DepositsTable::S_GENERATING],
                ]
            )
            ->contain(['Transfers']);
        $jobs = 0;
        /** @var EntityInterface $deposit */
        foreach ($query as $deposit) {
            $job = [
                'deposit_id' => $deposit->id,
                'user_id' => $deposit->get('created_user_id'),
            ];
            if ($this->emit('generate-deposit', $job, 'Deposits', $deposit->id)) {
                $jobs++;
            }
        }
        $success = true;
        if ($jobs > 0) {
            $this->out(
                __n(
                    "{0} job de type {1} a été envoyé",
                    "{0} jobs de type {1} ont été envoyés",
                    $jobs,
                    $jobs,
                    'generate-deposit'
                )
            );
            $success = false;
        }
        return $success;
    }

    /**
     * checkTransfers
     * @return bool
     * @throws ZMQSocketException
     */
    private function checkTransfers(): bool
    {
        $preparing = $this->checkPreparingTransfers();
        $sending = $this->checkSendingTransfers();
        return $preparing && $sending;
    }

    /**
     * checkPreparingTransfers
     * @return bool
     * @throws ZMQSocketException
     */
    private function checkPreparingTransfers(): bool
    {
        $query = $this->Transfers->find()
            ->innerJoinWith('Deposits')
            ->where(
                [
                    'Deposits.state IN' => [
                        DepositsTable::S_READY_TO_PREPARE,
                        DepositsTable::S_PREPARING_ERROR
                    ],
                ]
            )
            ->contain(['Deposits']);
        $jobs = 0;
        /** @var EntityInterface $transfer */
        foreach ($query as $transfer) {
            $job = [
                'transfer_id' => $transfer->id,
                'deposit_id' => $transfer->get('deposit_id'),
                'user_id' => $transfer->get('created_user_id'),
            ];
            if ($this->emit('transfer-build', $job, 'Transfers', $transfer->id)) {
                $jobs++;
            }
        }
        $success = true;
        if ($jobs > 0) {
            $this->out(
                __n(
                    "{0} job de type {1} a été envoyé",
                    "{0} jobs de type {1} ont été envoyés",
                    $jobs,
                    $jobs,
                    'transfer-build'
                )
            );
            $success = false;
        }
        return $success;
    }

    /**
     * checkSendingTransfers
     * @return bool
     * @throws ZMQSocketException
     */
    private function checkSendingTransfers(): bool
    {
        $query = $this->Transfers->find()
            ->innerJoinWith('Deposits')
            ->where(
                [
                    'Deposits.state IN' => [
                        DepositsTable::S_READY_TO_SEND,
                        DepositsTable::S_SENDING_ERROR,
                    ],
                ]
            )
            ->contain(['Deposits']);
        $jobs = 0;
        /** @var EntityInterface $transfer */
        foreach ($query as $transfer) {
            $job = [
                'transfer_id' => $transfer->id,
                'deposit_id' => $transfer->get('deposit_id'),
                'user_id' => $transfer->get('created_user_id'),
            ];
            if ($this->emit('transfer-send', $job, 'Transfers', $transfer->id)) {
                $jobs++;
            }
        }
        $success = true;
        if ($jobs > 0) {
            $this->out(
                __n(
                    "{0} job de type {1} a été envoyé",
                    "{0} jobs de type {1} ont été envoyés",
                    $jobs,
                    $jobs,
                    'transfer-send'
                )
            );
            $success = false;
        }
        return $success;
    }

    /**
     * Envoi un job, en prenant soin de ne pas créer un doublon
     * @param string $tube
     * @param array  $data
     * @param string $model
     * @param int    $fk
     * @return bool
     * @throws ZMQSocketException
     */
    private function emit(string $tube, array $data, string $model, int $fk): bool
    {
        $job = $this->BeanstalkJobs
            ->find()
            ->where(
                [
                    'tube' => $tube,
                    'object_model' => $model,
                    'object_foreign_key' => $fk,
                ]
            )
            ->first();
        // si le job existe, on le relance ou on ne fait rien
        if ($job) {
            if ($job->get('job_state') === BeanstalkJobsTable::S_FAILED) {
                $this->out(__("Job {0} relancé pour {1}:{2}", $tube, $model, $fk));
                $this->BeanstalkJobs->transitionOrFail($job, BeanstalkJobsTable::T_RETRY);
                return (bool)$this->BeanstalkJobs->save($job);
            }
        } else {
            $this->out(__("Job {0} créé pour {1}:{2}", $tube, $model, $fk));
            $this->Beanstalk->setTube($tube)->emit($data);
            return true;
        }
        return false;
    }
}
