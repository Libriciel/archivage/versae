<?php

/**
 * Versae\Cron\Check
 */

namespace Versae\Cron;

use AsalaeCore\Console\ConsoleLogTrait;
use AsalaeCore\Cron\ColoredCronTrait;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Cron\CronInterface;
use Cake\Console\ConsoleOutput as CakeConsoleOutput;
use Cake\Core\Configure;
use Cake\Database\Connection;
use Cake\Database\Driver\Mysql;
use Cake\Database\Driver\Postgres;
use Cake\Database\Driver\Sqlite;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Utility\Security;
use Exception;
use stdClass;
use Versae\Controller\AdminsController;
use Versae\Utility\Check as CheckUtility;

/**
 * Vérifie l'application
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Check implements CronInterface
{
    use ConsoleLogTrait;
    use ColoredCronTrait;

    public const PHP_MIN_VERSION = '7.0.0';
    public const POSTGRES_MIN_VERSION = '8.4.0';
    public const SQLITE_MIN_VERSION = '3.0.0';
    public const MYSQL_MIN_VERSION = '5.7.0';

    /**
     * @var Exception state = error si != empty
     */
    private $fail;

    /**
     * Constructeur de la classe du cron
     * @param array                  $params paramètres additionnels du cron
     * @param CakeConsoleOutput|null $out
     * @param CakeConsoleOutput|null $err
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->initializeOutput($out, $err);
    }

    /**
     * @inheritDoc
     */
    public static function getVirtualFields(): array
    {
        return [];
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     * @throws Exception
     */
    public function work(EntityInterface $exec = null, EntityInterface $cron = null): string
    {
        $php = $this->checkPhp();
        $database = $this->checkDatabase();
        $softwares = $this->checkSoftwares();
        /** @var CheckUtility $CheckUtility */
        $CheckUtility = Utility::get(CheckUtility::class);
        $beanstalkd = $CheckUtility->beanstalkd();
        $this->o($beanstalkd, __("Serveur Beanstalkd"));
        $ratchet = $CheckUtility->ratchet();
        $this->o($ratchet, __("Serveur Ratchet"));
        $writables = $CheckUtility->writableOk();
        $this->o($writables, __("Dossiers inscriptibles"));
        if ($this->fail) {
            return 'error';
        }
        $success = $php && $database && $softwares && $beanstalkd && $ratchet;
        return $success ? 'success' : 'warning';
    }

    /**
     * Vérifie php et ses modules
     * @return bool
     * @throws Exception
     */
    private function checkPhp(): bool
    {
        $phpVersion = version_compare(PHP_VERSION, self::PHP_MIN_VERSION, '>=');
        $this->o($phpVersion, __("Version php: {0}", PHP_VERSION));

        $composerJson = json_decode(file_get_contents(ROOT . DS . 'composer.json'), true);
        $requiredExts = array_filter(
            array_keys($composerJson['require']),
            function ($v) {
                return str_starts_with($v, 'ext-');
            }
        );
        $modulesPhp = array_map(
            function ($v) {
                return substr($v, 4);
            },
            $requiredExts
        );
        sort($modulesPhp);
        $success = $phpVersion;
        /** @var CheckUtility $Check */
        $Check = Utility::get(CheckUtility::class);
        foreach ($Check->extPhp() as $module) {
            $s = extension_loaded($module);
            $success = $success && $s;
            $this->o($s, __("Extension {0}", $module));
        }
        foreach ($Check->php() as $parametragePhp => $ok) {
            $success = $success && $ok;
            $this->o($ok, __("Paramétrage de {0}", $parametragePhp));
        }

        return $success;
    }

    /**
     * Vérifie la connexion et la version minimale de la base de données
     * @return bool
     */
    private function checkDatabase(): bool
    {
        $configPath = include Configure::read('App.paths.path_to_local_config');
        $config = json_decode(file_get_contents($configPath));
        $success = true;
        foreach ($config->Datasources as $datasource => $params) {
            try {
                /** @var Connection $conn */
                $conn = ConnectionManager::get($datasource);
                $conn->query('select 1');
                $version = 'unknown';
                switch ($params->driver ?? Postgres::class) {
                    case Postgres::class:
                        $version = current($conn->query('select version()')->fetch());
                        $ok = $version
                            && preg_match('/\w+\s+([\d.]+)/', $version, $match)
                            && version_compare($version = $match[1], self::POSTGRES_MIN_VERSION, '>=');
                        $type = 'postgres';
                        break;
                    case Sqlite::class:
                        /** @noinspection PhpFullyQualifiedNameUsageInspection il ne faut pas importer SQLite3 */
                        $ok = class_exists('\SQLite3')
                            && ($version = \SQLite3::version())
                            && isset($version['versionString'])
                            && ($version = $version['versionString'])
                            && version_compare($version, self::SQLITE_MIN_VERSION, '>=');
                        $type = 'sqlite';
                        break;
                    case Mysql::class:
                        $version = current($conn->query('select version()')->fetch());
                        $ok = $version
                            && version_compare($version, self::MYSQL_MIN_VERSION, '>=');
                        $type = 'mysql';
                        break;
                    default:
                        throw new Exception(__("Driver non compatible: {0}", $params->driver));
                }
                $this->o($ok, __("Datasource {0}, type {1}: {2}", $datasource, $type, $version));
            } catch (Exception $e) {
                $this->err('<error>' . ((string)$e) . '</error>');
                $success = false;
                $ok = false;
                $this->fail = $e;
            }
            $success = $success && $ok;
        }
        $Phinxlogs = TableRegistry::getTableLocator()->get('Phinxlog');
        $migrations = glob(CONFIG . 'Migrations/*.php');
        $schema = !empty($migrations);
        foreach ($migrations as $path) {
            $filename = basename($path, '.php');
            [$version] = explode('_', $filename, 2);
            $conditions = ['version' => $version];
            if (!$Phinxlogs->exists($conditions)) {
                $success = false;
                $schema = false;
                break;
            }
        }
        $this->o($schema, __("Schema de base de donnée"));

        return $success;
    }

    /**
     * Vérifie que les logiciels nécessaires sont installés
     * @return bool
     * @throws Exception
     */
    private function checkSoftwares(): bool
    {
        /** @var CheckUtility $CheckUtility */
        $CheckUtility = Utility::get(CheckUtility::class);
        $success = true;
        $prerequis = array_keys(Hash::normalize(Configure::read('Check.prerequis', CheckUtility::$prerequis)));
        $missings = $CheckUtility->missings();
        foreach ($prerequis as $key) {
            $currentSuccess = true;
            if (in_array($key, $missings)) {
                $success = false;
                $currentSuccess = false;
            }
            $this->o($currentSuccess, __("Executable {0}", $key));
        }
        return $success;
    }

    /**
     * Extrait les arguments de l'instance de l'horodateur entre la config et
     * les données saisies
     * @param array    $fields
     * @param stdClass $data
     * @return array
     * @noinspection PhpUnusedPrivateMethodInspection - utilisé dans $this->invokeMethod($check, 'getDriverArgs');
     */
    private function getDriverArgs(array $fields, $data): array
    {
        $decryptKey = hash('sha256', AdminsController::DECRYPT_KEY);
        $args = [];
        foreach ($fields as $field => $params) {
            $value = null;
            if (!empty($params['read_config'])) {
                $value = Configure::read($params['read_config']);
            } elseif (isset($data->$field)) {
                $value = $data->$field;
            } elseif (!empty($params['default'])) {
                $value = $params['default'];
            }
            if ($value && isset($params['type']) && $params['type'] === 'password') {
                $value = Security::decrypt(
                    base64_decode($value),
                    $decryptKey
                );
            }
            $args[$field] = $value ?: '';
        }
        return $args;
    }
}
