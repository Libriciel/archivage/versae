<?php

/**
 * Versae\Shell\Cron\DeleteExpiredData
 */

namespace Versae\Cron;

use AsalaeCore\Console\ConsoleOutput;
use AsalaeCore\Console\ConsoleTrait;
use AsalaeCore\Cron\CronInterface;
use Cake\Console\ConsoleOutput as CakeConsoleOutput;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\Stub\ConsoleOutput as TestConsoleOutput;
use DateTime;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Versae\Model\Entity\Fileupload;

/**
 * Cron de suppression des données expirées en base et sur le filesystem
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DeleteExpiredData implements CronInterface
{
    use ConsoleTrait;

    /**
     * Constructeur de la classe du cron
     * @param array                                    $params paramètres additionnels
     *                                                         du cron
     * @param CakeConsoleOutput|TestConsoleOutput|null $out
     * @param CakeConsoleOutput|TestConsoleOutput|null $err
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->out = $out ?: new ConsoleOutput('php://stdout');
        $this->err = $err ?: new ConsoleOutput('php://stderr');
        $this->out->setOutputAs(CakeConsoleOutput::COLOR);
        $this->err->setOutputAs(CakeConsoleOutput::COLOR);
    }

    /**
     * @inheritDoc
     */
    public static function getVirtualFields(): array
    {
        return [];
    }

    /**
     * stderr ou stdout de $message selon $success
     * @param bool   $success
     * @param string $message
     */
    private function o(bool $success, string $message): void
    {
        if ($success) {
            $this->out($message . ' <success>OK</success>');
        } else {
            $this->err($message . ' <error>Fail</error>');
        }
    }

    /**
     * fonction principale
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     * @throws Exception
     */
    public function work(EntityInterface $exec = null, EntityInterface $cron = null): string
    {
        $this->deleteExpiredUrl();
        $this->out('');
        $this->cleanUploadFolder();
        $this->out('');
        $this->cleanTestFormFiles();
        $this->out('');

        return 'success';
    }

    /**
     * Suppression des urls d'authentification expirés
     * @return void
     */
    protected function deleteExpiredUrl(): void
    {
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $expiredAuthUrls = $AuthUrls->find()->where(['expire <' => new DateTime()]);
        if ($expiredAuthUrls->count() > 0) {
            $this->out(__("Suppression des url d'authentification"));
            /** @var EntityInterface $expiredAuthUrl */
            foreach ($expiredAuthUrls as $expiredAuthUrl) {
                $this->o(
                    true,
                    '-> ' . __(
                        "Suppression de l'url d'authentification {0} qui a expiré le {1}",
                        $expiredAuthUrl->get('url'),
                        $expiredAuthUrl->get('expire')
                    )
                );
                $AuthUrls->delete($expiredAuthUrl);
            }
        } else {
            $this->out(__("Aucune url d'authentification à supprimer"));
        }
    }

    /**
     * nettoyage du dossier upload :
     *      supprimer les sous dossiers vides du dossier <App.paths.data>/upload
     *      dont la date de modification est antérieure à 24 heures avant la date courante
     * @return void
     * @throws Exception
     */
    protected function cleanUploadFolder(): void
    {
        $path = rtrim(Configure::read('App.paths.data'), DS) . DS . 'upload' . DS;
        $limitDate = time() - 24 * 3600;
        $count = 0;
        foreach (glob($path . '{*,*/*}', GLOB_BRACE | GLOB_ONLYDIR) as $dir) {
            if (count(scandir($dir)) === 2 && filemtime($dir) <= $limitDate) {
                $this->o(
                    true,
                    '-> ' . __("Suppression du dossier vide {0}", $dir)
                );
                $count++;
                Filesystem::remove($dir);
            }
        }
        if (!$count) {
            $this->out(__("Aucun fichier vide dans '$path' à supprimer"));
        }
    }

    /**
     * nettoyage des fichiers utilisés pour le test des formulaires :
     *      lire en base de données les enregistrements de la table fileuploads
     *      pour lesquels le path commence par <App.paths.data>/form_test/,
     *      qui sont lockés et dont la date de création est antérieure à 24 heures par rapport à la date courante
     *      (plantage lors du test du formulaire) et les supprimer
     * @return void
     */
    protected function cleanTestFormFiles(): void
    {
        $path = rtrim(Configure::read('App.paths.data'), DS) . DS . 'form_test' . DS;
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $expiredFiles = $Fileuploads->find()
            ->where(
                [
                    'path LIKE' => $path . '%',
                    'created <' => time() - 24 * 3600,
                    'locked' => true,
                ]
            );

        if ($expiredFiles->count()) {
            $this->out(__("Suppression des fichiers de test des formulaires expirés"));
            /** @var Fileupload $file */
            foreach ($expiredFiles as $file) {
                $this->o(
                    true,
                    '-> ' . __("Suppression du fichier {0}", $file->get('path'))
                );
                $Fileuploads->delete($file);
            }
        } else {
            $this->out(__("Aucun fichiers de test des formulaires expiré à supprimer"));
        }
    }
}
