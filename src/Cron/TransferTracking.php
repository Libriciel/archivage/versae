<?php

/**
 * Versae\Cron\TransferTracking
 */

namespace Versae\Cron;

use AsalaeCore\Console\ConsoleLogTrait;
use AsalaeCore\Cron\CronInterface;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\DOMUtility;
use Cake\Console\ConsoleOutput as CakeConsoleOutput;
use Cake\Datasource\EntityInterface;
use Cake\Http\Client\Response;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\Stub\ConsoleOutput as TestConsoleOutput;
use Cake\Utility\Hash;
use DateTime;
use DOMDocument;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Versae\Exception\GenericException;
use Versae\Model\Entity\ArchivingSystem;
use Versae\Model\Table\DepositsTable;
use Versae\Model\Table\TransfersTable;

/**
 * Met à jour l'état des transferts en interrogent le SAE distant
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TransferTracking implements CronInterface
{
    use ConsoleLogTrait;

    /**
     * @var DepositsTable
     */
    private $Deposits;
    /**
     * @var TransfersTable
     */
    private $Transfers;

    /**
     * Constructeur de la classe du cron
     * @param array                                    $params paramètres
     *                                                         additionnels du cron
     * @param CakeConsoleOutput|TestConsoleOutput|null $out
     * @param CakeConsoleOutput|TestConsoleOutput|null $err
     */
    public function __construct(array $params = [], $out = null, $err = null)
    {
        $this->initializeOutput($out, $err);
        $this->Deposits = TableRegistry::getTableLocator()->get('Deposits');
        $this->Transfers = TableRegistry::getTableLocator()->get('Transfers');
    }

    /**
     * Liste des champs virtuels pour un cron donné, permet de générer le formulaire
     * À l'exception des checkbox, l'option 'required' est mise à true par défaut
     *
     * Exemple :
     * [
     *      'myField' => ['type' => 'date', 'label' => '...', 'default' => '', ...] // formControl
     * ]
     * @return array
     */
    public static function getVirtualFields(): array
    {
        return [];
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     */
    public function work(
        EntityInterface $exec = null,
        EntityInterface $cron = null
    ): string {
        try {
            $this->trackAcknowledge();
            $this->trackReply();
            return 'success';
        } catch (Exception $e) {
            $this->err(
                sprintf(
                    'Error: %s',
                    $e->getMessage()
                )
            );
            $stack = preg_replace(
                '#' . preg_quote(ROOT) . '#',
                'ROOT',
                $e->getTraceAsString(),
            );
            $this->err($stack);
            return 'error';
        }
    }

    /**
     * Défini un transfert en erreur
     * @param EntityInterface $transfer
     * @param string          $message
     */
    private function setInError(EntityInterface $transfer, string $message): void
    {
        $this->Deposits->transitionOrFail($transfer->get('deposit'), DepositsTable::T_ERROR);

        $transfer->set('error_message', $message);
        $this->Transfers->saveOrFail($transfer);
        $this->Deposits->saveOrFail($transfer->get('deposit'));
    }

    /**
     * Mémorise la réponse dans le transfert
     * @param EntityInterface $transfer
     * @param Response        $response
     */
    private function saveResponse(EntityInterface $transfer, Response $response): void
    {
        $this->Transfers->patchEntity(
            $transfer,
            [
                'last_comm_date' => new DateTime(),
                'last_comm_code' => $response->getStatusCode(),
                'last_comm_message' => $response->getBody()->getContents(),
            ],
            ['validate' => false]
        );
    }

    /**
     * Vérifie l'accusé de réception
     */
    private function trackAcknowledge(): void
    {
        $query = $this->Transfers->find()
            ->where(
                [
                    'Deposits.state IN' => [
                        DepositsTable::S_SENT,
                        DepositsTable::S_ACKNOWLEDGING_ERROR,
                    ],
                ]
            )
            ->contain(
                [
                    'Deposits' => [
                        'Forms' => ['ArchivingSystems'],
                    ],
                ]
            )
            ->order(['Transfers.id']);
        $this->out(__("Transferts en état 'envoyé': {0}", $query->count()));
        /** @var EntityInterface $transfer */
        foreach ($query as $transfer) {
            /** @var ArchivingSystem $archivingSystem */
            $archivingSystem = Hash::get($transfer, 'deposit.form.archiving_system');
            try {
                $response = $archivingSystem->getAcknowledgement($transfer);
                $this->saveResponse($transfer, $response);
                $dom = new DOMDocument();
                switch (Hash::get($transfer, 'deposit.form.seda_version')) {
                    case 'seda1.0':
                        $schema = SEDA_V10_XSD;
                        break;
                    case 'seda2.1':
                        $schema = SEDA_V21_XSD;
                        break;
                    case 'seda2.2':
                        $schema = SEDA_V22_XSD;
                        break;
                    default:
                        throw new GenericException();
                }
                libxml_use_internal_errors(true);
                if ($response->getStatusCode() >= 400) {
                    $message = __("Erreur HTTP code : {0}", $response->getStatusCode());
                    $this->err('transfer_identifier = ' . $transfer->get('identifier') . ' : ' . $message);
                    $this->setInError($transfer, $message);
                } elseif (!($dom->loadXML($response->getStringBody()))) {
                    $message = __("Erreur, la réponse n'est pas un XML valide");
                    $this->err('transfer_identifier = ' . $transfer->get('identifier') . ' : ' . $message);
                    $this->setInError($transfer, $message);
                } elseif (!$dom->schemaValidate($schema)) {
                    $message = __("Erreur, la réponse n'est pas un fichier SEDA valide");
                    $this->err('transfer_identifier = ' . $transfer->get('identifier') . ' : ' . $message);
                    $this->setInError($transfer, $message);
                } else {
                    $this->Transfers->saveOrFail($transfer);
                    $this->Deposits->transitionOrFail($transfer->get('deposit'), DepositsTable::T_ACKNOWLEDGE);
                    $this->Deposits->saveOrFail($transfer->get('deposit'));
                    Filesystem::remove($transfer->get('zip'));
                }
            } catch (Exception $e) {
                $this->err(
                    'transfer_identifier = ' . $transfer->get('identifier') . ' : '
                    . 'Exception code ' . $e->getCode() . ' ' . $e->getMessage()
                );
                $this->setInError($transfer, $e->getMessage());
            }
        }
    }

    /**
     * Vérifie la réponse du SAE
     */
    private function trackReply(): void
    {
        $query = $this->Transfers->find()
            ->where(
                [
                    'Deposits.state IN' => [
                        DepositsTable::S_RECEIVED,
                        DepositsTable::S_ANSWERING_ERROR,
                    ],
                ]
            )
            ->contain(
                [
                    'Deposits' => [
                        'Forms' => ['ArchivingSystems'],
                    ],
                ]
            )
            ->order(['Transfers.id']);
        $this->out(__("Transferts en état 'reçu': {0}", $query->count()));

        $Notify = Utility::get('Notify');
        /** @var EntityInterface $transfer */
        foreach ($query as $transfer) {
            /** @var ArchivingSystem $archivingSystem */
            $archivingSystem = Hash::get($transfer, 'deposit.form.archiving_system');
            try {
                $response = $archivingSystem->getReply($transfer);
                $this->saveResponse($transfer, $response);
                $dom = new DOMDocument();
                switch (Hash::get($transfer, 'deposit.form.seda_version')) {
                    case 'seda1.0':
                        $schema = SEDA_V10_XSD;
                        break;
                    case 'seda2.1':
                        $schema = SEDA_V21_XSD;
                        break;
                    case 'seda2.2':
                        $schema = SEDA_V22_XSD;
                        break;
                    default:
                        throw new GenericException();
                }
                libxml_use_internal_errors(true);
                if ($response->getStatusCode() >= 400) {
                    $message = __("Erreur HTTP code : {0}", $response->getStatusCode());
                    $this->err('transfer_identifier = ' . $transfer->get('identifier') . ' : ' . $message);
                    $this->setInError($transfer, $message);
                } elseif (!($dom->loadXML($response->getStringBody()))) {
                    $message = __("Erreur, la réponse n'est pas un XML valide");
                    $this->err('transfer_identifier = ' . $transfer->get('identifier') . ' : ' . $message);
                    $this->setInError($transfer, $message);
                } elseif (($msg = $dom->getElementsByTagName('message')->item(0))) {
                    $message = $msg->nodeValue;
                    $this->out('transfer_identifier = ' . $transfer->get('identifier') . ' : ' . $message);
                } elseif (!$dom->schemaValidate($schema)) {
                    $message = __("Erreur, la réponse n'est pas un fichier SEDA valide");
                    $this->err('transfer_identifier = ' . $transfer->get('identifier') . ' : ' . $message);
                    $this->setInError($transfer, $message);
                } else {
                    $util = new DOMUtility($dom);
                    $code = $util->nodeValue('//ns:ReplyCode');
                    $accepted = $code === '000';
                    $this->Deposits->transitionOrFail(
                        $transfer->get('deposit'),
                        $accepted
                            ? DepositsTable::T_ACCEPT
                            : DepositsTable::T_REFUSE
                    );
                    $this->Transfers->saveOrFail($transfer);
                    $this->Deposits->saveOrFail($transfer->get('deposit'));
                    if ($accepted) {
                        $this->deleteUploads($transfer->get('deposit'));
                    } else {
                        $Notify->send(
                            $transfer->get('created_user_id'),
                            [],
                            __("Votre versement ''{0}'' a été rejeté", Hash::get($transfer, 'deposit.name')),
                            'alert-warning'
                        );
                    }
                }
            } catch (Exception $e) {
                $this->err(
                    'transfer_identifier = ' . $transfer->get('identifier')
                    . ' : ' . 'Exception code ' . $e->getCode() . ' ' . $e->getMessage()
                );
                $this->setInError($transfer, $e->getMessage());
            }
        }
    }

    /**
     * Supprime les fichier uploads
     * @param EntityInterface $deposit
     * @return void
     */
    private function deleteUploads(EntityInterface $deposit): void
    {
        $path = $deposit->get('path');
        $user_id = $deposit->get('created_user_id');
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        foreach (glob($path . '/upload/*/*') as $dir) {
            $fileupload_id = basename($dir);
            if (!is_numeric($fileupload_id)) {
                throw new GenericException();
            }
            $fileupload = $Fileuploads->find()
                ->where(['id' => $fileupload_id, 'user_id' => $user_id])
                ->first();
            if ($fileupload) {
                $Fileuploads->delete($fileupload);
            }
        }
        Filesystem::removeEmptySubFolders($path);
    }
}
