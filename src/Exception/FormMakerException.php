<?php

/**
 * Versae\Exception\FormMakerException
 */

namespace Versae\Exception;

use Exception;

/**
 * Exception du FormMakerForm
 *
 * @category Exception
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FormMakerException extends Exception
{
    /**
     * @var string
     */
    public $field;

    /**
     * Création de l'exception
     * @param string $field
     * @param string $error
     * @return FormMakerException
     */
    public static function create(string $field, string $error): FormMakerException
    {
        $exception = new self($error);
        $exception->field = $field;
        return $exception;
    }
}
