<?php

/**
 * Versae\Exception\KilledException
 */

namespace Versae\Exception;

use Cake\Http\Exception\HttpException;

/**
 * Envoyé si le processus nécessite d'être interrompu
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class KilledException extends HttpException
{

    /**
     * Default exception code
     *
     * @var int
     */
    protected $_defaultCode = 499;

    /**
     * Construct the exception. Note: The message is NOT binary safe.
     * @link http://php.net/manual/en/exception.construct.php
     * @param string $message  [optional] The Exception message to throw.
     * @param null   $code     [optional] The Exception code.
     * @param null   $previous [optional] The previous throwable used for the exception chaining.
     * @since 5.1.0
     */
    public function __construct($message = '', $code = null, $previous = null)
    {
        $message = $message ?: 'Client Closed Request';
        parent::__construct($message, $code, $previous);
    }
}
