<?php

/**
 * Versae\Exception\GenericException
 */

namespace Versae\Exception;

use Cake\Http\Exception\HttpException;

/**
 * Exception générique
 *
 * @category Exception
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class GenericException extends HttpException
{
}
