<?php

/**
 * Versae\Model\Entity\FormFieldset
 */

namespace Versae\Model\Entity;

use AsalaeCore\Model\Entity\JsonFieldEntityTrait;
use AsalaeCore\ORM\Entity;
use Cake\ORM\TableRegistry;
use Versae\Model\Table\FormFieldsetsTable;
use Versae\Model\Table\FormInputsTable;

/**
 * Entité de la table form_fieldsets
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FormFieldset extends Entity
{
    use JsonFieldEntityTrait;

    /**
     * @var string[] Liste des champs de type JsonString
     */
    public array $jsonFields = ['disable_expression'];

    /**
     * @var string nom de la méthode à executer
     */
    private string $jsonFieldFilter = 'jsonFieldFilter';

    /**
     * @var array[] champs virtuels à ratacher à l'objet JsonString
     */
    public array $jsonVirtualFields = [
        'disable_expression' => [
            'cond_display_select',
            'cond_display_input',
            'cond_display_input_type',
            'cond_display_value_select',
            'cond_display_value_file',
            'cond_display_value',
            'cond_display_way',
            'cond_display_field',
        ],
    ];

    /**
     * Champ virtuel
     * @return string
     */
    protected function _getCondDisplaySelecttrad()
    {
        $format = $this->_fields['cond_display_select'] ?? '';
        switch ($format) {
            case FormFieldsetsTable::COND_DISPLAY_CHECKBOX_CHECKED:
                $format = __dx(
                    'form-extractor',
                    'cond_display_select',
                    "Une case à cocher est cochée"
                );
                break;
            case FormFieldsetsTable::COND_DISPLAY_CHECKBOX_NOT_CHECKED:
                $format = __dx(
                    'form-extractor',
                    'cond_display_select',
                    "Une case à cocher n'est pas cochée"
                );
                break;
            case FormFieldsetsTable::COND_DISPLAY_INPUT_VALUE:
                $format = __dx(
                    'form-extractor',
                    'cond_display_select',
                    "Un champ a une valeur particulière"
                );
                break;
            case FormFieldsetsTable::COND_DISPLAY_INPUT_NOT_VALUE:
                $format = __dx(
                    'form-extractor',
                    'cond_display_select',
                    "Un champ n'a pas une valeur particulière"
                );
                break;
            case FormFieldsetsTable::COND_DISPLAY_INPUT_EMPTY:
                $format = __dx(
                    'form-extractor',
                    'cond_display_select',
                    "Un champ est vide"
                );
                break;
            case FormFieldsetsTable::COND_DISPLAY_INPUT_NOT_EMPTY:
                $format = __dx(
                    'form-extractor',
                    'cond_display_select',
                    "Un champ n'est pas vide"
                );
                break;
        }
        return $format;
    }

    /**
     * Traduction de la disableExpression
     * @return string
     */
    protected function _getReadableDisableExpression(): string
    {
        $dInput = $this->get('cond_display_input');

        if (empty($dInput) || !$this->get('form_id')) {
            return '';
        }

        $dValue = $this->get('cond_display_value');
        $query = TableRegistry::getTableLocator()->get('FormInputs')
            ->find()
            ->where(['name' => $this->get('cond_display_input')]);
        $selectLike = [FormInputsTable::TYPE_SELECT, FormInputsTable::TYPE_MULTI_CHECKBOX, FormInputsTable::TYPE_RADIO];
        switch ($this->get('cond_display_select')) {
            case FormFieldsetsTable::COND_DISPLAY_CHECKBOX_CHECKED:
                return __("La case à cocher \"{0}\" est cochée", $dInput);

            case FormFieldsetsTable::COND_DISPLAY_CHECKBOX_NOT_CHECKED:
                return __("La case à cocher \"{0}\" est décochée", $dInput);

            case FormFieldsetsTable::COND_DISPLAY_INPUT_VALUE:
                $input = $query->firstOrFail();
                $value = in_array($input->get('type'), $selectLike)
                    ? json_decode($input->get('options'), true)[$dValue]['value']
                    : $dValue;
                return __("Le champ \"{0}\" a la valeur \"{1}\"", $dInput, $value);

            case FormFieldsetsTable::COND_DISPLAY_INPUT_NOT_VALUE:
                $input = $query->firstOrFail();
                $value = in_array($input->get('type'), $selectLike)
                    ? json_decode($input->get('options'), true)[$dValue]['value']
                    : $dValue;
                return __("Le champ \"{0}\" n'a pas la valeur \"{1}\"", $dInput, $value);

            case FormFieldsetsTable::COND_DISPLAY_INPUT_EMPTY:
                return __("Le champ \"{0}\" est vide", $dInput);

            case FormFieldsetsTable::COND_DISPLAY_INPUT_NOT_EMPTY:
                return __("Le champ \"{0}\" n'est pas vide", $dInput);
            default:
                return '';
        }
    }

    /**
     * Filtre custom pour les valeur 'null' en string
     * @param mixed $value
     * @return bool
     */
    public function jsonFieldFilter($value): bool
    {
        return $value !== null
            && !in_array($value, [0, '', false, [], 'null'], true);
    }
}
