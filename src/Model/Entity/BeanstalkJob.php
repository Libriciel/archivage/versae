<?php

/**
 * Versae\Model\Entity\BeanstalkJob
 */

namespace Versae\Model\Entity;

use AsalaeCore\Factory\Utility;
use Beanstalk\Model\Entity\BeanstalkJob as BeanstalkBeanstalkJob;
use Beanstalk\Utility\Beanstalk;
use Exception;

/**
 * Entité de la table beanstalk_jobs
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class BeanstalkJob extends BeanstalkBeanstalkJob
{

    /**
     * Donne une instance de Beanstalk
     * @return Beanstalk
     * @throws Exception
     */
    public function getBeanstalk(): Beanstalk
    {
        $tube = !empty($this->_fields['tube']) ? $this->_fields['tube'] : 'default';
        return Utility::get('Beanstalk')->setTube($tube);
    }
}
