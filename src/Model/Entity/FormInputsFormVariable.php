<?php

/**
 * Versae\Model\Entity\FormInputsFormVariable
 */

namespace Versae\Model\Entity;

use AsalaeCore\ORM\Entity;

/**
 * Entité de la table form_inputs_form_variables
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FormInputsFormVariable extends Entity
{
}
