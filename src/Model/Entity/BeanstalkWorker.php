<?php

/**
 * Versae\Model\Entity\BeanstalkWorker
 */

namespace Versae\Model\Entity;

use Beanstalk\Model\Entity\BeanstalkWorker as BeanstalkBeanstalkWorker;

/**
 * Entité de la table beanstalk_workers
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class BeanstalkWorker extends BeanstalkBeanstalkWorker
{
}
