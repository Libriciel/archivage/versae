<?php

/**
 * Versae\Model\Entity\FormUnitKeywordDetail
 */

namespace Versae\Model\Entity;

use AsalaeCore\ORM\Entity;

/**
 * Entité de la table form_unit_keyword_details
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FormUnitKeywordDetail extends Entity
{
}
