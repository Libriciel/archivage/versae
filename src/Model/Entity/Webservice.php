<?php

/**
 * Versae\Model\Entity\Webservice
 */

namespace Versae\Model\Entity;

use AsalaeCore\Model\Entity\Webservice as WebserviceCore;

/**
 * Entité de la table webservices
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Webservice extends WebserviceCore
{
}
