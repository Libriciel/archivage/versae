<?php

/**
 * Versae\Model\Entity\Session
 */

namespace Versae\Model\Entity;

use AsalaeCore\Model\Entity\Session as CoreSession;

/**
 * Entité de la table sessions
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Session extends CoreSession
{
}
