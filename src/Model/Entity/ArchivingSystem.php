<?php

/**
 * Versae\Model\Entity\ArchivingSystem
 */

namespace Versae\Model\Entity;

use AsalaeCore\Model\Entity\ArchivingSystem as CoreArchivingSystem;
use Cake\ORM\TableRegistry;

/**
 * Entité de la table archiving_systems
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchivingSystem extends CoreArchivingSystem
{
    /**
     * Urls des apis
     * @var string[]
     */
    protected $apiCalls = [
        // asalae
        'transfer' => '/api/transfers',
        'transfer_chunk' => '/api/transfers/prepare-chunked',
        // versae
        'dl_zip' => '/api/transfers/zip',
        'after_dl_zip' => null,
    ];

    /**
     * Cette entité peut-elle être retirée :
     *      - true si aucun formulaire associé
     * @return bool
     */
    protected function _getRemovable(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        return TableRegistry::getTableLocator()->get('FormsTransferringAgencies')->find()
            ->innerJoinWith('Forms')
            ->where(['Forms.archiving_system_id' => $this->_fields['id']])
            ->count() === 0;
    }
}
