<?php

/**
 * Versae\Model\Entity\FormUnitKeyword
 */

namespace Versae\Model\Entity;

use AsalaeCore\ORM\Entity;

/**
 * Entité de la table form_unit_keywords
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FormUnitKeyword extends Entity
{
}
