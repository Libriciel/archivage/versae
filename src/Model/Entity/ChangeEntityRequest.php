<?php

/**
 * Versae\Model\Entity\ChangeEntityRequest
 */

namespace Versae\Model\Entity;

use Cake\ORM\Entity;

/**
 * Entité de la table change_entity_requests
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2023, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ChangeEntityRequest extends Entity
{
}
