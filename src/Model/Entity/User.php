<?php

/**
 * Versae\Model\Entity\User
 */

namespace Versae\Model\Entity;

use AsalaeCore\Model\Entity\User as CoreUser;

/**
 * Entité de la table users
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class User extends CoreUser
{
    /**
     * Vrai si l'utilisateur peut être supprimé
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        return true;
    }
}
