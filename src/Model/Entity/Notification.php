<?php

/**
 * Versae\Model\Entity\Notification
 */

namespace Versae\Model\Entity;

use AsalaeCore\Model\Entity\Notification as CoreNotification;

/**
 * Entité de la table notifications
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Notification extends CoreNotification
{
}
