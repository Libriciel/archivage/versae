<?php

/**
 * Versae\Model\Entity\RgpdInfo
 */

namespace Versae\Model\Entity;

use Cake\ORM\Entity;

/**
 * Entité de la table rgpd_infos
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2023, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class RgpdInfo extends Entity
{
}
