<?php

/**
 * Versae\Model\Entity\Version
 */

namespace Versae\Model\Entity;

use AsalaeCore\Model\Entity\Version as CoreVersion;

/**
 * Entité de la table versions
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Version extends CoreVersion
{
}
