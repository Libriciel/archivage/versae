<?php

/**
 * Versae\Model\Entity\Transfer
 */

namespace Versae\Model\Entity;

use AsalaeCore\ORM\Entity;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Exception;

/**
 * Entité de la table transfers
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Transfer extends Entity
{
    /**
     * Transfer constructor.
     * @param array $properties
     * @param array $options
     * @throws Exception
     */
    public function __construct(array $properties = [], array $options = [])
    {
        $properties += [
            'path_token' => bin2hex(random_bytes(4)),
        ];
        parent::__construct($properties, $options);
    }

    /**
     * Donne le chemin complet
     * @return string
     * @throws Exception
     */
    protected function _getPath(): string
    {
        if (empty($this->_fields['deposit_id'])) {
            return '';
        }
        if (
            empty($this->_fields['deposit'])
            || !$this->_fields['deposit'] instanceof EntityInterface
        ) {
            $Deposits = TableRegistry::getTableLocator()->get('Deposits');
            $this->_fields['deposit'] = $Deposits->get($this->_fields['deposit_id']);
        }
        return $this->_fields['deposit']->get('path');
    }

    /**
     * Donne le chemin vers le fichier tar.gz (utilisé dans asalae-core)
     * @return string
     * @throws Exception
     */
    protected function _getZip(): string
    {
        if (Configure::read('Transfers.use_zip')) {
            return $this->_getPath() . '/transfer.zip';
        }
        return $this->_getPath() . '/transfer.tar.gz';
    }
}
