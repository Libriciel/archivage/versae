<?php

/**
 * Versae\Model\Entity\Form
 */

namespace Versae\Model\Entity;

use AsalaeCore\ORM\Entity;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;
use Versae\Model\Table\FormInputsTable;
use Versae\Model\Table\FormsTable;
use Versae\Model\Table\FormUnitsTable;

/**
 * Entité de la table forms
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Form extends Entity
{
    /**
     * Champs virtuels
     * @var array
     */
    protected $_virtual = [
        'deletable',
        'editable',
        'not_publishable_reason',
        'publishable',
        'statetrad',
        'unpublishable',
        'versionable',
    ];

    /**
     * @var EntityInterface[]|null mise en cache du champs virtuel prev_versions
     */
    private $prev_versions = null;

    /**
     * @var bool|null mise en cache du champs virtuel publishable
     */
    private $publishable = null;

    /**
     * @var string|null mise en cache du champs virtuel not_publishable_reason
     * À améliorer : tableau avec toutes les raisons, si vide -> publishable
     */
    private $not_publishable_reason = null;

    /**
     * Traductions des états
     * @return string
     */
    protected function _getStatetrad(): string
    {
        $state = $this->_fields['state'] ?? '';
        switch ($state) {
            case FormsTable::S_EDITING:
                $state = __dx('form', 'state', "en cours d'édition");
                break;
            case FormsTable::S_PUBLISHED:
                $state = __dx('form', 'state', "publié");
                break;
            case FormsTable::S_DEPRECATED:
                $state = __dx('form', 'state', "déprécié");
                break;
            case FormsTable::S_DEACTIVATED:
                $state = __dx('form', 'state', "désactivé");
                break;
        }
        return $state;
    }

    /**
     * Vrai si le formulaire peut être supprimé :
     *      état editing
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        return $this->_fields['state'] === FormsTable::S_EDITING;
    }

    /**
     * Vrai si une nouvelle version peut être crée :
     *      état publié ou désactivé
     *      ET
     *      c'est déjà la version la plus récente
     *      ET
     *      aucun en cours d'édition avec le même identifier
     * @return bool
     */
    protected function _getVersionable(): bool
    {
        if (!in_array($this->_fields['state'] ?? null, [FormsTable::S_PUBLISHED, FormsTable::S_DEACTIVATED])) {
            return false;
        }

        $Forms = TableRegistry::getTableLocator()->get('Forms');

        $query = $Forms->find()->where(
            [
                'OR' => [
                    [
                        'identifier' => $this->_fields['identifier'],
                        'state' => FormsTable::S_EDITING,
                    ],
                    [
                        'identifier' => $this->_fields['identifier'],
                        'version_number >' => $this->_fields['version_number'],
                    ]
                ]
            ]
        );
        return $query->count() === 0;
    }

    /**
     * Vrai si le formulaire peut être publié :
     *      état editing
     *      ET
     *      au moins un input fichier ou archive
     *      ET
     *      au moins une unité d'archive
     * Si faux, met à jour le champs virtuel not_publishable_reason
     * @return bool
     */
    protected function _getPublishable(): bool
    {
        if ($this->publishable !== null) {
            return $this->publishable;
        }

        if (empty($this->_fields['id'])) {
            $this->not_publishable_reason = __("Entité non sauvegardée");
            return $this->publishable = false;
        }

        // État n'est pas editing
        if ($this->_fields['state'] !== FormsTable::S_EDITING) {
            $this->not_publishable_reason = __("Le Formulaire n'est pas en cours d'édition");
            return $this->publishable = false;
        }

        // Absence de champs file ou archive_file
        $subquery = TableRegistry::getTableLocator()->get('FormFieldsets')->find()
            ->from(['fieldset' => 'form_fieldsets'])
            ->select(['form_fieldset_id' => 'fieldset.id'])
            ->where(['form_id' => $this->_fields['id']]);

        $FormInputs = TableRegistry::getTableLocator()->get('FormInputs');
        $count = $FormInputs->find()
            ->from(['input' => 'form_inputs'])
            ->select(['existing' => 1])
            ->where(
                [
                    'input.form_fieldset_id IN' => $subquery,
                    'input.type IN' => [FormInputsTable::TYPE_FILE, FormInputsTable::TYPE_ARCHIVE_FILE],
                ]
            )
            ->limit(1)
            ->count();

        if (!$count) {
            $this->not_publishable_reason = __("Le Formulaire ne contient aucun champ Fichier ou ZIP");
            return $this->publishable = false;
        }

        // Absence de FormUnit Document ou Lot de document ou Tree root
        $FormUnits = TableRegistry::getTableLocator()->get('FormUnits');
        $count = $FormUnits->find()
            ->select(['existing' => 1])
            ->where(
                [
                    'form_id' => $this->_fields['id'],
                    'type IN' => [
                        FormUnitsTable::TYPE_DOCUMENT,
                        FormUnitsTable::TYPE_DOCUMENT_MULTIPLE,
                        FormUnitsTable::TYPE_TREE_ROOT,
                    ],
                ]
            )
            ->limit(1)
            ->count();

        if (!$count) {
            $this->not_publishable_reason = __(
                "Le Formulaire ne contient aucune unité d'archives Document, Lot de document ou "
                    . "Arborescence issue d'un fichier compressé (ZIP)"
            );
            return $this->publishable = false;
        }

        // Un champ multiple ne contient aucune option
        $FormInputs = TableRegistry::getTableLocator()->get('FormInputs');
        $inputs = $FormInputs->find()
            ->where(
                [
                    'form_id' => $this->_fields['id'],
                    'type IN' => [
                        FormInputsTable::TYPE_SELECT,
                        FormInputsTable::TYPE_MULTI_CHECKBOX,
                        FormInputsTable::TYPE_RADIO
                    ]
                ]
            )
            ->all();

        foreach ($inputs as $input) {
            if (!json_decode($input->get('options'))) {
                $this->not_publishable_reason = __(
                    "Le champ \"{0}\" de type \"{1}\" ne contient aucune option",
                    $input->get('name'),
                    $input->get('type')
                );
                return $this->publishable = false;
            }
        }

        return true;
    }

    /**
     * Donne la première raison trouvée qui rend le formulaire non publiable
     * @return string
     */
    protected function _getNotPublishableReason(): string
    {
        if ($this->publishable === null) {
            $this->_getPublishable();
        }

        return $this->not_publishable_reason ?? '';
    }

    /**
     * Vrai si le formulaire peut être réutilisé :
     *      état published
     *      ET
     *      il n'existe pas une version plus récente en cours d'édition
     *      ET
     *      aucun versement en cours
     * @return bool
     */
    protected function _getUnpublishable(): bool
    {
        if (empty($this->_fields['id']) || $this->_fields['state'] !== FormsTable::S_PUBLISHED) {
            return false;
        }
        $loc = TableRegistry::getTableLocator();

        $form = $loc->get('Forms')
            ->find()
            ->select(['existing' => 1])
            ->where(
                [
                    'identifier' => $this->_fields['identifier'],
                    'version_number >' => $this->_fields['version_number'],
                ]
            );
        if ($form->count()) {
            return false;
        }

        $deposits = $loc->get('Deposits')
            ->find()
            ->select(['existing' => 1])
            ->where(['form_id' => $this->_fields['id']]);

        return $deposits->count() === 0;
    }

    /**
     * Précédentes versions du formulaire par numéro de version décroissant
     * @return EntityInterface[]
     */
    protected function _getPrevVersions(): array
    {
        if ($this->prev_versions === null) {
            $this->prev_versions = TableRegistry::getTableLocator()->get('Forms')
                ->find()
                ->where(
                    [
                        'identifier' => $this->_fields['identifier'],
                        'version_number <' => $this->_fields['version_number'],
                    ]
                )
                ->order(['version_number' => 'desc'])
                ->all()
                ->toArray();
        }
        return $this->prev_versions;
    }

    /**
     * @return EntityInterface|false
     */
    protected function _getLastVersion()
    {
        $prevVersion = $this->_getPrevVersions();

        return current($prevVersion);
    }

    /**
     * Toutes les versions du formulaire
     * @return array
     */
    protected function _getAllVersions(): array
    {
        return TableRegistry::getTableLocator()->get('Forms')
            ->find()
            ->where(
                [
                    'identifier' => $this->_fields['identifier'],
                ]
            )
            ->order(['created' => 'asc'])
            ->all()
            ->toArray();
    }

    /**
     * Editable
     * @return bool
     */
    protected function _getEditable(): bool
    {
        return ($this->_fields['state'] ?? null) === FormsTable::S_EDITING;
    }

    /**
     * Nom du fichier export_filename
     * @return string
     */
    protected function _getExportFilename(): string
    {
        return sprintf(
            'export-form-%s-%s-v%d.json',
            date('Y-m-d'),
            Text::slug($this->_fields['name'] ?? ''),
            $this->_fields['version_number']
        );
    }
}
