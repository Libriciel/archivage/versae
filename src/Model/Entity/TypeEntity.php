<?php

/**
 * Versae\Model\Entity\TypeEntity
 */

namespace Versae\Model\Entity;

use AsalaeCore\Model\Entity\TypeEntity as CoreTypeEntity;

/**
 * Entité de la table type_entities
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TypeEntity extends CoreTypeEntity
{
}
