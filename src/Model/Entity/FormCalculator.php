<?php

/**
 * Versae\Model\Entity\FormCalculator
 */

namespace Versae\Model\Entity;

use AsalaeCore\ORM\Entity;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;

/**
 * Entité de la table form_calculators
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FormCalculator extends Entity
{
    /**
     * Champs virtuels
     * @var array
     */
    protected $_virtual = [
        'deletable',
        'linked',
    ];

    /**
     * Entité supprimable
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        return empty($this->_getLinked());
    }

    /**
     * Liste les utilisations de ce calculateur
     * @return array
     */
    protected function _getLinked()
    {
        if (!$this->id) {
            return [];
        }
        $linkedVariables = TableRegistry::getTableLocator()->get('FormVariables')
            ->find()
            ->innerJoinWith('FormCalculatorsFormVariables')
            ->where(['form_calculator_id' => $this->id])
            ->contain(
                [
                    'FormCalculators',
                    'FormTransferHeaders',
                    'FormUnitContents' => ['FormUnits'],
                    'FormUnitHeaders' => ['FormUnits'],
                    'FormUnitManagements' => ['FormUnits'],
                    'FormUnitKeywordDetails' => [
                        'FormUnitKeywords' => 'FormUnits'
                    ],
                ]
            );
        $linked = [];
        $checkModels = [
            'form_calculator',
            'form_transfer_header',
            'form_unit_content',
            'form_unit_header',
            'form_unit_management',
            'form_unit_keyword_detail',
        ];
        /** @var EntityInterface $formVariable */
        foreach ($linkedVariables as $formVariable) {
            foreach ($checkModels as $modelName) {
                /** @var EntityInterface|null $linkedModel */
                $linkedModel = $formVariable->get($modelName);
                $archiveUnitPath = "$modelName.form_unit";
                if ($modelName === 'form_unit_keyword_detail') {
                    $archiveUnitPath = "$modelName.form_unit_keyword.form_unit";
                }
                /** @var EntityInterface|null $formUnit */
                $formUnit = Hash::get($formVariable, $archiveUnitPath);
                if ($linkedModel && $formUnit) {
                    $linked[] = sprintf(
                        'archive_units.%s.%s.%s',
                        $formUnit->get('name'),
                        str_replace('form_unit_', '', $modelName),
                        $linkedModel->get('name')
                    );
                } elseif ($linkedModel) {
                    $linked[] = $modelName . '.' . $linkedModel->get('name');
                }
            }
        }

        $subQuery = TableRegistry::getTableLocator()->get('FormCalculators')
            ->find()
            ->select(['FormCalculators.form_variable_id'])
            ->where(['FormCalculators.id' => $this->id]);
        $formUnits = TableRegistry::getTableLocator()->get('FormUnits')
            ->find()
            ->where(['FormUnits.presence_condition_id IN' => $subQuery]);
        foreach ($formUnits as $formUnit) {
            $linked[] = sprintf(
                'archive_units.%s.presence_condition',
                $formUnit->get('name')
            );
        }

        return $linked;
    }
}
