<?php

/**
 * Versae\Model\Entity\Mail
 */

namespace Versae\Model\Entity;

use AsalaeCore\DataType\SerializedObject;
use AsalaeCore\Factory\Utility;
use AsalaeCore\ORM\Entity;
use Cake\Mailer\Mailer;
use Exception;

/**
 * Entité de la table mails
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Mail extends Entity
{
    /**
     * Donne l'object Mailer stocké en base
     * @return Mailer
     */
    public function getMailer(): Mailer
    {
        $serializedObject = $this->_getSerialized();
        return $serializedObject->getObject();
    }

    /**
     * Envoi l'email stocké
     * @return array
     * @throws Exception
     */
    public function send(): array
    {
        $mailer = $this->getMailer();

        /** @var Mailer $email */
        $defaultMailer = clone Utility::get(Mailer::class);
        $mailer->setTransport($defaultMailer->getTransport());

        return $mailer->send();
    }

    /**
     * Getter du champ data (json)
     * @return SerializedObject|mixed|null
     */
    protected function _getSerialized()
    {
        $value = $this->_fields['serialized'] ?? null;
        if (gettype($value) === 'resource') {
            rewind($value);
            $value = stream_get_contents($value);
        }
        return is_string($value)
            ? SerializedObject::createFromObject($value)
            : $value;
    }

    /**
     * Setter du champ data (json)
     * @param mixed $value
     * @return SerializedObject
     */
    protected function _setSerialized($value)
    {
        return is_array($value)
            ? new SerializedObject($value)
            : $value;
    }
}
