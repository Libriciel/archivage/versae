<?php

/**
 * Versae\Model\Entity\SavedFilter
 */

namespace Versae\Model\Entity;

use AsalaeCore\Model\Entity\SavedFilter as SavedFilterCore;

/**
 * Entité de la table saved_filters
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class SavedFilter extends SavedFilterCore
{
}
