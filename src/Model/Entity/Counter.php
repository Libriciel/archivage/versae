<?php

/**
 * Versae\Model\Table\SequencesTable
 */

namespace Versae\Model\Entity;

use AsalaeCore\Model\Entity\Counter as CoreCounter;

/**
 * Entité de la table Counters
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Counter extends CoreCounter
{
}
