<?php

/**
 * Versae\Model\Entity\Aro
 */

namespace Versae\Model\Entity;

use Acl\Model\Entity\Aro as AclAro;

/**
 * Entité de la table aros
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Aro extends AclAro
{
}
