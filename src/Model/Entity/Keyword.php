<?php

/**
 * Versae\Model\Entity\Keyword
 */

namespace Versae\Model\Entity;

use AsalaeCore\Model\Entity\Keyword as CoreKeyword;

/**
 * Entité de la table keywords
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Keyword extends CoreKeyword
{
}
