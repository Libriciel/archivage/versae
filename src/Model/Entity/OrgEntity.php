<?php

/**
 * Versae\Model\Entity\OrgEntity
 */

namespace Versae\Model\Entity;

use AsalaeCore\Model\Entity\OrgEntity as CoreOrgEntity;
use Cake\ORM\Association\HasMany;
use Cake\ORM\TableRegistry;

/**
 * Entité de la table org_entities
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class OrgEntity extends CoreOrgEntity
{
    /**
     * Champs virtuels
     *
     * @var array
     */
    protected $_virtual = [];

    /**
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        if ($this->_getCode() === 'SE') {
            return false;
        }
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $has = [
            'ArchivingSystems',
            'ChildOrgEntities',
            'Forms',
            'Ldaps',
            'Roles',
            'Users',
        ];
        foreach ($has as $modelName) {
            /** @var HasMany $assoc */
            $assoc = $OrgEntities->{$modelName};
            $query = $assoc->find()
                ->where([$assoc->getForeignKey() => $this->_fields['id']]);
            if ($query->count() > 0) {
                return false;
            }
        }

        return true;
    }
}
