<?php

/**
 * Versae\Model\Entity\FormUnit
 */

namespace Versae\Model\Entity;

use AsalaeCore\ORM\Entity;
use AsalaeCore\Utility\Translit;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\View\Helper\TextHelper;
use Cake\View\View;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Versae\Model\Table\FormUnitsTable;
use Versae\Utility\Twig;

/**
 * Entité de la table form_units
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FormUnit extends Entity
{
    /**
     * Champs virtuels
     * @var array
     */
    protected $_virtual = ['typetrad', 'storage_calculationtrad'];

    /**
     * Traductions des types
     * @return string
     */
    protected function _getTypetrad(): string
    {
        if (empty($this->_fields['type'])) {
            return '';
        }
        switch ($this->_fields['type']) {
            case FormUnitsTable::TYPE_DOCUMENT:
                return __dx('form_unit', 'type', "Document");
            case FormUnitsTable::TYPE_DOCUMENT_MULTIPLE:
                return __dx('form_unit', 'type', "Lot de documents");
            case FormUnitsTable::TYPE_SIMPLE:
                return __dx('form_unit', 'type', "Unité d'archives");
            case FormUnitsTable::TYPE_REPEATABLE:
                return __dx('form_unit', 'type', "Unité d'archives répétable");
            case FormUnitsTable::TYPE_TREE_SEARCH:
                return __dx('form_unit', 'type', "Recherche dans une arborescence");
            case FormUnitsTable::TYPE_TREE_PATH:
                return __dx('form_unit', 'type', "Chemin dans une arborescence");
            case FormUnitsTable::TYPE_TREE_ROOT:
                return __dx('form_unit', 'type', "Arborescence issue d'un fichier compressé (ZIP)");
        }
        return $this->_fields['type'];
    }

    /**
     * Traductions des storage_calculations
     * @return string
     */
    protected function _getStorageCalculationtrad(): string
    {
        if (empty($this->_fields['storage_calculation'])) {
            return '';
        }
        switch ($this->_fields['storage_calculation']) {
            case FormUnitsTable::STORAGE_CALCULATION_TREE_ELEMENT_NAME:
                return __dx(
                    'form_unit',
                    'storage_calculation',
                    "Nom de l'élément de l'arborescence"
                );
            case FormUnitsTable::STORAGE_CALCULATION_TREE_ELEMENT_NAME_INDEX:
                return __dx(
                    'form_unit',
                    'storage_calculation',
                    "Nom de l'élément de l'arborescence + index de la section répétable"
                );
            case FormUnitsTable::STORAGE_CALCULATION_ARCHIVE_UNIT_NAME:
                return __dx(
                    'form_unit',
                    'storage_calculation',
                    "Nom de l'unité d'archives (tronqué à 254 octets)"
                );
            case FormUnitsTable::STORAGE_CALCULATION_NONE:
                return __dx(
                    'form_unit',
                    'storage_calculation',
                    "Aucun"
                );
        }
        return $this->_fields['storage_calculation'];
    }

    /**
     * Donne le nom avec celui des parents séparés par des slashs (/)
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    protected function _getFullName(): string
    {
        $view = new View();
        $textHelper = new TextHelper($view);
        if (empty($this->_fields['form_id'])) {
            return $this->_fields['name'] ?? '';
        }
        $FormUnits = TableRegistry::getTableLocator()->get('FormUnits');
        $query = $FormUnits->find()
            ->where(
                [
                    'form_id' => $this->_fields['form_id'],
                    'lft <=' => $this->_fields['lft'],
                    'rght >=' => $this->_fields['rght'],
                ]
            )
            ->order(['lft']);
        $names = [];
        $twigData = $this->get('twig_data') ?: [];
        foreach ($query as $formUnit) {
            // on refait une requête pour corriger un bug (contain)
            $formUnit = $FormUnits->find()
                ->where(['FormUnits.id' => $formUnit->id])
                ->contain(
                    [
                        'FormUnitHeaders' => function (Query $q) {
                            return $q->where(['name' => 'Name'])
                                ->contain(['FormVariables'])
                                ->limit(1);
                        },
                        'FormUnitContents' => function (Query $q) {
                            return $q->where(['name' => 'Title'])
                                ->contain(['FormVariables'])
                                ->limit(1);
                        },
                    ]
                )
                ->firstOrFail();
            $pathables = [
                FormUnitsTable::TYPE_TREE_ROOT,
                FormUnitsTable::TYPE_SIMPLE,
                FormUnitsTable::TYPE_REPEATABLE,
            ];
            if (!in_array($formUnit->get('type'), $pathables)) {
                continue;
            }
            switch ($formUnit->get('storage_calculation')) {
                case FormUnitsTable::STORAGE_CALCULATION_NONE:
                    continue 2;
                case FormUnitsTable::STORAGE_CALCULATION_ARCHIVE_UNIT_NAME:
                    $name = Hash::get($formUnit, 'form_unit_headers.0.form_variable.twig');
                    $name = $name ?: Hash::get($formUnit, 'form_unit_contents.0.form_variable.twig');
                    if ($name && $twigData) {
                        $name = htmlspecialchars_decode(Twig::render($name, $twigData), ENT_QUOTES);
                    }
                    break;
                case FormUnitsTable::STORAGE_CALCULATION_TREE_ELEMENT_NAME_INDEX:
                    $name = sprintf(
                        '%s%d',
                        $formUnit->get('name'),
                        Twig::render('{{section.index}}', $twigData)
                    );
                    break;
                case null:
                default:
                case FormUnitsTable::STORAGE_CALCULATION_TREE_ELEMENT_NAME:
                    $name = $formUnit->get('name');
            }
            $name = str_replace('/', '', Translit::safeUri($name ?: $formUnit->get('name')));
            $maxLength = $cutLength = 254;
            while (strlen($name) > $maxLength) {
                $name = trim($textHelper->truncate($name, $cutLength--, ['ellipsis' => '']));
            }
            $names[] = trim($name);
        }
        return implode('/', $names);
    }

    /**
     * Donne l'idée de la section répétable à laquelle cette ua est reliée
     *      via elle même ou via un parent
     * @return int|null
     */
    protected function _getRepeatableFieldsetIdParent(): ?int
    {
        $FormUnits = TableRegistry::getTableLocator()->get('FormUnits');
        $ancestor = $FormUnits->find()
            ->where(
                [
                    'lft <=' => $this->_fields['lft'],
                    'rght >=' => $this->_fields['rght'],
                    'type' => FormUnitsTable::TYPE_REPEATABLE,
                ]
            )
            ->first();
        return $ancestor?->get('repeatable_fieldset_id');
    }

    /**
     * Liste les id des fieldset répétables liés
     * (normalement un seul fieldset max doit être retourné)
     * @return bool
     */
    protected function _getAsLinkedRepeatableFieldsets(): bool
    {
        $FormUnits = TableRegistry::getTableLocator()->get('FormUnits');
        $fieldsets = $FormUnits->find()
            ->leftJoinWith('FormInputs.FormFieldsets')
            ->leftJoinWith('FormUnitContents.FormVariables.FormInputs.FormFieldsets')
            ->leftJoinWith('FormUnitHeaders.FormVariables.FormInputs.FormFieldsets')
            ->leftJoinWith('FormUnitManagements.FormVariables.FormInputs.FormFieldsets')
            ->leftJoinWith('FormUnitKeywords.FormUnitKeywordDetails.FormVariables.FormInputs.FormFieldsets')
            ->leftJoinWith('ChildFormUnits.FormInputs.FormFieldsets')
            ->leftJoinWith('ChildFormUnits.FormUnitContents.FormVariables.FormInputs.FormFieldsets')
            ->leftJoinWith('ChildFormUnits.FormUnitHeaders.FormVariables.FormInputs.FormFieldsets')
            ->leftJoinWith('ChildFormUnits.FormUnitManagements.FormVariables.FormInputs.FormFieldsets')
            ->leftJoinWith(
                'ChildFormUnits.FormUnitKeywords.FormUnitKeywordDetails.FormVariables.FormInputs.FormFieldsets'
            )
            ->where(
                [
                    'FormUnits.lft >=' => $this->_fields['lft'],
                    'FormUnits.rght <=' => $this->_fields['rght'],
                    'FormFieldsets.repeatable IS' => true,
                ]
            )
            ->all();

        return $fieldsets->count() > 0;
    }
}
