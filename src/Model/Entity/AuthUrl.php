<?php

/**
 * Versae\Model\Entity\AuthUrl
 */

namespace Versae\Model\Entity;

use AsalaeCore\Model\Entity\AuthUrl as CoreAuthUrl;

/**
 * Entité de la table codes
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AuthUrl extends CoreAuthUrl
{
}
