<?php

/**
 * Versae\Model\Entity\Role
 */

namespace Versae\Model\Entity;

use AsalaeCore\Model\Entity\Role as CoreRole;

/**
 * Entité de la table roles
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Role extends CoreRole
{
}
