<?php

/**
 * Versae\Model\Entity\Deposit
 */

namespace Versae\Model\Entity;

use AsalaeCore\ORM\Entity;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Exception;
use Versae\Model\Table\DepositsTable;

/**
 * Entité de la table deposits
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Deposit extends Entity
{
    /**
     * Champs virtuels
     * @var array
     */
    protected $_virtual = [
        'cancelable',
        'deletable',
        'editable',
        'reeditable',
        'sendable',
        'statetrad',
    ];

    /**
     * Deposit constructor.
     * @param array $properties
     * @param array $options
     * @throws Exception
     */
    public function __construct(array $properties = [], array $options = [])
    {
        $properties += [
            'path_token' => bin2hex(random_bytes(4)),
        ];
        parent::__construct($properties, $options);
    }

    /**
     * Traductions des états
     * @return string
     */
    protected function _getStatetrad(): string
    {
        $state = $this->_fields['state'] ?? '';
        switch ($state) {
            case DepositsTable::S_EDITING:
                $state = __dx('form', 'state', "en cours d'édition");
                break;
            case DepositsTable::S_READY_TO_PREPARE:
                $state = __dx('form', 'state', "en cours de préparation");
                break;
            case DepositsTable::S_READY_TO_SEND:
                $state = __dx('form', 'state', "en cours d'envoi");
                break;
            case DepositsTable::S_SENT:
                $state = __dx('form', 'state', "envoyé");
                break;
            case DepositsTable::S_RECEIVED:
                $state = __dx('form', 'state', "reçu");
                break;
            case DepositsTable::S_REJECTED:
                $state = __dx('form', 'state', "rejeté");
                break;
            case DepositsTable::S_ACCEPTED:
                $state = __dx('form', 'state', "accepté");
                break;
            case DepositsTable::S_PREPARING_ERROR:
                $state = __dx('form', 'state', "erreur de préparation");
                break;
            case DepositsTable::S_SENDING_ERROR:
                $state = __dx('form', 'state', "erreur d'envoi");
                break;
            case DepositsTable::S_ACKNOWLEDGING_ERROR:
                $state = __dx('form', 'state', "erreur de réception");
                break;
            case DepositsTable::S_ANSWERING_ERROR:
                $state = __dx('form', 'state', "erreur de réponse");
                break;
            case DepositsTable::S_CANCELED:
                $state = __dx('form', 'state', "annulé");
                break;
            case DepositsTable::S_GENERATING:
                $state = __dx('form', 'state', "génération du bordereau");
                break;
        }
        return $state;
    }

    /**
     * Vrai si le versement peut être envoyé :
     *      il a été enregistré en mode "brouillon"
     *      tous les inputs obligatoires ont une valeur
     * @return bool
     */
    protected function _getSendable(): bool
    {
        if (empty($this->_fields['id']) || $this->_fields['state'] !== DepositsTable::S_EDITING) {
            return false;
        }

        // enregistrement en mode "brouillon"
        if ($this->_fields['bypass_validation']) {
            return false;
        }

        // le bordereau n'existe pas (plantage lors de la génération)
        if (!is_file($this->get('path') . '/transfer.xml')) {
            return false;
        }

        return TableRegistry::getTableLocator()->get('FormInputs')->find()
            ->leftJoinWith('DepositValues')
            ->where(
                [
                    'FormInputs.form_id' => $this->_fields['form_id'],
                    'FormInputs.required' => true,
                    'DepositValues.hidden' => false,
                    'OR' => [
                        'DepositValues.input_value IS' => null,
                        'DepositValues.input_value' => '',
                    ]
                ]
            )
            ->count() === 0;
    }

    /**
     * Vrai si le versement peut être réédité :
     *      état rejected
     * @return bool
     */
    protected function _getReeditable(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        return in_array(
            $this->_fields['state'],
            [
                DepositsTable::S_CANCELED,
                DepositsTable::S_PREPARING_ERROR,
                DepositsTable::S_SENDING_ERROR,
                DepositsTable::S_REJECTED,
            ]
        );
    }

    /**
     * Vrai si le versement peut être supprimé :
     *      état editing ou rejected
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        return in_array(
            $this->_fields['state'],
            [
                DepositsTable::S_EDITING,
                DepositsTable::S_REJECTED,
                DepositsTable::S_SENDING_ERROR,
            ]
        );
    }


    /**
     * Vrai si le versement peut être annuler :
     *      état acknowledging_error ou answering_error
     * @return bool
     */
    protected function _getCancelable(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        return in_array(
            $this->_fields['state'],
            [
                DepositsTable::S_ACKNOWLEDGING_ERROR,
                DepositsTable::S_ANSWERING_ERROR,
            ]
        );
    }

    /**
     * Répertoire des fichiers
     * @return string
     * @throws Exception
     */
    protected function _getBasedir(): string
    {
        if (empty($this->_fields['id'])) {
            return '';
        }
        return sprintf('deposits/%d_%s', $this->_fields['id'], $this->_fields['path_token']);
    }

    /**
     * Donne le chemin complet
     * @return string
     * @throws Exception
     */
    protected function _getPath(): string
    {
        if (empty($this->_fields['id'])) {
            return '';
        }
        return Configure::read('App.paths.data') . DS . $this->_getBasedir();
    }


    /**
     * Editable
     * @return bool
     */
    protected function _getEditable(): bool
    {
        return in_array(
            $this->_fields['state'] ?? null,
            [DepositsTable::S_EDITING, DepositsTable::S_GENERATING]
        );
    }
}
