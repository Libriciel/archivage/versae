<?php

/**
 * Versae\Model\Entity\Cron
 */

namespace Versae\Model\Entity;

use AsalaeCore\Model\Entity\Cron as CoreCron;

/**
 * Entité de la table crons
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Cron extends CoreCron
{
}
