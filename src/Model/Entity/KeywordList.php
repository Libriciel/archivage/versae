<?php

/**
 * Versae\Model\Entity\KeywordList
 */

namespace Versae\Model\Entity;

use AsalaeCore\Model\Entity\KeywordList as CoreKeywordList;

/**
 * Entité de la table keyword_lists
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class KeywordList extends CoreKeywordList
{
}
