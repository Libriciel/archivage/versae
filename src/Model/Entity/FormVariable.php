<?php

/**
 * Versae\Model\Entity\FormVariable
 */

namespace Versae\Model\Entity;

use AsalaeCore\Model\Entity\JsonFieldEntityTrait;
use AsalaeCore\ORM\Entity;
use Versae\Model\Table\FormVariablesTable;

/**
 * Entité de la table form_variables
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FormVariable extends Entity
{
    use JsonFieldEntityTrait;

    /**
     * @var string[] Liste des champs de type JsonString
     */
    public array $jsonFields = ['app_meta'];

    /**
     * @var array[] champs virtuels à ratacher à l'objet JsonString
     */
    public array $jsonVirtualFields = [
        'app_meta' => [
            'conditions',
            'conditions_else_value',
            'conditions_elseif_value',
            'conditions_if_value',
            'date_format',
            'switch_default_value',
            'switch_field',
            'switch_options',
            'multiple_with',
            'join_field',
            'join_with',
        ],
    ];

    /**
     * @var int
     */
    public $index;
    /**
     * Champs virtuels
     * @var string[]
     */
    protected $_virtual = [
        'typetrad',
        'deletable',
    ];

    /**
     * Traductions des types
     * @return string
     */
    protected function _getTypetrad(): string
    {
        $state = $this->_fields['type'] ?? '';
        switch ($state) {
            case FormVariablesTable::TYPE_CONCAT:
                $state = __dx('form-variable', 'type', "Concaténation de champs");
                break;
            case FormVariablesTable::TYPE_CONCAT_MULTIPLE:
                $state = __dx('form-variable', 'type', "Concaténation de champs multiple");
                break;
            case FormVariablesTable::TYPE_CONDITIONS:
                $state = __dx('form-variable', 'type', "Valeur conditionnelle (SI / SINON SI / SINON)");
                break;
            case FormVariablesTable::TYPE_SWITCH:
                $state = __dx('form-variable', 'type', "Tableau de correspondances (SWITCH)");
                break;
            case FormVariablesTable::TYPE_TWIG:
                $state = __dx('form-variable', 'type', "Saisie de code TWIG");
                break;
        }
        return $state;
    }
}
