<?php

/**
 * Versae\Model\Entity\FormExtractor
 */

namespace Versae\Model\Entity;

use AsalaeCore\Model\Entity\JsonFieldEntityTrait;
use AsalaeCore\ORM\Entity;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Versae\Model\Table\FormExtractorsTable;

/**
 * Entité de la table form_extractors
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FormExtractor extends Entity
{
    use JsonFieldEntityTrait;

    /**
     * @var string[] Liste des champs de type JsonString
     */
    public array $jsonFields = ['app_meta'];

    /**
     * @var array[] champs virtuels à ratacher à l'objet JsonString
     */
    public array $jsonVirtualFields = [
        'app_meta' => [
            'url',
            'username',
            'password',
            'use_proxy',
            'ssl_verify_peer',
            'ssl_verify_peer_name',
            'ssl_verify_depth',
            'ssl_verify_host',
            'ssl_cafile',
            'namespace',
            'virtual_field_multiple',
        ],
    ];

    /**
     * Champs virtuels
     * @var array
     */
    protected $_virtual = [
        'typetrad',
        'data_formattrad',
        'deletable',
        'linked',
    ];

    /**
     * Traductions des types
     * @return string
     */
    protected function _getTypetrad(): string
    {
        $state = $this->_fields['type'] ?? '';
        switch ($state) {
            case FormExtractorsTable::TYPE_ARCHIVE_FILE:
                $state = __dx('form-extractor', 'type', "dossier ZIP du formulaire");
                break;
            case FormExtractorsTable::TYPE_FILE:
                $state = __dx('form-extractor', 'type', "fichier du formulaire");
                break;
            case FormExtractorsTable::TYPE_WEBSERVICE:
                $state = __dx('form-extractor', 'type', "webservice, ressource http");
                break;
        }
        return $state;
    }

    /**
     * Traductions des data_format
     * @return string
     */
    protected function _getDataFormattrad(): string
    {
        $format = $this->_fields['data_format'] ?? '';
        switch ($format) {
            case FormExtractorsTable::DATA_FORMAT_JSON:
                $format = __dx('form-extractor', 'data_format', "Format JSON");
                break;
            case FormExtractorsTable::DATA_FORMAT_XML:
                $format = __dx('form-extractor', 'data_format', "Format XML");
                break;
            case FormExtractorsTable::DATA_FORMAT_CSV:
                $format = __dx('form-extractor', 'data_format', "Format CSV");
                break;
        }
        return $format;
    }

    /**
     * Entité supprimable
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        return empty($this->_getLinked());
    }

    /**
     * Liste les utilisations de ce calculateur
     * @return array
     */
    protected function _getLinked()
    {
        if (!$this->id) {
            return [];
        }
        $linkedVariables = TableRegistry::getTableLocator()->get('FormVariables')
            ->find()
            ->innerJoinWith('FormExtractorsFormVariables')
            ->where(['form_extractor_id' => $this->id])
            ->contain(
                [
                    'FormCalculators',
                    'FormTransferHeaders',
                    'FormUnitContents' => ['FormUnits'],
                    'FormUnitHeaders' => ['FormUnits'],
                    'FormUnitManagements' => ['FormUnits'],
                    'FormUnitKeywordDetails' => [
                        'FormUnitKeywords' => 'FormUnits'
                    ],
                ]
            );
        $linked = [];
        $checkModels = [
            'form_calculator',
            'form_transfer_header',
            'form_unit_content',
            'form_unit_header',
            'form_unit_management',
            'form_unit_keyword_detail',
        ];
        /** @var EntityInterface $formVariable */
        foreach ($linkedVariables as $formVariable) {
            foreach ($checkModels as $modelName) {
                /** @var EntityInterface|null $linkedModel */
                $linkedModel = $formVariable->get($modelName);
                $archiveUnitPath = "$modelName.form_unit";
                if ($modelName === 'form_unit_keyword_detail') {
                    $archiveUnitPath = "$modelName.form_unit_keyword.form_unit";
                }
                /** @var EntityInterface|null $formUnit */
                $formUnit = Hash::get($formVariable, $archiveUnitPath);
                if ($linkedModel && $formUnit) {
                    $linked[] = sprintf(
                        'archive_units.%s.%s.%s',
                        $formUnit->get('name'),
                        str_replace('form_unit_', '', $modelName),
                        $linkedModel->get('name')
                    );
                } elseif ($linkedModel) {
                    $linked[] = $modelName . '.' . $linkedModel->get('name');
                }
            }
        }

        return $linked;
    }

    /**
     * Setter de virtual_field_multiple ; influe sur multiple
     * @param bool $value
     * @return bool
     */
    protected function _setVirtualFieldMultiple($value)
    {
        if ($value) {
            $this->_fields['multiple'] = (bool)$value;
        }
        return $value;
    }
}
