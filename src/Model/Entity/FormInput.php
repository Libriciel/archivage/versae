<?php

/**
 * Versae\Model\Entity\FormInput
 */

namespace Versae\Model\Entity;

use AsalaeCore\Model\Entity\JsonFieldEntityTrait;
use AsalaeCore\ORM\Entity;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Symfony\Component\HtmlSanitizer\HtmlSanitizer;
use Symfony\Component\HtmlSanitizer\HtmlSanitizerConfig;
use Versae\Model\Table\FormInputsTable;

/**
 * Entité de la table form_inputs
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FormInput extends Entity
{
    use JsonFieldEntityTrait;

    /**
     * @var string[] Liste des champs de type JsonString
     */
    public array $jsonFields = ['disable_expression', 'app_meta'];

    /**
     * @var array[] champs virtuels à ratacher à l'objet JsonString
     */
    public array $jsonVirtualFields = [
        'disable_expression' => [
            'cond_display_select',
            'cond_display_input',
            'cond_display_input_type',
            'cond_display_value_select',
            'cond_display_value_file',
            'cond_display_value',
            'cond_display_way',
            'cond_display_field',
        ],
        'app_meta' => [
            'checked',
            'color',
            'empty',
            'formats',
            'max',
            'min',
            'max_date',
            'min_date',
            'options',
            'p',
            'select2',
            'step',
            'value',
            'keyword_list_id',
            'use_name_as_code',
        ],
    ];

    /**
     * @var string[] types des champs virtuels app_meta
     */
    public array $appMetaFieldTypes = [
        'checked' => 'boolean',
        'empty' => 'boolean',
        'formats' => 'array',
    ];

    /**
     * @var int
     */
    public $index;
    /**
     * Champs virtuels
     * @var string[]
     */
    protected $_virtual = [
        'date_formattrad',
        'typetrad',
    ];

    /**
     * Cache pour _getKinked
     * @var array|null
     */
    protected $_cacheLinked;

    /**
     * Cast en integer le champ required
     * @return int 0 ou 1
     */
    protected function _getRequired(): int
    {
        return (int)(isset($this->_fields['required']) && $this->_fields['required']);
    }

    /**
     * Cast en integer le champ readonly
     * @return int 0 ou 1
     */
    protected function _getReadonly(): int
    {
        return (int)(isset($this->_fields['readonly']) && $this->_fields['readonly']);
    }

    /**
     * Cast en integer le champ select2
     * @return int 0 ou 1
     */
    protected function _getSelect2(): int
    {
        return (int)(isset($this->_fields['select2']) && $this->_fields['select2']);
    }

    /**
     * Cast en integer le champ multiple
     * @return int 0 ou 1
     */
    protected function _getMultiple(): int
    {
        return (int)(isset($this->_fields['multiple']) && $this->_fields['multiple']);
    }

    /**
     * Cast en integer le champ checked
     * @return int 0 ou 1
     */
    protected function _getChecked(): int
    {
        return (int)(isset($this->_fields['checked']) && $this->_fields['checked']);
    }

    /**
     * Traduction du format_date
     * @return string
     */
    protected function _getDateFormattrad(): string
    {
        switch ($this->_fields['date_format'] ?? '') {
            case FormInputsTable::DATE_FORMAT_FR:
                return __("Format FR court (jj/mm/aaaa)");
            case FormInputsTable::DATE_FORMAT_FR_LONG:
                return __("Format FR long (l jj mm aaaa)");
            case FormInputsTable::DATE_FORMAT_ISO:
                return __("Format ISO (aaaa-mm-jj)");
        }
        return '';
    }

    /**
     * Paragraphes sécurisés (retrait des scripts)
     * @return string|null
     */
    protected function _getP(): ?string
    {
        if (!empty($this->_fields['p'])) {
            $config = (new HtmlSanitizerConfig())
                ->allowSafeElements()
                ->allowStaticElements()
                ->forceAttribute('a', 'rel', 'noopener noreferrer')
                ->allowLinkSchemes(['https', 'http', 'mailto'])
                ->allowRelativeLinks()
                ->allowMediaSchemes(['https', 'http'])
                ->allowRelativeMedias();
            $sanitizer = new HtmlSanitizer($config);
            return $sanitizer->sanitize($this->_fields['p']);
        }
        return $this->_fields['p'] ?? null;
    }

    /**
     * Entité supprimable
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        return empty($this->_getLinked());
    }

    /**
     * Liste les utilisations de ce champ
     * @return array
     */
    protected function _getLinked(): array
    {
        if (!is_null($this->_cacheLinked)) {
            return $this->_cacheLinked;
        }

        if (!$this->id) {
            return [];
        }
        $linkedVariables = TableRegistry::getTableLocator()->get('FormVariables')
            ->find()
            ->innerJoinWith('FormInputsFormVariables')
            ->where(['form_input_id' => $this->id])
            ->contain(
                [
                    'FormExtractors',
                    'FormCalculators',
                    'FormTransferHeaders',
                    'FormUnitContents' => ['FormUnits'],
                    'FormUnitHeaders' => ['FormUnits'],
                    'FormUnitManagements' => ['FormUnits'],
                    'FormUnitKeywordDetails' => [
                        'FormUnitKeywords' => 'FormUnits',
                    ],
                ]
            );
        $linked = [];
        $checkModels = [
            'form_extractor',
            'form_calculator',
            'form_transfer_header',
            'form_unit_content',
            'form_unit_header',
            'form_unit_management',
            'form_unit_keyword_detail',
        ];
        /** @var EntityInterface $formVariable */
        foreach ($linkedVariables as $formVariable) {
            foreach ($checkModels as $modelName) {
                /** @var EntityInterface|null $linkedModel */
                $linkedModel = $formVariable->get($modelName);
                $archiveUnitPath = "$modelName.form_unit";
                if ($modelName === 'form_unit_keyword_detail') {
                    $archiveUnitPath = "$modelName.form_unit_keyword.form_unit";
                }
                /** @var EntityInterface|null $formUnit */
                $formUnit = Hash::get($formVariable, $archiveUnitPath);
                if ($linkedModel && $formUnit) {
                    $linked[] = sprintf(
                        'archive_units.%s.%s.%s',
                        $formUnit->get('name'),
                        str_replace('form_unit_', '', $modelName),
                        $linkedModel->get('name')
                    );
                } elseif ($linkedModel) {
                    $linked[] = $modelName . '.' . $linkedModel->get('name');
                }
            }
        }

        $formExtractors = TableRegistry::getTableLocator()->get('FormExtractors')
            ->find()
            ->where(['FormExtractors.form_input_id' => $this->id]);
        /** @var EntityInterface $formExtractor */
        foreach ($formExtractors as $formExtractor) {
            $linked[] = 'extractor.' . $formExtractor->get('name');
        }

        $formUnits = TableRegistry::getTableLocator()->get('FormUnits')
            ->find()
            ->where(['FormUnits.form_input_id' => $this->id]);
        /** @var EntityInterface $formUnit */
        foreach ($formUnits as $formUnit) {
            $linked[] = 'archive_units.' . $formUnit->get('name');
        }

        return $this->_cacheLinked = $linked;
    }

    /**
     * Traductions des types
     * @return string
     */
    protected function _getTypetrad(): string
    {
        if (empty($this->_fields['type'])) {
            return '';
        }
        switch ($this->_fields['type']) {
            case FormInputsTable::TYPE_TEXT:
                return __dx('form_input', 'type', "Texte");
            case FormInputsTable::TYPE_NUMBER:
                return __dx('form_input', 'type', "Nombre");
            case FormInputsTable::TYPE_EMAIL:
                return __dx('form_input', 'type', "Email");
            case FormInputsTable::TYPE_TEXTAREA:
                return __dx('form_input', 'type', "Bloc de texte");
            case FormInputsTable::TYPE_SELECT:
                return __dx('form_input', 'type', "Liste déroulante");
            case FormInputsTable::TYPE_DATE:
                return __dx('form_input', 'type', "Date");
            case FormInputsTable::TYPE_DATETIME:
                return __dx('form_input', 'type', "Date et heure");
            case FormInputsTable::TYPE_CHECKBOX:
                return __dx('form_input', 'type', "Case à cocher");
            case FormInputsTable::TYPE_MULTI_CHECKBOX:
                return __dx('form_input', 'type', "Cases à cocher multiples");
            case FormInputsTable::TYPE_RADIO:
                return __dx('form_input', 'type', "Bouton radio");
            case FormInputsTable::TYPE_FILE:
                return __dx('form_input', 'type', "Fichier");
            case FormInputsTable::TYPE_ARCHIVE_FILE:
                return __dx('form_input', 'type', "Dossier zippé");
            case FormInputsTable::TYPE_PARAGRAPH:
                return __dx('form_input', 'type', "Paragraphe de texte (lecture seule)");
        }
        return $this->_fields['type'];
    }
}
