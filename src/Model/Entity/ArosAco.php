<?php

/**
 * Versae\Model\Entity\ArosAco
 */

namespace Versae\Model\Entity;

use Acl\Model\Entity\Permission;

/**
 * Entité de la table aros_acos
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArosAco extends Permission
{
}
