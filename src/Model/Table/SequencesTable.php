<?php

/**
 * Versae\Model\Table\SequencesTable
 */

namespace Versae\Model\Table;

use AsalaeCore\Model\Table\SequencesTable as CoreSequencesTable;
use Cake\ORM\Behavior\TimestampBehavior;

/**
 * Table sequences
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class SequencesTable extends CoreSequencesTable
{
}
