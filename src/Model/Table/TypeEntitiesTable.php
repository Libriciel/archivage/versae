<?php

/**
 * Versae\Model\Table\TypeEntitiesTable
 */

namespace Versae\Model\Table;

use AsalaeCore\Model\Behavior\AclBehavior;
use AsalaeCore\Model\Table\TypeEntitiesTable as CoreTypeEntitiesTable;
use Cake\ORM\Behavior\TimestampBehavior;

/**
 * Table type_entities
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin AclBehavior
 * @mixin TimestampBehavior
 */
class TypeEntitiesTable extends CoreTypeEntitiesTable
{

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->belongsToMany('Roles');

        parent::initialize($config);
    }
}
