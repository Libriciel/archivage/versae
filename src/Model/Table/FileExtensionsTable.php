<?php

/**
 * Versae\Model\Table\FileExtensionsTable
 */

namespace Versae\Model\Table;

use AsalaeCore\Model\Table\FileExtensionsTable as CoreFileExtensionsTable;
use Cake\ORM\Behavior\TimestampBehavior;

/**
 * Table file_extensions
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class FileExtensionsTable extends CoreFileExtensionsTable
{
}
