<?php

/**
 * Versae\Model\Table\UsersTable
 */

namespace Versae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Behavior\AclBehavior;
use AsalaeCore\Model\Table\BeforeDeleteInterface;
use AsalaeCore\Model\Table\BeforeSaveInterface;
use AsalaeCore\Model\Table\UsersTable as CoreUsersTable;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\RulesChecker;
use Cake\ORM\TableRegistry;

/**
 * Table users
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin AclBehavior
 * @mixin TimestampBehavior
 */
class UsersTable extends CoreUsersTable implements
    BeforeDeleteInterface,
    BeforeSaveInterface
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('AsalaeCore.Acl', ['type' => 'requester']);
        $this->addBehavior('Timestamp');

        $this->belongsTo('Ldaps');
        $this->belongsTo('OrgEntities');
        $this->belongsTo('Roles');

        $this->hasMany('Notifications')
            ->setDependent(true);
        $this->hasMany('SavedFilters')
            ->setDependent(true);
        $this->hasMany('Sessions')
            ->setDependent(true);

        $this->hasOne('ChangeEntityRequests');
        $this->hasMany('Fileuploads');
        $this->hasMany('Deposits')
            ->setForeignKey('created_user_id');
        $this->hasMany('Transfers')
            ->setForeignKey('created_user_id');
    }

    /**
     * Allows listeners to modify the rules checker by adding more rules.
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->addDelete(
            function (EntityInterface $entity) {
                return $entity->get('deletable');
            }
        );
        return parent::buildRules($rules);
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeDelete(Event $event, Entity $entity, ArrayObject $options): void
    {
        $Deposits = TableRegistry::getTableLocator()->get('Deposits');
        $DepositValues = TableRegistry::getTableLocator()->get('DepositValues');
        $Sessions = TableRegistry::getTableLocator()->get('Sessions');

        $subQuery = $Deposits->find()
            ->select(['deposit_id' => 'id'])
            ->where(
                [
                    'created_user_id' => $entity->id,
                    'state' => DepositsTable::S_EDITING,
                ]
            );
        $DepositValues->deleteAll(['deposit_id IN' => $subQuery]);

        $Deposits->deleteAll(
            [
                'created_user_id' => $entity->id,
                'state' => DepositsTable::S_EDITING,
            ]
        );

        $Sessions->deleteAll(['user_id' => $entity->get('id')]);
    }

    /**
     * The Model.beforeSave event is fired before an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeSave(Event $event, Entity $entity, ArrayObject $options)
    {
        // en cas de changement d'entité
        if (
            !$entity->isNew()
            && $entity->isDirty('org_entity_id')
            && (int)$entity->getOriginal('org_entity_id') !== (int)$entity->get('org_entity_id')
            && $entity->get('org_entity_id')
            && $entity->getOriginal('org_entity_id')
        ) {
            // suppression des versements en préparation
            $Deposits = TableRegistry::getTableLocator()->get('Deposits');
            $query = $Deposits->find()
                ->where(
                    [
                        'created_user_id' => $entity->id,
                        'state' => DepositsTable::S_EDITING,
                    ]
                );
            foreach ($query as $deposit) {
                $Deposits->delete($deposit);
            }
        }
    }
}
