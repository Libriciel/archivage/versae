<?php

/**
 * Versae\Model\Table\SessionsTable
 */

namespace Versae\Model\Table;

use AsalaeCore\Model\Table\SessionsTable as CoreSessionsTable;
use Cake\ORM\Behavior\TimestampBehavior;

/**
 * Table sessions
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class SessionsTable extends CoreSessionsTable
{
}
