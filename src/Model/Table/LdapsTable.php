<?php

/**
 * Versae\Model\Table\LdapsTable
 */

namespace Versae\Model\Table;

use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\LdapsTable as CoreLdapsTable;
use Cake\ORM\Behavior\TimestampBehavior;

/**
 * Table ldaps
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @mixin OptionsBehavior
 */
class LdapsTable extends CoreLdapsTable
{
}
