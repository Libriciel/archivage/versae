<?php

/**
 * Versae\Model\Table\DepositsTable
 */

namespace Versae\Model\Table;

use ArrayObject;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\BeforeDeleteInterface;
use AsalaeCore\Model\Table\StateChangeInterface;
use AsalaeCore\Utility\Notify;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Database\Expression\QueryExpression;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use PDOException;
use StateMachine\Model\Behavior\StateMachineBehavior;

/**
 * Table deposits
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin OptionsBehavior
 * @mixin StateMachineBehavior
 * @mixin TimestampBehavior
 * @property DepositValuesTable|HasMany DepositValues
 */
class DepositsTable extends Table implements StateChangeInterface, BeforeDeleteInterface
{
    public const S_EDITING = 'editing';
    public const S_GENERATING = 'generating';
    public const S_READY_TO_PREPARE = 'ready_to_prepare';
    public const S_READY_TO_SEND = 'ready_to_send';
    public const S_SENT = 'sent';
    public const S_RECEIVED = 'received';
    public const S_REJECTED = 'rejected';
    public const S_ACCEPTED = 'accepted';
    public const S_PREPARING_ERROR = 'preparing_error';
    public const S_SENDING_ERROR = 'sending_error';
    public const S_ACKNOWLEDGING_ERROR = 'acknowledging_error';
    public const S_ANSWERING_ERROR = 'answering_error';
    public const S_CANCELED = 'canceled';

    public const T_GENERATE = 'generate';
    public const T_PREPARE = 'prepare';
    public const T_RETRY = 'retry';
    public const T_SEND = 'send';
    public const T_ACKNOWLEDGE = 'acknowledge';
    public const T_ACCEPT = 'accept';
    public const T_REFUSE = 'refuse';
    public const T_ERROR = 'error';
    public const T_REEDIT = 'reedit';
    public const T_CANCEL = 'cancel';
    public const T_DONE = 'done';

    /**
     * Etat initial
     *
     * @var string
     */
    public $initialState = self::S_EDITING;

    /**
     * Changement d'êtats selon action
     *
     * @var array
     */
    public $transitions = [
        self::T_GENERATE => [
            self::S_EDITING => self::S_GENERATING,
        ],
        self::T_SEND => [
            self::S_EDITING => self::S_READY_TO_PREPARE,
            self::S_GENERATING => self::S_READY_TO_PREPARE,
            self::S_READY_TO_SEND => self::S_SENT,
            self::S_SENDING_ERROR => self::S_SENT,
        ],
        self::T_PREPARE => [
            self::S_READY_TO_PREPARE => self::S_READY_TO_SEND,
            self::S_PREPARING_ERROR => self::S_READY_TO_SEND,
        ],
        self::T_ERROR => [
            self::S_PREPARING_ERROR => self::S_PREPARING_ERROR,
            self::S_READY_TO_SEND => self::S_SENDING_ERROR,
            self::S_SENDING_ERROR => self::S_SENDING_ERROR,
            self::S_READY_TO_PREPARE => self::S_PREPARING_ERROR,
            self::S_SENT => self::S_ACKNOWLEDGING_ERROR,
            self::S_ACKNOWLEDGING_ERROR => self::S_ACKNOWLEDGING_ERROR,
            self::S_RECEIVED => self::S_ANSWERING_ERROR,
            self::S_ANSWERING_ERROR => self::S_ANSWERING_ERROR,
            self::S_GENERATING => self::S_EDITING,
        ],
        self::T_ACKNOWLEDGE => [
            self::S_SENT => self::S_RECEIVED,
            self::S_ACKNOWLEDGING_ERROR => self::S_RECEIVED,
        ],
        self::T_ACCEPT => [
            self::S_RECEIVED => self::S_ACCEPTED,
            self::S_ANSWERING_ERROR => self::S_ACCEPTED,
        ],
        self::T_REFUSE => [
            self::S_RECEIVED => self::S_REJECTED,
            self::S_ANSWERING_ERROR => self::S_REJECTED,
        ],
        self::T_REEDIT => [
            self::S_PREPARING_ERROR => self::S_EDITING,
            self::S_SENDING_ERROR => self::S_EDITING,
            self::S_CANCELED => self::S_EDITING,
            self::S_REJECTED => self::S_EDITING,
        ],
        self::T_CANCEL => [
            self::S_ACKNOWLEDGING_ERROR => self::S_CANCELED,
            self::S_ANSWERING_ERROR => self::S_CANCELED,
        ],
        self::T_RETRY => [
            self::S_PREPARING_ERROR => self::S_READY_TO_PREPARE,
            self::S_SENDING_ERROR => self::S_READY_TO_SEND,
        ],
        self::T_DONE => [
            self::S_GENERATING => self::S_EDITING,
        ],
    ];

    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
        $this->addBehavior('StateMachine.StateMachine');
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'state' => [
                    self::S_EDITING,
                    self::S_READY_TO_PREPARE,
                    self::S_READY_TO_SEND,
                    self::S_SENT,
                    self::S_RECEIVED,
                    self::S_REJECTED,
                    self::S_ACCEPTED,
                    self::S_PREPARING_ERROR,
                    self::S_SENDING_ERROR,
                    self::S_ACKNOWLEDGING_ERROR,
                    self::S_ANSWERING_ERROR,
                    self::S_CANCELED,
                    self::S_GENERATING,
                ],
            ]
        );
        $this->belongsTo('ArchivalAgencies')
            ->setClassName('OrgEntities')
            ->setForeignKey('archival_agency_id')
            ->setJoinType('INNER');
        $this->belongsTo('TransferringAgencies')
            ->setClassName('OrgEntities')
            ->setForeignKey('transferring_agency_id')
            ->setJoinType('INNER');
        $this->belongsTo('OriginatingAgencies')
            ->setClassName('OrgEntities')
            ->setForeignKey('originating_agency_id');
        $this->belongsTo('Forms');

        $this->hasMany('DepositValues')->setDependent(true);
        $this->hasMany('BeanstalkJobs')
            ->setDependent(true)
            ->setForeignKey('object_foreign_key')
            ->setConditions(
                function (QueryExpression $qe, Query $q) {
                    $assocAlias = $q->getRepository()->getAlias(); // BeanstalkJobs
                    return $qe->and(["$assocAlias.object_model" => 'Deposits']);
                }
            );
        $this->hasOne('Transfers');

        $this->belongsTo('CreatedUsers')
            ->setClassName('Users')
            ->setForeignKey('created_user_id');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('state')
            ->maxLength('state', 255)
            ->requirePresence('state', 'create')
            ->notEmptyString('state');

        $validator
            ->dateTime('last_state_update')
            ->allowEmptyDateTime('last_state_update');

        $validator
            ->scalar('states_history')
            ->allowEmptyString('states_history');

        $validator
            ->scalar('path_token')
            ->maxLength('path_token', 8)
            ->notEmptyString('path_token');

        $validator
            ->scalar('error_message')
            ->allowEmptyString('error_message');

        return $validator;
    }

    /**
     * Allows listeners to modify the rules checker by adding more rules.
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->addDelete(
            function (EntityInterface $entity) {
                return $entity->get('deletable');
            }
        );
        return parent::buildRules($rules);
    }

    /**
     * Insertion des valeurs d'une section multiple
     * @param EntityInterface $deposit
     * @param array           $sectionValues
     * @return void
     */
    private function insertSectionInDepositValue(EntityInterface $deposit, array $sectionValues): void
    {
        foreach ($sectionValues as $index => $sectionValue) {
            foreach ($sectionValue as $inputName => $values) {
                $this->insertDepositValue(
                    $deposit,
                    $inputName,
                    $values,
                    $index
                );
            }
        }
    }

    /**
     * Ajoute une valeur dans DepositValues
     * @param EntityInterface $deposit
     * @param string          $inputName
     * @param mixed           $values
     * @param int|null        $fieldsetIndex
     * @return void
     */
    private function insertDepositValue(
        EntityInterface $deposit,
        string $inputName,
        $values,
        int $fieldsetIndex = null
    ): void {
        $form_input_id = $this->DepositValues->FormInputs->find()
            ->select(['id'])
            ->where(
                [
                    'form_id' => $deposit->get('form_id'),
                    'name' => $inputName,
                ]
            )
            ->disableHydration()
            ->firstOrFail()
            ['id'];
        $conditions = [
            'deposit_id' => $deposit->id,
            'form_input_id' => $form_input_id,
        ];
        if ($fieldsetIndex !== null) {
            $conditions['fieldset_index'] = $fieldsetIndex;
        } else {
            $conditions['fieldset_index IS'] = null;
        }
        if (!empty($values)) {
            $conditions['input_value NOT IN'] = (array)$values;
        }
        $this->DepositValues->deleteAll($conditions);
        unset($conditions['input_value NOT IN']);
        foreach (array_filter((array)$values, fn($v) => $v !== null) as $val) {
            $conditions['input_value'] = $val;
            $depositValue = $this->DepositValues->find()
                ->where($conditions)
                ->first();
            if (!$depositValue) {
                $this->DepositValues->query()
                    ->insert(['deposit_id', 'form_input_id', 'fieldset_index', 'input_value'])
                    ->values($conditions)
                    ->execute();
            }
        }
    }

    /**
     * Donne l'entité issue de data
     * @param EntityInterface $deposit
     * @param array           $data
     * @return EntityInterface
     */
    public function updateDeposit(EntityInterface $deposit, array $data): EntityInterface
    {
        $conn = $this->getConnection();
        $conn->begin();
        try {
            $this->patchEntity($deposit, $data);
            $this->saveOrFail($deposit);
            foreach ($data['inputs'] ?? [] as $inputName => $values) {
                if (preg_match('/^section-(\d+)$/', $inputName)) {
                    $this->insertSectionInDepositValue(
                        $deposit,
                        $values
                    );
                } else {
                    $this->insertDepositValue(
                        $deposit,
                        $inputName,
                        $values
                    );
                }
            }

            $hiddenFields = json_decode($data['hidden_fields'] ?? "[]", true) ?: [];
            foreach ($hiddenFields as $inputName) {
                $fieldsetIndex = null;
                if (preg_match('/(.*)\[(?:\d+)?]$/', $inputName, $m)) {
                    $inputName = $m[1];
                }
                $inputName = rtrim($inputName, '[]');
                // exemple: section-1[0][myfieldname]
                if (preg_match('/^section-\d+\[(\d+)]\[(.*)/', $inputName, $m)) {
                    $fieldsetIndex = $m[1];
                    $inputName = $m[2];
                }
                $formInput = $this->DepositValues->FormInputs->find()
                    ->select(['id'])
                    ->where(
                        [
                            'form_id' => $deposit->get('form_id'),
                            'name' => $inputName,
                        ]
                    )
                    ->disableHydration()
                    ->first();
                if (!$formInput) {
                    continue;
                }
                $conditions = [
                    'deposit_id' => $deposit->id,
                    'form_input_id' => $formInput['id'],
                ];
                if ($fieldsetIndex) {
                    $conditions['fieldset_index'] = $fieldsetIndex;
                }
                $depositValue = $this->DepositValues->find()
                    ->where($conditions)
                    ->first();
                if (!$depositValue) {
                    $data = [
                        'deposit_id' => $deposit->id,
                        'form_input_id' => $formInput['id'],
                    ];
                    if ($fieldsetIndex) {
                        $data['fieldset_index'] = $fieldsetIndex;
                    }
                    $depositValue = $this->DepositValues->newEntity($data);
                }
                $depositValue->set('hidden', true);
                $this->DepositValues->saveOrFail($depositValue);
            }
        } catch (PDOException $e) {
            $conn->rollback();
            throw $e;
        }
        $conn->commit();
        return $deposit;
    }

    /**
     * Réédition avec un nouveau formulaire avec mise à jour des deposit_values
     * @param EntityInterface $entity
     * @param EntityInterface $newerForm
     * @return bool
     * @throws Exception
     */
    public function reeditWithNewForm(EntityInterface $entity, EntityInterface $newerForm): bool
    {
        if (!$entity->get('reeditable') || !$this->transition($entity, self::T_REEDIT)) {
            return false;
        }

        $conn = $this->getConnection();
        $conn->begin();

        try {
            // delete transfer
            $Transfers = TableRegistry::getTableLocator()->get('Transfers');
            $transfer = $Transfers->find()
                ->where(['deposit_id' => $entity->get('id')])
                ->firstOrFail();
            $Transfers->deleteOrFail($transfer);

            // change form
            $this->changeForm($entity, $newerForm);
        } catch (PDOException $e) {
            $conn->rollback();
            $entity->setError('change_form', $e->getMessage());

            return false;
        }
        $conn->commit();

        return true;
    }

    /**
     * Réédition simple: changement d'état et sauvegarde
     * @param EntityInterface $entity
     * @return bool
     */
    public function reedit(EntityInterface $entity): bool
    {
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');
        $transfer = $Transfers->find()
            ->where(['deposit_id' => $entity->get('id')])
            ->first();
        if ($transfer) {
            $Transfers->delete($transfer);
        }
        return $entity->get('reeditable')
            && $this->transition($entity, self::T_REEDIT)
            && $this->save($entity);
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws Exception
     */
    public function beforeDelete(Event $event, Entity $entity, ArrayObject $options): void
    {
        // suppression des fichiers "upload"
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        foreach (glob($entity->get('path') . '/upload/*/*') as $uploadInputDir) {
            $fileupload_id = (int)basename($uploadInputDir);
            if (!$fileupload_id) {
                continue;
            }
            $conditions = ['id' => $fileupload_id];
            if ($user_id = $entity->get('created_user_id')) {
                $conditions['user_id'] = $user_id;
            }
            $fileupload = $Fileuploads->find()
                ->where($conditions)
                ->first();
            if ($fileupload) {
                $Fileuploads->delete($fileupload);
            }
        }
        if (is_dir($entity->get('path'))) {
            Filesystem::remove($entity->get('path'));
        }
    }

    /**
     * Appelé après la transition
     * @param EntityInterface $entity
     * @param string          $state
     */
    public function onStateChange(EntityInterface $entity, string $state): void
    {
        $transfer = $entity->get('transfer')
            ?? $this->getAssociation('Transfers')->find()
                ->where(['deposit_id' => $entity->get('id')])
                ->first();
        try {
            $Notify = Utility::get(Notify::class);
            /** @var Notify $Notify */
            $Notify->emit(
                'deposits',
                [
                    'deposit_id' => $entity->id,
                    'state' => $state,
                    'statetrad' => $entity->get('statetrad'),
                    'transfer_created' => $transfer?->get('created'),
                ]
            );
        } catch (Exception $e) {
            trigger_error($e->getMessage());
        }
    }

    /**
     * Change le formulaire d'un versement (pour une montée de version)
     * @param EntityInterface $entity
     * @param EntityInterface $newerForm
     * @return void
     * @throws Exception
     */
    public function changeForm(EntityInterface $entity, EntityInterface $newerForm): void
    {
        $oldFormId = $entity->get('form_id');
        $entity->set('form_id', $newerForm->get('id'));
        $this->saveOrFail($entity);

        $loc = TableRegistry::getTableLocator();
        /** @var DepositValuesTable $DepositValues */
        $DepositValues = $loc->get('DepositValues');
        /** @var FormInputsTable $FormInputs */
        $FormInputs = $loc->get('FormInputs');
        /** @var FileuploadsTable $FormInputs */
        $Fileuploads = $loc->get('Fileuploads');

        // modification des chemins de fichier uploadés
        $query = $DepositValues->find()
            ->innerJoinWith('FormInputs')
            ->where(
                [
                    'form_id' => $oldFormId,
                    'type IN' => [
                        FormInputsTable::TYPE_ARCHIVE_FILE,
                        FormInputsTable::TYPE_FILE,
                    ],
                    'input_value !=' => '',
                ]
            )
            ->contain(['FormInputs']);
        foreach ($query as $depositValue) {
            $newFormInput = $FormInputs->find()
                ->where(
                    [
                        'form_id' => $newerForm->id,
                        'name' => Hash::get($depositValue, 'form_input.name')
                    ]
                )
                ->first();
            $fileupload = $Fileuploads->find()
                ->where(['id' => $depositValue->get('input_value')])
                ->first();
            if (!$fileupload) {
                continue;
            }
            $oldInputId = $depositValue->get('form_input_id');
            $basedir = $entity->get('path') . DS;
            $oldEnddir =  DS . $oldInputId . DS . $fileupload->id;
            $oldUploadDir = $basedir . 'upload' . $oldEnddir;
            $oldUncompressedDir = $basedir . 'uncompressed' . $oldEnddir;
            if (!$newFormInput) {
                $Fileuploads->delete($fileupload);
                if (is_dir($oldUncompressedDir)) {
                    Filesystem::remove($oldUncompressedDir);
                }
                continue;
            }
            $newEnddir =  DS . $newFormInput->id . DS . $fileupload->id;
            $newUploadDir = $basedir . 'upload' . $newEnddir;
            $newUncompressedDir = $basedir . 'uncompressed' . $newEnddir;

            if (is_dir($oldUncompressedDir)) {
                Filesystem::rename($oldUncompressedDir, $newUncompressedDir);
                Filesystem::removeEmptySubFolders(dirname($oldUncompressedDir));
            }
            Filesystem::rename($oldUploadDir, $newUploadDir);
            $Fileuploads->updateAll(
                [
                    'path' => $newUploadDir
                        . DS . basename($fileupload->get('name'))
                ],
                ['id' => $fileupload->id]
            );
            Filesystem::removeEmptySubFolders(dirname($oldUploadDir));
        }
        Filesystem::removeEmptySubFolders($entity->get('path') . DS . 'upload');
        Filesystem::removeEmptySubFolders($entity->get('path') . DS . 'uncompressed');

        $newFormInputs = $FormInputs->find()
            ->where(['form_id' => $newerForm->get('id')])
            ->select(['name']);

        // Suppression des deposit_values qui n'existent plus
        $DepositValues->deleteAll(
            [
                'id NOT IN' => $DepositValues->find()
                    ->innerJoinWith('FormInputs')
                    ->select(['id'])
                    ->where(['FormInputs.name IN' => $newFormInputs]),
                'deposit_id' => $entity->get('id'),
            ]
        );

        // changement des form_input_id pour les deposit_values qui restent
        $DepositValues->query()->update()
            ->set(
                [
                    'form_input_id' => $FormInputs->find()
                        ->select(['FormInputs.id'])
                        ->innerJoin(
                            ['fi' => 'form_inputs'],
                            [
                                'fi.name' => new IdentifierExpression('FormInputs.name'),
                                'fi.id' => new IdentifierExpression('deposit_values.form_input_id'),
                            ]
                        )
                        ->where(
                            [
                                'FormInputs.form_id' => $newerForm->get('id'),
                                'fi.form_id' => $oldFormId,
                            ]
                        )
                ]
            )
            ->where(['DepositValues.deposit_id' => $entity->get('id')])
            ->execute();
    }

    /**
     * Transforme les valeurs stockés dans deposit_values sous forme de data
     * @param EntityInterface $deposit
     * @return array
     */
    public function getDepositFormData(EntityInterface $deposit): array
    {
        $inputs = [];
        /** @var EntityInterface $depositValue */
        foreach ($deposit->get('deposit_values') as $depositValue) {
            if ($depositValue->get('input_value') === null) {
                continue;
            }
            $name = Hash::get($depositValue, 'form_input.name');
            $sectionIndex = $depositValue->get('fieldset_index');
            if ($sectionIndex !== null) {
                $sectionName = sprintf(
                    'section-%d',
                    Hash::get($depositValue, 'form_input.form_fieldset.ord')
                );
                if (!isset($inputs[$sectionName])) {
                    $inputs[$sectionName] = [];
                }
                if (!isset($inputs[$sectionName][$sectionIndex])) {
                    $inputs[$sectionName][$sectionIndex] = [];
                }
                $ref = &$inputs[$sectionName][$sectionIndex];
            } else {
                $ref = &$inputs;
            }
            $ref[$name] = isset($ref[$name])
                ? array_merge((array)$ref[$name], [$depositValue->get('input_value')])
                : $depositValue->get('input_value');
        }
        return $inputs;
    }
}
