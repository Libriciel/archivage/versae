<?php

/**
 * Versae\Model\Table\OrgEntitiesTable
 */

namespace Versae\Model\Table;

use ArrayObject;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Model\Table\AfterDeleteInterface;
use AsalaeCore\Model\Table\OrgEntitiesTable as CoreOrgEntitiesTable;
use AsalaeCore\Model\Table\AfterSaveInterface;
use Cake\Database\Expression\QueryExpression;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Behavior\TreeBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Exception;
use Versae\Model\Entity\User;

/**
 * Table org_entities
 *
 * Autres alias:
 * ServiceArchives
 * OriginatingAgencies
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @mixin TreeBehavior
 */
class OrgEntitiesTable extends CoreOrgEntitiesTable implements
    AfterSaveInterface,
    AfterDeleteInterface
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
        $this->addBehavior('Tree');

        $this->belongsTo('ParentOrgEntities')
            ->setClassName('OrgEntities')
            ->setForeignKey('parent_id');
        $this->belongsTo('TypeEntities');
        $this->belongsTo('ArchivalAgencies')
            ->setClassName('OrgEntities')
            ->setForeignKey(false)
            ->setConditions(
                function (QueryExpression $qe, Query $q) {
                    $alias = $this->getAlias(); // OrgEntities
                    $assocAlias = $q->getRepository()->getAlias(); // ArchivalAgencies
                    $query = $this->query()
                        ->select(['aa.id'])
                        ->from(['aa' => 'org_entities'])
                        ->innerJoin(
                            ['aat' => 'type_entities'],
                            ['aat.id' => $q->identifier('aa.type_entity_id')]
                        )
                        ->where(
                            [
                                'aat.code IN' => array_merge(
                                    OrgEntitiesTable::CODE_OPERATING_DEPARTMENT,
                                    OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES
                                ),
                                'aa.lft <=' => $q->identifier("$alias.lft"),
                                'aa.rght >=' => $q->identifier("$alias.rght"),
                            ]
                        )
                        ->orderDesc(new QueryExpression("aat.code = 'SA'"))
                        ->orderAsc('aa.lft')
                        ->limit(1);
                    return $qe->and(["$assocAlias.id" => $query]);
                }
            );
        $this->hasMany('Configurations')->setDependent(true);
        $this->hasMany('Counters')->setDependent(true);
        $this->hasMany('KeywordLists')->setDependent(true);
        $this->hasMany('ChildOrgEntities')
            ->setForeignKey('parent_id')
            ->setClassName('OrgEntities');
        $this->hasMany('Roles');
        $this->hasMany('Sequences')->setDependent(true);
        $this->hasMany('Users');
        $this->hasMany('ArchivingSystems');
        $this->hasMany('Ldaps');
        $this->hasMany('Forms');
        $this->hasMany('FormsTransferringAgencies');
        $this->belongsToMany('FormsAvailables')
            ->setClassName('Forms')
            ->setThrough('FormsTransferringAgencies')
            ->setForeignKey('org_entity_id')
            ->setTargetForeignKey('form_id');

        $this->hasMany('ArchivalDeposits')
            ->setForeignKey('archival_agency_id')
            ->setClassName('Deposits');
        $this->hasMany('TransferringAgencyDeposits')
            ->setForeignKey('transferring_agency_id')
            ->setClassName('Deposits');
        $this->hasOne('RgpdInfos');

        $this->associations()->remove('Timestampers');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('identifier')
            ->maxLength('identifier', 255)
            ->requirePresence('identifier', 'create')
            ->notEmptyString('identifier')
            ->add(
                'identifier',
                [
                    'unique' => [ // cas entités d'un même service d'archives
                        'rule' => function ($value, $context) {
                            if (!Hash::get($context, 'data.parent_id')) { // SE
                                return true;
                            }

                            $entity = Hash::get($context, 'data.parent_org_entity')
                                ?? $this->find()
                                    ->where(
                                        [
                                            'OrgEntities.id' => Hash::get(
                                                $context,
                                                'data.id',
                                                Hash::get($context, 'data.parent_id')
                                            )
                                        ]
                                    )
                                    ->contain('ArchivalAgencies')
                                    ->first();

                            $archivalAgencyId = Hash::get($entity ?? [], 'archival_agency.id');
                            if ($archivalAgencyId) {
                                return count(
                                    $this->find()
                                        ->select(['existing' => 1])
                                        ->innerJoinWith('ArchivalAgencies')
                                        ->where(
                                            [
                                                'OrgEntities.id !=' => Hash::get($context, 'data.id', 0),
                                                'OrgEntities.identifier' => $value,
                                                'OR' => [
                                                    'ArchivalAgencies.id' => $archivalAgencyId,
                                                    'OrgEntities.parent_id IS' => null,
                                                ],
                                            ]
                                        )
                                        ->disableHydration()
                                        ->limit(1)
                                        ->toArray()
                                ) === 0;
                            }
                            return true;
                        },
                        'message' => __("Cet identifiant est déjà utilisé")
                    ]
                ]
            );

        $validator
            ->boolean('active')
            ->notEmptyString('active');

        $validator
            ->boolean('is_main_archival_agency')
            ->allowEmptyString('is_main_archival_agency');

        $validator->add(
            'type_entity_id',
            'custom',
            [
                'rule' => function ($value, $context) {
                    if (empty($context['data']['id'])) {
                        return true;
                    }
                    $Users = TableRegistry::getTableLocator()->get('Users');
                    $RolesTypeEntities = TableRegistry::getTableLocator()->get('RolesTypeEntities');

                    // on liste les roles des l'utilisateurs de l'entité
                    $rolesUserResults = $Users->find()
                        ->select(['role_id'])
                        ->where(
                            [
                                'org_entity_id' => $context['data']['id'],
                                'agent_type' => 'person',
                                'active IS' => true
                            ]
                        )
                        ->all()
                        ->toArray();
                    $rolesUser = (array)array_map(
                        function (User $v) {
                            return $v->get('role_id');
                        },
                        $rolesUserResults
                    );
                    if (empty($rolesUser)) {
                        return true;
                    }
                    $rolesUser = array_unique($rolesUser);

                    // on liste les roles disponnible pour cette entité
                    $rolesEntiteResults = $RolesTypeEntities->find()
                        ->where(['type_entity_id' => $value])
                        ->all()
                        ->toArray();
                    $rolesEntite = (array)array_map(
                        function (Entity $v) {
                            return $v->get('role_id');
                        },
                        $rolesEntiteResults
                    );

                    // si un role d'utilisateur n'est pas dans la liste
                    $errors = array_diff(
                        $rolesUser,
                        $rolesEntite
                    );
                    if (empty($errors)) {
                        return true;
                    }

                    // message d'erreur personnalisé
                    $user = $Users->find()
                        ->where(
                            [
                                'Users.org_entity_id' => $context['data']['id'],
                                'Users.agent_type' => 'person',
                                'Users.active IS' => true,
                                'Users.role_id' => current($errors),
                            ]
                        )
                        ->contain(['Roles'])
                        ->firstOrFail();
                    return __(
                        "L'utilisateur ({0}) possède un rôle non"
                        . " disponible ({1}) dans ce type d'entité",
                        $user->get('username'),
                        Hash::get($user, 'role.name')
                    );
                },
                'message' => __(
                    "Un utilisateur de l'entité possède un rôle non disponible dans ce type d'entité"
                )
            ]
        );

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->notEmptyString('max_disk_usage');

        $validator
            ->notEmptyString('disk_usage');

        $validator
            ->scalar('short_name')
            ->maxLength('short_name', 40)
            ->allowEmptyString('short_name');

        $validator->add(
            'parent_id',
            'custom',
            [
                'rule' => function ($value, $context) {
                    if ($context['newRecord'] || !$value) {
                        return true;
                    }
                    $parent = $this->find()
                        ->where(['id' => $value])
                        ->firstOrFail();
                    return !(Hash::get($context, 'data.lft') < $parent->get('lft')
                        && Hash::get($context, 'data.rght') > $parent->get('rght')
                    );
                },
                'message' => __("Ne peux pas avoir comme parent une entité fille")
            ]
        );

        return $validator;
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws Exception
     */
    public function afterSave(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ): void {
        parent::afterSave($event, $entity, $options);
        if (
            $entity->isNew()
            && $entity->get('code') === 'SA'
            && !($options['dumpSave'] ?? false)
        ) {
            $this->initCounters($entity->get('id'));
        }
        if (
            $entity->isDirty('is_main_archival_agency')
            && $entity->get('is_main_archival_agency')
        ) {
            $this->updateAll(
                ['is_main_archival_agency' => false],
                ['id !=' => $entity->get('id')]
            );
        }
        $Exec = Utility::get('Exec');
        $Exec->async(CAKE_SHELL, 'session', 'orgentities');
    }

    /**
     * The Model.afterDelete event is fired after an entity is deleted.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws Exception
     */
    public function afterDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ): void {
        $Exec = Utility::get('Exec');
        $Exec->async(CAKE_SHELL, 'session', 'orgentities');
        parent::afterDelete($event, $entity, $options);
    }
}
