<?php

/**
 * Versae\Model\Table\CountersTable
 */

namespace Versae\Model\Table;

use AsalaeCore\Model\Table\CountersTable as CoreCountersTable;
use Cake\ORM\Behavior\TimestampBehavior;

/**
 * Table counters
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @property SequencesTable $Sequences
 */
class CountersTable extends CoreCountersTable
{
}
