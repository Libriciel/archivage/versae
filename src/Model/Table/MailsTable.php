<?php

/**
 * Versae\Model\Table\MailsTable
 */

namespace Versae\Model\Table;

use ArrayObject;
use AsalaeCore\DataType\SerializedObject;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Model\Table\BeforeMarshalInterface;
use Cake\Event\Event;
use Cake\Mailer\Mailer;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Exception;
use Pheanstalk\PheanstalkInterface;
use Versae\Exception\GenericException;

/**
 * Table mails
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class MailsTable extends Table implements BeforeMarshalInterface
{

    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->requirePresence('serialized', 'create')
            ->notEmptyString('serialized');

        return $validator;
    }

    /**
     * The Model.beforeMarshall modify request data before it is converted into
     * entities.
     *
     * @param Event       $event
     * @param ArrayObject $data
     * @param ArrayObject $options
     */
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options): void
    {
        if (
            isset($data['serialized'])
            && is_object($data['serialized'])
            && (!$data['serialized'] instanceof SerializedObject)
        ) {
            $data['serialized'] = (string)new SerializedObject($data['serialized']);
        }
    }

    /**
     * Envoi l'email par worker
     * @param Mailer     $mailer
     * @param string|int $user_id
     * @return bool
     */
    public function asyncMail(Mailer $mailer, $user_id): bool
    {
        try {
            $test = clone $mailer;
            $msg = trim($test->render()->getMessage()->getBodyString());
            if (!$msg) {
                throw new GenericException(__("Mailer::render failed"));
            }
            $entity = $this->newEntity(['serialized' => $mailer]);
            $this->saveOrFail($entity);
            $Beanstalk = Utility::get('Beanstalk');
            $Beanstalk->setTube('mail');
            $Beanstalk->emit(
                ['mail_id' => $entity->id, 'user_id' => $user_id],
                PheanstalkInterface::DEFAULT_PRIORITY,
                $this->getConnection()->inTransaction() ? 10 : PheanstalkInterface::DEFAULT_DELAY
            );
            return true;
        } catch (Exception $e) {
            debug($e);
            return false;
        }
    }
}
