<?php

/**
 * Versae\Model\Table\VersionsTable
 */

namespace Versae\Model\Table;

use AsalaeCore\Model\Table\VersionsTable as CoreVersionsTable;
use Cake\ORM\Behavior\TimestampBehavior;

/**
 * Table versions
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class VersionsTable extends CoreVersionsTable
{
}
