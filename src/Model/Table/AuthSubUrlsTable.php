<?php

/**
 * Versae\Model\Table\AuthSubUrlsTable
 */

namespace Versae\Model\Table;

use AsalaeCore\Model\Table\AuthSubUrlsTable as CoreAuthSubUrlsTable;

/**
 * Table auth_sub_urls
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AuthSubUrlsTable extends CoreAuthSubUrlsTable
{
}
