<?php

/**
 * Versae\Model\Table\ArchivingSystemsTable
 */

namespace Versae\Model\Table;

use Cake\ORM\Behavior\TimestampBehavior;
use AsalaeCore\Model\Table\ArchivingSystemsTable as CoreArchivingSystemsTable;
use Cake\Validation\Validator;

/**
 * Table archiving_systems
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class ArchivingSystemsTable extends CoreArchivingSystemsTable
{
    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator = parent::validationDefault($validator);

        $validator
            ->integer('chunk_size')
            ->allowEmptyString('chunk_size');

        return $validator;
    }
}
