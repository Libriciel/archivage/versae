<?php

/**
 * Versae\Model\Table\PronomsTable
 */

namespace Versae\Model\Table;

use AsalaeCore\Model\Table\PronomsTable as CorePronomsTable;
use Cake\ORM\Behavior\TimestampBehavior;

/**
 * Table pronoms
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class PronomsTable extends CorePronomsTable
{
}
