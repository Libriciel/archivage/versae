<?php

/**
 * Versae\Model\Table\NotificationsTable
 */

namespace Versae\Model\Table;

use AsalaeCore\Model\Table\NotificationsTable as CoreNotificationsTable;
use Cake\ORM\Behavior\TimestampBehavior;

/**
 * Table notifications
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class NotificationsTable extends CoreNotificationsTable
{
}
