<?php

/**
 * Versae\Model\Table\FormFieldsetsTable
 */

namespace Versae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\AfterSaveInterface;
use AsalaeCore\ORM\Marshaller;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Marshaller as CakeMarshaller;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Table form_fieldsets
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin OptionsBehavior
 */
class FormFieldsetsTable extends Table implements AfterSaveInterface
{
    public const COND_DISPLAY_CHECKBOX_CHECKED = 'checkbox_checked';
    public const COND_DISPLAY_CHECKBOX_NOT_CHECKED = 'checkbox_not_checked';
    public const COND_DISPLAY_INPUT_VALUE = 'input_value';
    public const COND_DISPLAY_INPUT_NOT_VALUE = 'input_not_value';
    public const COND_DISPLAY_INPUT_EMPTY = 'input_empty';
    public const COND_DISPLAY_INPUT_NOT_EMPTY = 'input_not_empty';

    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'cond_display_select' => [
                    self::COND_DISPLAY_CHECKBOX_CHECKED,
                    self::COND_DISPLAY_CHECKBOX_NOT_CHECKED,
                    self::COND_DISPLAY_INPUT_VALUE,
                    self::COND_DISPLAY_INPUT_NOT_VALUE,
                    self::COND_DISPLAY_INPUT_EMPTY,
                    self::COND_DISPLAY_INPUT_NOT_EMPTY,
                ],
            ]
        );
        $this->belongsTo('Forms');
        $this->hasMany('FormInputs');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('legend')
            ->maxLength('legend', 255)
            ->allowEmptyString('legend');

        $validator
            ->integer('ord')
            ->requirePresence('ord', 'create')
            ->notEmptyString('ord');

        $validator
            ->allowEmptyString('disable_expression');

        return $validator;
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options): void
    {
        if ($entity->isNew() || $entity->isDirty()) {
            TableRegistry::getTableLocator()->get('Forms')
                ->updateAll(
                    ['tested' => false],
                    ['id' => $entity->get('form_id')]
                );
        }
    }

    /**
     * Get the object used to marshal/convert array data into objects.
     *
     * Override this method if you want a table object to use custom
     * marshalling logic.
     *
     * @return Marshaller
     * @see Marshaller
     */
    public function marshaller(): CakeMarshaller
    {
        return new Marshaller($this);
    }
}
