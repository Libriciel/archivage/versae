<?php

/**
 * Versae\Model\Table\AcosTable
 */

namespace Versae\Model\Table;

use Acl\Model\Table\AcosTable as AclAcosTable;
use Cake\ORM\Behavior\TreeBehavior;

/**
 * Table acos
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TreeBehavior
 */
class AcosTable extends AclAcosTable
{
    /**
     * {@inheritDoc}
     *
     * @param array $config Config
     * @return void
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Tree', ['type' => 'nested']);

        $this->belongsToMany('Aros')
            ->setThrough('ArosAcos');
        $this->hasMany('ArosAcos');
    }
}
