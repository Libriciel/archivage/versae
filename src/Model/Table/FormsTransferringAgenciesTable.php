<?php

/**
 * Versae\Model\Table\FormsTransferringAgenciesTable
 */

namespace Versae\Model\Table;

use Cake\ORM\Table;

/**
 * Table forms_transferring_agencies
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FormsTransferringAgenciesTable extends Table
{
    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('Forms');
        $this->belongsTo('OrgEntities');
    }
}
