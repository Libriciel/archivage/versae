<?php

/**
 * Versae\Model\Table\RgpdInfosTable
 */

namespace Versae\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Table rgpd_infos
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2023, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class RgpdInfosTable extends Table
{

    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('OrgEntities');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('org_entity_id')
            ->allowEmptyString('org_entity_id');

        $validator
            ->scalar('org_name')
            ->allowEmptyString('org_name');

        $validator
            ->scalar('org_address')
            ->allowEmptyString('org_address');

        $validator
            ->scalar('org_siret')
            ->maxLength('org_siret', 255)
            ->allowEmptyString('org_siret');

        $validator
            ->scalar('org_ape')
            ->maxLength('org_ape', 255)
            ->allowEmptyString('org_ape');

        $validator
            ->scalar('org_phone')
            ->maxLength('org_phone', 255)
            ->allowEmptyString('org_phone');

        $validator
            ->scalar('org_email')
            ->maxLength('org_email', 255)
            ->allowEmptyString('org_email')
            ->email('org_email');

        $validator
            ->scalar('ceo_name')
            ->maxLength('ceo_name', 255)
            ->allowEmptyString('ceo_name');

        $validator
            ->scalar('ceo_function')
            ->maxLength('ceo_function', 255)
            ->allowEmptyString('ceo_function');

        $validator
            ->scalar('dpo_name')
            ->maxLength('dpo_name', 255)
            ->allowEmptyString('dpo_name');

        $validator
            ->scalar('dpo_email')
            ->maxLength('dpo_email', 255)
            ->allowEmptyString('dpo_email')
            ->email('dpo_mail');

        $validator
            ->scalar('comment')
            ->allowEmptyString('comment');

        return $validator;
    }
}
