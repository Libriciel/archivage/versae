<?php

/**
 * Versae\Model\Table\FormCalculatorsTable
 */

namespace Versae\Model\Table;

use ArrayObject;
use AsalaeCore\DataType\JsonString;
use AsalaeCore\Model\Table\AfterDeleteInterface;
use AsalaeCore\Model\Table\AfterSaveInterface;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Twig\Error\SyntaxError;
use Versae\Utility\Twig;

/**
 * Table form_calculators
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property FormCalculatorsFormVariablesTable FormCalculatorsFormVariables
 */
class FormCalculatorsTable extends Table implements AfterSaveInterface, AfterDeleteInterface
{

    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('Forms');
        $this->belongsTo('FormVariables');
        $this->belongsToMany('ManyFormVariables')
            ->setClassName('FormVariables')
            ->setTargetForeignKey('form_variable_id');
        $this->hasMany('FormCalculatorsFormVariables');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name')
            ->add(
                'name',
                'validFormat',
                [
                    'rule' => ['custom', '/^[a-zA-Z][a-zA-Z0-9_]*$/'],
                    'message' => __("Autorisés: lettres, chiffres et underscores `_`, seulement une lettre en premier"),
                ]
            )
            ->add(
                'name',
                [
                    'unique' => [
                        'rule' => ['validateUnique', ['scope' => 'form_id']],
                        'provider' => 'table',
                        'message' => __("Cet identifiant est déjà utilisé"),
                    ],
                ]
            )
            ->add(
                'name',
                [
                    'not_reserved' => [
                        'rule' => fn($value) => !in_array($value, Twig::TOKEN_PROG_LIST),
                        'message' => __("Ce nom est réservé"),
                    ]
                ]
            );

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        return $validator;
    }

    /**
     * Remplace le nom de la variable par le nouveau nom
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws SyntaxError
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options): void
    {
        $original = $entity->getOriginal('name');
        $name = $entity->get('name');
        if ($entity->isNew() === false && $original !== $name) {
            $FormVariables = $this->getAssociation('FormVariables');
            $query = $FormVariables->find()
                ->innerJoinWith('FormCalculatorsFormVariables')
                ->where(['form_calculator_id' => $entity->id]);
            /** @var EntityInterface $formVariable */
            foreach ($query as $formVariable) {
                $formVariable->set(
                    'twig',
                    Twig::replaceVar($formVariable->get('twig'), 'var.' . $original, 'var.' . $name)
                );
                $this->checkDirtyAppMeta($entity, $formVariable);
                $FormVariables->save($formVariable);
            }
        }
        if ($entity->isNew() || $entity->isDirty()) {
            TableRegistry::getTableLocator()->get('Forms')
                ->updateAll(
                    ['tested' => false],
                    ['id' => $entity->get('form_id')]
                );
        }
    }

    /**
     * Allows listeners to modify the rules checker by adding more rules.
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->addDelete(
            function (EntityInterface $entity) {
                return $entity->get('deletable');
            }
        );
        return parent::buildRules($rules);
    }

    /**
     * The Model.afterDelete event is fired after an entity is deleted.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterDelete(Event $event, Entity $entity, ArrayObject $options): void
    {
        $this->getAssociation('FormVariables')
            ->deleteAll(['id' => $entity->get('form_variable_id')]);
    }

    /**
     * Si un des elements de app_meta à changé, on setDirty
     * @param EntityInterface $entity
     * @param EntityInterface $formVariable
     * @return void
     */
    private function checkDirtyAppMeta(EntityInterface $entity, EntityInterface $formVariable): void
    {
        $original = $entity->getOriginal('name');
        $name = $entity->get('name');
        /** @var JsonString $jsonObject */
        $jsonObject = $formVariable->get('app_meta');
        foreach (Hash::flatten((array)$jsonObject) as $key => $value) {
            if ($value === 'var.' . $original) {
                $ref = &$jsonObject;
                foreach (explode('.', $key) as $skey) {
                    $ref = &$ref[$skey];
                }
                $ref = 'var.' . $name;
                $formVariable->setDirty('app_meta');
            }
        }
    }
}
