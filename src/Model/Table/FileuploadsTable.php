<?php

/**
 * Versae\Model\Table\FileuploadsTable
 */

namespace Versae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Table\AfterDeleteInterface;
use AsalaeCore\Model\Table\FileuploadsTable as CoreFileuploadsTable;
use AsalaeCore\Model\Table\MediainfosTable;
use AsalaeCore\Model\Table\SiegfriedsTable;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Exception;

/**
 * Table fileuploads
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property MediainfosTable Mediainfos
 * @property SiegfriedsTable Siegfrieds
 */
class FileuploadsTable extends CoreFileuploadsTable implements AfterDeleteInterface
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->belongsTo('Users');
        parent::initialize($config);
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws Exception
     */
    public function afterDelete(Event $event, Entity $entity, ArrayObject $options): void
    {
        parent::afterDelete($event, $entity, $options);

        $DepositValues = TableRegistry::getTableLocator()->get('DepositValues');
        $value = $DepositValues
            ->find()
            ->innerJoinWith('FormInputs')
            ->where(
                [
                    'type IN' => [
                        FormInputsTable::TYPE_ARCHIVE_FILE,
                        FormInputsTable::TYPE_FILE,
                    ],
                    'input_value' => $entity->get('id'),
                ]
            )
            ->first();

        if ($value) {
            $DepositValues->delete($value);
        }
    }
}
