<?php

/**
 * Versae\Model\Table\RolesTypeEntitiesTable
 */

namespace Versae\Model\Table;

use Cake\ORM\Table;

/**
 * Table roles_type_entities
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class RolesTypeEntitiesTable extends Table
{

    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('Roles');
        $this->belongsTo('TypeEntities');
    }
}
