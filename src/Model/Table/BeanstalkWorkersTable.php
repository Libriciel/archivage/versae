<?php

/**
 * Versae\Model\Table\BeanstalkWorkersTable
 */

namespace Versae\Model\Table;

use Beanstalk\Model\Table\BeanstalkWorkersTable as BeanstalkBeanstalkWorkersTable;

/**
 * Table beanstalk_workers
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class BeanstalkWorkersTable extends BeanstalkBeanstalkWorkersTable
{
}
