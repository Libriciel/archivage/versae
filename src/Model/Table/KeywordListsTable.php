<?php

/**
 * Versae\Model\Table\KeywordListsTable
 */

namespace Versae\Model\Table;

use AsalaeCore\Model\Behavior\AliasBehavior;
use AsalaeCore\Model\Table\KeywordListsTable as CoreKeywordListsTable;
use Cake\ORM\Behavior\TimestampBehavior;

/**
 * Table keyword_lists
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @mixin AliasBehavior
 */
class KeywordListsTable extends CoreKeywordListsTable
{
}
