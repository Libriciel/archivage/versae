<?php

/**
 * Versae\Model\Table\ArosTable
 */

namespace Versae\Model\Table;

use Acl\Model\Table\ArosTable as AclArosTable;
use Cake\ORM\Behavior\TreeBehavior;

/**
 * Table aros
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TreeBehavior
 */
class ArosTable extends AclArosTable
{
    /**
     * {@inheritDoc}
     *
     * @param array $config Config
     * @return void
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Tree', ['type' => 'nested']);

        $this->belongsToMany('Acos')
            ->setThrough('ArosAcos');
        $this->hasMany('ArosAcos');
    }
}
