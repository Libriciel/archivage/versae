<?php

/**
 * Versae\Model\Table\CronExecutionsTable
 */

namespace Versae\Model\Table;

use AsalaeCore\Model\Table\CronExecutionsTable as CoreCronExecutionsTable;

/**
 * Table cron_executions
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class CronExecutionsTable extends CoreCronExecutionsTable
{
}
