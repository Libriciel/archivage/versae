<?php

/**
 * Versae\Model\Table\FormVariablesTable
 */

namespace Versae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\AfterSaveInterface;
use AsalaeCore\ORM\Marshaller;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Marshaller as CakeMarshaller;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Versae\Utility\Twig;

/**
 * Table form_variables
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin OptionsBehavior
 */
class FormVariablesTable extends Table implements AfterSaveInterface
{
    public const TYPE_CONCAT = 'concat';
    public const TYPE_CONCAT_MULTIPLE = 'concat_multiple';
    public const TYPE_CONDITIONS = 'conditions';
    public const TYPE_SWITCH = 'switch';
    public const TYPE_TWIG = 'twig';

    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'type' => [
                    self::TYPE_CONCAT,
                    self::TYPE_CONCAT_MULTIPLE,
                    self::TYPE_CONDITIONS,
                    self::TYPE_SWITCH,
                    self::TYPE_TWIG,
                ],
            ]
        );

        $this->belongsTo('Forms');
        $this->hasMany('FormCalculatorsFormVariables');
        $this->hasMany('FormExtractorsFormVariables');
        $this->hasMany('FormInputsFormVariables');
        $this->belongsToMany('FormInputs');
        $this->belongsToMany('FormExtractors');
        $this->belongsToMany('ManyFormCalculators')
            ->setClassName('FormCalculators')
            ->setTargetForeignKey('form_variable_id');
        $this->hasOne('FormCalculators')->setDependent(true);
        $this->hasOne('FormTransferHeaders')->setDependent(true);
        $this->hasOne('FormUnitHeaders')->setDependent(true);
        $this->hasOne('FormUnitManagements')->setDependent(true);
        $this->hasOne('FormUnitContents')->setDependent(true);
        $this->hasOne('FormUnitKeywordDetails')->setDependent(true);
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->requirePresence('type', 'create')
            ->notEmptyString('type');

        $validator
            ->scalar('twig')
            ->requirePresence('twig', 'create')
            ->notEmptyString('twig')
            ->add(
                'twig',
                [
                    'valid_twig' => [
                        'rule' => fn(string $value)  => Twig::validate($value),
                        'message' => __("N'est pas un twig valide"),
                    ],
                ]
            );

        $validator
            ->allowEmptyString('app_meta');

        return $validator;
    }

    /**
     * Remplace le nom de la variable par le nouveau nom
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options): void
    {
        if ($entity->isNew() || $entity->isDirty()) {
            TableRegistry::getTableLocator()->get('Forms')
                ->updateAll(
                    ['tested' => false],
                    ['id' => $entity->get('form_id')]
                );
        }
    }

    /**
     * Get the object used to marshal/convert array data into objects.
     *
     * Override this method if you want a table object to use custom
     * marshalling logic.
     *
     * @return Marshaller
     * @see Marshaller
     */
    public function marshaller(): CakeMarshaller
    {
        return new Marshaller($this);
    }

    /**
     * Surcharge de patchEntity pour retirer les conditions twig lorsque le type
     * ne correspond pas
     * @param EntityInterface $entity
     * @param array           $data
     * @param array           $options
     * @return EntityInterface
     */
    public function patchEntity(EntityInterface $entity, array $data, array $options = []): EntityInterface
    {
        if (($data['type'] ?? '') !== self::TYPE_CONDITIONS) {
            foreach (array_keys($data) as $key) {
                if (str_starts_with($key, 'conditions')) {
                    $data[$key] = null;
                }
            }
        }
        return parent::patchEntity($entity, $data, $options);
    }
}
