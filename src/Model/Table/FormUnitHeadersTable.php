<?php

/**
 * Versae\Model\Table\FormUnitHeadersTable
 */

namespace Versae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Table\AfterDeleteInterface;
use AsalaeCore\Model\Table\AfterSaveInterface;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Versae\Model\Entity\FormUnit;

/**
 * Table form_unit_headers
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property FormVariablesTable FormVariables
 */
class FormUnitHeadersTable extends Table implements AfterDeleteInterface, AfterSaveInterface
{
    public const NAMES_SEDA10_ROOT = [
        'ArchivalAgreement',
        'ArchivalProfile',
        'DescriptionLanguage',
        'Name',
        'OriginatingAgencyArchiveIdentifier',
        'ServiceLevel',
        'TransferringAgencyArchiveIdentifier',
    ];
    public const NAMES_SEDA10_OBJECT = [
        'Name',
        'OriginatingAgencyObjectIdentifier',
        'TransferringAgencyObjectIdentifier',
    ];
    public const NAMES_SEDA10_DOCUMENT = [
        'Control',
        'Copy',
        'Creation',
        'Description',
        'Issue',
        'OriginatingAgencyDocumentIdentifier',
        'Purpose',
        'Receipt',
        'Response',
        'Status',
        'Submission',
        'TransferringAgencyDocumentIdentifier',
        'Type',
        'Language',
    ];

    /**
     * @var array
     */
    public $names = [];

    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('FormUnits');
        $this->belongsTo('FormVariables');

        $this->names = [
            'ArchivalAgreement' => [
                'label' => __("Identifiant de l'accord de versement"),
            ],
            'ArchivalProfile' => [
                'label' => __("Identifiant du profil d'archive"),
            ],
            'Control' => [
                'label' => __("Exigences de contrôle")
            ],
            'Copy' => [
                'label' => __("Original ou copie"),
            ],
            'Creation' => [
                'label' => __("Date de création"),
            ],
            'DescriptionLanguage' => [
                'label' => __("Langue de la description (vide = fra)"),
            ],
            'Name' => [
                'label' => __("Nom de l'archive, objet d'archive (vide = Nom de l'élément de l'arborescence)"),
            ],
            'OriginatingAgencyArchiveIdentifier' => [
                'label' => __("Identifiant donné par le Service Producteur"),
            ],
            'ServiceLevel' => [
                'label' => __("Identifiant du niveau de service"),
            ],
            'TransferringAgencyArchiveIdentifier' => [
                'label' => __("Identifiant donné par le Service Versant"),
            ],
            'OriginatingAgencyObjectIdentifier' => [
                'label' => __("Identifiant donné par le Service Producteur"),
            ],
            'TransferringAgencyObjectIdentifier' => [
                'label' => __("Identifiant donné par le Service Versant"),
            ],
            'Description' => [
                'label' => __("Description"),
            ],
            'Issue' => [
                'label' => __("Date d'émission"),
            ],
            'OriginatingAgencyDocumentIdentifier' => [
                'label' => __("Identifiant donné par le Service Producteur"),
            ],
            'Purpose' => [
                'label' => __("Objet ou objectif de l'objet-données"),
            ],
            'Receipt' => [
                'label' => __("Date de réception"),
            ],
            'Response' => [
                'label' => __("Date de réponse"),
            ],
            'Status' => [
                'label' => __("Etat par rapport cycle de vie"),
            ],
            'Submission' => [
                'label' => __("Date de soumission par un émetteur"),
            ],
            'TransferringAgencyDocumentIdentifier' => [
                'label' => __("Identifiant donné par le Service Versant"),
            ],
            'Type' => [
                'label' => __("Type de l'objet-données (vide = CDO)"),
            ],
            'Language' => [
                'label' => __("Langue du contenu de l'objet-données"),
            ],
        ];
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Renvoi un tableau de résultats (inclus les variables manquantes)
     * @param FormUnit $formUnit
     * @return array
     */
    public function formHeadersData(FormUnit $formUnit): array
    {
        if (in_array(Hash::get($formUnit, 'form.seda_version'), ['seda2.1', 'seda2.2'])) {
            return [];
        }
        if ($formUnit->get('type') === FormUnitsTable::TYPE_DOCUMENT) {
            $fields = self::NAMES_SEDA10_DOCUMENT;
        } elseif ($formUnit->get('parent_id') === null) {
            $fields = self::NAMES_SEDA10_ROOT;
        } else {
            $fields = self::NAMES_SEDA10_OBJECT;
        }
        $allNames = $this->names;
        if (in_array($formUnit->get('type'), [FormUnitsTable::TYPE_TREE_PATH, FormUnitsTable::TYPE_TREE_SEARCH])) {
            $allNames['Name']['label'] = __("Nom de l'archive (vide = Nom du dossier ou du fichier)");
        }
        $names = [];
        foreach ($fields as $fieldname) {
            $names[$fieldname] = $allNames[$fieldname];
        }
        $data = [];
        $entities = [];
        $availables = array_keys($names);
        /** @var EntityInterface $formUnitHeader */
        foreach (Hash::get($formUnit, 'form_unit_headers') ?: [] as $formUnitHeader) {
            $name = $formUnitHeader->get('name');
            if (in_array($name, $availables)) {
                $formUnitHeader->set('label', $names[$name]['label']);
                $formUnitHeader->set('required', $names[$name]['required'] ?? false);
                $formUnitHeader->set('additionnal', $names[$name]['additionnal'] ?? false);
                $entities[$name] = $formUnitHeader;
            }
        }
        foreach ($names as $name => $params) {
            if (isset($entities[$name])) {
                $data[] = $entities[$name];
            } else {
                $data[] = [
                    'id' => null,
                    'name' => $name,
                    'label' => $params['label'],
                    'required' => $params['required'] ?? false,
                    'additionnal' => $params['additionnal'] ?? false,
                ];
            }
        }
        return $data;
    }

    /**
     * The Model.afterDelete event is fired after an entity is deleted.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterDelete(Event $event, Entity $entity, ArrayObject $options): void
    {
        $this->FormVariables->deleteAll(['id' => $entity->get('form_variable_id')]);
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options): void
    {
        if ($entity->isNew() || $entity->isDirty()) {
            $loc = TableRegistry::getTableLocator();
            $subquery = $loc->get('FormUnits')
                ->find()
                ->select(['form_id'])
                ->where(
                    ['FormUnits.id' => $entity->get('form_unit_id')]
                );
            $loc->get('Forms')
                ->updateAll(
                    ['tested' => false],
                    ['Forms.id' => $subquery]
                );
        }
    }
}
