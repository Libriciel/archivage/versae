<?php

/**
 * Versae\Model\Table\FormUnitsTable
 */

namespace Versae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\AfterSaveInterface;
use AsalaeCore\Model\Table\BeforeDeleteInterface;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior\TreeBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\ResultSet;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Exception;

/**
 * Table form_units
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin OptionsBehavior
 * @mixin TreeBehavior
 * @property FormUnitContentsTable FormUnitContents
 * @property FormUnitHeadersTable FormUnitHeaders
 * @property FormUnitManagementsTable FormUnitManagements
 * @property FormUnitKeywordsTable FormUnitKeywords
 */
class FormUnitsTable extends Table implements BeforeDeleteInterface, AfterSaveInterface
{
    public const TYPE_SIMPLE = 'simple';
    public const TYPE_REPEATABLE = 'repeatable';
    public const TYPE_DOCUMENT = 'document';
    public const TYPE_DOCUMENT_MULTIPLE = 'document_multiple';
    public const TYPE_TREE_ROOT = 'tree_root';
    public const TYPE_TREE_SEARCH = 'tree_search';
    public const TYPE_TREE_PATH = 'tree_path';
    public const TYPE_ICONS = [
        self::TYPE_SIMPLE => 'fa-archive',
        self::TYPE_REPEATABLE => 'fas fa-cubes',
        self::TYPE_DOCUMENT => 'fa-paperclip',
        self::TYPE_DOCUMENT_MULTIPLE => 'fa-files-o',
        self::TYPE_TREE_ROOT => 'fa-code-fork',
        self::TYPE_TREE_SEARCH => 'fa-search',
        self::TYPE_TREE_PATH => 'fa-code',
    ];
    public const STORAGE_CALCULATION_TREE_ELEMENT_NAME = 'tree_element_name';
    public const STORAGE_CALCULATION_TREE_ELEMENT_NAME_INDEX = 'tree_element_name_index';
    public const STORAGE_CALCULATION_ARCHIVE_UNIT_NAME = 'archive_unit_name';
    public const STORAGE_CALCULATION_NONE = 'none';

    /**
     * @var int nombre de chars avant coupure du nom (NomDunNoeud[5] Nom dans le noeud~)
     */
    public $maxCharsPerName = 30;

    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Tree');
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'type' => [
                    self::TYPE_SIMPLE,
                    self::TYPE_REPEATABLE,
                    self::TYPE_DOCUMENT,
                    self::TYPE_DOCUMENT_MULTIPLE,
                    self::TYPE_TREE_ROOT,
                    self::TYPE_TREE_PATH,
                    self::TYPE_TREE_SEARCH,
                ],
                'storage_calculation' => [
                    self::STORAGE_CALCULATION_TREE_ELEMENT_NAME_INDEX,
                    self::STORAGE_CALCULATION_TREE_ELEMENT_NAME,
                    self::STORAGE_CALCULATION_ARCHIVE_UNIT_NAME,
                    self::STORAGE_CALCULATION_NONE,
                ],
            ]
        );

        $this->belongsTo('Forms');
        $this->belongsTo('RepeatableFieldsets')
            ->setClassName('FormFieldsets');
        $this->belongsTo('ParentFormUnits')
            ->setClassName('FormUnits')
            ->setForeignKey('parent_id');
        $this->belongsTo('FormInputs');
        $this->belongsTo('FormVariables')
            ->setForeignKey('presence_condition_id');
        $this->hasMany('ChildFormUnits')
            ->setClassName('FormUnits')
            ->setForeignKey('parent_id');
        $this->hasMany('FormUnitHeaders');
        $this->hasMany('FormUnitManagements');
        $this->hasMany('FormUnitContents');
        $this->hasMany('FormUnitKeywords');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->requirePresence('type', 'create')
            ->notEmptyString('type');

        $validator
            ->integer('ord')
            ->allowEmptyString('ord');

        $validator
            ->scalar('search_expression')
            ->allowEmptyString('search_expression');

        $validator
            ->boolean('presence_required')
            ->allowEmptyString('presence_required');

        return $validator;
    }

    /**
     * Donne le jstree à partir d'un threaded d'archive_units
     * @param array|Query|ResultSet $formUnits
     * @param int|null              $parent_id
     * @param bool                  $addButton
     * @return array
     */
    public function threadedToJstreeData($formUnits, int $parent_id = null, bool $addButton = true): array
    {
        $jstreeData = [];
        /** @var EntityInterface $formUnit */
        foreach ($formUnits as $formUnit) {
            $text = sprintf(
                '%s <span class="child-value" title="%s">%s</span>',
                $formUnit->get('type'),
                $formUnit->get('name'),
                $this->getShortName($formUnit->get('name'))
            );
            $children = [];
            $icon = self::TYPE_ICONS[$formUnit->get('type')] ?? self::TYPE_ICONS[self::TYPE_SIMPLE];
            if ($formUnit->get('children')) {
                $children = $this->threadedToJstreeData(
                    $formUnit->get('children'),
                    $formUnit->id,
                    $addButton
                    && in_array(
                        $formUnit->get('type'),
                        [self::TYPE_SIMPLE, self::TYPE_TREE_ROOT, self::TYPE_REPEATABLE]
                    )
                );
            }
            if ($addButton && in_array($formUnit->get('type'), [self::TYPE_SIMPLE, self::TYPE_REPEATABLE])) {
                $children[] = [
                    'id' => 'jstree-btn-add-' . $parent_id . '-' . $formUnit->id,
                    'action' => 'add',
                    'form_unit_id' => $formUnit->id,
                    'parent_id' => $formUnit->id,
                    'type' => 'navigation',
                    'text' => __("Ajouter un élément"),
                    'searchtext' => '',
                    'icon' => 'fa fa-plus-square text-success',
                    'children' => null,
                    'state' => ['opened' => true],
                    'li_attr' => ['class' => 'add-element text-success'],
                ];
            }
            if ($addButton && $formUnit->get('type') === self::TYPE_TREE_ROOT) {
                $children[] = [
                    'id' => 'jstree-btn-add-' . $parent_id . '-' . $formUnit->id,
                    'action' => 'add',
                    'form_unit_id' => $formUnit->id,
                    'parent_id' => $formUnit->id,
                    'type' => 'navigation',
                    'text' => __("Ajouter un sélecteur"),
                    'searchtext' => '',
                    'icon' => 'fa fa-plus text-success',
                    'children' => null,
                    'state' => ['opened' => true],
                    'li_attr' => ['class' => 'add-element text-success'],
                ];
            }

            $jstreeData[] = [
                'id' => 'jstree-node-' . $formUnit->id,
                'action' => 'edit',
                'form_unit_id' => $formUnit->id,
                'parent_id' => $parent_id ?? 'root',
                'type' => $formUnit->get('type'),
                'text' => $text,
                'searchtext' => $formUnit->get('type') . ' ' . $formUnit->get('name'),
                'icon' => 'fa ' . $icon,
                'children' => $children,
                'state' => ['opened' => true],
                'repeatable_fieldset_id_parent' => $formUnit->get('repeatable_fieldset_id_parent'),
                'has_linked_repeatable_fieldsets' => $formUnit->get('as_linked_repeatable_fieldsets'),
                'has_repeatable_child' => Hash::get($formUnit, 'repeatable_fieldset_id')
                    || array_filter($children, fn($e) => Hash::get($e, 'has_repeatable_child')),
            ];
        }
        if ($addButton && $parent_id === null) {
            $jstreeData[] = [
                'id' => 'jstree-btn-add-root',
                'action' => 'add',
                'form_unit_id' => 'root',
                'parent_id' => 'root',
                'type' => 'navigation',
                'text' => __("Ajouter un élément"),
                'searchtext' => '',
                'icon' => 'fa fa-plus-square text-success',
                'children' => null,
                'state' => ['opened' => true],
                'li_attr' => ['class' => 'add-element text-success'],
            ];
        }
        return $jstreeData;
    }

    /**
     * Donne un nom raccourci à 30 chars
     * @param string $name
     * @param bool   $isFilename
     * @return string
     */
    private function getShortName(string $name, bool $isFilename = false): string
    {
        if (mb_strlen($name) <= $this->maxCharsPerName) {
            $trimmed = $name;
        } elseif ($isFilename) {
            if (mb_strpos($name, '/')) {
                $name = basename($name);
                if (mb_strlen($name) <= $this->maxCharsPerName) {
                    return $name;
                }
            }
            $ext = pathinfo($name, PATHINFO_EXTENSION);
            $endString = $ext ? '~.' . $ext : '~';
            $cutLength = $this->maxCharsPerName - mb_strlen($endString);
            $trimmed = mb_substr($name, 0, $cutLength) . $endString;
        } else {
            $trimmed = mb_substr($name, 0, $this->maxCharsPerName - 1) . '~';
        }

        return sprintf(
            '<span class="child-value%s" title="%s">%s</span>',
            $trimmed !== $name ? 'trimmed' : '',
            $name,
            $trimmed
        );
    }

    /**
     * Donne les options selon le parent
     * @param EntityInterface|null $parent
     * @return array
     * @throws Exception
     */
    public function getTypesOptions(EntityInterface $parent = null): array
    {
        $options = $this->options('type');
        if (!$parent) {
            $blackList = [
                self::TYPE_REPEATABLE,
                self::TYPE_DOCUMENT,
                self::TYPE_DOCUMENT_MULTIPLE,
                self::TYPE_TREE_PATH,
                self::TYPE_TREE_SEARCH,
            ];
        } elseif (
            $parent->get('type') === self::TYPE_SIMPLE
            || $parent->get('type') === self::TYPE_REPEATABLE
        ) {
            $blackList = [
                self::TYPE_TREE_PATH,
                self::TYPE_TREE_SEARCH,
            ];
            // si un repeatable dans les parents, pas possible de faire un répétable
            $hasRepeatableAncestors = $this->exists(
                [
                    'form_id' => $parent->get('form_id'),
                    'lft <=' => $parent->get('lft'),
                    'rght >=' => $parent->get('rght'),
                    'type' => self::TYPE_REPEATABLE,
                ]
            );
            if ($hasRepeatableAncestors) {
                $blackList[] = self::TYPE_REPEATABLE;
            }
        } elseif ($parent->get('type') === self::TYPE_TREE_ROOT) {
            $blackList = [
                self::TYPE_SIMPLE,
                self::TYPE_REPEATABLE,
                self::TYPE_TREE_ROOT,
                self::TYPE_DOCUMENT,
                self::TYPE_DOCUMENT_MULTIPLE,
            ];
        } else {
            return [];
        }

        return array_filter(
            $options,
            (fn($key) => !in_array($key, $blackList)),
            ARRAY_FILTER_USE_KEY
        );
    }

    /**
     * Remplace le nom de la variable par le nouveau nom
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options): void
    {
        if ($entity->isNew() || $entity->isDirty()) {
            TableRegistry::getTableLocator()->get('Forms')
                ->updateAll(
                    ['tested' => false],
                    ['id' => $entity->get('form_id')]
                );
        }
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeDelete(Event $event, Entity $entity, ArrayObject $options): void
    {
        $query = $this->FormUnitHeaders->find()
            ->where(['form_unit_id' => $entity->id]);
        /** @var EntityInterface $formUnitHeader */
        foreach ($query as $formUnitHeader) {
            $this->FormUnitHeaders->delete($formUnitHeader);
        }
        $query = $this->FormUnitManagements->find()
            ->where(['form_unit_id' => $entity->id]);
        /** @var EntityInterface $formUnitManagement */
        foreach ($query as $formUnitManagement) {
            $this->FormUnitManagements->delete($formUnitManagement);
        }
        $query = $this->FormUnitContents->find()
            ->where(['form_unit_id' => $entity->id]);
        /** @var EntityInterface $formUnitContent */
        foreach ($query as $formUnitContent) {
            $this->FormUnitContents->delete($formUnitContent);
        }
    }
}
