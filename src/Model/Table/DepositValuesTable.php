<?php

/**
 * Versae\Model\Table\DepositValuesTable
 */

namespace Versae\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Table deposit_values
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property FormInputsTable FormInputs
 */
class DepositValuesTable extends Table
{

    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('Deposits');
        $this->belongsTo('FormInputs');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('input_value')
            ->add(
                'input_value',
                'custom',
                [
                    'rule' => function ($value, $context) {
                        if (empty($value)) {
                            return !TableRegistry::getTableLocator()->get('FormInputs')
                                ->get($context['data']['form_input_id'])
                                ->get('required');
                        }
                        return true;
                    },
                    'message' => __("Le champ est obligatoire")
                ]
            );

        return $validator;
    }
}
