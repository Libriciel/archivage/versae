<?php

/**
 * Versae\Model\Table\ConfigurationsTable
 */

namespace Versae\Model\Table;

use AsalaeCore\Model\Table\ConfigurationsTable as CoreConfigurationsTable;

/**
 * Table configurations
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ConfigurationsTable extends CoreConfigurationsTable
{
    public const CONF_BACKGROUND_IMAGE = 'background-image';
    public const CONF_BACKGROUND_SIZE = 'background-position'; // note: position transformé en size
    public const CONF_HEADER_TITLE = 'header-title';
    public const CONF_MAIL_TITLE_PREFIX = 'mail-title-prefix';
    public const CONF_MAIL_HTML_SIGNATURE = 'mail-html-signature';
    public const CONF_MAIL_TEXT_SIGNATURE = 'mail-text-signature';

    public const KEYS = [
        self::CONF_BACKGROUND_IMAGE,
        self::CONF_BACKGROUND_SIZE,
        self::CONF_HEADER_TITLE,
        self::CONF_MAIL_TITLE_PREFIX,
        self::CONF_MAIL_HTML_SIGNATURE,
        self::CONF_MAIL_TEXT_SIGNATURE,
    ];
}
