<?php

/**
 * Versae\Model\Table\FormUnitKeywordDetailsTable
 */

namespace Versae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Table\AfterDeleteInterface;
use AsalaeCore\Model\Table\AfterSaveInterface;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Versae\Model\Entity\FormUnitKeyword;

/**
 * Table form_unit_keyword_details
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FormUnitKeywordDetailsTable extends Table implements AfterDeleteInterface, AfterSaveInterface
{
    public const NAMES_SEDA10 = [
        'KeywordContent',
        'KeywordReference',
        'KeywordType',
        'AccessRestrictionRule_Code',
        'AccessRestrictionRule_StartDate',
    ];
    public const NAMES_SEDA21 = [
        'KeywordContent',
        'KeywordReference',
        'KeywordType',
    ];

    /**
     * @var array
     */
    public $names = [];

    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('FormUnitKeywords');
        $this->belongsTo('FormVariables');
        $this->names = [
            'KeywordContent' => [
                'label' => __("Valeur du mot clé (defaut : Nom du mot clé)"),
                'required' => true,
            ],
            'KeywordReference' => [
                'label' => __("Référence du mot clé dans un référentiel"),
            ],
            'KeywordType' => [
                'label' => __("Type de mot clé"),
            ],
            'AccessRestrictionRule_Code' => [
                'label' => __("Code de restriction d'accès"),
            ],
            'AccessRestrictionRule_StartDate' => [
                'label' => __("Date de départ du calcul (restriction)"),
            ],
        ];
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Renvoi un tableau de résultats (inclus les variables manquantes)
     * @param FormUnitKeyword $formUnit
     * @return array
     */
    public function formKeywordData(FormUnitKeyword $formUnit): array
    {
        $sedaVersion = Hash::get($formUnit, 'form_unit.form.seda_version');
        if ($sedaVersion === 'seda1.0') {
            $fields = self::NAMES_SEDA10;
        } else {
            $fields = self::NAMES_SEDA21;
        }

        $names = [];
        foreach ($fields as $field) {
            $names[$field] = $this->names[$field];
        }

        $data = [];
        $entities = [];
        $availables = array_keys($names);
        /** @var EntityInterface $formUnitHeader */
        foreach (Hash::get($formUnit, 'form_unit_keyword_details') ?: [] as $formUnitHeader) {
            $name = $formUnitHeader->get('name');
            if (in_array($name, $availables)) {
                $formUnitHeader->set('label', $names[$name]['label']);
                $formUnitHeader->set('required', $names[$name]['required'] ?? false);
                $entities[$name] = $formUnitHeader;
            }
        }
        foreach ($names as $name => $params) {
            if (isset($entities[$name])) {
                $data[] = $entities[$name];
            } else {
                $data[] = [
                    'id' => null,
                    'name' => $name,
                    'label' => $params['label'],
                    'required' => $params['required'] ?? false,
                ];
            }
        }
        return $data;
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options): void
    {
        if ($entity->isNew() || $entity->isDirty()) {
            $loc = TableRegistry::getTableLocator();
            $subquery = $loc->get('FormVariables')
                ->find()
                ->select(['form_id'])
                ->where(
                    ['FormVariables.id' => $entity->get('form_variable_id')]
                );
            $loc->get('Forms')
                ->updateAll(
                    ['tested' => false],
                    ['Forms.id' => $subquery]
                );
        }
    }

    /**
     * The Model.afterDelete event is fired after an entity is deleted.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterDelete(Event $event, Entity $entity, ArrayObject $options): void
    {
        $this->FormVariables->deleteAll(['id' => $entity->get('form_variable_id')]);
    }
}
