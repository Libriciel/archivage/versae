<?php

/**
 * Versae\Model\Table\KeywordsTable
 */

namespace Versae\Model\Table;

use AsalaeCore\Model\Table\KeywordsTable as CoreKeywordsTable;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Behavior\TreeBehavior;

/**
 * Table keywords
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @mixin TreeBehavior
 */
class KeywordsTable extends CoreKeywordsTable
{
}
