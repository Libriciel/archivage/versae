<?php

/**
 * Versae\Model\Table\FormTransferHeadersTable
 */

namespace Versae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Table\AfterDeleteInterface;
use AsalaeCore\Model\Table\AfterSaveInterface;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Versae\Model\Entity\Form as FormEntity;

/**
 * Table form_transfer_headers
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FormTransferHeadersTable extends Table implements AfterDeleteInterface, AfterSaveInterface
{
    /**
     * @var array
     */
    public $names = [];

    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('Forms');
        $this->belongsTo('FormVariables');
        $part1 = [
            'Comment' => [
                'label' => __("Commentaire (vide = Nom du versement)"),
                'labelView' => __("Commentaire"),
                'section' => 'header',
            ],
            'Date' => [
                'label' => __("Date du transfert (vide = Date et heure de la création du transfert)"),
                'labelView' => __("Date du transfert"),
                'section' => 'header',
            ],
        ];
        $this->names = [
            'seda1.0' => $part1 + [
                'TransferIdentifier' => [
                    'label' => __("Identifiant du transfert (vide = Identifiant auto)"),
                    'labelView' => __("Identifiant du transfert"),
                    'section' => 'header',
                ],
                'ArchivalAgency_Identification' => [
                    'label' => __(
                        "Identifiant du service d'Archives (vide = identifiant de l'entité du Service d'Archives)"
                    ),
                    'labelView' => __("Identifiant du service d'Archives"),
                    'section' => 'header',
                ],
                'ArchivalAgency_Name' => [
                    'label' => __(
                        "Nom du service d'Archives (vide = nom de l'entité du Service d'Archives)"
                    ),
                    'labelView' => __("Nom du service d'Archives"),
                    'section' => 'header',
                ],
                'TransferringAgency_Identification' => [
                    'label' => __(
                        "Identifiant du service versant (vide = identifiant de l'entité du Service Versant)"
                    ),
                    'labelView' => __("Identifiant du service versant"),
                    'section' => 'header',
                ],
                'TransferringAgency_Name' => [
                    'label' => __("Nom du service versant (vide = nom de l'entité du Service Versant)"),
                    'labelView' => __("Nom du service versant"),
                    'section' => 'header',
                ],
            ],
            'seda2.1' => $part1 + [
                'MessageIdentifier' => [
                    'label' => __("Identifiant du transfert (vide = Identifiant auto)"),
                    'labelView' => __("Identifiant du transfert"),
                    'section' => 'header',
                ],
                'ArchivalAgreement' => [
                    'label' => __("Identifiant de l'accord de versement"),
                    'labelView' => __("Identifiant de l'accord de versement"),
                    'section' => 'header',
                ],
                'ArchivalAgency_Identifier' => [
                    'label' => __(
                        "Identifiant du service d'Archives (vide = identifiant de l'entité du Service d'Archives)"
                    ),
                    'labelView' => __("Identifiant du service d'Archives"),
                    'section' => 'header',
                ],
                'ArchivalAgency_Name' => [
                    'label' => __(
                        "Nom du service d'Archives (vide = nom de l'entité du Service d'Archives)"
                    ),
                    'labelView' => __("Nom du service d'Archives"),
                    'section' => 'header',
                ],
                'TransferringAgency_Identifier' => [
                    'label' => __(
                        "Identifiant du service versant (vide = identifiant de l'entité du Service Versant)"
                    ),
                    'labelView' => __("Identifiant du service versant"),
                    'section' => 'header',
                ],
                'TransferringAgency_Name' => [
                    'label' => __("Nom du service versant (vide = nom de l'entité du Service Versant)"),
                    'labelView' => __("Nom du service versant"),
                    'section' => 'header',
                ],
                'DataObjectPackage_ManagementMetadata_ArchivalProfile' => [
                    'label' => __("Identifiant du profil d'archive"),
                    'labelView' => __("Identifiant du profil d'archive"),
                    'section' => 'management',
                ],
                'DataObjectPackage_ManagementMetadata_ServiceLevel' => [
                    'label' => __("Identifiant du niveau de service"),
                    'labelView' => __("Identifiant du niveau de service"),
                    'section' => 'management',
                ],
            ],
        ];

        $additionnals = [
            'AcquisitionInformation' => __("Modalités d'entrée des archives."),
            'LegalStatus' => __("Statut des archives échangées."),
            'OriginatingAgencyIdentifier' => '',
            'SubmissionAgencyIdentifier' => '',
            'StorageRule_Rule' => '',
            'StorageRule_StartDate' => '',
            'StorageRule_FinalAction' => '',
            'AppraisalRule_Rule' => '',
            'AppraisalRule_StartDate' => '',
            'AppraisalRule_FinalAction' => '',
            'AccessRule_Rule' => '',
            'AccessRule_StartDate' => '',
            'DisseminationRule_Rule' => '',
            'DisseminationRule_StartDate' => '',
            'ReuseRule_Rule' => '',
            'ReuseRule_StartDate' => '',
            'ClassificationRule_Rule' => '',
            'ClassificationRule_StartDate' => '',
            'ClassificationRule_ClassificationAudience' => '',
            'ClassificationRule_ClassificationLevel' => '',
            'ClassificationRule_ClassificationOwner' => '',
            'ClassificationRule_ClassificationReassessingDate' => '',
            'ClassificationRule_NeedReassessingAuthorization' => '',
            'NeedAuthorization' => '',
        ];
        foreach ($additionnals as $name => $label) {
            $label = $label ? "$name - $label" : $name;
            $name = "DataObjectPackage_ManagementMetadata_$name";
            $this->names['seda2.1'][$name] = [
                'label' => $label,
                'labelView' => $label,
                'section' => 'management',
                'additionnal' => true,
            ];
        }

        $versions = [
            'ReplyCodeListVersion',
            'MessageDigestAlgorithmCodeListVersion',
            'MimeTypeCodeListVersion',
            'EncodingCodeListVersion',
            'FileFormatCodeListVersion',
            'CompressionAlgorithmCodeListVersion',
            'DataObjectVersionCodeListVersion',
            'StorageRuleCodeListVersion',
            'AppraisalRuleCodeListVersion',
            'AccessRuleCodeListVersion',
            'DisseminationRuleCodeListVersion',
            'ReuseRuleCodeListVersion',
            'ClassificationRuleCodeListVersion',
            'AcquisitionInformationCodeListVersion',
            'AuthorizationReasonCodeListVersion',
            'RelationshipCodeListVersion',
        ];
        foreach ($versions as $name) {
            $label = $name;
            $name = "CodeListVersions_$name";
            $this->names['seda2.1'][$name] = [
                'label' => $label,
                'labelView' => $label,
                'section' => 'version',
                'additionnal' => true,
            ];
        }

        $this->names['seda2.2'] = $this->names['seda2.1'];
        $additionnals = [
            'HoldRule_Rule' => '',
            'HoldRule_StartDate' => '',
            'HoldRule_HoldEndDate' => '',
            'HoldRule_HoldOwner' => '',
            'HoldRule_HoldReassessingDate' => '',
            'HoldRule_HoldReason' => '',
            'HoldRule_PreventRearrangement' => '',
        ];
        foreach ($additionnals as $name => $label) {
            $label = $label ? "$name - $label" : $name;
            $name = "DataObjectPackage_ManagementMetadata_$name";
            $this->names['seda2.2'][$name] = [
                'label' => $label,
                'labelView' => $label,
                'section' => 'management',
                'additionnal' => true,
            ];
        }

        $versions = [
            'HoldRuleCodeListVersion',
        ];
        foreach ($versions as $name) {
            $this->names['seda2.2']["CodeListVersions_$name"] = [
                'label' => $name,
                'labelView' => $name,
                'section' => 'version',
                'additionnal' => true,
            ];
        }
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Renvoi un tableau de résultats (inclus les variables manquantes)
     * @param FormEntity $formEntity
     * @return array
     */
    public function formHeadersData(FormEntity $formEntity): array
    {
        $data = [];
        $entities = [];
        $names = $this->names[$formEntity->get('seda_version')];
        /** @var EntityInterface $formTransferHeader */
        foreach (Hash::get($formEntity, 'form_transfer_headers') ?: [] as $formTransferHeader) {
            $name = $formTransferHeader->get('name');
            if (!isset($names[$name])) {
                continue; // schema modifié ?
            }
            $formTransferHeader->set('label', $names[$name]['label']);
            $formTransferHeader->set('required', $names[$name]['required'] ?? false);
            $formTransferHeader->set('section', $names[$name]['section']);
            $formTransferHeader->set('additionnal', $names[$name]['additionnal'] ?? false);
            $entities[$name] = $formTransferHeader;
        }
        foreach ($names as $name => $params) {
            if (isset($entities[$name])) {
                $data[] = $entities[$name];
            } else {
                $data[] = [
                    'id' => null,
                    'name' => $name,
                    'label' => $params['label'],
                    'required' => $params['required'] ?? false,
                    'section' => $params['section'],
                    'additionnal' => $params['additionnal'] ?? false,
                ];
            }
        }
        return $data;
    }

    /**
     * Renvoi un tableau de résultats (sans les variables manquantes et sans label required)
     * @param FormEntity $formEntity
     * @return array
     */
    public function formHeadersDataForView(FormEntity $formEntity): array
    {
        $data = [];
        $names = $this->names[$formEntity->get('seda_version')];
        $availables = array_keys($names);
        /** @var EntityInterface $formTransferHeader */
        foreach (Hash::get($formEntity, 'form_transfer_headers') ?: [] as $formTransferHeader) {
            $name = $formTransferHeader->get('name');
            if (in_array($name, $availables)) {
                $formTransferHeader->set('label', $names[$name]['labelView']);
                $data[] = $formTransferHeader;
            }
        }
        return $data;
    }

    /**
     * The Model.afterDelete event is fired after an entity is deleted.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterDelete(Event $event, Entity $entity, ArrayObject $options): void
    {
        $this->getAssociation('FormVariables')
            ->deleteAll(['id' => $entity->get('form_variable_id')]);
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options): void
    {
        if ($entity->isNew() || $entity->isDirty()) {
            TableRegistry::getTableLocator()->get('Forms')
                ->updateAll(
                    ['tested' => false],
                    ['id' => $entity->get('form_id')]
                );
        }
    }
}
