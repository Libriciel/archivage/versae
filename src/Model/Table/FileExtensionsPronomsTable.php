<?php

/**
 * Versae\Model\Table\FileExtensionsPronomsTable
 */

namespace Versae\Model\Table;

use AsalaeCore\Model\Table\FileExtensionsPronomsTable as CoreFileExtensionsPronomsTable;
use Cake\ORM\Behavior\TimestampBehavior;

/**
 * Table file_extensions_pronoms
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class FileExtensionsPronomsTable extends CoreFileExtensionsPronomsTable
{
}
