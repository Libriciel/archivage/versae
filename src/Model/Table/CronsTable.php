<?php

/**
 * Versae\Model\Table\CronsTable
 */

namespace Versae\Model\Table;

use AsalaeCore\Model\Table\CronsTable as CoreCronsTable;

/**
 * Table crons
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class CronsTable extends CoreCronsTable
{
}
