<?php

/**
 * Versae\Model\Table\SavedFiltersTable
 */

namespace Versae\Model\Table;

use AsalaeCore\Model\Table\SavedFiltersTable as CoreSavedFiltersTable;

/**
 * Table saved_filters
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class SavedFiltersTable extends CoreSavedFiltersTable
{
}
