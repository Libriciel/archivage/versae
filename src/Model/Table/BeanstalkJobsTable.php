<?php

/**
 * Versae\Model\Table\BeanstalkJobsTable
 */

namespace Versae\Model\Table;

use AsalaeCore\Model\Behavior\OptionsBehavior;
use Beanstalk\Model\Table\BeanstalkJobsTable as BeanstalkBeanstalkJobsTable;
use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use StateMachine\Model\Behavior\StateMachineBehavior;

/**
 * Table beanstalk_jobs
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin OptionsBehavior
 * @mixin StateMachineBehavior
 * @mixin TimestampBehavior
 */
class BeanstalkJobsTable extends BeanstalkBeanstalkJobsTable
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');
        $this->addBehavior('StateMachine.StateMachine', ['fields' => ['state' => 'job_state']]);
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'state' => [
                    'ready',
                    'reserved',
                    'delayed',
                    'failed',
                ],
                'job_state' => [
                    BeanstalkBeanstalkJobsTable::S_PENDING,
                    BeanstalkBeanstalkJobsTable::S_WORKING,
                    BeanstalkBeanstalkJobsTable::S_FAILED,
                ],
            ]
        );
        $this->setEntityClass('BeanstalkJob');
        $this->belongsTo('BeanstalkWorkers');
        $this->belongsTo('Users');
        $this->belongsTo('Deposits')
            ->setForeignKey('object_foreign_key')
            ->setConditions(
                function (QueryExpression $qe, Query $q) {
                    $assocAlias = $q->getRepository()->getAlias(); // Deposits
                    $modelAlias = $this->getAlias();
                    return $qe->and(["$modelAlias.object_model" => $assocAlias]);
                }
            );
    }
}
