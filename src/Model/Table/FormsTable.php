<?php

/**
 * Versae\Model\Table\FormsTable
 */

namespace Versae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\BeforeDeleteInterface;
use AsalaeCore\Model\Table\BeforeSaveInterface;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Http\Exception\ForbiddenException;
use Cake\Log\Log;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Exception\PersistenceFailedException;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Utility\Text;
use Cake\Validation\Validator;
use DateTime;
use StateMachine\Model\Behavior\StateMachineBehavior;
use Versae\Exception\GenericException;

/**
 * Table forms
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin OptionsBehavior
 * @mixin StateMachineBehavior
 * @mixin TimestampBehavior
 * @property FormTransferHeadersTable FormTransferHeaders
 * @property FormCalculatorsTable FormCalculators
 * @property FormExtractorsTable FormExtractors
 * @property FormFieldsetsTable FormFieldsets
 * @property FormInputsTable FormInputs
 * @property FormVariablesTable FormVariables
 * @property FormsTransferringAgenciesTable FormsTransferringAgencies
 * @property FormUnitsTable FormUnits
 * @property DepositsTable Deposits
 */
class FormsTable extends Table implements BeforeSaveInterface, BeforeDeleteInterface
{
    public const S_EDITING = 'editing';
    public const S_PUBLISHED = 'published';
    public const S_DEPRECATED = 'deprecated';
    public const S_DEACTIVATED = 'deactivated';

    public const T_PUBLISH = 'publish';
    public const T_UNPUBLISH = 'unpublish';
    public const T_DEACTIVATE = 'deactivate';
    public const T_ACTIVATE = 'activate';
    public const T_DEPRECIATE = 'depreciate';

    /**
     * Etat initial
     *
     * @var string
     */
    public $initialState = self::S_EDITING;
    /**
     * Dernière entité importé
     * @var EntityInterface|null
     */
    public $lastImport = null;

    /**
     * Changement d'êtats selon action
     *
     * @var array
     */
    public $transitions = [
        self::T_PUBLISH => [
            self::S_EDITING => self::S_PUBLISHED,
        ],
        self::T_UNPUBLISH => [
            self::S_PUBLISHED => self::S_EDITING,
        ],
        self::T_DEACTIVATE => [
            self::S_PUBLISHED => self::S_DEACTIVATED,
            self::S_DEPRECATED => self::S_DEACTIVATED,
        ],
        self::T_ACTIVATE => [
            self::S_DEACTIVATED => self::S_PUBLISHED,
        ],
        self::T_DEPRECIATE => [
            self::S_PUBLISHED => self::S_DEPRECATED,
            self::S_DEACTIVATED => self::S_DEPRECATED,
            self::S_EDITING => self::S_DEPRECATED,
        ],
    ];

    /**
     * @var EntityInterface
     */
    private EntityInterface $originalForm;
    /**
     * @var EntityInterface
     */
    private EntityInterface $newForm;

    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
        $this->addBehavior('StateMachine.StateMachine');
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'state' => [
                    self::S_DEPRECATED,
                    self::S_EDITING,
                    self::S_DEACTIVATED,
                    self::S_PUBLISHED,
                ],
            ]
        );

        $this->belongsTo('OrgEntities');
        $this->belongsToMany('TransferringAgencies')
            ->setClassName('OrgEntities')
            ->setThrough('FormsTransferringAgencies')
            ->setTargetForeignKey('org_entity_id');
        $this->belongsTo('ArchivingSystems');
        $this->hasMany('FormTransferHeaders');
        $this->hasMany('FormCalculators')->setDependent(true);
        $this->hasMany('FormExtractors')->setDependent(true);
        $this->hasMany('FormFieldsets')->setDependent(true);
        $this->hasMany('FormInputs')->setDependent(true);
        $this->hasMany('FormVariables')->setDependent(true);
        $this->hasMany('FormsTransferringAgencies');
        $this->hasMany('FormUnits');
        $this->hasMany('Deposits');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name')
            ->add(
                'name',
                [
                    'unicity' => [
                        'rule' => function ($value, $context) use ($OrgEntities) {
                            if ($context['newRecord']) {
                                $orgEntityId = Hash::get($context, 'data.org_entity_id');
                                $identifier = Hash::get($context, 'data.identifier');
                            } else {
                                $entity = $this->get(Hash::get($context, 'data.id'));
                                $orgEntityId = $entity->get('org_entity_id');
                                $identifier = $entity->get('identifier');
                            }

                            $orgEntity = $OrgEntities->find()
                                ->where(['OrgEntities.id' => $orgEntityId])
                                ->contain('ArchivalAgencies')
                                ->first();

                            $archivalAgencyId = Hash::get(
                                $orgEntity,
                                'archival_agency.id',
                                Hash::get($orgEntity, 'id') // cas du SA lui même
                            );

                            return $this->find()
                                ->select(['existing' => 1])
                                ->innerJoinWith('OrgEntities')
                                ->innerJoinWith('OrgEntities.ArchivalAgencies')
                                ->where(
                                    [
                                        'ArchivalAgencies.id' => $archivalAgencyId,
                                        'Forms.name' => $value,
                                        'Forms.identifier !=' => $identifier,
                                    ]
                                )
                                ->count() === 0;
                        },
                        'message' => __("Ce nom est déjà utilisé")
                    ]
                ]
            );

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->integer('archiving_system_id')
            ->requirePresence('archiving_system_id');

        $validator
            ->scalar('identifier')
            ->maxLength('identifier', 255)
            ->notEmptyString('identifier');

        $validator
            ->integer('version_number')
            ->requirePresence('version_number', 'create')
            ->notEmptyString('version_number');

        $validator
            ->scalar('version_note')
            ->requirePresence('version_note', 'create')
            ->notEmptyString('version_note');

        $validator
            ->scalar('state')
            ->maxLength('state', 255)
            ->requirePresence('state', 'create')
            ->notEmptyString('state');

        $validator
            ->dateTime('last_state_update')
            ->allowEmptyDateTime('last_state_update');

        $validator
            ->scalar('states_history')
            ->allowEmptyString('states_history');

        $validator
            ->scalar('user_guide')
            ->allowEmptyString('user_guide');

        $validator
            ->scalar('seda_version')
            ->maxLength('seda_version', 255)
            ->notEmptyString('seda_version');

        $validator
            ->requirePresence('transferring_agencies')
            ->notEmptyArray('transferring_agencies');

        return $validator;
    }

    /**
     * The Model.beforeSave event is fired before an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeSave(Event $event, Entity $entity, ArrayObject $options): void
    {
        if ($entity->isNew()) {
            if (!empty($entity->get('identifier'))) {
                $lastVersion = $this->find()
                    ->where(['identifier' => $entity->get('identifier')])
                    ->orderDesc('version_number')
                    ->first();
                if ($lastVersion) {
                    $entity->set('version_number', $lastVersion->get('version_number') + 1);
                } elseif ($entity->get('version_number') === 0) {
                    $entity->set('version_number', 1);
                }
            } else {
                $entity->set('identifier', uuid_create(UUID_TYPE_TIME));
                $entity->set('version_number', 1);
            }
        }
    }

    /**
     * Allows listeners to modify the rules checker by adding more rules.
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->addDelete(
            function (EntityInterface $entity) {
                return $entity->get('deletable');
            }
        );
        return parent::buildRules($rules);
    }

    /**
     * Publie un formulaire et déprécie sa version précédente
     * @param string $id
     * @return EntityInterface
     */
    public function publish(string $id): EntityInterface
    {
        $entity = $this->find()
            ->where(['Forms.id' => $id])
            ->contain(['ArchivingSystems'])
            ->firstOrFail();

        if (
            !$entity->get('publishable')
            || !$this->transition($entity, self::T_PUBLISH)
        ) {
            throw new ForbiddenException();
        }

        // S'il existe une version précédente du formulaire à l'état publié, deprécier cette dernière.
        $this->save($entity);
        $query = $this->find()
            ->where(
                [
                    'Forms.identifier' => $entity->get('identifier'),
                    'Forms.state' => self::S_PUBLISHED,
                    'Forms.id !=' => $id,
                ]
            );
        foreach ($query as $prevVersion) {
            $this->transition($prevVersion, self::T_DEPRECIATE);
            $this->save($prevVersion);
        }

        return $entity;
    }

    /**
     * Duplique un form avec toutes ses entités liées
     * @param string $id
     * @return EntityInterface avec erreurs si problème
     */
    public function duplicate(string $id): EntityInterface
    {
        $originalForm = $this->getOriginalForm($id);

        $versionNote = __(
            "Copie du formulaire {0}",
            $originalForm->get('name')
        );

        $data = [
            'name' => substr($originalForm->get('name'), 0, 250) . '_copy_' . uniqid(),
            'state' => self::S_EDITING,
            'version_note' => $versionNote,
            'identifier' => uuid_create(UUID_TYPE_TIME),
        ] + $originalForm->toArray();

        unset(
            $data['id'],
            $data['created'],
            $data['modified'],
        );

        return $this->duplicateOrVersion($originalForm, $data);
    }

    /**
     * Versionne un form
     * @param string $id
     * @param string $versionNote
     * @return EntityInterface
     */
    public function version(string $id, string $versionNote): EntityInterface
    {
        $originalForm = $this->getOriginalForm($id);

        $data = [
            'state' => self::S_EDITING,
            'version_note' => $versionNote,
        ] + $originalForm->toArray();

        unset(
            $data['id'],
            $data['created'],
            $data['modified'],
        );

        return $this->duplicateOrVersion($originalForm, $data);
    }

    /**
     * Récupération d'un form original pour la copie ou la duplication
     * @param string $id
     * @return EntityInterface
     */
    protected function getOriginalForm(string $id): EntityInterface
    {
        return TableRegistry::getTableLocator()->get('Forms')
            ->find()
            ->contain(
                [
                    'TransferringAgencies',
                    'FormTransferHeaders',
                    'FormFieldsets' => [
                        'FormInputs',
                        'sort' => ['FormFieldsets.id'],
                    ],
                    'FormCalculators' => [
                        'ManyFormVariables',
                        'sort' => ['FormCalculators.id'],
                    ],
                    'FormExtractors' => [
                        'FormExtractorsFormVariables',
                        'sort' => ['FormExtractors.id'],
                    ],
                    'FormVariables' => [
                        'ManyFormCalculators',
                        'FormInputs',
                        'FormExtractors',
                        'FormCalculatorsFormVariables',
                        'sort' => ['FormVariables.id'],
                    ],
                    'FormUnits' => [
                        'ParentFormUnits',
                        'FormUnitContents',
                        'FormUnitKeywords' => [
                            'FormUnitKeywordDetails',
                            'sort' => ['FormUnitKeywords.id'],
                        ],
                        'FormUnitHeaders',
                        'FormUnitManagements',
                        'sort' => ['FormUnits.lft'],
                    ],
                    'FormInputs' => [
                        'FormExtractors',
                        'FormVariables' => [
                            'FormTransferHeaders',
                            'FormUnitHeaders',
                            'FormUnitContents',
                            'FormUnitManagements',
                        ],
                        'FormInputsFormVariables',
                        'sort' => ['FormInputs.id'],
                    ],
                ]
            )
            ->where(['Forms.id' => $id])
            ->firstOrFail();
    }

    /**
     * Duplique un form avec toutes ses entités liées
     * @param EntityInterface $originalForm
     * @param array           $data
     * @return EntityInterface avec erreurs si problème
     */
    public function duplicateOrVersion(EntityInterface $originalForm, array $data): EntityInterface
    {
        $this->originalForm = $originalForm;
        unset($originalForm);
        $this->newForm = $this->newEntity($data, ['associated' => []]);
        $this->saveOrFail($this->newForm);

        $conn = $this->getConnection();
        $conn->begin();
        try {
            $this->duplicateOrVersionFormsTransferringAgencies();
            $variableIds = $this->duplicateOrVersionFormVariables();
            $this->duplicateOrVersionFormsFormTransferHeaders($variableIds);
            $fieldsetsIds = $this->duplicateOrVersionFormsFieldsets();
            $inputsIds = $this->duplicateOrVersionFormsInputs($fieldsetsIds);
            $unitsIds = $this->duplicateOrVersionFormsUnits($inputsIds, $fieldsetsIds, $variableIds);
            $this->duplicateOrVersionFormUnitHeaders($unitsIds, $variableIds);
            $this->duplicateOrVersionFormUnitContents($unitsIds, $variableIds);
            $this->duplicateOrVersionFormsFormUnitManagements($unitsIds, $variableIds);
            $formUnitKeywordIds = $this->duplicateOrVersionFormsFormUnitKeywords($unitsIds);
            $this->duplicateOrVersionFormUnitKeywordDetails($formUnitKeywordIds, $variableIds);
            $calculatorIds = $this->duplicateOrVersionFormCalculators($variableIds);
            $this->duplicateOrVersionFormCalculatorsFormVariables($variableIds, $calculatorIds);
            $this->duplicateOrVersionFormInputsFormVariables($inputsIds, $variableIds);
            $extractorIds = $this->duplicateOrVersionFormExtractors($inputsIds);
            $this->duplicateOrVersionFormExtractorsFormVariables($extractorIds, $variableIds);

            $conn->commit();

            return $this->find()
                ->where(['Forms.id' => $this->newForm->id])
                ->contain(['ArchivingSystems'])
                ->first();
        } catch (PersistenceFailedException $e) {
            Log::error($e);
            if ($conn->inTransaction()) {
                $conn->rollback();
            }
            return $this->newForm;
        }
    }

    /**
     * Donne la requête pour une edition d'un formulaire
     * @return Query
     */
    public function findEditableEntity(): Query
    {
        return $this->find()
            ->contain(
                [
                    'TransferringAgencies',
                    'FormExtractors' => fn(Query $q)
                        => $q->contain(['FormInputs'])
                        ->order(['FormExtractors.name']),
                    'FormCalculators' => fn(Query $q)
                        => $q->contain(['FormVariables'])
                        ->order(['FormCalculators.id']),
                    'FormTransferHeaders' => ['FormVariables'],
                    'FormFieldsets' => fn(Query $q)
                        => $q->contain(['FormInputs' => fn(Query $sq) => $sq->order(['FormInputs.ord'])])
                        ->order(['FormFieldsets.ord']),
                ]
            );
    }

    /**
     * Retourne un formulaire publié plus récent s'il existe,
     * null sinon
     * @param EntityInterface|null $form
     * @return EntityInterface|null
     */
    public function getNewerForm($form): ?EntityInterface
    {
        return $this->find()
            ->where(
                [
                    'identifier' => Hash::get($form, 'identifier'),
                    'version_number >' => Hash::get($form, 'version_number'),
                    'state' => self::S_PUBLISHED,
                ]
            )
            ->order(
                [
                    'version_number' => 'desc',
                    'id' => 'desc',
                ]
            )
            ->first();
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeDelete(Event $event, Entity $entity, ArrayObject $options): void
    {
        // desactivation de la version n-1 si elle est deprecated
        $previous = $this->find()
            ->where(
                [
                    'identifier' => $entity->get('identifier'),
                    'version_number <' => $entity->get('version_number'),
                    'state' => self::S_DEPRECATED,
                ]
            )
            ->orderDesc('version_number')
            ->first();

        if ($previous) {
            $this->transition($previous, self::T_DEACTIVATE);
            $this->save($previous);
        }

        // Suppression des entités liées
        $this->FormTransferHeaders->deleteAll(['form_id' => $entity->id]);
        $this->FormCalculators->deleteAll(['form_id' => $entity->id]);
        $this->FormExtractors->deleteAll(['form_id' => $entity->id]);
        $this->FormUnits->deleteAll(['form_id' => $entity->id]);
        $this->FormFieldsets->deleteAll(['form_id' => $entity->id]);
        $this->FormInputs->deleteAll(['form_id' => $entity->id]);
        $this->FormVariables->deleteAll(['form_id' => $entity->id]);
        $this->FormsTransferringAgencies->deleteAll(['form_id' => $entity->id]);
        $this->Deposits->deleteAll(['form_id' => $entity->id]);
    }

    /**storage_calculation
     * Duplication / Versionnage pour FormsTransferringAgenciesstorage_calculation
     * @return void
     */
    protected function duplicateOrVersionFormsTransferringAgencies(): void
    {
        $FormsTransferringAgencies = TableRegistry::getTableLocator()->get('FormsTransferringAgencies');
        foreach ($this->originalForm->get('transferring_agencies') as $transferringAgency) {
            $FormsTransferringAgencies->saveOrFail(
                $FormsTransferringAgencies->newEntity(
                    [
                        'form_id' => $this->newForm->id,
                        'org_entity_id' => $transferringAgency->get('id'),
                    ]
                )
            );
        }
    }

    /**
     * Duplication / Versionnage pour FormVariables
     * @return array
     */
    protected function duplicateOrVersionFormVariables(): array
    {
        $variableIds = [];
        $Variables = TableRegistry::getTableLocator()->get('FormVariables');
        foreach ($this->originalForm->get('form_variables') as $variable) {
            $Variables->saveOrFail(
                $newVariable = $Variables->newEntity(
                    [
                        'form_id' => $this->newForm->id,
                        'type' => $variable->get('type'),
                        'twig' => $variable->get('twig'),
                        'conditions' => $variable->get('conditions'),
                        'conditions_else_value' => $variable->get('conditions_else_value'),
                        'conditions_elseif_value' => $variable->get('conditions_elseif_value'),
                        'conditions_if_value' => $variable->get('conditions_if_value'),
                        'date_format' => $variable->get('date_format'),
                        'switch_default_value' => $variable->get('switch_default_value'),
                        'switch_field' => $variable->get('switch_field'),
                        'switch_options' => $variable->get('switch_options'),
                    ]
                )
            );
            $variableIds[$variable->get('id')] = $newVariable->get('id');
        }
        return $variableIds;
    }

    /**
     * Duplication / Versionnage pour FormTransferHeaders
     * @param array $variableIds
     * @return void
     */
    protected function duplicateOrVersionFormsFormTransferHeaders(
        array $variableIds
    ): void {
        $FormTransferHeaders = TableRegistry::getTableLocator()->get('FormTransferHeaders');
        foreach ($this->originalForm->get('form_transfer_headers') as $transferHeader) {
            $FormTransferHeaders->saveOrFail(
                $FormTransferHeaders->newEntity(
                    [
                        'name' => $transferHeader->get('name'),
                        'form_id' => $this->newForm->id,
                        'form_variable_id' => $variableIds[$transferHeader->get('form_variable_id')],
                    ]
                )
            );
        }
    }

    /**
     * Duplication / Versionnage pour FormsFieldsets
     * @return array
     */
    protected function duplicateOrVersionFormsFieldsets(): array
    {
        $fieldsetsIds = [];
        $Fieldsets = TableRegistry::getTableLocator()->get('FormFieldsets');
        foreach ($this->originalForm->get('form_fieldsets') as $fieldset) {
            $Fieldsets->saveOrFail(
                $newFieldset = $Fieldsets->newEntity(
                    [
                        'form_id' => $this->newForm->id,
                        'legend' => $fieldset->get('legend'),
                        'ord' => $fieldset->get('ord'),
                        'disable_expression' => $fieldset->get('disable_expression'),
                        'repeatable' => $fieldset->get('repeatable'),
                        'required' => $fieldset->get('required'),
                        'cardinality' => $fieldset->get('cardinality'),
                        'button_name' => $fieldset->get('button_name'),
                    ]
                )
            );
            $fieldsetsIds[$fieldset->get('id')] = $newFieldset->get('id');
        }
        return $fieldsetsIds;
    }

    /**
     * Duplication / Versionnage pour FormInputs
     * @param array $fieldsetsIds
     * @return array
     */
    protected function duplicateOrVersionFormsInputs(array $fieldsetsIds): array
    {
        $inputsIds = [];
        $Inputs = TableRegistry::getTableLocator()->get('FormInputs');
        foreach ($this->originalForm->get('form_inputs') as $input) {
            $Inputs->saveOrFail(
                $newInput = $Inputs->newEntity(
                    [
                        'form_fieldset_id' => $fieldsetsIds[$input->get('form_fieldset_id')],
                        'name' => $input->get('name'),
                        'type' => $input->get('type'),
                        'ord' => $input->get('ord'),
                        'label' => $input->get('label'),
                        'default_value' => $input->get('default_value'),
                        'required' => $input->get('required'),
                        'readonly' => $input->get('readonly'),
                        'disable_expression' => $input->get('disable_expression'),
                        'pattern' => $input->get('pattern'),
                        'placeholder' => $input->get('placeholder'),
                        'help' => $input->get('help'),
                        'form_id' => $this->newForm->id,
                        'multiple' => $input->get('multiple'),
                        'checked' => $input->get('checked'),
                        'color' => $input->get('color'),
                        'empty' => $input->get('empty'),
                        'formats' => $input->get('formats'),
                        'max' => $input->get('max'),
                        'min' => $input->get('min'),
                        'options' => $input->get('options'),
                        'p' => $input->get('p'),
                        'select2' => $input->get('select2'),
                        'step' => $input->get('step'),
                        'value' => $input->get('value'),
                    ]
                )
            );
            $inputsIds[$input->get('id')] = $newInput->get('id');
        }
        return $inputsIds;
    }

    /**
     * Duplication / Versionnage pour FormUnits
     * @param array $inputsIds
     * @param array $fieldsetsIds
     * @param array $variableIds
     * @return array
     */
    protected function duplicateOrVersionFormsUnits(
        array $inputsIds,
        array $fieldsetsIds,
        array $variableIds
    ): array {
        $unitsIds = [];
        $FormUnits = TableRegistry::getTableLocator()->get('FormUnits');
        foreach ($this->originalForm->get('form_units') as $formUnit) {
            $formInputId = $formUnit->get('form_input_id');
            $repeatableFieldsetId = $formUnit->get('repeatable_fieldset_id');
            $presenceCond = $formUnit->get('presence_condition_id');
            $parentId = $formUnit->get('parent_id') ? $unitsIds[$formUnit->get('parent_id')] : null;
            $FormUnits->saveOrFail(
                $newFormUnit = $FormUnits->newEntity(
                    [
                        'form_id' => $this->newForm->id,
                        'name' => $formUnit->get('name'),
                        'type' => $formUnit->get('type'),
                        'parent_id' => $parentId,
                        'form_input_id' => $formInputId ? $inputsIds[$formInputId] : null,
                        'presence_condition_id' => $presenceCond ? $variableIds[$presenceCond] : null,
                        'search_expression' => $formUnit->get('search_expression'),
                        'presence_required' => $formUnit->get('presence_required'),
                        'storage_calculation' => $formUnit->get('storage_calculation'),
                        'repeatable_fieldset_id' => $repeatableFieldsetId ? $fieldsetsIds[$repeatableFieldsetId] : null,
                    ]
                )
            );
            $unitsIds[$formUnit->get('id')] = $newFormUnit->get('id');
        }
        return $unitsIds;
    }

    /**
     * Duplication / Versionnage pour FormUnitHeaders
     * @param array $unitsIds
     * @param array $variableIds
     * @return void
     */
    protected function duplicateOrVersionFormUnitHeaders(
        array $unitsIds,
        array $variableIds
    ): void {
        $FormUnitHeaders = TableRegistry::getTableLocator()->get('FormUnitHeaders');
        foreach (Hash::extract($this->originalForm, 'form_units.{n}.form_unit_headers.{n}') as $unitHeader) {
            $FormUnitHeaders->saveOrFail(
                $FormUnitHeaders->newEntity(
                    [
                        'name' => $unitHeader['name'],
                        'form_unit_id' => $unitsIds[$unitHeader['form_unit_id']],
                        'form_variable_id' => $variableIds[$unitHeader['form_variable_id']],
                    ]
                )
            );
        }
    }

    /**
     * Duplication / Versionnage pour FormUnitContents
     * @param array $unitsIds
     * @param array $variableIds
     * @return void
     */
    protected function duplicateOrVersionFormUnitContents(
        array $unitsIds,
        array $variableIds
    ): void {
        $FormUnitContents = TableRegistry::getTableLocator()->get('FormUnitContents');
        foreach (Hash::extract($this->originalForm, 'form_units.{n}.form_unit_contents.{n}') as $unitContent) {
            $FormUnitContents->saveOrFail(
                $FormUnitContents->newEntity(
                    [
                        'name' => $unitContent['name'],
                        'form_unit_id' => $unitsIds[$unitContent['form_unit_id']],
                        'form_variable_id' => $variableIds[$unitContent['form_variable_id']],
                    ]
                )
            );
        }
    }

    /**
     * Duplication / Versionnage pour FormUnitManagements
     * @param array $unitsIds
     * @param array $variableIds
     * @return void
     */
    protected function duplicateOrVersionFormsFormUnitManagements(
        array $unitsIds,
        array $variableIds
    ): void {
        $FormUnitManagements = TableRegistry::getTableLocator()->get('FormUnitManagements');
        $formUnitManagements = Hash::extract(
            $this->originalForm,
            'form_units.{n}.form_unit_managements.{n}'
        );
        foreach ($formUnitManagements as $formUnitManagement) {
            $FormUnitManagements->saveOrFail(
                $FormUnitManagements->newEntity(
                    [
                        'name' => $formUnitManagement['name'],
                        'form_unit_id' => $unitsIds[$formUnitManagement['form_unit_id']],
                        'form_variable_id' => $variableIds[$formUnitManagement['form_variable_id']],
                    ]
                )
            );
        }
    }

    /**
     * Duplication / Versionnage pour FormUnitKeywords
     * @param array $unitsIds
     * @return array
     */
    protected function duplicateOrVersionFormsFormUnitKeywords(
        array $unitsIds
    ): array {
        $formUnitKeywordIds = [];
        $FormUnitKeywords = TableRegistry::getTableLocator()->get('FormUnitKeywords');
        foreach (Hash::extract($this->originalForm, 'form_units.{n}.form_unit_keywords.{n}') as $formUnitKeyword) {
            $FormUnitKeywords->saveOrFail(
                $newFormUnitKeyword = $FormUnitKeywords->newEntity(
                    [
                        'name' => $formUnitKeyword['name'],
                        'form_unit_id' => $unitsIds[$formUnitKeyword['form_unit_id']],
                        'multiple_with' => $formUnitKeyword['multiple_with'],
                    ]
                )
            );
            $formUnitKeywordIds[$formUnitKeyword['id']] = $newFormUnitKeyword->get('id');
        }
        return $formUnitKeywordIds;
    }

    /**
     * Duplication / Versionnage pour FormUnitKeywordDetails
     * @param array $formUnitKeywordIds
     * @param array $variableIds
     * @return void
     */
    protected function duplicateOrVersionFormUnitKeywordDetails(
        array $formUnitKeywordIds,
        array $variableIds
    ): void {
        $FormUnitKeywordDetails = TableRegistry::getTableLocator()->get('FormUnitKeywordDetails');
        $formUnitKeywordDetails = Hash::extract(
            $this->originalForm,
            'form_units.{n}.form_unit_keywords.{n}.form_unit_keyword_details.{n}'
        );
        foreach ($formUnitKeywordDetails as $formUnitKeywordDetail) {
            $FormUnitKeywordDetails->saveOrFail(
                $FormUnitKeywordDetails->newEntity(
                    [
                        'name' => $formUnitKeywordDetail['name'],
                        'form_unit_keyword_id'
                            => $formUnitKeywordIds[$formUnitKeywordDetail['form_unit_keyword_id']],
                        'form_variable_id' => $variableIds[$formUnitKeywordDetail['form_variable_id']],
                    ]
                )
            );
        }
    }

    /**
     * Duplication / Versionnage pour FormCalculators
     * @param array $variableIds
     * @return array
     */
    protected function duplicateOrVersionFormCalculators(
        array $variableIds
    ): array {
        $calculatorIds = [];
        $FormCalculators = TableRegistry::getTableLocator()->get('FormCalculators');
        foreach ($this->originalForm->get('form_calculators') as $calculator) {
            $FormCalculators->saveOrFail(
                $newCalculator = $FormCalculators->newEntity(
                    [
                        'form_id' => $this->newForm->id,
                        'name' => $calculator->get('name'),
                        'description' => $calculator->get('description'),
                        'form_variable_id' => $variableIds[$calculator->get('form_variable_id')],
                    ]
                )
            );
            $calculatorIds[$calculator->get('id')] = $newCalculator->get('id');
        }
        return $calculatorIds;
    }

    /**
     * Duplication / Versionnage pour FormCalculatorsFormVariables
     * @param array $variableIds
     * @param array $calculatorIds
     * @return void
     */
    protected function duplicateOrVersionFormCalculatorsFormVariables(
        array $variableIds,
        array $calculatorIds
    ): void {
        if ($variableIds) {
            $CalculatorsVariables = TableRegistry::getTableLocator()->get('FormCalculatorsFormVariables');
            $calculatorVariables = Hash::extract(
                $this->originalForm,
                'form_variables.{n}.form_calculators_form_variables.{n}'
            );
            foreach ($calculatorVariables as $calculatorVariable) {
                $CalculatorsVariables->saveOrFail(
                    $CalculatorsVariables->newEntity(
                        [
                            'form_calculator_id' => $calculatorIds[$calculatorVariable['form_calculator_id']],
                            'form_variable_id' => $variableIds[$calculatorVariable['form_variable_id']],
                        ]
                    )
                );
            }
        }
    }

    /**
     * Duplication / Versionnage pour FormInputsFormVariables
     * @param array $inputsIds
     * @param array $variableIds
     * @return void
     */
    protected function duplicateOrVersionFormInputsFormVariables(
        array $inputsIds,
        array $variableIds
    ): void {
        if ($inputsIds) {
            $InputsVariables = TableRegistry::getTableLocator()->get('FormInputsFormVariables');
            $inputsVariables = Hash::extract(
                $this->originalForm,
                'form_inputs.{n}.form_inputs_form_variables.{n}'
            );
            foreach ($inputsVariables as $inputsVariable) {
                $InputsVariables->saveOrFail(
                    $InputsVariables->newEntity(
                        [
                            'form_input_id' => $inputsIds[$inputsVariable['form_input_id']],
                            'form_variable_id' => $variableIds[$inputsVariable['form_variable_id']],
                        ]
                    )
                );
            }
        }
    }

    /**
     * Duplication / Versionnage pour FormExtractors
     * @param array $inputsIds
     * @return array
     */
    protected function duplicateOrVersionFormExtractors(array $inputsIds): array
    {
        $extractorIds = [];
        $Extractors = TableRegistry::getTableLocator()->get('FormExtractors');
        foreach ($this->originalForm->get('form_extractors') as $extractor) {
            $Extractors->saveOrFail(
                $newExtractor = $Extractors->newEntity(
                    [
                        'form_id' => $this->newForm->id,
                        'name' => $extractor->get('name'),
                        'description' => $extractor->get('description'),
                        'type' => $extractor->get('type'),
                        'form_input_id' => $inputsIds[$extractor->get('form_input_id')] ?? null,
                        'file_selector' => $extractor->get('file_selector'),
                        'data_format' => $extractor->get('data_format'),
                        'data_path' => $extractor->get('data_path'),
                        'url' => $extractor->get('url'),
                        'username' => $extractor->get('username'),
                        'password' => $extractor->get('password'),
                        'use_proxy' => $extractor->get('use_proxy'),
                        'ssl_verify_peer' => $extractor->get('ssl_verify_peer'),
                        'ssl_verify_peer_name' => $extractor->get('ssl_verify_peer_name'),
                        'ssl_verify_depth' => $extractor->get('ssl_verify_depth'),
                        'ssl_verify_host' => $extractor->get('ssl_verify_host'),
                        'ssl_cafile' => $extractor->get('ssl_cafile'),
                        'namespace' => $extractor->get('namespace'),
                    ]
                )
            );
            $extractorIds[$extractor->get('id')] = $newExtractor->get('id');
        }
        return $extractorIds;
    }

    /**
     * Duplication / Versionnage pour FormExtractorsFormVariables
     * @param array $extractorIds
     * @param array $variableIds
     * @return void
     */
    protected function duplicateOrVersionFormExtractorsFormVariables(
        array $extractorIds,
        array $variableIds
    ): void {
        if ($extractorIds) {
            $ExtractorsVariables = TableRegistry::getTableLocator()->get('FormExtractorsFormVariables');
            $extractorsVariables = Hash::extract(
                $this->originalForm,
                'form_extractors.{n}.form_extractors_form_variables.{n}'
            );
            foreach ($extractorsVariables as $extractorsVariable) {
                $ExtractorsVariables->saveOrFail(
                    $ExtractorsVariables->newEntity(
                        [
                            'form_extractor_id' => $extractorIds[$extractorsVariable['form_extractor_id']],
                            'form_variable_id' => $variableIds[$extractorsVariable['form_variable_id']],
                        ]
                    )
                );
            }
        }
    }

    /**
     * Transforme un formulaire en array pour l'export
     * @param int   $form_id
     * @param array $conditions ex: ['version_number' => 1]
     * @return array
     */
    public function export(int $form_id, array $conditions = []): array
    {
        $identifier = $this->get($form_id)->get('identifier');
        $entities = $this->find()
            ->where(['Forms.identifier' => $identifier])
            ->andWhere($conditions)
            ->disableHydration()
            ->order('Forms.version_number')
            ->contain(
                [
                    'FormTransferHeaders' => function (Query $q) {
                        return $q
                            ->order('FormTransferHeaders.id');
                    },
                    'FormCalculators' => function (Query $q) {
                        return $q
                            ->order('FormCalculators.id')
                            ->contain(['FormCalculatorsFormVariables']);
                    },
                    'FormExtractors' => function (Query $q) {
                        return $q
                            ->order('FormExtractors.id')
                            ->contain(['FormExtractorsFormVariables']);
                    },
                    'FormFieldsets' => function (Query $q) {
                        return $q
                            ->order('FormFieldsets.ord')
                            ->contain(['FormInputs' => 'FormInputsFormVariables']);
                    },
                    'FormVariables' => function (Query $q) {
                        return $q
                            ->order('FormVariables.id');
                    },
                    'FormUnits' => function (Query $q) {
                        return $q
                            ->order('FormUnits.lft')
                            ->contain(
                                [
                                    'FormUnitHeaders',
                                    'FormUnitManagements',
                                    'FormUnitContents',
                                    'FormUnitKeywords' => ['FormUnitKeywordDetails'],
                                ]
                            );
                    },
                ]
            )
            ->all()
            ->toArray();

        $data = [];
        $idMap = [
            'vars' => [],
            'inputs' => [],
            'units' => [],
        ];
        foreach ($entities as $entity) {
            $versionData = $this->extractNoForeignKeys($entity, $this) + [
                'form_vars' => [],
                'form_fieldsets' => [],
                'form_extractors' => [],
                'form_calculators' => [],
                'form_transfer_headers' => [],
                'form_units' => [],
            ];
            $this->exportVars($entity, $idMap, $versionData);
            $this->exportFieldsets($entity, $idMap, $versionData);
            $this->exportExtractors($entity, $idMap, $versionData);
            $this->exportCalculators($entity, $idMap, $versionData);
            $this->exportTransferHeaders($entity, $idMap, $versionData);
            $this->exportUnits($entity, $idMap, $versionData);

            $data[] = $versionData;
        }

        return $data;
    }

    /**
     * Ajoute les fieldsets avec les inputs dans l'export
     * les ids des inputs sont convertis en uuid dans $idMap
     * @param array $entity
     * @param array $idMap
     * @param array $versionData
     * @return void
     */
    private function exportFieldsets(array $entity, array &$idMap, array &$versionData): void
    {
        foreach ($entity['form_fieldsets'] as $formFieldset) {
            $idMap['fieldsets'][$formFieldset['id']] = uuid_create(UUID_TYPE_TIME);
            $fieldsetData = $this->extractNoForeignKeys($formFieldset, $this->FormFieldsets) + [
                'uuid' => $idMap['fieldsets'][$formFieldset['id']],
                'form_inputs' => [],
            ];
            $fieldsetData['ord'] = count($versionData['form_fieldsets']);
            foreach ($formFieldset['form_inputs'] as $formInput) {
                $idMap['inputs'][$formInput['id']] = uuid_create(UUID_TYPE_TIME);
                $inputData = $this->extractNoForeignKeys($formInput, $this->FormInputs) + [
                    'uuid' => $idMap['inputs'][$formInput['id']],
                    'form_inputs_form_vars' => array_map(
                        fn($v) => $idMap['vars'][$v['form_variable_id']],
                        $formInput['form_inputs_form_variables']
                    ),
                ];

                $fieldsetData['form_inputs'][] = $inputData;
            }
            $versionData['form_fieldsets'][] = $fieldsetData;
        }
    }

    /**
     * Ajoute les archive_units dans l'export
     * les ids des form_units sont convertis en uuid dans $idMap
     * @param array $entity
     * @param array $idMap
     * @param array $versionData
     * @return void
     */
    private function exportUnits(array $entity, array &$idMap, array &$versionData): void
    {
        foreach ($entity['form_units'] as $formUnit) {
            $idMap['units'][$formUnit['id']] = uuid_create(UUID_TYPE_TIME);
            $parentId = $formUnit['parent_id']
                ? $idMap['units'][$formUnit['parent_id']]
                : null;
            $inputId = $formUnit['form_input_id']
                ? $idMap['inputs'][$formUnit['form_input_id']]
                : null;
            $varId = $formUnit['presence_condition_id']
                ? $idMap['vars'][$formUnit['presence_condition_id']]
                : null;
            $sectionId = $formUnit['repeatable_fieldset_id']
                ? $idMap['fieldsets'][$formUnit['repeatable_fieldset_id']]
                : null;
            $unitData = $this->extractNoForeignKeys($formUnit, $this->FormUnits) + [
                'uuid' => $idMap['units'][$formUnit['id']],
                'parent_uuid' => $parentId,
                'form_input_uuid' => $inputId,
                'presence_condition_uuid' => $varId,
                'repeatable_fieldset_uuid' => $sectionId,
                'form_unit_headers' => [],
                'form_unit_managements' => [],
                'form_unit_contents' => [],
                'form_unit_keywords' => [],
            ];
            foreach ($formUnit['form_unit_headers'] as $content) {
                $varId = $content['form_variable_id']
                    ? $idMap['vars'][$content['form_variable_id']]
                    : null;
                $unitData['form_unit_headers'][] = $this->extractNoForeignKeys(
                    $content,
                    $this->FormUnits->FormUnitHeaders
                ) + [
                    'form_variable_uuid' => $varId,
                ];
            }
            foreach ($formUnit['form_unit_managements'] as $content) {
                $varId = $content['form_variable_id']
                    ? $idMap['vars'][$content['form_variable_id']]
                    : null;
                $unitData['form_unit_managements'][] = $this->extractNoForeignKeys(
                    $content,
                    $this->FormUnits->FormUnitManagements
                ) + [
                    'form_variable_uuid' => $varId,
                ];
            }
            foreach ($formUnit['form_unit_contents'] as $content) {
                $varId = $content['form_variable_id']
                    ? $idMap['vars'][$content['form_variable_id']]
                    : null;
                $unitData['form_unit_contents'][] = $this->extractNoForeignKeys(
                    $content,
                    $this->FormUnits->FormUnitContents
                ) + [
                    'form_variable_uuid' => $varId,
                ];
            }
            foreach ($formUnit['form_unit_keywords'] as $content) {
                $keyword = $this->extractNoForeignKeys(
                    $content,
                    $this->FormUnits->FormUnitContents
                ) + [
                    'form_unit_keyword_details' => [],
                ];
                $keywordContents = [];
                foreach ($content['form_unit_keyword_details'] as $detail) {
                    $varId = $detail['form_variable_id']
                        ? $idMap['vars'][$detail['form_variable_id']]
                        : null;
                    $keywordContents[] = $this->extractNoForeignKeys(
                        $detail,
                        $this->FormUnits->FormUnitKeywords->FormUnitKeywordDetails
                    ) + [
                        'form_variable_uuid' => $varId,
                    ];
                }
                $keyword['form_unit_keyword_details'] = $keywordContents;
                $unitData['form_unit_keywords'][] = $keyword;
            }

            $versionData['form_units'][] = $unitData;
        }
    }

    /**
     * Ajoute les form_variables dans l'export
     * les ids des form_variables sont convertis en uuid dans $idMap
     * @param array $entity
     * @param array $idMap
     * @param array $versionData
     * @return void
     */
    private function exportVars(array $entity, array &$idMap, array &$versionData): void
    {
        foreach ($entity['form_variables'] as $formVar) {
            $idMap['vars'][$formVar['id']] = uuid_create(UUID_TYPE_TIME);
            $unitData = $this->extractNoForeignKeys($formVar, $this->FormVariables) + [
                'uuid' => $idMap['vars'][$formVar['id']],
            ];
            $versionData['form_vars'][] = $unitData;
        }
    }

    /**
     * Ajoute les form_extractors dans l'export
     * les ids des form_extractors sont convertis en uuid dans $idMap
     * @param array $entity
     * @param array $idMap
     * @param array $versionData
     * @return void
     */
    private function exportExtractors(array $entity, array $idMap, array &$versionData): void
    {
        foreach ($entity['form_extractors'] as $formExtractor) {
            $inputId = $formExtractor['form_input_id']
                ? $idMap['inputs'][$formExtractor['form_input_id']]
                : null;
            $unitData = $this->extractNoForeignKeys($formExtractor, $this->FormExtractors) + [
                'form_input_uuid' => $inputId,
                'form_extractors_form_variables' => array_map(
                    fn($v) => $idMap['vars'][$v['form_variable_id']],
                    $formExtractor['form_extractors_form_variables']
                ),
            ];
            $versionData['form_extractors'][] = $unitData;
        }
    }

    /**
     * Ajoute les form_calculators dans l'export
     * les ids des form_calculators sont convertis en uuid dans $idMap
     * @param array $entity
     * @param array $idMap
     * @param array $versionData
     * @return void
     */
    private function exportCalculators(array $entity, array $idMap, array &$versionData): void
    {
        foreach ($entity['form_calculators'] as $formCalculator) {
            $varId = $formCalculator['form_variable_id']
                ? $idMap['vars'][$formCalculator['form_variable_id']]
                : null;
            $unitData = $this->extractNoForeignKeys($formCalculator, $this->FormCalculators) + [
                'form_variable_uuid' => $varId,
                'form_calculators_form_variables' => array_map(
                    fn($v) => $idMap['vars'][$v['form_variable_id']],
                    $formCalculator['form_calculators_form_variables']
                ),
            ];
            $versionData['form_calculators'][] = $unitData;
        }
    }

    /**
     * Ajoute les form_transfer_headers dans l'export
     * @param array $entity
     * @param array $idMap
     * @param array $versionData
     * @return void
     */
    private function exportTransferHeaders(array $entity, array $idMap, array &$versionData): void
    {
        foreach ($entity['form_transfer_headers'] as $header) {
            $varId = $header['form_variable_id']
                ? $idMap['vars'][$header['form_variable_id']]
                : null;
            $unitData = $this->extractNoForeignKeys($header, $this->FormTransferHeaders) + [
                'form_variable_uuid' => $varId,
            ];
            $versionData['form_transfer_headers'][] = $unitData;
        }
    }

    /**
     * Ajoute les données dans data lorsque il ne sagi pas d'une clé étrangère
     * @param array         $data
     * @param Table|HasMany $model
     * @return array
     */
    private function extractNoForeignKeys(array $data, $model): array
    {
        $blackList = ['lft', 'rght', 'created', 'modified', 'state', 'last_state_update', 'states_history'];
        $jsonFields = ['app_meta', 'disable_expression'];
        $schema = $model->getSchema();
        foreach ($schema->constraints() as $constraintName) {
            $constraint = $schema->getConstraint($constraintName);
            if (in_array($constraint['type'], ['primary', 'foreign'])) {
                $blackList = array_merge($blackList, $constraint['columns']);
            }
        }
        $extracted = [];
        foreach ($data as $key => $value) {
            if (in_array($key, $blackList) || is_array($value)) {
                continue;
            }
            if (
                in_array($key, $jsonFields)
                && $decoded = json_decode($value, true)
            ) {
                $value = $decoded;
            }
            $extracted[$key] = $value;
        }
        return $extracted;
    }

    /**
     * Import d'un formulaire
     * @param array $data
     * @return bool
     */
    public function import(array $data): bool
    {
        $orgEntity = $this->getAssociation('OrgEntities')->find()
            ->where(['OrgEntities.id' => $data['org_entity_id']])
            ->contain('ArchivalAgencies')
            ->first();

        $archivalAgencyId = Hash::get(
            $orgEntity,
            'archival_agency.id',
            Hash::get($orgEntity, 'id') // cas du SA lui même
        );

        $nameExists = (bool)$this->find()
            ->select(['existing' => 1])
            ->innerJoinWith('OrgEntities')
            ->innerJoinWith('OrgEntities.ArchivalAgencies')
            ->where(
                [
                    'ArchivalAgencies.id' => $archivalAgencyId,
                    'Forms.name' => $data['name'],
                    'Forms.identifier !=' => $data['identifier'],
                ]
            )
            ->count();

        if ($nameExists) {
            $data['name'] = $data['name'] . '_' . (new DateTime())->format('Ymd_H-i');
        }

        $entity = $this->newEmptyEntity();
        $this->patchEntity(
            $entity,
            $data,
            [
                'associated' => [
                    'TransferringAgencies' => ['fields' => ['_ids']],
                ],
            ]
        );

        $this->saveOrFail($entity);
        $idMap = [
            'vars' => [],
            'inputs' => [],
            'units' => [],
        ];
        $this->importVars($entity, $idMap, $data);
        $this->importFieldsets($entity, $idMap, $data);
        $this->importExtractors($entity, $idMap, $data);
        $this->importCalculators($entity, $idMap, $data);
        $this->importTransferHeaders($entity, $idMap, $data);
        $this->importFormUnits($entity, $idMap, $data);
        $query = $this->find()
            ->where(
                [
                    'identifier' => $entity->get('identifier'),
                    'version_number <' => $entity->get('version_number'),
                ]
            )
            ->order('version_number');
        foreach ($query as $prevEntity) {
            if ($prevEntity->get('state') === self::S_EDITING) {
                $this->transition($prevEntity, self::T_DEPRECIATE);
                $this->save($prevEntity);
            }
        }
        $this->lastImport = $entity;
        return true;
    }

    /**
     * Ajoute les form_variables
     * @param EntityInterface $entity
     * @param array           $idMap
     * @param array           $data
     * @return void
     */
    private function importVars(EntityInterface $entity, array &$idMap, array $data): void
    {
        $opts = ['associated' => []];
        foreach ($data['form_vars'] as $formVar) {
            $formVar['form_id'] = $entity->id;
            $formVarEntity = $this->FormVariables->newEntity($formVar, $opts);
            $this->FormVariables->saveOrFail($formVarEntity);
            $idMap['vars'][$formVar['uuid']] = $formVarEntity->id;
        }
    }

    /**
     * Ajoute les form_fieldsets et les form_inputs
     * @param EntityInterface $entity
     * @param array           $idMap
     * @param array           $data
     * @return void
     */
    private function importFieldsets(EntityInterface $entity, array &$idMap, array $data): void
    {
        $opts = ['associated' => []];
        foreach ($data['form_fieldsets'] as $fieldset) {
            $fieldset['form_id'] = $entity->id;
            $fieldsetEntity = $this->FormFieldsets->newEntity($fieldset, $opts);
            $this->FormFieldsets->saveOrFail($fieldsetEntity);
            if (isset($fieldset['uuid'])) {
                $idMap['fieldsets'][$fieldset['uuid']] = $fieldsetEntity->id;
            }
            foreach ($fieldset['form_inputs'] as $input) {
                $input['form_id'] = $entity->id;
                $input['form_fieldset_id'] = $fieldsetEntity->id;
                $inputEntity = $this->FormInputs->newEntity($input, $opts);
                $this->FormInputs->saveOrFail($inputEntity);
                $idMap['inputs'][$input['uuid']] = $inputEntity->id;
                foreach ($input['form_inputs_form_vars'] as $linkData) {
                    if (!isset($idMap['vars'][$linkData])) {
                        throw new GenericException(__("Lien incorrect"));
                    }
                    $link = $this->FormInputs->FormInputsFormVariables->newEntity(
                        [
                            'form_input_id' => $inputEntity->id,
                            'form_variable_id' => $idMap['vars'][$linkData],
                        ]
                    );
                    $this->FormInputs->FormInputsFormVariables->saveOrFail($link);
                }
            }
        }
    }

    /**
     * Ajoute les form_extractors
     * @param EntityInterface $entity
     * @param array           $idMap
     * @param array           $data
     * @return void
     */
    private function importExtractors(EntityInterface $entity, array $idMap, array $data): void
    {
        $opts = ['associated' => []];
        foreach ($data['form_extractors'] as $extractor) {
            if (!empty($extractor['form_input_uuid'])) {
                if (!isset($idMap['inputs'][$extractor['form_input_uuid']])) {
                    throw new GenericException(__("Lien incorrect"));
                }
                $extractor['form_input_id'] = $idMap['inputs'][$extractor['form_input_uuid']];
            }
            $extractor['form_id'] = $entity->id;
            $extractorEntity = $this->FormExtractors->newEntity($extractor, $opts);
            $this->FormExtractors->saveOrFail($extractorEntity);
            foreach ($extractor['form_extractors_form_variables'] as $linkData) {
                if (!isset($idMap['vars'][$linkData])) {
                    throw new GenericException(__("Lien incorrect"));
                }
                $link = $this->FormExtractors->FormExtractorsFormVariables->newEntity(
                    [
                        'form_extractor_id' => $extractorEntity->id,
                        'form_variable_id' => $idMap['vars'][$linkData],
                    ]
                );
                $this->FormExtractors->FormExtractorsFormVariables->saveOrFail($link);
            }
        }
    }

    /**
     * Ajoute les form_calculators
     * @param EntityInterface $entity
     * @param array           $idMap
     * @param array           $data
     * @return void
     */
    private function importCalculators(EntityInterface $entity, array $idMap, array $data): void
    {
        $opts = ['associated' => []];
        foreach ($data['form_calculators'] as $calculator) {
            if (!isset($idMap['vars'][$calculator['form_variable_uuid']])) {
                throw new GenericException(__("Lien incorrect"));
            }
            $calculator['form_variable_id'] = $idMap['vars'][$calculator['form_variable_uuid']];
            $calculator['form_id'] = $entity->id;
            $calculatorEntity = $this->FormCalculators->newEntity($calculator, $opts);
            $this->FormCalculators->saveOrFail($calculatorEntity);
            foreach ($calculator['form_calculators_form_variables'] as $linkData) {
                if (!isset($idMap['vars'][$linkData])) {
                    throw new GenericException(__("Lien incorrect"));
                }
                $link = $this->FormCalculators->FormCalculatorsFormVariables->newEntity(
                    [
                        'form_calculator_id' => $calculatorEntity->id,
                        'form_variable_id' => $idMap['vars'][$linkData],
                    ]
                );
                $this->FormCalculators->FormCalculatorsFormVariables->saveOrFail($link);
            }
        }
    }

    /**
     * Ajoute les form_transfer_headers
     * @param EntityInterface $entity
     * @param array           $idMap
     * @param array           $data
     * @return void
     */
    private function importTransferHeaders(EntityInterface $entity, array $idMap, array $data): void
    {
        $opts = ['associated' => []];
        foreach ($data['form_transfer_headers'] as $transfer_header) {
            if (!isset($idMap['vars'][$transfer_header['form_variable_uuid']])) {
                throw new GenericException(__("Lien incorrect"));
            }
            $transfer_header['form_variable_id'] = $idMap['vars'][$transfer_header['form_variable_uuid']];
            $transfer_header['form_id'] = $entity->id;
            $transfer_headerEntity = $this->FormTransferHeaders->newEntity($transfer_header, $opts);
            $this->FormTransferHeaders->saveOrFail($transfer_headerEntity);
        }
    }

    /**
     * Ajoute les form_transfer_headers
     * @param EntityInterface $entity
     * @param array           $idMap
     * @param array           $data
     * @return void
     */
    private function importFormUnits(EntityInterface $entity, array $idMap, array $data): void
    {
        $opts = ['associated' => []];
        foreach ($data['form_units'] as $unit) {
            if (!empty($unit['form_input_uuid'])) {
                if (!isset($idMap['inputs'][$unit['form_input_uuid']])) {
                    throw new GenericException(__("Lien incorrect"));
                }
                $unit['form_input_id'] = $idMap['inputs'][$unit['form_input_uuid']];
            }
            if (!empty($unit['parent_uuid'])) {
                if (!isset($idMap['units'][$unit['parent_uuid']])) {
                    throw new GenericException(__("Lien incorrect"));
                }
                $unit['parent_id'] = $idMap['units'][$unit['parent_uuid']];
            }
            if (!empty($unit['presence_condition_uuid'])) {
                if (!isset($idMap['vars'][$unit['presence_condition_uuid']])) {
                    throw new GenericException(__("Lien incorrect"));
                }
                $unit['presence_condition_id'] = $idMap['vars'][$unit['presence_condition_uuid']];
            }
            if (!empty($unit['repeatable_fieldset_uuid'])) {
                if (!isset($idMap['fieldsets'][$unit['repeatable_fieldset_uuid']])) {
                    throw new GenericException(__("Lien incorrect"));
                }
                $unit['repeatable_fieldset_id'] = $idMap['fieldsets'][$unit['repeatable_fieldset_uuid']];
            }
            $unit['form_id'] = $entity->id;
            $unitEntity = $this->FormUnits->newEntity($unit, $opts);
            $this->FormUnits->saveOrFail($unitEntity);
            $idMap['units'][$unit['uuid']] = $unitEntity->id;
            foreach ($unit['form_unit_headers'] as $header) {
                if (!isset($idMap['vars'][$header['form_variable_uuid']])) {
                    throw new GenericException(__("Lien incorrect"));
                }
                $header['form_unit_id'] = $unitEntity->id;
                $header['form_variable_id'] = $idMap['vars'][$header['form_variable_uuid']];
                $headerEntity = $this->FormUnits->FormUnitHeaders->newEntity($header, $opts);
                $this->FormUnits->FormUnitHeaders->saveOrFail($headerEntity);
            }
            foreach ($unit['form_unit_managements'] as $management) {
                if (!isset($idMap['vars'][$management['form_variable_uuid']])) {
                    throw new GenericException(__("Lien incorrect"));
                }
                $management['form_unit_id'] = $unitEntity->id;
                $management['form_variable_id'] = $idMap['vars'][$management['form_variable_uuid']];
                $managementEntity = $this->FormUnits->FormUnitManagements->newEntity($management, $opts);
                $this->FormUnits->FormUnitManagements->saveOrFail($managementEntity);
            }
            foreach ($unit['form_unit_contents'] as $content) {
                if (!isset($idMap['vars'][$content['form_variable_uuid']])) {
                    throw new GenericException(__("Lien incorrect"));
                }
                $content['form_unit_id'] = $unitEntity->id;
                $content['form_variable_id'] = $idMap['vars'][$content['form_variable_uuid']];
                $contentEntity = $this->FormUnits->FormUnitContents->newEntity($content, $opts);
                $this->FormUnits->FormUnitContents->saveOrFail($contentEntity);
            }
            foreach ($unit['form_unit_keywords'] as $keyword) {
                $keyword['form_unit_id'] = $unitEntity->id;
                $keywordEntity = $this->FormUnits->FormUnitKeywords->newEntity($keyword, $opts);
                $this->FormUnits->FormUnitKeywords->saveOrFail($keywordEntity);
                foreach ($keyword['form_unit_keyword_details'] as $detail) {
                    if (!isset($idMap['vars'][$detail['form_variable_uuid']])) {
                        throw new GenericException(__("Lien incorrect"));
                    }
                    $detail['form_unit_keyword_id'] = $keywordEntity->id;
                    $detail['form_variable_id'] = $idMap['vars'][$detail['form_variable_uuid']];
                    $keywordDetailEntity = $this->FormUnits->FormUnitKeywords->FormUnitKeywordDetails
                        ->newEntity($detail, $opts);
                    $this->FormUnits->FormUnitKeywords->FormUnitKeywordDetails->saveOrFail($keywordDetailEntity);
                }
            }
        }
    }

    /**
     * Donne le jstree à partir d'un threaded d'archive_units
     * @param int $formId
     * @return array
     */
    public function previewJstreeData($formId): array
    {
        $jstreeData = [];
        $query = $this->FormTransferHeaders->find()
            ->where(['FormTransferHeaders.form_id' => $formId])
            ->contain(['FormVariables']);
        $headersData = [];
        foreach ($query as $transferHeader) {
            $name = $transferHeader->get('name');
            if (strpos($name, '_')) {
                [$pname, $name] = explode('_', $name);
                $ref = &$headersData[$pname];
                $ref = &$ref['children'];
                $ref = &$ref[count($ref ?: [])];
            } else {
                $ref = &$headersData[$name];
            }
            $twig = Hash::get($transferHeader, 'form_variable.twig');
            $text = "$name " . $this->getShortName($twig);
            $slug = Text::slug(strtolower($transferHeader->get('name')));
            $ref = [
                'id' => 'jstree-transfer-header-' . $slug,
                'text' => $text,
                'children' => null,
                'icon' => 'far fa-file',
            ];
        }
        foreach ($headersData as $key => $values) {
            $icon = 'far fa-file';
            $state = [];
            if (!empty($values['children'])) {
                $icon = 'fa fa-building text-primary';
                $state = ['opened' => true];
            }
            $jstreeData[] = [
                'id' => $values['id'] ?? 'jstree-transfer-header-' . Text::slug(strtolower($key)),
                'text' => $values['text'] ?? $key,
                'icon' => $icon,
                'state' => $state,
                'children' => $values['children'],
            ];
        }

        $query = $this->FormUnits->find()
            ->where(
                [
                    'FormUnits.form_id' => $formId,
                    'FormUnits.parent_id IS' => null,
                ]
            )
            ->order('FormUnits.lft')
            ->contain(
                [
                    'FormInputs',
                    'FormVariables',
                    'RepeatableFieldsets',
                    'FormUnitHeaders' => ['FormVariables'],
                    'FormUnitManagements' => ['FormVariables'],
                    'FormUnitContents' => ['FormVariables'],
                    'FormUnitKeywords' => function (Query $q) {
                        return $q->order(['FormUnitKeywords.name'])
                            ->contain(
                                [
                                    'FormUnitKeywordDetails' => ['FormVariables'],
                                ]
                            );
                    },
                ]
            );
        $this->recursiveAppendFormUnitsToPreviewJstreeData($query, $jstreeData);

        return $jstreeData;
    }

    /**
     * Fonction récursive d'ajout des form_unit
     * @param Query|array $formUnits
     * @param array       $jstreeData
     * @return void
     */
    private function recursiveAppendFormUnitsToPreviewJstreeData(
        $formUnits,
        array &$jstreeData
    ): void {
        /** @var EntityInterface $formUnit */
        foreach ($formUnits as $formUnit) {
            $text = $formUnit->get('type') . $this->getShortName($formUnit->get('name'));
            if ($formUnit->get('form_input')) {
                $text .= ' input.' . Hash::get($formUnit, 'form_input.name');
            }
            if ($formUnit->get('form_variable')) {
                $text .= ' condition.'
                    . $this->getShortName(
                        Hash::get($formUnit, 'form_variable.twig')
                    );
            }
            if ($formUnit->get('search_expression')) {
                $text .= ' search_exp.'
                    . $this->getShortName(
                        Hash::get($formUnit, 'search_expression')
                    );
            }
            if ($formUnit->get('repeatable_fieldset')) {
                $text .= ' fieldset.' . $this->getShortName(
                    Hash::get($formUnit, 'repeatable_fieldset.legend')
                );
            }
            $data = [
                'id' => 'jstree-form-unit-' . $formUnit->get('id'),
                'text' => $text,
                'icon' => 'fa ' . FormUnitsTable::TYPE_ICONS[$formUnit->get('type')],
                'state' => [],
                'children' => [],
            ];
            $ref = &$data['children'];
            $this->extractVariablesForJstree($formUnit, $ref);
            $query = $this->FormUnits->find()
                ->where(
                    [
                        'FormUnits.form_id' => $formUnit->get('form_id'),
                        'FormUnits.parent_id' => $formUnit->get('id'),
                    ]
                )
                ->order('FormUnits.lft')
                ->contain(
                    [
                        'FormInputs',
                        'FormVariables',
                        'RepeatableFieldsets',
                        'FormUnitHeaders' => ['FormVariables'],
                        'FormUnitManagements' => ['FormVariables'],
                        'FormUnitContents' => ['FormVariables'],
                        'FormUnitKeywords' => function (Query $q) {
                            return $q->order(['FormUnitKeywords.name'])
                                ->contain(
                                    [
                                        'FormUnitKeywordDetails' => ['FormVariables'],
                                    ]
                                );
                        },
                    ]
                );
            $this->recursiveAppendFormUnitsToPreviewJstreeData($query, $ref);
            if (!empty($ref)) {
                $data['state'] = ['opened' => true];
            }
            $jstreeData[] = $data;
        }
    }

    /**
     * Donne une liste de childs pour jstree à partir d'un form_unit
     * @param EntityInterface $formUnit
     * @param array           $ref
     */
    private function extractVariablesForJstree(EntityInterface $formUnit, array &$ref): void
    {
        $genericTables = [
            'form_unit_managements',
            'form_unit_contents',
            'form_unit_headers',
        ];
        $generics = [];
        foreach ($genericTables as $tableName) {
            /** @var EntityInterface $entity */
            foreach ($formUnit->get($tableName) as $entity) {
                $name = $entity->get('name');
                $twig = Hash::get($entity, 'form_variable.twig');
                // Converti Title en Title_0 si on a un Title_1
                if (preg_match('/^(\w+)_(\d+)$/', $name, $m) && isset($generics[$m[1]])) {
                    $prev = $generics[$m[1]];
                    unset($generics[$m[1]]);
                    $generics[$m[1] . '_' . ($m[2] - 1)] = $prev;
                }
                $generics[$name] = $twig;
            }
        }
        $data = [];
        $i = 0;
        foreach ($generics as $name => $twig) {
            if (strpos($name, '_')) {
                [$pname, $name] = explode('_', $name);
                if (!isset($data[$pname])) {
                    $data[$pname] = [
                        'id' => sprintf(
                            'jstree-form-unit-%d-%s-%d',
                            $formUnit->get('id'),
                            Text::slug(strtolower($pname)),
                            $i++
                        ),
                        'text' => $pname,
                        'icon' => $pname === 'OriginatingAgency'
                            ? 'fa fa-building text-primary'
                            : 'far fa-folder',
                        'state' => ['opened' => true],
                        'children' => [],
                    ];
                }
                $count = count($data[$pname]['children'] ?: []);
                $childref = &$data[$pname]['children'][$count];
            } else {
                $childref = &$data[$name];
            }
            $text = $name . ' ' . $this->getShortName($twig);
            $childref = [
                'id' => sprintf(
                    'jstree-form-unit-%d-%s-%d',
                    $formUnit->get('id'),
                    Text::slug(strtolower($name)),
                    $i++
                ),
                'text' => $text,
                'icon' => 'far fa-file',
                'state' => null,
                'children' => null,
            ];
        }
        $ref = array_merge($ref, array_values($data));
        foreach ($formUnit->get('form_unit_keywords') as $formKeyword) {
            $text = 'Keyword ' . $this->getShortName($formKeyword->get('name'));
            $data = [
                'id' => 'jstree-keyword-' . $formKeyword->get('id'),
                'text' => $text,
                'icon' => 'fa fa-key',
                'state' => ['opened' => true],
                'children' => [],
            ];
            /** @var EntityInterface $details */
            foreach ($formKeyword->get('form_unit_keyword_details') ?: [] as $details) {
                $text = $details->get('name')
                    . ' '
                    . $this->getShortName(Hash::get($details, 'form_variable.twig'));
                $data['children'][] = [
                    'id' => sprintf(
                        'jstree-keyword-%d-%s',
                        $formKeyword->get('id'),
                        Text::slug(strtolower($details->get('name')))
                    ),
                    'text' => $text,
                    'icon' => 'far fa-file',
                    'state' => null,
                    'children' => null,
                ];
            }
            $ref[] = $data;
        }
    }

    /**
     * Donne un nom raccourci à $maxCharsPerName chars
     * @param string $name
     * @param int    $maxCharsPerName
     * @return string
     */
    private function getShortName(string $name, int $maxCharsPerName = 50): string
    {
        if (mb_strlen($name) <= $maxCharsPerName) {
            $trimmed = $name;
        } else {
            $trimmed = mb_substr($name, 0, $maxCharsPerName - 1) . '~';
        }

        return sprintf(
            '<span class="child-value%s" title="%s">%s</span>',
            $trimmed !== $name ? ' trimmed' : '',
            h($name),
            h($trimmed)
        );
    }
}
