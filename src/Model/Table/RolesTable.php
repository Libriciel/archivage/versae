<?php

/**
 * Versae\Model\Table\RolesTable
 */

namespace Versae\Model\Table;

use AsalaeCore\Model\Behavior\AclBehavior;
use AsalaeCore\Model\Table\RolesTable as CoreRolesTable;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Behavior\TreeBehavior;

/**
 * Table roles
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin AclBehavior
 * @mixin TimestampBehavior
 * @mixin TreeBehavior
 */
class RolesTable extends CoreRolesTable
{
}
