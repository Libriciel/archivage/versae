<?php

/**
 * Versae\Model\Table\FormUnitContentsTable
 */

namespace Versae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Table\AfterDeleteInterface;
use AsalaeCore\Model\Table\AfterSaveInterface;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Versae\Model\Entity\FormUnit;

/**
 * Table form_unit_contents
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property FormVariablesTable FormVariables
 */
class FormUnitContentsTable extends Table implements AfterDeleteInterface, AfterSaveInterface
{
    public const NAMES_SEDA10 = [
        'Description',
        'DescriptionLevel',
        'FilePlanPosition',
        'Language',
        'OldestDate',
        'LatestDate',
        'AccessRestrictionRule_Code',
        'AccessRestrictionRule_StartDate',
        'CustodialHistory_CustodialHistoryItem',
        'CustodialHistory_CustodialHistoryItem@when',
        'OriginatingAgency_Identification',
        'OriginatingAgency_Name',
    ];
    public const NAMES_SEDA21 = [
        'DescriptionLevel',
        'Title',
        'OriginatingAgencyArchiveUnitIdentifier',
        'TransferringAgencyArchiveUnitIdentifier',
        'Description',
        'CustodialHistory_CustodialHistoryItem',
        'CustodialHistory_CustodialHistoryItem@when',
        'DescriptionLanguage',
        'OriginatingAgency_Identifier',
        'OriginatingAgency_Name',
        'StartDate',
        'EndDate',
        'RelatedObjectReference_IsPartOf_RepositoryArchiveUnitPID',
    ];
    public const NAMES_SEDA21_DOCUMENT = [
        'Title',
        'Description',
        'FilePlanPosition',
        'Type',
        'Language',
        'Status',
        'Version',
    ];
    public const NAMES_SEDA22 = self::NAMES_SEDA21;
    public const NAMES_SEDA22_DOCUMENT = self::NAMES_SEDA21_DOCUMENT;

    /**
     * @var array
     */
    public $names = [];

    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('FormUnits');
        $this->belongsTo('FormVariables');

        $coverageLinks = implode(
            ',',
            [
                'Coverage_{n}_Spatial',
                'Coverage_{n}_Temporal',
                'Coverage_{n}_Juridictional',
            ]
        );
        $eventLinks = implode(
            ',',
            [
                'Event_{n}_EventIdentifier',
                'Event_{n}_EventTypeCode',
                'Event_{n}_EventType',
                'Event_{n}_EventDateTime',
                'Event_{n}_EventDetail',
                'Event_{n}_Outcome',
                'Event_{n}_OutcomeDetail',
                'Event_{n}_OutcomeDetailMessage',
                'Event_{n}_EventDetailData',
            ]
        );
        $this->names = [
            'seda1.0' => [
                'Description' => [
                    'label' => __("Description détaillée"),
                ],
                'DescriptionLevel' => [
                    'label' => __("Niveau de description (defaut : recordgrp)"),
                ],
                'FilePlanPosition' => [
                    'label' => __("Position du contenu"),
                ],
                'Language' => [
                    'label' => __("Langue du contenu (defaut : fra)"),
                ],
                'LatestDate' => [
                    'label' => __("Date la plus récente"),
                ],
                'OldestDate' => [
                    'label' => __("Date la plus ancienne"),
                ],
                'OtherDescriptiveData' => [
                    'label' => __("Autres informations sur l'objet"),
                    'additionnal' => true,
                ],
                'CustodialHistory_CustodialHistoryItem' => [
                    'label' => __("Historique de conservation"),
                ],
                'CustodialHistory_CustodialHistoryItem@when' => [
                    'label' => __("Date d'un événement dans l'historique"),
                ],
                'OriginatingAgency_Identification' => [
                    'label' => __("Identifiant du Service Producteur"),
                ],
                'OriginatingAgency_Name' => [
                    'label' => __("Nom du Service Producteur"),
                ],
                'RelatedObjectReference_{n}_RelatedObjectIdentifier' => [
                    'label' => __(
                        "Identifiant d'un objet ne faisant pas partie du"
                        . " présent paquet d'information"
                    ),
                    'additionnal' => true,
                    'multiple' => true,
                ],
                'RelatedObjectReference_{n}_Relation' => [
                    'label' => __(
                        "Relation entre et l'ArchiveObject et l'objet externe"
                    ),
                    'additionnal' => true,
                    'multiple' => true,
                ],
                'Repository_BusinessType' => [
                    'label' => __(
                        "Nature de l'activité de l'organisation"
                    ),
                    'additionnal' => true,
                ],
                'Repository_Description' => [
                    'label' => __(
                        "Description textuelle de l'organisation"
                    ),
                    'additionnal' => true,
                ],
                'Repository_Identification' => [
                    'label' => __(
                        "Identifiant unique de l'organisation"
                    ),
                    'additionnal' => true,
                ],
                'AccessRestrictionRule_Code' => [
                    'label' => __("Code de restriction d'accès"),
                ],
                'AccessRestrictionRule_StartDate' => [
                    'label' => __("Date de départ du calcul (restriction)"),
                ],
            ],
            'seda2.1' => [
                'DescriptionLevel' => [
                    'label' => __("Niveau de description (defaut : recordgrp)"),
                ],
                'Title' => [
                    'label' => __("Intitulé de l'unité d'archives (vide = Nom de l'élément de l'arborescence)"),
                    'multiple' => true,
                ],
                'FilePlanPosition' => [
                    'label' => __("Position du contenu"),
                    'multiple' => true,
                ],
                'OriginatingAgencyArchiveUnitIdentifier' => [
                    'label' => __("Identifiant donné par le producteur"),
                    'multiple' => true,
                ],
                'TransferringAgencyArchiveUnitIdentifier' => [
                    'label' => __("Identifiant donné par le versant"),
                    'multiple' => true,
                ],
                'Description' => [
                    'label' => __("Description détaillée"),
                    'multiple' => true,
                ],
                'CustodialHistory_CustodialHistoryItem' => [
                    'label' => __("Historique de conservation"),
                ],
                'CustodialHistory_CustodialHistoryItem@when' => [
                    'label' => __("Date d'un événement dans l'historique"),
                ],
                'Type' => [
                    'label' => __("Type (OAIS)"),
                ],
                'DocumentType' => [
                    'label' => __("Type de document au sens diplomatique du terme"),
                    'additionnal' => true,
                ],
                'Language' => [
                    'label' => __("Langue du contenu"),
                    'multiple' => true,
                ],
                'DescriptionLanguage' => [
                    'label' => __("Langue de description"),
                ],
                'Status' => [
                    'label' => __("Etat des documents"),
                ],
                'Version' => [
                    'label' => __("Version du document"),
                ],
                'Tag_{n}' => [
                    'label' => __("Mots-clés"),
                    'additionnal' => true,
                    'multiple' => true,
                ],
                'Coverage_{n}_Spatial' => [
                    'label' => __(
                        "Couverture spatiale ou couverture géographique"
                    ),
                    'additionnal' => true,
                    'multiple' => true,
                    'links' => $coverageLinks,
                ],
                'Coverage_{n}_Temporal' => [
                    'label' => __(
                        "Couverture temporelle"
                    ),
                    'additionnal' => true,
                    'multiple' => true,
                    'links' => $coverageLinks,
                ],
                'Coverage_{n}_Juridictional' => [
                    'label' => __(
                        "Couverture juridictionnelle"
                    ),
                    'additionnal' => true,
                    'multiple' => true,
                    'links' => $coverageLinks,
                ],
                'OriginatingAgency_Identifier' => [
                    'label' => __("Identifiant du service producteur"),
                ],
                'OriginatingAgency_Name' => [
                    'label' => __("Nom du service producteur"),
                ],
                'SubmissionAgency_Identifier' => [
                    'label' => __("Identifiant du service versant responsable du transfert"),
                ],
                'SubmissionAgency_Name' => [
                    'label' => __("Nom du service versant responsable du transfert"),
                ],
                'Source_{n}' => [
                    'label' => __("Référence au papier"),
                    'additionnal' => true,
                    'multiple' => true,
                ],
                'RelatedObjectReference_IsPartOf_RepositoryArchiveUnitPID' => [
                    'label' => __("Référence d'une unité d'archive pour transfert lié"),
                ],
                'CreatedDate' => [
                    'label' => __("Date de création"),
                    'additionnal' => true,
                ],
                'TransactedDate' => [
                    'label' => __("Date de la transaction"),
                    'additionnal' => true,
                ],
                'AcquiredDate' => [
                    'label' => __("Date de numérisation"),
                    'additionnal' => true,
                ],
                'SentDate' => [
                    'label' => __("Date d'envoi"),
                    'additionnal' => true,
                ],
                'ReceivedDate' => [
                    'label' => __("Date de réception"),
                    'additionnal' => true,
                ],
                'RegisteredDate' => [
                    'label' => __("Date d'enregistrement"),
                    'additionnal' => true,
                ],
                'StartDate' => [
                    'label' => __("Date la plus ancienne"),
                ],
                'EndDate' => [
                    'label' => __("Date la plus récente"),
                ],
                'Event_{n}_EventIdentifier' => [
                    'label' => __("Identifiant de l'événement"),
                    'additionnal' => true,
                    'multiple' => true,
                    'links' => $eventLinks,
                ],
                'Event_{n}_EventTypeCode' => [
                    'label' => __("Code du type d'événement"),
                    'additionnal' => true,
                    'multiple' => true,
                    'links' => $eventLinks,
                ],
                'Event_{n}_EventType' => [
                    'label' => __("Type d'événement"),
                    'additionnal' => true,
                    'multiple' => true,
                    'links' => $eventLinks,
                ],
                'Event_{n}_EventDateTime' => [
                    'label' => __("Date et heure de l'événement"),
                    'additionnal' => true,
                    'multiple' => true,
                    'links' => $eventLinks,
                ],
                'Event_{n}_EventDetail' => [
                    'label' => __("Détail sur l'événement"),
                    'additionnal' => true,
                    'multiple' => true,
                    'links' => $eventLinks,
                ],
                'Event_{n}_Outcome' => [
                    'label' => __("Résultat du traitement"),
                    'additionnal' => true,
                    'multiple' => true,
                    'links' => $eventLinks,
                ],
                'Event_{n}_OutcomeDetail' => [
                    'label' => __("Détail sur le résultat du traitement"),
                    'additionnal' => true,
                    'multiple' => true,
                    'links' => $eventLinks,
                ],
                'Event_{n}_OutcomeDetailMessage' => [
                    'label' => __("Message détaillé sur le résultat du traitement"),
                    'additionnal' => true,
                    'multiple' => true,
                    'links' => $eventLinks,
                ],
                'Event_{n}_EventDetailData' => [
                    'label' => __("Message technique détaillant l'erreur"),
                    'additionnal' => true,
                    'multiple' => true,
                    'links' => $eventLinks,
                ],
                'Gps_GpsVersionID' => [
                    'label' => __("Identifiant de la version du GPS"),
                    'additionnal' => true,
                ],
                'Gps_GpsAltitude' => [
                    'label' => __("Altitude de la position GPS"),
                    'additionnal' => true,
                ],
                'Gps_GpsAltitudeRef' => [
                    'label' => __(
                        "0 (niveau de la mer) / 1 (référence au niveau"
                        . " de la mer - valeur négative -)"
                    ),
                    'additionnal' => true,
                ],
                'Gps_GpsLatitude' => [
                    'label' => __("N (Nord) /  S (Sud)"),
                    'additionnal' => true,
                ],
                'Gps_GpsLatitudeRef' => [
                    'label' => 'GpsLatitudeRef',
                    'additionnal' => true,
                ],
                'Gps_GpsLongitude' => [
                    'label' => __("Latitude de la position GPS"),
                    'additionnal' => true,
                ],
                'Gps_GpsLongitudeRef' => [
                    'label' => __("E (Est) / W (Ouest)"),
                    'additionnal' => true,
                ],
                'Gps_GpsDateStamp' => [
                    'label' => __("Heure et Date de la position GPS"),
                    'additionnal' => true,
                ],
            ],
        ];

        $agentMetadata = [
            'FirstName',
            'BirthName',
            'FullName',
            'GivenName',
            'Gender',
            'BirthDate',
            'BirthPlace_Geogname',
            'BirthPlace_Address',
            'BirthPlace_PostalCode',
            'BirthPlace_City',
            'BirthPlace_Region',
            'BirthPlace_Country',
            'DeathDate',
            'DeathPlace_Geogname',
            'DeathPlace_Address',
            'DeathPlace_PostalCode',
            'DeathPlace_City',
            'DeathPlace_Region',
            'DeathPlace_Country',
            'Nationality',
            'Corpname',
            'Identifier',
            'Function',
            'Activity',
            'Position',
            'Role',
            'Mandate',
        ];
        $links = [];
        $agentFields = [
            'AuthorizedAgent',
            'Writer',
            'Addressee',
            'Recipient',
            'Transmitter',
            'Sender',
        ];
        foreach ($agentFields as $field) {
            $link = &$links[$field];
            foreach ($agentMetadata as $subfield) {
                $name = "{$field}_{n}_$subfield";
                $link = $link ? $link . ',' . $name : $name;
                $this->names['seda2.1'][$name] = [
                    'label' => $name,
                    'additionnal' => true,
                    'multiple' => true,
                    'links' => &$link,
                ];
            }
        }

        $signatureLinks = '';
        foreach ($agentMetadata as $subfield) {
            $name = "Signature_{n}_Signer_$subfield";
            $signatureLinks = $signatureLinks ? $signatureLinks . ',' . $name : $name;
            $this->names['seda2.1'][$name] = [
                'label' => $name,
                'additionnal' => true,
                'multiple' => true,
                'links' => &$signatureLinks,
            ];
        }
        $name = "Signature_{n}_Signer_SigningTime";
        $signatureLinks = $signatureLinks . ',' . $name;
        $this->names['seda2.1'][$name] = [
            'label' => $name,
            'additionnal' => true,
            'multiple' => true,
            'links' => &$signatureLinks,
        ];
        foreach ($agentMetadata as $subfield) {
            $name = "Signature_{n}_Validator_$subfield";
            $signatureLinks = $signatureLinks . ',' . $name;
            $this->names['seda2.1'][$name] = [
                'label' => $name,
                'additionnal' => true,
                'multiple' => true,
                'links' => &$signatureLinks,
            ];
        }
        $name = "Signature_{n}_Validator_ValidationTime";
        $signatureLinks = $signatureLinks . ',' . $name;
        $this->names['seda2.1'][$name] = [
            'label' => $name,
            'additionnal' => true,
            'multiple' => true,
            'links' => &$signatureLinks,
        ];

        $signatureFields = [
            'Masterdata',
            'ReferencedObject_SignedObjectId',
            'ReferencedObject_SignedObjectDigest',
            'ReferencedObject_SignedObjectDigest@algorithm',
        ];
        foreach ($signatureFields as $field) {
            $name = "Signature_{n}_$field";
            $signatureLinks = $signatureLinks . ',' . $name;
            $this->names['seda2.1'][$name] = [
                'label' => $name,
                'additionnal' => true,
                'multiple' => true,
                'links' => &$signatureLinks,
            ];
        }

        $this->names['seda2.2'] = $this->names['seda2.1'];

        $agentLinks = '';
        foreach ($agentMetadata as $subfield) {
            $name = "Agent_{n}_$subfield";
            $agentLinks = $agentLinks ? $agentLinks . ',' . $name : $name;
            $this->names['seda2.2'][$name] = [
                'label' => $name,
                'additionnal' => true,
                'multiple' => true,
                'links' => &$agentLinks,
            ];
        }

        $eventLinks = $eventLinks .
            'Event_{n}_LinkingAgentIdentifier_LinkingAgentIdentifierType,' .
            'Event_{n}_LinkingAgentIdentifier_LinkingAgentIdentifierValue,' .
            'Event_{n}_LinkingAgentIdentifier_LinkingAgentRole';

        $this->names['seda2.2'] += [
            'DateLitteral' => [
                'label' => __("Champ date en texte libre"),
                'additionnal' => true,
            ],
            'OriginatingSystemIdReplyTo' => [
                'label' => __("Référence du message auquel on répond"),
                'additionnal' => true,
            ],
            'TextContent' => [
                'label' => __("Contenu du message électronique"),
                'additionnal' => true,
            ],
            'Event_{n}_LinkingAgentIdentifier_LinkingAgentIdentifierType' => [
                'label' => __("Identifiant d'un agent répertorié dans des évènements"),
                'additionnal' => true,
                'multiple' => true,
                'links' => $eventLinks,
            ],
            'Event_{n}_LinkingAgentIdentifier_LinkingAgentIdentifierValue' => [
                'label' => __("Mention d'un agent répertorié dans des évènements"),
                'additionnal' => true,
                'multiple' => true,
                'links' => $eventLinks,
            ],
            'Event_{n}_LinkingAgentIdentifier_LinkingAgentRole' => [
                'label' => __("Fonction d'un agent répertorié dans des évènements"),
                'additionnal' => true,
                'multiple' => true,
                'links' => $eventLinks,
            ],
        ];
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Renvoi un tableau de résultats (inclus les variables manquantes)
     * @param FormUnit $formUnit
     * @return array
     */
    public function formContentData(FormUnit $formUnit): array
    {
        $version = Hash::get($formUnit, 'form.seda_version');
        if ($formUnit->get('type') === 'document') {
            return [];
        }
        $names = [];
        foreach ($this->names[$version] as $name => $params) {
            if (($params['multiple'] ?? false) && !($params['additionnal'] ?? false)) {
                $names[$name . '_{n}'] = $params; // alias les names de champs multiple avec _{n}
            } else {
                $names[$name] = $params;
            }
        }
        $data = [];
        $entities = [];
        $indexes = [];
        /** @var EntityInterface $formUnitHeader */
        foreach (Hash::get($formUnit, 'form_unit_contents') ?: [] as $formUnitContent) {
            // en base, le nom est genre: Event_0 mais le nom est Event_{n} dans le schema
            $name = preg_replace('/_\d+/', '_{n}', $formUnitContent->get('name'));
            if (isset($names[$name . '_{n}'])) {
                $name = $name . '_{n}';
            } elseif (!isset($names[$name])) {
                continue; // schema modifié ?
            }
            $label = $names[$name]['label'];
            if ($names[$name]['multiple'] ?? false) {
                if (!isset($indexes[$name])) {
                    $indexes[$name] = 0;
                }
                $indexes[$name]++;
                if ($indexes[$name] > 1 || isset($names[$name]['additionnal'])) {
                    $label = $formUnitContent->get('name');
                }
            }
            $formUnitContent->set('label', preg_replace('/_\{n}/', '', $label));
            $formUnitContent->set('required', $names[$name]['required'] ?? false);
            $formUnitContent->set('parent', $names[$name]['parent'] ?? false);
            $formUnitContent->set('additionnal', $names[$name]['additionnal'] ?? false);
            $formUnitContent->set('multiple', $names[$name]['multiple'] ?? false);
            $formUnitContent->set('links', $names[$name]['links'] ?? false);
            $formUnitContent->set('genericLabel', $names[$name]['label'] ?? false);
            $formUnitContent->set('genericName', $name);
            $entities[$name][] = $formUnitContent;
        }
        foreach ($names as $name => $params) {
            if (
                $formUnit->get('parent_id')
                && $name === 'RelatedObjectReference_IsPartOf_RepositoryArchiveUnitPID'
            ) {
                continue;
            }
            if (isset($entities[$name])) {
                $data = array_merge($data, $entities[$name]);
            } else {
                $data[] = [
                    'id' => null,
                    'name' => $name,
                    'label' => $params['label'],
                    'required' => $params['required'] ?? false,
                    'additionnal' => $params['additionnal'] ?? false,
                    'multiple' => $params['multiple'] ?? false,
                    'links' => $params['links'] ?? false,
                    'genericName' => $name,
                    'genericLabel' => $params['label'],
                ];
            }
        }
        return $data;
    }

    /**
     * The Model.afterDelete event is fired after an entity is deleted.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterDelete(Event $event, Entity $entity, ArrayObject $options): void
    {
        $this->FormVariables->deleteAll(['id' => $entity->get('form_variable_id')]);
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options): void
    {
        if ($entity->isNew() || $entity->isDirty()) {
            $loc = TableRegistry::getTableLocator();
            $subquery = $loc->get('FormUnits')
                ->find()
                ->select(['form_id'])
                ->where(
                    ['FormUnits.id' => $entity->get('form_unit_id')]
                );
            $loc->get('Forms')
                ->updateAll(
                    ['tested' => false],
                    ['Forms.id' => $subquery]
                );
        }
    }
}
