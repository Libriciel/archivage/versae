<?php

/**
 * Versae\Model\Table\FormInputsTable
 */

namespace Versae\Model\Table;

use ArrayObject;
use AsalaeCore\DataType\JsonString;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\AfterSaveInterface;
use AsalaeCore\ORM\Marshaller;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Entity;
use Cake\ORM\Marshaller as CakeMarshaller;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Twig\Error\SyntaxError;
use Versae\Model\Entity\FormInput;
use Versae\Utility\Twig;

/**
 * Table form_inputs
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin OptionsBehavior
 * @property FormsTable|BelongsTo Forms
 * @property FormExtractorsTable|HasMany FormExtractors
 * @property FormFieldsetsTable|HasMany FormFieldsets
 * @property FormInputsFormVariablesTable|HasMany FormInputsFormVariables
 */
class FormInputsTable extends Table implements AfterSaveInterface
{
    /**
     * Types possibles
     */
    public const TYPE_ARCHIVE_FILE = 'archive_file';       // fichier compressé
    public const TYPE_CHECKBOX = 'checkbox';               // case à cocher
    public const TYPE_DATE = 'date';                       // date
    public const TYPE_DATETIME = 'datetime';               // date et heure
    public const TYPE_EMAIL = 'email';                     // email
    public const TYPE_FILE = 'file';                       // fichier
    public const TYPE_MULTI_CHECKBOX = 'multi_checkbox';   // case à cocher
    public const TYPE_NUMBER = 'number';                   // champ numérique
    public const TYPE_PARAGRAPH = 'paragraph';             // paragrapge
    public const TYPE_RADIO = 'radio';                     // bouton radio
    public const TYPE_SELECT = 'select';                   // sélection entre plusieurs options
    public const TYPE_TEXT = 'text';                       // zone de texte simple
    public const TYPE_TEXTAREA = 'textarea';               // zone de texte sur plusieurs lignes

    public const DATE_FORMAT_ISO = 'iso';
    public const DATE_FORMAT_FR = 'fr';
    public const DATE_FORMAT_FR_LONG = 'fr_long';

    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'type' => [
                    self::TYPE_ARCHIVE_FILE,
                    self::TYPE_CHECKBOX,
                    self::TYPE_DATE,
                    self::TYPE_DATETIME,
                    self::TYPE_EMAIL,
                    self::TYPE_FILE,
                    self::TYPE_MULTI_CHECKBOX,
                    self::TYPE_NUMBER,
                    self::TYPE_PARAGRAPH,
                    self::TYPE_RADIO,
                    self::TYPE_SELECT,
                    self::TYPE_TEXT,
                    self::TYPE_TEXTAREA,
                ],
                'date_format' => [
                    self::DATE_FORMAT_ISO,
                    self::DATE_FORMAT_FR,
                    self::DATE_FORMAT_FR_LONG,
                ],
            ]
        );

        $this->belongsTo('Forms');
        $this->belongsTo('FormFieldsets');
        $this->belongsToMany('FormVariables');
        $this->hasMany('FormInputsFormVariables');
        $this->hasMany('FormExtractors');
        $this->hasMany('DepositValues');
        $this->hasMany('FormUnits');
        $this->hasMany('FormInputsFormVariables');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name')
            ->add(
                'name',
                'validFormat',
                [
                    'rule' => ['custom', '/^[a-zA-Z][a-zA-Z0-9_]*$/'],
                    'message' => __("Autorisés: lettres, chiffres et underscores `_`, seulement une lettre en premier"),
                ]
            )
            ->add(
                'name',
                [
                    'unique' => [
                        'rule' => ['validateUnique', ['scope' => 'form_id']],
                        'provider' => 'table',
                        'message' => __("Cet identifiant est déjà utilisé"),
                    ],
                ]
            )
            ->add(
                'name',
                [
                    'not_reserved' => [
                        'rule' => fn($value) => !in_array($value, Twig::TOKEN_PROG_LIST),
                        'message' => __("Ce nom est réservé"),
                    ]
                ]
            );

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->requirePresence('type', 'create')
            ->notEmptyString('type');

        $validator
            ->integer('ord')
            ->requirePresence('ord', 'create')
            ->notEmptyString('ord');

        $validator
            ->scalar('label')
            ->maxLength('label', 255)
            ->requirePresence('label', 'create')
            ->notEmptyString('label');

        $validator
            ->scalar('default_value')
            ->maxLength('default_value', 255)
            ->allowEmptyString('default_value');

        $validator
            ->boolean('required')
            ->requirePresence('required', 'create')
            ->notEmptyString('required');

        $validator
            ->boolean('readonly')
            ->requirePresence('readonly', 'create')
            ->notEmptyString('readonly');

        $validator->add(
            'readonly',
            'custom',
            [
                'rule' => function ($value, $context) {
                    return !($context['data']['required'] && $context['data']['readonly']);
                },
                'message' => __("Un champ ne peut être obligatoire et en lecture seule")
            ]
        );

        $validator
            ->allowEmptyString('disable_expression');

        $validator
            ->scalar('pattern')
            ->allowEmptyString('pattern');

        $validator
            ->scalar('placeholder')
            ->allowEmptyString('placeholder');

        $validator
            ->scalar('help')
            ->allowEmptyString('help');

        $validator
            ->allowEmptyString('app_meta');

        return $validator;
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws SyntaxError
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options): void
    {
        $original = $entity->getOriginal('name');
        $name = $entity->get('name');
        if ($entity->isNew() === false && $original !== $name) {
            $FormVariables = $this->getAssociation('FormVariables');
            $query = $FormVariables->find()
                ->innerJoin(
                    ['FormInputsFormVariables' => 'form_inputs_form_variables'],
                    [
                        'form_variable_id' => new IdentifierExpression(
                            'FormVariables.id'
                        )
                    ]
                )
                ->where(['form_input_id' => $entity->id]);
            /** @var EntityInterface $formVariable */
            foreach ($query as $formVariable) {
                $this->afterSaveFormVariable(
                    $formVariable,
                    $original,
                    $name
                );
            }
        }
        if ($entity->isNew() || $entity->isDirty()) {
            TableRegistry::getTableLocator()->get('Forms')
                ->updateAll(
                    ['tested' => false],
                    ['id' => $entity->get('form_id')]
                );
        }
    }

    /**
     * Donne les options pour la liste des inputs à insérer dans le twig
     * @param int      $form_id
     * @param int|null $var_id
     * @return array
     */
    public function listOptions(int $form_id, int $var_id = null): array
    {
        $query = $this->find()
            ->select(
                [
                    'FormInputs.id',
                    'FormInputs.name',
                    'FormInputs.label',
                    'FormInputs.type',
                    'FormInputs.multiple',
                    'FormInputs.form_fieldset_id',
                    'legend' => 'FormFieldsets.legend',
                    'fieldset_ord' => 'FormFieldsets.ord',
                    'repeatable' => 'FormFieldsets.repeatable',
                ]
            )
            ->where(
                [
                    'FormFieldsets.form_id' => $form_id,
                    'FormInputs.type !=' => FormInputsTable::TYPE_PARAGRAPH,
                ]
            )
            ->order(['FormFieldsets.ord', 'FormInputs.ord'])
            ->contain(['FormFieldsets'])
            ->toArray();
        $options = [];
        foreach ($query as $formInput) {
            $legend = $formInput->get('legend')
                ?: __("Section sans titre #{0}", $formInput->get('fieldset_ord') + 1);
            if (Hash::get($formInput, 'repeatable')) {
                $legend = hex2bin('ef86b3') . $legend;
            }
            if (!isset($options[$legend])) {
                $options[$legend] = [];
            }
            $label = $formInput->get('label');
            $label = $formInput->get('name') . ($label ? ' - ' . $label : '');
            $options[$legend][] = [
                'value' => 'input.' . $formInput->get('name'),
                'text' => $label,
                'class' => $formInput->get('type'),
                'data-multiple' => (bool)$formInput->get('multiple'),
                'data-repeatable' => (bool)$formInput->get('repeatable'),
                'data-fieldsets' => $formInput->get('form_fieldset_id'),
            ];
        }

        $query = $this->Forms->FormCalculators->find()
            ->where(
                [
                    'FormCalculators.form_id' => $form_id,
                ]
            ) // attention pour edit, n'autoriser que les var ord <
            ->contain(
                [
                    'FormVariables' => [
                        'FormInputs' => ['FormFieldsets'],
                        'FormCalculatorsFormVariables',
                    ],
                ]
            )
            ->order(['FormCalculators.name']);
        if ($var_id) {
            $query->andWhere(['FormCalculators.id <' => $var_id]);
        }
        $group = __("Variables");
        $options[$group] = [];
        foreach ($query as $variable) {
            $label = $variable->get('name');
            $nestedVariables = $this->extractRecursiveVariables($variable);
            $repeatable = array_filter(
                array_merge(
                    Hash::extract(
                        $variable,
                        'form_variable.form_inputs.{n}.form_fieldset.repeatable'
                    ),
                    Hash::extract(
                        $nestedVariables,
                        '{n}.form_inputs.{n}.form_fieldset.repeatable'
                    )
                )
            );
            $fieldsetIds = array_merge(
                Hash::extract(
                    $variable,
                    'form_variable.form_inputs.{n}.form_fieldset_id'
                ),
                Hash::extract(
                    $nestedVariables,
                    '{n}.form_inputs.{n}.form_fieldset_id'
                )
            );
            sort($fieldsetIds);
            if ($repeatable) {
                $label = hex2bin('ef86b3') . ' ' . $label;
            }
            $options[$group][] = [
                'value' => 'var.' . $variable->get('name'),
                'text' => $label,
                'data-multiple' => (bool)$variable->get('multiple'),
                'data-repeatable' => (bool)$repeatable,
                'data-fieldsets' => implode(',', array_unique($fieldsetIds)),
            ];
        }

        $query = $this->FormExtractors->find()
            ->where(['FormExtractors.form_id' => $form_id])
            ->contain(['FormInputs' => ['FormFieldsets']])
            ->order(['FormExtractors.name']);
        $group = __("Extracteurs");
        $options[$group] = [];
        foreach ($query as $extractor) {
            $label = $extractor->get('name');
            $repeatable = (bool)Hash::get($extractor, 'form_input.form_fieldset.repeatable');
            if ($repeatable) {
                $label = hex2bin('ef86b3') . ' ' . $label;
            }
            $options[$group][] = [
                'value' => 'extract.' . $extractor->get('name'),
                'text' => $label,
                'data-multiple' => (bool)$extractor->get('multiple'),
                'data-repeatable' => $repeatable,
                'data-fieldsets' => Hash::get($extractor, 'form_input.form_fieldset_id'),
            ];
        }

        $query = $this->FormFieldsets->find()
            ->where(
                [
                    'FormFieldsets.form_id' => $form_id,
                    'FormFieldsets.repeatable IS' => true,
                ]
            )
            ->order(['FormFieldsets.ord']);
        $group = __("Métadonnées de sections répétables ({0})", hex2bin('ef86b3'));
        foreach ($query as $fieldset) {
            $label = __("Nombre de sections: {0}", $fieldset->get('legend'));
            $options[$group][] = [
                'value' => sprintf('meta.repeatable_counts.section_%d', $fieldset->get('ord')),
                'text' => $label,
                'data-multiple' => false,
                'data-repeatable' => false,
                'data-fieldsets' => $fieldset->id,
            ];
        }

        $options[__("Date du jour")] = [
            [
                'value' => '"now"|date("Y-m-d")',
                'text' => __("Format ISO (aaaa-mm-jj)"),
                'data-multiple' => false,
            ],
            [
                'value' => '"now"|format_datetime("short", "none", locale="fr")',
                'text' => __("Format FR court (jj/mm/aaaa)"),
                'data-multiple' => false,
            ],
            [
                'value' => '"now"|format_datetime("full", "none", locale="fr")',
                'text' => __("Format FR long (l jj MM aaaa)"),
                'data-multiple' => false,
            ],
            [
                'value' => '"now"|date("d")',
                'text' => __("Jour (jj)"),
                'data-multiple' => false,
            ],
            [
                'value' => '"now"|date("m")',
                'text' => __("Mois (mm)"),
                'data-multiple' => false,
            ],
            [
                'value' => '"now"|date("y")',
                'text' => __("Année (aa)"),
                'data-multiple' => false,
            ],
            [
                'value' => '"now"|date("Y")',
                'text' => __("Année (aaaa)"),
                'data-multiple' => false,
            ],
        ];

        $options[__("Service versant du versement")] = [
            [
                'value' => 'meta.transferring_agency.identifier',
                'text' => __("Identifiant du service versant du versement"),
                'data-multiple' => false,
            ],
            [
                'value' => 'meta.transferring_agency.name',
                'text' => __("Nom du service versant"),
                'data-multiple' => false,
            ],
        ];
        $options[__("Service producteur du versement")] = [
            [
                'value' => 'meta.originating_agency.identifier',
                'text' => __("Identifiant du service producteur du versement"),
                'data-multiple' => false,
            ],
            [
                'value' => 'meta.originating_agency.name',
                'text' => __("Nom du service producteur du versement"),
                'data-multiple' => false,
            ],
        ];
        $options[__("Données de l'utilisateur")] = [
            [
                'value' => 'meta.user.username',
                'text' => __("Identifiant de l'utilisateur"),
                'data-multiple' => false,
            ],
            [
                'value' => 'meta.user.name',
                'text' => __("Nom de l'utilisateur"),
                'data-multiple' => false,
            ],
            [
                'value' => 'meta.user.email',
                'text' => __("E-mail de l'utilisateur"),
                'data-multiple' => false,
            ],
            [
                'value' => 'meta.org_entity.identifier',
                'text' => __("Identifiant de l'entité de l'utilisateur"),
                'data-multiple' => false,
            ],
            [
                'value' => 'meta.org_entity.name',
                'text' => __("Nom de l'entité de l'utilisateur"),
                'data-multiple' => false,
            ],
        ];

        return array_filter($options);
    }

    /**
     * Réduit la liste d'$options issue de `$this->listOptions()` à la valeur du $multiple
     * @param array $options
     * @param bool  $multiple
     * @param bool  $files
     * @return array
     */
    public function filterMultiple(
        array $options,
        bool $multiple = true,
        bool $files = true
    ): array {
        foreach (array_keys($options) as $optgroup) {
            $options[$optgroup] = array_filter(
                $options[$optgroup],
                fn($opt) => $opt['data-multiple'] === $multiple
            );
            if (!$files) {
                foreach ($options[$optgroup] as $key => $value) {
                    if (Hash::get($value, 'class') === 'file') {
                        unset($options[$optgroup][$key]);
                    }
                }
            }
            if (empty($options[$optgroup])) {
                unset($options[$optgroup]);
            }
        }
        return $options;
    }

    /**
     * Allows listeners to modify the rules checker by adding more rules.
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->addDelete(
            function (EntityInterface $entity) {
                return $entity->get('deletable');
            }
        );
        return parent::buildRules($rules);
    }

    /**
     * AfterSave Individuel pour chaque FormVariable de l'input
     * @param EntityInterface $formVariable
     * @param string          $original
     * @param string          $name
     * @return void
     * @throws SyntaxError
     */
    protected function afterSaveFormVariable(
        EntityInterface $formVariable,
        string $original,
        string $name
    ): void {
        $formVariable->set(
            'twig',
            Twig::replaceVar(
                $formVariable->get('twig'),
                'input.' . $original,
                'input.' . $name
            )
        );
        /** @var JsonString $jsonObject */
        $jsonObject = $formVariable->get('app_meta');
        foreach (Hash::flatten((array)$jsonObject) as $key => $value) {
            if ($value === 'input.' . $original) {
                $ref = &$jsonObject;
                foreach (explode('.', $key) as $skey) {
                    $ref = &$ref[$skey];
                }
                $ref = 'input.' . $name;
                $formVariable->setDirty('app_meta');
            }
        }
        $FormVariables = $this->getAssociation('FormVariables');
        $FormVariables->save($formVariable);
    }

    /**
     * Get the object used to marshal/convert array data into objects.
     *
     * Override this method if you want a table object to use custom
     * marshalling logic.
     *
     * @return Marshaller
     * @see Marshaller
     */
    public function marshaller(): CakeMarshaller
    {
        return new Marshaller($this);
    }

    /**
     * Donne la liste complète des variables (avec leurs inputs et fieldset) lié
     * à une variable
     * @param EntityInterface $calculator
     * @param array           $variables
     * @return array
     */
    private function extractRecursiveVariables(EntityInterface $calculator, array $variables = []): array
    {
        $nestedCalculatorIds = Hash::extract(
            $calculator,
            'form_variable.form_calculators_form_variables.{n}.form_calculator_id'
        );
        // on refait le contain ici car form_inputs est vide sinon (conflits d'alias ?)
        if ($nestedCalculatorIds) {
            $nestedCalculators = $this->Forms->FormCalculators->find()
                ->where(['FormCalculators.id IN' => $nestedCalculatorIds])
                ->contain(
                    [
                        'FormVariables' => ['FormInputs' => ['FormFieldsets']],
                        'FormCalculatorsFormVariables',
                    ]
                )
                ->toArray();
            foreach ($nestedCalculators as $nestedCalculator) {
                $nestedVariable = $nestedCalculator->get('form_variable');
                if (isset($variables[$nestedVariable->get('id')])) {
                    continue;
                }
                $variables[$nestedVariable->get('id')] = $nestedVariable;
                $variables = $this->extractRecursiveVariables($nestedCalculator, $variables);
            }
        }
        return $variables;
    }

    /**
     * Patch entity mais avec les champs json en plus
     * @param int       $form_fieldset_id
     * @param int       $ord
     * @param FormInput $inputEntity
     * @param array     $input
     * @return EntityInterface
     */
    public function patchEntityWithDecodedInput(
        int $form_fieldset_id,
        int $ord,
        FormInput $inputEntity,
        array $input,
    ): EntityInterface {
        $data = [];
        foreach ($inputEntity->jsonVirtualFields['disable_expression'] as $column) {
            $data[$column] = $input[$column] ?? null;
        }
        $schema = $this->getSchema();
        $fields = [];
        foreach ($schema->columns() as $column) {
            $fields[$column] = $schema->getColumnType($column);
        }
        foreach ($inputEntity->jsonVirtualFields['app_meta'] as $column) {
            $fields[$column] = $inputEntity->appMetaFieldTypes[$column] ?? 'string';
        }
        foreach ($fields as $column => $type) {
            switch ($type) {
                case 'boolean':
                    $data[$column] = $input[$column] ?? false;
                    break;
                case 'array':
                    if ($column === 'app_meta' && !isset($input[$column])) {
                        break; // évite l'écrasement des valeurs app_meta
                    }
                    $data[$column] = $input[$column] ?? [];
                    break;
                case 'string':
                case 'text':
                case 'integer':
                default:
                    $data[$column] = $input[$column] ?? null;
            }
        }
        unset($data['disable_expression'], $data['app_meta']); // evite un conflit
        return $this->patchEntity(
            $inputEntity,
            [
                'form_id' => $inputEntity->get('form_id'),
                'form_fieldset_id' => $form_fieldset_id,
                'ord' => $ord,
            ] + $data,
        );
    }
}
