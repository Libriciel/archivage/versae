<?php

/**
 * Versae\Model\Table\FormUnitKeywordsTable
 */

namespace Versae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Table\AfterSaveInterface;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Table form_unit_keywords
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property FormUnitKeywordDetailsTable FormUnitKeywordDetails
 */
class FormUnitKeywordsTable extends Table implements AfterSaveInterface
{

    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('FormUnits');
        $this->hasMany('FormUnitKeywordDetails');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name')
            ->add(
                'name',
                [
                    'unique' => [
                        'rule' => ['validateUnique', ['scope' => 'form_unit_id']],
                        'provider' => 'table',
                        'message' => __("Ce nom est déjà utilisé"),
                    ],
                ]
            );

        $validator
            ->scalar('multiple_with')
            ->maxLength('multiple_with', 255)
            ->allowEmptyString('multiple_with');

        return $validator;
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options): void
    {
        if ($entity->isNew() || $entity->isDirty()) {
            $loc = TableRegistry::getTableLocator();
            $subquery = $loc->get('FormUnits')
                ->find()
                ->select(['form_id'])
                ->where(
                    ['FormUnits.id' => $entity->get('form_unit_id')]
                );
            $loc->get('Forms')
                ->updateAll(
                    ['tested' => false],
                    ['Forms.id' => $subquery]
                );
        }
    }
}
