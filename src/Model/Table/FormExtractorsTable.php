<?php

/**
 * Versae\Model\Table\FormExtractorsTable
 */

namespace Versae\Model\Table;

use ArrayObject;
use AsalaeCore\DataType\JsonString;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Model\Table\AfterSaveInterface;
use AsalaeCore\ORM\Marshaller;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Entity;
use Cake\ORM\Marshaller as CakeMarshaller;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Twig\Error\SyntaxError;
use Versae\Utility\Twig;

/**
 * Table form_extractors
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin OptionsBehavior
 * @property FormExtractorsFormVariablesTable|HasMany FormExtractorsFormVariables
 */
class FormExtractorsTable extends Table implements AfterSaveInterface
{
    public const TYPE_FILE = 'file';
    public const TYPE_ARCHIVE_FILE = 'archivefile';
    public const TYPE_WEBSERVICE = 'webservice';

    public const DATA_FORMAT_JSON = 'json';
    public const DATA_FORMAT_XML = 'xml';
    public const DATA_FORMAT_CSV = 'csv';

    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'type' => [
                    self::TYPE_FILE,
                    self::TYPE_ARCHIVE_FILE,
                    self::TYPE_WEBSERVICE,
                ],
                'data_format' => [
                    self::DATA_FORMAT_JSON,
                    self::DATA_FORMAT_XML,
                    self::DATA_FORMAT_CSV,
                ],
            ]
        );

        $this->belongsTo('Forms');
        $this->belongsTo('FormInputs');
        $this->hasMany('FormExtractorsFormVariables');
        $this->belongsToMany('FormVariables');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name')
            ->add(
                'name',
                'validFormat',
                [
                    'rule' => ['custom', '/^[a-zA-Z][a-zA-Z0-9_]*$/'],
                    'message' => __("Autorisés: lettres, chiffres et underscores `_`, seulement une lettre en premier"),
                ]
            )
            ->add(
                'name',
                [
                    'unique' => [
                        'rule' => ['validateUnique', ['scope' => 'form_id']],
                        'provider' => 'table',
                        'message' => __("Cet identifiant est déjà utilisé"),
                    ],
                ]
            )
            ->add(
                'name',
                [
                    'not_reserved' => [
                        'rule' => fn($value) => !in_array($value, Twig::TOKEN_PROG_LIST),
                        'message' => __("Ce nom est réservé"),
                    ]
                ]
            );

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->requirePresence('type', 'create')
            ->notEmptyString('type');

        $validator
            ->scalar('file_selector')
            ->maxLength('file_selector', 2048)
            ->allowEmptyString('file_selector');

        $validator
            ->scalar('data_format')
            ->maxLength('data_format', 255)
            ->requirePresence('data_format', 'create')
            ->notEmptyString('data_format');

        $validator
            ->scalar('data_path')
            ->maxLength('data_path', 2048)
            ->requirePresence('data_path', 'create')
            ->notEmptyString('data_path');

        $validator
            ->allowEmptyString('app_meta');

        return $validator;
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws SyntaxError
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options): void
    {
        $original = $entity->getOriginal('name');
        $name = $entity->get('name');
        if ($entity->isNew() === false && $original !== $name) {
            $FormVariables = $this->getAssociation('FormVariables');
            $query = $FormVariables->find()
                ->innerJoin(
                    ['FormExtractorsFormVariables' => 'form_extractors_form_variables'],
                    [
                        'form_variable_id' => new IdentifierExpression(
                            'FormVariables.id'
                        )
                    ]
                )
                ->where(['form_extractor_id' => $entity->id]);
            /** @var EntityInterface $formVariable */
            foreach ($query as $formVariable) {
                $this->updateFormVariableName($formVariable, $original, $name);
            }
        }
        if ($entity->isNew() || $entity->isDirty()) {
            TableRegistry::getTableLocator()->get('Forms')
                ->updateAll(
                    ['tested' => false],
                    ['id' => $entity->get('form_id')]
                );
        }
    }

    /**
     * Met à jour les form_variables.name suite au changement de nom d'un extracteur
     * @param EntityInterface $formVariable
     * @param string          $original
     * @param string          $name
     * @return void
     * @throws SyntaxError
     */
    private function updateFormVariableName(
        EntityInterface $formVariable,
        string $original,
        string $name
    ) {
        $FormVariables = $this->getAssociation('FormVariables');
        $formVariable->set(
            'twig',
            Twig::replaceVar($formVariable->get('twig'), 'extract.' . $original, 'extract.' . $name)
        );
        /** @var JsonString $jsonObject */
        $jsonObject = $formVariable->get('app_meta');
        foreach (Hash::flatten((array)$jsonObject) as $key => $value) {
            if ($value === 'extract.' . $original) {
                $ref = &$jsonObject;
                foreach (explode('.', $key) as $skey) {
                    $ref = &$ref[$skey];
                }
                $ref = 'extract.' . $name;
                $formVariable->setDirty('app_meta');
            }
        }
        $FormVariables->save($formVariable);
    }

    /**
     * Allows listeners to modify the rules checker by adding more rules.
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->addDelete(
            function (EntityInterface $entity) {
                return $entity->get('deletable');
            }
        );
        return parent::buildRules($rules);
    }

    /**
     * Get the object used to marshal/convert array data into objects.
     *
     * Override this method if you want a table object to use custom
     * marshalling logic.
     *
     * @return Marshaller
     * @see Marshaller
     */
    public function marshaller(): CakeMarshaller
    {
        return new Marshaller($this);
    }
}
