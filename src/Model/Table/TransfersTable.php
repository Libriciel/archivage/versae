<?php

/**
 * Versae\Model\Table\TransfersTable
 */

namespace Versae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Table\AfterSaveInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Table transfers
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @property DepositsTable Deposits
 */
class TransfersTable extends Table implements AfterSaveInterface
{
    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');

        $this->belongsTo('Deposits');
        $this->belongsTo('Users')
            ->setForeignKey('created_user_id');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('identifier')
            ->maxLength('identifier', 255)
            ->requirePresence('identifier', 'create')
            ->notEmptyString('identifier');

        $validator
            ->scalar('comment')
            ->allowEmptyString('comment');

        $validator
            ->scalar('path_token')
            ->maxLength('path_token', 8)
            ->notEmptyString('path_token');

        $validator
            ->integer('data_count')
            ->allowEmptyString('data_count');

        $validator
            ->allowEmptyString('data_size');

        $validator
            ->boolean('files_deleted')
            ->notEmptyFile('files_deleted');

        $validator
            ->scalar('error_message')
            ->allowEmptyString('error_message');

        $validator
            ->dateTime('last_comm_date')
            ->allowEmptyDateTime('last_comm_date');

        $validator
            ->scalar('last_comm_code')
            ->maxLength('last_comm_code', 255)
            ->allowEmptyString('last_comm_code');

        $validator
            ->scalar('last_comm_message')
            ->allowEmptyString('last_comm_message');

        $validator
            ->requirePresence('created_user_id', 'create')
            ->notEmptyString('created_user_id');

        return $validator;
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws Exception
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options): void
    {
        $transfers = $this->find()
            ->where(
                [
                    'Transfers.id !=' => $entity->id,
                    'Transfers.deposit_id' => $entity->get('deposit_id'),
                ]
            )
            ->contain(['Deposits']);
        foreach ($transfers as $transfer) {
            if (!is_file($entity->get('zip')) && is_file($transfer->get('zip'))) {
                Filesystem::rename($transfer->get('zip'), $entity->get('zip'));
            }
            $this->delete($transfer);
        }
    }
}
