<?php

/**
 * Versae\Model\Table\FiltersTable
 */

namespace Versae\Model\Table;

use AsalaeCore\Model\Table\FiltersTable as CoreFiltersTable;

/**
 * Table filters
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FiltersTable extends CoreFiltersTable
{
}
