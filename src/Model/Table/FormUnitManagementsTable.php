<?php

/**
 * Versae\Model\Table\FormUnitManagementsTable
 */

namespace Versae\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Table\AfterDeleteInterface;
use AsalaeCore\Model\Table\AfterSaveInterface;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Versae\Model\Entity\FormUnit;

/**
 * Table form_unit_managements
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property FormVariablesTable FormVariables
 */
class FormUnitManagementsTable extends Table implements AfterDeleteInterface, AfterSaveInterface
{
    /**
     * @var array
     */
    public $names = [];

    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo('FormUnits');
        $this->belongsTo('FormVariables');

        $this->names = [
            'seda1.0' => [
                'AccessRestrictionRule_Code' => [
                    'label' => __("Délai de communicabilité"),
                    'section' => 'access_rule',
                ],
                'AccessRestrictionRule_StartDate' => [
                    'label' => __("Date de départ du calcul (communicabilité)"),
                    'section' => 'access_rule',
                ],
                'AppraisalRule_Code' => [
                    'label' => __("Sort final à appliquer"),
                    'section' => 'appraisal_rule',
                ],
                'AppraisalRule_Duration' => [
                    'label' => __("Durée d'utilité administrative"),
                    'section' => 'appraisal_rule',
                ],
                'AppraisalRule_StartDate' => [
                    'label' => __("Date de départ du calcul (sort final)"),
                    'section' => 'appraisal_rule',
                ],
            ],
            'seda2.1' => [
                'AccessRule_Rule' => [
                    'label' => __("Délai de communicabilité"),
                    'section' => 'access_rule',
                ],
                'AccessRule_StartDate' => [
                    'label' => __("Date de départ du calcul (communicabilité)"),
                    'section' => 'access_rule',
                ],
                'AppraisalRule_FinalAction' => [
                    'label' => __("Sort final à appliquer"),
                    'section' => 'appraisal_rule',
                ],
                'AppraisalRule_Rule' => [
                    'label' => __("Durée d'utilité administrative"),
                    'section' => 'appraisal_rule',
                ],
                'AppraisalRule_StartDate' => [
                    'label' => __("Date de départ du calcul (sort final)"),
                    'section' => 'appraisal_rule',
                ],
            ],
        ];

        $additionnals = [
            'StorageRule' => [
                'Rule' => 'Rule',
                'StartDate' => 'StartDate',
                'FinalAction' => 'FinalAction',
            ],
            'DisseminationRule' => [
                'Rule' => 'Rule',
                'StartDate' => 'StartDate',
            ],
            'ReuseRule' => [
                'Rule' => 'Rule',
                'StartDate' => 'StartDate',
            ],
            'ClassificationRule' => [
                'Rule' => 'Rule',
                'StartDate' => 'StartDate',
                'ClassificationAudience' => 'ClassificationAudience',
                'ClassificationLevel' => 'ClassificationLevel',
                'ClassificationOwner' => 'ClassificationOwner',
                'ClassificationReassessingDate' => 'ClassificationReassessingDate',
                'NeedReassessingAuthorization' => 'NeedReassessingAuthorization',
            ],
        ];
        foreach ($additionnals as $name => $subfields) {
            $this->names['seda2.1']["Management_$name"] = [
                'label' => $name,
                'labelView' => $name,
                'section' => 'other_rule_options',
                'additionnal' => true,
                'subfields' => $name . '_' . implode(',' . $name . '_', array_keys($subfields)),
            ];
            foreach ($subfields as $field => $label) {
                $subfieldName = sprintf('%s_%s', $name, $field);
                $this->names['seda2.1']["Management_$subfieldName"] = [
                    'label' => $subfieldName,
                    'labelView' => $subfieldName,
                    'section' => 'other_rule',
                    'additionnal' => true,
                    'parent' => $name,
                ];
            }
        }
        $this->names['seda2.1']['Management_NeedAuthorization'] = [
            'label' => 'NeedAuthorization',
            'labelView' => 'NeedAuthorization',
            'section' => 'other_rule_options',
            'additionnal' => true,
            'parent' => false,
            'subfields' => 'NeedAuthorization',
        ];

        $this->names['seda2.2'] = $this->names['seda2.1'];

        $additionnals = [
            'HoldRule' => [
                'Rule' => 'Rule',
                'StartDate' => 'StartDate',
                'HoldEndDate' => 'HoldEndDate',
                'HoldOwner' => 'HoldOwner',
                'HoldReassessingDate' => 'HoldReassessingDate',
                'HoldReason' => 'HoldReason',
                'PreventRearrangement' => 'PreventRearrangement',
            ],
        ];
        foreach ($additionnals as $name => $subfields) {
            $this->names['seda2.2']["Management_$name"] = [
                'label' => $name,
                'labelView' => $name,
                'section' => 'other_rule_options',
                'additionnal' => true,
                'subfields' => $name . '_' . implode(',' . $name . '_', array_keys($subfields)),
            ];
            foreach ($subfields as $field => $label) {
                $subfieldName = sprintf('%s_%s', $name, $field);
                $this->names['seda2.2']["Management_$subfieldName"] = [
                    'label' => $subfieldName,
                    'labelView' => $subfieldName,
                    'section' => 'other_rule',
                    'additionnal' => true,
                    'parent' => $name,
                ];
            }
        }
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Renvoi un tableau de résultats (inclus les variables manquantes)
     * @param FormUnit $formUnit
     * @return array
     */
    public function formManagementData(FormUnit $formUnit): array
    {
        $names = $this->names[Hash::get($formUnit, 'form.seda_version')];
        $data = [];
        $entities = [];
        /** @var EntityInterface $formUnitHeader */
        foreach (Hash::get($formUnit, 'form_unit_managements') ?: [] as $formUnitHeader) {
            $name = $formUnitHeader->get('name');
            if (!isset($names[$name])) {
                continue; // schema modifié ?
            }
            $formUnitHeader->set('label', $names[$name]['label']);
            $formUnitHeader->set('required', $names[$name]['required'] ?? false);
            $formUnitHeader->set('section', $names[$name]['section'] ?? false);
            $formUnitHeader->set('parent', $names[$name]['parent'] ?? false);
            $entities[$name] = $formUnitHeader;
        }

        foreach ($names as $name => $params) {
            if (isset($entities[$name])) {
                $data[] = $entities[$name];
            } else {
                $data[] = [
                    'id' => null,
                    'name' => $name,
                    'label' => $params['label'],
                    'required' => $params['required'] ?? false,
                    'section' => $params['section'],
                    'additionnal' => $params['additionnal'] ?? false,
                    'subfields' => $params['subfields'] ?? false,
                    'parent' => $params['parent'] ?? false,
                ];
            }
        }

        return $this->filterManagementData($data);
    }

    /**
     * Retrait des entrées parentes dont tous les enfants sont déjà créés
     * @param array $data
     * @return array
     */
    protected function filterManagementData(array $data): array
    {
        return array_filter(
            $data,
            function ($e) use ($data) {
                if ($e['subfields'] ?? false) {
                    foreach (explode(',', $e['subfields']) as $subf) {
                        if (!array_filter($data, fn($item) => $item['name'] === "Management_$subf" && $item['id'])) {
                            return true;
                        }
                    }
                    return false;
                }
                return true;
            }
        );
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options): void
    {
        if ($entity->isNew() || $entity->isDirty()) {
            $loc = TableRegistry::getTableLocator();
            $subquery = $loc->get('FormUnits')
                ->find()
                ->select(['form_id'])
                ->where(
                    ['FormUnits.id' => $entity->get('form_unit_id')]
                );
            $loc->get('Forms')
                ->updateAll(
                    ['tested' => false],
                    ['Forms.id' => $subquery]
                );
        }
    }

    /**
     * The Model.afterDelete event is fired after an entity is deleted.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterDelete(Event $event, Entity $entity, ArrayObject $options): void
    {
        $this->FormVariables->deleteAll(['id' => $entity->get('form_variable_id')]);
    }
}
