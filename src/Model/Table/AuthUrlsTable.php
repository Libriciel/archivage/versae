<?php

/**
 * Versae\Model\Table\AuthUrlsTable
 */

namespace Versae\Model\Table;

use AsalaeCore\Model\Table\AuthUrlsTable as CoreAuthUrlsTable;
use AsalaeCore\Model\Table\BeforeFindInterface;
use Cake\ORM\Behavior\TimestampBehavior;

/**
 * Table auth_urls
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class AuthUrlsTable extends CoreAuthUrlsTable implements BeforeFindInterface
{
}
