<?php

/**
 * Versae\Model\Table\ChangeEntityRequestsTable
 */

namespace Versae\Model\Table;

use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Table;

/**
 * Table change_entity_requests
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2023, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class ChangeEntityRequestsTable extends Table
{
    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');

        $this->belongsTo('BaseArchivalAgencies')
            ->setClassName('OrgEntities');
        $this->belongsTo('TargetArchivalAgencies')
            ->setClassName('OrgEntities');
        $this->belongsTo('Users');
    }
}
