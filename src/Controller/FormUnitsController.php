<?php

/**
 * Versae\Controller\FormUnitsController
 */

namespace Versae\Controller;

use AsalaeCore\Utility\FormatError;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Response;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Exception;
use Psr\Http\Message\MessageInterface;
use Versae\Model\Table\FormCalculatorsTable;
use Versae\Model\Table\FormExtractorsTable;
use Versae\Model\Table\FormFieldsetsTable;
use Versae\Model\Table\FormInputsTable;
use Versae\Model\Table\FormsTable;
use Versae\Model\Table\FormUnitContentsTable;
use Versae\Model\Table\FormUnitHeadersTable;
use Versae\Model\Table\FormUnitKeywordsTable;
use Versae\Model\Table\FormUnitManagementsTable;
use Versae\Model\Table\FormUnitsTable;
use AsalaeCore\Controller\RenderDataTrait;

/**
 * FormUnits
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property FormCalculatorsTable FormCalculators
 * @property FormExtractorsTable FormExtractors
 * @property FormFieldsetsTable FormFieldsets
 * @property FormInputsTable FormInputs
 * @property FormUnitContentsTable FormUnitContents
 * @property FormUnitHeadersTable FormUnitHeaders
 * @property FormUnitKeywordsTable FormUnitKeywords
 * @property FormUnitManagementsTable FormUnitManagements
 * @property FormUnitsTable FormUnits
 * @property FormsTable Forms
 */
class FormUnitsController extends AppController
{
    use RenderDataTrait;

    /**
     * Donne le jstree d'archive_units d'un formulaire
     * @param string $form_id
     * @param string $displayAdds
     * @return Response
     * @throws Exception
     */
    public function getTree(string $form_id, string $displayAdds = 'true')
    {
        $formUnits = $this->FormUnits->find('threaded')
            ->innerJoinWith('Forms')
            ->where(
                [
                    'FormUnits.form_id' => $form_id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->order(['lft'])
            ->toArray();
        $this->loadComponent('AsalaeCore.Modal');
        $this->Modal->success();
        return $this->renderDataToJson(
            $this->FormUnits->threadedToJstreeData(
                $formUnits,
                null,
                $displayAdds === 'true'
            )
        );
    }

    /**
     * Ajoute une archive_unit au sein d'une autre archive_unit ou à la base
     * @param string $form_id
     * @param string $form_unit_id
     * @return Response|void
     * @throws Exception
     */
    public function add(string $form_id, string $form_unit_id)
    {
        $this->viewBuilder()->setLayout('ajax');
        $this->Forms = $this->fetchTable('Forms');
        $this->set('form_id', $form_id);
        $entity = $this->FormUnits->newEmptyEntity();
        if ($form_unit_id === 'root') {
            $this->Forms->find()
                ->where(
                    [
                        'id' => $form_id,
                        'org_entity_id' => $this->archivalAgencyId,
                    ]
                )
                ->firstOrFail();
            $parent_id = null;
            $parent = [];
        } else {
            $parent = $this->FormUnits->find()
                ->innerJoinWith('Forms')
                ->where(
                    [
                        'FormUnits.id' => $form_unit_id,
                        'Forms.id' => $form_id,
                        'Forms.org_entity_id' => $this->archivalAgencyId,
                    ]
                )
                ->firstOrFail();
            $parent_id = $parent->id;
        }

        $request = $this->getRequest();
        if ($request->is('post')) {
            // gestion des duplicates
            if ($archiveUnitId = $request->getData('duplicate')) {
                return $this->duplicate($archiveUnitId, $form_id, $parent);
            }

            // Vérifie que le parent_id est bien autorisé pour l'utilisateur
            if (
                $request->getData('parent_id')
                && $request->getData('parent_id') !== 'null'
                && $request->getData('parent_id') !== $parent_id
            ) {
                $parent = $this->FormUnits->find()
                    ->innerJoinWith('Forms')
                    ->where(
                        [
                            'FormUnits.id' => $request->getData('parent_id'),
                            'Forms.id' => $form_id,
                            'Forms.org_entity_id' => $this->archivalAgencyId,
                        ]
                    )
                    ->firstOrFail();
                $parent_id = $parent->id;
            }
            if ($parent) {
                $siblings = $this->FormUnits->find()
                    ->where(['parent_id' => $parent_id])
                    ->order(['lft' => 'desc'])
                    ->toArray();
            } else {
                $siblings = $this->FormUnits->find()
                    ->where(['parent_id IS' => null])
                    ->order(['lft' => 'desc'])
                    ->toArray();
            }
            $data = [
                'form_id' => $form_id,
                'name' => $request->getData('name'),
                'type' => $request->getData('type'),
                'parent_id' => $parent_id,
                'form_input_id' => $request->getData('form_input_id'),
                'presence_condition_id' => $request->getData('presence_condition_id'),
                'search_expression' => $request->getData('search_expression'),
                'presence_required' => $request->getData('presence_required'),
                'storage_calculation' => $request->getData('storage_calculation'),
                'repeatable_fieldset_id' => $request->getData('repeatable_fieldset_id'),
            ];
            $this->FormUnits->patchEntity($entity, $data);
            $this->loadComponent('AsalaeCore.Modal');
            $conn = $this->FormUnits->getConnection();
            $conn->begin();
            $success = $this->FormUnits->save($entity);
            $newId = $entity->id;
            if ($success) {
                $insertAfter = (int)$request->getData('order');
                $upAmount = $this->countUpAmount($insertAfter, $siblings);
                if ($upAmount > 0) {
                    $success = $this->FormUnits->moveUp($entity, $upAmount);
                }
            }
            if ($success) {
                $conn->commit();
                $this->Modal->success();
                return $this->getTree($form_id, $request->getData('append_addables') ?: '')
                    ->withHeader('X-Inserted-Id', $newId);
            } else {
                $conn->rollback();
                FormatError::logEntityErrors($entity);
                $this->Modal->fail();
            }
        }
        $this->set('entity', $entity);
        $this->set('parent_id', $parent_id);
        $this->set('parent', $parent);

        // options
        $this->setOptionsAddEdit($form_id, $parent);

        $duplicates = $this->FormUnits->find('treeList', ['spacer' => "\xCB\x91" . str_repeat("\xC2\xA0", 3)])
            ->where(['form_id' => $form_id])
            ->order(['lft'])
            ->toArray();
        $this->set('duplicates', $duplicates);
    }

    /**
     * Edition d'une unité d'archives
     * @param string $form_unit_id
     * @return Response|void
     * @throws Exception
     */
    public function edit(string $form_unit_id)
    {
        $this->viewBuilder()->setLayout('ajax');
        $this->Forms = $this->fetchTable('Forms');
        $entity = $this->FormUnits->find()
            ->innerJoinWith('Forms')
            ->where(
                [
                    'FormUnits.id' => $form_unit_id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(
                [
                    'ParentFormUnits',
                    'Forms',
                    'FormUnitHeaders' => ['FormVariables'],
                    'FormUnitManagements' => ['FormVariables'],
                    'FormUnitContents' => function (Query $q) {
                        return $q->order(['FormUnitContents.name'])
                            ->contain(['FormVariables']);
                    },
                    'FormUnitKeywords' => function (Query $q) {
                        return $q->order(['FormUnitKeywords.name'])
                            ->contain(
                                [
                                    'FormUnitKeywordDetails' => ['FormVariables'],
                                ]
                            );
                    },
                ]
            )
            ->firstOrFail();
        $this->set('form', $entity->get('form'));
        /** @var EntityInterface $parent */
        $parent = $entity->get('parent_form_unit');
        $parent_id = $parent?->id;
        $form_id = $entity->get('form_id');
        $this->set('form_id', $form_id);
        $currentPosIsAfter = $this->FormUnits->find()
            ->select(['id'])
            ->where(
                $parent
                    ? ['parent_id' => $parent_id]
                    : ['parent_id IS' => null, 'form_id' => $form_id]
            )
            ->andWhere(['lft <' => $entity->get('lft')])
            ->order(['lft' => 'desc'])
            ->disableHydration()
            ->first();
        $currentPosIsAfter = $currentPosIsAfter ? (int)$currentPosIsAfter['id'] : 0;
        $entity->set('order', $currentPosIsAfter);

        $request = $this->getRequest();
        if ($request->is('put')) {
            // Vérifie que le parent_id est bien autorisé pour l'utilisateur
            if (
                $request->getData('parent_id')
                && $request->getData('parent_id') !== 'null'
                && $request->getData('parent_id') !== $parent_id
            ) {
                $parent = $this->FormUnits->find()
                    ->innerJoinWith('Forms')
                    ->where(
                        [
                            'FormUnits.id' => $request->getData('parent_id'),
                            'Forms.id' => $form_id,
                            'Forms.org_entity_id' => $this->archivalAgencyId,
                        ]
                    )
                    ->firstOrFail();
                $parent_id = $parent->id;
            }
            $data = [
                'form_id' => $form_id,
                'name' => $request->getData('name'),
                'type' => $request->getData('type'),
                'parent_id' => $parent_id,
                'form_input_id' => $request->getData('form_input_id'),
                'presence_condition_id' => $request->getData('presence_condition_id'),
                'search_expression' => $request->getData('search_expression'),
                'presence_required' => $request->getData('presence_required'),
                'storage_calculation' => $request->getData('storage_calculation'),
                'repeatable_fieldset_id' => $request->getData('repeatable_fieldset_id'),
            ];
            $this->FormUnits->patchEntity($entity, $data);
            $this->loadComponent('AsalaeCore.Modal');
            $conn = $this->FormUnits->getConnection();
            $conn->begin();
            $success = $this->FormUnits->save($entity);
            /**
             * Déplacement du noeud
             */
            if ($success && $currentPosIsAfter !== (int)$request->getData('order')) {
                $insertAfter = (int)$request->getData('order');
                $currentPos = $this->FormUnits->find()
                    ->where(
                        $parent
                            ? ['parent_id' => $parent_id]
                            : ['parent_id IS' => null, 'form_id' => $form_id]
                    )
                    ->andWhere(['lft <' => $entity->get('lft')])
                    ->count();
                if ($insertAfter) {
                    $targetPos = $this->FormUnits->find()
                        ->where(
                            $parent
                                ? ['parent_id' => $parent_id]
                                : ['parent_id IS' => null, 'form_id' => $form_id]
                        )
                        ->andWhere(['lft <' => $this->FormUnits->get($insertAfter)->get('lft')])
                        ->count() + 1;
                } else {
                    $targetPos = 0;
                }
                if ($targetPos > $currentPos) {
                    $success = $this->FormUnits->moveDown($entity, $targetPos - $currentPos - 1);
                } elseif ($targetPos < $currentPos) {
                    $success = $this->FormUnits->moveUp($entity, $currentPos - $targetPos);
                }
            }

            if ($success) {
                $conn->commit();
                $this->Modal->success();
                return $this->getTree($form_id, $request->getData('append_addables') ?: '');
            } else {
                $conn->rollback();
                FormatError::logEntityErrors($entity);
                $this->Modal->fail();
            }
        }
        $this->set('entity', $entity);
        $this->set('parent_id', $parent_id);
        $this->set('parent', $parent);
        $this->set('id', $form_unit_id);

        $this->FormUnitHeaders = $this->fetchTable('FormUnitHeaders');
        $this->set('dataHeaders', $this->FormUnitHeaders->formHeadersData($entity));

        $this->FormUnitManagements = $this->fetchTable('FormUnitManagements');
        $this->set('dataManagements', $this->FormUnitManagements->formManagementData($entity));

        $this->FormUnitContents = $this->fetchTable('FormUnitContents');
        $this->set('dataContents', $this->FormUnitContents->formContentData($entity));

        $this->FormUnitKeywords = $this->fetchTable('FormUnitKeywords');
        $keywords = $this->FormUnitKeywords->find()
            ->where(['FormUnitKeywords.form_unit_id' => $form_unit_id])
            ->order(['FormUnitKeywords.name'])
            ->contain(['FormUnitKeywordDetails' => ['FormVariables']])
            ->toArray();
        $this->set('keywords', $keywords);

        // options
        $this->setOptionsAddEdit($form_id, $parent, $form_unit_id);
    }

    /**
     * Visualisation d'une unité d'archives
     * @param string $form_unit_id
     * @return void
     * @throws Exception
     */
    public function view(string $form_unit_id)
    {
        $this->viewBuilder()->setLayout('ajax');
        $this->Forms = $this->fetchTable('Forms');
        $entity = $this->FormUnits->find()
            ->innerJoinWith('Forms')
            ->where(
                [
                    'FormUnits.id' => $form_unit_id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(
                [
                    'ParentFormUnits',
                    'Forms',
                    'FormUnitHeaders' => ['FormVariables'],
                    'FormUnitManagements' => ['FormVariables'],
                    'FormUnitContents' => ['FormVariables'],
                    'FormUnitKeywords' => function (Query $q) {
                        return $q->order(['FormUnitKeywords.name'])
                            ->contain(
                                [
                                    'FormUnitKeywordDetails' => ['FormVariables'],
                                ]
                            );
                    },
                ]
            )
            ->firstOrFail();
        $this->set('form', $entity->get('form'));
        /** @var EntityInterface $parent */
        $parent = $entity->get('parent_form_unit');
        $parent_id = $parent?->id;
        $form_id = $entity->get('form_id');
        $this->set('form_id', $form_id);
        $currentPosIsAfter = $this->FormUnits->find()
            ->select(['id'])
            ->where(
                $parent
                    ? ['parent_id' => $parent_id]
                    : ['parent_id IS' => null, 'form_id' => $form_id]
            )
            ->andWhere(['lft <' => $entity->get('lft')])
            ->order(['lft' => 'desc'])
            ->disableHydration()
            ->first();
        $currentPosIsAfter = $currentPosIsAfter ? (int)$currentPosIsAfter['id'] : 0;
        $entity->set('order', $currentPosIsAfter);

        $this->set('entity', $entity);
        $this->set('parent_id', $parent_id);
        $this->set('parent', $parent);
        $this->set('id', $form_unit_id);

        $this->FormUnitHeaders = $this->fetchTable('FormUnitHeaders');
        $this->set('dataHeaders', $this->FormUnitHeaders->formHeadersData($entity));

        $this->FormUnitManagements = $this->fetchTable('FormUnitManagements');
        $this->set('dataManagements', $this->FormUnitManagements->formManagementData($entity));

        $this->FormUnitContents = $this->fetchTable('FormUnitContents');
        $this->set('dataContents', $this->FormUnitContents->formContentData($entity));

        $this->FormUnitKeywords = $this->fetchTable('FormUnitKeywords');
        $keywords = $this->FormUnitKeywords->find()
            ->where(['FormUnitKeywords.form_unit_id' => $form_unit_id])
            ->order(['FormUnitKeywords.name'])
            ->contain(['FormUnitKeywordDetails' => ['FormVariables']])
            ->toArray();
        $this->set('keywords', $keywords);
    }

    /**
     * Donne les options pour "Position dans l'arborescence" selon le parent_id
     * @param string      $form_id
     * @param string|null $parent_id
     * @return Response
     */
    public function getOrderOptions(string $form_id, string $parent_id = null): Response
    {
        // NOTE: le order ne fonctionne pas dans ce find list ! (sauf si on met un keyField=lft)
        $query = $this->FormUnits->find()
            ->where(['form_id' => $form_id])
            ->order(['lft']);
        if ($parent_id) {
            $query->where(['parent_id' => $parent_id]);
        } else {
            $query->where(['parent_id IS' => null]);
        }
        $options = [];
        /** @var EntityInterface $formUnit */
        foreach ($query as $formUnit) {
            $options[] = [
                'value' => $formUnit->id,
                'text' => __(
                    "Après: {0} - {1}",
                    $formUnit->get('type'),
                    $formUnit->get('name')
                )
            ];
        }
        return $this->renderDataToJson($options);
    }

    /**
     * Déplace un noeud sur un nouveau parent (glisser-déposer)
     * @param string $node_id
     * @param string $parent_id
     * @return Response
     * @throws Exception
     */
    public function move(string $node_id, string $parent_id)
    {
        $node = $this->FormUnits->find()
            ->innerJoinWith('Forms')
            ->where(
                [
                    'FormUnits.id' => $node_id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();
        $this->FormUnits->find()
            ->where(
                [
                    'FormUnits.id' => $parent_id,
                    'FormUnits.form_id' => $node->get('form_id'),
                ]
            )
            ->firstOrFail();
        $node->set('parent_id', $parent_id);
        $this->loadComponent('AsalaeCore.Modal');
        $request = $this->getRequest();
        if ($this->FormUnits->save($node)) {
            $this->Modal->success();
            return $this->getTree(
                $node->get('form_id'),
                $request->getData('display_addables', 'true')
            );
        } else {
            FormatError::logEntityErrors($node);
            $this->Modal->fail();
            throw new BadRequestException();
        }
    }

    /**
     * Options pour l'add et le edit
     * @param int                        $form_id
     * @param array|EntityInterface|null $parent       array vide, entité ou parent
     * @param int|null                   $form_unit_id
     * @throws Exception
     */
    private function setOptionsAddEdit(int $form_id, $parent = null, ?int $form_unit_id = null): void
    {
        $FormFieldsets = TableRegistry::getTableLocator()->get('FormFieldsets');
        $isThereRepeatableFieldsets = $FormFieldsets->exists(
            [
                'form_id' => $form_id,
                'repeatable' => true,
            ]
        );

        $types = [];
        foreach ($this->FormUnits->getTypesOptions($parent ?: null) as $type => $trad) {
            $opt = [
                'text' => $trad,
                'value' => $type,
            ];
            if ($type === FormUnitsTable::TYPE_TREE_SEARCH) {
                $opt['title'] = __("Règles pour chaque fichiers correspondant à l'expression de recherche");
            } elseif ($type === FormUnitsTable::TYPE_TREE_PATH) {
                $opt['title'] = __("Règles pour un fichier particulier situé dans le zip");
            } elseif (!$isThereRepeatableFieldsets && $type === FormUnitsTable::TYPE_REPEATABLE) {
                $opt['data-disabled'] = true;
                $opt['title'] = __("Aucune section répétable dans le formulaire");
            }
            $types[] = $opt;
        }

        $this->set('types', $types);

        $query = $this->FormUnits->find('treeList', ['spacer' => "\xCB\x91" . str_repeat("\xC2\xA0", 3)])
            ->where(
                [
                    'form_id' => $form_id,
                    'type IN' => [
                        FormUnitsTable::TYPE_SIMPLE,
                        FormUnitsTable::TYPE_REPEATABLE,
                    ],
                ]
            )
            ->order(['lft']);

        if ($form_unit_id) {
            $query->andWhere(['id !=' => $form_unit_id]);
        }

        $disableRep = $this->isRepeatableDisabled($parent, $form_unit_id);
        $this->set('disableRepeatable', $disableRep);
        $repeatableFieldsetId = null;
        if (!$disableRep && $parent) {
            $parentRepetable = $this->FormUnits->find()
                ->where(
                    [
                        'form_id' => $form_id,
                        'lft <=' => Hash::get($parent, 'lft'),
                        'rght >=' => Hash::get($parent, 'rght'),
                        'type' => FormUnitsTable::TYPE_REPEATABLE,
                    ]
                )
                ->first();
            if ($parentRepetable) {
                $repeatableFieldsetId = $parentRepetable->get('repeatable_fieldset_id');
                $this->set('isChildOfRepetable', true);
                $query->andWhere(
                    [
                        'lft >=' => $parentRepetable->get('lft'),
                        'rght <=' => $parentRepetable->get('rght'),
                    ]
                );
            }
        }
        $this->set('repeatableFieldsetId', $repeatableFieldsetId);

        $this->set('parent_ids', $query->toArray());

        $this->FormInputs = $this->fetchTable('FormInputs');
        $formInputs = [];
        $query = $this->FormInputs->find()
            ->where(
                [
                    'FormInputs.form_id' => $form_id,
                    'FormInputs.type IN' => [FormInputsTable::TYPE_FILE, FormInputsTable::TYPE_ARCHIVE_FILE],
                ]
            )
            ->contain(['FormFieldsets'])
            ->order(['FormFieldsets.ord', 'FormInputs.ord']);

        /** @var EntityInterface $formInput */
        foreach ($query as $formInput) {
            $fieldsetName = Hash::get($formInput, 'form_fieldset.legend')
                ?: __("Section sans titre #{0}", Hash::get($formInput, 'form_fieldset.ord') + 1);
            $title = __("Champ simple");
            if (Hash::get($formInput, 'form_fieldset.repeatable')) {
                $fieldsetName = hex2bin('ef86b3') . $fieldsetName;
            }
            if (!isset($formInputs[$fieldsetName])) {
                $formInputs[$fieldsetName] = [];
            }
            if ($formInput->get('multiple')) {
                $title = __("Champ multiple");
            } elseif (Hash::get($formInput, 'form_fieldset.repeatable')) {
                $title = __("Champ de section répétable");
            }
            $formInputs[$fieldsetName][] = [
                'text' => $formInput->get('name') . ' - ' . $formInput->get('label'),
                'title' => $title,
                'value' => $formInput->get('id'),
                'class' => $formInput->get('type'),
                'data-multiple' => (bool)$formInput->get('multiple'),
                'data-repeatable' => (bool)Hash::get($formInput, 'form_fieldset.repeatable'),
                'data-fieldsets' => $formInput->get('form_fieldset_id'),
            ];
        }
        $this->set('formInputs', $formInputs);

        $valueField = fn(EntityInterface $e)
        => __("Après: {0} - {1}", $e->get('type'), $e->get('name'));
        $query = $this->FormUnits->find('list', ['valueField' => $valueField])
            ->where(['form_id' => $form_id])
            ->order(['lft']);
        if ($parent) {
            $query->where(['parent_id' => $parent->id]);
        } else {
            $query->where(['parent_id IS' => null]);
        }
        if ($form_unit_id) {
            $query->andWhere(['id !=' => $form_unit_id]);
        }
        $this->set('orders', $query->toArray());

        $optParams = [
            'keyField' => function (EntityInterface $entity) {
                return $entity->get('form_variable_id');
            },
            'groupField' => function () {
                return __("Variables");
            },
        ];
        $this->FormCalculators = $this->fetchTable('FormCalculators');
        $variables = $this->FormCalculators->find('list', $optParams)
            ->where(
                [
                    'FormCalculators.form_id' => $form_id,
                ]
            ) // attention pour edit, n'autoriser que les var ord <
            ->order(['FormCalculators.name']);
        $variables = $variables->toArray();

        $this->set('presence_conditions', array_merge($variables));

        $this->set('storage_calculations', $this->FormUnits->options('storage_calculation'));

        $this->FormFieldsets = $this->fetchTable('FormFieldsets');
        $repeatableFieldsets = $this->FormFieldsets
            ->find(
                'list',
                [
                    'valueField' => function (EntityInterface $entity) {
                        return $entity->get('legend')
                            ?: __("Section sans titre #{0}", $entity->get('ord') + 1);
                    },
                ]
            )
            ->where(
                [
                    'FormFieldsets.form_id' => $form_id,
                    'FormFieldsets.repeatable IS' => true,
                ]
            );
        $this->set('repeatableFieldsets', $repeatableFieldsets->toArray());
    }

    /**
     * Supprime un form_unit
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete(string $id)
    {
        $this->getRequest()->allowMethod('delete');
        $entity = $this->FormUnits->find()
            ->innerJoinWith('Forms')
            ->where(
                [
                    'FormUnits.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();

        $this->loadComponent('AsalaeCore.Modal');
        if ($this->FormUnits->delete($entity)) {
            $this->Modal->success();
            return $this->getTree($entity->get('form_id'));
        } else {
            $this->Modal->fail();
            return $this->renderDataToJson('Erreur lors de la suppression');
        }
    }

    /**
     * Donne la position en fonction du $insertAfter
     * @param int   $insertAfter
     * @param array $siblings
     * @return int
     */
    private function countUpAmount(int $insertAfter, array $siblings): int
    {
        if ($insertAfter === 0) {
            $upAmount = count($siblings);
        } else {
            $upAmount = 0;
            /** @var EntityInterface $sibling */
            foreach ($siblings as $sibling) {
                if ($sibling->id === $insertAfter) {
                    break;
                }
                $upAmount++;
            }
        }
        return $upAmount;
    }

    /**
     * Action de duplication d'un element de formulaire
     * @param string                $archiveUnitId
     * @param string                $form_id
     * @param array|EntityInterface $parent
     * @return Response|MessageInterface
     * @throws Exception
     */
    private function duplicate($archiveUnitId, string $form_id, $parent)
    {
        if (Hash::get($parent, 'type') === FormUnitsTable::TYPE_TREE_ROOT) {
            throw new ForbiddenException('Duplicate on tree_root is not allowed');
        }

        $targetFormUnit = $this->FormUnits->get($archiveUnitId);
        $formUnit = $this->FormUnits->find('threaded')
            ->where(
                [
                    'FormUnits.lft >=' => $targetFormUnit->get('lft'),
                    'FormUnits.rght <=' => $targetFormUnit->get('rght'),
                    'FormUnits.form_id' => $form_id,
                ]
            )
            ->contain(
                [
                    'FormUnitHeaders' => ['FormVariables'],
                    'FormUnitManagements' => ['FormVariables'],
                    'FormUnitContents' => ['FormVariables'],
                    'FormUnitKeywords' => function (Query $q) {
                        return $q->order(['FormUnitKeywords.name'])
                            ->contain(
                                [
                                    'FormUnitKeywordDetails' => ['FormVariables'],
                                ]
                            );
                    },
                ]
            )
            ->toArray();
        $conn = $this->FormUnits->getConnection();
        $conn->begin();
        try {
            $newId = $this->recursiveImportDuplicate($formUnit, $parent);
        } catch (Exception $e) {
            $conn->rollback();
            throw $e;
        }
        $conn->commit();

        return $this->getTree($form_id)
            ->withHeader('X-Inserted-Id', $newId);
    }

    /**
     * Fonction récursive d'import d'un élément dupliqué
     * @param array                 $formUnits
     * @param array|EntityInterface $parent
     * @return int
     */
    private function recursiveImportDuplicate(array $formUnits, $parent): int
    {
        $lastId = 0;
        foreach ($formUnits as $child) {
            $keywords = $child->get('form_unit_keywords');
            $contents = $child->get('form_unit_contents');
            $managements = $child->get('form_unit_managements');
            $headers = $child->get('form_unit_headers');
            $children = $child->get('children');
            $child->unset('id');
            $child->unset('parent_id');
            $child->unset('form_unit_keywords');
            $child->unset('form_unit_contents');
            $child->unset('form_unit_managements');
            $child->unset('form_unit_headers');
            $child->unset('children');
            $child->setNew(true);
            if ($parent) {
                $child->set('parent_id', $parent->get('id'));
            }
            $this->fetchTable('FormUnits')->saveOrFail($child);
            $lastId = $child->get('id');
            foreach ($keywords as $keyword) {
                $this->duplicateKeywords($keyword, $child);
            }
            foreach ($contents as $content) {
                $this->duplicateGeneric(
                    'FormUnitContents',
                    $content,
                    $child
                );
            }
            foreach ($managements as $management) {
                $this->duplicateGeneric(
                    'FormUnitManagements',
                    $management,
                    $child
                );
            }
            foreach ($headers as $header) {
                $this->duplicateGeneric(
                    'FormUnitHeaders',
                    $header,
                    $child
                );
            }
            $this->recursiveImportDuplicate($children, $child);
        }
        return $lastId;
    }

    /**
     * Duplique une variable
     * @param EntityInterface $variable
     * @return EntityInterface
     */
    private function duplicateVariable(EntityInterface $variable)
    {
        $variable->unset('id');
        $variable->setNew(true);
        return $this->fetchTable('FormVariables')->saveOrFail($variable);
    }

    /**
     * Duplique une variable
     * @param EntityInterface $keyword
     * @param EntityInterface $child
     * @return void
     */
    private function duplicateKeywords(
        EntityInterface $keyword,
        EntityInterface $child
    ) {
        $keywordDetails = $keyword->get('form_unit_keyword_details');
        $keyword->setNew(true);
        $keyword->unset('id');
        $keyword->unset('form_unit_keyword_details');
        $keyword->set('form_unit_id', $child->get('id'));
        $this->fetchTable('FormUnitKeywords')->saveOrFail($keyword);
        foreach ($keywordDetails as $keywordDetail) {
            $variable = $this->duplicateVariable($keywordDetail->get('form_variable'));
            $keywordDetail->setNew(true);
            $keywordDetail->unset('id');
            $keywordDetail->unset('form_variable');
            $keywordDetail->set('form_unit_keyword_id', $keyword->get('id'));
            $keywordDetail->set('form_variable_id', $variable->get('id'));
            $this->fetchTable('FormUnitKeywordDetails')->saveOrFail($keywordDetail);
        }
    }

    /**
     * Duplique un form_content, form_management ou form_header
     * @param string          $modelName
     * @param EntityInterface $entity
     * @param EntityInterface $child
     * @return void
     */
    private function duplicateGeneric(
        string $modelName,
        EntityInterface $entity,
        EntityInterface $child
    ) {
        $variable = $this->duplicateVariable($entity->get('form_variable'));
        $entity->setNew(true);
        $entity->unset('id');
        $entity->unset('form_variable');
        $entity->set('form_unit_id', $child->get('id'));
        $entity->set('form_variable_id', $variable->get('id'));
        $this->fetchTable($modelName)->saveOrFail($entity);
    }

    /**
     * Détermine si on interdit le choix d'un champ de formulaire répétable pour cette UA :
     *      - il y a un parent répétable -> autorise
     *      - il y a pas de parent répétable MAIS cette UA est de type répétable -> autorise
     *      - sinon interdit
     * @param array|EntityInterface|null $parent
     * @param int|null                   $form_unit_id
     * @return bool
     */
    protected function isRepeatableDisabled($parent, ?int $form_unit_id): bool
    {
        if (!$parent) { // ua racine, pas pertinent
            return false;
        }

        if ($parent->get('repeatable_fieldset_id_parent')) { // enfant d'un répétable
            return false;
        }
        if ($form_unit_id) { // lui même répétable
            $ua = $this->FormUnits->get($form_unit_id);
            return $ua->get('type') !== FormUnitsTable::TYPE_REPEATABLE;
        }
        return true;
    }
}
