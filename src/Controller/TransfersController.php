<?php

/**
 * Versae\Controller\TransfersController
 */

namespace Versae\Controller;

use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Http\CallbackStream;
use Cake\Http\Exception\UnauthorizedException;
use Cake\Http\Response;
use Versae\Model\Table\AuthUrlsTable;
use Versae\Model\Table\TransfersTable;

/**
 * Rôles
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AuthUrlsTable AuthUrls
 * @property TransfersTable Transfers
 */
class TransfersController extends AppController implements ApiInterface
{
    use ApiTrait;

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return [
            'zip',
//            'deleteZip', // TODO
        ];
    }

    /**
     * Téléchargement du versement par api
     * @param string $id
     * @return Response
     */
    protected function zip(string $id)
    {
        $request = $this->getRequest();
        $this->AuthUrls = $this->fetchTable('AuthUrls');
        if (!$this->AuthUrls->isAuthorized($request)) {
            throw new UnauthorizedException();
        }
        $this->AuthUrls->consume($request);
        $ot = $this->Transfers->get($id);
        $zip = $ot->get('zip');
        return $this->getResponse()
            ->withHeader('Content-Type', mime_content_type($zip))
            ->withHeader('Content-Disposition', 'attachment; filename="' . basename($zip) . '"')
            ->withHeader('Content-Length', filesize($zip))
            ->withBody(
                new CallbackStream(
                    function () use ($zip) {
                        ob_end_clean();
                        ob_implicit_flush();
                        readfile($zip);
                    }
                )
            );
    }
}
