<?php

/**
 * Versae\Controller\Admins\ConfigurationsTrait
 */

namespace Versae\Controller\Admins;

use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\FormatError;
use Cake\Core\Configure;
use Cake\Mailer\Mailer;
use Cake\Mailer\TransportFactory;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Versae\Controller\AdminsController;
use Versae\Form\ConfigurationForm;
use Versae\Model\Entity\Configuration;
use Versae\Model\Entity\Fileupload;

/**
 * Trait ConfigurationsTrait
 *
 * @category Controller\Admins
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin AdminsController
 */
trait ConfigurationsTrait
{
    /**
     * Permet de modifier la configuration par editeur de texte
     */
    public function ajaxEditConf()
    {
        $this->Fileuploads = $this->fetchTable('Fileuploads');
        $this->viewBuilder()->setTemplatePath('Admins/Configurations');
        $data = ['name' => 'logo-client'];
        /** @var Configuration $configuration */
        if ($this->isConnected()) {
            $this->OrgEntities = $this->fetchTable('OrgEntities');
            $this->Configurations = $this->fetchTable('Configurations');
            $se = $this->OrgEntities->find()
                ->where(['TypeEntities.code' => 'SE'])
                ->contain(['TypeEntities'])
                ->first();
            if ($se) {
                $data += ['org_entity_id' => $se->get('id')];
                $configuration = $this->Configurations->find()
                    ->where($data)
                    ->first();
            } else {
                $configuration = null;
            }
            if (!$configuration) {
                $configuration = $this->Configurations->newEntity(
                    $data,
                    ['validate' => false]
                );
            }
        } else {
            $se = null;
            $configuration = null;
        }

        $this->set('se', $se);

        $form = new ConfigurationForm();
        if ($this->getRequest()->is('post')) {
            $data = $this->getRequest()->getData();
            Filesystem::begin();
            if ($this->isConnected()) {
                $conn = $this->Configurations->getConnection();
                $conn->begin();
            }
            // Upload du background -> copy sur webroot
            if (
                $se
                && ($file_id = $this->getRequest()->getData('fileuploads.id'))
                && $file_id !== 'local'
            ) {
                $this->Fileuploads = $this->fetchTable('Fileuploads');
                /** @var Fileupload $file */
                $file = $this->Fileuploads->get($file_id);
                $file->copyInEntityDirectory($configuration);
            }
            if ($this->isConnected()) {
                $data['libriciel_login_logo'] = $configuration->get('setting');
                if (!$data['libriciel_login_logo'] && $configuration->get('id')) {
                    $this->Configurations->delete($configuration->get('id'));
                }
                $success = empty($data['libriciel_login_logo']) || $this->Configurations->save($configuration);
            } elseif (!isset($success)) {
                $success = true;
            }
            $this->loadComponent('AsalaeCore.Modal');
            if ($success && $form->execute($data)) {
                if ($this->isConnected() && isset($conn)) {
                    $conn->commit();
                }
                Filesystem::commit();
                $this->Modal->success();
            } else {
                if ($this->isConnected() && isset($conn)) {
                    $conn->rollback();
                }
                Filesystem::rollback();
                FormatError::logFormErrors($form);
                $this->Modal->fail();
            }
        }
        $appLocalConfigFileUri = $this->getAppLocalConfigFileUri();
        if (empty($appLocalConfigFileUri)) {
            $this->Flash->error(
                __("Le fichier de configuration local n'a pas été trouvé")
            );
        }
        $this->set(
            'config',
            file_get_contents($appLocalConfigFileUri)
        );
        $this->set('default', include CONFIG . 'app_default.php');
        $this->set('form', $form);
        $this->set('hash_algo', ConfigurationForm::options('hash_algo'));
        $this->set(
            'password_complexity',
            ConfigurationForm::options('password_complexity')
        );
        $this->set(
            'datacompressor_encodefilename',
            ConfigurationForm::options('datacompressor_encodefilename')
        );
        $this->set(
            'filesystem_useshred',
            ConfigurationForm::options('filesystem_useshred')
        );
        $this->set('appLocalConfigFileUri', $this->getAppLocalConfigFileUri());

        $uploadFiles = [];
        if ($configuration && $url = $configuration->get('setting')) {
            $publicDir = Configure::read('OrgEntities.public_dir', WWW_ROOT);
            $uploadFiles[] = [
                'id' => 'local',
                'prev' => $url,
                'name' => basename($url),
                'size' => filesize($publicDir . ltrim($url, '/')),
                'webroot' => true
            ];
        }
        $this->set('uploadFiles', $uploadFiles);

        // Fichiers ssl du SedaGenerator
        $path = rtrim(Configure::read('App.paths.data'), DS) . DS . 'ssl' . DS . 'seda-generator';
        $sedaSslUploadData = $this->Fileuploads->find()
            ->where(['path LIKE' => $path . '%'])
            ->order(['name'])
            ->toArray();
        $this->set('sedaSslUploadData', $sedaSslUploadData);

        $defaultEmail = $this->getRequest()->getSession()->read('Admin.data.email');
        $this->set('defaultEmail', $defaultEmail);
    }

    /**
     * Supprime le logo configuré par l'entité
     * @throws Exception
     */
    public function deleteLogo()
    {
        $this->Configurations = $this->fetchTable('Configurations');
        $this->getRequest()->allowMethod('delete');
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $this->Configurations = $this->fetchTable('Configurations');
        $se = $this->OrgEntities->find()
            ->where(['TypeEntities.code' => 'SE'])
            ->contain(['TypeEntities'])
            ->firstOrFail();
        $id = $se->get('id');
        $this->getRequest()->allowMethod('delete');
        $entity = $this->Configurations->find()
            ->where(
                [
                    'org_entity_id' => $id,
                    'name' => 'logo-client',
                ]
            )
            ->firstOrFail();

        Filesystem::begin();
        Filesystem::setSafemode(true);
        if ($this->Configurations->delete($entity)) {
            Filesystem::commit();
            $report = 'done';
        } else {
            Filesystem::rollback();
            $report = 'Erreur lors de la suppression';
        }

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Envoi des emails de test
     */
    public function sendTestMail()
    {
        $emailAdress = $this->getRequest()->getData('email');
        $success = !empty($emailAdress);

        $request = $this->getRequest();
        $default = TransportFactory::getConfig('default');
        TransportFactory::drop('default');
        TransportFactory::setConfig(
            [
                'default' => $request->getData() + $default,
            ]
        );

        /** @var Mailer $email */
        $email = clone Utility::get(Mailer::class);
        $email->viewBuilder()
            ->setTemplate('test')
            ->setLayout('default')
            ->addHelpers(['Html', 'Url']);
        $success = $success && $email->setEmailFormat('both')
                ->setSubject('[versae] ' . __("Email de test"))
                ->setTo($emailAdress)
                ->send();
        $this->loadComponent('Emails');
        if ($response = $this->Emails->debug($email)) {
            return $response;
        }
        return $this->renderDataToJson(['success' => $success]);
    }
}
