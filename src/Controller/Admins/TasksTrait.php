<?php

/**
 * Versae\Controller\Admins\TasksTrait
 */

namespace Versae\Controller\Admins;

use Versae\Controller\AdminsController;
use Versae\Form\AddWorkerForm;
use Versae\Model\Entity\BeanstalkJob;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Http\Response as CoreResponse;
use AsalaeCore\Factory;
use Beanstalk\Command\WorkerCommand;
use Beanstalk\Model\Table\BeanstalkJobsTable;
use Beanstalk\Utility\Beanstalk;
use Cake\Http\Response as CakeResponse;
use Exception;
use Psr\Http\Message\ResponseInterface;
use Cake\Core\Configure;

/**
 * Trait TasksTrait
 *
 * @category Controller\Admins
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin AdminsController
 */
trait TasksTrait
{
    /**
     * Suppression du tube
     *
     * @param string $tube
     * @return CakeResponse
     * @throws Exception
     */
    public function deleteTube(string $tube)
    {
        $this->fetchTable('BeanstalkJobs')
            ->deleteAll(
                [
                    'tube' => $tube,
                    'job_state !=' => BeanstalkJobsTable::S_WORKING,
                ]
            );
        $this->loadComponent('AsalaeCore.Modal');
        $this->Modal->success();
        return $this->renderDataToJson('');
    }

    /**
     * Administration des tubes et des workers beanstalk
     */
    public function ajaxTasks()
    {
        $this->viewBuilder()->setTemplatePath('Admins/Tasks');
        /** @var Beanstalk $Beanstalk */
        $Beanstalk = Factory\Utility::get('Beanstalk');
        if (!$Beanstalk->isConnected() || !$this->isConnected()) {
            return $this->getResponse()->withStringBody(
                __(
                    "Le serveur de base de données et le serveur de jobs "
                    . "doivent fonctionner pour accéder à cette fonctionnalité"
                )
            );
        }

        $this->BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $this->BeanstalkWorkers = $this->fetchTable('BeanstalkWorkers');

        $workerParams = Configure::read('Beanstalk.workers');
        $workerTubes = array_keys($workerParams);
        sort($workerTubes);
        $tubes = [];
        foreach ($workerTubes as $tube) {
            $q = $this->BeanstalkJobs->query();
            $jobStates = $this->BeanstalkJobs->find()
                ->select(
                    [
                        'job_state',
                        'count' => $q->func()->count('*'),
                    ]
                )
                ->where(['tube' => $tube])
                ->group(['job_state'])
                ->toArray();
            $workerCount = $this->BeanstalkWorkers->find()
                ->where(['tube' => $tube])
                ->count();
            $tubes[$tube] = [
                'tube' => $tube,
                'workers' => $workerCount,
                BeanstalkJobsTable::S_WORKING => 0,
                BeanstalkJobsTable::S_PENDING => 0,
                BeanstalkJobsTable::S_DELAYED => 0,
                BeanstalkJobsTable::S_PAUSED => 0,
                BeanstalkJobsTable::S_FAILED => 0,
            ];
            foreach ($jobStates as $state) {
                $tubes[$tube][$state->get('job_state')] = $state->get('count');
            }
        }
        $q = $this->BeanstalkJobs->query();
        $jobCount = $this->BeanstalkJobs->find()
            ->select(['count' => $q->func()->count('*')])
            ->where(['tube' => $q->identifier('BeanstalkWorkers.tube')]);

        $workers = $this->BeanstalkWorkers->find()
            ->enableAutoFields()
            ->select(['jobs' => $jobCount])
            ->order(['tube']);
        $this->set('workers', $workers);
        $this->set('tubes', $tubes);
        $this->set('states', $this->BeanstalkJobs->options('job_state'));

        $filename = Configure::read('App.paths.logs', LOGS) . 'beanstalk.log';
        $command = sprintf(
            "tail -n500 %s",
            escapeshellarg($filename)
        );
        exec($command, $output);

        $this->set('serviceLogs', implode("\n", $output));
    }

    /**
     * Arrete tout les workers selon le pid en base
     */
    public function stopAllWorkers()
    {
        $Beanstalk = Factory\Utility::get('Beanstalk');
        $Beanstalk->socketEmit('stop-all-workers');
        return $this->renderDataToJson([]);
    }

    /**
     * Demande l'arret d'un worker
     *
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function stopWorker(string $id)
    {
        $worker = $this->fetchTable('BeanstalkWorkers')->get($id);
        $message = sprintf('stop:%s:%d', $worker->get('tube'), $id);
        Beanstalk::getInstance()->socketEmit($message);
        $this->loadComponent('AsalaeCore.Modal');
        $this->Modal->success();
        return $this->renderDataToJson('done');
    }

    /**
     * Tue un worker
     *
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function killWorker(string $id)
    {
        $this->BeanstalkWorkers = $this->fetchTable('BeanstalkWorkers');
        $worker = $this->BeanstalkWorkers->get($id);
        Beanstalk::getInstance()
            ->socketEmit(sprintf('kill:%s:%d', $worker->get('tube'), $id));
        $this->loadComponent('AsalaeCore.Modal');
        $this->Modal->success();
        return $this->renderDataToJson('done');
    }

    /**
     * Liste les jobs d'un tube
     * @param string $tube
     * @return ResponseInterface|void
     * @throws Exception
     */
    public function indexJobs(string $tube)
    {
        $this->viewBuilder()->setTemplatePath('Admins/Tasks');
        $this->loadComponent('AsalaeCore.Index');
        $this->loadComponent('AsalaeCore.AjaxPaginator');
        $this->BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $query = $this->BeanstalkJobs->find()
            ->where(['tube' => $tube])
            ->contain(['Users']);

        $this->Index->setConfig('model', 'BeanstalkJobs');
        $this->Index->setConfig('limit', 10);

        $this->Index->setQuery($query)
            ->filter('id')
            ->filter('job_state', IndexComponent::FILTER_IN)
            ->filter(
                'username',
                IndexComponent::FILTER_ILIKE,
                'Users.username'
            );

        if ($this->getRequest()->accepts('application/json')) {
            return $this->AjaxPaginator->json($query);
        }

        $data = $this->paginate($query);

        $this->set('data', $data);
        $this->set('count', $query->count());
        $this->set('tube', $tube);
        $this->set('states', $this->BeanstalkJobs->options('job_state'));
        $this->set('connected', (bool)$this->userId);
    }

    /**
     * Permet d'obtenir les infos d'un job au format json
     * @param string $id
     * @return CakeResponse
     */
    public function jobInfo(string $id)
    {
        $this->BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        /** @var BeanstalkJob $job */
        $job = $this->BeanstalkJobs->find()
            ->where(['BeanstalkJobs.id' => $id])
            ->contain(['Users'])
            ->first();
        return $this->renderDataToJson($job);
    }

    /**
     * delete le job
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function jobCancel(string $id)
    {
        $this->BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $entity = $this->BeanstalkJobs->find()
            ->where(['id' => $id])
            ->firstOrFail();
        $this->BeanstalkJobs->deleteOrFail($entity);
        $report = "done";

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * bury le job
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function jobPause(string $id)
    {
        $this->BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $entity = $this->BeanstalkJobs->find()
            ->where(['BeanstalkJobs.id' => $id])
            ->contain(['Users'])
            ->firstOrFail();
        $this->BeanstalkJobs->transitionOrFail($entity, BeanstalkJobsTable::T_PAUSE);
        $this->BeanstalkJobs->saveOrFail($entity);
        $report = "done";

        $entity->set('errors', 'pause');
        $data = $entity->toArray();

        return $this->renderDataToJson(['report' => $report, 'data' => $data]);
    }

    /**
     * kick le job
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function jobResume(string $id)
    {
        $this->BeanstalkJobs = $this->fetchTable('BeanstalkJobs');

        $entity = $this->BeanstalkJobs->find()
            ->where(['BeanstalkJobs.id' => $id])
            ->contain(['Users'])
            ->firstOrFail();
        $this->BeanstalkJobs->transitionOrFail($entity, BeanstalkJobsTable::T_RESUME);
        $this->BeanstalkJobs->saveOrFail($entity);
        $report = "done";

        $data = $entity->toArray();

        return $this->renderDataToJson(['report' => $report, 'data' => $data]);
    }

    /**
     * Permet de lancer manuellement un worker
     */
    public function addWorker()
    {
        $this->viewBuilder()->setTemplatePath('Admins/Tasks');
        $workerForm = new AddWorkerForm();

        if ($this->getRequest()->is('post')) {
            $this->loadComponent('AsalaeCore.Modal');
            $data = $this->getRequest()->getData();

            $success = $workerForm->execute($this->getRequest()->getData());
            if ($success) {
                $this->Modal->success();
                return $this->renderDataToJson($data);
            } else {
                $this->Modal->fail();
            }
        }

        $workers = WorkerCommand::availablesWorkers();
        $availables = [];
        foreach (array_keys($workers) as $tube) {
            $availables[$tube] = $tube;
        }
        $this->set('availables', $availables);
        $this->set('job', $workerForm);
    }

    /**
     * Permet d'obtenir les logs du tube d'un worker
     * @param string $tube
     * @throws Exception
     */
    public function workerLog(string $tube)
    {
        $this->viewBuilder()->setTemplatePath('Admins/Tasks');
        $logfile = Configure::read(
            'App.paths.logs',
            LOGS
        ) . 'worker_' . $tube . '.log';
        $log = Factory\Utility::get('Exec')->command(
            'tail -n500',
            $logfile
        )->stdout;
        $this->set('log', $log);
    }

    /**
     * Relance les jobs en erreur/pause d'un tube
     * @param string $tube
     * @return CakeResponse
     * @throws Exception
     */
    public function resumeAllTube(string $tube)
    {
        $this->BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $query = $this->BeanstalkJobs->find()
            ->where(['tube' => $tube, 'job_state' => BeanstalkJobsTable::S_FAILED]);
        $kicked = $query->count();
        foreach ($query as $job) {
            $this->BeanstalkJobs->transition($job, BeanstalkJobsTable::T_RETRY);
            $this->BeanstalkJobs->save($job);
        }
        return $this->renderDataToJson(
            ['success' => true, 'kicked' => $kicked]
        );
    }

    /**
     * Effectue un tail -f sur un fichier log
     * @param string $timeout
     * @return CoreResponse
     * @throws Exception
     */
    public function tailService($timeout = '10')
    {
        $filename = Configure::read('App.paths.logs', LOGS) . 'beanstalk.log';
        setlocale(LC_ALL, 'en_US.UTF-8');
        $command = sprintf(
            "timeout %.1F tail -fn500 %s",
            (float)$timeout,
            escapeshellarg($filename)
        );
        setlocale(LC_ALL, null);

        /** @var CoreResponse $response */
        $response = $this->getResponse()->withType('text');
        $handle = popen($command, 'r');
        while (!feof($handle)) {
            $response->withStringBody(fgets($handle))->partialEmit();
        }
        pclose($handle);

        return $response;
    }

    /**
     * Ordre d'arret sur le service beanstalk server
     * @return CoreResponse|CakeResponse
     */
    public function stopService()
    {
        Beanstalk::getInstance()->socketEmit('exit');
        return $this->renderDataToJson('');
    }
}
