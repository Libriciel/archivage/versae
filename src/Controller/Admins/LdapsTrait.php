<?php

/**
 * Versae\Controller\Admins\LdapsTrait
 */

namespace Versae\Controller\Admins;

use Adldap\Adldap;
use Adldap\Models\User;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\FormatError;
use Cake\Http\Response as CakeResponse;
use ErrorException;
use Exception;
use Throwable;
use Versae\Controller\AdminsController;
use Versae\Model\Entity\Ldap;
use Versae\Model\Entity\OrgEntity;

/**
 * Trait LdapsTrait
 *
 * @category Controller\Admins
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin AdminsController
 */
trait LdapsTrait
{
    /**
     * Liste des ldaps
     */
    public function indexLdaps()
    {
        $this->viewBuilder()->setTemplatePath('Admins/Ldaps');
        $this->Ldaps = $this->fetchTable('Ldaps');
        $this->set('tableId', 'index-ldaps-table');
        $this->set('data', $this->Ldaps->find()->order(['name' => 'asc'])->all());
    }

    /**
     * Action d'ajout
     */
    public function addLdap()
    {
        $this->viewBuilder()->setTemplatePath('Admins/Ldaps');
        $this->Ldaps = $this->fetchTable('Ldaps');
        $entity = $this->Ldaps->newEntity([], ['validate' => false]);

        if ($this->getRequest()->is('post')) {
            $data = $this->getRequest()->getData();
            $this->Ldaps->patchEntity($entity, $data);
            $entity->set('custom_options', $this->getRequest()->getData('custom_options') ?: []);
            $this->loadComponent('AsalaeCore.Modal');
            if ($this->Ldaps->save($entity)) {
                $this->Modal->success();
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->set('entity', $entity);
        $this->set('sas', $this->optionsSa()->toArray());
        $this->set('schemas', $this->Ldaps->options('schema'));
        $this->set('versions', $this->Ldaps->options('version'));
        $this->set('custom_options', array_keys($this->Ldaps->options('append_custom_option')));
    }

    /**
     * Action modifier
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function editLdap(string $id)
    {
        $this->viewBuilder()->setTemplatePath('Admins/Ldaps');
        $this->Ldaps = $this->fetchTable('Ldaps');
        $entity = $this->Ldaps->get($id);
        $request = $this->getRequest();
        if ($request->is('put')) {
            $data = $this->getRequest()->getData();
            $opts = [];
            foreach ($request->getData('custom_options', []) as $option => $val) {
                if (defined('LDAP_OPT_' . $option)) {
                    $opts[constant('LDAP_OPT_' . $option)]
                        = in_array($option, Ldap::LDAP_OPTS_INT) ? (int)$val : $val;
                }
            }
            $data['custom_options'] = $opts;
            $this->Ldaps->patchEntity($entity, $data);
            $entity->set('custom_options', $this->getRequest()->getData('custom_options', []));
            $this->loadComponent('AsalaeCore.Modal');
            if ($this->Ldaps->save($entity)) {
                $this->Modal->success();
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('entity', $entity);
        $this->set('sas', $this->optionsSa()->toArray());
        $this->set('schemas', $this->Ldaps->options('schema'));
        $this->set('versions', $this->Ldaps->options('version'));
        $this->set('custom_options', array_keys($this->Ldaps->options('append_custom_option')));
    }

    /**
     * Action supprimer
     * @param string $id
     * @return CakeResponse
     * @throws \Exception
     */
    public function deleteLdap(string $id)
    {
        $this->Ldaps = $this->fetchTable('Ldaps');
        $this->getRequest()->allowMethod('delete');
        $entity = $this->Ldaps->get($id);

        $report = $this->Ldaps->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Donne la liste des utilisateurs d'un ldap
     * @param string $id
     * @return CakeResponse|void
     * @throws Exception
     */
    public function getLdapUsers(string $id)
    {
        $this->Ldaps = $this->fetchTable('Ldaps');
        /** @var Ldap $ldap */
        $ldap = $this->Ldaps->get($id);

        $this->Users = $this->fetchTable('Users');
        $this->loadComponent('Login');
        /** @var Adldap $ad */
        $ad = Utility::get(Adldap::class);
        $ad->addProvider($ldap->getLdapConfig());
        $output = ['results' => [], 'pagination' => ['more' => false]];
        $page = $this->getRequest()->getData('page', 1);
        $perPage = 20;
        $paginateOver = $perPage - 1;

        set_error_handler(
            function ($errno, $errstr, $errfile, $errline) {
                throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
            }
        );
        try {
            $provider = $ad->connect();
            $search = $provider->search()->newQuery();
            $search->sortBy($ldap->get('user_login_attribute'));

            $filter = $ldap->get('ldap_users_filter');
            $search->rawFilter($filter);

            if ($q = $this->getRequest()->getData('q')) {
                $search->whereContains($ldap->get('user_name_attribute'), $q);
                $perPage = 10; // Note: pas de pagination sur les recherches
                $page = 1;
            }
            $paginator = $search->paginate($perPage, $page - 1);

            /** @var User $entry */
            foreach ($paginator as $entry) {
                $username = $entry->getAttribute($ldap->get('user_name_attribute'));
                $username = $username ? current($username) : 'null';
                $id = $entry->getAttribute('cn');
                $user = $this->Users->find()
                    ->where(['ldap_id' => $ldap->get('id'), 'username' => $username])
                    ->disableHydration()
                    ->first();
                if ($user !== null) { // seulement ceux qui ne sont pas déjà assignés
                    continue;
                }
                $output['results'][] = [
                    'id' => current($id),
                    'text' => h($username),
                ];
            }
            if (count($output['results']) >= $paginateOver) {
                $output['pagination']['more'] = true;
            }
        } catch (Throwable) {
            restore_error_handler();
            $this->Flash->error(__("impossible de se connecter au LDAP"));
            return;
        }
        restore_error_handler();
        return $this->renderDataToJson($output);
    }

    /**
     * @param string $id Service d'archive
     * @return CakeResponse|void
     * @throws Exception
     */
    public function importLdapUser(string $id)
    {
        $this->viewBuilder()->setTemplatePath('Admins/Ldaps');
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        /** @var OrgEntity $entity */
        $entity = $this->OrgEntities->find()
            ->where(['OrgEntities.id' => $id])
            ->andWhere(['TypeEntities.code' => 'SA'])
            ->contain(
                [
                    'TypeEntities',
                    'Ldaps',
                ]
            )
            ->firstOrFail();
        $ldaps = [];
        foreach ($entity->get('ldaps') as $ldap) {
            $ldaps[$ldap->get('id')] = [
                'value' => $ldap->get('id'),
                'text' => $ldap->get('name'),
            ];
        }
        $this->set('ldaps', $ldaps);

        $this->Roles = $this->fetchTable('Roles');
        $roles = $this->OrgEntities->findByRoles()
            ->where(
                [
                    'OrgEntities.id' => $id,
                    'Roles.agent_type' => 'person',
                ]
            )
            ->all()
            ->map(
                function ($r) {
                    return [
                        'value' => $r->get('Roles')['id'],
                        'text' => $r->get('Roles')['name'],
                    ];
                }
            )
            ->toArray();
        $this->set('roles', $roles);

        if ($this->getRequest()->is('post')) {
            $this->loadComponent('Login');
            $this->loadComponent('AsalaeCore.Modal');
            $this->Ldaps = $this->fetchTable('Ldaps');
            $this->Users = $this->fetchTable('Users');

            /** @var Ldap $ldap */
            $ldap = $this->Ldaps->find()
                ->where(
                    [
                        'id' => $this->getRequest()->getData('ldap_id'),
                        'org_entity_id' => $id,
                    ]
                )
                ->firstOrFail();
            /** @var Adldap $ad */
            $ad = Utility::get(Adldap::class);
            $ad->addProvider($ldap->getLdapConfig());

            try {
                $provider = $ad->connect();
                $search = $provider->search()->newQuery();
                $search->where('cn', '=', $this->getRequest()->getData('ldap_user'));
                /** @var User $ldapUser */
                $ldapUser = $search->firstOrFail();
                $password = $this->Users->generatePassword();
                $name = $ldapUser->getAttribute($ldap->get('user_name_attribute'));
                $mail = $ldapUser->getAttribute($ldap->get('user_mail_attribute'));
                $user = $this->Users->newEntity(
                    [
                        'username' => current($ldapUser->getAttribute($ldap->get('user_username_attribute'))),
                        'ldap_login' => current($ldapUser->getAttribute($ldap->get('user_login_attribute'))),
                        'name' => $name ? current($name) : null,
                        'email' => $mail ? current($mail) : null,
                        'password' => $password,
                        'confirm-password' => $password,
                        'high_contrast' => false,
                        'active' => true,
                        'role_id' => $this->getRequest()->getData('role_id'),
                        'org_entity_id' => $id,
                        'ldap_id' => $this->getRequest()->getData('ldap_id'),
                    ]
                );
                if ($user->getError('email')) {
                    $user->set('email');
                }

                if ($err = $user->getError('username')) {
                    foreach ($err as $e) {
                        $this->Flash->error($e);
                    }
                    $this->Modal->fail();
                    return;
                }

                $this->loadComponent('AsalaeCore.Modal');
                $success = $this->Users->save($user);

                if ($success) {
                    $this->Modal->success();
                    $json = $user->toArray();
                    $json['role'] = $this->Roles->get($json['role_id'])->toArray();
                    unset($json['password'], $json['confirm-password']);
                    return $this->renderJson(json_encode($json));
                } else {
                    $this->Flash->error(__("Erreur lors de l'importation de l'utilisateur"));
                    $this->Modal->fail();
                    FormatError::logEntityErrors($user);
                }
            } catch (Throwable) {
                $this->Flash->error(__("impossible de se connecter au LDAP"));
                $this->Modal->fail();
            }
        }
    }
}
