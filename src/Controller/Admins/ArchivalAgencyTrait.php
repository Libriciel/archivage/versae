<?php

/**
 * Versae\Controller\Admins\ArchivalAgencyTrait
 */

namespace Versae\Controller\Admins;

use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Utility\FormatError;
use Cake\Datasource\EntityInterface;
use Cake\Http\Response as CakeResponse;
use Cake\ORM\Query;
use Cake\Utility\Hash;
use Exception;
use Psr\Http\Message\ResponseInterface;
use Versae\Controller\AdminsController;
use Versae\Model\Entity\AuthUrl;
use Versae\Model\Entity\OrgEntity;
use Versae\Model\Entity\User;

/**
 * Trait ArchivalAgencyTrait
 *
 * @category Controller\Admins
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin AdminsController
 */
trait ArchivalAgencyTrait
{
    /**
     * Permet d'ajouter / retirer des Services d'archives
     */
    public function indexServicesArchives()
    {
        $this->viewBuilder()->setTemplatePath('Admins/ArchivalAgency');
        if (!$this->isConnected()) {
            return $this->getResponse()->withStringBody(
                __("Le serveur de base de données doit fonctionner pour accéder à cette fonctionnalité")
            );
        }
        $this->getRequest()->allowMethod('ajax');
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $this->Roles = $this->fetchTable('Roles');
        /** @var OrgEntity $entity */
        $data = $this->OrgEntities->find()
            ->where(['TypeEntities.code' => 'SA'])
            ->contain(['TypeEntities'])
            ->order(['OrgEntities.name' => 'asc'])
            ->all()
            ->map(
                function (EntityInterface $e) {
                    $e->set('deletable', $e->get('deletable'));
                    return $e;
                }
            )
            ->toArray();
        $this->set('data', $data);
        $this->set('tableIdServiceArchive', self::TABLE_SERVICE_ARCHIVE);
    }

    /**
     * Ajout d'un service d'archives
     */
    public function addServiceArchive()
    {
        $this->viewBuilder()->setTemplatePath('Admins/ArchivalAgency');
        $request = $this->getRequest();
        $request->allowMethod('ajax');
        $this->ArchivingSystems = $this->fetchTable('ArchivingSystems');
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $this->TypeEntities = $this->fetchTable('TypeEntities');
        $se = $this->OrgEntities->find()
            ->select(['OrgEntities.id'])
            ->contain(['TypeEntities'])
            ->where(['TypeEntities.code' => 'SE'])
            ->firstOrFail();
        $type = $this->TypeEntities->find()
            ->select(['id', 'code'])
            ->where(['code' => 'SA'])
            ->firstOrFail();
        $entity = $this->OrgEntities->newEntity(
            [
                'parent_id' => $se->get('id'),
                'type_entity_id' => $type->get('id'),
                'type_entity' => $type,
            ],
            ['validate' => false]
        );
        $mainExists = $this->OrgEntities->exists(['is_main_archival_agency' => true]);
        $this->set('mainExists', $mainExists);

        if ($request->is('post')) {
            $data = [
                'name' => $request->getData('name'),
                'identifier' => $request->getData('identifier'),
                'parent_id' => $se->get('id'),
                'type_entity_id' => $type->get('id'),
                'type_entity' => $type,
                'ldaps' => [
                    '_ids' => $request->getData('ldaps._ids')
                ],
                'archiving_systems' => [
                    '_ids' => $request->getData('archiving_systems._ids')
                ],
                'archival_agency_id' => null,
            ];
            if (!$mainExists) {
                $data['is_main_archival_agency'] = true;
            }
            if ($max = $this->getRequest()->getData('max_disk_usage_conv')) {
                $data['max_disk_usage'] = $max * (int)$this->getRequest()->getData('mult', 1);
            }
            $this->OrgEntities->patchEntity($entity, $data);
            $this->loadComponent('AsalaeCore.Modal');

            if ($this->OrgEntities->save($entity)) {
                $this->Modal->success();
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->set('entity', $entity);

        $this->Ldaps = $this->fetchTable('Ldaps');
        $query = $this->Ldaps->find()
            ->where(['Ldaps.org_entity_id IS' => null]);
        $ldaps = [];
        /** @var EntityInterface $ldap */
        foreach ($query as $ldap) {
            $ldaps[$ldap->get('id')] = [
                'value' => $ldap->get('id'),
                'text' => $ldap->get('name'),
                'locked' => $ldap->get('removable') ? false : 'locked',
            ];
        }
        $this->set('ldaps', $ldaps);
        $query = $this->ArchivingSystems->find()
            ->where(
                [
                    'ArchivingSystems.org_entity_id IS' => null,
                    'ArchivingSystems.active IS' => true,
                ]
            );
        $archivingSystems = [];
        /** @var EntityInterface $archivingSystem */
        foreach ($query as $archivingSystem) {
            $archivingSystems[$archivingSystem->get('id')] = [
                'value' => $archivingSystem->get('id'),
                'text' => $archivingSystem->get('name'),
                'locked' => false,
            ];
        }
        $this->set('archivingSystems', $archivingSystems);
    }

    /**
     * Edition d'un service d'archives
     * @param string $id
     * @throws Exception
     */
    public function editServiceArchive(string $id)
    {
        $this->viewBuilder()->setTemplatePath('Admins/ArchivalAgency');
        $request = $this->getRequest();
        $request->allowMethod('ajax');
        $this->ArchivingSystems = $this->fetchTable('ArchivingSystems');
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $this->Users = $this->fetchTable('Users');
        $this->loadComponent('AsalaeCore.AjaxPaginator');
        /** @var OrgEntity $entity */
        $entity = $this->OrgEntities->find()
            ->where(['OrgEntities.id' => $id])
            ->andWhere(
                [
                    'TypeEntities.code' => 'SA',
                ]
            )
            ->contain(
                [
                    'ArchivingSystems',
                    'Ldaps',
                    'TypeEntities',
                    'Users' => function (Query $q) {
                        return $q
                            ->order(['Users.username' => 'asc'])
                            ->contain(['Roles'])
                            ->limit($this->AjaxPaginator->getConfig('limit'));
                    },
                    'RgpdInfos',
                ]
            )
            ->firstOrFail();

        /** @var User $user */
        foreach ($entity->get('users') as $user) {
            $user->setVirtual(['deletable']);
        }

        $this->set('entity', $entity);
        $countUsers = $this->Users->find()
            ->where(['org_entity_id' => $entity->id])
            ->count();
        $this->set('countUsers', $countUsers);
        $this->set('tableIdUser', self::TABLE_EDIT_USER_SA);
        $rgpdEntity = $entity->get('rgpd_info')
            ?: $this->fetchTable('RgpdInfos')->newEmptyEntity();
        $entity->set('rgpd_info', $rgpdEntity);

        $diskUsage = $entity->get('max_disk_usage');
        if ($diskUsage && (int)$diskUsage % 1024 === 0) {
            $pow = 0;
            while ($diskUsage > 1024) {
                $diskUsage /= 1024;
                if (++$pow >= 5 || $diskUsage % 1024 !== 0) {
                    break;
                }
            }
            $entity->set('mult', pow(1024, $pow));
            $entity->setDirty('mult', false);
        }
        $entity->set('max_disk_usage_conv', $diskUsage);
        $entity->setDirty('max_disk_usage_conv', false);

        if ($request->is('put') && $request->getData('_tab') === 'main') {
            $data = [
                'name' => $request->getData('name'),
                'identifier' => $request->getData('identifier'),
                'type_entity' => $entity->get('type_entity'),
                'type_entity_id' => $entity->get('type_entity_id'),
                'ldaps' => [
                    '_ids' => $request->getData('ldaps._ids')
                ],
                'archiving_systems' => [
                    '_ids' => $request->getData('archiving_systems._ids')
                ],
                'archival_agency_id' => $entity->get('archival_agency_id'),
            ];
            if ($max = $this->getRequest()->getData('max_disk_usage_conv')) {
                $data['max_disk_usage'] = $max * (int)$this->getRequest()->getData('mult', 1);
            }
            $this->loadComponent('AsalaeCore.Modal');
            $conn = $this->OrgEntities->getConnection();
            $conn->begin();
            $this->OrgEntities->patchEntity($entity, $data);
            $success = (bool)$this->OrgEntities->save($entity);
            if ($success) {
                $this->Ldaps = $this->fetchTable('Ldaps');
                $conditions = ['org_entity_id' => $id];
                if ($request->getData('ldaps._ids')) {
                    $conditions['id NOT IN'] = $request->getData('ldaps._ids');
                }
                $this->Ldaps->updateAll(
                    ['org_entity_id' => null],
                    $conditions
                );
                $this->ArchivingSystems = $this->fetchTable('ArchivingSystems');
                $conditions = ['org_entity_id' => $id];
                if ($request->getData('archiving_systems._ids')) {
                    $conditions['id NOT IN'] = $request->getData('archiving_systems._ids');
                }
                $this->ArchivingSystems->updateAll(
                    ['org_entity_id' => null],
                    $conditions
                );
            }
            if ($success) {
                $this->Modal->success();
                $conn->commit();
            } else {
                $this->Modal->fail();
                $conn->rollback();
                FormatError::logEntityErrors($entity);
            }
        } elseif ($request->is(['post', 'put']) && $request->getData('_tab') === 'rgpd') {
            $data = [
                'rgpd_info' => $request->getData('rgpd_info'),
            ];
            if (empty(Hash::filter($data['rgpd_info']))) {
                if ($rgpdId = Hash::get($entity, 'rgpd_info.id')) {
                    $this->fetchTable('RgpdInfos')->deleteAll(['id' => $rgpdId]);
                }
                $success = true;
            } else {
                $this->OrgEntities->patchEntity($entity, $data);
                $success = (bool)$this->OrgEntities->save($entity);
            }
            $this->loadComponent('AsalaeCore.Modal');
            if ($success) {
                $this->Modal->success();
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->Ldaps = $this->fetchTable('Ldaps');
        $query = $this->Ldaps->find()
            ->where(
                [
                    'OR' => [
                        'Ldaps.org_entity_id IS' => null,
                        'Ldaps.org_entity_id' => $id
                    ]
                ]
            );
        $ldaps = [];
        /** @var EntityInterface $ldap */
        foreach ($query as $ldap) {
            $ldaps[$ldap->get('id')] = [
                'value' => $ldap->get('id'),
                'text' => $ldap->get('name'),
                'locked' => $ldap->get('removable') ? false : 'locked',
            ];
        }
        $this->set('ldaps', $ldaps);
        $query = $this->ArchivingSystems->find()
            ->where(
                [
                    'OR' => [
                        'ArchivingSystems.org_entity_id IS' => null,
                        'ArchivingSystems.org_entity_id' => $id
                    ]
                ]
            );
        $archivingSystems = [];
        /** @var EntityInterface $archivingSystem */
        foreach ($query as $archivingSystem) {
            $archivingSystems[$archivingSystem->get('id')] = [
                'value' => $archivingSystem->get('id'),
                'text' => $archivingSystem->get('name'),
                'locked' => $archivingSystem->get('removable') ? false : 'locked',
                'title' => $archivingSystem->get('removable')
                    ? null
                    : __("Ne peut pas être retiré car lié à au moins un formulaire"),
            ];
        }
        $this->set('archivingSystems', $archivingSystems);

        $rolesUsers = $this->OrgEntities->findByRoles($entity)
            ->select(['qexists' => 1], true)
            ->where(['OrgEntities.id' => $id, 'agent_type' => 'person'])
            ->disableHydration()
            ->first();
        $this->set('rolesUsers', (bool)$rolesUsers);
        $rolesWs = $this->OrgEntities->findByRoles($entity)
            ->select(['qexists' => 1], true)
            ->where(['OrgEntities.id' => $id, 'agent_type' => 'software'])
            ->disableHydration()
            ->first();
        $this->set('rolesWs', (bool)$rolesWs);
    }

    /**
     * Ajouter un utilisateur
     * @param string $orgEntityId
     * @return CakeResponse|null
     * @throws Exception
     */
    public function addUser(string $orgEntityId)
    {
        $this->viewBuilder()->setTemplatePath('Admins/ArchivalAgency');
        $this->getRequest()->allowMethod('ajax');

        $this->Users = $this->fetchTable('Users');
        $this->Roles = $this->fetchTable('Roles');
        $role = $this->Roles->find()
            ->select(['id'])
            ->where(['name' => 'Archiviste'])
            ->firstOrFail();
        /** @var User $entity */
        $entity = $this->Users->newEntity(
            [
                'org_entity_id' => $orgEntityId,
                'role_id' => $role->get('id'),
            ]
        );

        if ($this->getRequest()->is('post')) {
            $password = $this->Users->generatePassword();
            $this->Users->patchEntity(
                $entity,
                [
                    'username' => h($this->getRequest()->getData('username')),
                    'password' => $password,
                    'confirm-password' => $password,
                    'name' => h($this->getRequest()->getData('name')),
                    'email' => $this->getRequest()->getData('email'),
                    'high_contrast' => $this->getRequest()->getData('high_contrast'),
                    'active' => true,
                    'org_entity_id' => $orgEntityId, // important pour la validation
                    'agent_type' => 'person',
                ]
            );
            $this->loadComponent('AsalaeCore.Modal');

            if ($this->Users->save($entity)) {
                $this->Modal->success();
                $entity->setVirtual(['deletable', 'is_linked_to_ldap']);
                $json = $entity->toArray();
                $json['role'] = $role->toArray();
                $this->loadComponent('Emails');
                $rep = $this->Emails->submitEmailNewUser($entity);
                if ($rep instanceof AuthUrl) {
                    $json['code'] = $rep->get('code');
                }
                return $this->renderJson(json_encode($json));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('user', $entity);
        $this->set('title', __("Ajouter un utilisateur"));
    }

    /**
     * Défini le service d'archives gestionnaire
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function setMainArchivalAgency(string $id)
    {
        $this->viewBuilder()->setTemplatePath('Admins/ArchivalAgency');
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $entity = $this->OrgEntities->get($id);
        if ($this->getRequest()->is('put')) {
            $this->OrgEntities->patchEntity(
                $entity,
                [
                    'is_main_archival_agency' => true,
                ]
            );
            $this->loadComponent('AsalaeCore.Modal');
            if ($this->OrgEntities->save($entity)) {
                $this->Modal->success();
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->set('entity', $entity);
    }

    /**
     * Pagination des utilisateurs de l'entité
     * @param string $id
     * @return ResponseInterface
     * @throws Exception
     */
    public function paginateUsers(string $id)
    {
        $this->loadComponent('AsalaeCore.AjaxPaginator');
        $this->loadComponent('AsalaeCore.Index', ['model' => 'Users']);
        $this->Users = $this->fetchTable('Users');
        $query = $this->Users->find()
            ->where(['Users.org_entity_id' => $id])
            ->contain(['Roles']);
        $this->Index->setQuery($query)
            ->filter('username', IndexComponent::FILTER_ILIKE);
        return $this->AjaxPaginator->json($query, (bool)$this->getRequest()->getQuery('count'));
    }
}
