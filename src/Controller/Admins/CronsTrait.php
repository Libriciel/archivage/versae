<?php

/**
 * Versae\Controller\Admins\CronsTrait
 */

namespace Versae\Controller\Admins;

use AsalaeCore\Cron\CronInterface;
use AsalaeCore\Factory;
use Cake\Http\Response as CakeResponse;
use Exception;
use Versae\Controller\AdminsController;

/**
 * Trait CronsTrait
 *
 * @category Controller\Admins
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin AdminsController
 */
trait CronsTrait
{

    /**
     * Liste des crons
     * @return CakeResponse
     */
    public function indexCrons()
    {
        $this->viewBuilder()->setTemplatePath('Admins/Crons');
        if (!$this->isConnected()) {
            return $this->getResponse()->withStringBody(
                __("Le serveur de base de données doit fonctionner pour accéder à cette fonctionnalité")
            );
        }
        $this->set('tableId', self::TABLE_INDEX_CRONS);
        $this->Crons = $this->fetchTable('Crons');
        $crons = $this->Crons->find()
            ->order(['Crons.name' => 'asc'])
            ->all()
            ->toArray();
        $this->set('crons', $crons);
    }

    /**
     * Modification d'un cron
     * @param string $id
     * @throws Exception
     */
    public function editCron(string $id)
    {
        $this->viewBuilder()->setTemplatePath('Admins/Crons');
        $this->Crons = $this->fetchTable('Crons');
        $entity = $this->Crons->get($id);
        if ($this->getRequest()->is('put')) {
            $data = [
                'next' => $this->getRequest()->getData('next'),
                'active' => $this->getRequest()->getData('active'),
                // Important: "frequency_build_unit" doit être avant "frequency_build"
                'frequency_build_unit' => $this->getRequest()->getData('frequency_build_unit'),
                'frequency_build' => $this->getRequest()->getData('frequency_build'),
            ];
            if ($this->getRequest()->getData('hidden_locked')) {
                $data['locked'] = $this->getRequest()->getData('locked');
            }
            /** @var string|CronInterface $className */
            $className = $entity->get('classname');
            foreach ($className::getVirtualFields() as $field => $options) {
                $data[$field] = $this->getRequest()->getData($field);
            }
            $this->Crons->patchEntity($entity, $data);
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->save($this->Crons, $entity, $data);
        }
        $this->set('entity', $entity);
    }

    /**
     * Visualisation d'un cron
     * @param string $id
     */
    public function viewCron(string $id)
    {
        $this->viewBuilder()->setTemplatePath('Admins/Crons');
        $this->Crons = $this->fetchTable('Crons');
        $entity = $this->Crons->get($id);
        $this->set('entity', $entity);
    }

    /**
     * Lance le cron en manuel
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function runCron(string $id): CakeResponse
    {
        $this->Crons = $this->fetchTable('Crons');
        $this->Crons->get($id);
        Factory\Utility::get('Exec')
            ->async(CAKE_SHELL, 'AsalaeCore.cron', 'run', (int)$id, ['--context' => 'user-action']);
        return $this->renderDataToJson("done");
    }

    /**
     * Permet d'obtenir des informations sur un cron
     * @param string $id
     * @return CakeResponse
     */
    public function getCronState(string $id): CakeResponse
    {
        $this->Crons = $this->fetchTable('Crons');
        $entity = $this->Crons->get($id);
        return $this->renderDataToJson($entity);
    }
}
