<?php

/**
 * Versae\Controller\KeywordsController
 */

namespace Versae\Controller;

use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Utility\FormatError;
use Cake\Core\Configure;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Database\Expression\QueryExpression;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Response;
use Exception;
use Versae\Model\Entity\Fileupload;
use Versae\Model\Entity\Keyword;
use Versae\Model\Table\FileuploadsTable;
use Versae\Model\Table\KeywordListsTable;
use Versae\Model\Table\KeywordsTable;
use Versae\Model\Table\UsersTable;

/**
 * Mots clés (thésaurus)
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property KeywordsTable $Keywords
 * @property KeywordListsTable $KeywordLists
 * @property UsersTable $Users
 * @property FileuploadsTable $Fileuploads
 */
class KeywordsController extends AppController
{
    use RenderDataTrait;

    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public $paginate = [
        'order' => [
            'Keywords.id' => 'asc'
        ]
    ];

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const TABLE_INDEX = 'keywords-table';
    public const TABLE_UPLOAD = 'import-keywords-table';

    /**
     * Liste des mots clés
     * @param string $keyword_list_id
     * @param string $version
     * @throws Exception
     */
    public function index(string $keyword_list_id, string $version = '')
    {
        $this->loadComponent('AsalaeCore.Index');
        $this->Index->init();

        $this->KeywordLists = $this->fetchTable('KeywordLists');
        $list = $this->KeywordLists->find()
            ->where(
                [
                    'KeywordLists.id' => $keyword_list_id,
                    'org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();
        $this->set('list', $list);
        $existingWorkspace = $this->Keywords->exists(
            [
                'Keywords.keyword_list_id' => $keyword_list_id,
                'Keywords.version' => 0
            ]
        ) || $list->get('version') === 0;
        $this->set('existingWorkspace', $existingWorkspace);
        if ($version === '') {
            $version = $existingWorkspace
                ? 0
                : new IdentifierExpression('KeywordLists.version');
        }
        $this->set('version', is_numeric($version) ? $version : $list->get('version'));
        $query = $this->Keywords->find()
            ->innerJoinWith('KeywordLists')
            ->where(
                [
                    'Keywords.keyword_list_id' => $keyword_list_id,
                    'Keywords.version' => $version,
                ]
            );
        $this->set('count', $query->count()); // avant application des filtres
        $this->Index->setQuery($query)
            ->filter('code', IndexComponent::FILTER_ILIKE)
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter('modified', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('name', IndexComponent::FILTER_ILIKE);

        $data = $this->paginate($query)->toArray();
        $this->set('data', $data);
    }

    /**
     * Ajout d'un mot clé
     * @param string $keyword_list_id
     * @throws Exception
     */
    public function add(string $keyword_list_id)
    {
        $this->KeywordLists = $this->fetchTable('KeywordLists');
        $this->Keywords = $this->fetchTable('Keywords');
        $this->KeywordLists->find()
            ->where(
                [
                    'id' => $keyword_list_id,
                    'org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();

        $entity = $this->Keywords->newEntity([], ['validate' => false]);
        $this->set('entity', $entity);
        if ($this->getRequest()->is('post')) {
            $data = [
                'keyword_list_id' => $keyword_list_id,
                'version' => 0,
                'code' => $this->getRequest()->getData('code'),
                'name' => $this->getRequest()->getData('name'),
                'created_user_id' => $this->userId,
            ];
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->save($this->Keywords, $entity, $data);
        }
    }

    /**
     * Ajout d'un ensemble de clés
     * @param string $keyword_list_id
     * @throws Exception
     */
    public function addMultiple(string $keyword_list_id)
    {
        $this->KeywordLists = $this->fetchTable('KeywordLists');
        $this->Keywords = $this->fetchTable('Keywords');
        $list = $this->KeywordLists->find()
            ->where(
                [
                    'id' => $keyword_list_id,
                    'org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();
        $existingWorkspace = $this->Keywords->exists(
            [
                'Keywords.keyword_list_id' => $keyword_list_id,
                'Keywords.version' => 0
            ]
        ) || $list->get('version') === 0;
        if (!$existingWorkspace) {
            throw new BadRequestException(__("Impossible d'ajouter un mot clé, aucune version technique n'a été crée"));
        }
        $requestData = [];
        foreach ($this->getRequest()->getData() as $values) {
            if (empty($values['name'])) {
                continue;
            }
            $requestData[] = [
                'keyword_list_id' => $keyword_list_id,
                'version' => 0,
                'name' => $values['name'],
                'code' => $values['code'],
            ];
        }
        $entities = $this->Keywords->newEntities($requestData);
        $this->set('entities', $entities);

        if ($this->getRequest()->is('post')) {
            $data = array_filter(
                array_map(
                    function ($v) use ($keyword_list_id) {
                        if (empty($v['name'])) {
                            return null;
                        }
                        return [
                            'keyword_list_id' => $keyword_list_id,
                            'version' => 0,
                            'code' => $v['code'],
                            'name' => $v['name'],
                            'created_user_id' => $this->userId
                        ];
                    },
                    $this->getRequest()->getData()
                )
            );
            $this->loadComponent('AsalaeCore.Modal');

            $response = $this->getResponse();
            /** @var EntityInterface $entity Model->patchEntities() fonctionne mal (n'applique pas app_meta) */
            foreach ($entities as $entity) {
                $shift = array_shift($data);
                $entity->set($shift);
            }
            if ($this->Keywords->saveMany($entities)) {
                $body = $response->getBody();
                $body->write(json_encode($entities));
                $response = $response->withBody($body)
                    ->withType('json')
                    ->withHeader('X-Asalae-Success', 'true');
                $this->disableAutoRender()->setResponse($response);
            } else {
                $this->Modal->fail();
            }
        }
    }

    /**
     * Edition d'un mot clé
     * @param string $id
     * @throws Exception
     */
    public function edit(string $id)
    {
        $this->KeywordLists = $this->fetchTable('KeywordLists');
        $this->Keywords = $this->fetchTable('Keywords');
        /** @var Keyword $entity */
        $entity = $this->Keywords->find()
            ->innerJoinWith('KeywordLists')
            ->where(
                [
                    'Keywords.id' => $id,
                    'KeywordLists.org_entity_id' => $this->archivalAgencyId,
                    'Keywords.version' => 0,
                ]
            )
            ->firstOrFail();
        $this->set('entity', $entity);
        if ($this->getRequest()->is('put')) {
            $data = [
                'keyword_list_id' => $entity->get('keyword_list_id'),
                'version' => 0,
                'code' => $this->getRequest()->getData('code'),
                'name' => $this->getRequest()->getData('name'),
            ];
            $this->Keywords->patchEntity($entity, $data);
            $entity->appendModified($this->userId);
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->save($this->Keywords, $entity);
        }
    }

    /**
     * Suppression d'un mot clé
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete(string $id)
    {
        $this->Keywords = $this->fetchTable('Keywords');
        $entity = $this->Keywords->find()
            ->innerJoinWith('KeywordLists')
            ->where(
                [
                    'Keywords.id' => $id,
                    'KeywordLists.org_entity_id' => $this->archivalAgencyId,
                    'Keywords.version' => 0,
                ]
            )
            ->firstOrFail();

        $report = $this->Keywords->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Visualisation d'un mot clé
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        $this->Keywords = $this->fetchTable('Keywords');
        $this->Users = $this->fetchTable('Users');
        $entity = $this->Keywords->find()
            ->innerJoinWith('KeywordLists')
            ->where(
                [
                    'Keywords.id' => $id,
                    'KeywordLists.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();
        $this->set('entity', $entity);

        $query = $this->Users->find()
            ->select(['Users.username', 'Users.name'])
            ->where(['Users.id' => $entity->get('created_user_id')]);
        $lastEditUserId = $entity->get('modified_user_id');
        if (!empty($lastEditUserId)) {
            $query->select(['EditUser.name', 'EditUser.username'])
                ->innerJoin(
                    ['EditUser' => 'users'],
                    [
                        'EditUser.id' => $lastEditUserId
                    ]
                );
        }
        $user = $query->first();
        if (empty($user)) {
            $user = $this->Users->newEntity([], ['validate' => false]);
        }
        $this->set('users', $user);
    }

    /**
     * Import de mots clés par skos
     * @param string $keyword_list_id
     * @return Response
     * @throws Exception
     */
    public function import(string $keyword_list_id)
    {
        $this->KeywordLists = $this->fetchTable('KeywordLists');
        $this->Keywords = $this->fetchTable('Keywords');
        $list = $this->KeywordLists->find()
            ->where(
                [
                    'id' => $keyword_list_id,
                    'org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();
        $existingWorkspace = $this->Keywords->exists(
            [
                'Keywords.keyword_list_id' => $keyword_list_id,
                'Keywords.version' => 0
            ]
        ) || $list->get('version') === 0;
        if (!$existingWorkspace) {
            throw new BadRequestException(__("Impossible d'ajouter un mot clé, aucune version technique n'a été crée"));
        }
        $this->set('list', $list);
        $this->set('uploadId', self::TABLE_UPLOAD);

        if ($this->getRequest()->is('post')) {
            // On récupère le fichier d'import et on définit son type
            $this->Fileuploads = $this->fetchTable('Fileuploads');
            $fileupload_id = $this->getRequest()->getData('fileupload_id');
            /** @var Fileupload $file */
            $file = $this->Fileuploads->get($fileupload_id);
            $ext = $file->get('ext');
            if (in_array($ext, ['rdf', 'csv'])) {
                $type = $ext;
            } else {
                $type = strpos($file->get('mime'), 'xml') ? 'rdf' : 'csv';
            }

            // On crée la liste d'entités selon le type d'import
            if ($type === 'rdf') {
                $imports = $this->Keywords->importSkos(
                    file_get_contents($file->get('path')),
                    $keyword_list_id,
                    $this->userId
                );
            } else {
                $imports = $this->Keywords->importCsv(
                    $file->get('content'),
                    $keyword_list_id,
                    $this->userId,
                    $this->getRequest()->getData('Csv')
                );
                $file->fclose();
            }

            $conn = $this->Keywords->getConnection();
            $conn->begin();

            // S'il y a des conflits (noms identiques), ont les résouds selon la stratégie choisie
            $this->solveImportConflicts($keyword_list_id, $imports);

            // équivalent à un saveMany sans transaction
            $success = true;
            foreach ($imports as $entity) {
                if (!$this->Keywords->save($entity)) {
                    $success = false;
                    FormatError::logEntityErrors($entity);
                    break;
                }
            }

            $this->loadComponent('AsalaeCore.Modal');
            if ($success) {
                $conn->commit();
                $this->Modal->success();
                return $this->renderDataToJson(['success' => true, 'count' => count($imports)]);
            } else {
                $conn->rollback();
                $this->Modal->fail();
            }
        }
    }

    /**
     * S'il y a des conflits (noms identiques), on les résout selon la stratégie choisie
     * @param int|string $keyword_list_id
     * @param array      $imports
     * @throws Exception
     */
    private function solveImportConflicts($keyword_list_id, array &$imports)
    {
        $solver = $this->getRequest()->getData('conflicts');
        if (!$solver) {
            return;
        }
        /** @var Keyword $import */
        foreach ($imports as $key => $import) {
            $invalids = array_keys($import->getInvalid());
            if (!$invalids) {
                continue;
            }
            switch ($solver) {
                case 'overwrite':
                    $this->solveImportConflictsWithOverwrite($invalids, $import, $keyword_list_id);
                    break;
                case 'ignore':
                    unset($imports[$key]);
                    break;
                case 'rename':
                    $this->solveImportConflictsWithRename($invalids, $import, $keyword_list_id);
                    break;
            }
        }
    }

    /**
     * Permet d'obtenir des informations sur un import avant d'effectuer le dit import
     * @param string $keyword_list_id
     * @param string $fileupload_id
     * @throws Exception
     */
    public function getImportFileInfo(string $keyword_list_id, string $fileupload_id)
    {
        $this->viewBuilder()->setLayout('ajax');
        $this->Fileuploads = $this->fetchTable('Fileuploads');
        $this->Keywords = $this->fetchTable('Keywords');
        /** @var Fileupload $file */
        $file = $this->Fileuploads->get($fileupload_id);

        $ext = $file->get('ext');
        if (in_array($ext, ['rdf', 'csv'])) {
            $type = $ext;
        } else {
            $type = strpos($file->get('mime'), 'xml') ? 'rdf' : 'csv';
        }
        $this->set('type', $type);

        if ($type === 'rdf') {
            $imports = $this->Keywords->importSkos(
                file_get_contents($file->get('path')),
                $keyword_list_id,
                null
            );
        } else {
            $imports = $this->Keywords->importCsv(
                $file->get('content'),
                $keyword_list_id,
                $this->userId,
                $this->getRequest()->getData('Csv', [])
            );
            $file->fclose();
        }
        $count = count($imports);
        $conn = $this->Keywords->getConnection();
        $conn->begin();
        $this->solveImportConflicts($keyword_list_id, $imports);

        $this->set('count', $count);
        $this->set('imports', array_slice($imports, 0, 5));

        $conn->rollback();

        $conflicts = false;
        /** @var Keyword $import */
        foreach ($imports as $import) {
            if ($import->getErrors()) {
                $conflicts = true;
                break;
            }
        }
        $this->set('conflicts', $conflicts);

        if ($this->getRequest()->is('post')) {
            $this->viewBuilder()->setTemplate('test_import');
        }
    }

    /**
     * Effectue la recherche des options d'un select de mot clés
     * @throws Exception
     */
    public function populateSelect()
    {
        $this->viewBuilder()->setLayout('ajax');
        $this->Keywords = $this->fetchTable('Keywords');
        $this->paginate['limit'] = Configure::read('Pagination.limit', 20);

        $query = $this->Keywords
            ->find(
                'list',
                [
                    'keyField' => $this->getRequest()->getQuery('key') === 'content' ? 'name' : 'id',
                    'valueField' => function (EntityInterface $entity) {
                        return $entity->get('name') . ' - ' . $entity->get('code');
                    },
                    'groupField' => function (EntityInterface $entity) {
                    /** @var EntityInterface $join */
                        $join = $entity->get('_matchingData')['KeywordLists'];
                        return $join->get('name');
                    },
                ]
            )->select(
                [
                    'Keywords.id',
                    'Keywords.code',
                    'Keywords.name',
                    'KeywordLists.name',
                ]
            )
            ->innerJoinWith('KeywordLists')
            ->where(
                [
                    'KeywordLists.org_entity_id' => $this->archivalAgencyId,
                    'KeywordLists.active' => true,
                    'KeywordLists.version' => new IdentifierExpression('Keywords.version'),
                    'KeywordLists.version >=' => 1
                ]
            )
            ->limit($this->paginate['limit']);

        $this->loadComponent('AsalaeCore.Condition');
        $value = $this->getRequest()->getData('term');
        if ($value) {
            $query->where(
                [
                    'OR' => array_merge(
                        $this->Condition->ilike('Keywords.name', '%' . $value . '%'),
                        $this->Condition->ilike('Keywords.code', '%' . $value . '%')
                    )
                ]
            );
            $orderName = $this->Condition->ilike('Keywords.name', pg_escape_string($value) . '%');
            $orderCode = $this->Condition->ilike('Keywords.code', pg_escape_string($value) . '%');
            // Donne un <ORDER BY (Keywords.code ILIKE 'my_word%') desc>
            // -> Les mots qui commencent par 'my_word' en 1er
            $query->order(
                new QueryExpression(
                    '(' . array_keys($orderName)[0] . ' \'' . current($orderName) . '\') desc'
                )
            );
            $query->order(
                new QueryExpression(
                    '(' . array_keys($orderCode)[0] . ' \'' . current($orderCode) . '\') desc'
                )
            );
        }
        $query->order(
            [
                'KeywordLists.name' => 'asc',
                'Keywords.code' => 'asc',
            ]
        );
        $page = $this->getRequest()->getData('page', 1);
        if ($page > 1) {
            $query->offset($this->paginate['limit'] * ($page - 1));
        }
        $data = $query->all()->toArray();
        $output = ['results' => [], 'pagination' => ['more' => false]];
        if ($this->getRequest()->getQuery('include_search') && $value) {
            $output['results'][] = [
                'id' => $value,
                'text' => h($value)
            ];
        }
        if ($query->count() > $this->paginate['limit'] * $page) {
            $output['pagination']['more'] = true;
        }
        foreach ($data as $group => $values) {
            $formatedValues = [];
            foreach ($values as $key => $value) {
                $formatedValues[] = [
                    'id' => $key,
                    'text' => h($value),
                ];
            }
            $output['results'][] = [
                'id' => 0,
                'text' => h($group),
                'children' => $formatedValues
            ];
        }

        return $this->renderDataToJson($output);
    }

    /**
     * Donne les infos liés à un mot clé
     * @param string $id
     * @return Response
     */
    public function getData(string $id)
    {
        $this->viewBuilder()->setLayout('ajax');
        $this->Keywords = $this->fetchTable('Keywords');

        $keyword = $this->Keywords->find()
            ->where(['Keywords.id' => $id])
            ->contain(['KeywordLists'])
            ->firstOrFail();
        return $this->renderDataToJson($keyword);
    }

    /**
     * En cas de conflit lors d'un import, on écrase les anciennes données
     * @param array   $invalids
     * @param Keyword $import
     * @param int     $keyword_list_id
     * @return void
     */
    private function solveImportConflictsWithOverwrite(
        array $invalids,
        Keyword $import,
        int $keyword_list_id
    ) {
        $base = [
            'keyword_list_id' => $keyword_list_id,
            'version' => 0,
        ];
        $conditions = [
            'Keywords.keyword_list_id' => $keyword_list_id,
            'Keywords.version' => 0,
        ];
        $data = [];
        foreach ($invalids as $invalid) {
            $data[$invalid] = $import->getInvalidField($invalid);
            $import->unset($invalid);
            $this->Keywords->deleteAll(
                $conditions + [
                    'Keywords.' . $invalid => $import->getInvalidField($invalid)
                ]
            );
        }
        $this->Keywords->patchEntity($import, $data + $base);
    }

    /**
     * En cas de conflit lors d'un import, on renomme les données
     * @param array   $invalids
     * @param Keyword $import
     * @param int     $keyword_list_id
     * @return void
     * @throws Exception
     */
    private function solveImportConflictsWithRename(
        array $invalids,
        Keyword $import,
        int $keyword_list_id
    ) {
        $base = [
            'keyword_list_id' => $keyword_list_id,
            'version' => 0,
        ];
        $i = 1;
        $old = [];
        foreach ($invalids as $invalid) {
            $old[$invalid] = $import->getInvalidField($invalid);
        }
        do {
            $data = [];
            foreach ($invalids as $invalid) {
                $data[$invalid] = $old[$invalid] . '_' . $i;
            }
            $this->Keywords->patchEntity($import, $data + $base);
            $i++;
            if ($i > 100) {
                throw new Exception(__("Un problème a eu lieu, plus de 100 mots clés ont le même nom"));
            }
        } while ($invalids = array_keys($import->getInvalid()));
    }
}
