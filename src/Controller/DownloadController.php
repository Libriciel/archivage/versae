<?php

/**
 * Versae\Controller\DownloadController
 */

namespace Versae\Controller;

use Versae\Model\Entity\Fileupload;
use Versae\Model\Table\FileuploadsTable;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Response;
use Exception;

/**
 * Download de fichiers
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property FileuploadsTable $Fileuploads
 */
class DownloadController extends AppController
{
    use CachableTrait;

    /**
     * Permet de lire un fichier uploadé
     *
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function file(string $id)
    {
        $this->Fileuploads = $this->fetchTable('Fileuploads');
        $query = $this->Fileuploads->find()
            ->where(['Fileuploads.id' => $id]);
        if (!$this->getRequest()->getSession()->read('Admin.tech')) {
            $query
                ->innerJoinWith('Users')
                ->innerJoinWith('Users.OrgEntities')
                ->innerJoinWith('Users.OrgEntities.ArchivalAgencies')
                ->where(['ArchivalAgencies.id' => $this->archivalAgencyId]);
        }
        $file = $query->firstOrFail();
        return $this->getResponse()->withHeader('Content-Description', 'File Transfer')
            ->withHeader('Content-Type', $file->get('mime') ?: 'application/octet-stream')
            ->withHeader('Content-Disposition', 'attachment; filename="' . $file->get('name') . '"')
            ->withHeader('Expires', '0')
            ->withHeader('Cache-Control', 'must-revalidate')
            ->withHeader('Pragma', 'public')
            ->withHeader('Content-Length', $file->get('size'))
            ->withFile($file->get('path'), ['download' => true, 'name' => basename($file->get('name'))]);
    }

    /**
     * Permet de "lire" un fichier directement dans le navigateur
     *
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function open(string $id)
    {
        $this->Fileuploads = $this->fetchTable('Fileuploads');
        /** @var Fileupload $file */
        $entity = $this->Fileuploads->find()
            ->innerJoinWith('Users')
            ->innerJoinWith('Users.OrgEntities')
            ->innerJoinWith('Users.OrgEntities.ArchivalAgencies')
            ->where(
                [
                    'Fileuploads.id' => $id,
                    'ArchivalAgencies.id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();
        $mime = $entity->get('mime');
        if (preg_match('/^image\/.*$/', $mime)) {
            return $this->file($id);
        } elseif (preg_match('/^application\/pdf$/', $mime)) {
            $this->viewBuilder()->enableAutoLayout(false);
            $this->viewBuilder()->setTemplate('pdf');
            $this->set('id', $id);
        } elseif (preg_match('/^application\/vnd\.oasis\.opendocument\..*$/', $mime)) {
            $this->redirect('/webroot/js/ViewerJS/index.html#redirect_' . $id . '.odt');
        } else {
            throw new BadRequestException("Pas de lecteur trouvé pour le fichier. type mime: " . $mime);
        }
    }

    /**
     * Mock de la fonction (versae n'a pas de plugin de traitement d'image)
     *
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function thumbnailImage(string $id)
    {
        return $this->file($id);
    }
}
