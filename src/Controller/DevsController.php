<?php

/**
 * Versae\Controller\DevsController
 */

namespace Versae\Controller;

use AsalaeCore\Controller\RenderDataTrait;
use Cake\Core\Configure;
use Cake\Http\Response;
use Cake\ORM\TableRegistry;
use Cake\View\View;
use Exception;
use Sabberworm\CSS\CSSList\Document as SabberwormCSSDocument;
use Sabberworm\CSS\Parser as SabberwormCSSParser;
use Sabberworm\CSS\Parsing\SourceException;
use Sabberworm\CSS\Rule\Rule as SabberwormCSSRule;
use Sabberworm\CSS\Value\CSSString as SabberwormCSSValueString;
use Versae\Exception\GenericException;
use Versae\Model\Table\AcosTable;
use Versae\Model\Table\ArosAcosTable;
use Versae\Model\Table\ArosTable;
use Versae\Model\Table\RolesTable;
use Versae\View\Helper\TranslateHelper;

/**
 * Controller uniquement destiné au développement de versae
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AcosTable $Acos
 * @property ArosTable $Aros
 * @property ArosAcosTable $Permissions
 * @property RolesTable $Roles
 */
class DevsController extends AppController
{
    use RenderDataTrait;

    /**
     * liste les permissions des roles communs
     * @throws Exception
     */
    public function permissions()
    {
        $this->viewBuilder()->setLayout('not_connected');
        $exportFile = Configure::read('Devs.export_roles_path', RESOURCES . 'export_roles.json');
        $exportJson = json_decode(file_get_contents($exportFile), true);
        $pathToControllersJson = Configure::read(
            'App.paths.controllers_rules',
            RESOURCES . 'controllers.json'
        );
        $controllerJson = json_decode(file_get_contents($pathToControllersJson), true);
        $data = [];
        $translator = new TranslateHelper(new View());
        foreach ($controllerJson as $controller => $actions) {
            foreach ($actions as $action => $params) {
                if (($params['invisible'] ?? '1') === '1' || ($params['commeDroits'] ?? false)) {
                    continue;
                }
                $path = "root/controllers/$controller/$action";
                $tr = [
                    'path' => $path,
                    'traduction' => $translator->permission($controller, $action),
                    'controller' => $controller,
                    'action' => $action,
                    'type' => 'controllers',
                    'group' => $params['group'] ?? null,
                ];
                $defined = false;
                foreach ($exportJson as $role => $permissions) {
                    if (isset($permissions[$path])) {
                        $tr[$role] = $permissions[$path] === '1111';
                        $defined = true;
                    } else {
                        $tr[$role] = null;
                    }
                }
                $tr['defined'] = $defined;
                $data[] = $tr;
            }
        }

        // order by type, group, controller, action
        usort(
            $data,
            function ($a, $b) {
                $type = strcmp($b['type'], $a['type']);
                if ($type === 0) {
                    $group = strcmp($a['group'], $b['group']);
                    if ($group === 0) {
                        $controller = strcmp($a['controller'], $b['controller']);
                        if ($controller === 0) {
                            return strcmp($a['action'], $b['action']);
                        }
                        return $controller;
                    }
                    return $group;
                }
                return $type;
            }
        );
        $this->set('data', $data);

        // options
        $this->set('roles', array_keys($exportJson));
    }

    /**
     * Défini une permission pour un role
     * @return Response
     */
    public function setPermission(): Response
    {
        $request = $this->getRequest();
        $request->allowMethod('post');
        $role = $request->getData('role');
        $path = $request->getData('path');
        $value = $request->getData('value');

        $this->Roles = $this->fetchTable('Roles');
        TableRegistry::getTableLocator()->clear();
        // Roles->Aros doit venir de l'appli, alors que Permission->allow doit venir de Acl
        $this->Aros = $this->fetchTable('Aros');
        $this->Permissions = $this->fetchTable('ArosAcos');
        $aro_id = $this->Roles->find()
            ->select(['aro_id' => 'Aros.foreign_key'])
            ->innerJoinWith('Aros')
            ->where(['Roles.name' => $role, 'org_entity_id IS' => null])
            ->firstOrFail()
            ->get('aro_id');
        $aro = [
            'foreign_key' => $aro_id,
            'model' => 'Roles',
        ];

        TableRegistry::getTableLocator()->clear();
        $exportFile = Configure::read('Devs.export_roles_path', RESOURCES . 'export_roles.json');
        $exportJson = json_decode(file_get_contents($exportFile), true);
        if ($value === 'null') {
            unset($exportJson[$role][$path]);
            $this->Permissions->allow($aro, $path, '*', 0);
        } else {
            $exportJson[$role][$path] = $value === 'true' ? '1111' : '0000';
            $this->Permissions->allow($aro, $path, '*', $value === 'true' ? 1 : -1);
            ksort($exportJson[$role]);
        }
        if (!file_put_contents($exportFile, json_encode($exportJson, JSON_PRETTY_PRINT))) {
            throw new GenericException('write error');
        }
        return $this->renderDataToJson('done');
    }

    /**
     * Affiche un email
     * @param string $b64Filename
     * @return Response
     */
    public function debugMail(string $b64Filename)
    {
        $basePath = Configure::read('debug_mails_basepath', TMP . 'debug_mails');
        $filename = base64_decode($b64Filename);
        if (!is_file($basePath . DS . $filename)) {
            return $this->getResponse()
                ->withStatus(404)
                ->withType('txt')
                ->withStringBody(__("Le fichier n'existe plus"));
        }
        return $this->getResponse()
            ->withType('html')
            ->withStringBody(file_get_contents($basePath . DS . $filename));
    }

    /**
     * Liste les icones disponnibles
     * @return void
     * @throws SourceException
     */
    public function icons()
    {
        $css = [
            'font-awesome5.min.css',
            'font-awesome.min.css',
        ];
        $icons = [];
        foreach ($css as $filename) {
            $path = WWW_ROOT . 'css' . DS . $filename;
            if (is_file($path)) {
                $parser = new SabberwormCSSParser(file_get_contents($path));
                $cssDocument = $parser->parse();
                $icons[$filename] = $this->extractIconsFromCssDocument($cssDocument);
            }
        }
        $this->set('icons', $icons);
    }

    /**
     * Donne une liste d'icons
     * @param SabberwormCSSDocument $cssDocument
     * @return array ['fa-yin-yang' => bin, ...]
     */
    private function extractIconsFromCssDocument(SabberwormCSSDocument $cssDocument): array
    {
        $icons = [];
        foreach ($cssDocument->getAllDeclarationBlocks() as $block) {
            /** @var SabberwormCSSRule[] $rules */
            $rules = $block->getRulesAssoc();
            if (!isset($rules['content'])) {
                continue;
            }
            /** @var SabberwormCSSValueString $value */
            $value = $rules['content']->getValue();
            foreach ($block->getSelectors() as $selector) {
                $str = $selector->getSelector();
                if (preg_match('/^\.(fa-[^:]+):before$/', $str, $m)) {
                    $icons[$m[1]] = $value->getString();
                }
            }
        }
        return $icons;
    }
}
