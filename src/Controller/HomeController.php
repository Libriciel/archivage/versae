<?php

/**
 * Versae\Controller\HomeController
 */

namespace Versae\Controller;

use Cake\Utility\Hash;
use DateInterval;
use DateTime;
use Versae\Model\Table\DepositsTable;

/**
 * Accueil / dashboard
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property DepositsTable Deposits
 */
class HomeController extends AppController
{
    /**
     * Accueil
     */
    public function index()
    {
        $this->set('pageTitle', __("accueil"));

        $this->Deposits = $this->fetchTable('Deposits');

        $preparing = $this->Deposits->find()
            ->where(
                [
                    'Deposits.created_user_id' => $this->userId,
                    'Deposits.state' => DepositsTable::S_EDITING,
                ]
            )
            ->order(['Deposits.id' => 'desc'])
            ->contain(['Transfers', 'Forms']);
        $this->set('preparing_count', $preparing->count());
        $this->set('preparing', $preparing->limit(10)->toArray());

        $errors = $this->Deposits->find()
            ->where(
                [
                    'Deposits.created_user_id' => $this->userId,
                    'Deposits.state IN' => [
                        DepositsTable::S_SENDING_ERROR,
                        DepositsTable::S_PREPARING_ERROR,
                        DepositsTable::S_ANSWERING_ERROR,
                        DepositsTable::S_ACKNOWLEDGING_ERROR,
                    ]
                ]
            )
            ->order(['Deposits.id' => 'desc'])
            ->contain(['Transfers', 'Forms']);
        $this->set('errors_count', $errors->count());
        $this->set('errors', $errors->limit(10)->toArray());

        $finished = $this->Deposits->find()
            ->where(
                [
                    'Deposits.created_user_id' => $this->userId,
                    'Deposits.state IN' => [DepositsTable::S_ACCEPTED, DepositsTable::S_REJECTED],
                ]
            )
            ->order(['Deposits.id' => 'desc'])
            ->contain(['Transfers', 'Forms']);
        $this->set('finished_count', $finished->count());
        $this->set('finished', $finished->limit(10)->toArray());

        $dataChartTransfers = [];
        $dataChartTransfersAccepted = [];
        $dataChartTransfersRefused = [];
        $this->Transfers = $this->fetchTable('Transfers');
        $now = new DateTime();
        $sixDays = new DateInterval('P6D');
        $sevenDays = new DateInterval('P7D');
        $sixDaysAgo = (clone $now)->sub($sixDays);
        $sevenDaysAgo = (clone $now)->sub($sevenDays);
        $query = $this->Deposits->find()
            ->innerJoinWith('TransferringAgencies')
            ->where(
                [
                    'Deposits.archival_agency_id' => $this->archivalAgencyId,
                    'Deposits.created >' => $sixDaysAgo->format('Y-m-d')
                ]
            )
            ->order(['Deposits.created' => 'asc']);
        if (Hash::get($this->orgEntity, 'type_entity.code') === 'SA') {
            $query->andWhere(
                [
                    'TransferringAgencies.lft >=' => $this->archivalAgency->get('lft'),
                    'TransferringAgencies.rght <=' => $this->archivalAgency->get('rght'),
                ]
            );
        } elseif (Hash::get($this->user, 'role.hierarchical_view')) {
            $query->andWhere(
                [
                    'TransferringAgencies.lft >=' => $this->orgEntity->get('lft'),
                    'TransferringAgencies.rght <=' => $this->orgEntity->get('rght'),
                ]
            );
        } else {
            $query->andWhere(['TransferringAgencies.id' => $this->orgEntityId]);
        }

        $oneDay = new DateInterval('P1D');
        while ($sevenDaysAgo < $now) {
            $sevenDaysAgo->add($oneDay);
            $day = (int)$sevenDaysAgo->format('d');
            $dataChartTransfers[$day] = 0;
            $dataChartTransfersAccepted[$day] = 0;
            $dataChartTransfersRefused[$day] = 0;
        }
        $dataChartRatio = [0, 0, 0]; // ["acceptés", "refusés", "en cours"]
        foreach ($query as $data) {
            /** @var DateTime $edate */
            $edate = $data['created'];
            $day = (int)$edate->format('d');
            if (!isset($dataChartTransfers[$day])) {
                continue;
            }

            if ($data['state'] === DepositsTable::S_ACCEPTED) {
                $dataChartTransfersAccepted[$day]++;
                $dataChartRatio[0]++;
            } elseif ($data['state'] === DepositsTable::S_REJECTED) {
                $dataChartRatio[1]++;
                $dataChartTransfersRefused[$day]++;
            } else {
                $dataChartTransfers[$day]++;
                $dataChartRatio[2]++;
            }
        }

        $this->set('dataChartTransfers', $dataChartTransfers);
        $this->set('dataChartTransfersAccepted', $dataChartTransfersAccepted);
        $this->set('dataChartTransfersRefused', $dataChartTransfersRefused);
        $this->set('dataChartRatio', $dataChartRatio);
    }
}
