<?php

/**
 * Versae\Controller\TasksController
 */

namespace Versae\Controller;

use Versae\Model\Table\BeanstalkJobsTable;
use Versae\Model\Table\OrgEntitiesTable;
use Versae\Model\Table\UsersTable;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Factory;
use Beanstalk\Model\Table\BeanstalkWorkersTable;
use Beanstalk\Command\WorkerCommand;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response;
use Cake\Http\Response as CakeResponse;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use Exception;
use Pheanstalk\PheanstalkInterface;

/**
 * Jobs en cours de l'utilisateur
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property BeanstalkJobsTable BeanstalkJobs
 * @property BeanstalkWorkersTable BeanstalkWorkers
 * @property OrgEntitiesTable OrgEntities
 * @property UsersTable Users
 */
class TasksController extends AppController
{
    use RenderDataTrait;

    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public $paginate = [
        'order' => [
            "job_state = 'pending'" => 'desc',
            'priority' => 'asc',
            'BeanstalkJobs.id' => 'asc'
        ],
    ];

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const TABLE_ADMIN = 'all-jobs';
    public const TABLE_INDEX = 'index-jobs-table';

    /**
     * Liste des taches en cours/à faire
     */
    public function index()
    {
        $this->loadComponent('AsalaeCore.Index', ['model' => 'BeanstalkJobs']);
        $this->Index->init();

        $this->BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $query = $this->BeanstalkJobs->find()
            ->where(['user_id' => $this->userId]);
        $this->Index->setQuery($query)
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('job_state', IndexComponent::FILTER_IN)
            ->filter('tube', IndexComponent::FILTER_IN);

        try {
            /** @var EntityInterface[] $all */
            $all = $this->paginate($query)->toArray();
        } catch (NotFoundException) {
            $this->Flash->error(
                __("La page demandé n'existe plus, retour à la page 1")
            );
            return $this->redirect(
                ['controller' => 'Tasks', 'action' => __FUNCTION__]
            );
        }

        $this->set('all', $all);

        // counts
        $countReady = $this->BeanstalkJobs->find()
            ->where(
                [
                    'user_id' => $this->userId,
                    'job_state IN' => [
                        BeanstalkJobsTable::S_WORKING,
                        BeanstalkJobsTable::S_PENDING,
                    ],
                ]
            )
            ->count();
        $this->set('countReady', $countReady);
        $countFailed = $this->BeanstalkJobs->find()
            ->where(
                [
                    'user_id' => $this->userId,
                    'job_state' => BeanstalkJobsTable::S_FAILED,
                ]
            )
            ->count();
        $this->set('countFailed', $countFailed);

        // Options
        $this->set('states', $this->BeanstalkJobs->options('job_state'));
        $workerParams = Configure::read('Beanstalk.workers');
        $workerTubes = array_keys($workerParams);
        $this->set('tubes', array_combine($workerTubes, $workerTubes));
    }

    /**
     * Liste des taches en cours/à faire
     */
    public function admin()
    {
        $this->loadComponent('AsalaeCore.Index', ['model' => 'BeanstalkJobs']);
        $this->Index->init();

        $this->BeanstalkJobs = $this->fetchTable('BeanstalkJobs');

        $sa = $this->archivalAgency;

        $query = $this->BeanstalkJobs->find()
            ->innerJoinWith('Users')
            ->innerJoinWith('Users.OrgEntities')
            ->where(
                [
                    'OrgEntities.lft >=' => $sa->get('lft'),
                    'OrgEntities.rght <=' => $sa->get('rght'),
                    'tube IN' => array_keys(Configure::read('Beanstalk.workers', ['false'])),
                ]
            )
            ->contain(['Users']);
        $this->Index->setQuery($query)
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('job_state', IndexComponent::FILTER_IN)
            ->filter('tube', IndexComponent::FILTER_IN)
            ->filter('user_id', IndexComponent::FILTER_IN);

        try {
            /** @var EntityInterface[] $all */
            $all = $this->paginate($query)->toArray();
        } catch (NotFoundException) {
            $this->Flash->error(
                __("La page demandé n'existe plus, retour à la page 1")
            );
            return $this->redirect(
                ['controller' => 'Tasks', 'action' => __FUNCTION__]
            );
        }

        $this->set('all', $all);

        // options
        $this->set('job_states', $this->BeanstalkJobs->options('job_state'));
        $tubes = array_keys(WorkerCommand::availablesWorkers());
        $this->set('tubes', array_combine($tubes, $tubes));
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $users = $this->OrgEntities->find(
            'list',
            [
                'keyField' => '_matchingData.Users.id',
                'valueField' => '_matchingData.Users.username',
                'groupField' => 'name',
            ]
        )
            ->select(['OrgEntities.name', 'Users.id', 'Users.username'])
            ->innerJoinWith('Users')
            ->innerJoinWith('ArchivalAgencies')
            ->where(['ArchivalAgencies.id' => $this->archivalAgencyId])
            ->order(['OrgEntities.name' => 'asc', 'Users.username' => 'asc'])
            ->all();
        $this->set('users', $users);
    }

    /**
     * kick le job
     *
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function resume(string $id)
    {
        $this->BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $entity = $this->BeanstalkJobs->find()
            ->where(['id' => $id, 'user_id' => $this->userId])
            ->firstOrFail();
        $this->loadComponent('AsalaeCore.Modal');
        $this->BeanstalkJobs->transitionOrFail($entity, BeanstalkJobsTable::T_RESUME);
        $this->BeanstalkJobs->saveOrFail($entity);
        $this->Modal->success();
        return $this->renderDataToJson($this->BeanstalkJobs->get($id));
    }

    /**
     * bury le job
     *
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function pause(string $id)
    {
        $this->BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $entity = $this->BeanstalkJobs->find()
            ->where(['id' => $id, 'user_id' => $this->userId])
            ->firstOrFail();
        $this->loadComponent('AsalaeCore.Modal');
        $this->BeanstalkJobs->transitionOrFail($entity, BeanstalkJobsTable::T_PAUSE);
        $this->BeanstalkJobs->saveOrFail($entity);
        $this->Modal->success();
        return $this->renderDataToJson($this->BeanstalkJobs->get($id));
    }

    /**
     * delete le job
     *
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function cancel(string $id)
    {
        $this->BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $entity = $this->BeanstalkJobs->find()
            ->where(['id' => $id, 'user_id' => $this->userId])
            ->firstOrFail();
        $this->loadComponent('AsalaeCore.Modal');
        $this->BeanstalkJobs->deleteOrFail($entity);
        $this->Modal->success();

        return $this->renderDataToJson(['report' => 'done']);
    }

    /**
     * delete le job
     *
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function ajaxCancel(string $id)
    {
        if (!$this->getRequest()->is('ajax')) {
            throw new Exception(__("Accès direct interdit"));
        }
        $this->BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $entity = $this->BeanstalkJobs->find()
            ->where(['id' => $id, 'user_id' => $this->userId])
            ->firstOrFail();
        $this->BeanstalkJobs->deleteOrFail($entity);
        $report = "done";

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * bury le job
     *
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function ajaxPause(string $id)
    {
        if (!$this->getRequest()->is('ajax')) {
            throw new Exception(__("Accès direct interdit"));
        }
        $this->BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $entity = $this->BeanstalkJobs->find()
            ->where(['id' => $id, 'user_id' => $this->userId])
            ->firstOrFail();
        $this->BeanstalkJobs->transitionOrFail($entity, BeanstalkJobsTable::T_PAUSE);
        $this->BeanstalkJobs->saveOrFail($entity);
        $report = "done";
        $data = json_encode($entity);

        return $this->renderDataToJson(['report' => $report, 'data' => $data]);
    }

    /**
     * kick le job
     *
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function ajaxResume(string $id)
    {
        if (!$this->getRequest()->is('ajax')) {
            throw new Exception(__("Accès direct interdit"));
        }
        $this->BeanstalkJobs = $this->fetchTable('BeanstalkJobs');

        $entity = $this->BeanstalkJobs->find()
            ->where(['id' => $id, 'user_id' => $this->userId])
            ->firstOrFail();
        $this->BeanstalkJobs->transitionOrFail($entity, BeanstalkJobsTable::T_RESUME);
        $this->BeanstalkJobs->saveOrFail($entity);
        $report = "done";
        $data = json_encode($entity);

        return $this->renderDataToJson(['report' => $report, 'data' => $data]);
    }

    /**
     * Suppression du tube
     *
     * @param string $tubeName
     * @throws Exception
     */
    public function deleteTube(string $tubeName)
    {
        if (!Factory\Utility::get('Beanstalk')->isConnected()) {
            $this->Flash->error(__d('tasks', "Server Beanstalk down"));
            $this->redirect($this->referer());
        }

        $this->fetchTable('BeanstalkJobs')
            ->deleteAll(
                [
                    'tube' => $tubeName,
                    'job_state !=' => BeanstalkJobsTable::S_WORKING,
                ]
            );
        $this->redirect($this->referer());
    }

    /**
     * Ajout d'un job beanstalk
     * Appel addForm | addCheck | addSave selon le cas
     */
    public function add()
    {
        $this->getRequest()->allowMethod('ajax');
        $this->viewBuilder()->setLayout('ajax');

        $this->BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $entity = $this->BeanstalkJobs->newEntity([], ['validate' => false]);

        if ($this->getRequest()->is('post')) {
            $data = $this->getRequest()->getData('data');
            foreach ($this->getRequest()->getData('mix', []) as $d) {
                $data[$d['key']] = $d['value'];
            }
            $Beanstalk = Factory\Utility::get(
                'Beanstalk',
                $this->getRequest()->getData('tube', 'default')
            );
            $this->loadComponent('AsalaeCore.Modal');
            $data = Hash::filter($data);
            try {
                $data['id'] = $Beanstalk->emit(
                    $data,
                    $this->getRequest()->getData(
                        'priority',
                        PheanstalkInterface::DEFAULT_PRIORITY
                    ),
                    $this->getRequest()->getData(
                        'delay',
                        PheanstalkInterface::DEFAULT_DELAY
                    ),
                    $this->getRequest()->getData(
                        'ttr',
                        PheanstalkInterface::DEFAULT_TTR
                    )
                );
                $this->Modal->success();
                $data = $this->BeanstalkJobs->find()
                    ->where(['id' => $data['id']])
                    ->first();
                return $this->renderDataToJson($data);
            } catch (\Exception) {
                $this->Modal->fail();
            }
        }

        $this->set('entity', $entity);
        $this->BeanstalkWorkers = $this->fetchTable('BeanstalkWorkers');
        $tubes = array_keys(WorkerCommand::availablesWorkers());
        $this->set('tubes', array_combine($tubes, $tubes));

        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $query = $this->OrgEntities->find(
            'list',
            [
                'keyField' => '_matchingData.Users.id',
                'valueField' => '_matchingData.Users.username',
                'groupField' => 'name',
            ]
        )
            ->select(['OrgEntities.name', 'Users.id', 'Users.username'])
            ->innerJoinWith('Users');
        if (!$this->getRequest()->getSession()->read('Admin.tech')) {
            $sa = $this->archivalAgency;
            $query->where(
                [
                    'OrgEntities.lft >=' => $sa->get('lft'),
                    'OrgEntities.rght <=' => $sa->get('rght'),
                ]
            );
        }
        $users = $query->order(['OrgEntities.name' => 'asc', 'Users.username' => 'asc'])
            ->all();
        $this->set('users', $users);
    }

    /**
     * modification d'un job beanstalk
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function edit(string $id)
    {
        $this->getRequest()->allowMethod('ajax');
        $this->viewBuilder()->setLayout('ajax');

        $this->BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $entity = $this->BeanstalkJobs->get($id);
        if ($entity->get('job_state') === BeanstalkJobsTable::S_WORKING) {
            throw new ForbiddenException();
        }

        if ($this->getRequest()->is('put')) {
            $data = $this->getRequest()->getData('data');
            foreach ($this->getRequest()->getData('mix', []) as $d) {
                $data[$d['key']] = $d['value'];
            }
            $data = Hash::filter($data);

            $model = $fk = null;
            foreach ($data as $key => $value) {
                if (
                    $key !== 'user_id'
                    && is_numeric($value)
                    && preg_match('/^(\w+)_id$/', $key, $m)
                ) {
                    $modelName = Inflector::camelize(Inflector::pluralize($m[1]));
                    if (get_class($this->fetchTable($modelName)) !== Table::class) {
                        $model = $modelName;
                        $fk = (int)$value;
                        break;
                    }
                }
            }

            $this->BeanstalkJobs->patchEntity(
                $entity,
                [
                    'user_id' => $data['user_id'] ?? null,
                    'data' => $data,
                    'object_model' => $model,
                    'object_foreign_key' => $fk,
                ] + $this->getRequest()->getData()
            );
            $this->loadComponent('AsalaeCore.Modal');
            if ($this->BeanstalkJobs->save($entity)) {
                $this->Modal->success();
                return $this->renderDataToJson($entity);
            } else {
                $this->Modal->fail();
            }
        }

        $this->set('entity', $entity);
        $this->BeanstalkWorkers = $this->fetchTable('BeanstalkWorkers');
        $workerParams = Configure::read('Beanstalk.workers');
        $workerTubes = array_keys($workerParams);
        $this->set('tubes', array_combine($workerTubes, $workerTubes));

        $sa = $this->archivalAgency;
        $conditions = [];
        if ($sa) {
            $conditions = [
                'OrgEntities.lft >=' => $sa->get('lft'),
                'OrgEntities.rght <=' => $sa->get('rght'),
            ];
        }

        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $users = $this->OrgEntities->find(
            'list',
            [
                'keyField' => '_matchingData.Users.id',
                'valueField' => '_matchingData.Users.username',
                'groupField' => 'name',
            ]
        )
            ->select(['OrgEntities.name', 'Users.id', 'Users.username'])
            ->innerJoinWith('Users')
            ->where($conditions)
            ->order(['OrgEntities.name' => 'asc', 'Users.username' => 'asc'])
            ->all();
        $this->set('users', $users);
    }

    /**
     * Permet d'obtenir les infos d'un job au format json
     * @param string $id
     * @return Response
     */
    public function myJobInfo(string $id)
    {
        $this->BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $job = $this->BeanstalkJobs->find()
            ->where(
                [
                    'id' => $id,
                    'user_id' => $this->userId,
                ]
            )
            ->first();
        return $this->renderDataToJson($job);
    }

    /**
     * Permet d'obtenir les infos d'un job au format json (pour les admins)
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function adminJobInfo(string $id)
    {
        $this->BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $sa = $this->archivalAgency;
        $job = $this->BeanstalkJobs->find()
            ->innerJoinWith('Users')
            ->innerJoinWith('Users.OrgEntities')
            ->where(
                [
                    'BeanstalkJobs.id' => $id,
                    'OrgEntities.lft >=' => $sa->get('lft'),
                    'OrgEntities.rght <=' => $sa->get('rght'),
                ]
            )
            ->first();
        return $this->renderDataToJson($job);
    }

    /**
     * Donne le contenu du tableau de jobs
     * @return CakeResponse
     * @throws Exception
     */
    public function getTubesInfo()
    {
        $this->BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $this->BeanstalkWorkers = $this->fetchTable('BeanstalkWorkers');

        $workerParams = Configure::read('Beanstalk.workers');
        $workerTubes = array_keys($workerParams);
        sort($workerTubes);
        $tubes = [];
        foreach ($workerTubes as $tube) {
            $q = $this->BeanstalkJobs->query();
            $jobStates = $this->BeanstalkJobs->find()
                ->select(
                    [
                        'job_state',
                        'count' => $q->func()->count('*'),
                    ]
                )
                ->where(['tube' => $tube])
                ->group(['job_state'])
                ->toArray();
            $workerCount = $this->BeanstalkWorkers->find()
                ->where(['tube' => $tube])
                ->count();
            $tubes[$tube] = [
                'tube' => $tube,
                'workers' => $workerCount,
                BeanstalkJobsTable::S_WORKING => 0,
                BeanstalkJobsTable::S_PENDING => 0,
                BeanstalkJobsTable::S_DELAYED => 0,
                BeanstalkJobsTable::S_PAUSED => 0,
                BeanstalkJobsTable::S_FAILED => 0,
            ];
            foreach ($jobStates as $state) {
                $tubes[$tube][$state->get('job_state')] = $state->get('count');
            }
        }
        sort($tubes);
        return $this->renderDataToJson(array_values($tubes));
    }
}
