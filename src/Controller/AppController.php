<?php

/**
 * Versae\Controller\AppController
 */
namespace Versae\Controller;

use ArrayObject;
use AsalaeCore\Auth\AuthenticationService;
use AsalaeCore\Controller\Component\AclComponent;
use AsalaeCore\Controller\Controller as CoreController;
use AsalaeCore\Datasource\Paginator;
use AsalaeCore\Error\AppErrorHandler;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\AppServerRequest;
use AsalaeCore\Http\Response;
use Authorization\Identity;
use Cake\Controller\Exception\MissingActionException;
use Cake\Core\Configure;
use Cake\Database\Exception as DatabaseException;
use Cake\Datasource\EntityInterface;
use Cake\Event\EventInterface;
use Cake\Http\Response as CakeResponse;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Versae\Controller\Component\LoginComponent;
use Versae\Model\Table\FiltersTable;
use Versae\Model\Table\NotificationsTable;
use Versae\Model\Table\OrgEntitiesTable;
use Versae\Model\Table\SavedFiltersTable;
use Versae\Model\Table\UsersTable;

/**
 * Tous les contrôleurs descendent de AppController
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 * @property AclComponent Acl
 * @property AppServerRequest request
 * @property FiltersTable Filters
 * @property LoginComponent Login
 * @property NotificationsTable Notifications
 * @property Paginator Paginator
 * @property SavedFiltersTable SavedFilters
 * @property UsersTable Users
 */
class AppController extends CoreController
{
    /**
     * @var array Garde en mémoire les variables passés à la vue
     *            (remplace $this->viewVars qui est deprecated)
     */
    public $immutableViewVars = [];
    /**
     * @var null|EntityInterface utilisateur connecté
     */
    protected $user = [];
    /**
     * @var null|int id de l'utilisateur connecté
     */
    protected $userId = null;
    /**
     * @var null|EntityInterface entité de l'utilisateur connecté
     */
    protected $orgEntity = [];
    /**
     * @var null|int id de l'entité de l'utilisateur connecté
     */
    protected $orgEntityId = null;
    /**
     * @var null|EntityInterface service d'archives de l'utilisateur connecté
     */
    protected $archivalAgency = [];
    /**
     * @var null|int id du service d'archives de l'utilisateur connecté
     */
    protected $archivalAgencyId = null;
    /**
     * @var bool vrai si l'utilisateur connecté est un administrateur technique
     */
    protected $adminTech = false;

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     */
    public function initialize(): void
    {
        // Note: nécessaire pour éviter des connexions postgres qui ne se ferment pas (origine du problème non trouvée)
        if (!ob_get_level()) {
            ob_start();
        }
        parent::initialize();
        $request = $this->getRequest();

        $this->loadComponent('RequestHandler', ['enableBeforeRedirect' => false]);
        $this->loadComponent('Flash', ['duplicate' => false]);
        $this->loadComponent('Authentication.Authentication');
        $this->loadComponent('AsalaeCore.Authorization');
        $this->loadComponent('AsalaeCore.Rest');
        $this->Rest->setApiActions(static::getApiActions());
        /** @var AuthenticationService $authenticationService */
        $authenticationService = $this->getRequest()->getAttribute('authentication');
        $authUrl = $authenticationService
            ? $authenticationService->getConfig('unauthenticatedRedirect')
            : '';
        $this->set('authUrl', $authUrl);

        /** @var Identity $identity */
        $identity = $this->getRequest()->getAttribute('identity');
        $user = $identity?->getOriginalData();
        $connected = $user && Hash::get($user, 'id');
        $this->adminTech = $user && Hash::get($user, 'admin');

        // Permet l'accès à l'application sans page de login (permet de profiter des outils d'analyse en ligne
        if (Configure::read("Auth.disabled") && !$connected) {
            $this->Users = $this->fetchTable('Users');
            $user = $this->Users->find()->first()->toArray();
            $connected = true;
            $this->Authentication->setIdentity(new ArrayObject($user));
        }

        // Evite des bugs dans les acls si action en uppercase
        $action = $request->getParam('action');
        if ($action && !in_array($action, get_class_methods($this))) {
            throw new MissingActionException(
                [
                    'controller' => $this->getName() . "Controller",
                    'action' => $request->getParam('action'),
                    'prefix' => $request->getParam('prefix') ?: '',
                    'plugin' => $request->getParam('plugin'),
                ]
            );
        }

        // donne aux reponses ajax l'état de la connexion et le layout
        if ($this->getRequest()->is('ajax')) {
            $this->setResponse(
                $this->getResponse()->withHeader(
                    'X-Connected',
                    $connected ? 'true' : 'false'
                )
            );
            if ($request->getHeaderLine('No-Layout')) {
                $this->viewBuilder()->setLayout('no-layout');
            }
        }

        // FIXME placer dans un middleware
        $this->setResponse(
            $this->getResponse()->withHeader(
                'X-Frame-Options',
                Configure::read('AppController.xFrameOptions', 'deny')
            )
        );

        if ($request->is('json')) {
            // erreurs récupérés dans $this->render()
            AppErrorHandler::disableErrorOutput();
        }

        if ($connected) {
            $this->user = $user;
            $this->userId = Hash::get($user, 'id');
            $this->orgEntity = Hash::get($user, 'org_entity', []);
            $this->orgEntityId = Hash::get($user, 'org_entity_id');
            $this->archivalAgency = Hash::get($this->orgEntity, 'archival_agency', []);
            $this->archivalAgencyId = Hash::get($this->archivalAgency, 'id');

            // cookies
            $this->loadComponent('AsalaeCore.Jstable');
            $this->Jstable->saveCookies();

            Configure::write('ConfigArchivalAgency', $this->getRequest()->getSession()->read('ConfigArchivalAgency'));
        }

        // alerte espace disque faible (< 1Go)
        if (!is_dir(Configure::read('App.paths.data'))) {
            mkdir(Configure::read('App.paths.data'), 0777, true);
        }
        if (
            !$request->is('ajax')
            && (disk_free_space(ROOT) < 1000000000
            || disk_free_space(Configure::read('App.paths.data')) < 1000000000
            || disk_free_space(sys_get_temp_dir()) < 1000000000)
        ) {
            $this->Flash->error(
                __("Espace disque faible, veuillez contacter un administrateur")
            );
        }
        /*
         * Enable the following components for recommended CakePHP security settings.
         * see http://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');
        // FIXME placer dans un middleware
        // l'ip de l'utilisateur sera celle du proxy configuré
        if ($proxy = Configure::read('Proxy.host')) {
            $request->setTrustedProxies([$proxy]);
        }
    }

    /**
     * Surcharge $this->set afin de garder en mémoire les variables
     * dans $this->immutableViewVars
     * @param array|string $name
     * @param null         $value
     */
    public function set($name, $value = null)
    {
        if (is_array($name)) {
            if (is_array($value)) {
                $data = array_combine($name, $value);
            } else {
                $data = $name;
            }
        } else {
            $data = [$name => $value];
        }
        foreach ($data as $key => $value) {
            $this->immutableViewVars[$key] = $value;
        }
        parent::set($data);
    }

    /**
     * Before render callback.
     *
     * @param EventInterface $event An Event instance
     * @return CakeResponse|null|void
     * @throws Exception
     * @link https://book.cakephp.org/4/en/controllers.html#request-life-cycle-callbacks
     */
    public function beforeRender(EventInterface $event)
    {
        $request = $this->getRequest();
        $session = $request->getSession();

        $action = Inflector::underscore($this->getRequest()->getParam('action', ''));
        $type = $this->getResponse()->getType();
        [, $subDir] = explode('/', $type);
        $templates = Configure::read(
            'App.paths.templates.0',
            ROOT . DS . 'templates' . DS
        );
        if (
            in_array($type, ['application/json', 'application/xml'])
            && !file_exists(
                $templates . $this->name . '/' . $subDir . '/' . $action . '.php'
            )
        ) {
            $this->set('viewVars', $this->immutableViewVars);
            $this->viewBuilder()
                ->setTemplatePath('default')
                ->setTemplate('default');
        }

        $notifications = [];
        $hostRgpdInfo = null;
        $entityRgpdInfo = null;
        try {
            $adminData = (array)$session->read('Admin');
            $highContrast = $session->read('Auth.high_contrast');
            $hostRgpdInfo = $this->fetchTable('RgpdInfos')->find()
                ->innerJoinWith('OrgEntities')
                ->innerJoinWith('OrgEntities.TypeEntities')
                ->where(
                    [
                        'TypeEntities.code IN' => OrgEntitiesTable::CODE_OPERATING_DEPARTMENT,
                    ]
                )
                ->orderAsc('OrgEntities.lft')
                ->first();
        } catch (DatabaseException) {
            $adminData = [];
            $highContrast = false;
        }
        $username = Hash::get($this->user ?: [], 'username');
        $path = Configure::read('App.paths.administrators_json');

        if ($this->userId && !$request->is('ajax')) {
            // evite une double exception lors de l'affichage d'erreur en cas d'exception dans les tests
            try {
                $this->Notifications = $this->fetchTable('Notifications');
                $results = $this->Notifications
                    ->find()
                    ->where(['user_id' => $this->userId])
                    ->order(['id' => 'asc'])
                    ->limit(101)
                    ->all();
                $notifications = $results->toArray();

                $entityRgpdInfo = $this->fetchTable('RgpdInfos')->find()
                    ->innerJoinWith('OrgEntities')
                    ->innerJoinWith('OrgEntities.TypeEntities')
                    ->where(
                        [
                            'OrgEntities.lft <=' => $this->orgEntity->get('lft'),
                            'OrgEntities.rght >=' => $this->orgEntity->get('rght'),
                            'TypeEntities.code NOT IN' => OrgEntitiesTable::CODE_OPERATING_DEPARTMENT,
                        ]
                    )
                    ->orderDesc('OrgEntities.lft')
                    ->first();
            } catch (Exception) {
            }

            // Menu perso
            $this->set(
                'userMenu',
                (array)json_decode($session->read('Auth.menu'), true)
            );
        } elseif (Hash::get($adminData, 'tech') && is_writable($path)) {
            // cookies
            $this->loadComponent('AsalaeCore.Cookie');
            $this->Cookie->setConfig('encryption', false);
            $cookies = $request->getCookieParams();
            unset($cookies['PHPSESSID']);
            foreach (array_keys($cookies) as $cookie) {
                $this->Cookie->delete($cookie);
            }

            $oldCookies = Hash::get($adminData, 'data.cookies', []);
            $newCookies = array_filter(
                array_merge($oldCookies, $cookies),
                function ($v) {
                    return !empty(json_decode($v));
                }
            );
            $session->write('Admin.data.cookies', $newCookies);
            $json = json_encode($newCookies);
            $this->cookies = $newCookies;
            if ($request->getHeaderLine('Accept') !== 'application/json') {
                $this->set('cookies', $json);
            }

            $path = Configure::read('App.paths.administrators_json');
            $admins = json_decode(file_get_contents($path)) ?: [];
            $dirty = false;
            foreach ($admins as $values) {
                if (
                    isset($values->username)
                    && Hash::get($adminData, 'data.username') === $values->username
                    && (!isset($values->cookies)
                    || json_encode($values->cookies) !== json_encode($newCookies))
                ) {
                    $values->cookies = $newCookies;
                    $dirty = true;
                    break;
                }
            }
            if ($dirty) {
                /** @var Filesystem $Filesystem */
                $Filesystem = Utility::get('Filesystem');
                try {
                    $Filesystem->begin();
                    $Filesystem->setSafemode(true);
                    $Filesystem->dumpFile($path, json_encode($admins, JSON_PRETTY_PRINT));
                    $Filesystem->commit();
                } catch (Exception) {
                    $Filesystem->rollback();
                }
            }

            // mention rgpd pour l'admin tech = service d'archives gestionnaire
            try {
                $entityRgpdInfo = $this->fetchTable('RgpdInfos')->find()
                    ->innerJoinWith('OrgEntities')
                    ->where(['OrgEntities.is_main_archival_agency IS' => true])
                    ->first();
            } catch (Exception) {
            }
        }
        $this->set('user_id', $this->userId);
        $this->set('username', $username);
        $this->set('saId', $this->archivalAgencyId);
        $this->set('notifications', json_encode($notifications));
        $this->set('high_contrast', $highContrast);
        $this->set(
            'rgpdData',
            [
                'host' => $hostRgpdInfo ?: [],
                'entity' => $entityRgpdInfo ?: [],
            ]
        );

        return null;
    }

    /**
     * Juste pour l'IDE (qui bug)
     * @return Response
     */
    public function getCoreResponse(): Response
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->response;
    }
}
