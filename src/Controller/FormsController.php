<?php

/**
 * Versae\Controller\FormsController
 */

namespace Versae\Controller;

use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Form\MessageSchema\Seda21Schema;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\FormatError;
use Cake\Core\Configure;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Utility\Xml;
use DOMDocument;
use DOMElement;
use DateTime;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Psr\Http\Message\MessageInterface;
use Versae\Exception\GenericException;
use Versae\Form\AddInputForm;
use Versae\Form\FormMakerForm;
use Versae\Form\ImportForm;
use Versae\Model\Entity\FormInput;
use Versae\Model\Table\FileuploadsTable;
use Versae\Model\Table\FormExtractorsTable;
use Versae\Model\Table\FormFieldsetsTable;
use Versae\Model\Table\FormInputsTable;
use Versae\Model\Table\FormTransferHeadersTable;
use Versae\Model\Table\FormVariablesTable;
use Versae\Model\Table\FormsTable;
use Versae\Model\Table\KeywordListsTable;
use Versae\Model\Table\KeywordsTable;
use Versae\Model\Table\OrgEntitiesTable;
use XSLTProcessor;

/**
 * Formulaires
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property FileuploadsTable Fileuploads
 * @property FormExtractorsTable FormExtractors
 * @property FormFieldsetsTable FormFieldsets
 * @property FormInputsTable FormInputs
 * @property FormTransferHeadersTable FormTransferHeaders
 * @property FormVariablesTable FormVariables
 * @property FormsTable Forms
 * @property KeywordListsTable KeywordLists
 * @property KeywordsTable Keywords
 * @property OrgEntitiesTable OrgEntities
 */
class FormsController extends AppController implements ApiInterface
{
    use RenderDataTrait;
    use ApiTrait;

    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public $paginate = [
        'order' => [
            'Forms.id' => 'desc'
        ]
    ];

    /**
     * Liste des actions apis
     * @return string[]
     */
    public static function getApiActions(): array
    {
        return ['testExtractor'];
    }

    /**
     * Liste des formulaires en préparation
     */
    public function indexEditing()
    {
        $subQuery = $this->Forms->query()
            ->select(['id_last_version' => 'id'])
            ->from(['f' => 'forms'])
            ->where(['f.identifier' => new IdentifierExpression('Forms.identifier')])
            ->order(['f.version_number' => 'desc'])
            ->limit(1);
        $query = $this->Forms->find()
            ->where(['Forms.id' => $subQuery]);
        $this->indexCommons($query);

        // options
        $this->ArchivingSystems = $this->fetchTable('ArchivingSystems');
        $archivingSystems = $this->ArchivingSystems->find()
            ->where(
                [
                    'active' => true,
                    'org_entity_id' => $this->archivalAgencyId,
                ]
            );
        $this->set('archivingSystems', $archivingSystems);
        $this->set('availableArchivingSystem', (bool)$archivingSystems->count());
    }

    /**
     * Liste des formulaires publiés
     */
    public function indexPublished()
    {
        $query = $this->Forms->find()
            ->where(['Forms.state' => FormsTable::S_PUBLISHED]);
        $this->indexCommons($query);
    }

    /**
     * Liste de tous les formulaires
     */
    public function indexAll()
    {
        $query = $this->Forms->find();
        $this->indexCommons($query);
    }

    /**
     * Liste les enregistrements
     * @param Query $query
     * @return Response|null
     * @throws Exception
     */
    private function indexCommons(Query $query)
    {
        $this->loadComponent('AsalaeCore.Index');
        $this->Index->init();

        $query->contain(['ArchivingSystems']);
        $query->where(['Forms.org_entity_id' => $this->archivalAgencyId]);
        $this->Index->setQuery($query)
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('name', IndexComponent::FILTER_ILIKE)
            ->filter('version_number', IndexComponent::FILTER_COUNTOPERATOR)
            ->filter('archiving_system_id')
            ->filter('state', IndexComponent::FILTER_IN)
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter(
                'last_version',
                function ($value, Query $q) {
                    $subQuery = $this->Forms->query()
                        ->select(['id_last_version' => 'id'])
                        ->from(['f' => 'forms'])
                        ->where(['f.identifier' => new IdentifierExpression('Forms.identifier')])
                        ->order(['f.version_number' => 'desc'])
                        ->limit(1);
                    return $q->where(['forms.id' => $subQuery]);
                }
            );

        try {
            $data = $this->paginate($query)
                ->map(
                    function (EntityInterface $entity) {
                        $entity->setVirtual(
                            [
                                'deletable',
                                'editable',
                                'not_publishable_reason',
                                'publishable',
                                'statetrad',
                                'unpublishable',
                                'versionable',
                            ]
                        );
                        return $entity;
                    }
                )
                ->toArray();
        } catch (NotFoundException) {
            $this->Flash->error(__("La page demandée n'existe plus, retour à la page 1"));
            return $this->redirect('/forms/index');
        }
        $this->set('data', $data);

        // options
        $this->set('states', $this->Forms->options('state'));

        $this->ArchivingSystems = $this->fetchTable('ArchivingSystems');
        $archivingSystems = $this->ArchivingSystems->find('list')
            ->where(
                [
                    'org_entity_id' => $this->archivalAgencyId,
                    'active' => true,
                ]
            )
            ->order(['name' => 'asc']);
        $this->set('archivingSystemsOptions', $archivingSystems);

        // override IndexComponent
        if ($this->getRequest()->is('ajax') && $this->viewBuilder()->getLayout() === null) {
            $this->viewBuilder()->setTemplate('ajax_index');
        }
    }

    /**
     * Ajout d'un formulaire
     */
    public function add()
    {
        $request = $this->getRequest();
        $entity = $this->Forms->newEmptyEntity();

        if ($request->is('post')) {
            $data = [
                'org_entity_id' => $this->archivalAgencyId,
                'name' => $request->getData('name'),
                'description' => $request->getData('description'),
                'transferring_agencies' => $request->getData('transferring_agencies'),
                'archiving_system_id' => $request->getData('archiving_system_id'),
                'identifier' => uuid_create(UUID_TYPE_TIME),
                'version_number' => 0,
                'version_note' => __("Création du formulaire"),
                'state' => $this->Forms->initialState,
                'user_guide' => $request->getData('user_guide'),
                'seda_version' => $request->getData('seda_version'),
            ];
            $this->loadComponent('AsalaeCore.Modal');
            $this->Forms->patchEntity(
                $entity,
                $data,
                [
                    'associated' => [
                        'TransferringAgencies' => ['fields' => ['_ids']],
                    ],
                ]
            );
            if ($this->Forms->save($entity)) {
                $this->Modal->success();
                $this->Modal->step('actionEditForm(' . $entity->get('id') . ')');
                $this->ArchivingSystems = $this->fetchTable('ArchivingSystems');
                $entity->set('archiving_system', $this->ArchivingSystems->get($entity->get('archiving_system_id')));
                return $this->renderDataToJson($entity->toArray());
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('entity', $entity);

        // options
        $this->ArchivingSystems = $this->fetchTable('ArchivingSystems');
        $archivingSystems = $this->ArchivingSystems->find('list')
            ->where(
                [
                    'active' => true,
                    'org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->order(['name' => 'asc'])
            ->all()
            ->toArray();
        $this->set('archivingSystems', $archivingSystems);

        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->OrgEntities = $this->fetchTable('OrgEntities');
        $ta = $OrgEntities->listOptionsWithoutSeal()
            ->where(
                [
                    'TypeEntities.code IN' => OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES,
                    'OrgEntities.lft >=' => $this->archivalAgency->get('lft'),
                    'OrgEntities.rght <=' => $this->archivalAgency->get('rght'),
                ]
            );
        $this->set('transferringAgencies', $ta->all());
    }

    /**
     * Ajout d'un input au formulaire en javascript
     * @param string|null $type
     * @return Response
     * @throws Exception
     */
    public function addInput(string $type = null)
    {
        if ($type === null) {
            $this->viewBuilder()->setTemplate('add_input1');
            $this->addInput1();
        } else {
            $this->viewBuilder()->setTemplate('add_input2');
            return $this->addInput2($type);
        }
    }

    /**
     * Edition d'un input en javascript
     */
    public function editInput()
    {
        $request = $this->getRequest();
        $request->allowMethod('post');
        $form = new AddInputForm();

        if ($request->getData('is_submit')) {
            $this->loadComponent('AsalaeCore.Modal');
            $data = $request->getData();
            unset($data['example'], $data['_tab']);
            if ($form->execute($data)) {
                $this->Modal->success();
                $data = Hash::filter($form->getData());
                unset($data['is_submit']);
                if (($data['multiple'] ?? null) === '0') {
                    unset($data['multiple']);
                }
                if (($data['required'] ?? null) === '0') {
                    unset($data['required']);
                }
                if (($data['readonly'] ?? null) === '0') {
                    unset($data['readonly']);
                }
                if (($data['multiple'] ?? null) === '0') {
                    unset($data['multiple']);
                }
                return $this->renderDataToJson($data);
            } else {
                $this->Modal->fail();
                FormatError::logFormErrors($form);
            }
        } else {
            $form->setData($request->getData('field', []));
        }

        $this->set('form', $form);
        $this->set('type', $request->getData('field.type'));

        $id = $request->getData('field.id');
        if ($id) {
            $input = TableRegistry::getTableLocator()->get('FormInputs')->get($id);
            $linked = $input->get('linked');
        }
        $this->set('linked', $linked ?? false);

        // options
        $inputs = Hash::extract($request->getData('form', []), 'fieldsets.{n}.inputs.{n}');
        $targets = [];
        $inputsData = [];
        $fieldName = $request->getData('field.name');
        foreach ($inputs as $input) {
            $input = json_decode($input, true);
            if (!$input) {
                continue; // contenu du input_test
            }
            $inputsData[] = $input;
            if ($input['type'] !== FormInputsTable::TYPE_PARAGRAPH && $input['name'] !== $fieldName) {
                $targets[$input['name']] = $input['name'] . (($input['label'] ?? false) ? ' - ' . $input['label'] : '');
            }
        }
        $jsonInputs = array_map('json_decode', $inputs);
        $this->set('jsonInputs', json_encode($jsonInputs));
        $this->set('targets', $targets);
        $this->set('inputsData', $inputsData);

        $this->KeywordLists = $this->fetchTable('KeywordLists');
        $keywordLists = $this->KeywordLists->find('list')
            ->where(
                [
                    'KeywordLists.org_entity_id' => $this->archivalAgencyId,
                    'KeywordLists.active IS' => true,
                    'KeywordLists.version >=' => 1,
                ]
            )
            ->order(['KeywordLists.name'])
            ->toArray();
        $this->set('keywordLists', $keywordLists);
        $this->FormFieldsets = $this->fetchTable('FormFieldsets');
        $this->set('cond_display_selects', $this->FormFieldsets->options('cond_display_select'));
        $this->set('fieldsets', $request->getData('form.fieldsets'));
    }

    /**
     * Etape 1 de l'ajout d'un input en javascript
     * @throws Exception
     */
    private function addInput1()
    {
        $form = new AddInputForm();
        $request = $this->getRequest();
        if ($request->getData('is_submit')) {
            $type = $request->getData('type');
            $copy = $request->getData('copy');
            $data = [
                'type' => $type,
                'copy' => $copy,
            ];
            $this->loadComponent('AsalaeCore.Modal');
            if ($form->execute($data)) {
                $stepUrl = $copy
                    ? sprintf('addFormInput2("%s?name=%s")', $type, addcslashes($copy, '"'))
                    : sprintf('addFormInput2("%s")', $type);
                $this->Modal->success();
                $this->Modal->step($stepUrl);
            } else {
                $this->Modal->fail();
                FormatError::logFormErrors($form);
            }
        }
        $this->set('form', $form);
        $inputs = [];
        foreach ($request->getData('form.fieldsets') ?: [] as $ord => $fieldset) {
            $fieldsetData = [];
            foreach ($fieldset['inputs'] ?? [] as $input) {
                $input = json_decode($input, true);
                if (!$input || $input['type'] === 'paragraph') {
                    continue;
                }
                $fieldsetData[$input['name']] = sprintf(
                    '%s - %s',
                    $input['name'],
                    $input['label'] ?? ''
                );
            }
            if (empty($fieldsetData)) {
                continue;
            }
            $legend = $fieldset['legend']
                ?: __("Section sans titre #{0}", $ord + 1);
            $inputs[$legend] = $fieldsetData;
        }
        $this->set('inputs', $inputs);
    }

    /**
     * Etape 2 de l'ajout d'un input en javascript
     * @param string $type
     * @return \AsalaeCore\Http\Response|Response|void
     * @throws Exception
     */
    private function addInput2(string $type)
    {
        $request = $this->getRequest();
        // permet de conserver les informations issues du formulaire en cas d'échec
        if ($prevData = $request->getData('request')) {
            foreach (json_decode($prevData, true) as $name => $value) {
                if (is_array($value)) {
                    $value = json_encode($value);
                }
                $request = $request->withData($name, $value);
            }
            $request = $request->withoutData('request');
        }
        $type = $request->getData('type') ?: $type;
        $form = new AddInputForm();
        if ($type === 'copy') {
            $this->set('copy', true);
            $targetName = $request->getQuery('name');
            $targetInput = null;
            foreach ($request->getData('form.fieldsets') as $fieldset) {
                foreach ($fieldset['inputs'] ?? [] as $input) {
                    $input = json_decode($input, true);
                    if ($input['name'] === $targetName) {
                        $targetInput = $input;
                        break 2;
                    }
                }
            }
            if (!$targetInput) {
                throw new NotFoundException('target input was not found');
            }
            $type = $targetInput['type'];
            $form->setData($targetInput);
        } else {
            $form->setData(
                ['type' => $type]
                + ($type === FormInputsTable::TYPE_SELECT ? ['select2' => true] : [])
            );
        }
        if ($request->getData('is_submit')) {
            $this->loadComponent('AsalaeCore.Modal');
            $data = ['type' => $type] + $request->getData();
            unset($data['example']);
            if ($form->execute($data)) {
                $this->Modal->success();
                $data = Hash::filter($form->getData() ?: []);
                if (($data['required'] ?? null) === '0') {
                    unset($data['required']);
                }
                if (($data['readonly'] ?? null) === '0') {
                    unset($data['readonly']);
                }
                if (($data['multiple'] ?? null) === '0') {
                    unset($data['multiple']);
                }
                if (($data['select2'] ?? null) === '0') {
                    unset($data['select2']);
                }
                if (($data['checked'] ?? null) === '0') {
                    unset($data['checked']);
                }
                unset($data['is_submit'], $data['_tab'], $data['form'], $data['fieldset']);
                return $this->renderDataToJson($data);
            } else {
                $this->Modal->fail();
                FormatError::logFormErrors($form);
                $form->setData($request->getData());
            }
        }
        $this->set('form', $form);
        $this->set('type', $type);

        // options
        $inputs = Hash::extract($request->getData('form', []), 'fieldsets.{n}.inputs.{n}');
        $jsonInputs = array_map('json_decode', $inputs);
        $this->set('jsonInputs', json_encode($jsonInputs));
        $targets = [];
        $inputsData = [];
        foreach ($inputs as $input) {
            $input = json_decode($input, true);
            if (!$input) {
                continue;
            }
            $inputsData[] = $input;
            if ($input['type'] !== FormInputsTable::TYPE_PARAGRAPH) {
                $targets[$input['name']] = $input['name'] . (($input['label'] ?? false) ? ' - ' . $input['label'] : '');
            }
        }
        $this->set('targets', $targets);
        $this->set('inputsData', $inputsData);

        $this->KeywordLists = $this->fetchTable('KeywordLists');
        $keywordLists = $this->KeywordLists->find('list')
            ->where(
                [
                    'KeywordLists.org_entity_id' => $this->archivalAgencyId,
                    'KeywordLists.active IS' => true,
                    'KeywordLists.version >=' => 1,
                ]
            )
            ->order(['KeywordLists.name'])
            ->toArray();
        $this->set('keywordLists', $keywordLists);
        $this->FormFieldsets = $this->fetchTable('FormFieldsets');
        $this->set('cond_display_selects', $this->FormFieldsets->options('cond_display_select'));
        $this->set('fieldsets', $request->getData('form.fieldsets'));
    }

    /**
     * Rendu html d'un input Form->control()
     */
    public function generateInput()
    {
        $request = $this->getRequest();
        $request->allowMethod('post');
        $this->set('inputData', $request->getData());
        if ($request->getData('id')) {
            $formInput = $this->fetchTable('FormInputs')
                ->get($request->getData('id'));
            $this->set('formInput', $formInput);
        }
        $this->viewBuilder()->setLayout('ajax');
    }

    /**
     * Ajout d'une section en javascript
     */
    public function addFieldset()
    {
        $request = $this->getRequest();
        $this->FormFieldsets = $this->fetchTable('FormFieldsets');
        $entity = $this->FormFieldsets->newEmptyEntity();
        if ($request->getData('is_submit')) {
            $data = Hash::filter($request->getData() ?: []) + ['ord' => -1];
            $this->FormFieldsets->patchEntity($entity, $data);
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->success();
            return $this->renderDataToJson(Hash::filter($entity->toArray(), fn($v) => $v !== null));
        }
        $this->set('entity', $entity);

        // options
        $inputs = Hash::extract($request->getData() ?: [], 'fieldsets.{n}.inputs.{n}');
        $jsonInputs = array_map('json_decode', $inputs);
        $this->set('jsonInputs', json_encode($jsonInputs));
        $targets = [];
        $inputsData = [];
        foreach ($inputs as $input) {
            $input = json_decode($input, true);
            $inputsData[] = $input;
            if ($input['type'] !== FormInputsTable::TYPE_PARAGRAPH) {
                $targets[$input['name']] = $input['name'] . (($input['label'] ?? false) ? ' - ' . $input['label'] : '');
            }
        }
        $this->set('targets', $targets);
        $this->set('inputsData', $inputsData);
        $this->set('cond_display_selects', $this->FormFieldsets->options('cond_display_select'));
        $this->set('fieldsets', $request->getData('fieldsets'));
    }

    /**
     * Modification d'une section en javascript
     */
    public function editFieldset()
    {
        $request = $this->getRequest();
        $request->allowMethod('post');
        $this->FormFieldsets = $this->fetchTable('FormFieldsets');
        $data = Hash::filter($request->getData() ?: []) + ['ord' => -1];
        if (!empty($data['cond_display_value_select']) && is_string($data['cond_display_value_select'])) {
            $json = json_decode($data['cond_display_value_select'], true);
            if ($json) {
                $data['cond_display_value_select'] = $json;
            }
        }
        unset($data['form']);
        $entity = $this->FormFieldsets->newEntity($data, ['validate' => false]);
        if ($request->getData('is_submit')) {
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->success();
            return $this->renderDataToJson(Hash::filter($entity->toArray(), fn($v) => $v !== null));
        }
        $thisFieldsetInputsIds = Hash::extract(
            array_map(
                'json_decode',
                Hash::extract($request->getData('current_fieldset', []), 'fieldsets.{n}.inputs.{n}')
            ),
            '{n}.id'
        );
        $linked = [];
        foreach ($thisFieldsetInputsIds as $inputId) {
            $input = $this->fetchTable('FormInputs')->get($inputId);
            $isLinked = $input->get('linked');
            if ($isLinked) {
                $linked[$inputId] = $isLinked;
            }
        }
        $this->set('linked', $linked);
        $this->set('entity', $entity);

        // options
        $inputs = Hash::extract($request->getData('form', []), 'fieldsets.{n}.inputs.{n}');
        $jsonInputs = array_map('json_decode', $inputs);
        $this->set('jsonInputs', json_encode($jsonInputs));
        $thisFieldsetInputs = Hash::extract(
            array_map(
                'json_decode',
                Hash::extract($request->getData('current_fieldset', []), 'fieldsets.{n}.inputs.{n}')
            ),
            '{n}.name'
        );
        $notThisFieldsetInputs = array_filter(
            $jsonInputs,
            fn($input) =>
            !in_array(
                $input->name,
                $thisFieldsetInputs
            )
        );
        $targets = [];
        foreach ($notThisFieldsetInputs as $input) {
            $input = (array)$input;
            if ($input['type'] !== FormInputsTable::TYPE_PARAGRAPH) {
                $targets[$input['name']] = $input['name'] . (($input['label'] ?? false) ? ' - ' . $input['label'] : '');
            }
        }
        $this->set('targets', $targets);
        $this->set('inputsData', array_map(fn($e) => json_decode($e, true), $inputs));
        $this->set('cond_display_selects', $this->FormFieldsets->options('cond_display_select'));
        $this->set('fieldsets', $request->getData('form.fieldsets'));
    }

    /**
     * Prévisualisition d'un formulaire via ajax
     * @param string $id
     * @return Response|void
     * @throws Exception
     */
    public function preview(string $id)
    {
        $request = $this->getRequest();
        $request->allowMethod(['post', 'put']);

        if ($request->getData('is_submit')) {
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->success();
            return $this->renderDataToJson('done');
        }

        // on met en forme pour la vue générique "generate-form"
        $formEntity = new Entity();
        $fieldsets = [];
        $allInputs = [];
        $ord = 0;
        foreach ($request->getData('fieldsets', []) as $fieldset) {
            $fieldset['ord'] = $ord;
            if (!empty($fieldset['cond_display_value_select']) && is_string($fieldset['cond_display_value_select'])) {
                $json = json_decode($fieldset['cond_display_value_select'], true);
                if ($json) {
                    $fieldset['cond_display_value_select'] = $json;
                }
            }
            $fieldsetEntity = $this->Forms->FormFieldsets->newEntity($fieldset);
            $inputs = [];
            foreach ($fieldset['inputs'] ?? [] as $input) {
                $input = new Entity(json_decode($input, true));
                $input['form_fieldset'] = $fieldset;
                $inputs[] = $input;
                $allInputs[] = $input;
            }
            $fieldsetEntity->set('form_inputs', $inputs);
            $fieldsets[] = $fieldsetEntity;
            $ord++;
        }
        $formEntity->set('form_fieldsets', $fieldsets);
        $this->set('formEntity', $formEntity);

        $form = FormMakerForm::createPreview($allInputs, $id);
        $this->set('form', $form);
    }

    /**
     * Donne la liste complète des mots clés d'une liste de mots clés
     * @param string $keywordListId
     * @return Response
     */
    public function keywordListContent(string $keywordListId)
    {
        $this->Keywords = $this->fetchTable('Keywords');
        $keywords = $this->Keywords->find()
            ->innerJoinWith('KeywordLists')
            ->where(
                [
                    'KeywordLists.id' => $keywordListId,
                    'KeywordLists.org_entity_id' => $this->archivalAgencyId,
                    'KeywordLists.active' => true,
                    'KeywordLists.version' => new IdentifierExpression('Keywords.version'),
                    'KeywordLists.version >=' => 1,
                ]
            )
            ->disableHydration()
            ->order(['Keywords.id'])
            ->all()
            ->map(fn($e) => ['text' => $e['name'], 'value' => $e['code'] ?: $e['name']])
            ->toArray();
        return $this->renderDataToJson($keywords);
    }

    /**
     * Action modifier
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function edit(string $id)
    {
        $entity = $this->Forms->findEditableEntity()
            ->where(
                [
                    'Forms.id' => $id,
                    'Forms.state' => FormsTable::S_EDITING,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();
        $this->set('entity', $entity);
        $this->set('id', $id);

        $request = $this->getRequest();
        if ($request->is('put')) {
            $this->loadComponent('AsalaeCore.Modal');
            $success = false;
            switch ($request->getData('tab')) {
                case 'main':
                    $data = [
                        'name' => $request->getData('name'),
                        'description' => $request->getData('description'),
                        'transferring_agencies' => $request->getData('transferring_agencies'),
                        'archiving_system_id' => $request->getData('archiving_system_id'),
                        'user_guide' => $request->getData('user_guide'),
                    ];
                    $this->Forms->patchEntity($entity, $data);
                    $success = $this->Forms->save($entity);
                    break;
                case 'fieldsets':
                    $success = $this->saveEditFieldsets($entity);
                    break;
                case 'extractors':
                case 'variables':
                case 'headers':
                case 'units':
                    $success = true;
                    break;
            }
            if ($success) {
                $this->Modal->success();
                $entity = $this->Forms->find()
                    ->where(['Forms.id' => $id])
                    ->contain(['ArchivingSystems'])
                    ->firstOrFail();
                return $this->renderDataToJson($entity);
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->FormTransferHeaders = $this->fetchTable('FormTransferHeaders');
        $this->set('dataTransferHeaders', $this->FormTransferHeaders->formHeadersData($entity));

        // options
        $this->ArchivingSystems = $this->fetchTable('ArchivingSystems');
        $archivingSystems = $this->ArchivingSystems->find('list')
            ->where(
                [
                    'active' => true,
                    'org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->order(['name' => 'asc'])
            ->all()
            ->toArray();
        $this->set('archivingSystems', $archivingSystems);

        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->OrgEntities = $this->fetchTable('OrgEntities');
        $ta = $OrgEntities->listOptionsWithoutSeal()
            ->where(
                [
                    'TypeEntities.code IN' => OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES,
                    'OrgEntities.lft >=' => $this->archivalAgency->get('lft'),
                    'OrgEntities.rght <=' => $this->archivalAgency->get('rght'),
                ]
            );
        $this->set('transferringAgencies', $ta->all());
    }

    /**
     * Action publier
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function publish(string $id)
    {
        $this->Forms->find()
            ->where(
                [
                    'Forms.id' => $id,
                    'Forms.state' => FormsTable::S_EDITING,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['ArchivingSystems'])
            ->firstOrFail();

        $entity = $this->Forms->publish($id);

        $this->loadComponent('AsalaeCore.Modal');
        if (empty($entity->getErrors())) {
            $this->Modal->success();
            return $this->renderDataToJson($entity);
        } else {
            $report = [
                'success' => false,
                'errors' => $entity->getErrors(),
            ];
            $this->Modal->fail();
            return $this->renderDataToJson($report);
        }
    }

    /**
     * Action dépublier
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function unpublish(string $id)
    {
        $entity = $this->Forms->find()
            ->where(
                [
                    'Forms.id' => $id,
                    'Forms.state' => FormsTable::S_PUBLISHED,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['ArchivingSystems'])
            ->firstOrFail();

        if (
            !$entity->get('unpublishable')
            || !$this->Forms->transition($entity, FormsTable::T_UNPUBLISH)
        ) {
            throw new ForbiddenException();
        }

        $this->loadComponent('AsalaeCore.Modal');
        if ($this->Forms->save($entity)) {
            $this->Modal->success();
            return $this->renderDataToJson($entity);
        } else {
            $report = [
                'success' => false,
                'errors' => $entity->getErrors(),
            ];
            $this->Modal->fail();
            return $this->renderDataToJson($report);
        }
    }

    /**
     * Action visualiser
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        $entity = $this->Forms->find()
            ->where(
                [
                    'Forms.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(
                [
                    'TransferringAgencies',
                    'ArchivingSystems',
                    'FormFieldsets' => ['FormInputs'],
                    'FormExtractors',
                    'FormCalculators' => ['FormVariables'],
                    'FormTransferHeaders' => [
                        'FormVariables',
                    ],
                ]
            )
            ->firstOrFail();

        $entity->setVirtual(['prev_versions']);

        $this->FormTransferHeaders = $this->fetchTable('FormTransferHeaders');
        $this->set('dataTransferHeaders', $this->FormTransferHeaders->formHeadersDataForView($entity));

        $this->set('entity', $entity);
        $this->set('id', $id);
    }

    /**
     * Action tester
     * @param string $id
     * @throws Exception
     */
    public function test(string $id)
    {
        $formEntity = $this->Forms->find()
            ->where(
                [
                    'Forms.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(
                [
                    'FormFieldsets' => [
                        'sort' => 'ord',
                        'FormInputs' => [
                            'sort' => 'ord',
                        ],
                    ]
                ]
            )
            ->firstOrFail();
        if ($formEntity->get('state') === FormsTable::S_EDITING && !$formEntity->get('publishable')) {
            throw new ForbiddenException(__("Formulaire non testable"));
        }

        $this->set('sedaGeneratorIsDown', FormMakerForm::pingSedaGenerator() === false);
        $this->set('formEntity', $formEntity);
        $dir = sprintf(
            '%s/form_test/user_%d/%d',
            rtrim(Configure::read('App.paths.data'), DS),
            $this->userId,
            $id
        );
        $testEntity = new Entity(['name' => 'test_entity', 'path' => $dir]);
        $form = FormMakerForm::create($formEntity->id, $this->userId, $testEntity);
        $this->set('form', $form);

        $request = $this->getRequest();
        $isValidForm = $form->isValidForm();
        if ($isValidForm && $request->is('post')) {
            $transferring_agency_id = $request->getData('transferring_agency_id')
                ?: $this->archivalAgencyId;
            $additionnalData = [
                'additionnalData' => [
                    'user_id' => $this->userId,
                    'org_entity_id' => $this->orgEntityId,
                    'archival_agency_id' => $this->archivalAgencyId,
                    'transferring_agency_id' => $transferring_agency_id,
                    'originating_agency_id' => $request->getData('originating_agency_id')
                        ?: $transferring_agency_id,
                ],
                'hidden_fields' => $request->getData('hidden_fields', []),
            ];
            $this->loadComponent('AsalaeCore.Modal');
            if ($form->execute($data = $request->getData('inputs', []) + $additionnalData)) {
                $name = $this->userId . '_' . time() . '_test_generated_form.xml';
                Filesystem::dumpFile($dir . DS . $name, $form->xml);
                $this->Fileuploads = $this->fetchTable('Fileuploads');
                $fileupload = $this->Fileuploads->newUploadedEntity(
                    $name,
                    $dir . DS . $name,
                    $this->userId
                );
                $this->Fileuploads->saveOrFail($fileupload);

                $vars = $form->twigVars;
                $twigVars = Hash::flatten($vars);
                $twigName = $this->userId . '_' . time() . '_form_vars.serialize';
                Filesystem::dumpFile($dir . DS . $twigName, serialize($twigVars));
                $fileupload2 = $this->Fileuploads->newUploadedEntity(
                    $twigName,
                    $dir . DS . $twigName,
                    $this->userId
                );
                $this->Fileuploads->saveOrFail($fileupload2);

                $this->Forms->updateAll(
                    ['tested' => true],
                    ['id' => $id]
                );
                $this->cleanTestFiles($id, $data, $dir);

                $this->Modal->success();
                $this->Modal->step('actionAfterTest("' . $fileupload->id . '/' . $id . '/' . $fileupload2->id . '")');
            } else {
                FormatError::logFormErrors($form);
                $this->Modal->fail();
                $this->Fileuploads = $this->fetchTable('Fileuploads');
                $fileuploads = FormMakerForm::extractFileuploadsFromRequestData(
                    $this->userId,
                    $this->getRequest()->getData(),
                    $formEntity
                );
            }
        } elseif (!$isValidForm) {
            FormatError::logFormErrors($form);
        }
        $this->set('fileuploads', $fileuploads ?? []);

        // options
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $query = $this->OrgEntities->listOptionsWithoutSeal()
            ->innerJoinWith('FormsTransferringAgencies')
            ->where(['FormsTransferringAgencies.form_id' => $formEntity->get('id')]);
        if (Hash::get($this->orgEntity, 'type_entity.code') === 'SA') {
            $query->andWhere(
                [
                    'OrgEntities.lft >=' => $this->archivalAgency->get('lft'),
                    'OrgEntities.rght <=' => $this->archivalAgency->get('rght'),
                ]
            );
        } elseif (Hash::get($this->user, 'role.hierarchical_view')) {
            $query->andWhere(
                [
                    'OrgEntities.lft >=' => $this->orgEntity->get('lft'),
                    'OrgEntities.rght <=' => $this->orgEntity->get('rght'),
                ]
            );
        } else {
            $query->andWhere(['OrgEntities.id' => $this->orgEntityId]);
        }
        $this->set('transferringAgencies', $query->toArray());
    }

    /**
     * Suppression d'un fichier avec appel du callback
     * @param string $id
     * @return bool
     */
    protected function deleteFile(string $id): bool
    {
        $Fileuploads = $this->fetchTable('Fileuploads');
        $file = $Fileuploads->find()
            ->where(
                [
                    'id' => $id,
                    'user_id' => $this->userId,
                ]
            )
            ->first();
        return $Fileuploads->delete($file);
    }

    /**
     * Donne un aperçu ainsi que des liens pour le bordereau généré dans le test
     * @param string $fileupload_id  xml
     * @param string $formId
     * @param string $fileupload2_id serialize
     * @throws Exception
     */
    public function afterTest(string $fileupload_id, string $formId, string $fileupload2_id)
    {
        $this->Fileuploads = $this->fetchTable('Fileuploads');
        $entity = $this->Fileuploads->find()
            ->where(
                [
                    'Fileuploads.id' => $fileupload_id,
                    'Fileuploads.user_id' => $this->userId,
                ]
            )
            ->firstOrFail();
        $this->set('entity', $entity);
        $util = DOMUtility::load($entity->get('path'));
        $data = [
            'identifier' => $util->nodeValue('ns:TransferIdentifier|ns:MessageIdentifier'),
            'date' => $util->nodeValue('ns:Date'),
            'au_count' => $util->xpath->query('//ns:Archive|//ns:ArchiveObject|//ns:ArchiveUnit')->count(),
            'file_count' => $util->xpath->query('//ns:Attachment')->count(),
        ] + $entity->toArray();
        $this->set('data', new Entity($data));
        $this->set('formId', $formId);

        $twigVarsFileupload = $this->Fileuploads->find()
            ->where(
                [
                    'Fileuploads.id' => $fileupload2_id,
                    'Fileuploads.user_id' => $this->userId,
                ]
            )
            ->firstOrFail();
        $twigVars = unserialize(
            file_get_contents($twigVarsFileupload->get('path'))
        );
        $formatedTwigVars = [];
        $twigVarsCategories = [];
        foreach ($twigVars as $key => $value) {
            $formatedKey = '';
            $exp = explode('.', $key);
            [$category] = $exp;
            foreach ($exp as $part) {
                if (is_numeric($part)) {
                    $formatedKey .= "[$part]";
                } elseif (str_contains($part, '-')) {
                    $formatedKey .= "['$part']";
                } else {
                    $formatedKey .= ".$part";
                }
            }
            $formatedKey = ltrim($formatedKey, '.');
            $formatedTwigVars[$formatedKey] = $value;
            $twigVarsCategories[$category][$formatedKey]
                = fn() => $value ? h($value) : '<i>' . __("vide") . '</i>';
        }
        $this->set('twigVars', $formatedTwigVars);
        $this->set('twigVarsCategories', $twigVarsCategories);

        $this->Fileuploads->delete($twigVarsFileupload);
    }

    /**
     * "Jolie vue" pour le test
     * @param string $fileupload_id
     * @return Response
     */
    public function displayXmlTest(string $fileupload_id): Response
    {
        $this->Fileuploads = $this->fetchTable('Fileuploads');
        $entity = $this->Fileuploads->find()
            ->where(
                [
                    'Fileuploads.id' => $fileupload_id,
                    'Fileuploads.user_id' => $this->userId,
                ]
            )
            ->firstOrFail();

        $dom = new DOMDocument();
        $dom->load($entity->get('path'));

        switch ($namespace = $dom->documentElement->namespaceURI) {
            case NAMESPACE_SEDA_10:
                $xsl = SEDA_V10_XSL;
                break;
            case NAMESPACE_SEDA_22:
                $xsl = SEDA_V22_XSL;
                break;
            case NAMESPACE_SEDA_21:
            default:
                $xsl = SEDA_V21_XSL;
        }

        $xslDom = new DOMDocument();
        if (!$xslDom->load($xsl)) {
            throw new GenericException('fail to load xsl');
        }

        if (in_array($namespace, [NAMESPACE_SEDA_21, NAMESPACE_SEDA_22])) {
            // ajout d'éléments pour rendu de la DUA
            $appraisals = $dom->getElementsByTagName('AppraisalRule');
            $rules = [];
            for ($i = 0; $i <= 100; $i++) {
                $rules['APP' . $i . 'Y'] = __n("{0} an", "{0} ans", $i, $i);
            }

            $finals = [
                'Destroy' => __("Détruire"),
                'Keep' => __("Conserver"),
            ];
            /** @var DOMElement $appraisal */
            foreach ($appraisals as $appraisal) {
                /** @var DOMElement $rule */
                $rule = $appraisal->getElementsByTagName('Rule')->item(0);
                if ($rule && isset($rules[$rule->nodeValue])) {
                    $rule->setAttribute('trad', $rules[$rule->nodeValue]);
                }
                /** @var DOMElement $final */
                $final = $appraisal->getElementsByTagName('FinalAction')->item(0);
                if ($final && isset($finals[$final->nodeValue])) {
                    $final->setAttribute('trad', $finals[$final->nodeValue]);
                }
            }

            // ajout d'éléments pour rendu de la communicabilité
            $accesses = $dom->getElementsByTagName('AccessRule');
            $rules = [];
            $raw = Xml::toArray(Xml::build(Seda21Schema::getXsdCodePath('access_code'), ['readFile' => true]));
            $codes = $raw['schema']['xsd:simpleType']['xsd:restriction']['xsd:enumeration'] ?? [];
            foreach ($codes as $code) {
                $docs = Hash::get($code, 'xsd:annotation.xsd:documentation', []);
                $rules[$code['@value']] = Hash::get($docs, 'ccts:Name');
            }
            /** @var DOMElement $access */
            foreach ($accesses as $access) {
                /** @var DOMElement $rule */
                $rule = $access->getElementsByTagName('Rule')->item(0);
                if ($rule && isset($rules[$rule->nodeValue])) {
                    $rule->setAttribute('trad', $rules[$rule->nodeValue]);
                }
            }
        }

        $xslt = new XsltProcessor();
        $xslt->importStylesheet($xslDom);
        $xslt->setParameter('', 'afficheDocument', '#');
        $xslt->setParameter('', 'ratchetUrl', Configure::read('Ratchet.connect'));
        $xslt->setParameter('', 'files_exists', true);

        return $this->getResponse()
            ->withStringBody($xslt->transformToXML($dom));
    }

    /**
     * Action désactiver
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function deactivate(string $id): Response
    {
        $entity = $this->Forms->find()
            ->where(
                [
                    'Forms.id' => $id,
                    'Forms.state' => FormsTable::S_PUBLISHED,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['ArchivingSystems'])
            ->firstOrFail();
        if (!$this->Forms->transition($entity, FormsTable::T_DEACTIVATE)) {
            throw new ForbiddenException();
        }

        $this->loadComponent('AsalaeCore.Modal');
        if ($this->Forms->save($entity)) {
            $this->Modal->success();
            return $this->renderDataToJson($entity);
        } else {
            $report = [
                'success' => false,
                'errors' => $entity->getErrors(),
            ];
            $this->Modal->fail();
            return $this->renderDataToJson($report);
        }
    }

    /**
     * Action activer
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function activate(string $id): Response
    {
        $entity = $this->Forms->find()
            ->where(
                [
                    'Forms.id' => $id,
                    'Forms.state' => FormsTable::S_DEACTIVATED,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['ArchivingSystems'])
            ->firstOrFail();
        if (!$this->Forms->transition($entity, FormsTable::T_ACTIVATE)) {
            throw new ForbiddenException();
        }

        $this->loadComponent('AsalaeCore.Modal');
        if ($this->Forms->save($entity)) {
            $this->Modal->success();
            return $this->renderDataToJson($entity);
        } else {
            $report = [
                'success' => false,
                'errors' => $entity->getErrors(),
            ];
            $this->Modal->fail();
            return $this->renderDataToJson($report);
        }
    }

    /**
     * Action versionner
     * @param string $id
     * @return Response|void
     * @throws Exception
     */
    public function version(string $id)
    {
        $this->loadComponent('AsalaeCore.Modal');
        $entity = $this->Forms->find()
            ->where(
                [
                    'Forms.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();
        if (!$entity->get('versionable')) {
            throw new ForbiddenException(__("Formulaire non versionnable"));
        }

        $entity->unset('version_note');
        $this->set('entity', $entity);
        $request = $this->getRequest();

        if ($request->is('put')) {
            if (empty($request->getData('version_note'))) {
                throw new ForbiddenException();
            }

            $entity = $this->Forms->version($id, $request->getData('version_note'));

            if (empty($entity->getErrors())) {
                $this->Modal->success();
                return $this->renderDataToJson($entity);
            } else {
                $report = [
                    'success' => false,
                    'errors' => $entity->getErrors(),
                ];
                $this->Modal->fail();
                return $this->renderDataToJson($report);
            }
        }
    }

    /**
     * Action supprimer
     * @param string $id
     * @return Response
     */
    public function delete(string $id): Response
    {
        $this->getRequest()->allowMethod('delete');
        $entity = $this->Forms->find()
            ->where(
                [
                    'Forms.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();

        $report = $this->Forms->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Action dupliquer
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function duplicate(string $id): Response
    {
        $this->loadComponent('AsalaeCore.Modal');

        $this->Forms->find()
            ->where(
                [
                    'Forms.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();

        $entity = $this->Forms->duplicate($id);

        if (empty($entity->getErrors())) {
            $this->Modal->success();
            return $this->renderDataToJson($entity);
        } else {
            $report = [
                'success' => false,
                'errors' => $entity->getErrors(),
            ];

            $this->Modal->fail();
            return $this->renderDataToJson($report);
        }
    }

    /**
     * Mises à jour des fielsets et des champs du formulaire
     * @param EntityInterface $form
     * @return bool
     * @throws Exception
     */
    private function saveEditFieldsets(EntityInterface $form): bool
    {
        $request = $this->getRequest();
        $this->FormFieldsets = $this->fetchTable('FormFieldsets');
        $this->FormInputs = $this->fetchTable('FormInputs');
        $conn = $this->FormFieldsets->getConnection();
        $conn->begin();
        $query = $this->FormFieldsets->query();
        $query
            ->update()
            ->set(
                $query->newExpr('ord = ord + 1000')
            )
            ->where(['form_id' => $form->id])
            ->execute();
        $inputs = [];
        $inputIds = [];
        foreach (array_values($request->getData('fieldsets', [])) as $i => $fieldset) {
            if (isset($fieldset['id'])) {
                $fieldsetEntity = $this->FormFieldsets->get($fieldset['id']);
                $query = $this->FormInputs->query();
                $query
                    ->update()
                    ->set(
                        $query->newExpr('ord = ord + 1000')
                    )
                    ->where(['form_fieldset_id' => $fieldset['id']])
                    ->execute();
            } else {
                $fieldsetEntity = $this->FormFieldsets->newEmptyEntity();
            }
            $data = [
                'form_id' => $form->id,
                'legend' => $fieldset['legend'] ?? '',
                'ord' => $i,
                'repeatable' => $fieldset['repeatable'] ?? false,
                'required' => isset($fieldset['required']) && $fieldset['required'] !== ''
                    ? $fieldset['required']
                    : null,
                'cardinality' => isset($fieldset['cardinality']) && $fieldset['cardinality'] !== ''
                    ? $fieldset['cardinality']
                    : null,
                'button_name' => isset($fieldset['button_name']) && $fieldset['button_name'] !== ''
                    ? $fieldset['button_name']
                    : null,
                'cond_display_input' => $fieldset['cond_display_input'] ?? null,
                'cond_display_way' => $fieldset['cond_display_way'] ?? null,
                'cond_display_value' => $fieldset['cond_display_value'] ?? null,
                'cond_display_value_select' => $fieldset['cond_display_value_select'] ?? null,
                'cond_display_value_file' => $fieldset['cond_display_value_file'] ?? null,
                'cond_display_field' => $fieldset['cond_display_field'] ?? null,
            ];
            $this->FormFieldsets->patchEntity($fieldsetEntity, $data);
            $this->FormFieldsets->saveOrFail($fieldsetEntity);
            foreach (array_values($fieldset['inputs'] ?? []) as $j => $input) {
                $input = json_decode($input, true);
                if (!$input) {
                    continue;
                }
                if (
                    $input['type'] === FormInputsTable::TYPE_PARAGRAPH // valeur aléatoire pour champ non nullable
                    && (empty($input['name'])
                    || empty($input['label']))
                ) {
                    $input['name'] = $input['label'] = 'para_' . bin2hex(random_bytes(4));
                }
                /** @var FormInput $inputEntity */
                if (isset($input['id'])) {
                    $inputIds[] = $input['id'];
                    $inputEntity = $this->FormInputs->get($input['id']);
                } else {
                    $inputEntity = $this->FormInputs->newEntity(['form_id' => $form->id]);
                }
                $this->FormInputs->patchEntityWithDecodedInput(
                    $fieldsetEntity->id,
                    $j,
                    $inputEntity,
                    $input
                );
                $inputEntity->index = $i;
                $inputs[] = $inputEntity;
            }
        }
        if ($inputIds) {
            $this->FormInputs->deleteAll(
                [
                    'form_id' => $form->id,
                    'id NOT IN' => $inputIds,
                ]
            );
        } else {
            $this->FormInputs->deleteAll(['form_id' => $form->id]);
        }

        $success = true;
        foreach ($inputs as $j => $input) {
            if (!$this->FormInputs->save($input)) {
                $success = false;
                foreach ($input->getErrors() as $field => $errors) {
                    $form->setError(
                        sprintf('form.fieldsets.%d.inputs.%d.%s', $input->index, $j, $field),
                        $errors
                    );
                }
            }
        }

        // supprime les fieldsets qui ne sont pas dans la requete
        $this->FormFieldsets->deleteAll(['ord >=' => 1000]);
        $conn->commit();
        return $success;
    }

    /**
     * Test de webservices pour l'extracteur (api accessible par défaut pour tous les utilisateurs)
     * @return Response
     */
    protected function testExtractor()
    {
        return $this->renderData(
            [
                'message' => __("Ceci est un test d'appel de webservices pour l'extracteur"),
                'uuid' => uuid_create(UUID_TYPE_TIME),
                'date' => new DateTime(),
                'code' => $this->getResponse()->getStatusCode(),
                'float' => 3.14,
                'comma' => '3,14',
                'table' => [1, 2, 3],
            ]
        );
    }

    /**
     * Suppression des fichiers envoyés après test de formulaire
     * @param string $id
     * @param array  $data
     * @param string $dir
     * @return void
     * @throws Exception
     */
    protected function cleanTestFiles(string $id, array $data, string $dir): void
    {
        $FormInputs = $this->fetchTable('FormInputs');
        $query = $FormInputs->find()
            ->where(
                [
                    'form_id' => $id,
                    'type IN' => [
                        FormInputsTable::TYPE_FILE,
                        FormInputsTable::TYPE_ARCHIVE_FILE,
                    ],
                ]
            );

        /** @var EntityInterface $formInput */
        foreach ($query as $formInput) {
            $inputName = $formInput->get('name');
            if (empty($data[$inputName])) {
                continue;
            }
            if ($formInput->get('multiple')) {
                foreach ($data[$inputName] as $idInput) {
                    $this->deleteFile($idInput);
                }
            } else { // zip
                $this->deleteFile($data[$inputName]);
            }
        }
        if (is_dir($dir . '/uncompressed')) {
            Filesystem::remove($dir . '/uncompressed');
        }
        if (is_dir($dir . '/upload')) {
            Filesystem::remove($dir . '/upload');
        }
    }

    /**
     * Export d'un formulaire
     * @param string $id
     * @param string $version
     * @return Response|MessageInterface
     */
    public function export(string $id, string $version)
    {
        $entity = $this->Forms->find()
            ->where(
                [
                    'Forms.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();
        $export = $this->Forms->export($entity->id, ['Forms.version_number' => $version]);
        $json = json_encode($export, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        $len = strlen($json);
        return $this->getResponse()
            ->withHeader('Content-Description', 'File Transfer')
            ->withHeader('Content-Type', 'application/json')
            ->withHeader('Content-Length', $len)
            ->withHeader('File-Size', $len)
            ->withHeader(
                'Content-Disposition',
                'attachment; filename="' . $entity->get('export_filename') . '"'
            )
            ->withHeader('Expires', '0')
            ->withHeader('Cache-Control', 'must-revalidate')
            ->withHeader('Pragma', 'public')
            ->withStringBody($json);
    }

    /**
     * Importer un formulaire
     * @return Response|MessageInterface
     * @throws Exception
     */
    public function import()
    {
        $request = $this->getRequest();
        $form = new ImportForm();

        if ($request->is('post')) {
            $data = [
                'org_entity_id' => $this->archivalAgencyId,
            ] + $request->getData();
            $uploadData = $request->getData('fileuploads.id')
                ? $this->fetchTable('Fileuploads')->find()
                    ->where(
                        [
                            'Fileuploads.id' => $request->getData('fileuploads.id'),
                            'Fileuploads.user_id' => $this->userId,
                        ]
                    )
                    ->toArray()
                : [];
            $this->set('uploadData', $uploadData);
            $this->loadComponent('AsalaeCore.Modal');
            if ($form->execute($data)) {
                $this->Modal->success();
                $this->Modal->step('actionEditForm(' . $form->lastImport->id . ')');
                $this->ArchivingSystems = $this->fetchTable('ArchivingSystems');
                $form->lastImport->set(
                    'archiving_system',
                    $this->ArchivingSystems->get($form->lastImport->get('archiving_system_id'))
                );
                return $this->renderDataToJson($form->lastImport->toArray());
            } else {
                $this->Modal->fail();
                FormatError::logFormErrors($form);
            }
        }

        $this->set('form', $form);

        // options
        $this->ArchivingSystems = $this->fetchTable('ArchivingSystems');
        $archivingSystems = $this->ArchivingSystems->find('list')
            ->where(
                [
                    'active' => true,
                    'org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->order(['name' => 'asc'])
            ->all()
            ->toArray();
        $this->set('archivingSystems', $archivingSystems);

        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $this->OrgEntities = $this->fetchTable('OrgEntities');
        $ta = $OrgEntities->listOptionsWithoutSeal()
            ->where(
                [
                    'TypeEntities.code IN' => OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES,
                    'OrgEntities.lft >=' => $this->archivalAgency->get('lft'),
                    'OrgEntities.rght <=' => $this->archivalAgency->get('rght'),
                ]
            );
        $this->set('transferringAgencies', $ta->all());
    }

    /**
     * Affiche un arbre avec tous les élements composant l'arborescence
     * d'un formulaire
     * @param string $id
     * @return void
     */
    public function previewFormTree(string $id)
    {
        $entity = $this->Forms->find()
            ->where(
                [
                    'Forms.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();
        $this->set('entity', $entity);
        $this->set('jsData', $this->Forms->previewJstreeData($id));
    }
}
