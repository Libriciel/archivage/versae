<?php

/**
 * Versae\Controller\FormCalculatorsController
 */

namespace Versae\Controller;

use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Utility\FormatError;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Response;
use Cake\Utility\Hash;
use Exception;
use Twig\Error\SyntaxError;
use Versae\Model\Entity\FormVariable;
use Versae\Model\Table\FormCalculatorsTable;
use Versae\Model\Table\FormExtractorsFormVariablesTable;
use Versae\Model\Table\FormExtractorsTable;
use Versae\Model\Table\FormInputsFormVariablesTable;
use Versae\Model\Table\FormInputsTable;
use Versae\Model\Table\FormsTable;
use Versae\Model\Table\FormCalculatorsFormVariablesTable;
use Versae\Model\Table\FormTransferHeadersTable;
use Versae\Model\Table\FormUnitContentsTable;
use Versae\Model\Table\FormUnitHeadersTable;
use Versae\Model\Table\FormUnitKeywordDetailsTable;
use Versae\Model\Table\FormUnitKeywordsTable;
use Versae\Model\Table\FormUnitManagementsTable;
use Versae\Model\Table\FormUnitsTable;
use Versae\Model\Table\FormVariablesTable;
use Versae\Utility\Twig;

/**
 * Variables de formulaire
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property FormCalculatorsFormVariablesTable FormCalculatorsFormVariables
 * @property FormCalculatorsTable FormCalculators
 * @property FormExtractorsFormVariablesTable FormExtractorsFormVariables
 * @property FormExtractorsTable FormExtractors
 * @property FormInputsFormVariablesTable FormInputsFormVariables
 * @property FormInputsTable FormInputs
 * @property FormTransferHeadersTable FormTransferHeaders
 * @property FormUnitContentsTable FormUnitContents
 * @property FormUnitHeadersTable FormUnitHeaders
 * @property FormUnitKeywordDetailsTable FormUnitKeywordDetails
 * @property FormUnitKeywordsTable FormUnitKeywords
 * @property FormUnitManagementsTable FormUnitManagements
 * @property FormUnitsTable FormUnits
 * @property FormVariablesTable FormVariables
 * @property FormsTable Forms
 */
class FormCalculatorsController extends AppController
{
    use RenderDataTrait;

    /**
     * Ajout
     * @param string $form_id
     * @return \AsalaeCore\Http\Response|Response|void
     * @throws SyntaxError
     */
    public function add(string $form_id)
    {
        $request = $this->getRequest();
        $this->Forms = $this->fetchTable('Forms');
        $this->FormVariables = $this->fetchTable('FormVariables');
        $formEntity = $this->Forms->find()
            ->where(
                [
                    'Forms.id' => $form_id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();

        $entity = $this->FormVariables->newEmptyEntity();
        $calculator = $this->FormCalculators->newEmptyEntity();
        $calculator->set('form_id', $form_id);
        $entity->set('form_calculator', $calculator);

        if ($request->is('post')) {
            $this->FormVariables->patchEntity(
                $entity,
                [
                    'form_id' => $form_id,
                ]
                + $request->getData()
            );
            $entity->setDirty('app_meta');
            $this->loadComponent('AsalaeCore.Modal');

            $conn = $this->FormVariables->getConnection();
            $conn->begin();
            $success = $this->FormVariables->save($entity)
                && $this->saveVarLinks($entity);
            if ($success) {
                $conn->commit();
                $this->Modal->success();
                return $this->renderDataToJson(
                    $this->getEditData($entity)
                );
            } else {
                $conn->rollback();
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('formEntity', $formEntity);
        $this->set('entity', $entity);

        // options
        $this->setAddEditOptions($form_id, null, true);
    }

    /**
     * Edition d'une variable
     * @param string $id
     * @return Response
     * @throws SyntaxError
     */
    public function edit(string $id)
    {
        $request = $this->getRequest();
        $this->FormVariables = $this->fetchTable('FormVariables');
        $entity = $this->FormVariables->find()
            ->innerJoinWith('FormCalculators')
            ->innerJoinWith('Forms')
            ->where(
                [
                    'FormCalculators.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['Forms', 'FormCalculators'])
            ->firstOrFail();
        $formEntity = $entity->get('form');
        $form_id = $entity->get('form_id');
        $calculator = Hash::get($entity, 'form_calculator');
        if (!$calculator instanceof EntityInterface) {
            throw new BadRequestException('form_calculator not found');
        }

        if ($request->is('put')) {
            $this->FormVariables->patchEntity(
                $entity,
                [
                    'form_id' => $form_id,
                ]
                + $request->getData()
            );
            $entity->setDirty('app_meta');
            $this->loadComponent('AsalaeCore.Modal');

            $conn = $this->FormCalculators->getConnection();
            $conn->begin();
            $success = $this->FormVariables->save($entity);
            $this->FormCalculators->patchEntity(
                $calculator,
                ['form_variable_id' => $entity->id]
                + $request->getData('form_calculator')
                + $calculator->toArray()
            );
            $success = $success && $this->FormCalculators->save($calculator);
            $success = $success && $this->saveVarLinks($entity);
            if ($success) {
                $conn->commit();
                $this->Modal->success();
                return $this->renderDataToJson($this->getEditData($entity));
            } else {
                $conn->rollback();
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('formEntity', $formEntity);
        $this->set('entity', $entity);

        // options
        $this->setAddEditOptions($form_id, $id, true);
    }

    /**
     * Défini les options de select
     * @param string|int $form_id
     * @param null|int   $var_id
     * @param bool       $multiple
     * @throws Exception
     */
    private function setAddEditOptions($form_id, $var_id = null, bool $multiple = false): void
    {
        $this->FormVariables = $this->fetchTable('FormVariables');
        $types = $this->FormVariables->options('type');

        $this->FormInputs = $this->fetchTable('FormInputs');
        $optParams = [
            'keyField' => function (EntityInterface $entity) {
                return 'input.' . $entity->get('name');
            }
        ];
        $dates = $this->FormInputs->find('list', $optParams)
            ->innerJoinWith('FormFieldsets')
            ->where(
                [
                    'FormInputs.type IN' => ['date', 'datetime'],
                    'FormFieldsets.form_id' => $form_id,
                ]
            )
            ->toArray();

        $this->set('types', $types);
        $this->set('dates', array_keys($dates));
        $this->set('dateFormats', $this->FormInputs->options('date_format'));
        $options = $this->FormInputs->listOptions($form_id, $var_id);
        $fields = [];
        foreach (array_keys($options) as $optgroup) {
            $fields[$optgroup] = [];
            foreach ($options[$optgroup] as $opt) {
                if ($opt['data-multiple']) {
                    $opt['disabled'] = true;
                    $opt['title'] = __("Champ multiple, ne peut être utilisé.");
                }
                $fields[$optgroup][] = $opt;
            }
        }
        $this->set('fields_multiples', $this->FormInputs->filterMultiple($options));
        if ($multiple) {
            $fields = array_merge(
                [
                    __("Valeur multiple du champ") => [
                        [
                            'value' => 'multiple.value',
                            'text' => __("Valeur multiple"),
                            'class' => 'opt-multiple', // ne s'affichera que si un champ multiple est sélectionné
                            'data-multiple' => false, // nécessaire pour l'affichage dans la liste
                        ],
                    ],
                ],
                $fields
            );
        }
        $this->set('fields', $fields);

        $query = $this->FormInputs->query()
            ->where(
                [
                    'FormInputs.form_id' => $form_id,
                    'FormInputs.type' => FormInputsTable::TYPE_FILE,
                ]
            );
        $metas = [];
        foreach ($query as $formInput) {
            $metas[$formInput->get('name')] = [
                'meta.' . $formInput->get('name') . '.name' => 'name',
                'meta.' . $formInput->get('name') . '.size' => 'size',
                'meta.' . $formInput->get('name') . '.mime' => 'mime',
                'meta.' . $formInput->get('name') . '.pronom' => 'pronom',
                'meta.' . $formInput->get('name') . '.ext' => 'extension',
            ];
        }
        $this->set('metas', $metas);

        $keywordLists = $this->fetchTable('KeywordLists')->find('list')
            ->where(
                [
                    'KeywordLists.org_entity_id' => $this->archivalAgencyId,
                    'KeywordLists.active IS' => true,
                    'KeywordLists.version >=' => 1,
                ]
            )
            ->order(['KeywordLists.name'])
            ->toArray();
        $this->set('keywordLists', $keywordLists);
    }

    /**
     * Créé les liens entre le form_variable et d'autres models (form_variable, form_input...)
     * @param FormVariable $formVariable
     * @return bool
     * @throws SyntaxError
     */
    private function saveVarLinks(FormVariable $formVariable): bool
    {
        $fields = Twig::extractVars($formVariable->get('twig'));
        $this->FormCalculatorsFormVariables = $this->fetchTable('FormCalculatorsFormVariables');
        $this->FormExtractors = $this->fetchTable('FormExtractors');
        $this->FormExtractorsFormVariables = $this->fetchTable('FormExtractorsFormVariables');
        $this->FormInputs = $this->fetchTable('FormInputs');
        $this->FormInputsFormVariables = $this->fetchTable('FormInputsFormVariables');
        $vars = [];
        $inputs = [];
        $extractors = [];
        $calculatorId = Hash::get($formVariable, 'form_calculator.id');
        foreach ($fields as $field) {
            if (!strpos($field, '.')) {
                continue;
            }
            [$cat, $name] = explode('.', $field);
            if ($cat === 'var') {
                $query = $this->FormCalculators->find()
                    ->select(['id'])
                    ->innerJoinWith('FormVariables')
                    ->where(
                        [
                            'FormVariables.form_id' => $formVariable->get('form_id'),
                            'FormCalculators.name' => $name,
                        ]
                    );
                // condition supplémentaire pour éviter les références circulaires
                if ($calculatorId) {
                    $query->andWhere(['FormCalculators.id <' => $calculatorId]);
                }
                $linkId = $query->first()->id;
                if (!$linkId) {
                    return false;
                }
                $vars[] = $this->FormCalculatorsFormVariables->findOrCreate(
                    [
                        'form_calculator_id' => $linkId,
                        'form_variable_id' => $formVariable->id,
                    ]
                )->id;
            } elseif ($cat === 'input') {
                $linkId = $this->FormInputs->find()
                    ->select(['FormInputs.id'])
                    ->innerJoinWith('FormFieldsets')
                    ->where(
                        [
                            'FormFieldsets.form_id' => $formVariable->get('form_id'),
                            'FormInputs.name' => $name,
                        ]
                    )
                    ->first();
                if (!$linkId) {
                    return false;
                }
                $inputs[] = $this->FormInputsFormVariables->findOrCreate(
                    [
                        'form_input_id' => $linkId->id,
                        'form_variable_id' => $formVariable->id,
                    ]
                )->id;
            } elseif ($cat === 'extract') {
                $linkId = $this->FormExtractors->find()
                    ->select(['FormExtractors.id'])
                    ->where(
                        [
                            'FormExtractors.form_id' => $formVariable->get('form_id'),
                            'FormExtractors.name' => $name,
                        ]
                    )
                    ->first();
                if (!$linkId) {
                    return false;
                }
                $extractors[] = $this->FormExtractorsFormVariables->findOrCreate(
                    [
                        'form_extractor_id' => $linkId->id,
                        'form_variable_id' => $formVariable->id,
                    ]
                )->id;
            }
        }
        if ($vars) {
            $this->FormCalculatorsFormVariables->deleteAll(
                [
                    'form_variable_id' => $formVariable->id,
                    'id NOT IN' => $vars,
                ]
            );
        } else {
            $this->FormCalculatorsFormVariables->deleteAll(['form_variable_id' => $formVariable->id]);
        }
        if ($inputs) {
            $this->FormInputsFormVariables->deleteAll(
                [
                    'form_variable_id' => $formVariable->id,
                    'id NOT IN' => $inputs,
                ]
            );
        } else {
            $this->FormInputsFormVariables->deleteAll(['form_variable_id' => $formVariable->id]);
        }
        if ($extractors) {
            $this->FormExtractorsFormVariables->deleteAll(
                [
                    'form_variable_id' => $formVariable->id,
                    'id NOT IN' => $extractors,
                ]
            );
        } else {
            $this->FormExtractorsFormVariables->deleteAll(['form_variable_id' => $formVariable->id]);
        }
        return true;
    }

    /**
     * Suppression d'une variable
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete(string $id)
    {
        $entity = $this->FormCalculators->find()
            ->innerJoinWith('Forms')
            ->where(
                [
                    'FormCalculators.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();

        $report = $this->FormCalculators->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        return $this->renderDataToJson(
            [
                'report' => $report,
                'edit_data' => $this->getEditData($entity)
            ]
        );
    }

    /**
     * Suppression d'une variable
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function deleteTransferHeader(string $id): Response
    {
        $this->FormTransferHeaders = $this->fetchTable('FormTransferHeaders');
        $entity = $this->FormTransferHeaders->find()
            ->innerJoinWith('Forms')
            ->where(
                [
                    'FormTransferHeaders.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();

        $report = $this->FormTransferHeaders->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        $this->Forms = $this->fetchTable('Forms');
        return $this->renderDataToJson(
            [
                'report' => $report,
                'edit_data' => $this->getEditData($entity)
            ]
        );
    }

    /**
     * Suppression d'une variable
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function deleteUnitHeader(string $id)
    {
        $this->FormUnitHeaders = $this->fetchTable('FormUnitHeaders');
        $entity = $this->FormUnitHeaders->find()
            ->innerJoinWith('FormUnits')
            ->innerJoinWith('FormUnits.Forms')
            ->where(
                [
                    'FormUnitHeaders.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();

        $report = $this->FormUnitHeaders->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        $this->Forms = $this->fetchTable('Forms');
        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Suppression d'une variable
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function deleteUnitManagement(string $id)
    {
        $this->FormUnitManagements = $this->fetchTable('FormUnitManagements');
        $entity = $this->FormUnitManagements->find()
            ->innerJoinWith('FormUnits')
            ->innerJoinWith('FormUnits.Forms')
            ->where(
                [
                    'FormUnitManagements.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();

        $report = $this->FormUnitManagements->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        $this->Forms = $this->fetchTable('Forms');
        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Suppression d'une variable
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function deleteUnitContent(string $id)
    {
        $this->FormUnitContents = $this->fetchTable('FormUnitContents');
        $entity = $this->FormUnitContents->find()
            ->innerJoinWith('FormUnits')
            ->innerJoinWith('FormUnits.Forms')
            ->where(
                [
                    'FormUnitContents.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();

        $report = $this->FormUnitContents->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        $this->Forms = $this->fetchTable('Forms');
        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Suppression d'une variable de mot clé
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function deleteKeywordDetail(string $id)
    {
        $this->FormUnitKeywordDetails = $this->fetchTable('FormUnitKeywordDetails');
        $entity = $this->FormUnitKeywordDetails->find()
            ->innerJoinWith('FormUnitKeywords')
            ->innerJoinWith('FormUnitKeywords.FormUnits')
            ->innerJoinWith('FormUnitKeywords.FormUnits.Forms')
            ->where(
                [
                    'FormUnitKeywordDetails.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();

        $report = $this->FormUnitKeywordDetails->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        $this->Forms = $this->fetchTable('Forms');
        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Ajout et modification des variables pour form_transfer_headers
     * @param string $form_id
     * @param string $id
     * @return Response|void
     * @throws SyntaxError
     */
    public function addEditHeaderVar(string $form_id, string $id)
    {
        $this->Forms = $this->fetchTable('Forms');
        $this->FormTransferHeaders = $this->fetchTable('FormTransferHeaders');
        $this->FormVariables = $this->fetchTable('FormVariables');
        $form = $this->Forms->find()
            ->where(
                [
                    'Forms.id' => $form_id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();
        if (is_numeric($id)) {
            $entity = $this->FormVariables->find()
                ->innerJoinWith('FormTransferHeaders')
                ->where(
                    [
                        'FormTransferHeaders.id' => $id,
                        'FormVariables.form_id' => $form_id,
                    ]
                )
                ->contain(['FormTransferHeaders'])
                ->firstOrFail();
        } else {
            $entity = $this->FormVariables->find()
                ->innerJoinWith('FormTransferHeaders')
                ->where(
                    [
                        'FormTransferHeaders.name' => $id,
                        'FormVariables.form_id' => $form_id,
                    ]
                )
                ->contain(['FormTransferHeaders'])
                ->first();
            if (!$entity) {
                $entity = $this->FormVariables->newEmptyEntity();
                $transferHeader = $this->FormTransferHeaders->newEmptyEntity();
                $transferHeader->set('name', $id);
                $transferHeader->set('form_id', $form_id);
                $entity->set('form_transfer_header', $transferHeader);
            }
        }
        if (!isset($transferHeader)) {
            $transferHeader = Hash::get($entity, 'form_transfer_header');
        }

        $request = $this->getRequest();
        if (
            (is_numeric($id)
            && $request->is('put'))
            || (!is_numeric($id)
            && $request->is('post'))
        ) {
            $data = [
                'form_id' => $form_id,
            ]
            + $request->getData();

            $this->FormVariables->patchEntity($entity, $data);

            $entity->setDirty('app_meta');
            $this->loadComponent('AsalaeCore.Modal');

            $conn = $this->FormVariables->getConnection();
            $conn->begin();
            $success = $this->FormVariables->save($entity);
            $this->FormTransferHeaders->patchEntity(
                $transferHeader,
                ['form_variable_id' => $entity->id] + $transferHeader->toArray()
            );
            $success = $success && $this->FormTransferHeaders->save($transferHeader);
            $success = $success && $this->saveVarLinks($entity);
            if ($success) {
                $conn->commit();
                $this->Modal->success();
                $entity->set(
                    'edit_data',
                    $this->getEditData($entity)
                );
                return $this->renderDataToJson($entity);
            } else {
                $conn->rollback();
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
                FormatError::logEntityErrors($transferHeader);
            }
        }

        $this->set('id', $id);
        $this->set('entity', $entity);
        $this->set('form', $form);

        // options
        $this->setAddEditOptions($form_id, $entity->get('id'));
    }

    /**
     * Ajout et modification des variables pour form_unit_headers
     * @param string $form_unit_id
     * @param string $id
     * @return Response|void
     * @throws SyntaxError
     */
    public function addEditHeaderUnit(string $form_unit_id, string $id)
    {
        $this->Forms = $this->fetchTable('Forms');
        $this->FormUnits = $this->fetchTable('FormUnits');
        $this->FormUnitHeaders = $this->fetchTable('FormUnitHeaders');
        $this->FormVariables = $this->fetchTable('FormVariables');
        $formUnit = $this->FormUnits->find()
            ->innerJoinWith('Forms')
            ->where(
                [
                    'FormUnits.id' => $form_unit_id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['Forms'])
            ->firstOrFail();
        $this->set('formUnit', $formUnit);
        $form = $formUnit->get('form');
        if (is_numeric($id)) {
            $entity = $this->FormVariables->find()
                ->innerJoinWith('FormUnitHeaders')
                ->where(
                    [
                        'FormUnitHeaders.id' => $id,
                        'FormVariables.form_id' => $form->id,
                    ]
                )
                ->contain(['FormUnitHeaders'])
                ->firstOrFail();
        } else {
            $entity = $this->FormVariables->find()
                ->innerJoinWith('FormUnitHeaders')
                ->where(
                    [
                        'FormUnitHeaders.name' => $id,
                        'FormUnitHeaders.form_unit_id' => $form_unit_id,
                        'FormVariables.form_id' => $form->id,
                    ]
                )
                ->contain(['FormUnitHeaders'])
                ->first();
            if (!$entity) {
                $entity = $this->FormVariables->newEmptyEntity();
                $unitHeader = $this->FormUnitHeaders->newEmptyEntity();
                $unitHeader->set('name', $id);
                $unitHeader->set('form_unit_id', $form_unit_id);
                $entity->set('form_unit_header', $unitHeader);
            }
        }
        if (!isset($unitHeader)) {
            $unitHeader = Hash::get($entity, 'form_unit_header');
        }

        $request = $this->getRequest();
        if (
            (is_numeric($id)
            && $request->is('put'))
            || (!is_numeric($id)
            && $request->is('post'))
        ) {
            $data = [
                'form_id' => $form->id,
            ]
            + $request->getData();

            $this->FormVariables->patchEntity($entity, $data);

            $entity->setDirty('app_meta');
            $this->loadComponent('AsalaeCore.Modal');

            $conn = $this->FormVariables->getConnection();
            $conn->begin();
            $success = $this->FormVariables->save($entity);
            $this->FormUnitHeaders->patchEntity(
                $unitHeader,
                ['form_variable_id' => $entity->id] + $unitHeader->toArray()
            );
            $success = $success && $this->FormUnitHeaders->save($unitHeader);
            $success = $success && $this->saveVarLinks($entity);
            if ($success) {
                $conn->commit();
                $this->Modal->success();
                $entity->set(
                    'edit_data',
                    $this->getEditData($entity)
                );
                return $this->renderDataToJson($entity);
            } else {
                $conn->rollback();
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
                FormatError::logEntityErrors($unitHeader);
            }
        }

        $this->set('id', $id);
        $this->set('entity', $entity);
        $this->set('form', $form);

        // options
        $this->setAddEditOptions($form->id);
    }

    /**
     * Ajout et modification des variables pour form_unit_managements
     * @param string $form_unit_id
     * @param string $id
     * @return Response|void
     * @throws SyntaxError
     */
    public function addEditManagementUnit(string $form_unit_id, string $id)
    {
        $this->Forms = $this->fetchTable('Forms');
        $this->FormUnits = $this->fetchTable('FormUnits');
        $this->FormUnitManagements = $this->fetchTable('FormUnitManagements');
        $this->FormVariables = $this->fetchTable('FormVariables');
        $formUnit = $this->FormUnits->find()
            ->innerJoinWith('Forms')
            ->where(
                [
                    'FormUnits.id' => $form_unit_id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['Forms'])
            ->firstOrFail();
        $this->set('formUnit', $formUnit);
        $form = $formUnit->get('form');
        if (is_numeric($id)) {
            $entity = $this->FormVariables->find()
                ->innerJoinWith('FormUnitManagements')
                ->where(
                    [
                        'FormUnitManagements.id' => $id,
                        'FormVariables.form_id' => $form->id,
                    ]
                )
                ->contain(['FormUnitManagements'])
                ->firstOrFail();
        } else {
            $entity = $this->FormVariables->find()
                ->innerJoinWith('FormUnitManagements')
                ->where(
                    [
                        'FormUnitManagements.name' => $id,
                        'FormUnitManagements.form_unit_id' => $form_unit_id,
                        'FormVariables.form_id' => $form->id,
                    ]
                )
                ->contain(['FormUnitManagements'])
                ->first();
            if (!$entity) {
                $entity = $this->FormVariables->newEmptyEntity();
                $unitManagement = $this->FormUnitManagements->newEmptyEntity();
                $unitManagement->set('name', $id);
                $unitManagement->set('form_unit_id', $form_unit_id);
                $entity->set('form_unit_management', $unitManagement);
            }
        }
        if (!isset($unitManagement)) {
            $unitManagement = Hash::get($entity, 'form_unit_management');
        }

        $request = $this->getRequest();
        if (
            (is_numeric($id)
            && $request->is('put'))
            || (!is_numeric($id)
            && $request->is('post'))
        ) {
            $data = [
                'form_id' => $form->id,
            ]
            + $request->getData();

            $this->FormVariables->patchEntity($entity, $data);

            $entity->setDirty('app_meta');
            $this->loadComponent('AsalaeCore.Modal');

            $conn = $this->FormVariables->getConnection();
            $conn->begin();
            $success = $this->FormVariables->save($entity);
            $this->FormUnitManagements->patchEntity(
                $unitManagement,
                ['form_variable_id' => $entity->id] + $unitManagement->toArray()
            );
            $success = $success && $this->FormUnitManagements->save($unitManagement);
            $success = $success && $this->saveVarLinks($entity);
            if ($success) {
                $conn->commit();
                $this->Modal->success();
                $entity->set(
                    'edit_data',
                    $this->getEditData($entity)
                );
                return $this->renderDataToJson($entity);
            } else {
                $conn->rollback();
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
                FormatError::logEntityErrors($unitManagement);
            }
        }

        $this->set('id', $id);
        $this->set('entity', $entity);
        $this->set('form', $form);

        // options
        $this->setAddEditOptions($form->id);
    }

    /**
     * Ajout et modification des variables pour form_unit_contents
     * @param string $form_unit_id
     * @param string $id
     * @return Response|void
     * @throws SyntaxError
     */
    public function addEditContentUnit(string $form_unit_id, string $id)
    {
        $this->Forms = $this->fetchTable('Forms');
        $this->FormUnits = $this->fetchTable('FormUnits');
        $this->FormUnitContents = $this->fetchTable('FormUnitContents');
        $this->FormVariables = $this->fetchTable('FormVariables');
        $formUnit = $this->FormUnits->find()
            ->innerJoinWith('Forms')
            ->where(
                [
                    'FormUnits.id' => $form_unit_id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['Forms'])
            ->firstOrFail();
        $this->set('formUnit', $formUnit);
        $form = $formUnit->get('form');
        if (is_numeric($id)) {
            $entity = $this->FormVariables->find()
                ->innerJoinWith('FormUnitContents')
                ->where(
                    [
                        'FormUnitContents.id' => $id,
                        'FormVariables.form_id' => $form->id,
                    ]
                )
                ->contain(['FormUnitContents'])
                ->firstOrFail();
        } else {
            $entity = $this->FormVariables->find()
                ->innerJoinWith('FormUnitContents')
                ->where(
                    [
                        'FormUnitContents.name' => $id,
                        'FormUnitContents.form_unit_id' => $form_unit_id,
                        'FormVariables.form_id' => $form->id,
                    ]
                )
                ->contain(['FormUnitContents'])
                ->first();
            if (!$entity) {
                $entity = $this->FormVariables->newEmptyEntity();
                $unitContent = $this->FormUnitContents->newEmptyEntity();
                $unitContent->set('name', $id);
                $unitContent->set('form_unit_id', $form_unit_id);
                $entity->set('form_unit_content', $unitContent);
            }
        }
        if (!isset($unitContent)) {
            $unitContent = Hash::get($entity, 'form_unit_content');
        }

        $request = $this->getRequest();
        if (
            (is_numeric($id)
            && $request->is('put'))
            || (!is_numeric($id)
            && $request->is('post'))
        ) {
            $data = [
                'form_id' => $form->id,
            ]
            + $request->getData();

            $this->FormVariables->patchEntity($entity, $data);

            $entity->setDirty('app_meta');
            $this->loadComponent('AsalaeCore.Modal');

            $conn = $this->FormVariables->getConnection();
            $conn->begin();
            $success = $this->FormVariables->save($entity);
            $this->FormUnitContents->patchEntity(
                $unitContent,
                ['form_variable_id' => $entity->id] + $unitContent->toArray()
            );
            $success = $success && $this->FormUnitContents->save($unitContent);
            $success = $success && $this->saveVarLinks($entity);
            if ($success) {
                $conn->commit();
                $this->Modal->success();
                $entity->set(
                    'edit_data',
                    $this->getEditData($entity)
                );
                return $this->renderDataToJson($entity);
            } else {
                $conn->rollback();
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
                FormatError::logEntityErrors($unitContent);
            }
        }

        $this->set('id', $id);
        $this->set('entity', $entity);
        $this->set('form', $form);

        // options
        $this->setAddEditOptions($form->id);
    }

    /**
     * Ajout et modification des variables pour form_unit_keyword_details
     * @param string $form_unit_keyword_id
     * @param string $id
     * @param string $multiple_with
     * @return Response|void
     * @throws SyntaxError
     */
    public function addEditKeywordDetail(
        string $form_unit_keyword_id,
        string $id,
        string $multiple_with = ''
    ) {
        $this->Forms = $this->fetchTable('Forms');
        $this->FormUnits = $this->fetchTable('FormUnits');
        $this->FormUnitKeywordDetails = $this->fetchTable('FormUnitKeywordDetails');
        $this->FormVariables = $this->fetchTable('FormVariables');
        $this->FormUnitKeywords = $this->fetchTable('FormUnitKeywords');

        $formUnitKeyword = $this->FormUnitKeywords->get($form_unit_keyword_id);
        $form_unit_id = $formUnitKeyword->get('form_unit_id');

        $formUnit = $this->FormUnits->find()
            ->innerJoinWith('Forms')
            ->where(
                [
                    'FormUnits.id' => $form_unit_id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['Forms'])
            ->firstOrFail();
        $this->set('formUnit', $formUnit);
        $form = $formUnit->get('form');
        if (is_numeric($id)) {
            $entity = $this->FormVariables->find()
                ->innerJoinWith('FormUnitKeywordDetails')
                ->where(
                    [
                        'FormUnitKeywordDetails.id' => $id,
                        'FormVariables.form_id' => $form->id,
                    ]
                )
                ->contain(['FormUnitKeywordDetails'])
                ->firstOrFail();
        } else {
            $entity = $this->FormVariables->find()
                ->innerJoinWith('FormUnitKeywordDetails')
                ->where(
                    [
                        'FormUnitKeywordDetails.name' => $id,
                        'FormUnitKeywordDetails.form_unit_keyword_id' => $form_unit_keyword_id,
                        'FormVariables.form_id' => $form->id,
                    ]
                )
                ->contain(['FormUnitKeywordDetails'])
                ->first();
            if (!$entity) {
                $entity = $this->FormVariables->newEmptyEntity();
                $unitKeywordDetail = $this->FormUnitKeywordDetails->newEmptyEntity();
                $unitKeywordDetail->set('name', $id);
                $unitKeywordDetail->set('form_unit_keyword_id', $form_unit_keyword_id);
                $entity->set('form_unit_keyword_detail', $unitKeywordDetail);
            }
        }
        if (!isset($unitKeywordDetail)) {
            $unitKeywordDetail = Hash::get($entity, 'form_unit_keyword_detail');
        }

        $request = $this->getRequest();
        if (
            (is_numeric($id)
            && $request->is('put'))
            || (!is_numeric($id)
            && $request->is('post'))
        ) {
            $data = [
                'form_id' => $form->id,
            ]
                + $request->getData();

            $this->FormVariables->patchEntity($entity, $data);

            $entity->setDirty('app_meta');
            $this->loadComponent('AsalaeCore.Modal');

            $conn = $this->FormVariables->getConnection();
            $conn->begin();
            $success = $this->FormVariables->save($entity);
            $this->FormUnitKeywordDetails->patchEntity(
                $unitKeywordDetail,
                ['form_variable_id' => $entity->id] + $unitKeywordDetail->toArray()
            );
            $success = $success && $this->FormUnitKeywordDetails->save($unitKeywordDetail);
            $success = $success && $this->saveVarLinks($entity);
            if ($success) {
                $conn->commit();
                $this->Modal->success();
                $entity->set(
                    'edit_data',
                    $this->getEditData($entity)
                );
                return $this->renderDataToJson($entity);
            } else {
                $conn->rollback();
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
                FormatError::logEntityErrors($unitKeywordDetail);
            }
        }

        $this->set('id', $id);
        $this->set('entity', $entity);
        $this->set('form', $form);
        $this->set('multiple_with', $multiple_with);

        // options
        $this->setAddEditOptions($form->id, null, true);
    }

    /**
     * Appel canonique de getEditData pour rafraîchir l'onglet après modification d'une UA
     * @param string $id
     * @return Response
     */
    public function getData(string $id)
    {
        $this->FormVariables = $this->fetchTable('FormVariables');
        $entity = $this->FormVariables->find()
            ->innerJoinWith('FormCalculators')
            ->innerJoinWith('Forms')
            ->where(
                [
                    'Forms.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['Forms', 'FormCalculators'])
            ->first();
        return $this->renderDataToJson(
            $entity ? $this->getEditData($entity) : [] // il peut ne pas y avoir de variables
        );
    }

    /**
     * Donne les données pour rafraichir une vue Forms/edit
     * @param EntityInterface $entity
     * @return array
     */
    private function getEditData(EntityInterface $entity)
    {
        $this->Forms = $this->fetchTable('Forms');
        $this->FormTransferHeaders = $this->fetchTable('FormTransferHeaders');
        $form = $this->Forms->findEditableEntity()
            ->where(['Forms.id' => $entity->get('form_id')])
            ->firstOrFail();
        $data = $form->toArray();
        $data['form_transfer_headers'] = $this->FormTransferHeaders->formHeadersData($form);
        return $data;
    }
}
