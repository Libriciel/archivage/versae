<?php

/**
 * Versae\Controller\DepositsController
 */

namespace Versae\Controller;

use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\FormatError;
use AsalaeCore\Utility\PreControlMessages;
use Beanstalk\Utility\Beanstalk;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\QueryInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Exception\ServiceUnavailableException;
use Cake\Http\Response;
use Cake\I18n\FrozenTime;
use Cake\ORM\Query;
use Cake\Utility\Hash;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Pheanstalk\PheanstalkInterface;
use Versae\Form\FormMakerForm;
use Versae\Model\Table\ArchivingSystemsTable;
use Versae\Model\Table\CountersTable;
use Versae\Model\Table\DepositsTable;
use Versae\Model\Table\FileuploadsTable;
use Versae\Model\Table\FormInputsTable;
use Versae\Model\Table\FormsTable;
use Versae\Model\Table\OrgEntitiesTable;
use Versae\Model\Table\TransfersTable;

/**
 * Deposits
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ArchivingSystemsTable ArchivingSystems
 * @property CountersTable Counters
 * @property DepositsTable Deposits
 * @property FileuploadsTable Fileuploads
 * @property FormsTable Forms
 * @property OrgEntitiesTable OrgEntities
 * @property TransfersTable Transfers
 */
class DepositsController extends AppController
{
    use RenderDataTrait;

    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public $paginate = [
        'order' => [
            'Deposits.last_state_update' => 'desc',
            'Deposits.id' => 'desc',
        ],
        'sortable' => [
            'form_name' => 'Forms.name',
            'transfer_created' => 'Transfers.created',
        ],
    ];

    /**
     * Liste mes versements en cours
     */
    public function indexInProgress()
    {
        $this->loadComponent('AsalaeCore.Index');
        $this->Index->init();

        $query = $this->Deposits->find()
            ->where(
                [
                    'Deposits.state IN' => [
                        DepositsTable::S_EDITING,
                        DepositsTable::S_READY_TO_PREPARE,
                        DepositsTable::S_PREPARING_ERROR,
                        DepositsTable::S_READY_TO_SEND,
                        DepositsTable::S_SENDING_ERROR,
                        DepositsTable::S_SENT,
                        DepositsTable::S_ACKNOWLEDGING_ERROR,
                        DepositsTable::S_RECEIVED,
                        DepositsTable::S_ANSWERING_ERROR,
                        DepositsTable::S_GENERATING,
                    ],
                    'Deposits.created_user_id' => $this->userId,
                ]
            );

        $this->indexCommons($query);
    }

    /**
     * Liste tous mes versement
     */
    public function indexAll()
    {
        $this->loadComponent('AsalaeCore.Index');
        $this->Index->init();

        $query = $this->Deposits->find()
            ->where(['Deposits.created_user_id' => $this->userId]);

        $this->indexCommons($query);
    }

    /**
     * Liste tous les versements de mon service
     */
    public function indexMyEntity()
    {
        $this->loadComponent('AsalaeCore.Index');
        $this->Index->init();

        $query = $this->Deposits->find()
            ->innerJoinWith('TransferringAgencies')
            ->where(['Deposits.archival_agency_id' => $this->archivalAgencyId]);
        if (Hash::get($this->orgEntity, 'type_entity.code') === 'SA') {
            $query->andWhere(
                [
                    'TransferringAgencies.lft >=' => $this->archivalAgency->get('lft'),
                    'TransferringAgencies.rght <=' => $this->archivalAgency->get('rght'),
                ]
            );
        } elseif (Hash::get($this->user, 'role.hierarchical_view')) {
            $query->andWhere(
                [
                    'TransferringAgencies.lft >=' => $this->orgEntity->get('lft'),
                    'TransferringAgencies.rght <=' => $this->orgEntity->get('rght'),
                ]
            );
        } else {
            $query->andWhere(['TransferringAgencies.id' => $this->orgEntityId]);
        }

        $this->indexCommons($query);
    }

    /**
     * Choix du formulaire, du nom et du service versant
     */
    public function add1()
    {
        $request = $this->getRequest();

        $orgEntity = $this->orgEntity;
        $archivalAgency = $this->archivalAgency;
        $hierarchicalView = Hash::get($this->user, 'role.hierarchical_view');
        $whereOrgEntities = $hierarchicalView
            ? [
                'OrgEntities.lft >=' => Hash::get($archivalAgency, 'lft'),
                'OrgEntities.rght <=' => Hash::get($archivalAgency, 'rght'),
            ]
            : ['Forms.org_entity_id' => $this->archivalAgencyId];
        $this->Forms = $this->fetchTable('Forms');
        $forms = $this->Forms->find()
            ->innerJoinWith('ArchivingSystems')
            ->innerJoinWith('OrgEntities')
            ->where(
                [
                    'Forms.state' => FormsTable::S_PUBLISHED,
                    'ArchivingSystems.active IS' => true,
                ] + $whereOrgEntities
            )
            ->order(['Forms.name'])
            ->contain(
                [
                    'TransferringAgencies' =>
                        function (QueryInterface $q) use ($orgEntity, $archivalAgency, $hierarchicalView) {
                            if (Hash::get($this->orgEntity, 'type_entity.code') === 'SA') {
                                $q->where(
                                    [
                                        'TransferringAgencies.lft >=' => $archivalAgency->get('lft'),
                                        'TransferringAgencies.rght <=' => $archivalAgency->get('rght'),
                                    ]
                                );
                            } elseif ($hierarchicalView) {
                                $q->where(
                                    [
                                        'TransferringAgencies.lft >=' => $orgEntity->get('lft'),
                                        'TransferringAgencies.rght <=' => $orgEntity->get('rght'),
                                    ]
                                );
                            } else {
                                $q->where(['TransferringAgencies.id' => $orgEntity->get('id')]);
                            }
                            return $q;
                        },
                ]
            )
            ->all()
            ->toArray();

        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $transferringsAgencies = [];
        $originatingAgencies = [];
        foreach ($forms as $key => $form) {
            $transferringsAgencies[$form->get('id')] = Hash::combine(
                $form->get('transferring_agencies'),
                '{n}.id',
                ['%s - %s', '{n}.identifier', '{n}.name']
            );
            if (empty($transferringsAgencies[$form->get('id')])) {
                unset($forms[$key]);
            }
            /** @var EntityInterface $transferingAgency */
            foreach ($form->get('transferring_agencies') ?: [] as $transferringAgency) {
                if (Hash::get($this->user, 'role.hierarchical_view')) {
                    $query = $this->OrgEntities->find(
                        'list',
                        [
                            'valueField' => function (EntityInterface $entity) {
                                return $entity->get('identifier') . ' - ' . $entity->get('name');
                            },
                        ]
                    )
                        ->innerJoinWith('TypeEntities')
                        ->where(
                            [
                                'TypeEntities.code IN' => OrgEntitiesTable::CODE_ORIGINATING_AGENCIES,
                                'OrgEntities.lft >=' => $transferringAgency->get('lft'),
                                'OrgEntities.rght <=' => $transferringAgency->get('rght'),
                            ]
                        );
                    $originatingAgencies[$transferringAgency->get('id')] = $query->toArray();
                } else {
                    $originatingAgencies[$transferringAgency->get('id')] = [
                        $transferringAgency->get('id') => sprintf(
                            '%s - %s',
                            $transferringAgency->get('identifier'),
                            $transferringAgency->get('name')
                        ),
                    ];
                }
            }
        }
        $forms = Hash::combine($forms, '{n}.id', '{n}.name');

        if ($request->is('post')) {
            if (!isset($forms[(int)$request->getData('form_id')])) {
                throw new BadRequestException();
            }
            $transferringsIds = array_unique(
                array_reduce(
                    array_map('array_keys', $transferringsAgencies),
                    'array_merge',
                    []
                )
            );
            if (!in_array((int)$request->getData('transferring_agency_id'), $transferringsIds)) {
                throw new BadRequestException();
            }

            $data = [
                'archival_agency_id' => $this->archivalAgencyId,
                'state' => $this->Deposits->initialState,
                'created_user_id' => $this->userId,
                'last_state_update' => new FrozenTime(),
                'bypass_validation' => false,
            ] + $request->getData();
            $entity = $this->Deposits->newEntity($data);
            $this->loadComponent('AsalaeCore.Modal');
            if ($this->Deposits->save($entity)) {
                $this->Modal->success();
                return $this->Modal->step('addDepositStep2(' . $entity->get('id') . ')')
                    ->withType('json')
                    ->withStringBody(json_encode($entity->toArray()));
            } else {
                return $this->Modal->fail();
            }
        }

        $this->set('sedaGeneratorIsDown', FormMakerForm::pingSedaGenerator() === false);

        // options
        $this->set('transferringsAgencies', $transferringsAgencies);
        $this->set('originatingAgencies', $originatingAgencies);
        $this->set('forms', $forms);
    }

    /**
     * Saisie du versement
     * @param string $id
     * @return Response|void
     * @throws Exception
     */
    public function add2(string $id)
    {
        $deposit = $this->Deposits->find()
            ->where(
                [
                    'Deposits.id' => $id,
                    'Deposits.archival_agency_id' => $this->archivalAgencyId,
                    'Deposits.state' => $this->Deposits->initialState,
                    'Deposits.created_user_id' => $this->userId,
                ]
            )
            ->contain(
                [
                    'Forms' => [
                        'FormFieldsets' => [
                            'sort' => 'ord',
                            'FormInputs' => [
                                'sort' => 'ord',
                            ],
                        ],
                    ],
                    'DepositValues' => [
                        'sort' => 'fieldset_index',
                        'FormInputs' => ['FormFieldsets'],
                    ],
                ]
            )
            ->firstOrFail();

        $this->set('sedaGeneratorIsDown', FormMakerForm::pingSedaGenerator() === false);

        $formEntity = $deposit->get('form');
        $form = FormMakerForm::create($formEntity->id, $this->userId, $deposit);

        $inputs = $this->getRequest()->getData('inputs', [])
            + $this->Deposits->getDepositFormData($deposit);
        $form->setData($inputs);
        $this->set('inputData', $inputs);

        $fileuploads = [];
        $request = $this->getRequest();
        if ($form->isValidForm() && $request->is(['post', 'put'])) {
            $data = [
                'archival_agency_id' => $this->archivalAgencyId,
                'state' => $this->Deposits->initialState,
                'created_user_id' => $this->userId,
            ] + $request->getData();
            $transferring_agency_id = $deposit->get('transferring_agency_id')
                ?: $this->archivalAgencyId;
            $additionnalData = [
                'additionnalData' => [
                    'user_id' => $this->userId,
                    'org_entity_id' => $this->orgEntityId,
                    'archival_agency_id' => $this->archivalAgencyId,
                    'transferring_agency_id' => $transferring_agency_id,
                    'originating_agency_id' => $deposit->get('originating_agency_id')
                        ?: $transferring_agency_id,
                ],
                'hidden_fields' => $request->getData('hidden_fields'),
            ];
            $this->loadComponent('AsalaeCore.Modal');
            if ($request->getData('bypass_validation')) {
                $deposit = $this->Deposits->updateDeposit($deposit, $data + $additionnalData);
                $this->Deposits->saveOrFail($deposit);
                $xml = $deposit->get('path') . '/transfer.xml';
                if (is_file($xml)) {
                    Filesystem::remove($xml);
                }
                $this->Modal->success();

                return $this->renderDataToJson($deposit->toArray())
                    ->withHeader('X-Asalae-Bypass-Validation', 'true');
            } elseif ($form->validate($request->getData('inputs', []) + $additionnalData)) {
                $deposit->set(
                    'request_data',
                    serialize(
                        [
                            'data' => $data,
                            'additionnalData' => $additionnalData,
                            'send' => $request->getData('send') ?: false,
                        ]
                    )
                );
                $this->Deposits->transitionOrFail($deposit, DepositsTable::T_GENERATE);
                $data = $data + $additionnalData;
                unset($data['state']); // sinon on sauvegarde le mauvais état issu de la requête
                $deposit = $this->Deposits->updateDeposit($deposit, $data);

                /** @var Beanstalk $Beanstalk */
                $Beanstalk = Utility::get('Beanstalk');
                if (!$Beanstalk->isConnected()) {
                    throw new ServiceUnavailableException();
                }
                $Beanstalk->setTube('generate-deposit')
                    ->emit(
                        [
                            'deposit_id' => $deposit->get('id'),
                            'user_id' => $this->userId,
                            'send' => $request->getData('send'),
                        ],
                        1024,
                        2 // laisse le temps à la modale de se recharger
                    );

                $this->Modal->success();
                return $this->Modal->step('addGeneratingStep(' . $deposit->get('id') . ')')
                    ->withType('json')
                    ->withStringBody(json_encode($deposit->toArray()));
            } else {
                FormatError::logFormErrors($form);
                $this->Modal->fail();
                $this->Fileuploads = $this->fetchTable('Fileuploads');
                $fileuploads = FormMakerForm::extractFileuploadsFromRequestData(
                    $this->userId,
                    $this->getRequest()->getData(),
                    $formEntity
                );
            }
        }
        $this->set('entity', $deposit);
        $this->set('sedaGeneratorIsDown', FormMakerForm::pingSedaGenerator() === false);
        $deposit->set('inputs', $inputs);
        $this->set('form', $form);
        $this->set('formEntity', $formEntity);
        $fileuploadList = [];
        foreach ($fileuploads as $values) {
            foreach ((array)$values as $v) {
                if ($v) {
                    $fileuploadList[Hash::get($v, 'id')] = Hash::get($v, 'name');
                }
            }
        }
        $this->set('fileuploadList', $fileuploadList);
        $this->set('fileuploads', $fileuploads);

        // options
        $this->setAddEditOptions($formEntity);
        $this->set(
            'ws_channel',
            sprintf(
                'form_maker_%d_%d_%s',
                $this->userId,
                $deposit->get('form_id'),
                md5($deposit->get('name') . '_salted') // NOSONAR md5 ok dans ce cas
            )
        );
    }

    /**
     * Interrogation ajax avant de proposer l'édition afin de vérifier si une nouvelle version du form existe
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function checkNewFormVersion(string $id): Response
    {
        $entity = $this->Deposits->find()
            ->where(
                [
                    'Deposits.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['Forms'])
            ->firstOrFail();

        $this->Forms = $this->fetchTable('Forms');
        $newerForm = $this->Forms->getNewerForm($entity->get('form'));
        $this->loadComponent('AsalaeCore.Modal');
        $this->Modal->success();

        return $this->renderDataToJson(['new_form' => (bool)$newerForm]);
    }

    /**
     * Change le formulaire du deposit (montée de version)
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function upgradeFormVersion(string $id): Response
    {
        $entity = $this->Deposits->find()
            ->where(
                [
                    'Deposits.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['Forms'])
            ->firstOrFail();

        $this->Forms = $this->fetchTable('Forms');
        $newerForm = $this->Forms->getNewerForm($entity->get('form'));

        if (!$newerForm) {
            throw new NotFoundException();
        }

        $this->loadComponent('AsalaeCore.Modal');
        $conn = $this->Deposits->getConnection();
        $conn->begin();
        try {
            $this->Deposits->changeForm($entity, $newerForm);
            $conn->commit();

            $this->Modal->success();
            return $this->renderDataToJson(['success' => true]);
        } catch (Exception $e) {
            $conn->rollback();

            $this->Modal->fail();
            return $this->renderDataToJson(
                [
                    'success' => false,
                    'error' => $e->getMessage(),
                ]
            );
        }
    }

    /**
     * Edition d'un versement
     * @param string $id
     * @return Response|void
     * @throws Exception
     */
    public function edit(string $id)
    {
        $deposit = $this->Deposits->find()
            ->where(
                [
                    'Deposits.id' => $id,
                    'Deposits.state IN' => [
                        DepositsTable::S_EDITING,
                        DepositsTable::S_GENERATING
                    ],
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(
                [
                    'Forms' => [
                        'FormFieldsets' => [
                            'sort' => 'ord',
                            'FormInputs' => [
                                'sort' => 'ord',
                            ],
                        ],
                    ],
                    'DepositValues' => [
                        'sort' => ['fieldset_index', 'DepositValues.id'],
                        'FormInputs' => ['FormFieldsets'],
                    ],
                ]
            )
            ->firstOrFail();
        $deposit->set('bypass_validation', false);

        if ($deposit->get('state') === DepositsTable::S_GENERATING) {
            $this->Flash->warning(
                __("Attention, l'état de ce versement est ''{0}''", $deposit->get('statetrad'))
            );
        }
        /** @var EntityInterface $formEntity */
        $formEntity = $deposit->get('form');
        $form = FormMakerForm::create($formEntity->id, $this->userId, $deposit);

        $this->Fileuploads = $this->fetchTable('Fileuploads');
        $fileuploads = [];
        /** @var EntityInterface $depositValue */
        foreach ($deposit->get('deposit_values') as $depositValue) {
            if (!$depositValue->get('input_value')) {
                continue;
            }
            $name = Hash::get($depositValue, 'form_input.name');
            $type = Hash::get($depositValue, 'form_input.type');
            if (
                $type === FormInputsTable::TYPE_FILE
                || $type === FormInputsTable::TYPE_ARCHIVE_FILE
            ) {
                $fileuploads[$name][$depositValue->get('input_value')] = $this->Fileuploads->find()
                    ->where(
                        [
                            'id' => $depositValue->get('input_value'),
                            'user_id' => $this->userId,
                        ]
                    )
                    ->first();
            }
        }
        $inputs = $this->getRequest()->getData('inputs', [])
            + $this->Deposits->getDepositFormData($deposit);

        $form->setData($inputs);
        $this->set('inputData', $inputs);

        $request = $this->getRequest();
        if ($request->is('put')) {
            $data = [
                'archival_agency_id' => $this->archivalAgencyId,
                'form_id' => $formEntity->id,
            ] + $request->getData();
            $transferring_agency_id = $deposit->get('transferring_agency_id')
                ?: $this->archivalAgencyId;
            $additionnalData = [
                'additionnalData' => [
                    'user_id' => $this->userId,
                    'org_entity_id' => $this->orgEntityId,
                    'archival_agency_id' => $this->archivalAgencyId,
                    'transferring_agency_id' => $transferring_agency_id,
                    'originating_agency_id' => $deposit->get('originating_agency_id')
                        ?: $transferring_agency_id,
                ],
                'hidden_fields' => $request->getData('hidden_fields'),
            ];
            $this->loadComponent('AsalaeCore.Modal');
            if ($request->getData('bypass_validation')) {
                $deposit = $this->Deposits->updateDeposit($deposit, $data + $additionnalData);
                $this->Deposits->saveOrFail($deposit);
                $xml = $deposit->get('path') . '/transfer.xml';
                if (is_file($xml)) {
                    Filesystem::remove($xml);
                }
                $this->Modal->success();

                return $this->renderDataToJson($deposit->toArray())
                    ->withHeader('X-Asalae-Bypass-Validation', 'true');
            } elseif ($form->validate($request->getData('inputs', []) + $additionnalData)) {
                $deposit->set(
                    'request_data',
                    serialize(
                        [
                            'data' => $data,
                            'additionnalData' => $additionnalData,
                        ]
                    )
                );
                $this->Deposits->transition($deposit, DepositsTable::T_GENERATE);
                $deposit->set('bypass_validation', true);
                $this->Deposits->saveOrFail($deposit);

                /** @var Beanstalk $Beanstalk */
                $Beanstalk = Utility::get('Beanstalk');
                if (!$Beanstalk->isConnected()) {
                    throw new ServiceUnavailableException();
                }
                $Beanstalk->setTube('generate-deposit')
                    ->emit(
                        [
                            'deposit_id' => $deposit->get('id'),
                            'user_id' => $this->userId,
                            'send' => $request->getData('send') ?: false,
                        ],
                        1024,
                        2 // laisse le temps à la modale de se recharger
                    );

                $this->Modal->success();
                return $this->Modal->step(
                    'editGeneratingStep(' . $deposit->get('id') . ')'
                );
            } else {
                FormatError::logFormErrors($form);
                $this->Modal->fail();
                $fileuploads = FormMakerForm::extractFileuploadsFromRequestData(
                    $this->userId,
                    $this->getRequest()->getData(),
                    $formEntity,
                    $fileuploads
                );
            }
        }
        $this->set('entity', $deposit);
        $this->set('sedaGeneratorIsDown', FormMakerForm::pingSedaGenerator() === false);
        $deposit->set('inputs', $inputs);
        $this->set('form', $form);
        $this->set('formEntity', $formEntity);
        $fileuploadList = [];
        foreach ($fileuploads as $name => $values) {
            foreach ((array)$values as $v) {
                if ($v) {
                    $fileuploadList[Hash::get($v, 'id')] = Hash::get($v, 'name');
                }
            }
            $fileuploads[$name] = array_values($values);
        }
        $this->set('fileuploadList', $fileuploadList);
        $this->set('fileuploads', $fileuploads);

        // options
        $this->setAddEditOptions($formEntity);

        $this->set(
            'ws_channel',
            sprintf(
                'form_maker_%d_%d_%s',
                $this->userId,
                $deposit->get('form_id'),
                md5($deposit->get('name') . '_salted') // NOSONAR md5 ok dans ce cas
            )
        );
    }

    /**
     * Action visualiser
     * @param string $id
     * @throws Exception
     */
    public function view(string $id): void
    {
        $entity = $this->Deposits->find()
            ->where(
                [
                    'Deposits.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(
                [
                    'DepositValues',
                    'Forms' => ['ArchivingSystems'],
                    'TransferringAgencies',
                    'Transfers',
                    'CreatedUsers',
                ]
            )
            ->firstOrFail();

        $this->set('stateOptions', $this->Deposits->options('state'));
        $this->set('entity', $entity);
    }

    /**
     * Résumé du versement avant l'envoit
     * @param string $id
     * @return void
     * @throws Exception
     */
    public function summarize(string $id): void
    {
        $deposit = $this->Deposits->find()
            ->where(
                [
                    'Deposits.id' => $id,
                    'Deposits.state IN' => [
                        DepositsTable::S_EDITING,
                        DepositsTable::S_GENERATING,
                        DepositsTable::S_SENDING_ERROR,
                    ],
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(
                [
                    'Forms' => [
                        'FormFieldsets' => [
                            'sort' => 'ord',
                            'FormInputs' => [
                                'sort' => 'ord',
                            ],
                        ],
                    ],
                    'DepositValues' => [
                        'sort' => 'fieldset_index',
                        'FormInputs' => ['FormFieldsets'],
                    ],
                ]
            )
            ->firstOrFail();
        /** @var EntityInterface $formEntity */
        $formEntity = $deposit->get('form');
        $form = FormMakerForm::create($formEntity->id, $this->userId, $deposit);

        $this->Fileuploads = $this->fetchTable('Fileuploads');
        $fileuploads = [];
        /** @var EntityInterface $depositValue */
        foreach ($deposit->get('deposit_values') as $depositValue) {
            if (!$depositValue->get('input_value')) {
                continue;
            }
            $name = Hash::get($depositValue, 'form_input.name');
            $type = Hash::get($depositValue, 'form_input.type');
            if (
                $type === FormInputsTable::TYPE_FILE
                || $type === FormInputsTable::TYPE_ARCHIVE_FILE
            ) {
                $fileuploads[$name][$depositValue->get('input_value')] = $this->Fileuploads->find()
                    ->where(
                        [
                            'id' => $depositValue->get('input_value'),
                            'user_id' => $this->userId,
                        ]
                    )
                    ->first();
            }
        }
        $inputs = $this->Deposits->getDepositFormData($deposit);

        $form->setData($inputs);
        $this->set('inputData', $inputs);

        $this->set('entity', $deposit);
        $this->set('sedaGeneratorIsDown', FormMakerForm::pingSedaGenerator() === false);
        $deposit->set('inputs', $inputs);
        $this->set('form', $form);
        $this->set('formEntity', $formEntity);
        $fileuploadList = [];
        foreach ($fileuploads as $name => $values) {
            foreach ((array)$values as $v) {
                if ($v) {
                    $fileuploads[$name][Hash::get($v, 'id')] = $v;
                    $fileuploadList[Hash::get($v, 'id')] = Hash::get($v, 'name');
                }
            }
            $fileuploads[$name] = array_values($fileuploads[$name]);
        }
        $this->set('fileuploadList', $fileuploadList);
        $this->set('fileuploads', $fileuploads);

        // options
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $query = $this->OrgEntities->listOptionsWithoutSeal()
            ->innerJoinWith('FormsTransferringAgencies')
            ->where(['FormsTransferringAgencies.form_id' => $formEntity->id]);
        if (Hash::get($this->orgEntity, 'type_entity.code') === 'SA') {
            $query->andWhere(
                [
                    'OrgEntities.lft >=' => $this->archivalAgency->get('lft'),
                    'OrgEntities.rght <=' => $this->archivalAgency->get('rght'),
                ]
            );
        } elseif (Hash::get($this->user, 'role.hierarchical_view')) {
            $query->andWhere(
                [
                    'OrgEntities.lft >=' => $this->orgEntity->get('lft'),
                    'OrgEntities.rght <=' => $this->orgEntity->get('rght'),
                ]
            );
        } else {
            $query->andWhere(['OrgEntities.id' => $this->orgEntityId]);
        }
        $this->set('transferringAgencies', $query->toArray());
        $this->set(
            'ws_channel',
            sprintf(
                'form_maker_%d_%d_%s',
                $this->userId,
                $deposit->get('form_id'),
                md5($deposit->get('name') . '_salted') // NOSONAR md5 ok dans ce cas
            )
        );
    }

    /**
     * Action supprimer
     * @param string $id
     * @return Response
     */
    public function delete(string $id): Response
    {
        $this->getRequest()->allowMethod('delete');
        $entity = $this->Deposits->find()
            ->where(
                [
                    'Deposits.id' => $id,
                    'Deposits.archival_agency_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();

        $report = $this->Deposits->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Action envoyer
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function send(string $id): Response
    {
        $this->getRequest()->allowMethod(['post', 'put']);
        /** @var Beanstalk $Beanstalk */
        $Beanstalk = Utility::get('Beanstalk');
        if (!$Beanstalk->isConnected()) {
            throw new ServiceUnavailableException();
        }

        $this->Counters = $this->fetchTable('Counters');
        $this->Transfers = $this->fetchTable('Transfers');

        $deposit = $this->Deposits->find()
            ->where(
                [
                    'Deposits.id' => $id,
                    'Deposits.created_user_id' => $this->userId,
                    'Deposits.state IN' => [
                        DepositsTable::S_EDITING,
                        DepositsTable::S_GENERATING,
                        DepositsTable::S_SENDING_ERROR,
                    ],
                ]
            )
            ->contain(
                [
                    'Forms' => [
                        'FormTransferHeaders' => fn(Query $q)
                            => $q->where(['name IN' => ['TransferIdentifier', 'MessageIdentifier']]),
                    ],
                ]
            )
            ->firstOrFail();

        $resending = $deposit->get('state') === DepositsTable::S_SENDING_ERROR;

        $conn = $this->Deposits->getConnection();
        $conn->begin();
        try {
            // si un identifier est défini dans le formulaire
            if (Hash::get($deposit, 'form.form_transfer_headers.0.id')) {
                $precontrol = new PreControlMessages(
                    $deposit->get('path') . '/transfer.xml'
                );
                $identifier = $precontrol->getTransferIdentifier();
            }
            $transfer = $this->Transfers->newEntity(
                [
                    'deposit_id' => $deposit->id,
                    'identifier' => $identifier ?? $this->Counters->next(
                        $this->archivalAgencyId,
                        'ArchiveTransfer'
                    ),
                    'created_user_id' => $this->userId,
                ]
            );
            if ($resending) {
                $path = $deposit->get('path');
                $files = Filesystem::listFiles($path . DS . 'original_data');
                $size = array_reduce(
                    $files,
                    fn($size, $a) => $size + filesize($a)
                );
                $transfer->set('data_count', count($files));
                $transfer->set('data_size', $size);
            }

            $this->Transfers->saveOrFail($transfer);
            $this->Deposits->transitionOrFail($deposit, DepositsTable::T_SEND);
            $this->Deposits->saveOrFail($deposit);

            $tube = $resending ? 'transfer-send' : 'transfer-build';
            $Beanstalk->setTube($tube)->emit(
                ['transfer_id' => $transfer->id, 'deposit_id' => $deposit->id, 'user_id' => $this->userId],
                PheanstalkInterface::DEFAULT_PRIORITY,
                1 // le temps de sortir de la transaction
            );
        } catch (Exception $e) {
            $conn->rollback();
            throw $e;
        }
        $conn->commit();

        return $this->renderDataToJson(['report' => 'done'] + $deposit->toArray());
    }

    /**
     * Action rééditer (passer de erreur/annulé/rejetté à l'état édition)
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function reedit(string $id): Response
    {
        $entity = $this->Deposits->find()
            ->where(
                [
                    'Deposits.id' => $id,
                    'Deposits.state IN' => [
                        DepositsTable::S_PREPARING_ERROR,
                        DepositsTable::S_SENDING_ERROR,
                        DepositsTable::S_CANCELED,
                        DepositsTable::S_REJECTED,
                    ],
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['Forms'])
            ->firstOrFail();

        $this->Forms = $this->fetchTable('Forms');

        $this->loadComponent('AsalaeCore.Modal');
        if ($this->Deposits->reedit($entity)) {
            $this->Modal->success();
            return $this->renderDataToJson($entity);
        }
        return $this->getResponse()
            ->withHeader('X-Asalae-Success', 'false')
            ->withStringBody(
                FormatError::logEntityErrors($entity) ?: __("Erreur lors de l'enregistrement")
            );
    }

    /**
     * Action annuler
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function cancel(string $id): Response
    {
        $entity = $this->Deposits->find()
            ->where(
                [
                    'Deposits.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(
                [
                    'Forms',
                    'Transfers',
                ]
            )
            ->firstOrFail();

        if (!$entity->get('cancelable')) {
            throw new NotFoundException();
        }

        $success = $this->Deposits->transition($entity, DepositsTable::T_CANCEL)
            && $this->Deposits->save($entity);

        $this->loadComponent('AsalaeCore.Modal');
        if ($success) {
            $this->Modal->success();
            return $this->renderDataToJson($entity);
        } else {
            $report = [
                'success' => false,
                'errors' => $entity->getErrors(),
            ];
            $this->Modal->fail();
            return $this->renderDataToJson($report);
        }
    }

    /**
     * Téléchargement du bordereau, seulement si transfers créé
     * @param string $id
     * @return Response
     */
    public function getXml(string $id): Response
    {
        $entity = $this->Deposits->find()
            ->where(
                [
                    'Deposits.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(
                [
                    'Forms',
                    'Transfers',
                ]
            )
            ->firstOrFail();

        $identifier = Hash::get($entity, 'transfer.identifier');
        if ($identifier === null) {
            throw new NotFoundException();
        }

        $basedir = Hash::get($entity, 'path');
        $file = sprintf(
            '%s/transfer.xml',
            $basedir
        );
        return $this->getResponse()->withFile(
            $file,
            ['download' => true, 'name' => $identifier . '.xml']
        );
    }

    /**
     * Liste les enregistrements
     * @param Query $query
     * @return Response|null
     * @throws Exception
     */
    private function indexCommons(Query $query)
    {
        $this->loadComponent('AsalaeCore.Index');
        $this->Index->init();

        $query
            ->innerJoinWith('Forms')
            ->leftJoinWith('Transfers')
            ->contain(
                [
                    'DepositValues',
                    'Forms' => ['ArchivingSystems'],
                    'Transfers',
                    'CreatedUsers',
                ]
            );

        $this->Index->setQuery($query)
            ->filter('archiving_system_id', null, 'Forms.archiving_system_id')
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter('form', null, 'Forms.identifier')
            ->filter('name', IndexComponent::FILTER_ILIKE)
            ->filter('state', IndexComponent::FILTER_IN)
            ->filter('transfer_created', IndexComponent::FILTER_DATEOPERATOR, 'Transfers.created')
            ->filter('created_user_id');

        try {
            $data = $this->paginate($query)
                ->map(
                    function (EntityInterface $entity) {
                        $entity->setVirtual(
                            [
                                'cancelable',
                                'deletable',
                                'editable',
                                'reeditable',
                                'sendable',
                                'statetrad',
                            ]
                        );
                        return $entity;
                    }
                )
                ->toArray();
        } catch (NotFoundException) {
            $this->Flash->error(__("La page demandée n'existe plus, retour à la page 1"));
            return $this->redirect('/deposits/index');
        }
        $this->set('data', $data);

        // options
        $this->set('states', $this->Deposits->options('state'));

        $this->Forms = $this->fetchTable('Forms');

        $forms = $this->Forms->find('list', ['keyField' => 'identifier'])
            ->innerJoinWith('Deposits')
            ->where(['Forms.org_entity_id' => $this->archivalAgencyId])
            ->orderAsc('Forms.name');
        $this->set('formsOptions', $forms);

        $this->ArchivingSystems = $this->fetchTable('ArchivingSystems');
        $archivingSystems = $this->ArchivingSystems->find('list')
            ->where(
                [
                    'org_entity_id' => $this->archivalAgencyId,
                    'active' => true,
                ]
            )
            ->order(['name' => 'asc']);
        $this->set('archivingSystemsOptions', $archivingSystems);

        $this->Users = $this->fetchTable('Users');
        $users = $this->Users->find('list', ['valueField' => 'username'])
            ->where(
                [
                    'org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->order(['username' => 'asc']);
        $this->set('createdUsers', $users);

        // override IndexComponent
        if ($this->getRequest()->is('ajax') && $this->viewBuilder()->getLayout() === null) {
            $this->viewBuilder()->setTemplate('ajax_index');
        }
    }

    /**
     * Page d'attente de la génération du versement
     * @param string $id
     * @return void
     */
    public function generating(string $id)
    {
        $deposit = $this->Deposits->find()
            ->where(
                [
                    'Deposits.id' => $id,
                    'Deposits.archival_agency_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['Forms'])
            ->firstOrFail();
        $this->set('deposit', $deposit);
    }

    /**
     * Options en commun entre add2 et edit
     * @param EntityInterface $formEntity
     * @return void
     */
    private function setAddEditOptions(EntityInterface $formEntity)
    {
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $query = $this->OrgEntities->find()
            ->select(
                [
                    'OrgEntities.id',
                    'OrgEntities.name',
                    'OrgEntities.identifier',
                    'OrgEntities.lft',
                    'OrgEntities.rght',
                    'archival_agency' => 'ArchivalAgencies.name',
                    'path' => $this->OrgEntities->queryParentGroupSelect(),
                ]
            )
            ->contain(['ArchivalAgencies', 'TypeEntities'])
            ->order(['OrgEntities.lft', 'OrgEntities.identifier'])
            ->innerJoinWith('FormsTransferringAgencies')
            ->where(['FormsTransferringAgencies.form_id' => $formEntity->id]);
        if (Hash::get($this->orgEntity, 'type_entity.code') === 'SA') {
            $query->andWhere(
                [
                    'OrgEntities.lft >=' => $this->archivalAgency->get('lft'),
                    'OrgEntities.rght <=' => $this->archivalAgency->get('rght'),
                ]
            );
        } elseif (Hash::get($this->user, 'role.hierarchical_view')) {
            $query->andWhere(
                [
                    'OrgEntities.lft >=' => $this->orgEntity->get('lft'),
                    'OrgEntities.rght <=' => $this->orgEntity->get('rght'),
                ]
            );
        } else {
            $query->andWhere(['OrgEntities.id' => $this->orgEntityId]);
        }
        $transferringAgencies = [];
        $originatingAgencies = [];
        /** @var EntityInterface $transferringAgency */
        foreach ($query as $transferringAgency) {
            $parents = $transferringAgency->get('path');
            $key = $parents ?: $transferringAgency->get('archival_agency');
            $transferringAgencies[$key][$transferringAgency->get('id')]
                = $transferringAgency->get('identifier') . ' - ' . $transferringAgency->get('name');

            if (Hash::get($this->user, 'role.hierarchical_view')) {
                $q = $this->OrgEntities->find(
                    'list',
                    [
                        'valueField' => function (EntityInterface $entity) {
                            return $entity->get('identifier') . ' - ' . $entity->get('name');
                        },
                    ]
                )
                    ->innerJoinWith('TypeEntities')
                    ->where(
                        [
                            'TypeEntities.code IN' => OrgEntitiesTable::CODE_ORIGINATING_AGENCIES,
                            'OrgEntities.lft >=' => $transferringAgency->get('lft'),
                            'OrgEntities.rght <=' => $transferringAgency->get('rght'),
                        ]
                    );
                $originatingAgencies[$transferringAgency->get('id')] = $q->toArray();
            } else {
                $originatingAgencies[$transferringAgency->get('id')] = [
                    $transferringAgency->get('id')
                        => $transferringAgency->get('identifier') . ' - ' . $transferringAgency->get('name'),
                ];
            }
        }
        $this->set('transferringAgencies', $transferringAgencies);
        $this->set('originatingAgencies', $originatingAgencies);
    }

    /**
     * Vérifi l'état de la génération du bordereau
     * @param string $id
     * @return Response
     */
    public function checkGenerating(string $id)
    {
        $lastJob = $this->fetchTable('BeanstalkJobs')
            ->find()
            ->select(
                [
                    'BeanstalkJobs.tube',
                    'BeanstalkJobs.created',
                    'BeanstalkJobs.errors',
                    'BeanstalkJobs.beanstalk_worker_id',
                    'BeanstalkJobs.job_state',
                ]
            )
            ->innerJoinWith('Deposits')
            ->where(
                [
                    'Deposits.id' => $id,
                    'Deposits.archival_agency_id' => $this->archivalAgencyId,
                ]
            )
            ->orderDesc('BeanstalkJobs.id')
            ->disableHydration()
            ->first()
            ?: [];
        $lastJob['working_workers'] = $this->fetchTable('BeanstalkWorkers')
            ->exists(['tube' => 'generate-deposit']);
        return $this->renderDataToJson($lastJob);
    }
}
