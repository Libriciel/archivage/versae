<?php

/**
 * Versae\Controller\Traits\PermTrait
 */

namespace Versae\Controller\Traits;

use AsalaeCore\Controller\ApiInterface;
use Cake\Core\Configure;
use Versae\Controller\AppController;

/**
 * ArchiveKeywords
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin AppController
 */
trait PermTrait
{
    /**
     * Défini le niveau api des permissions
     */
    protected function setApis()
    {
        $path = Configure::read('App.paths.apis_rules');
        $basePath = Configure::read('App.namespace') . '\\Controller\\';
        $apis = [];
        foreach (
            json_decode(file_get_contents($path), true)
                ?: [] as $controller => $actions
        ) {
            /** @var string|AppController $classname */
            $classname = $basePath . $controller . 'Controller';
            if (
                !class_exists($classname)
                || !in_array(
                    ApiInterface::class,
                    class_implements($classname)
                )
            ) {
                continue;
            }
            $actions = array_filter(
                $actions,
                function ($v) {
                    return empty($v['invisible']) && empty($v['commeDroits']);
                }
            );
            $apis[$controller] = $actions;
            if (empty($actions)) {
                unset($apis[$controller]);
            }
        }
        $this->set('apis', $apis);
    }
}
