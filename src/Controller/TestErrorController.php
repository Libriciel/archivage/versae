<?php

/**
 * Versae\Controller\TestErrorController
 */

namespace Versae\Controller;

use Cake\Event\EventInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\HttpException;
use Cake\Http\Exception\UnauthorizedException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Exception\ConflictException;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\GoneException;
use Cake\Http\Exception\MethodNotAllowedException;
use Cake\Http\Exception\NotAcceptableException;
use Cake\Http\Exception\NotImplementedException;
use Cake\Http\Exception\ServiceUnavailableException;
use Cake\Http\Exception\UnavailableForLegalReasonsException;
use Cake\Http\Response as CakeResponse;
use Exception;

/**
 * Permet de tester tout types d'erreurs
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TestErrorController extends AppController
{
    /**
     * Called before the controller action. You can use this method to configure and customize components
     * or perform logic that needs to happen before each controller action.
     *
     * @param EventInterface $event An Event instance
     * @return CakeResponse|void|null
     * @link https://book.cakephp.org/4/en/controllers.html#request-life-cycle-callbacks
     */
    public function beforeFilter(EventInterface $event)
    {
        $this->Authentication->allowUnauthenticated(
            [$this->getRequest()->getParam('action')]
        );
        return parent::beforeFilter($event);
    }

    /**
     * Test un code d'erreur particulier
     * @param string $code
     * @throws Exception
     */
    public function code(string $code)
    {
        switch ($code) {
            case 400:
                throw new BadRequestException();
            case 401:
                throw new UnauthorizedException();
            case 403:
                throw new ForbiddenException();
            case 404:
                throw new NotFoundException();
            case 405:
                throw new MethodNotAllowedException();
            case 406:
                throw new NotAcceptableException();
            case 409:
                throw new ConflictException();
            case 410:
                throw new GoneException();
            case 451:
                throw new UnavailableForLegalReasonsException();
            case 501:
                throw new NotImplementedException();
            case 503:
                throw new ServiceUnavailableException();
            default:
                throw new HttpException("", (int)$code);
        }
    }

    /**
     * Envoi une exception choisie
     * @param mixed ...$classname
     */
    public function throwException(...$classname)
    {
        $classname = '\\' . implode('\\', $classname);
        throw new $classname('');
    }
}
