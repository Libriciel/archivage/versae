<?php

/**
 * Versae\Controller\FormUnitKeywordsController
 */

namespace Versae\Controller;

use AsalaeCore\Form\MessageSchema\Seda10Schema;
use AsalaeCore\Utility\FormatError;
use Cake\Http\Response;
use Cake\Utility\Hash;
use Exception;
use Versae\Model\Table\FormInputsTable;
use Versae\Model\Table\FormUnitKeywordDetailsTable;
use Versae\Model\Table\FormUnitKeywordsTable;
use AsalaeCore\Controller\RenderDataTrait;
use Versae\Model\Table\FormUnitsTable;
use Versae\Model\Table\FormVariablesTable;

/**
 * FormUnitKeywords
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property FormInputsTable FormInputs
 * @property FormUnitKeywordDetailsTable FormUnitKeywordDetails
 * @property FormUnitKeywordsTable FormUnitKeywords
 * @property FormUnitsTable FormUnits
 * @property FormVariablesTable FormVariables
 */
class FormUnitKeywordsController extends AppController
{
    use RenderDataTrait;

    /**
     * @var array Liste des normes seda avec leurs namespaces
     */
    public const NORMES_SEDA = [
        'fr:gouv:ae:archive:draft:standard_echange_v0.2' => 'seda0.2',
        'fr:gouv:culture:archivesdefrance:seda:v1.0' => 'seda1.0',
        'fr:gouv:culture:archivesdefrance:seda:v2.0' => 'seda2.0',
        'fr:gouv:culture:archivesdefrance:seda:v2.1' => 'seda2.1',
        'fr:gouv:culture:archivesdefrance:seda:v2.2' => 'seda2.2',
    ];

    /**
     * Action d'ajout
     * @param string $form_unit_id
     * @return Response|void
     * @throws Exception
     */
    public function add(string $form_unit_id)
    {
        $this->FormUnits = $this->fetchTable('FormUnits');
        $formUnit = $this->FormUnits->find()
            ->innerJoinWith('Forms')
            ->where(
                [
                    'FormUnits.id' => $form_unit_id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['Forms'])
            ->firstOrFail();
        $entity = $this->FormUnitKeywords->newEmptyEntity();
        $request = $this->getRequest();

        $keywordTypeOptions = Seda10Schema::getXsdOptions('keywordtype');

        if ($request->is('post')) {
            $data = [
                'name' => $request->getData('name'),
                'form_unit_id' => $form_unit_id,
                'multiple_with' => $request->getData('multiple_with'),
            ];
            $this->FormUnitKeywords->patchEntity($entity, $data);
            $this->loadComponent('AsalaeCore.Modal');
            $conn = $this->FormUnitKeywords->getConnection();
            $conn->begin();
            if ($success = $this->FormUnitKeywords->save($entity)) {
                $this->FormVariables = $this->fetchTable('FormVariables');
                $twig = $request->getData('multiple_with')
                    ? '{{multiple.value}}'
                    : $request->getData('name');
                $contentVar = $this->FormVariables->newEntity(
                    [
                        'form_id' => $formUnit->get('form_id'),
                        'type' => FormVariablesTable::TYPE_CONCAT,
                        'twig' => $twig,
                    ]
                );
                $success = $this->FormVariables->save($contentVar);
            }
            if ($success && isset($contentVar)) {
                $this->FormUnitKeywordDetails = $this->fetchTable('FormUnitKeywordDetails');
                $contentDetail = $this->FormUnitKeywordDetails->newEmptyEntity();
                $this->FormUnitKeywordDetails->patchEntity(
                    $contentDetail,
                    [
                        'form_unit_keyword_id' => $entity->id,
                        'name' => 'KeywordContent',
                        'form_variable_id' => $contentVar->id,
                    ]
                );
                $success = $this->FormUnitKeywordDetails->save($contentDetail);
                $contentDetail->set('form_variable', $contentVar);
                $details = [$contentDetail];

                if (
                    $success
                    && !$request->getData('multiple_with')
                    && $ref = $request->getData('reference')
                ) {
                    $referenceVar = $this->FormVariables->newEntity(
                        [
                            'form_id' => $formUnit->get('form_id'),
                            'type' => FormVariablesTable::TYPE_CONCAT,
                            'twig' => $ref,
                        ]
                    );
                    if ($success = $this->FormVariables->save($referenceVar)) {
                        $referenceDetail = $this->FormUnitKeywordDetails->newEmptyEntity();
                        $this->FormUnitKeywordDetails->patchEntity(
                            $referenceDetail,
                            [
                                'form_unit_keyword_id' => $entity->id,
                                'name' => 'KeywordReference',
                                'form_variable_id' => $referenceVar->id,
                            ]
                        );
                        $success = $this->FormUnitKeywordDetails->save($referenceDetail);

                        $referenceDetail->set('form_variable', $referenceVar);
                        $details[] = $referenceDetail;
                    }
                }

                if (
                    $success
                    && !$request->getData('multiple_with')
                    && ($type = $request->getData('type'))
                    && in_array($type, Hash::extract($keywordTypeOptions, '{n}.value'))
                ) {
                    $typeVar = $this->FormVariables->newEntity(
                        [
                            'form_id' => $formUnit->get('form_id'),
                            'type' => FormVariablesTable::TYPE_CONCAT,
                            'twig' => $type,
                        ]
                    );
                    if ($success = $this->FormVariables->save($typeVar)) {
                        $typeDetail = $this->FormUnitKeywordDetails->newEmptyEntity();
                        $this->FormUnitKeywordDetails->patchEntity(
                            $typeDetail,
                            [
                                'form_unit_keyword_id' => $entity->id,
                                'name' => 'KeywordType',
                                'form_variable_id' => $typeVar->id,
                            ]
                        );
                        $success = $this->FormUnitKeywordDetails->save($typeDetail);

                        $typeDetail->set('form_variable', $typeVar);
                        $details[] = $typeDetail;
                    }
                }

                $entity->set('form_unit_keyword_details', $details);
            }
            if ($success) {
                $conn->commit();
                $this->Modal->success();
                $this->Modal->step('actionEditFormKeyword(' . $entity->id . ')');
                return $this->renderDataToJson($entity->toArray());
            } else {
                $conn->rollback();
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
                if (isset($var)) {
                    FormatError::logEntityErrors($var);
                }
                if (isset($detail)) {
                    FormatError::logEntityErrors($detail);
                }
            }
        }

        $this->set('entity', $entity);

        // options
        $this->FormInputs = $this->fetchTable('FormInputs');
        $options = $this->FormInputs->listOptions($formUnit->get('form_id'));
        $this->set('fields_multiples', $this->FormInputs->filterMultiple($options, true, false));
        $this->set('types', $keywordTypeOptions);
    }

    /**
     * Action modifier
     * @param string $id
     * @return Response|void
     * @throws Exception
     */
    public function edit(string $id)
    {
        $entity = $this->FormUnitKeywords->find()
            ->innerJoinWith('FormUnits')
            ->innerJoinWith('FormUnits.Forms')
            ->where(
                [
                    'FormUnitKeywords.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(
                [
                    'FormUnits' => ['Forms'],
                    'FormUnitKeywordDetails' => ['FormVariables'],
                ]
            )
            ->firstOrFail();
        $this->set('entity', $entity);

        // pour le tableau de résultats des KeywordDetails
        $this->FormUnitKeywordDetails = $this->fetchTable('FormUnitKeywordDetails');
        $details = $this->FormUnitKeywordDetails->formKeywordData($entity);
        $this->set('keywordDetails', $details);
        $this->set('id', $id);

        $request = $this->getRequest();
        if ($request->is('put')) {
            $data = [
                'name' => $request->getData('name'),
                'multiple_with' => $request->getData('multiple_with'),
            ];
            $this->FormUnitKeywords->patchEntity($entity, $data);
            $this->loadComponent('AsalaeCore.Modal');
            if ($this->FormUnitKeywords->save($entity)) {
                $this->Modal->success();
                return $this->renderDataToJson($entity->toArray());
            }
        }

        // options
        $this->FormInputs = $this->fetchTable('FormInputs');
        $options = $this->FormInputs->listOptions(Hash::get($entity, 'form_unit.form_id'));
        $this->set('fields_multiples', $this->FormInputs->filterMultiple($options, true, false));
    }

    /**
     * Suppression d'un mot clé
     * @param string $id
     * @return Response
     */
    public function delete(string $id)
    {
        $entity = $this->FormUnitKeywords->find()
            ->innerJoinWith('FormUnits')
            ->innerJoinWith('FormUnits.Forms')
            ->where(
                [
                    'FormUnitKeywords.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();

        $report = $this->FormUnitKeywords->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        $this->Forms = $this->fetchTable('Forms');
        return $this->renderDataToJson(['report' => $report]);
    }
}
