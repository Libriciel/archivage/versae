<?php

/**
 * Versae\Controller\RolesController
 */

namespace Versae\Controller;

use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Utility\FormatError;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Http\Response;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Exception;
use Versae\Exception\GenericException;
use Versae\Form\PermissionsForm;
use Versae\Model\Table\AcosTable;
use Versae\Model\Table\ArosAcosTable;
use Versae\Model\Table\ArosTable;
use Versae\Model\Table\OrgEntitiesTable;
use Versae\Model\Table\RolesTable;
use Versae\Model\Table\TypeEntitiesTable;

/**
 * Rôles
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AcosTable Acos
 * @property ArosTable Aros
 * @property OrgEntitiesTable OrgEntities
 * @property ArosAcosTable ArosAcos
 * @property RolesTable Roles
 * @property TypeEntitiesTable TypeEntities
 */
class RolesController extends AppController implements ApiInterface
{
    use ApiTrait;
    use RenderDataTrait;
    use Traits\PermTrait;

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const TABLE_INDEX = 'roles-index-table';
    public const TABLE_INDEX_GLOBALS = 'roles-globals-index-table';
    public const TABLE_INDEX_WEBSERVICES = 'roles-webservices-index-table';

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return [
            'default',
        ];
    }

    /**
     * Modifier un rôle
     */
    public function index()
    {
        $this->Roles = $this->fetchTable('Roles');
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $this->set('userSa', $this->archivalAgency);
        TableRegistry::getTableLocator()->clear();
        $roles = $this->Roles->find()
            ->where(['org_entity_id IS' => null, 'agent_type' => 'person'])
            ->contain(['Aros'])
            ->order(['name' => 'asc'])
            ->all();
        $this->set('roles_globals', $roles);
        $wsRoles = $this->Roles->find()
            ->where(['org_entity_id IS' => null, 'agent_type' => 'software'])
            ->contain(['Aros'])
            ->order(['name' => 'asc'])
            ->all();
        $this->set('roles_webservices', $wsRoles);
        $roles = $this->Roles->find()
            ->where(['Roles.org_entity_id' => $this->archivalAgencyId, 'Roles.agent_type' => 'person'])
            ->contain(['Aros', 'ParentRoles'])
            ->order(['Roles.name' => 'asc'])
            ->all();
        $this->set('roles', $roles);
        $this->set('tableIdRoles', self::TABLE_INDEX);
        $this->set('tableIdRolesGlobaux', self::TABLE_INDEX_GLOBALS);
        $this->set('tableIdRolesWebservices', self::TABLE_INDEX_WEBSERVICES);

        $request = $this->getRequest();
        $this->loadComponent('AsalaeCore.Index');
        if ($request->getQuery('export_csv')) {
            $DevsController = new DevsController();
            $DevsController->permissions();
            $data = $DevsController->viewBuilder()->getVar('data');
            return $this->Index->renderCsv($data);
        } else {
            $this->Index->exportCsv(); // génère le bouton uniquement
        }
    }

    /**
     * Ajouter un rôle spécifique
     * @return Response
     * @throws Exception
     */
    public function add()
    {
        $this->Roles = $this->fetchTable('Roles');
        $entity = $this->Roles->newEntity(
            ['active' => true],
            ['validate' => false]
        );
        return $this->addEditCommon($entity, 'post');
    }

    /**
     * Modification d'un rôle
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function edit(string $id)
    {
        $this->Roles = $this->fetchTable('Roles');
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $conditions = [
            'Roles.id' => $id
        ];
        if (Configure::read('Roles.global_is_editable')) {
            $conditions['OR'] = [
                'Roles.org_entity_id' => $this->archivalAgencyId,
                'Roles.org_entity_id IS' => null,
            ];
        } else {
            $conditions['Roles.org_entity_id'] = $this->archivalAgencyId;
        }
        $entity = $this->Roles->find()
            ->where($conditions)
            ->contain(['TypeEntities'])
            ->firstOrFail();
        return $this->addEditCommon($entity, 'put');
    }

    /**
     * Code commun entre add et edit
     * @param EntityInterface $entity
     * @param string          $is     'post' ou 'put'
     * @return Response
     * @throws Exception
     */
    private function addEditCommon(EntityInterface $entity, string $is)
    {
        if ($this->getRequest()->is($is)) {
            $isNew = $entity->isNew();
            $data = [
                'org_entity_id' => $this->archivalAgencyId,
                'name' => $this->getRequest()->getData('name'),
                'description' => $this->getRequest()->getData('description'),
                'type_entities' => $this->getRequest()->getData('type_entities'),
                'parent_id' => $this->getRequest()->getData('parent_id'),
                'hierarchical_view' => $this->getRequest()->getData('hierarchical_view', false),
            ];
            if (($active = $this->getRequest()->getData('active')) !== null) {
                $data['active'] = $active;
            }
            $this->Roles->patchEntity($entity, $data);
            $this->loadComponent('AsalaeCore.Modal');
            $conn = $this->Roles->getConnection();
            $conn->begin();
            if ($this->Roles->save($entity)) {
                $this->Modal->success();
                if ($isNew) {
                    $this->initPermissions($entity);
                }
                $json = $this->Roles->find()
                    ->where(['Roles.id' => $entity->get('id')])
                    ->contain(['Aros'])
                    ->firstOrFail()
                    ->toArray();
                $conn->commit();
                return $this->renderDataToJson($json);
            } else {
                $conn->rollback();
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('entity', $entity);

        // options
        $this->TypeEntities = $this->fetchTable('TypeEntities');
        $typeEntities = $this->TypeEntities->find('list')
            ->where(
                [
                    'code IS NOT' => 'SE',
                    'active' => true,
                ]
            );
        $this->set('type_entities', $typeEntities);
        $sa = $this->archivalAgency;
        $roles = $this->Roles->find(
            'list',
            [
                'groupField' => function (EntityInterface $entity) {
                    /** @var EntityInterface $org */
                    $org = $entity->get('_matchingData')['OrgEntities'];
                    $name = $org->get('name');
                    if (empty($name)) {
                        return __("Global");
                    }
                    return $name;
                },
            ]
        )
            ->select(['Roles.id', 'Roles.name', 'OrgEntities.name'])
            ->leftJoinWith('OrgEntities')
            ->where(
                [
                    'OR' => [
                        'OrgEntities.id IS' => null,
                        [
                            'OrgEntities.lft >=' => $sa->get('lft'),
                            'OrgEntities.rght <=' => $sa->get('rght'),
                        ]
                    ],
                    'Roles.agent_type' => 'person',
                ]
            )
            ->order(['Roles.name' => 'asc']);
        if ($id = $entity->get('id')) {
            $roles->andWhere(
                [
                    'Roles.id !=' => $id,
                    'NOT' => [
                        'Roles.lft >=' => $entity->get('lft'),
                        'Roles.rght <=' => $entity->get('rght'),
                    ]
                ]
            );
        }
        $this->set('roles', $roles->all());

        $this->Users = $this->fetchTable('Users');
        $canBeDesactivated = $entity->isNew()
            || $this->Users->find()
                ->where(['role_id' => $entity->get('id')])->count() === 0;
        $this->set('canBeDesactivated', $canBeDesactivated);
    }

    /**
     * Suppression d'un rôle
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete(string $id)
    {
        $this->Roles = $this->fetchTable('Roles');
        $entity = $this->Roles->find()
            ->where(
                [
                    'Roles.id' => $id,
                    'Roles.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();

        $conn = $this->Roles->getConnection();
        $conn->begin();
        if ($this->Roles->delete($entity)) {
            $report = 'done';
            $conn->commit();
        } else {
            $report = 'Erreur lors de la suppression';
            $conn->rollback();
        }

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Ajoute les permissions par défaut
     *
     * @param EntityInterface $entity User
     * @return bool
     */
    private function initPermissions(EntityInterface $entity): bool
    {
        $path = Configure::read('App.paths.controllers_rules');
        $controllers = json_decode(file_get_contents($path), true);
        $this->Aros = $this->fetchTable('Aros');
        $aro = $this->Aros->find()
            ->where(['model' => 'Roles', 'foreign_key' => $entity->id])
            ->first();

        $success = true;
        foreach ($controllers as $controller => $actions) {
            foreach ($actions as $action => $params) {
                if (!empty($params['accesParDefaut'])) {
                    $success = $success
                        && $this->grantAccess(
                            $aro,
                            $controller,
                            $action,
                            $params
                        );
                }
            }
        }

        return $success;
    }

    /**
     * Donne à un aro, les droits d'accès sur un $controller::$action
     *
     * @param EntityInterface $aro
     * @param string          $controller
     * @param string          $action
     * @param array           $params
     * @return bool|EntityInterface
     */
    private function grantAccess(
        EntityInterface $aro,
        string $controller,
        string $action,
        array $params
    ) {
        $this->ArosAcos = $this->fetchTable('ArosAcos');
        $this->Acos = $this->fetchTable('Acos');
        if (!empty($params['commeDroits'])) {
            return true;
        }
        $aco = $this->Acos->find()
            ->where(['model' => $controller, 'alias' => $action])
            ->first();
        if (!$aco) {
            throw new GenericException(
                __("L'aco pour l'action {0}::{1} n'a pas été trouvé", $controller, $action)
            );
        }
        $entity = $this->ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => 1,
                '_read' => 1,
                '_update' => 1,
                '_delete' => 1,
            ]
        );
        return $this->ArosAcos->save($entity);
    }

    /**
     * Visualiser un rôle global (moins contraint que view)
     * @param string $id
     * @throws Exception
     */
    public function viewGlobal(string $id)
    {
        $this->viewBuilder()->setTemplate('view');
        $this->view($id);
    }

    /**
     * Action de visualisation d'un role et de ses permissions
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        $this->Roles = $this->fetchTable('Roles');
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $role = $this->Roles->find()
            ->where(
                [
                    'OR' => [
                        'Roles.org_entity_id' => $this->archivalAgencyId,
                        'Roles.org_entity_id IS' => null,
                    ],
                    'Roles.id' => $id,
                ]
            )
            ->contain(
                [
                    'ParentRoles',
                    'TypeEntities'
                ]
            )
            ->firstOrFail();
        $this->set('role', $role);

        // importé depuis PermissionsController::edit (et modifié)
        $path = Configure::read('App.paths.controllers_rules');
        $this->ArosAcos = $this->fetchTable('ArosAcos');
        $this->Aros = $this->fetchTable('Aros');

        $aro = $this->Aros->find()
            ->where(
                [
                    'Aros.model' => 'Roles',
                    'Aros.foreign_key' => $id,
                ]
            )
            ->firstOrFail();
        $form = new PermissionsForm();
        $this->set('form', $form);
        // Récupère la liste des permissions de l'aro (1 seul niveau)
        $currentPerms = Hash::expand($form->list($aro->get('id')));
        $this->set('defaults', $currentPerms);
        $accesses = [];
        /**
         * Lecture des permissions (héritage compris)
         */
        set_error_handler(fn() => null); // nécéssaire pour les tests unitaire malgrès le @
        foreach ($currentPerms as $type => $controllers) {
            foreach ($controllers as $controller => $values) {
                foreach ($values as $crud => $access) {
                    if ($crud === 'action') {
                        continue;
                    }
                    if ($access !== '0') {
                        $accesses[$type][$controller][$crud] = $access === '1';
                        continue;
                    }
                    $aco = $type . '/' . $controller;
                    $accesses[$type][$controller][$crud]
                        = @$this->Authorization->Acl->check(
                            $aro->get('id'),
                            $aco,
                            $crud
                        );
                }
                foreach ($values['action'] ?? [] as $action => $access) {
                    if ($access !== '0') {
                        $accesses[$type][$controller]['action'][$action] = $access === '1';
                        continue;
                    }
                    $aco = $type . '/' . $controller . '/' . $action;
                    $accesses[$type][$controller]['action'][$action]
                        = @$this->Authorization->Acl->check(
                            $aro->get('id'),
                            $aco
                        );
                }
            }
        }
        restore_error_handler();
        $this->set('accesses', $accesses);
        $controllers = json_decode(file_get_contents($path), true);
        foreach ($controllers as $controller => $actions) {
            $actions = array_filter(
                $actions,
                function ($v) {
                    return empty($v['invisible']) && empty($v['commeDroits']);
                }
            );
            $controllers[$controller] = $actions;
            if (empty($actions)) {
                unset($controllers[$controller]);
            }
        }
        $this->set('controllers', $controllers);
        $this->set('aro', $aro);
        $this->set('aro_id', $aro->get('id'));
    }

    /**
     * Visualisation d'une permission (webservice)
     *
     * @param string $aro_id
     * @throws Exception
     */
    public function viewWebservice(string $aro_id)
    {
        $this->setApis();
        $this->view($aro_id);
    }
}
