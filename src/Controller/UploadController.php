<?php

/**
 * Versae\Controller\UploadController
 */

namespace Versae\Controller;

use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Http\Response;
use AsalaeCore\View\Helper\PrevHelper;
use Cake\Core\Configure;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Response as CakeResponse;
use Cake\Utility\Hash;
use Cake\View\View;
use Exception;
use FileConverters\Utility\FileConverters;
use Flow\Basic;
use Flow\Config;
use Flow\File;
use Flow\Request;
use Libriciel\Filesystem\Utility\Filesystem;
use Psr\Http\Message\MessageInterface;
use Versae\Model\Entity\Fileupload;
use Versae\Model\Table\FileuploadsTable;

/**
 * Upload de fichiers
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property FileuploadsTable $Fileuploads
 */
class UploadController extends AppController
{
    use RenderDataTrait;

    /**
     * @var FileUpload
     */
    private $entity;

    /**
     * Upload du fichier
     * (appelé par ajax via <b>new Flow({target: '/upload'})</b>)
     *
     * @param string $validation Fonction de validation du modèle
     * @return Response|CakeResponse
     * @throws Exception
     */
    public function index(string $validation = 'default')
    {
        $this->setResponse($this->getResponse()->withHeader('Connection', 'close'));

        $path = Configure::read('Upload.dir', TMP . 'upload') . DS . 'user_' . $this->userId;
        if (!file_exists($path)) {
            Filesystem::mkdir($path);
        }
        return $this->uploadTo($path, $validation);
    }

    /**
     * Upload d'un fichier sur formulaire
     * @param string $formInputId
     * @return Response|CakeResponse
     * @throws Exception
     */
    public function formFile(string $formInputId)
    {
        $this->setResponse($this->getResponse()->withHeader('Connection', 'close'));
        $path = Configure::read('Upload.dir', Configure::read('App.paths.data'))
            . "/upload/user_$this->userId/$formInputId";

        if (!file_exists($path)) {
            Filesystem::mkdir($path);
        }
        return $this->uploadTo($path);
    }

    /**
     * Sauvegarde les informations relative au fichier en base
     *
     * @param  array  $report
     * @param  string $validation Fonction de validation du modèle
     * @return Response|CakeResponse
     * @throws Exception
     */
    private function saveFileinfo(array &$report, string $validation = 'default')
    {
        if (!isset($report['path']) || !is_readable($report['path'])) {
            throw new Exception(__("Impossible de lire le fichier"));
        }
        $report['completion'] = date("Y-m-d H:i:s");
        $report['mime'] = mime_content_type($report['path']);

        // Envoi les informations à la vue avant de lancer la validation
        /** @var Response $response */
        $buffer = '{"report": {';
        foreach ($report as $key => $value) {
            $buffer .= '"' . $key . '": ' . json_encode($value) . ', ';
        }
        $response = $this->getResponse()->withStringBody($buffer)->withType('json');
        $response->partialEmit();
        $buffer = '';

        $reportAfter = [];
        try {
            $this->Fileuploads = $this->fetchTable('Fileuploads');
            $entity = $this->Fileuploads->newUploadedEntity(
                $report['filename'],
                $report['path'],
                $this->userId ?: null,
                ['validate' => $validation]
            );
            $this->entity = $entity;

            if ($this->Fileuploads->save($entity)) {
                $reportAfter['id'] = $entity->id;
                $reportAfter['statetrad'] = $entity->statetrad;
                $reportAfter['pronom'] = isset($entity->siegfrieds[0]) ? $entity->siegfrieds[0]->pronom : null;
                $reportAfter['openable'] = $entity->openable;
                $reportAfter['hash'] = $entity->hash;
                $reportAfter['hash_algo'] = $entity->hash_algo;
                $Prev = new PrevHelper(new View());
                $reportAfter['prev'] = $Prev->create($reportAfter['id'], $report['filename'], $report['path']);
                $reportAfter['convertable']
                    = FileConverters::matchFormats($report['path']);
            } else {
                if (is_file($report['path'])) {
                    Filesystem::remove($report['path']);
                }
                $errors = Hash::flatten($entity->getErrors());
                $reportAfter['error'] = end($errors);
                throw new BadRequestException();
            }
            $output = [];
            foreach ($reportAfter as $key => $value) {
                $output[] = '"' . $key . '": ' . json_encode($value);
            }
            $buffer .= implode(', ', $output) . '}';
        } catch (Exception $e) {
            $output = [
                'error' => $reportAfter['error'] ?? $e->getMessage(),
                'message' => $e->getMessage(),
                'report' => $reportAfter,
                'file' => base64_encode($report['path'])
            ];
            if (Configure::read('debug')) {
                $prev = $e->getPrevious();
                $output += [
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                    'previous' => $prev
                        ? [
                            'message' => $prev->getMessage(),
                            'file' => $prev->getFile(),
                            'line' => $prev->getLine(),
                            'trace' => explode(
                                "\n",
                                str_replace(ROOT, 'ROOT', $prev->getTraceAsString())
                            )
                        ]
                        : null
                ];
            }
            return $response->withStringBody(
                substr(json_encode($output), 1) . '}'
            );
        }
        $buffer .= "}";
        return $response->withStringBody($buffer);
    }

    /**
     * Supprime un fichier uploadé
     *
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function delete(string $id)
    {
        $this->Fileuploads = $this->fetchTable('Fileuploads');

        $entity = $this->Fileuploads->find()
            ->where(['id' => $id])
            ->andWhere($this->userId ? ['user_id' => $this->userId] : ['user_id IS' => null])
            ->firstOrFail();

        $delete = $this->Fileuploads->delete($entity);

        $unlink = true;
        if (is_writable($entity->path)) {
            $unlink = @unlink($entity->path);
        }

        $report = $delete && $unlink
            ? 'done'
            : (!$delete ? __("Erreur lors de la suppression en base (id: {0}) ", $id) : '')
            . (!$unlink ? __("Erreur lors de la suppression du fichier") : '');

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Upload générique
     * @param string $path
     * @param string $validation
     * @return CakeResponse|MessageInterface|Response
     * @throws Exception
     */
    private function uploadTo(string $path, $validation = 'default')
    {
        $this->setResponse($this->getResponse()->withHeader('Connection', 'close'));

        $config = new Config();
        $config->setTempDir($path);
        $request = new Request($this->getRequest()->getData() + $this->getRequest()->getQuery());

        $id = $request->getIdentifier();
        if (empty($id)) {
            throw new Exception(__("Une erreur inattendue a eu lieu"));
        }
        $finalPath = $path . DS . $request->getFileName();

        $file = $request->getFile() ?: ['error' => 'not found'];
        $name = $request->getFileName();
        $report = [
            'uid' => $id,
            'filename' => $name,
            'name' => $name,
            'path' => $finalPath,
            'completion' => false,
            'error' => $file['error']
        ];
        if ($this->getRequest()->is('post')) {
            switch ($report['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_INI_SIZE:
                    throw new Exception(
                        __(
                            "Problème de configuration serveur ({0}), veuillez contacter un administrateur",
                            'UPLOAD_ERR_INI_SIZE'
                        )
                    );
                case UPLOAD_ERR_PARTIAL:
                    throw new Exception(__("Connexion au serveur interrompue"));
                default:
                    throw new Exception(__("Erreur code {0} lors de l'upload du fichier", $report['error']));
            }
        }

        $neededTime = (int)(300 / 1000000000 * $request->getTotalSize());// 1Go ~= 300s
        if (PHP_SAPI !== 'cli' && $neededTime > 30) {
            set_time_limit($neededTime);
        }
        if ($this->getRequest()->is('get')) {
            $file = new File($config, $request);
            if (!$file->checkChunk()) {
                return $this->getResponse()
                    ->withStatus(204, 'No Content')
                    ->withStringBody('');
            }
        }
        if (Basic::save($finalPath, $config, $request)) {
            return $this->saveFileinfo($report, $validation);
        }

        $report['path'] = urlencode(substr($report['path'], strlen($path) + 1));
        return $this->renderDataToJson(['report' => $report])->withHeader('Connection', 'close');
    }

    /**
     * Upload d'un certificat pour SAE distant
     */
    public function archivingSystemCert()
    {
        return $this->ssl('archiving-system');
    }

    /**
     * Upload d'un certificat pour SAE distant
     */
    public function sedaGeneratorCert()
    {
        return $this->ssl('seda-generator');
    }

    /**
     * Upload d'un certificat pour SAE distant
     */
    public function formCert()
    {
        return $this->ssl('seda-generator-' . $this->archivalAgencyId);
    }

    /**
     * Upload d'un certificat pour archive
     * @param string $subdir
     * @return Response|CakeResponse|MessageInterface
     * @throws Exception
     */
    private function ssl(string $subdir)
    {
        $path = rtrim(Configure::read('App.paths.data'), DS) . DS . 'ssl' . DS . $subdir;
        if (!file_exists($path)) {
            Filesystem::mkdir($path);
        }
        $this->Fileuploads = $this->fetchTable('Fileuploads');
        $uid = $this->getRequest()->getQuery('flowIdentifier');
        $name = $this->getRequest()->getQuery('flowFilename');
        $filename = $path . DS . $name;
        if (is_file($filename)) {
            $report = [
                'uid' => $uid,
                'filename' => $name,
                'name' => $name,
                'path' => $filename,
                'completion' => true,
                'error' => UPLOAD_ERR_OK
            ];
            return $this->renderDataToJson(['report' => $report])
                ->withHeader('Connection', 'close');
        }

        $response = $this->uploadTo($path);

        if ($this->entity) {
            $this->entity->set('locked', true);
            $this->Fileuploads->save($this->entity);
        }

        return $response;
    }
}
