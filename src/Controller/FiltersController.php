<?php

/**
 * Versae\Controller\FiltersController
 */

namespace Versae\Controller;

use AsalaeCore\Controller\FiltersControllerTrait;
use Versae\Model\Table\FiltersTable;
use Versae\Model\Table\SavedFiltersTable;

/**
 * Filtres de rechercher
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property SavedFiltersTable $SavedFilters
 * @property FiltersTable $Filters
 */
class FiltersController extends AppController
{
    use FiltersControllerTrait;
}
