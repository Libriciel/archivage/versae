<?php

/**
 * Versae\Controller\PermissionsController
 */

namespace Versae\Controller;

use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Utility\FormatError;
use AsalaeCore\Utility\Notify;
use BadMethodCallException;
use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Response;
use Cake\I18n\FrozenDate;
use Cake\Utility\Hash;
use Exception;
use Versae\Form\PermissionsForm;
use Versae\Model\Table\AcosTable;
use Versae\Model\Table\ArosAcosTable;
use Versae\Model\Table\ArosTable;

/**
 * Gestion des permissions Acl
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ArosTable $Aros
 * @property AcosTable $Acos
 * @property ArosAcosTable $Permissions
 */
class PermissionsController extends AppController
{
    use RenderDataTrait;

    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public $paginate = [
        'order' => [
            'Users.username' => 'asc'
        ]
    ];

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const TABLE_INDEX = 'permissions-table';

    /**
     * Liste les permissions
     */
    public function index()
    {
        $this->loadComponent('AsalaeCore.Jstable');
        $this->Jstable->configurablePagination(self::TABLE_INDEX);
        $this->set('tableId', self::TABLE_INDEX);

        $this->Aros = $this->fetchTable('Aros');
        $this->Aros->belongsTo(
            'Users',
            [
                'foreignKey' => 'foreign_key',
                'conditions' => ['model' => 'Users']
            ]
        );
        $query = $this->Aros->find()
            ->where(['model' => 'Users'])
            ->contain(['Users']);

        $this->loadComponent('AsalaeCore.Filter');
        $this->Filter->loadAndSave();
        $this->loadComponent('AsalaeCore.Condition');
        $params = $this->getRequest()->getParam('?') ?: [];

        // Filtre name
        foreach ((array)Hash::get($params, 'name') as $value) {
            $query->where($this->Condition->ilike('Users.username', $value));
        }

        // Filtre created
        foreach ((array)Hash::get($params, 'created') as $key => $value) {
            $date = FrozenDate::parseDateTime($value);
            $operator = $this->getRequest()->getParam("?.dateoperator.$key");
            $query->where($this->Condition->dateOperator('Users.created', $operator, $date));
        }

        // Filtre model
        foreach ((array)Hash::get($params, 'model') as $values) {
            $query->where($this->Condition->in('Aros.model', $values));
        }

        $this->set('aros', $this->paginate($query)->toArray());
    }

    /**
     * Modification d'une permission
     *
     * @param string $aro_id
     * @throws Exception
     */
    public function edit(string $aro_id)
    {
        $path = Configure::read('App.paths.controllers_rules');
        if (!is_readable($path)) {
            throw new Exception(
                __(
                    "{0} n'a pas accès à controllers.json en lecture, impossible de modifier les droits!",
                    get_current_user()
                )
            );
        }

        $this->Permissions = $this->fetchTable('ArosAcos');
        $this->Aros = $this->fetchTable('Aros');
        $this->Aros->belongsTo('Users')
            ->setForeignKey('foreign_key')
            ->setConditions(['Aros.model' => 'Users']);
        $this->Aros->belongsTo('Parent')
            ->setForeignKey('parent_id')
            ->setTable('aros');
        $this->Aros->belongsTo('Roles')
            ->setForeignKey('foreign_key')
            ->setConditions(['Aros.model' => 'Roles']);

        $entity = $this->Aros->find()
            ->where(['Aros.id' => $aro_id])
            ->contain(['Parent', 'Roles'])
            ->firstOrFail();
        $globalPermission = Hash::get($entity, 'role.org_entity_id') === null;
        $roleOrgEntityId = Hash::get($entity, 'role.org_entity_id');
        if ($globalPermission && !Configure::read('Roles.global_is_editable')) {
            throw new ForbiddenException();
        } elseif (!$globalPermission && $roleOrgEntityId !== $this->archivalAgencyId) {
            throw new ForbiddenException();
        }
        $form = new PermissionsForm();

        if ($this->getRequest()->is('post')) {
            $this->loadComponent('AsalaeCore.Modal');
            $data = [
                'aro' => $entity
            ] + $this->getRequest()->getData();
            if ($form->execute($data)) {
                $this->Modal->success();
                $query = $this->Aros->find()
                    ->where(
                        [
                            'model' => 'Users',
                            'lft >=' => $entity->get('lft'),
                            'rght <=' => $entity->get('rght'),
                        ]
                    )
                    ->contain(['Users']);
                foreach ($query as $aro) {
                    Notify::send(
                        Hash::get($aro, 'user.id'),
                        [],
                        __("Vos permissions ont été modifiés"),
                        'alert-warning'
                    );
                }
            } else {
                $this->Modal->fail();
                FormatError::logFormErrors($form);
            }
        }
        $this->set('form', $form);
        // Récupère la liste des permissions de l'aro (1 seul niveau)
        $currentPerms = Hash::expand($form->list($entity->get('id')));
        $this->set('defaults', $currentPerms);
        $accesses = [];
        $controllersJson = json_decode(file_get_contents($path), true);
        /**
         * Lecture des permissions (héritage compris)
         */
        set_error_handler(fn() => null); // nécéssaire pour les tests unitaire malgrès le @
        foreach ($currentPerms as $type => $controllers) {
            foreach ($controllers as $controller => $values) {
                foreach ($values as $crud => $access) {
                    if ($crud === 'action') {
                        continue;
                    }
                    if ($access !== '0') {
                        $accesses[$type][$controller][$crud] = $access === '1';
                        continue;
                    }
                    $aco = $type . '/' . $controller;
                    $accesses[$type][$controller][$crud]
                        = @$this->Authorization->Acl->check(
                            $entity->get('id'),
                            $aco,
                            $crud
                        );
                }
                foreach ($values['action'] ?? [] as $action => $access) {
                    if ($access !== '0') {
                        $accesses[$type][$controller]['action'][$action] = $access === '1';
                        continue;
                    }
                    $commeDroit = Hash::get(
                        $controllersJson,
                        "$controller.$action.commeDroits"
                    );
                    if ($type === 'controllers' && $commeDroit) {
                        [$ctrl, $act] = explode('::', $commeDroit);
                        $aco = $type . '/' . $ctrl . '/' . $act;
                    } else {
                        $aco = $type . '/' . $controller . '/' . $action;
                    }
                    $accesses[$type][$controller]['action'][$action]
                        = @$this->Authorization->Acl->check(
                            $entity->get('id'),
                            $aco
                        );
                }
            }
        }
        restore_error_handler();
        $this->set('accesses', $accesses);
        foreach ($controllersJson as $controller => $actions) {
            $actions = array_filter(
                $actions,
                function ($v) {
                    return empty($v['invisible']) && empty($v['commeDroits']);
                }
            );
            $controllersJson[$controller] = $actions;
            if (empty($actions)) {
                unset($controllersJson[$controller]);
            }
        }
        $this->set('controllers', $controllersJson);
        $this->set('entity', $entity);
        $this->set('aro_id', $aro_id);
    }

    /**
     * Permet d'obtenir la permission d'accès d'un aro à un controller action
     * On y fait également la distinction des droits hérités
     *
     * @param string $aro_id
     * @param string $controller
     * @param string $action
     * @return Response
     * @throws Exception
     * @throws BadMethodCallException
     */
    public function ajaxGetPermission(string $aro_id, string $controller, string $action)
    {
        if (!$this->getRequest()->is('ajax')) {
            throw new Exception(__("Accès direct interdit"));
        }

        $this->Permissions = $this->fetchTable('ArosAcos');
        $this->Acos = $this->fetchTable('Acos');

        if (preg_match('/^api\.(create|read|update|delete)$/', $action, $match)) {
            $aco = $this->Acos->find()
                ->where(['alias' => $controller, 'model' => 'api'])
                ->first();
        } else {
            $this->loadComponent('AsalaeCore.Condition');
            $aco = $this->Acos->find()
                ->where(['alias' => $action])
                ->andWhere($this->Condition->ilike('model', $controller))
                ->order(['lft'])
                ->first();
        }

        if (!$aco) {
            throw new BadMethodCallException(__("{0}::{1} n'existe pas", $controller, $action));
        }

        $permission = $this->Permissions->find()
            ->where(['aro_id' => $aro_id, 'aco_id' => $aco->get('id')])
            ->contain('Acos')
            ->first();

        $key = $match[1] ?? 'create';
        $access = 0;

        // On sauvegarde la valeur d'accès, on l'a met à 0 (héritage)
        // et on vérifie les droits pour connaitre les droits du parent
        if ($permission) {
            $access = $permission->get('_' . $key);
            $this->Permissions->patchEntity($permission, ['_' . $key => 0]);
            $this->Permissions->save($permission);
        }
        $accessParent = $this->Authorization->Acl->check($aro_id, $aco->get('id'), $key);
        if ($permission) {
            $this->Permissions->patchEntity($permission, ['_' . $key => $access]);
            $this->Permissions->save($permission);
        }

        return $this->renderDataToJson(['access' => $access, 'accessParent' => $accessParent]);
    }
}
