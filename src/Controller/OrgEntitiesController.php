<?php

/**
 * Versae\Controller\OrgEntitiesController
 */

namespace Versae\Controller;

use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Utility\FormatError;
use Cake\Core\Configure;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Database\Expression\QueryExpression;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\QueryInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response;
use Cake\Http\Response as CakeResponse;
use Cake\ORM\Query;
use Cake\ORM\ResultSet;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;
use Versae\Form\ArchivalAgencyConfigurationForm;
use Versae\Model\Entity\Form;
use Versae\Model\Entity\OrgEntity;
use Versae\Model\Entity\TypeEntity;
use Versae\Model\Table\ConfigurationsTable;
use Versae\Model\Table\FileuploadsTable;
use Versae\Model\Table\FormsTable;
use Versae\Model\Table\OrgEntitiesTable;
use Versae\Model\Table\RolesTable;
use Versae\Model\Table\TypeEntitiesTable;

/**
 * Contrôleur des entités
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ConfigurationsTable Configurations
 * @property FileuploadsTable Fileuploads
 * @property OrgEntitiesTable OrgEntities
 * @property RolesTable Roles
 * @property TypeEntitiesTable TypeEntities
 */
class OrgEntitiesController extends AppController
{
    use RenderDataTrait;

    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public $paginate = [
        'order' => [
            'OrgEntities.lft' => 'asc',
        ],
        'sortable' => [
            'type_entity' => 'TypeEntities.name',
        ],
    ];

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const TABLE_INDEX = 'org-entities-table';
    public const TABLE_EDIT_USER = 'users-entity-table';

    /**
     * Liste des entités
     * @param string $type organigramme || table
     * @throws Exception
     */
    public function index(string $type = 'organigramme')
    {
        switch ($type) {
            case 'organigramme':
                $this->indexOrgchart();
                break;
            case 'table':
                $this->indexTable();
                break;
            default:
                throw new NotFoundException();
        }
    }

    /**
     * index sous forme d'organigramme
     * @throws Exception
     */
    private function indexOrgchart()
    {
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $userSA = $this->archivalAgency;
        $orgEntities = $this->OrgEntities->find('threaded')
            ->enableAutoFields()
            ->where(
                [
                    'OrgEntities.lft >=' => $userSA->get('lft'),
                    'OrgEntities.rght <=' => $userSA->get('rght'),
                ]
            )
            ->contain(['TypeEntities', 'RgpdInfos'])
            ->order(['OrgEntities.name' => 'asc'])
            ->all()
            ->first();

        $f = function ($v) use (&$f) {
            if ($v instanceof EntityInterface) {
                $v->set('deletable', $v->get('deletable'));
                $v->set('html_id', 'OrgEntity_' . $v->get('id'));
                $children = $v->get('children');
                foreach ($children as $key => $child) {
                    $children[$key] = $f($child);
                }
                $v->set('children', $children);
                $v = $v->toArray();
            }
            $v['html_id'] = 'OrgEntity_' . ($v['id'] === null ? 'root' : $v['id']);
            foreach ($v as $k => $val) {
                if (is_string($val)) {
                    $v[$k] = h($val);
                }
            }
            return $v;
        };
        $chartDatasource = $f($orgEntities);// first() ne fonctionne pas en threaded
        $this->set('chartDatasource', $chartDatasource);

        $allNames = $this->OrgEntities->listOptionsWithoutSeal()
            ->where(
                [
                    'OrgEntities.lft >=' => $userSA->get('lft'),
                    'OrgEntities.rght <=' => $userSA->get('rght'),
                ]
            )
            ->disableHydration()
            ->all()
            ->toArray();
        $this->set('allNames', $allNames);
        $this->viewBuilder()->setTemplate('index_orgchart');
    }

    /**
     * Index sous la forme d'un tableau de résultats classique
     * @throws Exception
     */
    private function indexTable()
    {
        $this->loadComponent('AsalaeCore.Index');
        $this->Index->init();

        $userSA = $this->archivalAgency;

        $query = $this->OrgEntities->find();
        $query->enableAutoFields()
            ->where(
                [
                    'OrgEntities.lft >=' => $userSA->get('lft'),
                    'OrgEntities.rght <=' => $userSA->get('rght'),
                ]
            )
            ->contain(['TypeEntities']);

        $this->Index->setQuery($query)
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter('identifier', IndexComponent::FILTER_ILIKE)
            ->filter('name', IndexComponent::FILTER_ILIKE)
            ->filter('type_entity_id', IndexComponent::FILTER_IN);

        $data = $this->paginate($query)
            ->map(
                function (EntityInterface $entity) {
                    $entity->setVirtual(['parents', 'deletable']);
                    return $entity;
                }
            )
            ->toArray();
        $this->set('data', $data);

        //options
        $this->TypeEntities = $this->fetchTable('TypeEntities');
        $types = $this->TypeEntities->find('list')->order('name')->toArray();
        $this->set('types', $types);
    }

    /**
     * Ajouter une entité
     * @param string $parentId
     * @return Response
     * @throws Exception
     */
    public function add(string $parentId = '')
    {
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $entity = $this->OrgEntities->newEntity([], ['validate' => false]);
        $disabledTypes = $this->setTypeEntities();

        $typeTransferringAgencies = TableRegistry::getTableLocator()->get('TypeEntities')
            ->find('list', ['valueField' => 'id'])
            ->where(['code IN' => OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES])
            ->toArray();
        $this->set('typeTransferringAgencies', $typeTransferringAgencies);

        if ($this->getRequest()->is('post')) {
            $type_id = $this->getRequest()->getData('type_entity_id');
            if (in_array($type_id, $disabledTypes)) {
                $entity->setError(
                    'type_entity_id',
                    __("Veuillez sélectionner une valeur parmi les valeurs disponibles")
                );
            } else {
                $data = [
                    'name' => $this->getRequest()->getData('name'),
                    'short_name' => $this->getRequest()->getData('short_name'),
                    'identifier' => $this->getRequest()->getData('identifier'),
                    'type_entity_id' => $type_id,
                    'parent_id' => $parentId ?: $this->getRequest()->getData('parent_id'),
                    'description' => $this->getRequest()->getData('description'),
                    'forms_availables' => ['_ids' => $this->getRequest()->getData('forms_availables._ids')],
                ];
                if ($max = $this->getRequest()->getData('max_disk_usage_conv')) {
                    $data['max_disk_usage'] = $max * (int)$this->getRequest()->getData('mult', 1);
                }
                $this->OrgEntities->patchEntity($entity, $data);
            }
            $this->loadComponent('AsalaeCore.Modal');
            if ($this->OrgEntities->save($entity)) {
                $entity->setVirtual(['parents', 'deletable']);
                $json = $entity->toArray();
                if (empty($json['parent_id'])) {
                    $json['parent_id'] = 'root';
                }
                $this->Modal->success();
                return $this->renderJson(json_encode($json));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->set('entity', $entity);

        // options
        $userSA = $this->archivalAgency;
        $query = $this->OrgEntities->find()
            ->select(
                [
                    'OrgEntities.id',
                    'OrgEntities.name',
                    'Parents.name',
                ]
            )
            ->where(
                [
                    'OrgEntities.lft >=' => $userSA->get('lft'),
                    'OrgEntities.rght <=' => $userSA->get('rght'),
                ]
            )
            ->contain(['Parents'])
            ->order(['OrgEntities.name' => 'asc', 'OrgEntities.lft' => 'asc']);
        $allNames = [];
        /** @var OrgEntity $entity */
        foreach ($query as $entity) {
            $name = $entity->get('name');
            if ($parent = $entity->get('parent')) {
                $name .= ' (' . $parent->get('name') . ')';
            }
            $allNames[$entity->get('id')] = $name;
        }
        $this->set('parents', $allNames);
        $this->set('parentId', $parentId);

        $formsOptions = TableRegistry::getTableLocator()->get('Forms')->find('list')
            ->where(
                [
                    'state in' => [FormsTable::S_EDITING, FormsTable::S_PUBLISHED],
                    'org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->all()
            ->toArray();
        $this->set('formsOptions', $formsOptions);
    }

    /**
     * Affichage / édition(ajax) d'une entité
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function edit(string $id)
    {
        $this->loadComponent('AsalaeCore.AjaxPaginator');
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $this->Roles = $this->fetchTable('Roles');
        $this->Configurations = $this->fetchTable('Configurations');
        $sa = $this->archivalAgency;

        $entity = $this->OrgEntities->find()
            ->enableAutoFields()
            ->where(['OrgEntities.id' => $id])
            ->contain(
                [
                    'TypeEntities',
                    'Users' => function (Query $q) {
                        return $q
                            ->order(['Users.username' => 'asc'])
                            ->contain(['Roles'])
                            ->limit($this->AjaxPaginator->getConfig('limit'));
                    },
                    'FormsAvailables',
                    'RgpdInfos',
                ]
            )
            ->firstOrFail();
        $entity->setVirtual(['parents', 'deletable']);
        /** @var EntityInterface $user */
        foreach ($entity->get('users') as $user) {
            $user->setVirtual(['deletable']);
        }
        $this->Users = $this->fetchTable('Users');
        $countUsers = $this->Users->find()
            ->where(['org_entity_id' => $entity->id])
            ->count();
        $this->set('countUsers', $countUsers);

        $forms = TableRegistry::getTableLocator()->get('Forms')->find()
            ->where(
                [
                    'Forms.state in' => [FormsTable::S_EDITING, FormsTable::S_PUBLISHED],
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['FormsTransferringAgencies'])
            ->all()
            ->map(
                function (Form $entity) use ($id) {
                    $locked = [(int)$id]
                        === Hash::extract($entity->get('forms_transferring_agencies'), '{n}.org_entity_id');
                    $entity->set('locked', $locked);
                    return $entity;
                }
            )
            ->toArray();

        $lockedForms = array_reduce(
            $forms,
            function ($acc, $e) {
                if ($e->get('locked')) {
                    $acc[] = (string)$e->get('id');
                }
                return $acc;
            },
            []
        );
        $this->set('lockedForms', $lockedForms);

        $typeTransferringAgencies = TableRegistry::getTableLocator()->get('TypeEntities')
            ->find('list', ['valueField' => 'id'])
            ->where(['code IN' => OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES])
            ->toArray();
        $this->set('typeTransferringAgencies', $typeTransferringAgencies);

        $diskUsage = $entity->get('max_disk_usage');
        if ($diskUsage && (int)$diskUsage % 1024 === 0) {
            $pow = 0;
            while ($diskUsage > 1024) {
                $diskUsage /= 1024;
                if (++$pow >= 5 || $diskUsage % 1024 !== 0) {
                    break;
                }
            }
            $entity->set('mult', pow(1024, $pow));
            $entity->setDirty('mult', false);
        }
        $entity->set('max_disk_usage_conv', $diskUsage);
        $entity->setDirty('max_disk_usage_conv', false);

        $req = $this->getRequest();
        $requireParentForNonSa = ($req->getData('parent_id')
            xor Hash::get($entity, 'type_entity.code') === 'SA');
        if (
            $req->is('put')
            && $req->getData('name')
            && $requireParentForNonSa
        ) {
            $code = $entity->get('code');
            $typeEntityId = in_array($code, ['SA', 'SE'])
                ? $entity->get('type_entity_id')
                : $req->getData('type_entity_id');
            $parentId = in_array($code, ['SA', 'SE'])
                ? $entity->get('parent_id')
                : $req->getData('parent_id');
            $data = [
                'name' => $req->getData('name'),
                'short_name' => $req->getData('short_name'),
                'identifier' => $req->getData('identifier'),
                'type_entity_id' => $typeEntityId,
                'parent_id' => $parentId,
                'lft' => $entity->get('lft'),
                'rght' => $entity->get('rght'),
                'description' => $req->getData('description'),
                'active' => $req->getData('active'),
                'forms_availables' => [
                    '_ids' => ($req->getData('forms_availables._ids') ?: [])
                        + $lockedForms
                ],
            ];
            $this->OrgEntities->patchEntity($entity, $data);
            $this->loadComponent('AsalaeCore.Modal');
            $dirty = $entity->isDirty();
            if ($this->OrgEntities->save($entity)) {
                $this->Modal->success();
                if ($dirty) {
                    $this->setResponse($this->getResponse()->withHeader('X-Asalae-Reload', 'true'));
                }
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        } elseif ($req->is(['post', 'put']) && $req->getData('_tab') === 'rgpd') {
            $data = [
                'rgpd_info' => $req->getData('rgpd_info'),
            ];
            if (empty(Hash::filter($data['rgpd_info']))) {
                if ($rgpdId = Hash::get($entity, 'rgpd_info.id')) {
                    $this->fetchTable('RgpdInfos')->deleteAll(['id' => $rgpdId]);
                }
                $success = true;
            } else {
                $this->OrgEntities->patchEntity($entity, $data);
                $success = (bool)$this->OrgEntities->save($entity);
            }
            $this->loadComponent('AsalaeCore.Modal');
            if ($success) {
                $this->setResponse($this->getResponse()->withHeader('X-Asalae-Reload', 'true'));
                return $this->Modal->success();
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('entity', $entity);

        $this->set('tableIdUser', self::TABLE_EDIT_USER);

        $roles = $this->Roles->find('list')
            ->where(['active' => true])
            ->order(['name' => 'asc'])
            ->all()
            ->toArray();
        $this->set('roles', $roles);
        $this->setTypeEntities();

        $parents = $this->OrgEntities->find()
            ->select(
                [
                    'OrgEntities.id',
                    'OrgEntities.name',
                    'TypeEntities.code',
                ]
            )
            ->where(
                function (QueryExpression $exp) use ($entity) {
                    return $exp->or(
                        [
                            'lft <' => $entity->get('lft'),
                            'rght >' => $entity->get('rght'),
                        ]
                    );
                }
            )
            ->andWhere(
                [
                    'OrgEntities.lft >=' => $sa->get('lft'),
                    'OrgEntities.rght <=' => $sa->get('rght'),
                ]
            )
            ->contain(['TypeEntities'])
            ->order(['OrgEntities.name' => 'asc']);
        $parentsOptions = [];
        $disabledOptions = [];
        /** @var OrgEntity $entity */
        foreach ($parents as $entity) {
            $parentsOptions[$entity->get('id')] = $entity->get('name');
            $code = Hash::get($entity, 'type_entity.code');
            if ($entity->get('type_entity') && $code === 'SE') {
                $disabledOptions[$code] = $entity->get('id');
            }
        }
        $this->set('parents', $parentsOptions);
        $this->set('parentsDisabled', $disabledOptions);
        $this->set('id', $id);

        $rolesUsers = $this->OrgEntities->findByRoles($sa)
            ->select(['qexists' => 1], true)
            ->where(['OrgEntities.id' => $id, 'agent_type' => 'person'])
            ->disableHydration()
            ->first();
        $this->set('rolesUsers', (bool)$rolesUsers);

        $this->set('formsOptions', Hash::combine($forms, '{n}.id', '{n}.name'));
    }

    /**
     * Pagination des utilisateurs de l'entité
     * @param string $id
     * @return ResponseInterface
     * @throws Exception
     */
    public function paginateUsers(string $id)
    {
        $this->loadComponent('AsalaeCore.AjaxPaginator');
        $this->loadComponent('AsalaeCore.Index', ['model' => 'Users']);
        $this->Users = $this->fetchTable('Users');
        $query = $this->Users->find()
            ->innerJoinWith('OrgEntities')
            ->innerJoinWith('OrgEntities.ArchivalAgencies')
            ->where(
                [
                    'Users.org_entity_id' => $id,
                    'ArchivalAgencies.id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['Roles']);
        $this->Index->setQuery($query)
            ->filter('username', IndexComponent::FILTER_ILIKE);
        return $this->AjaxPaginator->json(
            $query,
            (bool)$this->getRequest()->getQuery('count'),
            function (ResultSet $data) {
                return $data->map(
                    function (EntityInterface $user) {
                        return $user->setVirtual(['deletable']);
                    }
                );
            }
        );
    }

    /**
     * Action du formulaire (put) de modification de politique de conversion
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function editConversions(string $id)
    {
        $request = $this->getRequest();
        $request->allowMethod('put');
        if ((int)$id !== $this->archivalAgencyId) {
            throw new NotFoundException();
        }
        $this->loadComponent('AsalaeCore.Modal');
        $this->ConversionPolicies = $this->fetchTable('ConversionPolicies');
        foreach ($request->getData() as $media => $values) {
            foreach ($values as $usage => $data) {
                $params = $data['params'] ?? [];
                unset($data['params']);
                $base = [
                    'org_entity_id' => $id,
                    'media' => $media,
                    'usage' => $usage,
                ];
                $entity = $this->ConversionPolicies->find()
                    ->where($base)
                    ->first();
                if (empty($data['method'])) {
                    if ($entity) {
                        $this->ConversionPolicies->deleteOrFail($entity);
                    }
                    continue;
                }
                if (!$entity) {
                    $entity = $this->ConversionPolicies->newEntity($base);
                }
                $this->ConversionPolicies->patchEntity($entity, $base + $data);
                // patch entity ne fonctionne pas avec un array dans params
                if ($params) {
                    $entity->set('params', $params);
                }
                if (!$this->ConversionPolicies->save($entity)) {
                    $this->Modal->fail();
                    FormatError::logEntityErrors($entity);
                    return $this->renderDataToJson(['errors' => $entity->getErrors()]);
                }
            }
        }
        $this->Modal->success();
        return $this->renderDataToJson('done');
    }

    /**
     * Donne les options pour les type_entities Disponibles
     * @return array
     */
    private function setTypeEntities(): array
    {
        $this->TypeEntities = $this->fetchTable('TypeEntities');
        $typeEntitiesQuery = $this->TypeEntities->find()
            ->select(
                [
                    'TypeEntities.id',
                    'TypeEntities.name',
                    'TypeEntities.code',
                    'TypeEntities.active',
                ]
            )
            ->order(['TypeEntities.name' => 'asc'])
            ->contain(
                [
                    'Roles' => function (QueryInterface $q) {
                        return $q->where(
                            [
                                'OR' => [
                                    'Roles.org_entity_id IS' => null,
                                    'Roles.org_entity_id' => $this->archivalAgencyId,
                                ],
                            ]
                        );
                    },
                ]
            );
        $disabledOptions = [];
        $enabledOptions = [];
        $typeEntities = [];
        /** @var TypeEntity $type */
        foreach ($typeEntitiesQuery as $type) {
            $title = implode(PHP_EOL, Hash::extract($type, 'roles.{n}.name'));
            $typeEntities[$type->get('id')] = [
                'text' => $type->get('name'),
                'value' => $type->get('id'),
                'title' => sprintf(
                    "%s :\n%s",
                    __("Rôles utilisateurs associés"),
                    $title
                ),
            ];
            if (in_array($code = $type->get('code'), ['SA', 'SE']) || !$type->get('active')) {
                $disabledOptions[$code] = $type->get('id');
            } else {
                $enabledOptions[$type->get('id')] = $typeEntities[$type->get('id')];
            }
        }
        $this->set('typeEntitiesDisabled', $disabledOptions);
        $this->set('typeEntitiesOptions', $enabledOptions);
        $this->set('typeEntities', $typeEntities);
        return $disabledOptions;
    }

    /**
     * Supprime une entité
     *
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function delete(string $id)
    {
        $this->getRequest()->allowMethod('delete');
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $entity = $this->OrgEntities->find()
            ->where(['OrgEntities.id' => $id])
            ->contain(['TypeEntities']) // pour le champ virtuel "deletable"
            ->firstOrFail();
        if (!$entity->get('deletable')) {
            throw new ForbiddenException(__("Tentative de suppression d'une entité protégée"));
        }

        $conn = $this->OrgEntities->getConnection();
        $conn->begin();
        if ($this->OrgEntities->delete($entity)) {
            $report = 'done';
            $conn->commit();
        } else {
            $report = 'Erreur lors de la suppression';
            $conn->rollback();
        }

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Permet de modifier le parent_id via requète HTTP
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function editParentId(string $id)
    {
        $this->getRequest()->allowMethod('put');
        $this->checkAccess($id);
        $parentId = $this->getRequest()->getData('parent_id');
        $this->checkAccess($parentId);
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $entity = $this->OrgEntities->find()
            ->innerJoinWith('TypeEntities')
            ->where(
                [
                    'OrgEntities.id' => $id,
                    'TypeEntities.code NOT IN' => OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES,
                ]
            )
            ->firstOrFail();

        $this->OrgEntities->patchEntity(
            $entity,
            [
                'parent_id' => $this->getRequest()->getData('parent_id'),
                'lft' => $entity->get('lft'),
                'rght' => $entity->get('rght'),
            ]
        );
        try {
            $success = $this->OrgEntities->save($entity);
        } catch (RuntimeException) { // Cannot use node "x" as parent for entity "y"
            $success = false;
        }
        if ($success) {
            $report = ['success' => true];
        } else {
            $report = [
                'success' => false,
                'errors' => $entity->getErrors(),
            ];
        }
        return $this->renderData($report);
    }

    /**
     * Supprime l'image du logo client et la retire de la configuration
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function deleteBackground(string $id)
    {
        $publicDir = rtrim(Configure::read('App.paths.data'), DS);
        $webrootDir = rtrim(Configure::read('OrgEntities.public_dir', WWW_ROOT), DS);
        $this->Configurations = $this->fetchTable('Configurations');
        $this->getRequest()->allowMethod('delete');
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $entity = $this->Configurations->find()
            ->where(
                [
                    'org_entity_id' => $id,
                    'name' => 'background-image',
                ]
            )
            ->firstOrFail();
        Filesystem::begin();
        Filesystem::setSafemode(true);
        Filesystem::remove($publicDir . DS . $entity->get('setting'));
        Filesystem::remove($webrootDir . DS . $entity->get('setting'));
        Filesystem::remove($publicDir . DS . 'org-entity-data' . DS . $id . DS . 'background.css');
        Filesystem::remove($webrootDir . DS . 'org-entity-data' . DS . $id . DS . 'background.css');
        $dir = dirname($publicDir . $entity->get('setting'));
        if (count(scandir($dir)) === 2) {
            Filesystem::remove($dir);
        }
        $dir = dirname($webrootDir . $entity->get('setting'));
        if (count(scandir($dir)) === 2) {
            Filesystem::remove($dir);
        }
        if ($this->Configurations->delete($entity)) {
            Filesystem::commit();
            $report = 'done';
        } else {
            Filesystem::rollback();
            $report = 'Erreur lors de la suppression';
        }

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Donne la liste des roles disponnible pour les utilisateurs d'une entité
     * @param string $id
     * @return Response
     */
    public function getAvailablesRoles(string $id)
    {
        $agentType = $this->getRequest()->getParam('?.agent_type');
        $agentTypeCondition = $agentType ? ['Roles.agent_type' => $agentType] : [];
        $roles = $this->OrgEntities->find()
            ->select(
                [
                    'Roles.id',
                    'Roles.name',
                ]
            )
            ->where(
                [
                    'OrgEntities.id' => $id,
                ]
            )
            ->innerJoin(
                ['TypeEntities' => 'type_entities'],
                [
                    'TypeEntities.id' => new IdentifierExpression('OrgEntities.type_entity_id'),
                ]
            )
            ->innerJoin(
                ['TypesRoles' => 'roles_type_entities'],
                [
                    'TypesRoles.type_entity_id' => new QueryExpression('TypeEntities.id'),
                ]
            )
            ->innerJoin(
                ['Roles' => 'roles'],
                [
                    'Roles.id' => new QueryExpression('TypesRoles.role_id'),
                    'OR' => [
                        'Roles.org_entity_id IS' => null,
                        'Roles.org_entity_id' => $this->archivalAgencyId,
                    ],

                ] + $agentTypeCondition
            )
            ->orderAsc('Roles.name')
            ->all()
            ->toArray();
        return $this->renderDataToJson(
            array_reduce(
                $roles,
                function ($res, OrgEntity $v) {
                    $res[$v->get('Roles')['id']] = $v->get('Roles')['name'];
                    return $res;
                }
            )
        );
    }

    /**
     * Configuration lié au service d'archives
     * @return Response
     * @throws Exception
     */
    public function editConfig()
    {
        $this->Configurations = $this->fetchTable('Configurations');

        $entity = $this->archivalAgency;
        $this->set('entity', $entity);

        $form = ArchivalAgencyConfigurationForm::create($this->archivalAgencyId);

        // on charge le fichier logo si il existe
        $uploadFiles = [];
        $confBgImage = $form->entities[ConfigurationsTable::CONF_BACKGROUND_IMAGE] ?? null;
        $file = $confBgImage ? $confBgImage->get('webroot_filename') : null;
        if ($file && is_readable($file)) {
            $url = $confBgImage->get('setting');
            $uploadFiles[] = [
                'id' => 'local',
                'prev' => $url,
                'name' => basename($url),
                'size' => filesize($file),
                'webroot' => true,
            ];
        }
        $this->set('uploadFiles', $uploadFiles);

        if ($this->getRequest()->is('post')) {
            $conn = $this->Configurations->getConnection();
            $conn->begin();
            $this->loadComponent('AsalaeCore.Modal');
            if ($form->execute($this->getRequest()->getData())) {
                $conn->commit();
                $this->Modal->success();
                $configurations = $this->Configurations->find()
                    ->where(['org_entity_id' => $this->archivalAgencyId])
                    ->toArray();
                $this->archivalAgency->set('configurations', $configurations);
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $conn->rollback();
                $this->Modal->fail();
            }
        }
        $this->set($form->getOptions());
        $this->set('form', $form);
    }

    /**
     * Vérifi qu'un org_entity fait bien partie du service d'archives de l'utilisateur
     * Laisse l'accès à un admin technique
     * @param int $id
     * @throws NotFoundException|Exception
     */
    private function checkAccess($id)
    {
        $adminTech = $this->getRequest()->getSession()->read('Admin.tech');
        if ($adminTech || !$id) {
            return;
        }
        $sa = $this->archivalAgency;
        $this->OrgEntities->find()
            ->where(
                [
                    'OrgEntities.id' => $id,
                    'OrgEntities.lft >=' => $sa->get('lft'),
                    'OrgEntities.rght <=' => $sa->get('rght'),
                ]
            )
            ->firstOrFail();
    }
}
