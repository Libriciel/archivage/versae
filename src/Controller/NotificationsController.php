<?php

/**
 * Versae\Controller\NotificationsController
 */

namespace Versae\Controller;

use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Factory;
use Cake\Http\Response;
use Versae\Model\Table\NotificationsTable;
use Versae\Model\Table\UsersTable;

/**
 * Notifications de messages beanstalkd à l'utilisateur de versae
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property NotificationsTable $Notifications
 * @property UsersTable $Users
 */
class NotificationsController extends AppController
{
    use RenderDataTrait;

    /**
     * Envoi un message test à l'utilisateur qui l'a provoqué
     */
    public function test()
    {

        $result = Factory\Utility::get('Notify')->send(
            $this->userId,
            [$this->getRequest()->getSession()->read('Session.token')],
            $this->getRequest()->getData('msg')
        );
        return $this->renderDataToJson(['result' => $result]);
    }

    /**
     * Supprime une notification
     *
     * @param string $id
     * @return Response
     */
    public function delete(string $id)
    {
        $this->getRequest()->allowMethod('delete');

        $this->Notifications = $this->fetchTable('Notifications');
        $entity = $this->Notifications
            ->find()
            ->where(['id' => $id, 'user_id' => $this->userId])
            ->firstOrFail();

        $report = $this->Notifications->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        return $this->renderDataToJson(['report' => $report]);
    }
}
