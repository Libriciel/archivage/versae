<?php

/**
 * Versae\Controller\Component\EmailsComponent
 */

namespace Versae\Controller\Component;

use AsalaeCore\Factory\Utility;
use Cake\Controller\Component;
use Cake\Core\Configure;
use Cake\Http\Response as CakeResponse;
use Cake\Mailer\Mailer;
use Cake\ORM\TableRegistry;
use Cake\View\Helper\UrlHelper;
use DateTime;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Versae\Model\Entity\AuthUrl;
use Versae\Model\Entity\User;
use Versae\Model\Table\MailsTable;
use Versae\View\AppView;

/**
 * Partage de code d'envoi de mails entre les controllers
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class EmailsComponent extends Component
{

    /**
     * Email à envoyer lors de la création d'un nouvel utilisateur
     * @param User $user
     * @return AuthUrl|bool
     * @throws Exception
     */
    public function submitEmailNewUser(User $user)
    {
        if (!$user->get('role') || !$user->get('org_entity')) {
            $user = TableRegistry::getTableLocator()->get('Users')->find()
                ->where(['Users.id' => $user->get('id')])
                ->contain(['Roles', 'OrgEntities'])
                ->firstOrFail();
        }
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        /** @var AuthUrl $code */
        $code = $AuthUrls->newEntity(
            [
                'url' => '/users/initialize-password/' . $user->get('id'),
                'expire' => date('Y-m-d H:i:s', strtotime('+7 days'))
            ]
        );
        $AuthUrls->saveOrFail($code);
        if (explode('@', $user->get('email'))[1] === 'test.fr') {
            return $code;
        }
        $session = $this->getController()->getRequest()->getSession();
        $prefix = ($session->read('ConfigArchivalAgency.mail-title-prefix') ?: '[versae]');

        /** @var Mailer $email */
        $email = clone Utility::get(Mailer::class);
        $email->setViewVars(
            [
                'htmlSignature' => $session->read('ConfigArchivalAgency.mail-html-signature') ?: null,
                'textSignature' => $session->read('ConfigArchivalAgency.mail-text-signature') ?: null
            ]
        );
        $email->viewBuilder()
            ->setTemplate('newuser')
            ->setLayout('AsalaeCore.default')
            ->addHelpers(['Html', 'Url']);
        $email->setEmailFormat('both')
            ->setSubject($prefix . '  ' . __("Création de votre compte"))
            ->setViewVars(
                [
                    "title" => __("Vos identifiants"),
                    "user" => $user,
                    "code" => $code->get('code'),
                ]
            )
            ->setTo($user->get('email'));

        /** @var MailsTable $Mails */
        $Mails = TableRegistry::getTableLocator()->get('Mails');
        return $Mails->asyncMail($email, $user->id);
    }

    /**
     * Si le debug est activé, renvoi une url pour consulter le mail
     * @param Mailer $email
     * @return CakeResponse|null
     * @throws Exception
     */
    public function debug(Mailer $email): ?CakeResponse
    {
        if (Configure::read('debug_mails')) {
            $basePath = Configure::read('debug_mails_basepath', TMP . 'debug_mails');
            $filename = (new DateTime())->format('Ymd_H-i') . uniqid('_sendTestMail_') . '.html';
            Filesystem::dumpFile(
                $basePath . DS . $filename,
                $email->getMessage()->getBodyHtml()
            );
            $url = new UrlHelper(new AppView());
            return $this->getController()
                ->getResponse()
                ->withType('txt')
                ->withHeader('X-Email-Debug', 'true')
                ->withStringBody(
                    $url->build('/devs/debug-mail/' . base64_encode($filename), ['fullBase' => true])
                );
        }
        return null;
    }
}
