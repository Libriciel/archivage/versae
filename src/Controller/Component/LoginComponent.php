<?php

/**
 * Versae\Controller\Component\LoginComponent
 */

namespace Versae\Controller\Component;

use Adldap\Adldap;
use Adldap\Auth\BindException;
use Adldap\Auth\Guard;
use Adldap\Auth\PasswordRequiredException;
use Adldap\Auth\UsernameRequiredException;
use Adldap\Connections\Provider;
use Adldap\Models\Model;
use Adldap\Query\Builder;
use Adldap\Query\Factory;
use ArrayObject;
use AsalaeCore\Controller\Component\CookieComponent;
use AsalaeCore\Factory\Utility;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Controller\Component;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Exception;
use Versae\Controller\AppController;
use Versae\Model\Entity\Ldap;
use Versae\Model\Entity\User;

/**
 * Logique de connexion
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class LoginComponent extends Component
{
    /**
     * @var array evite l'inscription dans EventLogs de plusieurs tentatives sur
     * une même connexion
     */
    public static $attempts = [];

    /**
     * Connecte un utilisateur (initialise la session)
     * @param string $username
     * @param string $password
     * @return bool
     * @throws Exception
     */
    public function logUser(string $username, string $password): bool
    {
        if (isset(self::$attempts[$username])) {
            return self::$attempts[$username];
        }
        $success = false;
        /** @var AppController $controller */
        $controller = $this->getController();
        $locator = TableRegistry::getTableLocator();
        $Users = $locator->get('Users');
        $Configurations = $locator->get('Configurations');
        $user = $Users->find()
            ->where(
                [
                    'Users.username' => $username,
                    'Users.active' => true,
                ]
            )
            ->innerJoinWith('OrgEntities')
            ->contain(
                [
                    'OrgEntities' => [
                        'ArchivalAgencies',
                        'TypeEntities'
                    ],
                    'Ldaps',
                    'Roles',
                ]
            )
            ->first();
        if ($user) {
            $hasher = new DefaultPasswordHasher();
            $controller->loadComponent('AsalaeCore.Modal');
            $request = $controller->getRequest();
            if ($code = $request->getHeaderLine('X-Encode-Password')) {
                $password = hash($code, $password);
            }
            $passwordOk = $hasher->check(
                $password,
                $user->get('password') ?: ''
            );
            if (!$passwordOk && $user->get('ldap_id')) {
                $passwordOk = $this->logLdapUser($user, $password);
            }
            if ($passwordOk) {
                $connect = $user->toArray();
                $controller->Authentication->setIdentity(
                    new ArrayObject($connect)
                );
                /** @noinspection PhpPossiblePolymorphicInvocationInspection loadModel -> fetchTable */
                $controller->OrgEntities = $controller->fetchTable(
                    'OrgEntities'
                );
                /** @noinspection PhpPossiblePolymorphicInvocationInspection loadModel -> fetchTable */
                $controller->Configurations = $controller->fetchTable(
                    'Configurations'
                );
                $config = [];
                $archivalAgencyId = Hash::get(
                    $connect,
                    'org_entity.archival_agency.id'
                );
                /** @var EntityInterface $conf */
                $q = $Configurations->find()->where(['org_entity_id' => $archivalAgencyId]);
                foreach ($q as $conf) {
                    $config[$conf->get('name')] = $conf->get('setting');
                }
                $session = $controller->getRequest()->getSession();
                $session->write('ConfigArchivalAgency', $config);
                $success = true;
            }
        }

        self::$attempts[$username] = $success;
        return $success;
    }

    /**
     * Charge les cookies enregistré en base de donnée
     * @param User $user
     * @throws Exception
     */
    public function loadCookies(User $user)
    {
        $controller = $this->getController();
        /** @var CookieComponent $CookieComponent */
        $CookieComponent = $controller->loadComponent('AsalaeCore.Cookie');
        $CookieComponent->setConfig('encryption', false);
        $cookies = $controller->getRequest()->getCookieParams();
        unset($cookies['PHPSESSID'], $cookies['XDEBUG_SESSION']);
        foreach (array_keys($cookies) as $cookie) {
            $CookieComponent->delete($cookie);
        }
        if (!empty($user->cookies)) {
            foreach (json_decode($user->cookies, true) as $cookie => $value) {
                $CookieComponent->write($cookie, $value);
            }
        }
    }

    /**
     * Essaye de logger et de mettre à jour $user avec $password sur le ldap
     * @param EntityInterface $user
     * @param string          $password
     * @return bool
     * @throws UsernameRequiredException
     */
    private function logLdapUser(EntityInterface $user, string $password)
    {
        /** @var Ldap $ldap */
        $ldap = $user->get('ldap');
        $ldapLogin = $user->get('ldap_login');
        /** @var Adldap $ad */
        $ad = Utility::get(Adldap::class);
        $ad->addProvider($ldap->getLdapConfig());
        try {
            /** @var Provider $provider */
            $provider = $ad->connect();
            /** @var Guard $auth */
            $auth = $provider->auth();
            $passwordOk = $auth->attempt($ldapLogin, $password, true);
            if ($passwordOk) {
                /** @var Factory $search */
                $search = $provider->search();
                /** @var Builder $users */
                $users = $search->users();
                /** @var Model|false $ldapUser */
                $ldapUser = $users->findBy(
                    $ldap->get('user_login_attribute'),
                    $ldapLogin
                );
            }
            if ($passwordOk && !empty($ldapUser)) {
                $name = $ldapUser->getAttribute(
                    $ldap->get('user_name_attribute')
                );
                $mail = $ldapUser->getAttribute(
                    $ldap->get('user_mail_attribute')
                );
                if ($name) {
                    $user->set('name', current($name));
                }
                if ($mail) {
                    $user->set('email', current($mail));
                }
            }
        } catch (BindException) {
            $this->getController()->Flash->error(
                __("Échec de la connexion au LDAP")
            );
        } catch (PasswordRequiredException) {
            // The user didn't supply a password.
        }
        return $passwordOk ?? false;
    }
}
