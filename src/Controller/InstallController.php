<?php

/**
 * Versae\Controller\InstallController
 */

namespace Versae\Controller;

use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Factory\Utility;
use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Http\Response;
use Cake\Mailer\Mailer;
use Cake\Mailer\TransportFactory;
use Cake\Utility\Hash;
use Exception;
use Versae\Controller\Component\EmailsComponent;
use Versae\Form\InstallForm;
use Versae\Model\Table\OrgEntitiesTable;

/**
 * Installation de l'application
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property EmailsComponent Emails
 * @property OrgEntitiesTable OrgEntities
 */
class InstallController extends Controller
{
    use RenderDataTrait;

    /**
     * Action unique de l'installation
     * @throws Exception
     */
    public function index()
    {
        $this->loadComponent('Flash');
        $this->viewBuilder()->setLayout('not_connected');
        $pathToLocalConfig = Configure::read('App.paths.path_to_local_config');
        if (!is_file($pathToLocalConfig) && !is_writable($dir = dirname($pathToLocalConfig))) {
            $this->Flash->error(__("Le dossier ''{0}'' n'est pas inscriptible", $dir));
        }
        try {
            $this->OrgEntities = $this->fetchTable('OrgEntities');
            $config = is_file($pathToLocalConfig)
                ? json_decode(
                    file_get_contents(
                        include $pathToLocalConfig
                    ),
                    true
                )
                : [];
            if (Hash::get($config, 'Security.salt') && $this->OrgEntities->find()->count()) {
                return $this->redirect('/');
            }
        } catch (Exception) {
        }

        $form = new InstallForm();

        if ($this->getRequest()->is('post') && $this->getRequest()->getData('initializeDatabase')) {
            $Exec = Utility::get('Exec');
            $Exec->rawCommand(
                ROOT . DS . 'bin' . DS . 'cake migrations migrate 2>&1'
                . '&& ' . ROOT . DS . 'bin' . DS . 'cake update 2>&1'
                . '&& ' . ROOT . DS . 'bin' . DS . 'cake RolesPerms import '
                . RESOURCES . 'export_roles.json --keep 2>&1',
                $output,
                $code
            );
            $method = $code === 0 ? 'success' : 'error';
            $message = implode("<br>\n", $output);
            if (str_contains($message, 'PDOException')) {
                $method = 'error';
            }
            $this->Flash->$method(implode("<br>\n", $output), ['escape' => false]);
        } elseif ($this->getRequest()->is('post') && $form->execute($this->getRequest()->getData())) {
            $admin = (json_decode(Configure::read('App.paths.administrators_json'), true) ?: [null])[0];
            unset($admin['password']);
            $session = $this->getRequest()->getSession();
            $session->write('Admin.tech', true);
            $session->write('Admin.data', $admin);
            return $this->redirect('/admins');
        }

        $this->set('form', $form);
    }

    /**
     * Envoi des emails de test
     * @return Response|null
     * @throws Exception
     */
    public function sendTestMail()
    {
        $request = $this->getRequest();
        $request->allowMethod('post');
        $this->loadComponent('Flash');
        $this->viewBuilder()->setLayout('not_connected');
        $pathToLocalConfig = Configure::read('App.paths.path_to_local_config');
        if (!is_file($pathToLocalConfig) && !is_writable($dir = dirname($pathToLocalConfig))) {
            $this->Flash->error(__("Le dossier ''{0}'' n'est pas inscriptible", $dir));
        }
        try {
            $this->OrgEntities = $this->fetchTable('OrgEntities');
            if ($this->OrgEntities->find()->count()) {
                return $this->redirect('/');
            }
        } catch (Exception) {
        }

        try {
            $request = $this->getRequest();
            TransportFactory::drop('default');
            TransportFactory::setConfig(
                [
                    'default' => [
                        'className' => $request->getData('Config__EmailTransport__default__className'),
                        // The following keys are used in SMTP transports
                        'host' => $request->getData('Config__EmailTransport__default__host'),
                        'port' => $request->getData('Config__EmailTransport__default__port'),
                        'timeout' => 30,
                        'username' => $request->getData('Config__EmailTransport__default__username'),
                        'password' => $request->getData('Config__EmailTransport__default__password'),
                        'client' => null,
                        'tls' => null,
                        'url' => env('EMAIL_TRANSPORT_DEFAULT_URL'),
                    ],
                ]
            );

            $emailAdress = $this->getRequest()->getData('email');
            $success = !empty($emailAdress);
            /** @var Mailer $email */
            $email = clone Utility::get(Mailer::class);
            $email->viewBuilder()
                ->setTemplate('test')
                ->setLayout('AsalaeCore.default')
                ->addHelpers(['Html', 'Url']);
            $success = $success && $email->setEmailFormat('both')
                ->setSubject('[versae] ' . __("Email de test"))
                ->setFrom($request->getData('Config__Email__default__from'))
                ->setTo($emailAdress)
                ->send();
            $this->loadComponent('Emails');
            if ($response = $this->Emails->debug($email)) {
                return $response;
            }
        } catch (Exception) {
            $success = false;
        }

        return $this->renderDataToJson(['success' => $success]);
    }
}
