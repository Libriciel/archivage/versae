<?php

/**
 * Versae\Controller\KeywordListsController
 */

namespace Versae\Controller;

use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\RenderDataTrait;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Response;
use Cake\ORM\Query;
use Exception;
use Versae\Model\Entity\Keyword;
use Versae\Model\Table\KeywordListsTable;
use Versae\Model\Table\KeywordsTable;

/**
 * Liste des mots clés (thésaurus)
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property KeywordListsTable $KeywordLists
 * @property KeywordsTable $Keywords
 */
class KeywordListsController extends AppController
{
    use RenderDataTrait;

    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public $paginate = [
        'order' => [
            'KeywordLists.id' => 'asc'
        ]
    ];

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const TABLE_INDEX = 'keyword-lists-table';

    /**
     * Liste des listes de mots clés
     */
    public function index()
    {
        $this->loadComponent('AsalaeCore.Index');
        $this->Index->init();

        $this->Keywords = $this->fetchTable('Keywords');
        $query = $this->KeywordLists->find();
        $countSq = $this->Keywords->find()
            ->select(['count' => $query->func()->count('*')])
            ->where(
                [
                    'Keywords.keyword_list_id' => new IdentifierExpression('KeywordLists.id'),
                    'Keywords.version' => new IdentifierExpression('KeywordLists.version'),
                ]
            );

        $query
            ->select(['count' => $countSq])
            ->where(['KeywordLists.org_entity_id' => $this->archivalAgencyId])
            ->enableAutoFields();

        $this->Index->setQuery($query)
            ->filter('active', IndexComponent::FILTER_IS)
            ->filter(
                'count',
                function ($v, Query $q) use ($countSq) {
                    $operator = $v['operator'];
                    if (!in_array($operator, ['=', '<=', '>=', '<', '>'])) {
                        throw new BadRequestException(__("L'opérateur indiqué n'est pas autorisé"));
                    }
                    $sql = $countSq->sql();
                    $q->where(["($sql) $operator" => $v['value']]);
                }
            )
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter('identifier', IndexComponent::FILTER_ILIKE)
            ->filter('modified', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('name', IndexComponent::FILTER_ILIKE)
            ->filter('version', IndexComponent::FILTER_COUNTOPERATOR);

        $data = $this->paginate($query)->toArray();
        $this->set('data', $data);
    }

    /**
     * Ajout d'une liste de mots clés
     */
    public function add()
    {
        $this->KeywordLists = $this->fetchTable('KeywordLists');
        $entity = $this->KeywordLists->newEntity(
            [
                'org_entity_id' => $this->archivalAgencyId,
                'active' => true,
            ],
            ['validate' => false]
        );
        $this->set('entity', $entity);
        if ($this->getRequest()->is('post')) {
            $data = [
                'identifier' => $this->getRequest()->getData('identifier'),
                'name' => $this->getRequest()->getData('name'),
                'description' => $this->getRequest()->getData('description'),
                'active' => $this->getRequest()->getData('active'),
            ];
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->save($this->KeywordLists, $entity, $data);
        }
    }

    /**
     * Edition d'une liste de mots clés
     * @param string $id
     * @throws Exception
     */
    public function edit(string $id)
    {
        $this->KeywordLists = $this->fetchTable('KeywordLists');
        $entity = $this->KeywordLists->find()
            ->where(
                [
                    'KeywordLists.id' => $id,
                    'KeywordLists.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();
        $this->set('entity', $entity);
        if ($this->getRequest()->is('put')) {
            $data = [
                'identifier' => $this->getRequest()->getData('identifier'),
                'name' => $this->getRequest()->getData('name'),
                'description' => $this->getRequest()->getData('description'),
                'active' => $this->getRequest()->getData('active'),
            ];
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->save($this->KeywordLists, $entity, $data);
        }
    }

    /**
     * Suppression d'une liste de mots clés
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete(string $id)
    {
        $this->KeywordLists = $this->fetchTable('KeywordLists');
        $entity = $this->KeywordLists->find()
            ->where(
                [
                    'KeywordLists.id' => $id,
                    'KeywordLists.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();

        $conn = $this->KeywordLists->getConnection();
        $conn->begin();
        if ($this->KeywordLists->delete($entity)) {
            $report = 'done';
            $conn->commit();
        } else {
            $report = 'Erreur lors de la suppression';
            $conn->rollback();
        }

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Visualisation d'une liste de mots clés
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        $this->KeywordLists = $this->fetchTable('KeywordLists');
        $entity = $this->KeywordLists->find()
            ->where(
                [
                    'KeywordLists.id' => $id,
                    'KeywordLists.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();
        $this->set('entity', $entity);
    }

    /**
     * Création de la nouvelle version de travail
     * @param string $id
     * @return Response|null
     * @throws Exception
     */
    public function newVersion(string $id)
    {
        $this->KeywordLists = $this->fetchTable('KeywordLists');
        $this->Keywords = $this->fetchTable('Keywords');
        $entity = $this->KeywordLists->find()
            ->where(
                [
                    'KeywordLists.id' => $id,
                    'KeywordLists.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();
        $newVersionExists = $this->Keywords->exists(
            [
                'keyword_list_id' => $id,
                'version' => 0
            ]
        );
        if ($newVersionExists || $entity->get('version') === 0) {
            $this->Flash->error(__("Il existe déjà une version de travail pour cette liste de mots clés"));
            return $this->redirect('/Keywords/index/' . $id);
        }
        $keywords = $this->Keywords->find()
            ->where(
                [
                    'keyword_list_id' => $id,
                    'version' => $entity->get('version')
                ]
            );
        $conn = $this->Keywords->getConnection();
        $conn->begin();
        /** @var Keyword $keyword */
        foreach ($keywords as $keyword) {
            $k = $this->Keywords->newEntity(
                [
                    'keyword_list_id' => $id,
                    'code' => $keyword->get('code'),
                    'name' => $keyword->get('name'),
                    'app_meta' => $keyword->get('app_meta'),
                    'version' => 0,
                ]
            );
            if (!$this->Keywords->save($k)) {
                $conn->rollback();
                $this->Flash->error(__("Échec lors de la création de la version de travail"));
                return $this->redirect('/Keywords/index/' . $id);
            }
        }
        $conn->commit();
        return $this->redirect('/Keywords/index/' . $id . '/0');
    }

    /**
     * Active la version des mots clés
     * @param string $id
     * @return Response|null
     * @throws Exception
     */
    public function publishVersion(string $id)
    {
        $this->KeywordLists = $this->fetchTable('KeywordLists');
        $this->Keywords = $this->fetchTable('Keywords');
        $entity = $this->KeywordLists->find()
            ->where(
                [
                    'KeywordLists.id' => $id,
                    'KeywordLists.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();
        $newVersionExists = $this->Keywords->exists(
            [
                'keyword_list_id' => $id,
                'version' => 0
            ]
        );
        if (!$newVersionExists) {
            $this->Flash->error(__("Impossible de publier une liste de mots clés vide"));
            return $this->redirect('/Keywords/index/' . $id);
        }
        $newVersion = $entity->get('version') + 1;
        $conn = $this->KeywordLists->getConnection();
        $conn->begin();
        $count = $this->Keywords->updateAll(
            ['version' => $newVersion],
            [
                'keyword_list_id' => $id,
                'version' => 0
            ]
        );
        $entity->set('version', $newVersion);
        if ($count && $this->KeywordLists->save($entity)) {
            $conn->commit();
        } else {
            $conn->rollback();
            $this->Flash->error(__("Une erreur est survenue lors de la création de la nouvelle version"));
        }
        return $this->redirect('/KeywordLists/index');
    }

    /**
     * Supprime la version de travail
     * @param string $id
     * @return Response|null
     * @throws Exception
     */
    public function removeVersion(string $id)
    {
        $this->KeywordLists = $this->fetchTable('KeywordLists');
        $this->Keywords = $this->fetchTable('Keywords');
        $this->KeywordLists->find()
            ->where(
                [
                    'KeywordLists.id' => $id,
                    'KeywordLists.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();
        $this->Keywords->deleteAll(
            [
                'keyword_list_id' => $id,
                'version' => 0
            ]
        );
        return $this->redirect('/KeywordLists/index');
    }
}
