<?php

/**
 * Versae\Controller\UsersController
 */

namespace Versae\Controller;

use ArrayObject;
use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\Component\CookieComponent;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Factory;
use AsalaeCore\Form\ForgottenPasswordForm;
use AsalaeCore\Http\Response;
use AsalaeCore\Utility\FormatError;
use AsalaeCore\Utility\Session;
use Cake\Core\Configure;
use Cake\Database\Expression\QueryExpression;
use Cake\Datasource\EntityInterface;
use Cake\Event\EventInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response as CakeResponse;
use Cake\ORM\Query;
use Cake\Utility\Hash;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Versae\Controller\Component\EmailsComponent;
use Versae\Form\ChangeEntityForm;
use Versae\Model\Entity\Fileupload;
use Versae\Model\Entity\OrgEntity;
use Versae\Model\Entity\User;
use Versae\Model\Table\AuthUrlsTable;
use Versae\Model\Table\ConfigurationsTable;
use Versae\Model\Table\DepositsTable;
use Versae\Model\Table\FileuploadsTable;
use Versae\Model\Table\LdapsTable;
use Versae\Model\Table\MailsTable;
use Versae\Model\Table\OrgEntitiesTable;
use Versae\Model\Table\RolesTable;
use Versae\Model\Table\UsersTable;
use Versae\Model\Table\VersionsTable;

/**
 * Tout ce qui conserne l'utilisateur de l'application
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AuthUrlsTable AuthUrls
 * @property ConfigurationsTable Configurations
 * @property CookieComponent Cookie
 * @property EmailsComponent Emails
 * @property FileuploadsTable Fileuploads
 * @property LdapsTable Ldaps
 * @property MailsTable Mails
 * @property OrgEntitiesTable OrgEntities
 * @property RolesTable Roles
 * @property UsersTable Users
 * @property VersionsTable Versions
 */
class UsersController extends AppController implements ApiInterface
{
    use ApiTrait;
    use RenderDataTrait;

    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public $paginate = [
        'order' => [
            'username' => 'asc',
        ],
        'sortable' => [
            'role' => 'Roles.name',
            'org_entity' => 'OrgEntities.name',
            'ldap' => 'Ldaps.name',
        ],
    ];

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return [
            'default' => [
                'post' => [
                    'function' => 'apiAdd',
                ],
            ],
        ];
    }

    /**
     * Authorize les méthodes login et logout
     *
     * @param EventInterface $event An Event instance
     * @return CakeResponse|void|null
     * @link https://book.cakephp.org/4/en/controllers.html#request-life-cycle-callbacks
     */
    public function beforeFilter(EventInterface $event)
    {
        $this->Authentication->addUnauthenticatedActions(
            [
                'ajaxLogin',
                'forgottenPassword',
                'forgottenPasswordMail',
                'initializePassword',
                'login',
                'logout',
                'resetSession',
            ]
        );
        return parent::beforeFilter($event);
    }

    public const TABLE_INDEX = 'users-table';

    /**
     * Liste des utilisateurs accessibles par l'utilisateur connecté
     */
    public function index()
    {
        $this->loadComponent('AsalaeCore.Index');
        $this->Index->init();

        $sa = $this->archivalAgency;
        $exp = new QueryExpression('ChangeEntityRequests.id IS NULL');
        $query = $this->Users->find()
            ->select(['same_archival_agency' => $exp])
            ->enableAutoFields()
            ->leftJoinWith(
                'ChangeEntityRequests',
                function (Query $q) {
                    return $q->where(
                        ['ChangeEntityRequests.target_archival_agency_id' => $this->archivalAgencyId]
                    );
                }
            )
            ->innerJoinWith('OrgEntities.ArchivalAgencies')
            ->where(
                [
                    [
                        'OR' => [
                            'ArchivalAgencies.id' => $this->archivalAgencyId,
                            'ChangeEntityRequests.id IS NOT' => null,
                        ],
                    ],
                ]
            )
            ->contain(
                [
                    'OrgEntities',
                    'Roles',
                    'Ldaps'
                ]
            );
        $this->Index->setQuery($query)
            ->filter('active', IndexComponent::FILTER_IS)
            ->filter('created', IndexComponent::FILTER_DATEOPERATOR)
            ->filter('email', IndexComponent::FILTER_ILIKE)
            ->filter('favoris', IndexComponent::FILTER_FAVORITES)
            ->filter(
                'is_linked_to_ldap',
                function ($v) {
                    return ['Users.ldap_id IS' . ($v ? ' NOT' : '') => null];
                }
            )
            ->filter('ldap_id')
            ->filter('name', IndexComponent::FILTER_ILIKE)
            ->filter('org_entity_id', IndexComponent::FILTER_IN)
            ->filter('role_id', IndexComponent::FILTER_IN)
            ->filter('username', IndexComponent::FILTER_ILIKE);

        try {
            $data = $this->paginate($query)
                ->map(
                    function (EntityInterface $entity) {
                        $entity->setVirtual(['deletable', 'is_linked_to_ldap']);
                        return $entity;
                    }
                )
                ->toArray();
        } catch (NotFoundException) {
            $this->Flash->error(
                __("La page demandée n'existe plus, retour à la page 1")
            );
            return $this->redirect('/users/index');
        }
        $this->set('data', $data);

        // Options
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $entities = $this->OrgEntities->find(
            'list',
            [
                'valueField' => function (EntityInterface $entity) {
                    $parent = Hash::get($entity, 'parent.name');
                    $name = $entity->get('name');
                    return $parent ? sprintf('%s (%s)', $name, $parent) : $name;
                }
            ]
        )
            ->select(
                [
                    'OrgEntities.id',
                    'OrgEntities.name',
                    'Parents.name',
                ]
            )
            ->where(
                [
                    'OrgEntities.lft >=' => $sa->get('lft'),
                    'OrgEntities.rght <=' => $sa->get('rght'),
                ]
            )
            ->contain(['Parents'])
            ->order(['OrgEntities.name' => 'asc', 'OrgEntities.lft' => 'asc']);
        $this->set('org_entities', $entities->all());
        $this->Roles = $this->fetchTable('Roles');
        $roles = $this->Roles->find('list')
            ->where(
                [
                    'OR' => [
                        'Roles.org_entity_id IS' => null,
                        'Roles.org_entity_id' => $this->archivalAgencyId,
                    ],
                ]
            )
            ->order(['Roles.name' => 'asc']);
        $this->set('roles', $roles->all());
        $this->Ldaps = $this->fetchTable('Ldaps');
        $ldaps = $this->Ldaps->find('list')
            ->where(['Ldaps.org_entity_id' => $this->archivalAgencyId])
            ->order(['Ldaps.name' => 'asc']);
        $this->set('ldaps', $ldaps->all());
    }

    /**
     * Page d'identification
     *
     * @return CakeResponse|null
     * @throws Exception
     */
    public function login()
    {
        $this->Users = $this->fetchTable('Users');
        $result = $this->Authentication->getResult();
        $request = $this->getRequest();
        if ($request->is('get') && $result->isValid()) { // déjà connecté
            $redirect = $this->redirect(
                ['controller' => 'Home', 'action' => 'index']
            );
            return $this->redirect($redirect);
        }
        if ($request->is('post')) {
            if ($result->isValid()) {
                $redirect = $this->redirect(
                    ['controller' => 'Home', 'action' => 'index']
                );
                return $this->redirect($redirect);
            }
            $this->Flash->error(__("Login ou mot de passe invalide"));
        }
        $this->set(
            'entity',
            $this->Users->newEmptyEntity()
        );

        $this->set('pageTitle', 'Connexion');
        $this->viewBuilder()->setLayout('Libriciel/Login.login');
        $this->viewBuilder()->setTemplate('Libriciel/Login.login');
    }

    /**
     * Déconnexion
     *
     * @return CakeResponse|null
     */
    public function logout()
    {
        $this->Authentication->logout();
        $this->destroySession();
        return $this->redirect('/');
    }

    /**
     * Détruit proprement la session
     */
    private function destroySession()
    {
        $this->Users = $this->fetchTable('Users');
        $this->loadComponent('AsalaeCore.Cookie');
        $this->Cookie->setConfig('encryption', false);

        $cookies = $this->getRequest()->getCookieParams();
        unset($cookies['PHPSESSID'], $cookies['XDEBUG_SESSION']);
        foreach (array_keys($cookies) as $cookie) {
            $this->Cookie->delete($cookie);
        }

        if ($this->userId) {
            $this->Fileuploads = $this->fetchTable('Fileuploads');
            $query = $this->Fileuploads->find()
                ->select(['id', 'path'])
                ->where(['user_id' => $this->userId, 'locked' => false]);
            $tmpCount = strlen(TMP);
            /** @var Fileupload $file */
            foreach ($query as $file) {
                if (
                    substr($file->get('path'), 0, $tmpCount) === TMP
                    && is_writable($file->get('path'))
                ) {
                    Filesystem::remove($file->get('path'));
                }
                $this->Fileuploads->delete($file);
            }
            // Cookies
            $entity = $this->Users->find()
                ->where(['id' => $this->userId])
                ->first();
            $oldCookies = $entity ? (array)json_decode($entity->get('cookies'))
                : [];
            $json = json_encode(
                array_filter(array_merge($cookies, $oldCookies))
            );
            if ($entity && $entity->get('cookies') !== $json) {
                $this->Users->patchEntity(
                    $entity,
                    ['cookies' => json_encode($cookies)]
                );
                $this->Users->save($entity);
            }
        }

        $this->getRequest()->getSession()->destroy();
    }

    /**
     * Modification du profil d'utilisateur
     */
    public function edit()
    {
        return $this->editByAdmin($this->userId);
    }

    /**
     * Modification d'un utilisateur
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function editByAdmin(string $id)
    {
        $request = $this->getRequest();
        $request->allowMethod('ajax');
        $this->viewBuilder()->setLayout('ajax');

        $this->Users = $this->fetchTable('Users');
        $request = $this->getRequest();
        $session = $request->getSession();
        $conditions = ['Users.id' => $id];
        $adminTech = $session->read('Admin.tech');
        if (!$adminTech) {
            $sa = $this->archivalAgency;
            $conditions += [
                'OrgEntities.lft >=' => $sa->get('lft'),
                'OrgEntities.rght <=' => $sa->get('rght'),
            ];
        }
        $entity = $this->Users->find()
            ->contain(['OrgEntities' => ['TypeEntities'], 'Roles'])
            ->where($conditions)
            ->firstOrFail();
        $entity->unset('password');

        if ($request->is('put')) {
            $data = [
                'high_contrast' => $request->getData('high_contrast'),
                'org_entity_id' => $entity->get('org_entity_id'), // important pour la validation
            ];

            if (!$entity->get('ldap_id')) {
                $data['username'] = $request->getData('username');
                $data['name'] = $request->getData('name');
                $data['email'] = $request->getData('email');
            }

            $password = $request->getData('password');
            if (!$entity->get('ldap_id') && $password) {
                $data += [
                    'password' => $password,
                    'confirm-password' => $request->getData(
                        'confirm-password'
                    ),
                ];
            }
            if ($request->getParam('action') === 'editByAdmin') {
                $data += [
                    'role_id' => $request->getData('role_id'),
                    'active' => $request->getData('active'),
                ];
            }
            $this->Users->patchEntity($entity, $data);
            $this->loadComponent('AsalaeCore.Modal');
            if ($this->Users->save($entity)) {
                $this->Modal->success();
                $entity = $this->Users->find()
                    ->contain(['OrgEntities' => ['TypeEntities'], 'Roles'])
                    ->where(['Users.id' => $id])
                    ->firstOrFail();
                $entity->setVirtual(['deletable']);
                $json = $entity->toArray();
                unset($json['password'], $json['confirm-password']);
                if ($entity->get('id') === $this->userId) {
                    $request->getSession()->write('User', $json);
                } else {
                    Session::emitReset($entity->id);
                }
                return $this->renderDataToJson($json);
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('user', $entity);
        $this->setAvailablesRoles($entity->get('org_entity_id'));
    }

    /**
     * Ajouter un utilisateur
     * @param string|null $orgEntityId
     * @return CakeResponse
     * @throws Exception
     */
    public function add(string $orgEntityId = null)
    {
        $request = $this->getRequest();
        $request->allowMethod('ajax');
        $this->viewBuilder()->setLayout('ajax');
        $this->checkAccess($orgEntityId);

        $this->Users = $this->fetchTable('Users');
        $this->Roles = $this->fetchTable('Roles');

        /** @var User $entity */
        $entity = $this->Users->newEntity(
            [
                'org_entity_id' => $orgEntityId,
                'agent_type' => 'person',
            ]
        );

        if ($orgEntityId !== null) {
            $this->OrgEntities = $this->fetchTable('OrgEntities');
            $orgEntity = $this->OrgEntities->get($orgEntityId);
            $entity->set('org_entity', $orgEntity);
        }

        if ($request->is('post')) {
            $password = $this->Users->generatePassword();
            $this->Users->patchEntity(
                $entity,
                [
                    'role_id' => $request->getData('role_id'),
                    'username' => h($request->getData('username')),
                    'password' => $password,
                    'confirm-password' => $password,
                    'name' => h($request->getData('name')),
                    'email' => $request->getData('email'),
                    'high_contrast' => $request->getData('high_contrast'),
                    'active' => true,
                    'org_entity_id' => $orgEntityId
                        ?: $request->getData('org_entity_id'),
                    'agent_type' => 'person',
                ]
            );
            $this->loadComponent('AsalaeCore.Modal');
            if ($this->Users->save($entity)) {
                $this->Modal->success();
                $entity = $this->Users->find()
                    ->where(['Users.id' => $entity->id])
                    ->contain(['OrgEntities'])
                    ->firstOrFail();
                $entity->setVirtual(['deletable', 'is_linked_to_ldap']);
                $json = $entity->toArray();
                $json['role'] = $this->Roles->get($json['role_id'])->toArray();
                unset($json['password'], $json['confirm-password']);
                $this->loadComponent('Emails');
                $this->Emails->submitEmailNewUser($entity);
                return $this->renderJson(json_encode($json));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('user', $entity);
        $this->set('title', __("Ajouter un utilisateur"));
        $this->set('orgEntityId', $orgEntityId);
        if ($orgEntityId) {
            $this->setAvailablesRoles($orgEntityId);
        } else {
            $this->OrgEntities = $this->fetchTable('OrgEntities');
            $userSA = $this->archivalAgency;
            $query = $this->OrgEntities->find()
                ->select(
                    [
                        'OrgEntities.id',
                        'OrgEntities.name',
                        'Parents.name',
                    ]
                )
                ->where(
                    [
                        'OrgEntities.lft >=' => $userSA->get('lft'),
                        'OrgEntities.rght <=' => $userSA->get('rght'),
                    ]
                )
                ->contain(['Parents'])
                ->order(['OrgEntities.name' => 'asc', 'OrgEntities.lft' => 'asc']);
            $allNames = [];
            /** @var OrgEntity $entity */
            foreach ($query as $entity) {
                $name = $entity->get('name');
                if ($parent = $entity->get('parent')) {
                    $name .= ' (' . $parent->get('name') . ')';
                }
                $allNames[$entity->get('id')] = $name;
            }
            $this->set('entities', $allNames);
            if ($request->getData('org_entity_id')) {
                $this->setAvailablesRoles($request->getData('org_entity_id'));
            }
        }
    }

    /**
     * permet d'obtenir la liste des rôles Disponible pour un OrgEntity
     * @param int|string $orgEntityId
     * @return array
     */
    private function setAvailablesRoles($orgEntityId): array
    {
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $this->Roles = $this->fetchTable('Roles');
        if ($orgEntityId) {
            $sa = $this->OrgEntities->getEntitySA($orgEntityId);
            $roles = $this->OrgEntities->findByRoles($sa)
                ->where(['OrgEntities.id' => $orgEntityId, 'agent_type' => 'person'])
                ->all()
                ->toArray();
            $roles = $roles ? array_reduce(
                $roles,
                function ($res, EntityInterface $v) {
                    $res[$v->get('Roles')['id']] = $v->get('Roles')['name'];
                    return $res;
                }
            ) : [];
        } else {
            $roles = [];
        }
        $this->set('roles', $roles);
        return $roles;
    }

    /**
     * Permet la connexion par ajax
     */
    public function ajaxLogin()
    {
        $request = $this->getRequest();
        $this->viewBuilder()->setLayout('ajax');
        if (Configure::read('Keycloak.enabled') || Configure::read('OpenIDConnect.enabled')) {
            if ($request->is('post')) {
                $this->loadComponent('AsalaeCore.Modal');
                if ($this->user) {
                    $this->Modal->success();
                } else {
                    $this->Modal->fail();
                }
            }
            return;
        }
        $this->Users = $this->fetchTable('Users');
        $user = $this->Users->newEmptyEntity();

        if ($request->is('post')) {
            $this->loadComponent('AsalaeCore.Modal');
            if ($request->getData('prev_username') !== $request->getData('username')) {
                $user->setErrors(['username' => __("Identifiant incorrect")]);
                $this->Modal->fail();
                $this->set('entity', $user);
                return;
            }

            // vérifi le login/mdp et connecte l'utilisateur
            $result = $this->Authentication->getResult();
            if ($result->isValid()) {
                $this->Modal->success();
            } else {
                $this->Modal->fail();
                $user->setErrors(['name' => __("Login ou mot de passe invalide")]);
            }
        }
        $this->set('entity', $user);
    }

    /**
     * Permet de sauvegarder le menu utilisateur
     */
    public function ajaxSaveMenu()
    {
        $this->getRequest()->allowMethod('ajax');
        $this->viewBuilder()->setLayout('ajax');
        $this->Users = $this->fetchTable('Users');
        $report = [];
        if ($this->getRequest()->is('post')) {
            $entity = $this->Users->get($this->userId);
            $data = ['menu' => $this->getRequest()->getData('menu')];
            $this->Users->patchEntity($entity, $data);
            $report['success'] = (bool)$this->Users->save($entity);
            if ($report['success']) {
                $this->getRequest()->getSession()->write(
                    'Auth.menu',
                    $data['menu']
                );
            }
        }
        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Ajout par webservices
     * @return CakeResponse
     * @throws Exception
     */
    protected function apiAdd()
    {
        $request = $this->getRequest();
        $password = $request->getData('agent_type') === 'software'
            ? $request->getData('password')
            : $this->Users->generatePassword();
        $high_contrast = null;
        if ($request->getData('high_contrast')) {
            $high_contrast = $request->getData('high_contrast') === 'true';
        }
        $active = null;
        if ($request->getData('active')) {
            $active = $request->getData('active') === 'true';
        }
        $is_validator = null;
        if ($request->getData('is_validator')) {
            $is_validator = $request->getData('is_validator') === 'true';
        }
        $use_cert = null;
        if ($request->getData('use_cert')) {
            $use_cert = $request->getData('use_cert') === 'true';
        }
        $entity = $this->Users->newEntity(
            [
                'org_entity_id' => $request->getData('org_entity_id'),
                'agent_type' => 'person',
                'role_id' => $request->getData('role_id'),
                'username' => $request->getData('username'),
                'password' => $password,
                'confirm-password' => $password,
                'name' => $request->getData('name'),
                'email' => $request->getData('email'),
                'high_contrast' => $high_contrast,
                'active' => $active,
                'is_validator' => $is_validator,
                'use_cert' => $use_cert,
                'ldap_id' => $request->getData('ldap_id'),
                'ldap_login' => $request->getData('ldap_login'),
            ]
        );
        if ($this->Users->save($entity)) {
            $this->loadComponent('EventLogger');
            $this->EventLogger->logAdd($entity);
            $json = $entity->toArray();
            $this->loadComponent('Emails');
            $this->Emails->submitEmailNewUser($entity);
            return $this->renderData(
                ['success' => true, 'id' => $entity->id]
                + (isset($json['code']) ? ['code' => $json['code']] : [])
            );
        } else {
            return $this->renderData(
                ['success' => false, 'errors' => $entity->getErrors()]
            );
        }
    }

    /**
     * Supprime un utilisateur
     * @param string $id
     * @return CakeResponse
     * @throws Exception
     */
    public function delete(string $id): CakeResponse
    {
        $session = $this->getRequest()->getSession();
        if ($session->read('Auth.id') === $id) {
            throw new ForbiddenException(__("Un utilisateur ne peut se supprimer lui même"));
        }

        $this->Users = $this->fetchTable('Users');
        $entity = $this->Users->get($id);

        $hasEditingDeposits = $this->fetchTable('Deposits')
            ->exists(
                [
                    'state' => DepositsTable::S_EDITING,
                    'created_user_id' => $id,
                ]
            );

        if ($hasEditingDeposits && !$this->getRequest()->getHeader('X-Versae-Force-Delete')) {
            return $this->response->withHeader('X-Asalae-Deposits', 'true');
        }

        $conn = $this->Users->getConnection();
        $conn->begin();
        if ($this->Users->delete($entity)) {
            $report = 'done';
            $conn->commit();
        } else {
            $report = 'Erreur lors de la suppression';
            $conn->rollback();
        }

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Permet de reconstruire la session à la demande d'un administrateur
     * ex : Les permissions ont changés
     */
    public function resetSession()
    {
        $request = $this->getRequest();
        $request->allowMethod('ajax');
        $request->allowMethod('post');
        $reset = Factory\Utility::get('Session')->doReset(
            $request->getSession(),
            $request->getData('message')
        );
        if ($reset) {
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->success();
            return $this->renderData(
                [
                    'css_class' => 'alert alert-info',
                    'text' => __(
                        "Votre session a été réinitialisée par un administrateur. \n"
                        . "Vos permissions ont potentiellement changé."
                    ),
                    'id' => 'instant-message',
                ]
            );
        }
        return $this->renderData([]);
    }

    /**
     * Réinitialisation du mot de passe par url
     * @param string $id
     * @return CakeResponse|null
     * @throws Exception
     */
    public function forgottenPasswordMail(string $id)
    {
        return $this->initializePassword($id);
    }

    /**
     * Initialisation du mot de passe par url
     * @param string $id
     * @return CakeResponse|null|void
     * @throws Exception
     */
    public function initializePassword(string $id)
    {
        $this->viewBuilder()->setLayout('not_connected');
        $this->AuthUrls = $this->fetchTable('AuthUrls');
        $this->Users = $this->fetchTable('Users');
        if (!$this->AuthUrls->isAuthorized($this->getRequest())) {
            $this->getRequest()->getSession()->clear(true);
            $this->set('notAuthorized', true);
            return;
        }
        $this->set('code', $this->AuthUrls->getCode($this->getRequest()));
        $user = $this->Users->find()
            ->contain(['OrgEntities' => ['TypeEntities'], 'Roles'])
            ->where(['Users.id' => $id])
            ->contain(
                [
                    'OrgEntities' => [
                        'ArchivalAgencies' => ['Configurations'],
                        'TypeEntities',
                    ],
                    'Ldaps',
                    'Roles',
                ]
            )
            ->firstOrFail();
        $this->set('user', $user);

        if ($this->getRequest()->is('put')) {
            $data = [
                'password' => $this->getRequest()->getData('password'),
                'confirm-password' => $this->getRequest()->getData(
                    'confirm-password'
                ),
            ];
            $this->Users->patchEntity($user, $data);
            if ($this->Users->save($user)) {
                $this->AuthUrls->consume($this->getRequest());
                $this->destroySession();
                $this->loadComponent('Login');
                $this->Login->loadCookies($user);
                $this->Authentication->setIdentity(new ArrayObject($user));
                $this->getRequest()->getSession()->write('Auth', $user);
                return $this->redirect('/');
            }
        }
    }

    /**
     * Permet de redéfinir un mot de passe oublié
     * @return CakeResponse|null
     */
    public function forgottenPassword()
    {
        if ($this->getRequest()->is('post')) {
            $form = new ForgottenPasswordForm();
            $result = $form->execute($this->getRequest()->getData());
            $code = $form->code;
            if ($code) {
                return $this->redirect(
                    '/auth-urls/activate/' . $code->get('code')
                );
            }
            $errors = $form->getErrors();
            $mailer = $form->mailer;
            if (($result && empty($errors)) || isset($errors['user']['notFound'])) {
                if ($mailer) {
                    $this->Mails = $this->fetchTable('Mails');
                    $this->Mails->asyncMail($mailer, null);
                }
                $this->Flash->success(
                    __(
                        "Si vos informations sont correctes, un e-mail a été envoyé"
                    )
                );
                return $this->redirect('/users/login');
            } else {
                $this->Flash->error(
                    $errors['username']['_empty'] ?? __("Erreur")
                );
            }
        }
    }

    /**
     * Donne les roles pour un org_entity_id donné
     * @param string $org_entity_id
     * @return CakeResponse
     * @throws Exception
     */
    public function getRolesOptions(string $org_entity_id)
    {
        $this->checkAccess($org_entity_id);
        return $this->renderDataToJson(
            $this->setAvailablesRoles($org_entity_id)
        );
    }

    /**
     * Visualisation d'un utilisateur
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        $sa = $this->archivalAgency;
        $entity = $this->Users->find()
            ->where(
                [
                    'Users.id' => $id,
                    'OrgEntities.lft >=' => $sa->get('lft'),
                    'OrgEntities.rght <=' => $sa->get('rght'),
                ]
            )
            ->contain(
                [
                    'Roles',
                    'Ldaps',
                    'OrgEntities',
                ]
            )
            ->firstOrFail();
        $this->set('entity', $entity);
    }

    /**
     * Vérifie qu'un org_entity fait bien partie du service d'archive de l'utilisateur
     * Laisse l'accès à un admin technique
     * @param int $id
     * @throws NotFoundException|Exception
     */
    private function checkAccess($id)
    {
        $adminTech = $this->getRequest()->getSession()->read('Admin.tech');
        if ($adminTech || !$id) {
            return;
        }
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $this->OrgEntities->find()
            ->where(
                [
                    'OrgEntities.id' => $id,
                    'OrgEntities.lft >=' => $this->archivalAgency->get('lft'),
                    'OrgEntities.rght <=' => $this->archivalAgency->get('rght'),
                ]
            )
            ->firstOrFail();
    }

    /**
     * Interface pour modifier l'entité d'un utilisateur
     * @param string $user_id
     * @return CakeResponse|void
     * @throws Exception
     */
    public function changeEntity(string $user_id)
    {
        $user = $this->Users->find()
            ->innerJoinWith('OrgEntities.ArchivalAgencies')
            ->where(
                [
                    'Users.id' => $user_id,
                    'ArchivalAgencies.id' => $this->archivalAgencyId,
                ]
            )
            ->contain(
                [
                    'Roles',
                    'Ldaps',
                    'OrgEntities',
                    'Deposits' => function (Query $q) {
                        return $q->where(
                            ['Deposits.state' => DepositsTable::S_EDITING]
                        );
                    },
                ]
            )
            ->firstOrFail();
        $this->set('user', $user);

        $form = new ChangeEntityForm();
        $form->setData(
            [
                'base_archival_agency_id' => $this->archivalAgencyId,
                'archival_agency_id' => $this->archivalAgencyId,
                'user_id' => $user_id,
            ]
        );
        $ChangeEntityRequests = $this->fetchTable('ChangeEntityRequests');
        $existingLink = $ChangeEntityRequests->find()
            ->where(
                [
                    'user_id' => $user_id,
                    'base_archival_agency_id' => $this->archivalAgencyId,
                ]
            )
            ->first();
        if ($existingLink) {
            $form->setData(
                [
                    'base_archival_agency_id' => $this->archivalAgencyId,
                    'user_id' => $user_id,
                    'archival_agency_id' => $existingLink->get('target_archival_agency_id'),
                    'reason' => $existingLink->get('reason'),
                ]
            );
        }
        $this->set('form', $form);

        $request = $this->getRequest();
        if ($request->is('post')) {
            $this->loadComponent('AsalaeCore.Modal');
            if ($form->execute($request->getData() + $form->getData())) {
                $exp = new QueryExpression('ChangeEntityRequests.id IS NULL');
                $user = $this->Users->find()
                    ->select(['same_archival_agency' => $exp])
                    ->enableAutoFields()
                    ->leftJoinWith(
                        'ChangeEntityRequests',
                        function (Query $q) {
                            return $q->where(
                                ['ChangeEntityRequests.target_archival_agency_id' => $this->archivalAgencyId]
                            );
                        }
                    )
                    ->where(['Users.id' => $user_id])
                    ->contain(
                        [
                            'Ldaps',
                            'OrgEntities',
                            'Roles',
                        ]
                    )
                    ->firstOrFail()
                    ->setVirtual(['deletable', 'is_linked_to_ldap']);
                $this->Modal->success();
                return $this->renderDataToJson($user);
            } else {
                $this->Modal->fail();
                FormatError::logFormErrors($form);
            }
        }

        // options
        $vField = fn(EntityInterface $e) => sprintf('%s - %s', $e->get('identifier'), $e->get('name'));
        $archivalAgencies = $this->fetchTable('OrgEntities')
            ->find('list', ['valueField' => $vField])
            ->innerJoinWith('TypeEntities')
            ->where(
                [
                    'TypeEntities.code' => 'SA',
                    'OrgEntities.active' => true,
                ]
            )
            ->orderAsc('OrgEntities.name')
            ->toArray();
        $this->set('archivalAgencies', $archivalAgencies);

        $orgEntities = $this->fetchTable('OrgEntities')
            ->find('list', ['valueField' => $vField])
            ->innerJoinWith('ArchivalAgencies')
            ->where(
                [
                    'OrgEntities.id !=' => $user->get('org_entity_id'),
                    'ArchivalAgencies.id' => $this->archivalAgencyId,
                ]
            )
            ->orderAsc('OrgEntities.name')
            ->toArray();
        $this->set('orgEntities', $orgEntities);

        $roles = $this->fetchTable('Roles')->find('list')
            ->where(
                [
                    'OR' => [
                        'org_entity_id IS' => null,
                        'org_entity_id' => $this->archivalAgencyId,
                    ],
                    'agent_type' => $user->get('agent_type'),
                ]
            )
            ->orderAsc('name')
            ->toArray();
        $this->set('roles', $roles);

        $query = $this->fetchTable('OrgEntities')->find()
            ->where(
                [
                    'OrgEntities.lft >=' => $this->archivalAgency->get('lft'),
                    'OrgEntities.rght <=' => $this->archivalAgency->get('rght'),
                ]
            )
            ->contain(['TypeEntities' => ['Roles' => ['sort' => 'name']]]);
        $availablesRolesByEntities = [];
        /** @var EntityInterface $entity */
        foreach ($query as $entity) {
            $availablesRolesByEntities[$entity->id] = [];
            /** @var EntityInterface $role */
            foreach (Hash::get($entity, 'type_entity.roles') as $role) {
                $availablesRolesByEntities[$entity->id][] = $role->id;
            }
        }
        $this->set('availablesRolesByEntities', $availablesRolesByEntities);
    }

    /**
     * Permet d'accepter un utilisateur dans son service d'archives et lui
     * attribuer une entité
     * @param string $user_id
     * @return CakeResponse|void
     * @throws Exception
     */
    public function acceptUser(string $user_id)
    {
        $user = $this->Users->find()
            ->innerJoinWith(
                'ChangeEntityRequests',
                function (Query $q) {
                    return $q->where(
                        ['ChangeEntityRequests.target_archival_agency_id' => $this->archivalAgencyId]
                    );
                }
            )
            ->where(['Users.id' => $user_id])
            ->contain(
                [
                    'ChangeEntityRequests' => ['BaseArchivalAgencies'],
                    'OrgEntities',
                    'Roles',
                ]
            )
            ->firstOrFail();
        $this->set('user', $user);
        $this->set('entity', $user->get('change_entity_request'));

        $form = new ChangeEntityForm();
        $form->setData(
            [
                'base_archival_agency_id' => $this->archivalAgencyId,
                'archival_agency_id' => $this->archivalAgencyId,
                'user_id' => $user_id,
            ]
        );
        $this->set('form', $form);

        $request = $this->getRequest();
        if ($request->is('post')) {
            $this->loadComponent('AsalaeCore.Modal');
            if ($form->execute($request->getData() + $form->getData())) {
                $user = $this->Users->find()
                    ->where(['Users.id' => $user_id])
                    ->contain(
                        [
                            'Ldaps',
                            'OrgEntities',
                            'Roles',
                        ]
                    )
                    ->firstOrFail()
                    ->setVirtual(['deletable', 'is_linked_to_ldap']);
                $this->Modal->success();
                return $this->renderDataToJson($user);
            } else {
                $this->Modal->fail();
                FormatError::logFormErrors($form);
            }
        }

        // options
        $vField = fn(EntityInterface $e) => sprintf('%s - %s', $e->get('identifier'), $e->get('name'));
        $orgEntities = $this->fetchTable('OrgEntities')
            ->find('list', ['valueField' => $vField])
            ->innerJoinWith('ArchivalAgencies')
            ->where(['ArchivalAgencies.id' => $this->archivalAgencyId])
            ->orderAsc('OrgEntities.name')
            ->toArray();
        $this->set('orgEntities', $orgEntities);

        $roles = $this->fetchTable('Roles')->find('list')
            ->where(['agent_type' => $user->get('agent_type')])
            ->orderAsc('name')
            ->toArray();
        $this->set('roles', $roles);

        $query = $this->fetchTable('OrgEntities')->find()
            ->where(
                [
                    'OrgEntities.lft >=' => $this->archivalAgency->get('lft'),
                    'OrgEntities.rght <=' => $this->archivalAgency->get('rght'),
                ]
            )
            ->contain(['TypeEntities' => ['Roles' => ['sort' => 'name']]]);
        $availablesRolesByEntities = [];
        /** @var EntityInterface $entity */
        foreach ($query as $entity) {
            $availablesRolesByEntities[$entity->id] = [];
            /** @var EntityInterface $role */
            foreach (Hash::get($entity, 'type_entity.roles') as $role) {
                $availablesRolesByEntities[$entity->id][] = $role->id;
            }
        }
        $this->set('availablesRolesByEntities', $availablesRolesByEntities);
    }

    /**
     * Refus d'un utilisateur venant d'un autre service d'archive
     * @param string $user_id
     * @return Response|CakeResponse
     */
    public function refuseUser(string $user_id)
    {
        $this->getRequest()->allowMethod('delete');

        $ChangeEntityRequests = $this->fetchTable('ChangeEntityRequests');
        $changeEntityRequest = $ChangeEntityRequests->find()
            ->where(
                [
                    'user_id' => $user_id,
                    'target_archival_agency_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();
        $ChangeEntityRequests->deleteOrFail($changeEntityRequest);

        return $this->renderDataToJson(['report' => "done"]);
    }
}
