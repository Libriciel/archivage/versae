<?php

/**
 * Versae\Controller\FileuploadsController
 */

namespace Versae\Controller;

use AsalaeCore\Controller\RenderDataTrait;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Response;
use Exception;
use Versae\Model\Table\FileuploadsTable;

/**
 * Fichiers uploadés
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property FileuploadsTable $Fileuploads
 */
class FileuploadsController extends AppController
{
    use RenderDataTrait;

    /**
     * Permet de supprimer un fichier uploadé
     *      Suppression dans le dossier temporaire si Disponible
     *      Suppression en base
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete(string $id)
    {
        $this->getRequest()->allowMethod('delete');
        $report = [];
        $this->Fileuploads = $this->fetchTable('Fileuploads');
        $file = $this->Fileuploads->get($id);
        $report['entity_deleted'] = $this->Fileuploads->delete($file);
        if (!$report['entity_deleted']) {
            throw new BadRequestException(__("Impossible de supprimer cet enregistrement"));
        }
        if (is_file($file->get('path'))) {
            $report['file_deleted'] = false;
        }
        return $this->renderDataToJson($report);
    }
}
