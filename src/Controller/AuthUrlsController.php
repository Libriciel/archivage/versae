<?php

/**
 * Versae\Controller\AuthUrlsController
 */

namespace Versae\Controller;

use Cake\Event\EventInterface;
use Cake\Http\Response;
use Cake\Http\Response as CakeResponse;
use Versae\Model\Table\AuthUrlsTable;

/**
 * Permet d'accèder à certaines parties de l'application avec une simple url
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AuthUrlsTable $AuthUrls
 */
class AuthUrlsController extends AppController
{
    /**
     * Désactive l'autentification par défaut
     * @param EventInterface $event An Event instance
     * @return CakeResponse|void|null
     * @link https://book.cakephp.org/4/en/controllers.html#request-life-cycle-callbacks
     */
    public function beforeFilter(EventInterface $event)
    {
        $this->Authentication->allowUnauthenticated([$this->getRequest()->getParam('action')]);
        return parent::beforeFilter($event);
    }

    /**
     * Utilise le code et redirige vers l'url correspondant
     * @param string $code
     * @return Response|null|void
     * @fixme Déplacer dans un Middleware ou Authenticator/Identifier
     */
    public function activate(string $code)
    {
        $this->AuthUrls = $this->fetchTable('AuthUrls');
        $entity = $this->AuthUrls->find()
            ->where(['code' => $code])
            ->first();
        if (!$entity) {
            $this->setResponse($this->getResponse()->withStatus(410)); // Gone
            $this->viewBuilder()->setLayout('not_connected');
            $this->viewBuilder()->setTemplate('gone');
            $this->Flash->success(__("La ressource demandée n'est plus disponible"));
            return;
        }
        $this->Authentication->logout();
        $auth = [
            'code' => $code,
            'url' => $entity->get('url'),
            'url_id' => $entity->id,
        ];
        $session = $this->getRequest()->getSession();
        $session->clear(true);
        $session->write('Auth', $auth);
        return $this->redirect($entity->get('url') . '?code=' . $code)
            ->withHeader('code', $code);
    }
}
