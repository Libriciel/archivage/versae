<?php

/**
 * Versae\Controller\AssetsController
 */

namespace Versae\Controller;

use Cake\Event\EventInterface;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response as CakeResponse;
use Cake\Routing\Router;
use Cake\Core\Configure;

/**
 * Permet de générer des assets dynamique
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AssetsController extends AppController
{
    use CachableTrait;

    /**
     * Par défaut, on ne contrôle pas l'accès à ce Controller
     * @param EventInterface $event An Event instance
     * @return CakeResponse|void|null
     * @link https://book.cakephp.org/4/en/controllers.html#request-life-cycle-callbacks
     */
    public function beforeFilter(EventInterface $event)
    {
        $this->Authentication->allowUnauthenticated([$this->getRequest()->getParam('action')]);
        return parent::beforeFilter($event);
    }

    /**
     * Génère un background-image à partir du fichier indiqué dans
     * Assets.global-background
     */
    public function configuredBackground()
    {
        $path = Configure::read('Assets.global-background');

        if ($path && is_readable($path)) {
            return $this->renderCachable($path);
        }
        throw new NotFoundException();
    }

    /**
     * Génère une image à partir du fichier indiqué dans
     * Assets.login.logo-client
     */
    public function configuredLogoClient()
    {
        $path = Configure::read('Assets.login.logo-client');

        if ($path && is_readable($path)) {
            return $this->renderCachable($path);
        }
        throw new NotFoundException();
    }

    /**
     * Génère le css pour le logo client
     *
     * @param string $path
     * @return string
     */
    private function cssLogoClient(string $path): string
    {
        $url = Router::url("/Assets/configured-background", true);
        $position = Configure::read('Assets.global-background-position') ?: '50px 50px';

        $confPos = Configure::read('Assets.global-background-position') ?: '50px 50px';
        $posX = preg_match('/^(\d+)px/', $confPos, $matches) ? (int)$matches[1] : 0;

        $containerSize = 1170;
        [$width, $height] = getimagesize($path);
        $maxSize = $containerSize + (($width + $posX + 10) * 2);
        $height20 = $height + 20;
        return <<<EOT
div#body-content {
    background-image: url('$url');
    background-position: $position;
    background-repeat: no-repeat;
}
@media (max-width: {$maxSize}px) {
    div#body-content {
        background-image: none;
        padding-top: {$height20}px;
    }

    div#body-content:before {
        content: ' ';
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        width: 100%;
        height: {$height20}px;
        background-color: #fff;
        background-image: url('$url');
        background-position: center center;
        background-repeat: no-repeat;
        box-shadow: 0 0 1px rgba(0, 0, 0, 0.6);
    }
}
EOT;
    }

    /**
     * Génère un css
     */
    public function css()
    {
        $path = Configure::read('Assets.global-background');
        $css = $path && is_readable($path) ? $this->cssLogoClient($path) : '';
        return $this->getResponse()
            ->withHeader('Content-Type', 'text/css')
            ->withHeader('X-Content-Type-Options', 'nosniff')
            ->withHeader('Cache-control', 'public, no-cache')
            ->withHeader('Expires', $gmdate = gmdate(DATE_RFC1123, time()))
            ->withHeader('Last-Modified', $gmdate)
            ->withStringBody($css);
    }
}
