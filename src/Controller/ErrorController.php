<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.3.4
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace Versae\Controller;

use Cake\Core\BasePlugin;
use Cake\Core\Plugin;
use Cake\Database\Connection;
use Cake\Datasource\ConnectionManager;
use Cake\Event\EventInterface;
use Exception;

/**
 * Error Handling Controller
 *
 * Controller used by ExceptionRenderer to render error responses.
 */
class ErrorController extends AppController
{
    /**
     * Initialization hook method.
     */
    public function initialize(): void
    {
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Authentication.Authentication');
        $this->Authentication->allowUnauthenticated([$this->getRequest()->getParam('action')]);
    }

    /**
     * beforeRender callback.
     *
     * @param EventInterface $event An Event instance
     * @throws Exception
     * @link https://book.cakephp.org/4/en/controllers.html#request-life-cycle-callbacks
     */
    public function beforeRender(EventInterface $event)
    {
        $builder = $this->viewBuilder();
        $template = $builder->getTemplate();
        parent::beforeRender($event);

        $core = new BasePlugin(
            [
                'autoload' => false,
                'bootstrap' => false,
                'routes' => false,
                'console' => true,
                'classBase' => 'src',
                'classPath' => ASALAE_CORE . DS,
                'ignoreMissing' => false,
                'name' => 'AsalaeCore',
                'path' => ASALAE_CORE_INCLUDE_PATH . DS,
                'configPath' => ASALAE_CORE_INCLUDE_PATH . DS . 'config' . DS,
            ]
        );
        Plugin::getCollection()->add($core);

        $builder->setPlugin('AsalaeCore');
        $builder->setTemplatePath('Error');
        $builder->setLayoutPath('');
        $builder->setLayout('error');
        $builder->setTemplate($template);
        if ($this->getRequest()->is('ajax')) {
            $builder->setLayout('ajax');
        }

        try {
            /** @var Connection $conn */
            $conn = ConnectionManager::get('default');
            $i = 0; // juste au cas où
            while ($conn->inTransaction() && $i < 1000) {
                $conn->rollback();
                $i++;
            }
        } catch (Exception) {
        }
    }
}
