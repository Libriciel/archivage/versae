<?php

/**
 * Versae\Controller\ArchivingSystemsController
 */

namespace Versae\Controller;

use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Utility\FormatError;
use Cake\Database\Connection;
use Cake\Database\Exception\MissingConnectionException;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\EntityInterface;
use Cake\Http\Response;
use Exception;
use PDOException;
use Versae\Exception\GenericException;
use Versae\Model\Entity\ArchivingSystem;
use Versae\Model\Table\ArchivingSystemsTable;
use Versae\Model\Table\OrgEntitiesTable;

/**
 * ArchivingSystems
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property ArchivingSystemsTable ArchivingSystems
 * @property OrgEntitiesTable OrgEntities
 */
class ArchivingSystemsController extends AppController
{
    use RenderDataTrait;
    use ApiTrait;

    /**
     * Options de pagination par défaut
     * @var array
     */
    public $paginate = [
        'order' => [
            'ArchivingSystems.id' => 'desc'
        ]
    ];

    /**
     * @var null|bool connecté à la bdd (défini par isConnected())
     */
    private $isConnected = null;

    /**
     * Liste les enregistrements
     */
    public function index()
    {
        if (!$this->isConnected()) {
            return $this->getResponse()->withStringBody(
                __(
                    "Le serveur de base de données doit fonctionner pour accéder à cette fonctionnalité"
                )
            );
        }
        $this->ArchivingSystems = $this->fetchTable('ArchivingSystems');
        $archivingSystems = $this->ArchivingSystems->find()
            ->contain(['OrgEntities'])
            ->order(['ArchivingSystems.name'])
            ->all()
            ->map(
                function (EntityInterface $e) {
                    $e->setVirtual(['deletable']);
                    return $e;
                }
            )
            ->toArray();
        $this->set('archivingSystems', $archivingSystems);
        $this->set('tableId', 'index-archiving-systems-table');
    }

    /**
     * Action d'ajout
     */
    public function add()
    {
        $entity = $this->ArchivingSystems->newEntity(['chunk_size' => 2097152], ['validate' => false]);
        $request = $this->getRequest();

        if ($request->is('post')) {
            $data = [
                'org_entity_id' => $this->archivalAgencyId
                    ?: $request->getData('org_entity_id'),
                'name' => $request->getData('name'),
                'url' => $request->getData('url'),
                'username' => $request->getData('username'),
                'password' => $request->getData('password'),
                'use_proxy' => $request->getData('use_proxy'),
                'active' => $request->getData('active'),
                'ssl_verify_peer' => $request->getData('ssl_verify_peer'),
                'ssl_verify_peer_name' => $request->getData(
                    'ssl_verify_peer_name'
                ),
                'ssl_verify_depth' => $request->getData('ssl_verify_depth'),
                'ssl_verify_host' => $request->getData('ssl_verify_host'),
                'ssl_cafile' => $request->getData('ssl_cafile') ?: null,
                'chunk_size' => $request->getData('chunk_size') ?: null,
            ];
            $this->ArchivingSystems->patchEntity($entity, $data);
            $this->loadComponent('AsalaeCore.Modal');
            if ($this->ArchivingSystems->save($entity)) {
                $this->Modal->success();
                $entity = $this->ArchivingSystems->find()
                    ->where(['ArchivingSystems.id' => $entity->id])
                    ->contain(['OrgEntities'])
                    ->firstOrFail();
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }
        $this->set('entity', $entity);
        $this->set('sas', $this->optionsSa());
        if (!$this->adminTech && $this->archivalAgencyId) {
            $this->set('org_entity_id', $this->archivalAgencyId);
        }
    }

    /**
     * Action modifier
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function edit(string $id)
    {
        $query = $this->ArchivingSystems->find()
            ->where(['ArchivingSystems.id' => $id])
            ->contain(['OrgEntities']);
        if (!$this->adminTech && $this->archivalAgencyId) {
            $query->where(['ArchivingSystems.org_entity_id' => $this->archivalAgencyId]);
        }
        $entity = $query->firstOrFail();
        $request = $this->getRequest();

        if ($request->is('put')) {
            $data = [
                'org_entity_id' => $this->archivalAgencyId
                    ?: ($entity->get('removable') ? $request->getData('org_entity_id') : $entity->get('org_entity_id')),
                'name' => $request->getData('name'),
                'url' => $request->getData('url'),
                'username' => $request->getData('username'),
                'password' => $request->getData('password'),
                'use_proxy' => $request->getData('use_proxy'),
                'active' => $request->getData('active'),
                'ssl_verify_peer' => $request->getData('ssl_verify_peer'),
                'ssl_verify_peer_name' => $request->getData('ssl_verify_peer_name'),
                'ssl_verify_depth' => $request->getData('ssl_verify_depth'),
                'ssl_verify_host' => $request->getData('ssl_verify_host'),
                'ssl_cafile' => $request->getData('ssl_cafile') ?: null,
                'chunk_size' => $request->getData('chunk_size') ?: null,
            ];
            $this->ArchivingSystems->patchEntity($entity, $data);
            $this->loadComponent('AsalaeCore.Modal');
            if ($this->ArchivingSystems->save($entity)) {
                $this->Modal->success();
                $entity = $this->ArchivingSystems->find()
                    ->where(['ArchivingSystems.id' => $entity->id])
                    ->contain(['OrgEntities'])
                    ->firstOrFail();
                return $this->renderJson(json_encode($entity->toArray()));
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('entity', $entity);
        $this->set('sas', $this->optionsSa());
        if (!$this->adminTech && $this->archivalAgencyId) {
            $this->set('org_entity_id', $this->archivalAgencyId);
        }
    }

    /**
     * Action supprimer
     * @param string $id
     * @return Response
     */
    public function delete(string $id)
    {
        $this->getRequest()->allowMethod('delete');
        $query = $this->ArchivingSystems->find()
            ->where(['ArchivingSystems.id' => $id]);
        if (!$this->adminTech && $this->archivalAgencyId) {
            $query->where(['ArchivingSystems.org_entity_id' => $this->archivalAgencyId]);
        }
        $entity = $query->firstOrFail();

        $report = $this->ArchivingSystems->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Visualisation des données
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        $query = $this->ArchivingSystems->find()
            ->where(['ArchivingSystems.id' => $id])
            ->contain(['OrgEntities']);
        if (!$this->adminTech && $this->archivalAgencyId) {
            $query->where(['ArchivingSystems.org_entity_id' => $this->archivalAgencyId]);
        }
        $archivingSystem = $query->firstOrFail();
        $this->set('archivingSystem', $archivingSystem);
    }

    /**
     * Test une connexion basic
     * @param string|null $id
     * @return Response
     */
    public function testConnection(string $id = null)
    {
        if (!$id) {
            /** @var ArchivingSystem $entity */
            $entity = $this->ArchivingSystems->newEntity(
                $this->getRequest()->getData()
            );
        } else {
            $query = $this->ArchivingSystems->find()
                ->where(['ArchivingSystems.id' => $id]);
            if (!$this->adminTech && $this->archivalAgencyId) {
                $query->where(['ArchivingSystems.org_entity_id' => $this->archivalAgencyId]);
            }
            $entity = $query->firstOrFail();
        }

        try {
            $resp = $entity->whoami();
            if ($resp === null) {
                throw new GenericException(
                    __("Le Système d'Archivage Electronique ne répond pas ou est mal configuré")
                );
            }
            return $this->renderDataToJson($resp);
        } catch (Exception $e) {
            return $this->renderDataToJson($e->getMessage())
                ->withStatus($e->getCode() ?: 500);
        }
    }

    /**
     * Donne la liste des services d'archives
     * @return array
     * @see TimestampersTrait
     * @see VolumesTrait
     * @see LdapsTrait
     */
    protected function optionsSa(): array
    {
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        return $this->OrgEntities->find('list')
            ->innerJoinWith('TypeEntities')
            ->where(['TypeEntities.code' => 'SA'])
            ->order(['OrgEntities.name'])
            ->toArray();
    }

    /**
     * Vérifie la connexion à la base de donnée
     *
     * @return bool
     */
    protected function isConnected(): bool
    {
        if ($this->isConnected !== null) {
            return $this->isConnected;
        }
        try {
            $conn = ConnectionManager::get('default');
            if ($conn instanceof Connection) {
                $conn->query('select 1');
            }
            $this->isConnected = true;
        } catch (PDOException | MissingConnectionException) {
            $this->isConnected = false;
        }
        return $this->isConnected;
    }
}
