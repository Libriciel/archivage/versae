<?php

/**
 * Versae\Controller\CachableTrait
 */

namespace Versae\Controller;

use Cake\Http\Response;
use Psr\Http\Message\MessageInterface;

/**
 * Cache de fichier
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin AppController
 */
trait CachableTrait
{

    /**
     * Permet de lire un fichier hors du webroot et permet la mise en cache par
     * le navigateur
     * @param string $path
     * @return Response|MessageInterface
     */
    private function renderCachable(string $path)
    {
        $duration = 3600 * 24 * 365;
        $lastModFile = filemtime($path);
        $date = gmdate(DATE_RFC1123, $lastModFile);
        $response = $this->getResponse()
            ->withHeader('Content-Type', mime_content_type($path))
            ->withHeader('Cache-control', 'max-age=' . $duration)
            ->withHeader('Expires', gmdate(DATE_RFC1123, time() + $duration))
            ->withHeader('Last-Modified', $date)
            ->withHeader('Etag', $lastModFile);

        $useCache = (!empty($_SERVER['HTTP_IF_NONE_MATCH']) && $_SERVER['HTTP_IF_NONE_MATCH'] === $lastModFile)
            || (!empty($_SERVER['HTTP_IF_MODIFIED_SINCE']) && $_SERVER['HTTP_IF_MODIFIED_SINCE'] === $date);

        return $useCache
            ? $response->withStatus(304)
            : $response->withFile($path, ['download' => true, 'name' => basename($path)]);
    }
}
