<?php

/**
 * Versae\Controller\AdminsController
 */

namespace Versae\Controller;

use AsalaeCore\Controller\Component\TasksComponent;
use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Factory;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Form\AdminShellForm;
use AsalaeCore\Http\Response as CoreResponse;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\FormatError;
use AsalaeCore\Utility\Ip;
use AsalaeCore\Utility\Notify;
use Beanstalk\Model\Table\BeanstalkWorkersTable;
use Cake\Core\Configure;
use Cake\Database\Connection;
use Cake\Database\Exception\MissingConnectionException;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\Event\EventInterface;
use Cake\Http\Client;
use Cake\Http\Client\Message;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response as CakeResponse;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use DOMDocument;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use PDOException;
use Symfony\Component\Filesystem\Exception\IOException;
use Versae\Controller\Component\EmailsComponent;
use Versae\Form\InterruptForm;
use Versae\Model\Entity\OrgEntity;
use Versae\Model\Table\AcosTable;
use Versae\Model\Table\ArchivingSystemsTable;
use Versae\Model\Table\ArosAcosTable;
use Versae\Model\Table\ArosTable;
use Versae\Model\Table\BeanstalkJobsTable;
use Versae\Model\Table\ConfigurationsTable;
use Versae\Model\Table\CronsTable;
use Versae\Model\Table\FileuploadsTable;
use Versae\Model\Table\LdapsTable;
use Versae\Model\Table\OrgEntitiesTable;
use Versae\Model\Table\RolesTable;
use Versae\Model\Table\SessionsTable;
use Versae\Model\Table\TypeEntitiesTable;
use Versae\Model\Table\UsersTable;
use Versae\Utility\Check;
use ZMQSocketException;

/**
 * Administration technique de versae
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AcosTable Acos
 * @property ArchivingSystemsTable ArchivingSystems
 * @property ArosTable Aros
 * @property BeanstalkJobsTable BeanstalkJobs
 * @property BeanstalkWorkersTable BeanstalkWorkers
 * @property ConfigurationsTable Configurations
 * @property CronsTable Crons
 * @property EmailsComponent $Emails
 * @property FileuploadsTable Fileuploads
 * @property LdapsTable Ldaps
 * @property OrgEntitiesTable OrgEntities
 * @property ArosAcosTable Permissions
 * @property RolesTable Roles
 * @property SessionsTable Sessions
 * @property Table Phinxlog
 * @property TasksComponent $Tasks
 * @property TypeEntitiesTable TypeEntities
 * @property UsersTable Users
 */
class AdminsController extends AppController
{
    use Admins\ArchivalAgencyTrait;
    use Admins\ConfigurationsTrait;
    use Admins\CronsTrait;
    use Admins\LdapsTrait;
    use Admins\TasksTrait;
    use RenderDataTrait;

    /**
     * @var string clé de cryptage/décryptage (hashé en sha256)
     */
    public const DECRYPT_KEY = 'timestamper';

    public const TEST_FILENAME = 'test_file.txt';

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const TABLE_EDIT_USER_SA = 'users-sa-entity-table';
    public const TABLE_SERVICE_ARCHIVE = 'entity-service-archive-table';
    public const TABLE_INDEX_SPACES = 'index-spaces-table';
    public const TABLE_INDEX_CRONS = 'index-crons-table';
    public const TABLE_INDEX_SESSIONS = 'index-sessions-table';
    public const TABLE_INDEX_ARCHIVING_SYSTEMS = 'index-archiving_systems-table';

    /**
     * @var null|bool connecté à la bdd (défini par isConnected())
     */
    private $isConnected = null;

    /**
     * Neutralise le beforeRender pour permettre de se connecter a ce controller
     * sans base de donnée
     *
     * @param EventInterface $event An Event instance
     * @return CoreResponse|null
     * @throws Exception
     * @link https://book.cakephp.org/4/en/controllers.html#request-life-cycle-callbacks
     */
    public function beforeRender(EventInterface $event): ?CoreResponse
    {
        if ($event->getData('do_not_override')) {
            return parent::beforeRender($event);
        }
        $this->set('user_id');
        $this->set('notifications', json_encode([]));
        try {
            return parent::beforeRender($event);
        } catch (Exception) {
            return null;
        }
    }

    /**
     * Contrôle que l'utilisateur est connecté en administrateur technique
     *
     * @param EventInterface $event An Event instance
     * @return CakeResponse|null
     * @link https://book.cakephp.org/4/en/controllers.html#request-life-cycle-callbacks
     */
    public function beforeFilter(EventInterface $event)
    {
        if ($event->getData('do_not_override')) {
            return parent::beforeFilter($event);
        }
        $allowed = ['login', 'logout', 'ajaxLogin', 'probe', 'ping'];
        $this->Authentication->addUnauthenticatedActions($allowed);

        $adminTech = $this->getRequest()->getSession()->read('Admin.tech');

        if (
            Configure::read('Ip.enable_whitelist', false)
            && !Ip::inWhitelist($this->getRequest()->clientIp())
        ) {
            $this->Flash->error(
                __("Vous n'avez pas accès à cette partie de l'application")
            );
            return $this->redirect('/');
        }
        if (
            !$this->Authentication->getResult()
            && !in_array($this->getRequest()->getParam('action'), $allowed)
            && !$this->getRequest()->is('ajax')
        ) {
            return $this->redirect('/admins/login');
        }

        // pour le session timeout -> loginAjax
        $this->set('admin', $adminTech);
    }

    /**
     * Page principale de l'administration technique
     */
    public function index()
    {
        $this->viewBuilder()->setLayout('admin_tech');
        if (empty($this->getAppLocalConfigFileUri())) {
            $this->Flash->error(
                __("Le fichier de configuration local n'a pas été trouvé")
            );
        }
        /** @var Check $Check */
        $Check = Factory\Utility::get(Check::class);
        $installation = !$Check->needInstall();
        $database = $Check->database();
        $beanstalkd = $Check->beanstalkd();
        $ratchet = $Check->ratchet();
        $clamav = $Check->clamav();
        $php = $Check->phpOk();
        $writable = $Check->writableOk();
        $system = $installation && $database && $ratchet && $clamav
            && $beanstalkd && $php && $writable;
        $this->set('system', $system);
        if ($this->isConnected()) {
            try {
                $this->Phinxlog = $this->fetchTable('Phinxlog');
                $this->Phinxlog->find()->first();
                $this->OrgEntities = $this->fetchTable('OrgEntities');
                $se = $this->OrgEntities->find()
                    ->contain(['TypeEntities'])
                    ->where(['code' => 'SE'])
                    ->first();
                if (!$se) {
                    $this->Flash->error(
                        __("L'application n'a pas été installée")
                    );
                }
            } catch (Exception) {
                $this->Flash->error(
                    __("La base de données n'a pas été initialisée")
                );
            }
        } else {
            $this->Flash->error(__("Base de données down"));
        }
    }

    /**
     * Donne des infos sur l'état du system
     */
    public function probe()
    {
        $this->viewBuilder()->setLayout('ajax');
        if (!$this->getRequest()->getSession()->read('Admin.tech')) {
            return $this->getResponse()->withStringBody('');
        }

        // memory usage
        exec('cat /proc/meminfo', $output);
        $parsed = [];
        foreach ($output as $str) {
            if (strpos($str, ':')) {
                [$key, $value] = explode(':', $str, 2);
                $parsed[$key] = (int)$value;
            }
        }
        $probe = [
            'memory' => [
                'total' => $parsed['MemTotal'] ?? 0,
                'available' => $parsed['MemAvailable'] ?? 0,
            ],
            'swap' => [
                'total' => $parsed['SwapTotal'] ?? 0,
                'available' => $parsed['SwapFree'] ?? 0,
            ],
            'loadavg' => array_map(fn($v) => round($v, 3), sys_getloadavg() ?: []),
        ];
        $this->set('probe', $probe);
    }

    /**
     * Connection en administrateur technique
     *
     * @return CakeResponse|null
     */
    public function login()
    {
        $this->viewBuilder()->setLayout('login');
        $result = $this->Authentication->getResult();
        if ($result->isValid()) {
            $redirect = $this->redirect(
                [
                    'controller' => 'Admins',
                    'action' => 'index',
                ]
            );
            return $this->redirect($redirect);
        }
        if ($this->request->is('post') && !$result->isValid()) {
            $this->Flash->error(__("Login ou mot de passe invalide"));
        }

        $appLocalConfigFileUri = $this->getAppLocalConfigFileUri();
        if (empty($appLocalConfigFileUri)) {
            $this->Flash->error(
                __("Le fichier de configuration local n'a pas été trouvé")
            );
        }
    }

    /**
     * Permet la connexion par ajax
     */
    public function ajaxLogin()
    {
        $result = $this->Authentication->getResult();
        $this->Users = $this->fetchTable('Users');
        $user = $this->Users->newEntity([], ['validate' => false]);
        $this->loadComponent('AsalaeCore.Modal');
        if ($result->isValid()) {
            $this->Modal->success();
        } elseif ($this->getRequest()->is('post')) {
            $this->Modal->fail();
            $user->setErrors(['name' => __("Login ou mot de passe invalide")]);
        }
        $this->set('entity', $user);
        $this->viewBuilder()->setTemplatePath('Users');
    }

    /**
     * Déconnection de l'administration technique
     *
     * @return CakeResponse|null
     */
    public function logout(): ?CakeResponse
    {
        $result = $this->Authentication->getResult();
        if ($result->isValid()) {
            $this->Authentication->logout();
        }
        $this->getRequest()->getSession()->destroy();
        return $this->redirect(['controller' => 'users', 'action' => 'login']);
    }

    /**
     * Vérification asynchrone des TreeBehavior
     * @throws Exception
     */
    public function ajaxCheckTreeSanity()
    {
        /** @var Check $Check */
        $Check = Factory\Utility::get(Check::class);
        $this->set('tree_ok', $Check->treeSanityOk());
    }

    /**
     * Vérification du système
     */
    public function ajaxCheck()
    {
    }

    /**
     * Visualisation des logs de l'application
     */
    public function ajaxLogs()
    {
        $logs = [];
        $logPath = Configure::read('App.paths.logs', LOGS) . '*.log';
        foreach (glob($logPath) as $filename) {
            $output = null;
            exec('tail -n500 "' . $filename . '"', $output);
            $logs[basename($filename)] = implode("\n", $output);
        }
        $this->set('logs', $logs);
    }

    /**
     * Donne le fichier de log
     * @param string $filename
     * @return CoreResponse
     */
    public function downloadLog(string $filename)
    {
        $logPath = Configure::read('App.paths.logs', LOGS) . $filename;
        return $this->getCoreResponse()
            ->withFileStream($logPath);
    }

    /**
     * Envoi une notification à tous les utilisateurs
     */
    public function notifyAll()
    {
        if (!$this->isConnected()) {
            return $this->getResponse()->withStringBody(
                __(
                    "Le serveur de base de données doit fonctionner pour accéder à cette fonctionnalité"
                )
            );
        }

        if ($this->getRequest()->is('post')) {
            $title = '<h4>' . __("Message de l'administrateur") . '</h4>';
            $notificationsSubmited = 0;
            /** @var Notify $Notify */
            $Notify = Factory\Utility::get('Notify');
            $innerBody = $this->getNotifyBody();
            $msg = $title . $innerBody;
            $class = h($this->getRequest()->getData('color'));

            if ($this->getRequest()->getData('activeusers')) {
                /** @var EntityInterface $session */
                $usersTokens = [];
                $this->Sessions = $this->fetchTable('Sessions');
                $query = $this->Sessions->find()
                    ->select(['user_id', 'token'])
                    ->where(['token IS NOT' => null]);
                foreach ($query as $session) {
                    $usersTokens[$session->get('user_id')][] = $session->get('token');
                }
                foreach ($usersTokens as $user_id => $tokens) {
                    $notificationsSubmited++;
                    $Notify->send(
                        $user_id,
                        $tokens,
                        $msg,
                        $class
                    );
                }
            } else {
                $this->Users = $this->fetchTable('Users');
                $users = $this->Users->find()->where(['active' => true])->all();
                /** @var EntityInterface $user */
                foreach ($users as $user) {
                    $notificationsSubmited++;
                    $Notify->send(
                        $user->get('id'),
                        [],
                        $msg,
                        $class
                    );
                }
            }
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->success();
            return $this->renderDataToJson(['count' => $notificationsSubmited]);
        }
    }

    /**
     * Etat des disques
     */
    public function ajaxCheckDisk()
    {
        exec('df -h | grep -v tmpfs | grep -v /dev/loop', $disks);
        $this->set('disks', $disks);
    }

    /**
     * Permet de définir l'état de debug par ajax
     * @return CakeResponse
     * @throws Exception
     */
    public function ajaxToggleDebug(): CakeResponse
    {
        $appLocalConfigFileUri = $this->getAppLocalConfigFileUri();
        if (!is_writable($appLocalConfigFileUri)) {
            throw new ForbiddenException(
                sprintf("Can't write on %s", $appLocalConfigFileUri)
            );
        }
        if (empty($appLocalConfigFileUri)) {
            $this->Flash->error(
                __("Le fichier de configuration local n'a pas été trouvé")
            );
        }
        $confJson = file_get_contents($appLocalConfigFileUri);
        $conf = json_decode($confJson, true);
        $conf['debug'] = $this->getRequest()->getData('state') === 'true';
        ksort($conf);
        /** @var Filesystem $Filesystem */
        $Filesystem = Factory\Utility::get('Filesystem');
        try {
            $Filesystem->begin('toggle_debug');
            $Filesystem->setSafemode(true);
            $Filesystem->dumpFile(
                $appLocalConfigFileUri,
                json_encode($conf, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES)
            );
            $Filesystem->commit('toggle_debug');
            return $this->renderData(['success' => true]);
        } catch (IOException $e) {
            $Filesystem->rollback('toggle_debug');
            return $this->renderData(
                ['success' => false, 'error' => $e->getMessage()]
            );
        }
    }

    /**
     * Permet de modifier la configuration par editeur de texte
     */
    public function ajaxInterrupt()
    {
        $form = new InterruptForm();
        if ($this->getRequest()->is('post')) {
            $this->loadComponent('AsalaeCore.Modal');
            $success = $form->execute($this->getRequest()->getData());
            if ($success) {
                $this->Modal->success();
            } else {
                $this->Modal->fail();
                FormatError::logFormErrors($form);
            }
        }
        $this->set('form', $form);
    }

    /**
     * Edition de l'entité de type SE
     * Permet d'ajouter / retirer des Services d'archives / des utilisateurs
     * @return CakeResponse|null
     * @throws Exception
     */
    public function editServiceExploitation()
    {
        if (!$this->isConnected()) {
            return $this->getResponse()->withStringBody(
                __(
                    "Le serveur de base de données doit fonctionner pour accéder à cette fonctionnalité"
                )
            );
        }
        $this->getRequest()->allowMethod('ajax');
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $this->Roles = $this->fetchTable('Roles');
        /** @var OrgEntity $entity */
        $entity = $this->OrgEntities->find()
            ->where(['TypeEntities.code' => 'SE'])
            ->contain(
                [
                    'Users' => ['Roles'],
                    'TypeEntities',
                    'Children',
                    'RgpdInfos',
                ]
            )
            ->firstOrFail();
        if ($this->getRequest()->is('put')) {
            $data = [
                'name' => $this->getRequest()->getData('name'),
                'identifier' => $this->getRequest()->getData('identifier'),
                'rgpd_info' => $this->getRequest()->getData('rgpd_info'),
            ];
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->save($this->OrgEntities, $entity, $data);
            $this->set('entity', $entity);
            return null;
        }

        $this->set('entity', $entity);
    }

    /**
     * Effectue un tail -f sur un fichier log
     * @param string $logfile
     * @param string $timeout
     * @return CoreResponse
     * @throws Exception
     */
    public function tail(string $logfile, $timeout = '10'): CoreResponse
    {
        $filename = Configure::read('App.paths.logs', LOGS) . $logfile;
        if (!is_readable($filename)) {
            throw new NotFoundException(
                __("Impossible de lire le fichier {0}", $filename)
            );
        }
        /** @var CoreResponse $response */
        $response = $this->getResponse()->withType('text');

        setlocale(LC_ALL, 'en_US.UTF-8');
        $command = sprintf(
            "timeout %.1F tail -f -n500 %s 2>&1",
            (float)$timeout,
            Exec::escapeshellarg($filename)
        );
        setlocale(LC_ALL, null);
        $handle = popen($command, 'r');
        while (!feof($handle)) {
            $response->withStringBody(fgets($handle))->partialEmit();
        }
        pclose($handle);

        return $response;
    }

    /**
     * Affiche un phpinfo()
     * @return CakeResponse
     */
    public function phpInfo(): CakeResponse
    {
        ob_start();
        phpinfo();
        $content = ob_get_contents();
        ob_end_clean();
        return $this->getResponse()->withStringBody($content);
    }

    /**
     * Liste les utilisateurs connectés
     * @return CakeResponse
     */
    public function indexSessions()
    {
        if (!$this->isConnected()) {
            return $this->getResponse()->withStringBody(
                __(
                    "Le serveur de base de données doit fonctionner pour accéder à cette fonctionnalité"
                )
            );
        }
        $this->Sessions = $this->fetchTable('Sessions');

        $this->set('tableIdSessions', self::TABLE_INDEX_SESSIONS);

        $sessions = $this->Sessions->find()
            ->contain(
                [
                    'Users' => [
                        'OrgEntities' => [
                            'ArchivalAgencies',
                        ]
                    ]
                ]
            )
            ->where(['Sessions.user_id IS NOT' => null])
            ->order(['Users.username' => 'asc', 'Sessions.modified' => 'desc'])
            ->limit(100) // garde fou - envisager une pagination si besoin
            ->all()
            ->toArray();
        $this->set('sessions', $sessions);
    }

    /**
     * Déconnecte un utilisateur
     * @param string $session_id
     * @return CakeResponse
     */
    public function deleteSession(string $session_id): CakeResponse
    {
        $this->getRequest()->allowMethod('delete');
        $this->Sessions = $this->fetchTable('Sessions');
        $entity = $this->Sessions->get($session_id);

        $report = $this->Sessions->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        return $this->renderDataToJson(['report' => $report]);
    }

    /**
     * Envoi une notification à un utilisateur spécifique
     * @param string $session_id
     * @return CakeResponse
     * @throws ZMQSocketException
     */
    public function notifyUser(string $session_id)
    {
        if (!$this->isConnected()) {
            return $this->getResponse()->withStringBody(
                __(
                    "Le serveur de base de données doit fonctionner pour accéder à cette fonctionnalité"
                )
            );
        }
        $this->Sessions = $this->fetchTable('Sessions');
        /** @var EntityInterface $entity */
        $entity = $this->Sessions->find()
            ->select(['user_id', 'token', 'Users.username'])
            ->contain(['Users'])
            ->where(['Sessions.id' => $session_id])
            ->firstOrFail();

        if ($this->getRequest()->is('post')) {
            $innerBody = $this->getNotifyBody();
            $title = '<h4>' . __("Message privé de l'administrateur") . '</h4>';
            $msg = $title . $innerBody;
            /** @var Notify $Notify */
            $Notify = Factory\Utility::get('Notify');

            $class = h($this->getRequest()->getData('color'));
            $Notify->send(
                $entity->get('user_id'),
                [$entity->get('token')],
                $msg,
                $class
            );
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->success();
            return $this->renderDataToJson(['success' => true]);
        }
        $this->set('entity', $entity);
    }

    /**
     * Ping
     * @return CakeResponse
     */
    public function ping(): CakeResponse
    {
        return $this->getResponse()->withStringBody('pong');
    }

    /**
     * Donne les roles disponibles pour un webservice
     * @param string $org_entity_id
     * @return CakeResponse
     */
    public function getWsRolesOptions(string $org_entity_id)
    {
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $this->Roles = $this->fetchTable('Roles');
        $sa = $this->OrgEntities->getEntitySA($org_entity_id);
        $roles = $this->OrgEntities->findByRoles($sa)
            ->where(
                [
                    'OrgEntities.id' => $org_entity_id,
                    'Roles.agent_type' => 'software',
                ]
            )
            ->all()
            ->toArray();
        $roles = array_reduce(
            $roles,
            function ($res, OrgEntity $v) {
                $res[$v->get('Roles')['id']] = $v->get('Roles')['name'];
                return $res;
            }
        );
        return $this->renderDataToJson($roles);
    }

    /**
     * Donne la liste des services d'archives
     * @return ResultSetInterface
     * @see TimestampersTrait
     * @see VolumesTrait
     * @see LdapsTrait
     */
    protected function optionsSa(): ResultSetInterface
    {
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        return $this->OrgEntities->find('list')
            ->innerJoinWith('TypeEntities')
            ->where(['TypeEntities.code' => 'SA'])
            ->order(['OrgEntities.name'])
            ->all();
    }

    /**
     * Vérifie la connexion à la base de donnée
     *
     * @return bool
     */
    protected function isConnected(): bool
    {
        if ($this->isConnected !== null) {
            return $this->isConnected;
        }
        try {
            $conn = ConnectionManager::get('default');
            if ($conn instanceof Connection) {
                $conn->query('select 1');
            }
            $this->isConnected = true;
        } catch (PDOException | MissingConnectionException) {
            $this->isConnected = false;
        }
        return $this->isConnected;
    }

    /**
     * Retourne l'uri du fichier de configuration local
     */
    protected function getAppLocalConfigFileUri(): string
    {
        $pathToLocalConfig = Configure::read('App.paths.path_to_local_config');
        if (is_readable($pathToLocalConfig)) {
            $appLocalConfigFileUri = include $pathToLocalConfig;
        } elseif (is_readable('/data/config/app_local.json')) {
            $appLocalConfigFileUri = '/data/config/app_local.json';
        } elseif (is_readable(CONFIG . 'app_local.json')) {
            $appLocalConfigFileUri = CONFIG . 'app_local.json';
        } else {
            $appLocalConfigFileUri = '';
        }
        return $appLocalConfigFileUri;
    }

    /**
     * Récupère le formulaire sous forme utilisable par les notifications
     * @return string
     */
    protected function getNotifyBody(): string
    {
        // filtre les script et corrige les erreurs html
        $dom = new DOMDocument();
        @$dom->loadHTML($this->getRequest()->getData('msg'));
        if ($dom->getElementsByTagName('script')->count() > 0) {
            throw new BadRequestException('script in notification');
        }
        $body = $dom->getElementsByTagName('body')->item(0);
        $innerBody = '';
        foreach ($body->childNodes as $child) {
            $innerBody .= $dom->saveHTML($child);
        }
        return $innerBody;
    }

    /**
     * Retourne le contenu json d'un ping sur le service Seda Generator
     * @return CakeResponse
     */
    public function sedageneratorPing(): CakeResponse
    {
        $client = new Client(
            [
                'ssl_verify_peer' => $this->getRequest()->getData('seda_ssl_verify_peer'),
                'ssl_verify_peer_name' => $this->getRequest()->getData('seda_ssl_verify_peer_name'),
                'ssl_verify_depth' => $this->getRequest()->getData('seda_ssl_verify_depth'),
                'ssl_verify_host' => $this->getRequest()->getData('seda_ssl_verify_host'),
                'ssl_cafile' => $this->getRequest()->getData('seda_ssl_cafile'),
            ]
        );
        try {
            $response = $client->get(Configure::read('SedaGenerator.url') . '/ping');
        } catch (Exception $e) {
            return $this->renderDataToJson(['error' => $e->getMessage()]);
        }

        return $this->renderDataToJson(json_decode($response->getStringBody()));
    }

    /**
     * Edition de l'utilisateur admin (soit-même)
     * @throws Exception
     */
    public function editAdmin()
    {
        $request = $this->getRequest();
        $path = Configure::read(
            'App.paths.administrators_json',
            Configure::read('App.paths.data') . DS . 'administrateurs.json'
        );
        if (!is_writeable(dirname($path))) {
            throw new Exception(
                __(
                    "Le repertoire de configuration {0} n'est pas accessible en écriture",
                    $path
                )
            );
        }
        $admins = json_decode(file_get_contents($path));
        $identity = $this->getRequest()->getAttribute('identity');
        $user = $identity ? $identity->getOriginalData() : null;
        if (Hash::get($user, 'id')) {
            throw new ForbiddenException();
        }
        $username = Hash::get($user, 'username');
        foreach ($admins as $admin) {
            if ($admin->username === $username) {
                break;
            }
        }
        $form = new AdminShellForm();
        $user = new Entity((array)($admin ?? []));
        $this->set('user', $user);
        $data = $user->toArray();
        unset($data['password'], $data['confirm-password']);

        if ($request->is('post')) {
            $data = [
                'action' => 'edit',
                'username' => $username,
            ] + $request->getData();
            $this->loadComponent('AsalaeCore.Modal');
            if ($form->execute($data)) {
                $this->Modal->success();
                return $this->renderDataToJson($data);
            } else {
                $this->Modal->fail();
            }
        }

        $form->setData($data);
        $this->set('form', $form);
    }

    /**
     * Permet de vérifier la connexion par websocket
     * @param string $uuid
     * @return CoreResponse|CakeResponse
     * @throws ZMQSocketException
     */
    public function ajaxCheckWebsocket(string $uuid)
    {
        $Notify = Utility::get('Notify');
        $Notify->emit('ping_' . $uuid, 'ok');
        return $this->renderDataToJson('ok');
    }

    /**
     * Permet de vérifier la connexion aux service d'archives
     * @return CoreResponse|CakeResponse
     * @throws Exception
     */
    public function ajaxCheckArchivingSystem()
    {
        /** @var Check $Check */
        $Check = Factory\Utility::get(Check::class);
        $archivingSystems = $Check->archivingSystems();
        $errors = array_filter($archivingSystems, fn ($v) => $v['success'] === false);
        if (!$errors) {
            return $this->renderDataToJson('ok');
        }
        return $this->getResponse()
            ->withStatus(Message::STATUS_ACCEPTED)
            ->withType('json')
            ->withStringBody(json_encode(['failed' => $errors]));
    }
}
