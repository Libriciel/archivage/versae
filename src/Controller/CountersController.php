<?php

/**
 * Versae\Controller\CountersController
 */

namespace Versae\Controller;

use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\RenderDataTrait;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Response as CakeResponse;
use Exception;
use Versae\Model\Entity\Sequence;
use Versae\Model\Table\CountersTable;
use Versae\Model\Table\OrgEntitiesTable;
use Versae\Model\Table\SequencesTable;

/**
 * Accords de versement
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property CountersTable $Counters
 * @property SequencesTable $Sequences
 * @property OrgEntitiesTable $OrgEntities
 */
class CountersController extends AppController
{
    use RenderDataTrait;

    /**
     * Options de pagination par défaut
     *
     * @var array
     */
    public $paginate = [
        'limit' => 100,
        'order' => [
            'Counters.identifier' => 'asc'
        ]
    ];

    /**
     * @var string id (HTML) du tableau de résultat
     */
    public const TABLE_INDEX = 'counters-table';

    /**
     * Liste des Compteurs
     *
     */
    public function index()
    {
        $this->loadComponent('AsalaeCore.Index');
        $this->Index->init();

        $query = $this->Counters->find()
            ->where(['Counters.org_entity_id' => $this->archivalAgencyId])
            ->order(['Counters.identifier' => 'asc']);
        $this->Index->setQuery($query)
            ->filter('name', IndexComponent::FILTER_ILIKE)
            ->filter('identifier', IndexComponent::FILTER_ILIKE)
            ->filter('active', IndexComponent::FILTER_IS)
            ->filter('favoris', IndexComponent::FILTER_FAVORITES);

        $data = $this->paginate($query)->toArray();
        $this->set('data', $data);
    }

    /**
     * Action d'initialisation des compteurs
     * @throws Exception
     */
    public function initCounters(): CakeResponse
    {
        $id = $this->archivalAgencyId;
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $sa = $this->OrgEntities->find()
            ->where(['OrgEntities.id' => $id])
            ->contain(['Counters'])
            ->firstOrFail();
        if ($sa->get('counters')) {
            throw new BadRequestException();
        }
        $this->OrgEntities->initCounters($id);
        return $this->renderDataToJson(['success' => true]);
    }

    /**
     * Edition d'un compteur
     * @param string $id
     * @throws Exception
     */
    public function edit(string $id)
    {
        $this->Counters = $this->fetchTable('Counters');
        $this->Sequences = $this->fetchTable('Sequences');
        $entity = $this->Counters->find()
            ->where(
                [
                    'Counters.id' => $id,
                    'Counters.org_entity_id' => $this->archivalAgencyId
                ]
            )
            ->firstOrFail();
        $sequences = $this->Sequences->find()
            ->select(['Sequences.id', 'Sequences.name', 'Sequences.value'])
            ->where(['Sequences.org_entity_id' => $this->archivalAgencyId])
            ->order(['Sequences.name' => 'asc']);
        $sequencesOptions = [];
        $sequencesValues = [];
        /** @var Sequence $sequence */
        foreach ($sequences as $sequence) {
            $sequencesOptions[$sequence->get('id')] = $sequence->get('name') . ' (' . $sequence->get('value') . ')';
            $sequencesValues[$sequence->get('id')] = $sequence->get('value');
        }
        $this->set('sequencesOptions', $sequencesOptions);
        $this->set('sequencesValues', $sequencesValues);
        $this->set('entity', $entity);
        if ($this->getRequest()->is('put')) {
            $data = [
                'name' => $this->getRequest()->getData('name'),
                'description' => $this->getRequest()->getData('description'),
                'definition_mask' => $this->getRequest()->getData('definition_mask'),
                'sequence_reset_mask' => $this->getRequest()->getData('sequence_reset_mask'),
            ];
            $this->loadComponent('AsalaeCore.Modal');
            $this->Modal->save($this->Counters, $entity, $data);
        }
    }

    /**
     * Visualisation d'un compteur
     * @param string $id
     * @throws Exception
     */
    public function view(string $id)
    {
        $this->Counters = $this->fetchTable('Counters');
        $entity = $this->Counters->find()
            ->where(
                [
                    'Counters.id' => $id,
                    'Counters.org_entity_id' => $this->archivalAgencyId
                ]
            )
            ->contain(['Sequences'])
            ->firstOrFail();
        $this->set('entity', $entity);
    }
}
