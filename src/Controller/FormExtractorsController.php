<?php

/**
 * Versae\Controller\FormExtractorsController
 */

namespace Versae\Controller;

use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\FormatError;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Http\Client;
use Cake\Http\Response;
use Cake\Utility\Hash;
use DOMDocument;
use DOMXPath;
use Exception;
use Flow\JSONPath\JSONPath;
use Flow\JSONPath\JSONPathException;
use Libriciel\Filesystem\Utility\Filesystem;
use Versae\Model\Table\FileuploadsTable;
use Versae\Model\Table\FormInputsTable;
use Versae\Model\Table\FormsTable;
use Versae\Model\Table\FormExtractorsTable;
use Versae\Model\Table\FormTransferHeadersTable;
use Versae\Utility\Csv;

/**
 * Variables de formulaire
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property FileuploadsTable Fileuploads
 * @property FormExtractorsTable FormExtractors
 * @property FormInputsTable FormInputs
 * @property FormTransferHeadersTable FormTransferHeaders
 * @property FormsTable Forms
 */
class FormExtractorsController extends AppController
{
    use RenderDataTrait;

    /**
     * Ajout
     * @param string $form_id
     * @return \AsalaeCore\Http\Response|Response|void
     * @throws Exception
     */
    public function add(string $form_id)
    {
        $request = $this->getRequest();
        $this->Forms = $this->fetchTable('Forms');
        $formEntity = $this->Forms->find()
            ->where(
                [
                    'Forms.id' => $form_id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();

        $entity = $this->FormExtractors->newEmptyEntity();

        if ($request->is('post')) {
            $this->FormExtractors->patchEntity(
                $entity,
                [
                    'form_id' => $form_id,
                ]
                + $request->getData()
            );
            $entity->setDirty('app_meta');
            $this->loadComponent('AsalaeCore.Modal');

            if ($this->FormExtractors->save($entity)) {
                $this->Modal->success();
                return $this->renderDataToJson(
                    $this->getEditData($entity)
                );
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('formEntity', $formEntity);
        $this->set('entity', $entity);

        // options
        $this->setAddEditOptions($form_id);
    }

    /**
     * Edition d'une variable
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function edit(string $id)
    {
        $request = $this->getRequest();
        $entity = $this->FormExtractors->find()
            ->innerJoinWith('Forms')
            ->where(
                [
                    'FormExtractors.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->contain(['Forms'])
            ->firstOrFail();
        $formEntity = $entity->get('form');
        $form_id = $entity->get('form_id');

        if ($request->is('put')) {
            $this->FormExtractors->patchEntity(
                $entity,
                [
                    'form_id' => $form_id,
                ]
                + $request->getData()
            );
            $entity->setDirty('app_meta');
            $this->loadComponent('AsalaeCore.Modal');

            if ($this->FormExtractors->save($entity)) {
                $this->Modal->success();
                $this->Forms = $this->fetchTable('Forms');
                return $this->renderDataToJson(
                    $this->getEditData($entity)
                );
            } else {
                $this->Modal->fail();
                FormatError::logEntityErrors($entity);
            }
        }

        $this->set('formEntity', $formEntity);
        $this->set('entity', $entity);

        // options
        $this->setAddEditOptions($form_id);
    }

    /**
     * Donne une liste de xpath à partir d'un fichier
     * @param string $fileuploads_id
     * @throws Exception
     */
    public function extractXpaths(string $fileuploads_id)
    {
        $this->Fileuploads = $this->fetchTable('Fileuploads');
        $file = $this->Fileuploads->find()
            ->where(['id' => $fileuploads_id, 'user_id' => $this->userId])
            ->firstOrFail();
        $dom = new DOMDocument();
        $dom->load($file->get('path'));
        $this->set('dom', $dom);
    }

    /**
     * Donne une liste de json path à partir d'un fichier
     * @param string $fileuploads_id
     * @throws Exception
     */
    public function extractJsonPath(string $fileuploads_id)
    {
        $this->Fileuploads = $this->fetchTable('Fileuploads');
        $file = $this->Fileuploads->find()
            ->where(['id' => $fileuploads_id, 'user_id' => $this->userId])
            ->firstOrFail();
        $json = json_decode(file_get_contents($file->get('path')), true);
        $this->set('json', $json);
    }

    /**
     * Donne une liste de csv path à partir d'un fichier
     * @param string $fileuploads_id
     * @throws Exception
     */
    public function extractCsvPath(string $fileuploads_id)
    {
        $this->Fileuploads = $this->fetchTable('Fileuploads');
        $file = $this->Fileuploads->find()
            ->where(['id' => $fileuploads_id, 'user_id' => $this->userId])
            ->firstOrFail();
        $this->set('file', $file->get('path'));
    }

    /**
     * Donne le contenu dans le chemin indiqué
     * @param string $fileuploads_id
     * @return Response
     */
    public function testFilePath(string $fileuploads_id): Response
    {
        $this->Fileuploads = $this->fetchTable('Fileuploads');
        $file = $this->Fileuploads->find()
            ->where(['id' => $fileuploads_id, 'user_id' => $this->userId])
            ->firstOrFail();
        $ext = strtolower(pathinfo($file->get('name'), PATHINFO_EXTENSION));
        $request = $this->getRequest();
        switch ($ext) {
            case 'xml':
                $response = $this->testFilePathXml($file);
                break;
            case 'json':
                $response = $this->testFilePathJson($file);
                break;
            case 'csv':
                $csv = new Csv($file->get('path'));
                $response = implode("\n", (array)$csv->get($request->getQuery('path')));
                break;
            default:
                $response = __("Le type de fichier n'est pas pris en charge");
        }
        return $this->getResponse()
            ->withType('text/plain')
            ->withStringBody(
                __("Message obtenu dans le fichier:")
                . "\n\n"
                . $response
            );
    }

    /**
     * Suppression d'une variable
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete(string $id): Response
    {
        $entity = $this->FormExtractors->find()
            ->innerJoinWith('Forms')
            ->where(
                [
                    'FormExtractors.id' => $id,
                    'Forms.org_entity_id' => $this->archivalAgencyId,
                ]
            )
            ->firstOrFail();

        $report = $this->FormExtractors->delete($entity)
            ? 'done'
            : 'Erreur lors de la suppression';

        $this->Forms = $this->fetchTable('Forms');
        return $this->renderDataToJson(
            [
                'report' => $report,
                'edit_data' => $this->getEditData($entity)
            ]
        );
    }

    /**
     * Donne les données pour rafraichir une vue Forms/edit
     * @param EntityInterface $entity
     * @return array
     */
    private function getEditData(EntityInterface $entity): array
    {
        $this->Forms = $this->fetchTable('Forms');
        $this->FormTransferHeaders = $this->fetchTable('FormTransferHeaders');
        $form = $this->Forms->findEditableEntity()
            ->where(['Forms.id' => $entity->get('form_id')])
            ->firstOrFail();
        $data = $form->toArray();
        $data['form_transfer_headers'] = $this->FormTransferHeaders->formHeadersData($form);
        return $data;
    }

    /**
     * Donne les options de select
     * @param string $form_id
     * @return void
     * @throws Exception
     */
    private function setAddEditOptions(string $form_id)
    {
        $this->set('types', $this->FormExtractors->options('type'));
        $this->set('data_formats', $this->FormExtractors->options('data_format'));

        $this->FormInputs = $this->fetchTable('FormInputs');
        $formInputs = [];
        $query = $this->FormInputs->find()
            ->where(
                [
                    'FormInputs.form_id' => $form_id,
                    'FormInputs.type IN' => [FormInputsTable::TYPE_FILE, FormInputsTable::TYPE_ARCHIVE_FILE],
                ]
            )
            ->contain(['FormFieldsets'])
            ->order(['FormFieldsets.ord', 'FormInputs.ord']);
        /** @var EntityInterface $formInput */
        foreach ($query as $formInput) {
            $fieldsetName = Hash::get($formInput, 'form_fieldset.legend')
                ?: __("Section sans titre #{0}", Hash::get($formInput, 'form_fieldset.ord') + 1);
            if (Hash::get($formInput, 'form_fieldset.repeatable')) {
                $fieldsetName = hex2bin('ef86b3') . $fieldsetName;
            }
            if (!isset($formInputs[$fieldsetName])) {
                $formInputs[$fieldsetName] = [];
            }
            $formInputs[$fieldsetName][] = [
                'text' => $formInput->get('name') . ' - ' . $formInput->get('label'),
                'value' => $formInput->get('id'),
                'class' => $formInput->get('type'),
                'data-multiple' => $formInput->get('multiple'),
                'data-section-repeatable' => Hash::get($formInput, 'form_fieldset.repeatable'),
            ];
        }
        $this->set('formInputs', $formInputs);
    }

    /**
     * Télécharge le fichier du webservice
     * @param bool $debug
     * @return \AsalaeCore\Http\Response|Response
     * @throws Exception
     */
    public function testWebserviceFile($debug = null)
    {
        $data = $this->getRequest()->getData();
        $data['data_format'] = $data['data_format'] ?: 'json';
        $params = [
            'auth' => [
                'username' => $data['username'],
                'password' => $data['password'],
            ],
            'ssl_verify_peer' => $data['ssl_verify_peer'],
            'ssl_verify_peer_name' => $data['ssl_verify_peer_name'],
            'ssl_verify_host' => $data['ssl_verify_host'],
            'ssl_cafile' => $data['ssl_cafile'],
            'headers' => [
                'Accept' => $data['data_format'] === 'csv'
                    ? 'text/csv'
                    : 'application/' . $data['data_format'],
                'X-Asalae-Webservice' => 'true',
            ],
        ];
        if ($data['use_proxy'] && Configure::read('Proxy.host')) {
            $params['proxy'] = [
                'proxy' => Configure::read('Proxy.host') . ':' . Configure::read('Proxy.port'),
                'username' => Configure::read('Proxy.login'),
                'password' => Configure::read('Proxy.password'),
            ];
        }

        $filename = 'testfile.' . $data['data_format'];
        $path = Configure::read('Upload.dir', TMP . 'upload') . DS . 'user_' . $this->userId;
        if (!file_exists($path)) {
            Filesystem::mkdir($path);
        }

        $client = get_class(Utility::get(Client::class));
        $client = new $client($params);

        /** @var Client $client */
        $clientResponse = $client->get($data['url'], [], ['cookies' => []]);

        if ($clientResponse->getStatusCode() === 401) {
            return $this->getResponse()
                ->withStringBody(__("Échec d'authentification de l'utilisateur"));
        }
        if ($debug || $clientResponse->getStatusCode() >= 400) {
            return $this->getResponse()
                ->withStatus($clientResponse->getStatusCode())
                ->withStringBody($clientResponse->getStringBody());
        }
        $body = $clientResponse->getBody();
        $body->rewind();

        $handle = fopen($path . DS . $filename, 'w');
        while (!$body->eof()) {
            fwrite($handle, $body->read(4096));
        }
        fclose($handle);

        $this->Fileuploads = $this->fetchTable('Fileuploads');
        $entity = $this->Fileuploads->newUploadedEntity(
            $filename,
            $path . DS . $filename,
            $this->userId
        );
        $this->Fileuploads->saveOrFail($entity);
        return $this->renderDataToJson($entity->toArray());
    }

    /**
     * Vérifi la validité d'un chemin dans un xml
     * @param EntityInterface $file
     * @return string
     */
    private function testFilePathXml(EntityInterface $file): string
    {
        $request = $this->getRequest();
        $dom = new DOMDocument();
        $dom->load($file->get('path'));
        $xpath = new DOMXPath($dom);
        foreach ($request->getQuery('namespace') ?: [] as $uri => $ns) {
            $xpath->registerNamespace($ns, $uri);
        }
        $node = $xpath->query($request->getQuery('path'));
        if (!$node) {
            $response = __("Le chemin indiqué est incorrect");
        } elseif ($node->count() > 1) {
            $arr = [];
            foreach ($node as $n) {
                $arr[] = $n->nodeValue;
            }
            $response = implode("\n", $arr);
        } else {
            $response = $node->count() >= 1
                ? $node->item(0)->nodeValue
                : __("Le chemin indiqué n'a pas été trouvé dans le document");
        }
        return $response;
    }

    /**
     * Vérifi la validité d'un chemin dans un json
     * @param EntityInterface $file
     * @return string
     */
    private function testFilePathJson(EntityInterface $file): string
    {
        $request = $this->getRequest();
        $json = new JSONPath(
            json_decode(
                file_get_contents($file->get('path'))
            )
        );
        try {
            $node = $json->find($request->getQuery('path'));
            if ($node->count() > 1) {
                $arr = [];
                foreach ($node as $n) {
                    $arr[] = $n;
                }
                $response = implode("\n", $arr);
            } else {
                $response = $node->count() >= 1
                    ? $node->first()
                    : __("Le chemin indiqué n'a pas été trouvé dans le document");
            }
        } catch (JSONPathException) {
            $response = __("Le chemin indiqué est incorrect");
        }
        return $response;
    }
}
