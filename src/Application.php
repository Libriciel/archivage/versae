<?php

/**
 * Versae\Application
 */

namespace Versae;

use AsalaeCore\Application as CoreApplication;
use AsalaeCore\Auth\AuthenticationService;
use AsalaeCore\Auth\Authenticator\BasicAuthenticator;
use AsalaeCore\Auth\Authenticator\FormAuthenticator;
use AsalaeCore\Auth\Authenticator\KeycloakAuthenticator;
use AsalaeCore\Auth\Authenticator\OpenIDConnectAuthenticator;
use AsalaeCore\Auth\Authenticator\SessionAuthenticator;
use AsalaeCore\Auth\Authenticator\UrlAuthenticator;
use AsalaeCore\Auth\Identifier\LdapIdentifier;
use AsalaeCore\Auth\Identifier\OpenIDConnectIdentifier;
use AsalaeCore\Auth\Identifier\PasswordIdentifier;
use AsalaeCore\Auth\Identifier\UrlIdentifier;
use AsalaeCore\Error\ExceptionTrap;
use AsalaeCore\Exception\GenericException;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\Client;
use AsalaeCore\Middleware\CorsMiddleware;
use AsalaeCore\Middleware\MaintenanceMiddleware;
use AsalaeCore\Middleware\RateLimiterMiddleware;
use AsalaeCore\Middleware\UrlAuthMiddleware;
use Authentication\AuthenticationServiceInterface;
use Authentication\AuthenticationServiceProviderInterface;
use Authentication\Identity;
use Authentication\Middleware\AuthenticationMiddleware;
use Authorization\AuthorizationServiceProviderInterface;
use Authorization\Middleware\AuthorizationMiddleware;
use Authorization\Middleware\RequestAuthorizationMiddleware;
use Cake\Core\Configure;
use Cake\Error\Middleware\ErrorHandlerMiddleware;
use Cake\Http\Middleware\BodyParserMiddleware;
use Cake\Http\MiddlewareQueue;
use Cake\Routing\Middleware\AssetMiddleware;
use Cake\Routing\Middleware\RoutingMiddleware;
use Cake\Routing\Router;
use Cake\Utility\Hash;
use Exception;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class Application
 * Existance nécessaire pour éviter un deprecated sur debug_kit
 *
 * @category Application
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Application extends CoreApplication implements
    AuthenticationServiceProviderInterface,
    AuthorizationServiceProviderInterface
{
    public const ADMINS_CONTROLLER = 'Admins';

    /**
     * @var \string[][] params d'auth
     */
    public $authentication = [
        'service' => [
            'className' => AuthenticationService::class,
            'identityClass' => Identity::class,
            'unauthenticatedRedirect' => '/Users/login',
            'queryParam' => 'redirect',
        ],
        'keycloak' => false,
        'openid-connect' => false,
    ];

    /**
     * Load all the application configuration and bootstrap logic.
     *
     * Override this method to add additional bootstrap logic for your application.
     */
    public function bootstrap(): void
    {
        parent::bootstrap();

        $this->addPlugin('AsalaeCore', ['bootstrap' => false]);
        $this->addPlugin('Libriciel/Login');
        $this->addPlugin('StateMachine');
    }

    /**
     * Setup the middleware your application will use.
     *
     * @param MiddlewareQueue $middlewareQueue The middleware queue to setup.
     * @return MiddlewareQueue The updated middleware queue.
     */
    public function middleware(MiddlewareQueue $middlewareQueue): MiddlewareQueue
    {
        $middlewareQueue
            // Cross-Origin Resource Sharing
            ->add(new CorsMiddleware())

            // Interruption de service
            ->add(new MaintenanceMiddleware())

            // Catch any exceptions in the lower layers,
            // and make an error page/response
            ->add(new ErrorHandlerMiddleware(ExceptionTrap::instance()))

            // Handle plugin/theme assets like CakePHP normally does.
            ->add(
                new AssetMiddleware(
                    [
                        'cacheTime' => Configure::read('Asset.cacheTime'),
                    ]
                )
            )

            // Add routing middleware.
            // If you have a large number of routes connected, turning on routes
            // caching in production could improve performance. For that when
            // creating the middleware instance specify the cache config name by
            // using it's second constructor argument:
            // `new RoutingMiddleware($this, '_cake_routes_')`
            ->add(new RoutingMiddleware($this))

            // add Authentication after RoutingMiddleware
            ->add(new AuthenticationMiddleware($this))

            // limite les tentatives de login
            ->add(new RateLimiterMiddleware())

            // Gestion des erreurs dans un context UrlAuth
            ->add(new UrlAuthMiddleware())

            // Add authorization (after authentication if you are using that plugin too).
            ->add(new AuthorizationMiddleware($this))
            ->add(new RequestAuthorizationMiddleware())

            // Parse various types of encoded request bodies so that they are
            // available as array through $request->getData()
            // https://book.cakephp.org/4/en/controllers/middleware.html#body-parser-middleware
            ->add(new BodyParserMiddleware());

        return $middlewareQueue;
    }

    /**
     * Charge les Identifiers pour identifier l'utilisateur à partir de credentials
     * @param AuthenticationService $authenticationService
     */
    private function loadIdentifiers(
        AuthenticationService $authenticationService
    ) {
        if ($this->authentication['keycloak']) {
            $authenticationService->loadIdentifier(
                'Keycloak',
                ['className' => OpenIDConnectIdentifier::class]
                + Configure::read('Keycloak')
                + $this->authentication['keycloak']
            );
        }
        if ($this->authentication['openid-connect']) {
            $authenticationService->loadIdentifier(
                'OpenIDConnect',
                ['className' => OpenIDConnectIdentifier::class]
                + Configure::read('OpenIDConnect')
                + $this->authentication['openid-connect']
            );
        }
        $authenticationService->loadIdentifier(
            'Ldap',
            [
                'className' => LdapIdentifier::class,
                'fields' => [
                    'username' => 'username',
                    'password' => 'password',
                ],
            ]
        );
        $authenticationService->loadIdentifier(
            'Password',
            [
                'className' => PasswordIdentifier::class,
                'fields' => [
                    'username' => 'username',
                    'password' => 'password',
                ],
            ]
        );
        $authenticationService->loadIdentifier(
            'Url',
            [
                'className' => UrlIdentifier::class,
            ]
        );
    }

    /**
     * Charge les Authenticators pour extraire les credentials de l'utilisateur connecté
     * @param AuthenticationService $authenticationService
     */
    private function loadAuthenticators(AuthenticationService $authenticationService)
    {
        $authenticationService->loadAuthenticator(
            'Url',
            [
                'className' => UrlAuthenticator::class,
            ]
        );
        if ($this->authentication['keycloak']) {
            $authenticationService->loadAuthenticator(
                'Keycloak',
                ['className' => KeycloakAuthenticator::class]
                + Configure::read('Keycloak')
                + $this->authentication['keycloak']
            );
        }
        if ($this->authentication['openid-connect']) {
            $authenticationService->loadAuthenticator(
                'OpenIDConnect',
                ['className' => OpenIDConnectAuthenticator::class]
                + Configure::read('OpenIDConnect')
                + $this->authentication['openid-connect']
            );
        }
        $authenticationService->loadAuthenticator(
            'Form',
            [
                'className' => FormAuthenticator::class,
                'fields' => [
                    'username' => 'username',
                    'password' => 'password',
                ],
                'loginUrl' => [
                    '/Users/login',
                    '/Users/ajax-login',
                    '/Admins/login',
                    '/Admins/ajax-login/',
                ],
            ]
        );
        $authenticationService->loadAuthenticator(
            'Basic',
            [
                'className' => BasicAuthenticator::class,
            ]
        );
        $authenticationService->loadAuthenticator(
            'Session',
            [
                'className' => SessionAuthenticator::class,
            ]
        );
    }

    /**
     * Donne le service d'auth
     * @param ServerRequestInterface $request
     * @return AuthenticationServiceInterface
     * @throws Exception
     */
    public function getAuthenticationService(ServerRequestInterface $request): AuthenticationServiceInterface
    {
        $this->authentication['service']['queryParam'] = null;
        $url = Router::parseRequest($request);
        if (($url['controller'] ?? '') === self::ADMINS_CONTROLLER) {
            $this->authentication['service']['unauthenticatedRedirect'] = '/admins/login';
        } elseif (Configure::read('Keycloak.enabled')) {
            $baseUrl = sprintf(
                '%s/auth/realms/%s/protocol/openid-connect/',
                Configure::read('Keycloak.base_url'),
                Configure::read('Keycloak.realm')
            );
            $this->authentication['keycloak'] = [
                'endpoints' => [
                    'authorization' => $baseUrl . 'auth',
                    'token' => $baseUrl . 'token',
                    'introspection' => $baseUrl . 'token/introspect',
                    'userinfo' => $baseUrl . 'userinfo',
                    'end_session' => $baseUrl . 'logout',
                ],
            ];
            $params = ['response_type' => 'code'];
            foreach (['client_id', 'redirect_uri', 'scope'] as $arg) {
                if ($val = Configure::read('Keycloak.' . $arg)) {
                    $params[$arg] = $val;
                }
            }
            $params = '?' . http_build_query($params);
            $authUrl = $this->authentication['keycloak']['endpoints']['authorization'] . $params;
            $this->authentication['service']['unauthenticatedRedirect'] = $authUrl;
        } elseif (Configure::read('OpenIDConnect.enabled')) {
            $conf = Configure::read('OpenIDConnect');
            $options = $conf['clientParams'] ?? [];
            $client = Utility::get(Client::class);
            /** @var Client $client */
            $response = $client->get(
                $url = sprintf('%s/.well-known/openid-configuration', $conf['base_url']),
                null,
                $options
            );
            $json = json_decode($response->getStringBody(), true);
            if ($response->getStatusCode() !== 200 || $json === false) {
                throw new GenericException(
                    sprintf(
                        'Error code %d in %s : %s',
                        $response->getStatusCode(),
                        $url,
                        substr($response->getStringBody(), 0, 1000)
                    )
                );
            }
            $endpoints = [];
            foreach ($json as $key => $value) {
                if (preg_match('/^(.*)_endpoint$/', $key, $m)) {
                    $endpoints[$m[1]] = $value;
                }
            }
            $this->authentication['openid-connect'] = ['endpoints' => $endpoints];

            if (Configure::read('OpenIDConnect.redirect_uri') === 'auto') {
                Configure::write(
                    'OpenIDConnect.redirect_uri',
                    Configure::read('App.fullBaseUrl') . '/'
                );
            }
            $params = ['response_type' => 'code'];
            foreach (['client_id', 'redirect_uri', 'scope'] as $arg) {
                if ($val = Configure::read('OpenIDConnect.' . $arg)) {
                    $params[$arg] = $val;
                }
            }
            $params = '?' . http_build_query($params);
            $authUrl = Hash::get($this->authentication, 'openid-connect.endpoints.authorization') . $params;
            $this->authentication['service']['unauthenticatedRedirect'] = $authUrl;
        } elseif ($xdebugSessionStart = Hash::get($url, '?.XDEBUG_SESSION_START')) {
            $ref = &$this->authentication['service']['unauthenticatedRedirect'];
            $ref = str_contains($ref, '?') ? $ref . '&' : $ref . '?';
            $ref .= 'XDEBUG_SESSION_START=' . $xdebugSessionStart;
        }

        /** @var \Authentication\AuthenticationService $authenticationService */
        $authenticationService = Utility::get(
            $this->authentication['service']['className'],
            $this->authentication['service']
        );
        $this->loadIdentifiers($authenticationService);
        $this->loadAuthenticators($authenticationService);

        return $authenticationService;
    }
}
