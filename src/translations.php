<?php

/**
 * Importé dans webroot/js/asalae.translation.js.php
 * Permet un bon fonctionnement du I18nShell::extract
 */

return [
    // global-top
    //      Filters
    'Retirer le filtre de recherche'
    => __d('js', "Retirer le filtre de recherche"),
    'Supprimer les filtres'
    => __d('js', "Supprimer les filtres"),
    'Rechercher'
    => __d('js', "Rechercher"),
    'Cette action supprimera la sauvegarde de façon définitive. Voulez-vous continuer ?'
    => __d('js', "Cette action supprimera la sauvegarde de façon définitive. Voulez-vous continuer ?"),
    'Impossible de sauvegarder en cas de formulaire vide ou en erreur!'
    => __d('js', "Impossible de sauvegarder en cas de formulaire vide ou en erreur!"),
    'Cette action va modifier la sauvegarde de façon définitive. Voulez-vous continuer ?'
    => __d('js', "Cette action va modifier la sauvegarde de façon définitive. Voulez-vous continuer ?"),
    'Nouvelle sauvegarde'
    => __d('js', "Nouvelle sauvegarde"),
    'Filtrer'
    => __d('js', "Filtrer"),
    'Réinitialiser les filtres'
    => __d('js', "Réinitialiser les filtres"),
    'Déconnecté'
    => __d('js', "Déconnecté"),
    "Erreur {0}{1}\n\nmessage: {2}"
    => __d('js', "Erreur {0}{1}\n\nmessage: {2}"),

    // Notifications
    "à l'instant" => __d('js', "à l'instant"),
    "depuis {0} minute(s)" => __d('js', "depuis {0} minute(s)"),
    "depuis {0} heure(s)" => __d('js', "depuis {0} heure(s)"),
    "depuis {0} jour(s)" => __d('js', "depuis {0} jour(s)"),

    // asalae.modal
    "Une erreur inattendue a eu lieu"
    => __d('js', "Une erreur inattendue a eu lieu"),
    "Vous devez être connecté pour accéder à ce contenu"
    => __d('js', "Vous devez être connecté pour accéder à ce contenu"),
    "Vous n'avez pas l'autorisation d'accéder à ce contenu"
    => __d('js', "Vous n'avez pas l'autorisation d'accéder à ce contenu"),
    "Une erreur a eu lieu lors de la suppression"
    => __d('js', "Une erreur a eu lieu lors de la suppression"),

    // asalae.upload.js
    "Validation..." => __d('js', "Validation..."),
    "Mettre l'upload en pause" => __d('js', "Mettre l'upload en pause"),
    "Reprise de l'upload" => __d('js', "Reprise de l'upload"),
    "Annuler l'upload" => __d('js', "Annuler l'upload"),
    "Relancer l'upload"
    => __d('js', "Relancer l'upload"),
    "Nouvelle tentative dans {0} secondes"
    => __d('js', "Nouvelle tentative dans {0} secondes"),
    "Validation du fichier en cours, veuillez patienter"
    => __d('js', "Validation du fichier en cours, veuillez patienter"),
    "Finalisation..."
    => __d('js', "Finalisation..."),
    "Votre navigateur ne supporte pas la fragmentation de fichiers, merci de le mettre à jour."
    => __d(
        'js',
        "Votre navigateur ne supporte pas la fragmentation de fichiers, merci de le mettre à jour."
    ),
    "Ou contactez un administrateur."
    => __d('js', "Ou contactez un administrateur."),
    "Fichier"
    => __d('js', "Fichier"),
    "Êtes-vous sûr de vouloir supprimer ce fichier ?"
    => __d('js', "Êtes-vous sûr de vouloir supprimer ce fichier ?"),
    "Êtes-vous sûr de vouloir supprimer les fichiers sélectionnés ?"
    => __d('js', "Êtes-vous sûr de vouloir supprimer les fichiers sélectionnés ?"),
    "Supprimer {0}" => __d('js', "Supprimer {0}"),
    "Sélection" => __d('js', "Sélection"),
    "Nom de fichier" => __d('js', "Nom de fichier"),
    "Message" => __d('js', "Message"),
    "Actions" => __d('js', "Actions"),

    // asalae.paginator.js
    "Affichage des résultats de {0} à {1} sur les<br>{2} résultats"
    => __d('js', "Affichage des résultats de {0} à {1} sur les<br>{2} résultats"),
    "Précédent"
    => __d('js', "Précédent"),
    "Suivant"
    => __d('js', "Suivant"),
    "Page: {0}"
    => __d('js', "Page: {0}"),
    "Retirer le tri et les filtres"
    => __d('js', "Retirer le tri et les filtres"),

    // asalae.libersign
    "L'extension de navigateur LiberSign est requise pour vous permettre d'effectuer une signature numérique."
    => __d(
        'js',
        "L'extension de navigateur LiberSign est requise pour vous permettre d'effectuer une signature numérique."
    ),
    "Aucun certificat n'a été trouvé"
    => __d('js', "Aucun certificat n'a été trouvé"),
    "Erreur lors de la tentative de récupération de vos certificats"
    => __d('js', "Erreur lors de la tentative de récupération de vos certificats"),
    "Identifiant"
    => __d('js', "Identifiant"),
    "Nom"
    => __d('js', "Nom"),
    "E-mail"
    => __d('js', "E-mail"),
    "Émetteur"
    => __d('js', "Émetteur"),
    "Sélectionner {0}"
    => __d('js', "Sélectionner {0}"),
    "Date d'expiration"
    => __d('js', "Date d'expiration"),
    "Signer"
    => __d('js', "Signer"),
    "Une erreur a eu lieu lors de la signature"
    => __d('js', "Une erreur a eu lieu lors de la signature"),

    // extented_jquery
    "Conflit d'id (DOM) détecté"
    => __d('js', "Conflit d'id (DOM) détecté"),

    // src
    'Vous avez apporté des modifications au formulaire. Souhaitez-vous les sauvegarder ?'
    => __d("js", 'Vous avez apporté des modifications au formulaire. Souhaitez-vous les sauvegarder ?'),
    'Êtes-vous sûr de vouloir supprimer cet enregistrement ?'
    => __d('js', 'Êtes-vous sûr de vouloir supprimer cet enregistrement ?'),
    "Veuillez sélectionner un fichier"
    => __d('js', "Veuillez sélectionner un fichier"),
    "Une erreur a eu lieu lors de l'import"
    => __d('js', "Une erreur a eu lieu lors de l'import"),
    "Import de {0} mots clés effectué avec succès"
    => __d('js', "Import de {0} mots clés effectué avec succès"),
    "Êtes-vous sûr de vouloir supprimer les mots clés sélectionnés ?"
    => __d('js', "Êtes-vous sûr de vouloir supprimer les mots clés sélectionnés ?"),
    "Supprimer le champ de formulaire"
    => __d('js', "Supprimer le champ de formulaire"),
    "Modifier"
    => __d('js', "Modifier"),
    "Section sans titre #{0}"
    => __d('js', "Section sans titre #{0}"),
    "Supprimer la section supprimera également tout ce qu'elle contient. Voulez-vous continuer ?"
    => __d('js', "Supprimer la section supprimera également tout ce qu'elle contient. Voulez-vous continuer ?"),
    "Importer cette liste supprimera les mots-clés existants. Voulez-vous continuer ?"
    => __d('js', "Importer cette liste supprimera les mots-clés existants. Voulez-vous continuer ?"),
    "Retirer la liste de mots-clés supprimera les mots-clés existants. Voulez-vous continuer ?"
    => __d('js', "Retirer la liste de mots-clés supprimera les mots-clés existants. Voulez-vous continuer ?"),
    "Terminé"
    => __d('js', "Terminé"),
    "Autorisé"
    => __d('js', "Autorisé"),
    "Non autorisé"
    => __d('js', "Non autorisé"),
    "Valide"
    => __d('js', "Valide"),
    "Pas assez complexe"
    => __d('js', "Pas assez complexe"),
    "Identique au mot de passe"
    => __d('js', "Identique au mot de passe"),
    "Différent du mot de passe"
    => __d('js', "Différent du mot de passe"),
    "ET"
    => __d('js', "ET"),
    "OU"
    => __d('js', "OU"),
    "Supprimer la condition supprimera également tout ce qu'elle contient. Voulez-vous continuer ?"
    => __d('js', "Supprimer la condition supprimera également tout ce qu'elle contient. Voulez-vous continuer ?"),
    "Valeur du Sinon SI"
    => __d('js', "Valeur du Sinon SI"),
    "Sinon SI"
    => __d('js', "Sinon SI"),
    "Ajouter une condition (OU)"
    => __d('js', "Ajouter une condition (OU)"),
    "Services versants disponibles"
    => __d('js', "Services versants disponibles"),
    "Afficher les boutons \"ajouter\""
    => __d('js', "Afficher les boutons \"ajouter\""),
    "Enregistrer l'unité d'archives"
    => __d('js', "Enregistrer l'unité d'archives"),
    "Cette action supprimera cette unité d'archives et tout ce qu'elle contient."
    . " Tapez le mot 'delete' pour confirmer la suppression"
    => __d(
        'js',
        "Cette action supprimera cette unité d'archives et tout ce qu'elle contient."
        . " Tapez le mot 'delete' pour confirmer la suppression"
    ),
    "Annuler" => __d('js', "Annuler"),
    "Fermer" => __d('js', "Fermer"),
    "Ldaps disponibles" => __d('js', "Ldaps disponibles"),
    "Réponse de {0}" => __d('js', "Réponse de {0}"),
    "Liste des jobs du tube {0}"
    => __d('js', "Liste des jobs du tube {0}"),
    "Liste des jobs {0} du tube {1}"
    => __d('js', "Liste des jobs {0} du tube {1}"),
    "Enregistrer et envoyer"
    => __d('js', "Enregistrer et envoyer"),
    "en cours"
    => __d('js', "en cours"),
    "acceptés"
    => __d('js', "acceptés"),
    "refusés"
    => __d('js', "refusés"),
    "Supprimer le Sinon SI"
    => __d('js', "Supprimer le Sinon SI"),
    "Supprimer le bloc Sinon SI supprimera également tout ce qu'il contient. Voulez-vous continuer ?"
    => __d(
        'js',
        "Supprimer le bloc Sinon SI supprimera également tout ce qu'il contient. Voulez-vous continuer ?"
    ),
    "Valeur de la variable si la condition de ce bloc Sinon SI est VRAI."
    => __d('js', "Valeur de la variable si la condition de ce bloc Sinon SI est VRAI."),
    "Cet élément est utilisé et ne peut donc pas être supprimé"
    => __d('js', "Cet élément est utilisé et ne peut donc pas être supprimé"),
    "Voulez-vous supprimer ce champ ?"
    => __d('js', "Voulez-vous supprimer ce champ ?"),
    "Des versements sont en cours pour cet utilisateur. Si vous confirmez la suppression, "
    . "ses versements en cours seront également supprimés."
    => __d(
        'js',
        "Des versements sont en cours pour cet utilisateur. Si vous confirmez la suppression, "
        . "ses versements en cours seront également supprimés."
    ),
    "Message de l'administrateur" => __d('js', "Message de l'administrateur"),
    "Un champ avec ce nom existe déjà" => __d('js', "Un champ avec ce nom existe déjà"),
    "Modifier le champ de formulaire" => __d('js', "Modifier le champ de formulaire"),
    "Cette action va forcer le lancement de la tâche planifiée, voulez-vous continuer ?"
    => __d(
        'js',
        "Cette action va forcer le lancement de la tâche planifiée, voulez-vous continuer ?"
    ),
    "Supprimer ce bloc Sinon SI" => __d('js', "Supprimer ce bloc Sinon SI"),
    "Si" => __d('js', "Si"),
    "Sinon" => __d('js', "Sinon"),
    "valeur {0}" => __d('js', "valeur {0}"),
    "champ {0}" => __d('js', "champ {0}"),
    "texte {0}" => __d('js', "texte {0}"),
    "métadonnée de fichier {0}" => __d('js', "métadonnée de fichier {0}"),
    "alors la valeur sera {0}" => __d('js', "alors la valeur sera {0}"),
    "la valeur sera {0}" => __d('js', "la valeur sera {0}"),
    "égal au" => __d('js', "égal au"),
    "différent du" => __d('js', "différent du"),
    "supérieur au" => __d('js', "supérieur au"),
    "supérieur ou égal au" => __d('js', "supérieur ou égal au"),
    "inférieur au" => __d('js', "inférieur au"),
    "inférieur ou égal au" => __d('js', "inférieur ou égal au"),
    "à l'intérieur du" => __d('js', "à l'intérieur du"),
    "en dehors du" => __d('js', "en dehors du"),
    "non défini" => __d('js', "non défini"),
    "valeur vide" => __d('js', "valeur vide"),
    "la valeur de la métadonnée de fichier {0}" => __d('js', "la valeur de la métadonnée de fichier {0}"),
    "la valeur du champ {0}" => __d('js', "la valeur du champ {0}"),
    "le texte {0}" => __d('js', "le texte {0}"),
    "la valeur vide" => __d('js', "la valeur vide"),
    "le nombre {0}" => __d('js', "le nombre {0}"),
    "un espace" => __d('js', "un espace"),
    "en minuscule" => __d('js', "en minuscule"),
    "nombre {0}" => __d('js', "nombre {0}"),
    "Ajouter" => __d('js', "Ajouter"),
    "est" => __d('js', "est"),
    "Ajouter une section" => __d('js', "Ajouter une section"),
    "Supprimer cette section de formulaire" => __d('js', "Supprimer cette section de formulaire"),
    "Voulez-vous supprimer cette section ?" => __d('js', "Voulez-vous supprimer cette section ?"),
    "Cette option n'est pas disponible" => __d('js', "Cette option n'est pas disponible"),
    "Ce nom est réservé" => __d('js', "Ce nom est réservé"),
    "La case est cochée" => __d('js', "La case est cochée"),
    "La case n'est pas cochée" => __d('js', "La case n'est pas cochée"),
    "Ajouter la valeur ?" => __d('js', "Ajouter la valeur ?"),
    "Ordre alphabétique sur texte affiché" => __d('js', "Ordre alphabétique sur texte affiché"),
    "Ordre alphabétique inverse sur texte affiché" => __d('js', "Ordre alphabétique inverse sur texte affiché"),
    "Ordre alphabétique sur valeur" => __d('js', "Ordre alphabétique sur valeur"),
    "Ordre alphabétique inverse sur valeur" => __d('js', "Ordre alphabétique inverse sur valeur"),
    "Ordre numérique sur valeur" => __d('js', "Ordre numérique sur valeur"),
    "Ordre numérique inverse sur valeur" => __d('js', "Ordre numérique inverse sur valeur"),
    "Changer l'ordre des options" => __d('js', "Changer l'ordre des options"),
    "Une erreur a eu lieu avec le message suivant: {0}"
    => __d('js', "Une erreur a eu lieu avec le message suivant: {0}"),
    "Le versement contient des erreurs, sauvegarder le brouillon ?"
    => __d('js', "Le versement contient des erreurs, sauvegarder le brouillon ?"),
    "Veuillez entrer le mail de destination" => __d("js", "Veuillez entrer le mail de destination"),
    "Email invalide" => __d("js", "Email invalide"),
    "Préparation du versement" => __d("js", "Préparation du versement"),
    "Non lié à un worker" => __d("js", "Non lié à un worker"),
    "Lié à un worker" => __d("js", "Lié à un worker"),
    "Etat du job : {0}" => __d("js", "Etat du job : {0}"),
    "Un worker travaille actuellement" => __d("js", "Un worker travaille actuellement"),
    "Aucun workers ne travaille actuellement" => __d("js", "Aucun workers ne travaille actuellement"),
    "Ajouter la date" => __d("js", "Ajouter la date"),
    "Attention, il semble que le fichier {0} soit déjà présent dans le formulaire"
    => __d("js", "Attention, il semble que le fichier {0} soit déjà présent dans le formulaire"),
    "Ce champ fait partie d'une autre section répétable"
    => __d("js", "Ce champ fait partie d'une autre section répétable"),
    "On ne peut pas utiliser un élément de section répétable en dehors d'une unité d'archives répétables"
    => __d("js", "On ne peut pas utiliser un élément de section répétable en dehors d'une unité d'archives répétables"),
    "Veuillez sélectionner au moins une case à cocher"
    => __d("js", "Veuillez sélectionner au moins une case à cocher"),
    "La variable {0} ne fait pas partie des variables disponibles"
    => __d("js", "La variable {0} ne fait pas partie des variables disponibles"),
    "Le nombre d'ouverture de variables Twig {{ ne correspond pas au nombre de fermetures }}."
    => __d("js", "Le nombre d'ouverture de variables Twig {{ ne correspond pas au nombre de fermetures }}."),
    "On ne peut pas utiliser \"Valeur multiple\" pour une variable non multiple"
    => __d("js", "On ne peut pas utiliser \"Valeur multiple\" pour une variable non multiple"),
    "Cette option fait partie d'une autre section répétable"
    => __d("js", "Cette option fait partie d'une autre section répétable"),
    "Cette option fait partie d'une section répétable"
    => __d("js", "Cette option fait partie d'une section répétable"),
];
