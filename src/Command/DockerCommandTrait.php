<?php

/**
 * Versae\Command\DockerTrait
 */

namespace Versae\Command;

use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\Client;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Http\Client\Response;
use Cake\Http\Exception\HttpException;
use Exception;

/**
 * Kill un worker par commande socket (docker)
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin DockerCommandInterface
 */
trait DockerCommandTrait
{
    /**
     * @var string
     */
    private $portenairUrl = 'http://portainer:9000'; // NOSONAR interne, pas besoin de https
    /**
     * @var string
     */
    private $portenairUsername = 'versae';
    /**
     * @var string
     */
    private $portenairPassword = 'versaeversae';// NOSONAR n'est pas accessible de l'exterieur
    /**
     * @var string
     */
    private $portainerToken = '';

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->setDescription(self::getDescription());
        $parser->addArgument(
            'identifier',
            [
                'help' => __("Identifier ou name du container"),
                'required' => true,
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        try {
            $this->connectToPortainer();
            /** @var Response $response */
            $response = $this->client(
                'post',
                sprintf(
                    'api/endpoints/1/docker/containers/%s/' . $this->getCommandName(),
                    $args->getArgument('identifier')
                ),
                json_encode(['t' => 0])
            );
        } catch (Exception $e) {
            $io->error($e->getMessage());
            throw $e;
        }
        if ($response->getStatusCode() === 204) {
            $io->success($this->getSuccessMessage($args));
        } else {
            throw new HttpException($response->getStringBody(), $response->getStatusCode());
        }
    }

    /**
     * Récupère le token de connexion
     * @throws Exception
     */
    private function connectToPortainer()
    {
        /** @var Client $client */
        $client = Utility::get(Client::class);
        /** @var Response $response */
        $response = $client->post(
            sprintf('%s/api/auth', $this->portenairUrl),
            json_encode(['Username' => $this->portenairUsername, 'Password' => $this->portenairPassword]),
            ['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]
        );
        if ($response->getStatusCode() >= 400) {
            throw new HttpException($response->getStringBody(), $response->getStatusCode());
        }
        $this->portainerToken = json_decode($response->getStringBody(), true)['jwt'];
    }

    /**
     * Effectue un GET sur portainer
     * @param string $method get | post | delete | put ...
     * @param string $url
     * @param mixed  $data
     * @return Response
     * @throws Exception
     */
    private function client(string $method, string $url, $data): Response
    {
        /** @var Client $client */
        $client = Utility::get(Client::class);
        /** @var Response $response */
        $response = $client->$method(
            sprintf('%s/%s', $this->portenairUrl, $url),
            $data,
            $this->getClientParams()
        );
        if ($response->getStatusCode() >= 400) {
            throw new HttpException($response->getStringBody(), $response->getStatusCode());
        }
        return $response;
    }

    /**
     * Donne les params pour le client HTTP
     * @return array
     */
    private function getClientParams(): array
    {
        return [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->portainerToken,
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
        ];
    }
}
