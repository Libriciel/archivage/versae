<?php

/**
 * Versae\Command\SessionCommand
 */

namespace Versae\Command;

use AsalaeCore\Command\CommandShellTrait;
use Cake\Command\Command;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\ORM\TableRegistry;
use DateTimeInterface;
use Versae\Model\Entity\Session;

/**
 * Update asynchrone des entités d'un utilisateur (stockés en session)
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class SessionCommand extends Command
{
    use CommandShellTrait;

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->addSubcommand(
            'orgentities',
            [
                'help' => __("Met à jour les org_entities stockés en session utilisateur")
            ]
        );
        $parser->addSubcommand(
            'list',
            [
                'help' => __("Liste les sessions utilisateurs")
            ]
        );
        return $parser;
    }

    /**
     * Méthode principale
     */
    public function orgentities()
    {
        $Sessions = TableRegistry::getTableLocator()->get('Sessions');
        /** @var Session $session */
        foreach ($Sessions->find() as $session) {
            try {
                $session->updateOrgEntities();
                $Sessions->save($session);
            } catch (RecordNotFoundException) {
                $Sessions->delete($session);
            }
        }
        $this->success('done');
    }

    /**
     * Liste les sessions actives
     */
    public function list()
    {
        $Sessions = TableRegistry::getTableLocator()->get('Sessions');
        $fields = ['id', 'created', 'modified', 'username', 'name'];
        $data = $Sessions->find()
            ->select(
                [
                    'id' => 'Sessions.id',
                    'created' => 'Sessions.created',
                    'modified' => 'Sessions.modified',
                    'username' => 'Users.username',
                    'name' => 'Users.name',
                ]
            )
            ->contain(['Users'])
            ->where(['Sessions.user_id IS NOT' => null])
            ->order(['Users.username' => 'asc', 'Sessions.modified' => 'desc'])
            ->disableHydration()
            ->all()
            ->map(
                function ($data) {
                    foreach ($data as $k => $v) {
                        if ($v instanceof DateTimeInterface) {
                            $data[$k] = $v->format('Y-m-d H:i:s');
                        }
                    }
                    return $data;
                }
            )
            ->toArray();
        array_unshift($data, $fields);

        $this->helper('Table')->output($data);
    }
}
