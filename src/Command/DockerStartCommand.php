<?php

/**
 * Versae\Command\DockerStartCommand
 */

namespace Versae\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;

/**
 * Start un worker par commande (docker)
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DockerStartCommand extends Command implements DockerCommandInterface
{
    use DockerCommandTrait;

    /**
     * @var Arguments
     */
    private $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'docker start';
    }

    /**
     * Donne le nom de la commande
     * @return string
     */
    public function getCommandName(): string
    {
        return 'start';
    }

    /**
     * Donne la description
     * @return string
     */
    public static function getDescription(): string
    {
        return __("Démarre un container (docker)");
    }

    /**
     * Donne le message si la commande réussi
     * @param Arguments $args
     * @return string
     */
    public function getSuccessMessage(Arguments $args): string
    {
        return __("{0} démarré avec succès", $args->getArgument('identifier'));
    }
}
