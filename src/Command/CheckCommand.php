<?php

/**
 * Versae\Command\CheckCommand
 */

namespace Versae\Command;

use AsalaeCore\Factory\Utility;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Exception;

/**
 * Vérification de l'application
 *
 * @category    Command
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class CheckCommand extends Command
{
    /**
     * Gets the option parser instance and configures it.
     *
     * @param ConsoleOptionParser $parser Option parser to update.
     * @return ConsoleOptionParser
     */
    public function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->setDescription(
            __("Permet de savoir si l'application est en état de fonctionner")
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return void
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $Check = Utility::get('Check');
        $missings = $Check->missings();
        if ($missings) {
            $io->error('missing binaries: ' . implode(', ', $missings));
        }
        $missingsExtPhp = $Check->missingsExtPhp();
        if ($missingsExtPhp) {
            $io->error('missing ext-php: ' . implode(', ', $missingsExtPhp));
        }
        $database = $Check->database();
        if (!$database) {
            $io->error('unable to connect to database');
        }
        $beanstalkd = $Check->beanstalkd();
        if (!$beanstalkd) {
            $io->error('unable to connect to beanstalkd');
        }
        $ratchet = $Check->ratchet();
        if (!$ratchet) {
            $io->error('unable to connect to ratchet');
        }
        $php = $Check->phpOk();
        if (!$php) {
            $io->error('bad php configuration');
        }
        $systemOk = empty($missings) && empty($missingsExtPhp)
            && $database && $ratchet && $beanstalkd && $php;
        if ($systemOk) {
            $io->success('OK');
        }
        $this->abort($systemOk ? self::CODE_SUCCESS : self::CODE_ERROR);
    }
}
