<?php

/**
 * Asalae\Command\PasswordCommand
 */

namespace Versae\Command;

use Versae\Model\Entity\AuthUrl;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\View\Helper\UrlHelper;
use Cake\View\View;

/**
 * Génère un lien de saisie du mot de passe pour un utilisateur donné
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2023, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class PasswordCommand extends Command
{
    /**
     * Gets the option parser instance and configures it.
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->setDescription(
            __("Permet de générer un lien vers la saisie du mot de passe d'un utilisateur")
        );
        $parser->addArgument(
            'user',
            [
                'help' => __("ID ou username de l'utilisateur"),
                'required' => true,
            ]
        );
        return $parser;
    }

    /**
     * Donne un lien de changement de mot de passe utilisateur
     * @param Arguments $args
     * @param ConsoleIo $io
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        parent::execute($args, $io);

        $userId = $args->getArgument('user');
        $condition = is_numeric($userId)
            ? ['Users.id' => $userId]
            : ['Users.username' => $userId];

        $Users = $this->fetchTable('Users');
        $user = $Users->find()
            ->where($condition)
            ->firstOrFail();

        $AuthUrls = $this->fetchTable('AuthUrls');
        /** @var AuthUrl $code */
        $code = $AuthUrls->newEntity(
            [
                'url' => '/users/initialize-password/' . $user->get('id'),
                'expire' => date('Y-m-d H:i:s', strtotime('+10 minutes'))
            ]
        );
        $AuthUrls->saveOrFail($code);

        $Url = new UrlHelper(new View());
        $io->out($Url->build('/auth-urls/activate/' . $code->get('code'), ['fullBase' => true]));
    }
}
