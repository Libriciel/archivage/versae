<?php

/**
 * Versae\Command\FixturePermsCommand
 */

namespace Versae\Command;

use AsalaeCore\Command\DatabaseFixturesCommand;
use AsalaeCore\Command\FixturesDatabaseCommand;
use AsalaeCore\Console\AppConsoleOptionParser;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\ConnectionManager;
use Exception;

/**
 * Met à jour les fixtures de permissions
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FixturesPermsCommand extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'fixtures perms';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new AppConsoleOptionParser();
        $parser->setDescription(
            __("Met à jour les fixtures de permissions (Acos, Aros et ArosAcos)")
        );
        return $parser;
    }

    /**
     * Action principale
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $datasourceName = 'fixtures_perms';
        $datasource = ConnectionManager::get('fixtures_perms');
        $config = $datasource->config();
        if (is_file($config['database'])) {
            unlink($config['database']);
        }
        try {
            $nArgs = new Arguments([], ['datasource' => $datasourceName], []);
            (new FixturesDatabaseCommand())->execute($nArgs, $io);
            (new DatabaseFixturesCommand())->execute($nArgs, $io);
        } finally {
            if (is_file($config['database'])) {
                unlink($config['database']);
            }
        }
        $fixtureDir = TESTS . 'Fixture' . DS;
        $tmpDir = TMP . 'Fixture' . DS;
        $fixtures = ['Acos', 'Aros', 'ArosAcos'];
        foreach ($fixtures as $fixture) {
            $fixture .= 'Fixture.php';
            file_put_contents(
                $fixtureDir . $fixture,
                file_get_contents($tmpDir . $fixture)
            );
            $io->success(__("Mise à jour de {0}", $fixtureDir . $fixture));
        }
    }
}
