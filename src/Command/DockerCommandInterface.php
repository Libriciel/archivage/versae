<?php

/**
 * Versae\Command\DockerCommandInterface
 */

namespace Versae\Command;

use Cake\Console\Arguments;

/**
 * Commande socket (docker)
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface DockerCommandInterface
{
    /**
     * Donne le nom de la commande
     * @return string
     */
    public function getCommandName(): string;

    /**
     * Donne la description
     * @return string
     */
    public static function getDescription(): string;

    /**
     * Donne le message si la commande réussi
     * @param Arguments $args
     * @return string
     */
    public function getSuccessMessage(Arguments $args): string;
}
