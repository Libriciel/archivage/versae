<?php

/**
 * Versae\Command\DockerKillCommand
 */

namespace Versae\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;

/**
 * Kill un worker par commande socket (docker)
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DockerKillCommand extends Command implements DockerCommandInterface
{
    use DockerCommandTrait;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'docker kill';
    }

    /**
     * Donne le nom de la commande
     * @return string
     */
    public function getCommandName(): string
    {
        return 'kill';
    }

    /**
     * Donne la description
     * @return string
     */
    public static function getDescription(): string
    {
        return __("Kill un container (docker)");
    }

    /**
     * Donne le message si la commande réussi
     * @param Arguments $args
     * @return string
     */
    public function getSuccessMessage(Arguments $args): string
    {
        return __("{0} tué avec succès", $args->getArgument('identifier'));
    }
}
