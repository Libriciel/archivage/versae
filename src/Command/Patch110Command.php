<?php

/**
 * Versae\Command\Patch110Command
 */

namespace Versae\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Versae\Model\Table\DepositsTable;

/**
 * Suppression des répertoires des transferts acceptés et des dossiers plus pertinents
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2023, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Patch110Command extends Command
{
    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();

        $parser->setDescription(
            __("Suppression des répertoires des transferts acceptés et des dossiers plus pertinents")
        );

        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io): void
    {
        $appPathData = Configure::read('App.paths.data');
        if (!$appPathData) {
            throw new Exception('empty App.paths.data config key');
        }
        // suppression du répertoire <App.paths.data>/transfers
        $appPathData = rtrim($appPathData, DS) . DS;
        $path =  $appPathData . 'transfers' . DS;
        if (is_dir($path)) {
            $io->out(__("Suppression du répertoire {0}", $path));
            Filesystem::remove($path);
        }

        // suppression des fichiers et sous répertoires dans <App.paths.data>/tmp
        //      dont la date de création est antérieure à 7 jours par rapport à la date courante
        $path = $appPathData . 'tmp' . DS;
        $io->out(
            __(
                "Suppression des fichiers et sous répertoires dans {0}"
                    . " dont la date de création est antérieure à 7 jours par rapport à la date courante",
                $path
            )
        );
        $limitDate = time() - 7 * 24 * 3600;
        foreach (glob($path . '*') as $uri) {
            if (filemtime($uri) <= $limitDate) {
                Filesystem::remove($uri);
            }
        }

        // suppression des sous répertoires vides dans <App.paths.data>/form_test
        $path = $appPathData . 'form_test' . DS;
        $io->out(__("Suppression des sous répertoires vides dans {0}", $path));
        Filesystem::removeEmptySubFolders($path);

        // suppression des sous répertoires vides dans <App.paths.data>/upload
        $path = $appPathData . 'upload' . DS;
        $io->out(__("Suppression des sous répertoires vides dans {0}", $path));
        Filesystem::removeEmptySubFolders($path);

        // Pour les versements acceptés :
        $depositsPath = $appPathData . 'deposits' . DS;
        $Deposits = TableRegistry::getTableLocator()->get('Deposits');
        $deposits = $Deposits->find()
            ->where(['state' => DepositsTable::S_ACCEPTED]);

        foreach ($deposits as $deposit) {
            $dPath = $depositsPath . $deposit->get('id') . '_' . $deposit->get('path_token') . DS;
            $io->out(__("Nettoyage du dossier {0}", $dPath));
            if (is_file($dPath . 'management_data/transfer.xml')) {
                Filesystem::rename(
                    $dPath . 'management_data/transfer.xml',
                    $dPath . 'transfer.xml'
                );
            }
            foreach (glob($dPath . '*') as $uri) {
                if (is_dir($uri)) {
                    Filesystem::remove($uri);
                }
            }
        }

        $Roles = TableRegistry::getTableLocator()->get('Roles');
        $Roles->updateAll(
            [
                'description' => "Ce rôle correspond à un agent des services qui"
                    . " est en relation avec le Service d'Archives. Il a la"
                    . " possibilité de faire des versements pour le compte"
                    . " d'autres services versants."
            ],
            ['code' => 'ARA'],
        );

        $io->success('done');
    }
}
