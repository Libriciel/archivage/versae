<?php

/**
 * Versae\Command\CronsProbeCommand
 */

namespace Versae\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;
use Versae\Model\Table\CronExecutionsTable;

/**
 * Class CronsProbeShell : check if any crons ended with 'error' or 'aborted' state
 * Return code 0 if ok, 1 if only ok or warning, 2 if at least one error
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property CronExecutionsTable CronExecutions
 */
class CronsProbeCommand extends Command
{
    /**
     * Warning code for this shell
     *
     * @var int
     */
    public const CODE_PROBE_WARNING = 1;

    /**
     * Error code for this shell
     *
     * @var int
     */
    public const CODE_PROBE_ERROR = 2;

    /**
     * Méthode principale
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->CronExecutions = $this->fetchTable('CronExecutions');

        $cronExecutions = $this->CronExecutions->find()
            ->where(['date_end IS NOT' => null]) // execution is over
            ->order(
                [
                    'cron_id' => 'asc',
                    'date_end' => 'desc', // the last one
                ]
            )
            ->distinct(['cron_id'])
            ->contain(['Crons'])
            ->all()
            ->filter(
                function (EntityInterface $e): bool {
                    return in_array(
                        $e->get('state'),
                        [
                            CronExecutionsTable::S_WARNING,
                            CronExecutionsTable::S_ERROR,
                            CronExecutionsTable::S_ABORTED,
                        ]
                    );
                }
            );

        if ($cronExecutions->count() === 0) {
            return self::CODE_SUCCESS;
        }

        $returnCode = self::CODE_PROBE_WARNING;

        foreach ($cronExecutions as $exec) {
            $cronId = Hash::get($exec, 'cron.id');
            if (!$cronId) {
                if ($exec->get('state') === CronExecutionsTable::S_WARNING) {
                    $io->out($exec->get('report'));
                } else {
                    $io->err($exec->get('report'));
                }
                continue;
            }
            $cronName = Hash::get($exec, 'cron.name');
            $display = "#$cronId $cronName :";
            $report = $exec->get('report');
            $cronName = Hash::get($exec, 'cron.classname');
            $cron = new $cronName();
            if (method_exists($cron, 'formatForConsole')) {
                $report = $cron->formatForConsole($exec->get('report'));
            }
            if ($exec->get('state') === CronExecutionsTable::S_WARNING) {
                $io->out($display);
                $io->out($report);
            } else {
                $returnCode = self::CODE_PROBE_ERROR;
                $io->err($display);
                $io->err($report);
            }
            $io->out();
        }
        return $returnCode;
    }
}
