<?php

/**
 * Versae\Command\InstallCommand
 */

namespace Versae\Command;

use AsalaeCore\Command\CommandShellTrait;
use AsalaeCore\Command\CreateAcosTrait;
use AsalaeCore\Command\PromptOptionsTrait;
use AsalaeCore\Console\AppConsoleOptionParser;
use Cake\Command\Command;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\Database\Connection;
use Cake\Database\Exception\MissingConnectionException;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Table;
use Exception;
use PDOException;
use Versae\Model\Entity\OrgEntity;
use Versae\Model\Table\AcosTable;
use Versae\Model\Table\ArosAcosTable;
use Versae\Model\Table\ArosTable;
use Versae\Model\Table\OrgEntitiesTable;
use Versae\Model\Table\TypeEntitiesTable;
use Versae\Model\Table\UsersTable;

/**
 * Facilite l'installation initale de Versae
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 * @property UsersTable $Users
 * @property OrgEntitiesTable $OrgEntities
 * @property TypeEntitiesTable $TypeEntities
 * @property ArosAcosTable $Permissions
 * @property ArosTable $Aros
 * @property AcosTable $Acos
 * @property Table $Phinxlog
 */
class InstallCommand extends Command
{
    use PromptOptionsTrait;
    use CreateAcosTrait;
    use CommandShellTrait;

    /**
     * @var string ApiInterface::class
     */
    public const API_INTERFACE = '\\AsalaeCore\\Controller\\ApiInterface';

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new AppConsoleOptionParser();
        $parser->addOption(
            'se-name',
            [
                'help' => __d('install_shell', "Nom du service d'exploitation"),
            ]
        );
        $parser->addOption(
            'se-id',
            [
                'help' => __d('install_shell', "Identifiant du service d'exploitation"),
            ]
        );
        return $parser;
    }

    /**
     * Méthode principale
     */
    public function main(): void
    {
        if (!is_dir(TMP)) {
            mkdir(TMP);
        }
        if (!is_dir(Configure::read('App.paths.logs', LOGS))) {
            mkdir(Configure::read('App.paths.logs', LOGS));
        }
        $this->checkConfig();
        $this->checkDatabase();

        /** @var Connection $conn */
        $conn = ConnectionManager::get('default');
        $conn->begin();
        $this->createServiceExploitation();
        $this->initializePermissions();
        $conn->commit();
    }

    /**
     * Vérifie que la base de donnée est accessible à partir des paramètres saisi
     * dans la configuration
     *
     * @return boolean
     */
    private function checkConnection(): bool
    {
        try {
            $conn = ConnectionManager::get('default');
            if ($conn instanceof Connection) {
                $conn->query('select 1');
            }
            return true;
        } catch (PDOException | MissingConnectionException) {
            return false;
        }
    }

    /**
     * Vérification du fichier de configuration
     */
    private function checkConfig(): void
    {
        $this->out(__d('install', "Vérification du fichier de configuration..."));
        $path = Configure::read('App.paths.path_to_local_config');
        if (is_file($path)) {
            $configFile = include $path;
            if (is_file($configFile)) {
                $this->out(__d('install', "Fichier de configuration trouvé."));
            } else {
                $this->abortShell(__d('install', "Erreur, impossible de trouver le fichier de configuration !"));
            }
        } else {
            $this->abortShell(
                __d(
                    'install',
                    "Il semble que l'application n'a pas été configurée. "
                    . "Merci de lancer le shell `Configuration` avant de lancer l'installation."
                )
            );
        }
    }

    /**
     * Vérification de l'état de la base de données (connexion et migrations)
     */
    private function checkDatabase(): void
    {
        $this->out(__d('install', "Tentative de connexion à la base de donnée..."));
        if (!$this->checkConnection()) {
            $this->out(__d('install', "échec de connexion. merci de vérifier le fichier {0}", "config/app_local.json"));
            exit;
        }
        $this->success(__d('install', "Connexion réussie."));
        $this->out(__d('install', "Vérifications de l'existance de la migration..."));
        while (!is_file(CONFIG . 'Migrations' . DS . 'schema-dump-default.lock')) {
            $this->out(__d('install', "Vous devez effectuer la migration pour continuer."));
            $this->out("bin/cake migrations migrate");
            $this->in(__d('install', "OK..."));
        }
        do {
            $error = null;
            try {
                $this->Phinxlog = $this->fetchTable('Phinxlog');
                $this->Phinxlog->find()->first();
            } catch (\Cake\Database\Exception $e) {
                $error = $e->getMessage();
                $this->err($error);
                $this->out(__d('install', "Vous devez effectuer la migration pour continuer."));
                $this->out("bin/cake migrations migrate");
                $this->in(__d('install', "OK..."));
            }
        } while ($error);
        $this->success(__d('install', "Migration ok"));
    }

    /**
     * Initialise le service d'exploitation
     * @return OrgEntity Service d'exploitation
     */
    private function createServiceExploitation(): OrgEntity
    {
        $this->OrgEntities = $this->fetchTable('OrgEntities');
        $this->TypeEntities = $this->fetchTable('TypeEntities');
        /** @var OrgEntity $se */
        $se = $this->OrgEntities->find()->where(['code' => 'SE'])->contain(['TypeEntities'])->first();
        if ($se) {
            $this->out(__d('install', "Service d'exploitation ok"));
            return $se;
        }
        $type_entity_id = $this->TypeEntities->find()
            ->select(['id'])
            ->where(['code' => 'SE'])
            ->firstOrFail()
            ->get('id');
        /** @var OrgEntity $se */
        $se = $this->createEntity(
            $this->OrgEntities,
            [
                'name' => [
                    'message' => __d('install', "Veuillez saisir un nom pour le service d'exploitation"),
                    'option' => 'se-name',
                ],
                'identifier' => [
                    'message' => __d('install', "Veuillez saisir son identifiant unique"),
                    'option' => 'se-id',
                ],
            ],
            compact('type_entity_id')
        );
        $this->success(__d('install', "Entité créée avec succès"));

        return $se;
    }

    /**
     * Crée une entité de façon générique
     *
     * ex:
     * $model = $this->OrgEntities
     * $fields = [
     *      'name' => ['message' => 'Saisir un nom', 'option' => 'username']
     * ]
     * @param Table $model
     * @param array $fields
     * @param array $initialData
     * @return EntityInterface
     */
    private function createEntity(Table $model, array $fields, array $initialData = []): EntityInterface
    {
        $data = $initialData;
        foreach ($fields as $field => $params) {
            if (!empty($params['option'])) {
                $data[$field] = $this->param($params['option']);
            }
        }
        do {
            foreach ($fields as $field => $params) {
                if (!empty($data[$field])) {
                    continue;
                }
                $func = !empty($params['type']) && $params['type'] === 'password'
                    ? 'enterPassword'
                    : 'in';
                $value = $this->$func($params['message']);
                $data[$field] = $value;
                if (empty($params['copy'])) {
                    continue;
                }
                foreach ($params['copy'] as $field) {
                    $data[$field] = $value;
                }
            }
            $entity = $model->newEntity($data);
            if ($err = $entity->getErrors()) {
                foreach ($err as $field => $error) {
                    $this->err(__d('install', "Erreur sur le champ ''{0}'': {1}", $field, current($error)));
                }
                $data = $initialData;
            }
        } while (!$model->save($entity));
        return $entity;
    }

    /**
     * Créé les acos et assure l'accès à l'application de l'administrateur
     * @throws Exception
     */
    private function initializePermissions(): void
    {
        $this->Aros = $this->fetchTable('Aros');
        $this->Permissions = $this->fetchTable('ArosAcos');
        $this->out(__d('install', "Initialisation en cours..."));
        $this->createControllerAcos();
        $this->success(__d('install', "L'application a été installée."));
        $this->out(__d('install', "Initialisation de l'api REST..."));
        $this->createApiAcos();
        $this->success(__d('install', "Api REST installée."));
    }
}
