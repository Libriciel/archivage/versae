<?php

/**
 * Versae\Command\ConfigurationCommand
 */

namespace Versae\Command;

use AsalaeCore\Command\CommandShellTrait;
use AsalaeCore\Command\FileTrait;
use AsalaeCore\Command\PasswordTrait;
use AsalaeCore\Utility\Config;
use Cake\Command\Command;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\Database\Connection;
use Cake\Error\Debugger;
use Cake\I18n\I18n;
use Cake\Datasource\ConnectionManager;
use Cake\Utility\Hash;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Permet de créer le fichier de configuration
 *
 * @category    Shell
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ConfigurationCommand extends Command
{
    use PasswordTrait;
    use FileTrait;
    use CommandShellTrait;

    /**
     * @var array Datasources configurables
     */
    public static $datasources = ['default', 'test'];

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parserGet = parent::getOptionParser();
        $parserSet = parent::getOptionParser();
        $apacheUser = exec('apachectl -S 2>/dev/null | grep User | sed \'s/.*name="\([^"]*\)".*/\1/\'');
        if (!$apacheUser) {
            $extract = exec('cat /etc/apache2/envvars | grep APACHE_RUN_USER=');
            $apacheUser = substr($extract, strpos($extract, '=') + 1);
        }
        $apacheUser = $apacheUser ?: 'www-data';

        $parser->addOption(
            'user',
            [
                'short' => 'u',
                'help' => __("L'utilisateur système http ex: pour apache2: {0}", $apacheUser),
                'default' => $apacheUser
            ]
        );
        $parser->addSubcommand(
            'check-database',
            [
                'help' => __("vérifie la connection vers la base de données configuré")
            ]
        );
        $parser->addSubcommand(
            'get',
            [
                'help' => __("Visualise la valeur d'une clé"),
                'parser' => $parserGet,
            ]
        );
        $parserGet->addArgument(
            'path',
            [
                'help' => __("Chemin de la clé de configuration. ex: App.paths.data"),
                'required' => true,
            ]
        );
        $parserGet->addOption(
            'raw',
            [
                'help' => __("Affiche la valeur brute"),
                'parser' => $parserGet,
            ]
        );
        $parser->addSubcommand(
            'set',
            [
                'help' => __("Lance le shell en mode injection de configuration"),
                'parser' => $parserSet,
            ]
        );
        $parserSet->addArgument(
            'path',
            [
                'help' => __("Chemin de la clé de configuration. ex: App.paths.data"),
                'required' => true,
            ]
        );
        $parserSet->addArgument(
            'value',
            [
                'help' => __(
                    "Valeur à insérer. "
                    . "ex: 'true' = (bool)1, 'un mot' = (string)'un mot', '3.14' = (float)3.14"
                ),
                'required' => true,
            ]
        );
        $parserSet->addOption(
            'user',
            [
                'short' => 'u',
                'help' => __("L'utilisateur système http ex: pour apache2: {0}", $apacheUser),
                'default' => $apacheUser
            ]
        );
        $parser->addSubcommand(
            'all',
            [
                'help' => __("Visualise les clés de configurations"),
            ]
        );
        $parser->addSubcommand(
            'editor',
            [
                'help' => __("Editeur interactif de configuration"),
            ]
        );
        return $parser;
    }

    /**
     * Méthode principale
     */
    public function main()
    {
        $this->checkWritable();
        $local = $this->in(
            __d('configuration_shell', "Please select a locale"),
            null,
            'fr_FR'
        );
        I18n::setLocale($local);
        $this->nl();

        $config = [];
        $configPath = $this->configPath();

        $exist = file_exists($configPath);

        if ($exist) {
            $this->success(
                __d(
                    'configuration_shell',
                    "Le fichier existe déjà, les valeurs par défaut seront"
                    . " définies en fonction du fichier existant "
                    . "et les valeurs non configurables ici seront conservées."
                ),
                2
            );
            $config = json_decode(file_get_contents($configPath), true);
        }

        $config['App']['defaultLocale'] = $local;

        $this->configureDataPath($config);
        $this->configureDomain($config);
        $this->configureDatabase($config);
        $this->configureSalt($config);
        $this->configureRatchet($config);

        ksort($config);
        $this->createFile($configPath, json_encode($config, JSON_PRETTY_PRINT));
        $pathToLocalConfig = Configure::read('App.paths.path_to_local_config');
        $this->createFile($pathToLocalConfig, "<?php return '$configPath'; ?>");
    }

    /**
     * Affiche le contenu d'une clé de configuration
     * @param string $path
     * @throws Exception
     */
    public function get(string $path)
    {
        $config = $this->getConfig();
        $value = Hash::get(Hash::merge($config, Config::readAll()), $path);
        if (!isset($this->paramsShell['raw']) || is_array($value)) {
            $value = json_encode(
                $value,
                JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES
            );
        }
        $this->out($value);
    }

    /**
     * Défini une valeur sur une clé
     * @param string $path
     * @param string $value
     * @throws Exception
     */
    public function set(string $path, string $value)
    {
        $config = $this->getConfig();
        $this->checkWritable();
        if ($value === 'true') {
            $value = true;
        } elseif ($value === 'false') {
            $value = false;
        } elseif ($value === 'null') {
            $value = null;
        } elseif (preg_match('/^\d+\.\d*$/', $value)) {
            $value = (float)$value;
        } elseif (is_numeric($value)) {
            $value = (int)$value;
        }
        $this->out(
            __(
                "Précédente valeur: \n{0}",
                json_encode(
                    Hash::insert([], $path, Hash::get($config, $path)),
                    JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES
                )
            )
        );
        $config = Hash::insert($config, $path, $value);
        $this->out(__("Nouvelle valeur: \n{0}", json_encode(Hash::insert([], $path, $value))));

        ksort($config);
        $configPath = $this->getConfigPath();
        $json = json_encode($config, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        if (!$json) {
            $this->abortShell(__("Erreur lors du parsing JSON"));
        }
        Filesystem::begin();
        Filesystem::dumpFile($configPath, $json);
        Filesystem::commit();
    }

    /**
     * Affiche toute les clés de configuration
     */
    public function all()
    {
        $keys = array_keys(Hash::flatten(Hash::merge(Configure::read(), $this->getConfig())));
        sort($keys);
        foreach ($keys as $key) {
            $this->out($key);
        }
    }

    /**
     * Editeur interactif
     * @throws Exception
     */
    public function editor()
    {
        $this->checkWritable();
        $config = $this->getConfig();
        $this->out(
            __d(
                'configuration_shell',
                "Saisissez un chemin de configuration (ex: {0}) suivi par la valeur souhaitée (ex: {1}).
                    En cas d’ambiguïté sur le type de valeur, un choix vous sera proposé.
                    En cas de valeur vide, la suppression de la valeur vous sera proposée.",
                "App.defaultLocale",
                "fr_FR"
            )
        );
        do {
            do {
                $path = $this->in(
                    __d(
                        'configuration_shell',
                        "Veuillez indiquer un chemin de configuration à insérer, tapez {0} pour arréter.",
                        "stop"
                    )
                );
            } while (!$path);
            if ($path === 'stop') {
                break;
            }
            if ($value = Hash::get($config, $path)) {
                if (is_array($value)) {
                    $this->warn(
                        __d(
                            'configuration_shell',
                            "La valeur est un array, la seule action possible est la suppression"
                        )
                    );
                    $this->out(Debugger::exportVar($value));
                    $delete = $this->in(
                        __d('configuration_shell', "Voulez-vous retirer cette valeur ?"),
                        ['y', 'n'],
                        'n'
                    );
                    if ($delete === 'y') {
                        $config = Hash::remove($config, $path);
                    }
                    $continuer = $this->in(
                        __d('configuration_shell', "Voulez-vous saisir une autre valeur ?"),
                        ['y', 'n'],
                        'y'
                    );
                    continue;
                }
                $value = '(' . gettype($value) . ')' . $value;
                $this->out(
                    __d(
                        'configuration_shell',
                        "<info>Une valeur existe déjà</info> : {0}",
                        '<warning>' . $value . '</warning>'
                    )
                );
            }
            $value = $this->in(__d('configuration_shell', "Veuillez indiquer une valeur"));
            if ($value === 'true' || $value === 'false') {
                $type = $this->in(
                    __d('configuration_shell', "Insérer une valeur de type {0} ?", "boolean"),
                    ['boolean', 'string'],
                    'boolean'
                );
                if ($type === 'boolean') {
                    $value = $value === 'true';
                }
            } elseif ($value === 'null') {
                $type = $this->in(
                    __d('configuration_shell', "Insérer une valeur de type {0} ?", "null"),
                    ['null', 'string'],
                    'null'
                );
                if ($type === 'null') {
                    $value = null;
                }
            } elseif (preg_match('/^\d+\.\d*$/', $value)) {
                $type = $this->in(
                    __d('configuration_shell', "Insérer une valeur de type {0} ?", "float"),
                    ['float', 'string'],
                    'float'
                );
                if ($type === 'float') {
                    $value = (float)$value;
                }
            } elseif (is_numeric($value)) {
                $type = $this->in(
                    __d('configuration_shell', "Insérer une valeur de type {0} ?", "integer"),
                    ['integer', 'string'],
                    'integer'
                );
                if ($type === 'integer') {
                    $value = (int)$value;
                }
            }
            $delete = false;
            if ($value === '' || $value === null) {
                $delete = $this->in(
                    __d('configuration_shell', "Voulez-vous retirer cette valeur ?"),
                    ['y', 'n'],
                    'n'
                );
            }
            if ($delete && $delete !== 'n') {
                $config = Hash::remove($config, $path);
            } else {
                $config = Hash::insert($config, $path, $value);
                $this->out(
                    json_encode(
                        Hash::insert([], $path, $value),
                        JSON_UNESCAPED_SLASHES
                    )
                );
            }

            $continuer = $this->in(
                __d('configuration_shell', "Voulez-vous saisir une autre valeur ?"),
                ['y', 'n'],
                'y'
            );
        } while ($continuer === 'y');
        ksort($config);
        $this->createFileWithDefault(
            $this->getConfigPath(),
            json_encode($config, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES),
            'y'
        );
    }

    /**
     * Défini un chemin inscriptible pour le fichier de configuration
     *
     * @return string
     */
    private function configPath(): string
    {
        $pathToLocalConfig = Configure::read('App.paths.path_to_local_config');
        $defaultPath = is_readable($pathToLocalConfig)
            ? include $pathToLocalConfig
            : Configure::read('App.paths.local_config', CONFIG . 'app_local.json');
        if ($defaultPath === 1) {
            $defaultPath = Configure::read('App.paths.local_config', CONFIG . 'app_local.json');
        }
        do {
            $configPath = $this->in(
                __d('configuration_shell', "Indiquez le chemin qu'aura le fichier de configuration"),
                null,
                $defaultPath
            );
            $writable = is_writable(
                file_exists($configPath) ? $configPath : dirname($configPath)
            );
            if (!$writable) {
                $this->err(__d('configuration_shell', "Le chemin indiqué n'est pas inscriptible"));
            }
        } while ($writable === false);
        return realpath($configPath)
            ?: realpath(dirname($configPath)) . DS . basename($configPath);
    }

    /**
     * Configuration de la base de données
     *
     * @param array $config
     * @throws Exception
     */
    private function configureDatabase(array &$config)
    {
        $default = include CONFIG . 'app_default.php';
        $connected = false;

        foreach (self::$datasources as $datasource) {
            $connected = false;
            do {
                $choice = $this->in(
                    __d('configuration_shell', "Voulez-vous configurer le datasource [{0}] ?", $datasource),
                    ['y', 'n'],
                    'y'
                );
                if ($choice === 'n') {
                    break;
                }

                // la config précédente SINON la config de ce datasource SINON la config du datasource default
                $values = Hash::get(
                    $config,
                    "Datasources.$datasource",
                    Hash::get(
                        $default,
                        "Datasources.default",
                        Hash::get($default, "Datasources.$datasource", [])
                    )
                ) + [
                    'className' => \Cake\Database\Connection::class,
                    'driver' => 'Cake\\Database\\Driver\\Postgres',
                    'host' => 'localhost',
                    'username' => '',
                    'password' => '',
                    'database' => '',
                ];
                $this->configureDatabaseDriver($datasource, $values['driver'], $default);
                $this->configureDatabaseHost($datasource, $values['host'], $default);
                $this->configureDatabaseUsername($datasource, $values['username'], $default);
                $this->configureDatabasePassword($datasource, $values['password'], $default);
                $this->configureDatabaseDatabase($datasource, $values['database'], $default);

                ConnectionManager::drop($datasource);
                ConnectionManager::setConfig($datasource, $values);

                $connected = $this->checkConnection($datasource);
                if (!$connected) {
                    $this->err(__d('configuration_shell', "Connection failed!"));
                } else {
                    $this->success(
                        __d(
                            'configuration_shell',
                            "La connexion à la base de données a été effectué avec succès !"
                        ),
                        2
                    );
                }
            } while ($connected === false);

            if ($choice === 'y') {
                $config['Datasources'][$datasource] = $default['Datasources'][$datasource];
            }
        }

        if ($connected || $this->checkConnection('default')) {
            $this->success(
                __d(
                    'configuration_shell',
                    "La connexion à la base de données a été effectué avec succès !"
                ),
                2
            );
        } else {
            $this->err(__d('configuration_shell', "Impossible de se connecter à la base de données"));
        }
    }

    /**
     * Configure la valeur "driver" du datasource défini
     *
     * @param string $datasource
     * @param string $driver
     * @param array  $config
     */
    private function configureDatabaseDriver(string $datasource, string &$driver, array &$config)
    {
        do {
            $driver = $this->in(
                __d('configuration_shell', "Database driver"),
                null,
                $driver ?: 'Cake\\Database\\Driver\\Postgres'
            );
        } while (!class_exists($driver));
        $config = Hash::insert(
            $config,
            "Datasources.$datasource.driver",
            $driver
        );
        echo PHP_EOL;
    }

    /**
     * Configure la valeur "host" du datasource défini
     *
     * @param string $datasource
     * @param string $host
     * @param array  $config
     */
    private function configureDatabaseHost(string $datasource, string &$host, array &$config)
    {
        do {
            $host = $this->in(
                __d('configuration_shell', "Database host"),
                null,
                $host ?: 'localhost'
            );
        } while (empty($host));
        $config = Hash::insert(
            $config,
            "Datasources.$datasource.host",
            $host
        );
        echo PHP_EOL;
    }

    /**
     * Configure la valeur "username" du datasource défini
     *
     * @param string $datasource
     * @param string $username
     * @param array  $config
     */
    private function configureDatabaseUsername(string $datasource, string &$username, array &$config)
    {
        do {
            $username = $this->in(
                __d('configuration_shell', "Database username"),
                null,
                $username ?: null
            );
        } while (empty($username));
        $config = Hash::insert(
            $config,
            "Datasources.$datasource.username",
            $username
        );
        echo PHP_EOL;
    }

    /**
     * Configure la valeur "password" du datasource défini
     *
     * @param string $datasource
     * @param string $password
     * @param array  $config
     * @throws Exception
     */
    private function configureDatabasePassword(string $datasource, string &$password, array &$config)
    {
        do {
            $password = $this->enterPassword(__d('configuration_shell', "Database password"));
        } while (empty($password));
        $config = Hash::insert(
            $config,
            "Datasources.$datasource.password",
            $password
        );
        echo PHP_EOL;
    }

    /**
     * Configure la valeur "database" du datasource défini
     *
     * @param string $datasource
     * @param string $database
     * @param array  $config
     */
    private function configureDatabaseDatabase(string $datasource, string &$database, array &$config)
    {
        do {
            $database = $this->in(
                __d('configuration_shell', "Database name"),
                null,
                $database ?: null
            );
        } while (empty($database));
        $config = Hash::insert(
            $config,
            "Datasources.$datasource.database",
            $database
        );
        echo PHP_EOL;
    }

    /**
     * Vérifie que la base de données est accessible à partir des paramètres
     * saisis dans la configuration
     *
     * @param string $datasource
     * @return boolean
     */
    private function checkConnection(string $datasource): bool
    {
        try {
            $conn = ConnectionManager::get($datasource);
            if ($conn instanceof Connection) {
                $conn->query('select 1');
            }
            return true;
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            return false;
        }
    }

    /**
     * Configuration de la clé de sécurité
     *
     * @param array $config
     */
    private function configureSalt(array &$config)
    {
        if (!empty($config['Security']['salt'])) {
            $keep = $this->in(
                __d('configuration_shell', "Une clé de sécurité existe déjà, voulez-vous la conserver ?"),
                ['y', 'n'],
                'y'
            );
            $confirm = 'y';
            if ($keep === 'n') {
                $confirm = $this->in(
                    __d(
                        'configuration_shell',
                        "Il est vivement conseillé de conserver la clé, confirmez-vous sa suppression ?"
                    ),
                    ['y', 'n'],
                    'n'
                );
            }
            if ($keep === 'y' || $confirm === 'n') {
                $this->success(
                    __d('configuration_shell', "La clé de sécurité a bien été conservée"),
                    2
                );
                return;
            }
        }

        while (empty($key) || !is_string($key)) {
            $options = $this->in(
                __d('configuration_shell', "Configuration de la clé de sécurité [(s)aisir, (g)énérer]"),
                ['s', 'g'],  // (s)et or (g)enerate
                'g'
            );
            switch ($options) {
                case 's':
                    $key = $this->in(
                        __d('configuration_shell', "Veuillez rentrer une clé de sécurité :"),
                        null,
                        Hash::get($config, 'Security.salt')
                    );
                    break;
                case 'g':
                    $key = hash('sha256', ROOT . php_uname() . microtime(true));
                    break;
            }
        }
        $config['Security']['salt'] = $key;
        $this->success(__d('configuration_shell', "La clé de sécurité a bien été définie"), 2);
    }

    /**
     * Configuration du domaine
     *
     * @param array $config
     */
    private function configureDomain(array &$config)
    {
        stream_context_set_default(
            [
                'ssl' => [
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                ],
            ]
        );
        do {
            /** @noinspection HttpUrlsUsage */
            $domain = $this->in(
                __d(
                    'configuration_shell',
                    "Veuillez indiquer l'url complète de l'application (ex: http://versae.fr)"
                ),
                null,
                Hash::get($config, 'App.fullBaseUrl')
            );
            $continue = false;
            $headers = @get_headers($domain);
            if (!$headers || $headers[0] === 'HTTP/1.1 404 Not Found') {
                $continue = true;
                $break = $this->in(
                    __d(
                        'configuration_shell',
                        "L'url mentionnée n'a pas fonctionné. Voulez-vous quand même saisir cette valeur ?"
                    ),
                    ['y', 'n'],
                    'y'
                );
                if ($break === 'y') {
                    break;
                }
            }
        } while ($continue);
        $config['App']['fullBaseUrl'] = $domain;

        $this->success(__d('configuration_shell', "Le nom de domaine indiqué est valide"), 2);
    }

    /**
     * Configuration de ratchet
     *
     * @param array $config
     */
    private function configureRatchet(array &$config)
    {
        if (!$default = Hash::get($config, 'Ratchet.connect')) {
            $p = parse_url(Hash::get($config, 'App.fullBaseUrl'));
            $default = sprintf('wss://%s/wss', $p['host'] ?? '');
        }
        $config['Ratchet']['connect'] = $this->in(
            __d('configuration_shell', "Veuillez indiquer le websocket de ratchet"),
            null,
            $default
        );
    }

    /**
     * Configuration du chemin vers data (données de l'application ne
     * faisant pas partie des sources)
     *
     * @param array $config
     */
    private function configureDataPath(array &$config)
    {
        do {
            $configDataPath = $this->in(
                __d('configuration_shell', "Veuillez indiquer le chemin vers data"),
                null,
                Hash::get($config, 'App.paths.data') ?: ROOT . DS . 'data'
            );
            $path = realpath($configDataPath);
            $writable = is_writable($path);
            if (!$writable) {
                $this->err(__d('configuration_shell', "Le chemin indiqué n'est pas inscriptible"));
            }
        } while ($writable === false);
        $config['App']['paths']['data'] = $configDataPath;
    }

    /**
     * Vérifie la connection vers la base de données configuré
     * @return int 0: success - 1: error
     */
    public function checkDatabase(): int
    {
        if ($this->checkConnection('default')) {
            $this->success('connected');
            return self::CODE_SUCCESS;
        } else {
            $this->err('not connected');
            return self::CODE_ERROR;
        }
    }

    /**
     * Donne le chemin vers le fichier de configuration
     * @return string
     */
    private function getConfigPath(): string
    {
        return Config::getPathToLocal();
    }

    /**
     * Donne la config
     */
    private function getConfig(): array
    {
        $pathToLocalConfig = $this->getConfigPath();
        if ($pathToLocalConfig) {
            return json_decode(file_get_contents($pathToLocalConfig), true);
        }
        return [];
    }

    /**
     * Vérifie que la config est modifiable
     */
    private function checkWritable()
    {
        $httpUser = $this->param('user');
        $currentUser = posix_getpwuid(posix_geteuid());
        $pathToLocalConfig = Configure::read('App.paths.path_to_local_config');

        if ($currentUser['name'] !== $httpUser) {
            $this->abortShell(
                __d(
                    'configuration_shell',
                    "This script must be called by {0} but was called by {1}",
                    $httpUser,
                    $currentUser['name']
                )
            );
        }
        if ((!is_file($pathToLocalConfig) && !is_writable(dirname($pathToLocalConfig)))) {
            $this->abortShell(
                __(
                    "Le dossier config doit être inscriptible par {0} pour utiliser ce shell",
                    $currentUser['name']
                )
            );
        } elseif (is_file($pathToLocalConfig) && !is_writable($pathToLocalConfig)) {
            $this->abortShell(
                __(
                    "Le fichier config/path_to_local.php doit être inscriptible par {0} pour utiliser ce shell",
                    $currentUser['name']
                )
            );
        }
        $configPath = $this->getConfigPath();
        if (empty($configPath) || !is_file($configPath) || !is_writeable($configPath)) {
            $this->abortShell(
                __(
                    "Le fichier de configuration n'existe pas ou n'est pas inscriptible par {0}",
                    $currentUser['name']
                )
            );
        }
    }
}
