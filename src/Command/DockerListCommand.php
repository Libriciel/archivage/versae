<?php

/**
 * Versae\Command\DockerKillCommand
 */

namespace Versae\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Exception;
use Versae\Model\Table\BeanstalkWorkersTable;

/**
 * Liste les workers par commande socket (docker)
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 * @property BeanstalkWorkersTable BeanstalkWorkers
 */
class DockerListCommand extends Command
{
    use DockerCommandTrait;

    /**
     * @var Arguments
     */
    private $args;

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'docker list';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->setDescription(
            __("Liste les workers par commande socket (docker)")
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        try {
            $this->connectToPortainer();
            $response = $this->client(
                'get',
                'api/endpoints/1/docker/containers/json',
                json_encode(['all' => true])
            );
            $json = json_decode($response->getStringBody(), true);
        } catch (Exception $e) {
            $io->error($e->getMessage());
            throw $e;
        }

        $this->BeanstalkWorkers = $this->fetchTable('BeanstalkWorkers');
        // synchroniser maintenant entraine une boucle de sync car le shell est appelé dans la synchro
        $this->BeanstalkWorkers->sync = true;
        foreach ($json as $container) {
            $containerId = ($container['Names'][0] ?? false) . ' ' . $container['Id'] ?? false;
            $worker = $this->BeanstalkWorkers->find()
                ->where(['hostname LIKE' => substr($container['Id'] ?? '', 0, 12) . '%'])
                ->first();
            if ($worker) {
                $io->out(sprintf("Worker %s (id=%d): %s", $worker->get('name'), $worker->id, $containerId));
            } else {
                $io->out(sprintf("Container %s", $containerId));
            }
        }
        $io->out('done');
    }
}
