<?php

/**
 * Versae\Form\ImportForm
 */

namespace Versae\Form;

use Cake\Datasource\EntityInterface;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\ORM\Exception\PersistenceFailedException;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Exception;
use Versae\Exception\GenericException;
use Versae\Model\Table\FormsTable;

/**
 * Formulaire d'import de formulaire
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2023, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ImportForm extends Form
{
    /**
     * Dernière entité importé
     * @var EntityInterface|null
     */
    public $lastImport = null;

    /**
     * A hook method intended to be implemented by subclasses.
     *
     * You can use this method to define the schema using
     * the methods on Cake\Form\Schema, or loads a pre-defined
     * schema from a concrete class.
     *
     * @param Schema $schema The schema to customize.
     * @return Schema The schema to use.
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        $schema->addField('transferring_agencies', ['type' => 'array']);
        $schema->addField('fileuploads', ['type' => 'array']);
        $schema->addField('archiving_system_id', ['type' => 'integer']);
        $schema->addField('import', ['type' => 'string']);
        $schema->addField('check', ['type' => 'string']);
        $schema->addField('version_note', ['type' => 'text']);
        $schema->addField('name', ['type' => 'string']);

        return $schema;
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->add(
            'check',
            'valid',
            [
                'rule' => function ($value, $context) {
                    $fileuploadId = Hash::get($context, 'data.fileuploads.id');
                    if (!$fileuploadId) {
                        return __("Aucun fichier n'a été sélectionné");
                    }
                    try {
                        $checkArchivalAgency = $value === 'add';
                        $exists = $this->formAlreadyExists(
                            $fileuploadId,
                            $checkArchivalAgency
                        );
                    } catch (Exception $e) {
                        return $e->getMessage();
                    }
                    if ($exists && $value === 'strict') {
                        return __("Ce formulaire existe déjà");
                    }
                    return true;
                }
            ]
        );
        return $validator;
    }

    /**
     * Hook method to be implemented in subclasses.
     *
     * Used by `execute()` to execute the form's action.
     *
     * @param array $data Form data.
     * @return bool
     * @throws Exception
     */
    protected function _execute(array $data): bool
    {
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        /** @var FormsTable $Forms */
        $Forms = TableRegistry::getTableLocator()->get('Forms');
        $upload = $Fileuploads->find()
            ->where(['Fileuploads.id' => Hash::get($data, 'fileuploads.id')])
            ->contain(['Users' => ['OrgEntities' => ['ArchivalAgencies']]])
            ->first();
        $json = json_decode(
            file_get_contents($upload->get('path')),
            true
        );
        if ($data['import'] === 'last') {
            $json = [end($json)];
        }
        if (!empty($data['name'])) {
            foreach ($json as &$jsonData) {
                $jsonData['name'] = $data['name'];
            }
            unset($jsonData);
        }

        if ($data['check'] === 'new') {
            $identifier = uuid_create(UUID_TYPE_TIME);
            $version_number = 1;
        } else {
            $identifier = current($json)['identifier'];
            $form = $Forms->find()
                ->where(['Forms.identifier' => $identifier])
                ->orderDesc('version_number')
                ->first();
            if ($form && $form->get('version_number')) {
                $version_number = $form->get('version_number') + 1;
            } else {
                $version_number = 1;
            }
        }
        if (!empty($data['name']) && !$this->checkUniqueName(current($json)['name'], $identifier, $data)) {
            return false;
        }
        $conn = $Forms->getConnection();
        $conn->begin();
        foreach ($json as $form) {
            $form['identifier'] = $identifier;
            $form['version_number'] = $version_number;
            $form['org_entity_id'] = $data['org_entity_id'];
            $form['transferring_agencies'] = ['_ids' => $data['transferring_agencies']];
            $form['archiving_system_id'] = $data['archiving_system_id'];
            $form['state'] = $Forms->initialState;
            $form['version_note'] = $data['version_note'];
            try {
                $Forms->import($form);
            } catch (PersistenceFailedException $e) {
                foreach ($e->getAttributes() as $attr) {
                    if (preg_match('/(\w+).(\w+): "(.*)"/', $attr, $m)) {
                        $this->_errors[$m[1]][$m[2]] = $m[3];
                    }
                }
                return false;
            } catch (Exception $e) {
                $conn->rollback();
                throw $e;
            }
            $version_number++;
            $this->lastImport = $Forms->lastImport;
        }
        $conn->commit();
        return parent::_execute($data);
    }

    /**
     * Vérifi que le fichier d'import est valide et qu'il ne désigne pas un
     * formulaire existant ou sur un autre service d'archives
     * @param int  $fileupload_id
     * @param bool $checkArchivalAgency
     * @return bool
     */
    public function formAlreadyExists($fileupload_id, bool $checkArchivalAgency): bool
    {
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $upload = $Fileuploads->find()
            ->where(['Fileuploads.id' => $fileupload_id])
            ->contain(['Users' => ['OrgEntities' => ['ArchivalAgencies']]])
            ->first();
        if (!$upload) {
            throw new GenericException(__("Le fichier d'upload n'a pas été trouvé"));
        }
        $json = json_decode(
            file_get_contents($upload->get('path')),
            true
        );
        if (!$json) {
            throw new GenericException(
                __("Le fichier uploadé n'est pas un fichier json valide")
            );
        }
        if (
            !$json[0]
            || !Hash::get($json, '0.version_number')
            || !Hash::get($json, '0.identifier')
        ) {
            throw new GenericException(
                __("Le fichier uploadé ne semble pas être un export de formulaire")
            );
        }
        $Forms = TableRegistry::getTableLocator()->get('Forms');
        $form = $Forms->find()
            ->where(['identifier' => Hash::get($json, '0.identifier')])
            ->first();
        if (!$form) {
            return false;
        }

        if ($checkArchivalAgency) { // vrai si "Options d'import" == 'add'
            $Forms = TableRegistry::getTableLocator()->get('Forms');
            $formEditing = $Forms->find()
                ->where(
                    [
                        'identifier' => Hash::get($json, '0.identifier'),
                        'state' => FormsTable::S_EDITING,
                    ]
                )
                ->count();
            if ($formEditing) {
                throw new GenericException(
                    __("Vous ne pouvez pas importer sur ce formulaire car une édition est en cours")
                );
            }
            $archivalAgencyId = Hash::get($upload, 'user.org_entity.archival_agency.id');
            if ($archivalAgencyId !== $form->get('org_entity_id')) {
                throw new GenericException(
                    __("Ce formulaire existe mais sur un autre service d'archives")
                );
            }
        }
        return true;
    }

    /**
     * Vérifi l'unicité du nom au nivau du formulaire
     * (évite une erreur 500 sur la validation au save)
     * @param string $name
     * @param string $identifier
     * @param array  $data
     * @return bool
     */
    private function checkUniqueName(string $name, string $identifier, array $data): bool
    {
        $Forms = TableRegistry::getTableLocator()->get('Forms');
        $exists = $Forms->exists(
            [
                'org_entity_id' => $data['org_entity_id'],
                'name' => $name,
                'identifier !=' => $identifier,
            ]
        );
        if ($exists) {
            $this->_data['name'] = $name;
            $this->_errors['name']['unique']
                = __("Un formulaire avec le même nom existe déjà");
        }
        return !$exists;
    }
}
