<?php

/**
 * Versae\Form\AddWorkerForm
 */

namespace Versae\Form;

use Beanstalk\Utility\Beanstalk;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;
use Exception;

/**
 * Formulaire de job Beanstalk
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2023, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AddWorkerForm extends Form
{

    /**
     * A hook method intended to be implemented by subclasses.
     *
     * You can use this method to define the schema using
     * the methods on Cake\Form\Schema, or loads a pre-defined
     * schema from a concrete class.
     *
     * @param Schema $schema The schema to customize.
     * @return Schema The schema to use.
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        $schema->addField('tube', ['type' => 'string']);
        $schema->addField('keep-alive', ['type' => 'boolean']);

        return $schema;
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->notEmptyString('tube');
        $validator->add(
            'tube',
            'validFormat',
            [
                'rule' => [
                    'custom',
                    "/^[\w-]+$/i",
                ],
                'message' => __("Ce nom de tube est mal formaté")
            ]
        );
        return $validator;
    }

    /**
     * Hook method to be implemented in subclasses.
     *
     * Used by `execute()` to execute the form's action.
     *
     * @param array $data Form data.
     * @return bool
     * @throws Exception
     */
    protected function _execute(array $data): bool
    {
        $message = sprintf(
            'create-worker:%s%s',
            $data['tube'],
            !empty($data['keep-alive']) ? ':keep-alive' : ''
        );
        Beanstalk::getInstance()
            ->socketEmit($message);
        return true;
    }
}
