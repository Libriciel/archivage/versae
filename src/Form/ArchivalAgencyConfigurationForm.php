<?php

/**
 * Versae\Form\ArchivalAgencyConfigurationForm
 */

namespace Versae\Form;

use Versae\Model\Table\ConfigurationsTable;
use Versae\Model\Table\FileuploadsTable;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Formulaire d'édition de la configuration d'un service d'archives
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchivalAgencyConfigurationForm extends Form
{
    /**
     * @var EntityInterface[] entités configurés
     */
    public array $entities = [];
    /**
     * @var ConfigurationsTable
     */
    private Table $Configurations;
    /**
     * @var FileuploadsTable
     */
    private Table $Fileuploads;
    /**
     * @var int service d'archives lié au formulaire
     */
    private int $archivalAgencyId;

    /**
     * Donne une instance avec service d'archives lié
     * @param int $archivalAgencyId
     * @return static
     */
    public static function create(int $archivalAgencyId): self
    {
        $instance = new self();
        $instance->archivalAgencyId = $archivalAgencyId;
        $loc = TableRegistry::getTableLocator();
        $instance->Configurations = $loc->get('Configurations');
        $instance->Fileuploads = $loc->get('Fileuploads');

        $query = $instance->Configurations->find()
            ->where(['org_entity_id' => $archivalAgencyId]);
        /** @var EntityInterface $entity */
        foreach ($query as $entity) {
            $instance->entities[$entity->get('name')] = $entity;
            $instance->_data[$entity->get('name')] = $entity->get('setting');
        }

        return $instance;
    }

    /**
     * Donne les options calculés
     * @return array
     */
    public function getOptions(): array
    {
        return [];
    }

    /**
     * A hook method intended to be implemented by subclasses.
     *
     * You can use this method to define the schema using
     * the methods on Cake\Form\Schema, or loads a pre-defined
     * schema from a concrete class.
     *
     * @param Schema $schema The schema to customize.
     * @return Schema The schema to use.
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        // champs génériques (sera écrasé par les appels qui suivent)
        foreach (ConfigurationsTable::KEYS as $name) {
            $schema->addField($name, ['type' => 'string']);
        }

        $schema->addField(
            ConfigurationsTable::CONF_BACKGROUND_SIZE,
            ['type' => 'integer', 'default' => 75]
        );
        $schema->addField(
            ConfigurationsTable::CONF_MAIL_HTML_SIGNATURE,
            ['type' => 'text']
        );
        $schema->addField(
            ConfigurationsTable::CONF_MAIL_TEXT_SIGNATURE,
            ['type' => 'text']
        );
        return $schema;
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        foreach (ConfigurationsTable::KEYS as $name) {
            $validator->scalar($name)
                ->allowEmptyString($name);
        }
        $validator->integer(ConfigurationsTable::CONF_BACKGROUND_SIZE)
            ->add(
                ConfigurationsTable::CONF_BACKGROUND_SIZE,
                'min',
                [
                    'rule' => fn ($value) => !$value || $value >= 50,
                    'message' => __("Valeur minimale: {0}", 50),
                ]
            )
            ->add(
                ConfigurationsTable::CONF_BACKGROUND_SIZE,
                'max',
                [
                    'rule' => fn ($value) => !$value || $value <= 150,
                    'message' => __("Valeur maximale: {0}", 150),
                ]
            );

        $validator->add(
            'fileuploads.id',
            'valid',
            [
                'rule' => function ($value) {
                    if ($value === 'local') {
                        return true;
                    }
                    $fileupload = $this->Fileuploads->find()
                        ->innerJoinWith('Users')
                        ->innerJoinWith('Users.OrgEntities')
                        ->innerJoinWith('Users.OrgEntities.ArchivalAgencies')
                        ->where(
                            [
                                'Users.id' => $value,
                                'ArchivalAgencies.id' => $this->archivalAgencyId,
                            ]
                        )
                        ->first();
                    return $fileupload
                        && is_readable($fileupload->get('path'))
                        && str_contains(mime_content_type($fileupload->get('path')), 'image/');
                }
            ]
        );

        return $validator;
    }

    /**
     * Hook method to be implemented in subclasses.
     *
     * Used by `execute()` to execute the form's action.
     *
     * @param array $data Form data.
     * @return bool
     * @throws Exception
     */
    protected function _execute(array $data): bool
    {
        foreach ($data as $key => $value) {
            if (!in_array($key, ConfigurationsTable::KEYS)) {
                continue;
            }
            $entity = $this->Configurations->find()
                ->where(
                    $confData = [
                        'org_entity_id' => $this->archivalAgencyId,
                        'name' => $key,
                    ]
                )
                ->first();
            $hasValue = $value || $value === '0';
            if ($entity) {
                if ($hasValue) {
                    $this->Configurations->patchEntity($entity, ['setting' => $value]);
                    $success = $this->Configurations->save($entity);
                } else {
                    $success = $this->Configurations->delete($entity);
                }
            } elseif ($hasValue) {
                $entity = $this->Configurations->newEntity(
                    $confData + ['setting' => $value]
                );
                $success = $this->Configurations->save($entity);
            } else {
                $success = true;
            }
            if (!$success) {
                return false;
            }
        }

        $this->handleLogo($data);
        return true;
    }

    /**
     * Gestion du logo
     * @param array $data du formulaire
     * @return false|void
     * @throws Exception
     */
    private function handleLogo(array $data)
    {
        $fileupload_id = Hash::get($data, 'fileuploads.id');
        $entity = $this->Configurations->find()
            ->where(
                $logoData = [
                    'org_entity_id' => $this->archivalAgencyId,
                    'name' => ConfigurationsTable::CONF_BACKGROUND_IMAGE,
                ]
            )
            ->first();
        // si fileupload_id est un integer, il sagit d'un nouveau logo
        if (isset($fileupload_id) && is_numeric($fileupload_id)) {
            $fileupload = $this->Fileuploads->get($fileupload_id);
            $savedataDir = rtrim(Configure::read('App.paths.data'), DS);
            $webrootDir = rtrim(
                Configure::read('OrgEntities.public_dir', WWW_ROOT),
                DS
            );
            // Génère un nouveau répertoire unique
            do {
                $randdir = bin2hex(random_bytes(16));
                $setting = DS . implode(
                    DS,
                    ['org-entity-data', $this->archivalAgencyId, $randdir, $fileupload->get('name')]
                );
                $targetPath = $savedataDir . $setting;
            } while (is_file($targetPath));

            // copy du fichier
            Filesystem::copy($fileupload->get('path'), $targetPath);
            Filesystem::copy($fileupload->get('path'), $webrootDir . $setting);

            // Patch de l'entité
            if (!$entity) {
                $entity = $this->Configurations->newEmptyEntity();
            } else {
                $oldTargetPath = $entity->get('setting');
                if (is_file($savedataDir . $oldTargetPath)) {
                    Filesystem::remove($savedataDir . $oldTargetPath);
                }
                if (is_file($webrootDir . $oldTargetPath)) {
                    Filesystem::remove($webrootDir . $oldTargetPath);
                }
            }
            $this->Configurations->patchEntity($entity, $logoData + ['setting' => $setting]);
            if (!$this->Configurations->save($entity)) {
                return false;
            }
            $this->Fileuploads->delete($fileupload);
        } elseif (!isset($fileupload_id) && $entity) {
            $this->Configurations->delete($entity);
            unset($entity);
        }
        if (empty($entity)) {
            $publicDir = rtrim(Configure::read('App.paths.data'), DS);
            $webrootDir = rtrim(
                Configure::read('OrgEntities.public_dir', WWW_ROOT),
                DS
            );
            $target = DS . 'org-entity-data' . DS . $this->archivalAgencyId . DS . 'background.css';
            if (is_file($publicDir . $target)) {
                Filesystem::remove($publicDir . $target);
            }
            if (is_file($webrootDir . $target)) {
                Filesystem::remove($webrootDir . $target);
            }
        } else {
            $bgSize = $data[ConfigurationsTable::CONF_BACKGROUND_SIZE] ?? 75;
            $this->generateBackgroundCss($entity->get('setting'), $bgSize);
        }
    }

    /**
     * Génère le css pour le background-image du logo client
     * @param string $imageUrl
     * @param int    $bgSize
     * @throws Exception
     */
    private function generateBackgroundCss(string $imageUrl, int $bgSize = 75)
    {
        $fullUrl = addcslashes(Router::url($imageUrl, true), "'");
        $css = <<<EOT
#sidebar-container .list-group:before{
    content: ' ';
    top: 0;
    left: 0;
    right: 0;
    width: 100%;
    height: {$bgSize}px;
    background-image: url('$fullUrl');
    background-color: #ffffff;
    background-position: center center;
    background-repeat: no-repeat;
    background-size: contain;
    border-bottom: 1px solid #ccc;
}
EOT;
        $publicDir = rtrim(Configure::read('App.paths.data'), DS);
        $webrootDir = rtrim(Configure::read('OrgEntities.public_dir', WWW_ROOT), DS);
        $target = DS . 'org-entity-data' . DS . $this->archivalAgencyId . DS . 'background.css';
        Filesystem::dumpFile($publicDir . $target, $css);
        Filesystem::dumpFile($webrootDir . $target, $css);
    }
}
