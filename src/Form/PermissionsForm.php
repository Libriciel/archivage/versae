<?php

/**
 * Versae\Form\PermissionsForm
 */

namespace Versae\Form;

use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\Component\AclComponent;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Http\Exception\BadRequestException;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Exception;

/**
 * Formulaire d'édition des permissions
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class PermissionsForm extends Form
{
    /**
     * Donne la liste des permissions sous la forme:
     *                                      ["controller_action" => "-1|0|1"]
     * @param int $aro_id
     * @return array
     */
    public function list($aro_id)
    {
        $perms = [];
        foreach ($this->getSchema()->fields() as $field) {
            $perms[$field] = '0';
        }
        $loc = TableRegistry::getTableLocator();
        $Permissions = $loc->get('ArosAcos');
        $Acos = $loc->get('Acos');
        $query = $Permissions->find()
            ->where(
                [
                    'aro_id' => $aro_id,
                    'OR' => [
                        '_create !=' => '0',
                        '_read !=' => '0',
                        '_update !=' => '0',
                        '_delete !=' => '0',
                    ]
                ]
            )
            ->contain(['Acos']);
        $controllers = $Acos->find()
            ->where(['alias' => 'controllers', 'model' => 'root'])
            ->firstOrFail();
        $apis = $Acos->find()
            ->where(['alias' => 'api', 'model' => 'root'])
            ->firstOrFail();
        $permControllers = (clone $query)
            ->where(
                [
                    'Acos.lft >=' => $controllers->get('lft'),
                    'Acos.rght <=' => $controllers->get('rght'),
                ]
            );
        $permApis = (clone $query)
            ->where(
                [
                    'Acos.lft >=' => $apis->get('lft'),
                    'Acos.rght <=' => $apis->get('rght'),
                ]
            );
        $this->listPerms($permControllers, 'controllers', $perms);
        $this->listPerms($permApis, 'api', $perms);

        return $perms;
    }

    /**
     * Donne un array représentant les permissions
     * @param Query  $permissions
     * @param string $category
     * @param array  $perms
     */
    private function listPerms(
        Query $permissions,
        string $category,
        array &$perms
    ) {
        /* @var EntityInterface $permission */
        foreach ($permissions as $permission) {
            /* @var EntityInterface $aco */
            $aco = $permission->get('aco');
            $model = $aco->get('model');
            $alias = $aco->get('alias');
            if ($model === $category) {
                $perms[$category . '.' . $alias . '.create'] = $permission->get('_create');
                $perms[$category . '.' . $alias . '.read'] = $permission->get('_read');
                $perms[$category . '.' . $alias . '.update'] = $permission->get('_update');
                $perms[$category . '.' . $alias . '.delete'] = $permission->get('_delete');
            } else {
                $key = $category . '.' . $aco->get('model') . '.action.' . $aco->get('alias');
                $perms[$key] = $permission->get('_create');
            }
        }
    }

    /**
     * A hook method intended to be implemented by subclasses.
     *
     * You can use this method to define the schema using
     * the methods on Cake\Form\Schema, or loads a pre-defined
     * schema from a concrete class.
     *
     * @param Schema $schema The schema to customize.
     * @return Schema The schema to use.
     * @throws Exception
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        $path = Configure::read('App.paths.controllers_rules');
        if (!is_readable($path)) {
            throw new Exception(
                __(
                    "{0} n'a pas accès à controllers.json en lecture, impossible de modifier les droits!",
                    get_current_user()
                )
            );
        }
        $controllers = json_decode(file_get_contents($path));
        $basePath = Configure::read('App.namespace') . '\\Controller\\';
        foreach ($controllers as $controller => $actions) {
            $classname = $basePath . $controller . 'Controller';
            if (!class_exists($classname)) {
                continue;
            }
            $schema->addField(
                'controllers.' . $controller . '.create',
                'string'
            );
            $schema->addField('controllers.' . $controller . '.read', 'string');
            $schema->addField(
                'controllers.' . $controller . '.update',
                'string'
            );
            $schema->addField(
                'controllers.' . $controller . '.delete',
                'string'
            );
            foreach ($actions as $action => $params) {
                $schema->addField(
                    'controllers.' . $controller . '.action.' . $action,
                    'string'
                );
            }
            if (is_subclass_of($classname, ApiInterface::class)) {
                $schema->addField('api.' . $controller . '.create', 'string');
                $schema->addField('api.' . $controller . '.read', 'string');
                $schema->addField('api.' . $controller . '.update', 'string');
                $schema->addField('api.' . $controller . '.delete', 'string');
            }
            if (method_exists($classname, 'getApiActions')) {
                foreach ($classname::getApiActions() as $key => $values) {
                    if (is_numeric($key) && $values !== 'default') {
                        $schema->addField(
                            'api.' . $controller . '.action.' . $values,
                            'string'
                        );
                    }
                }
            }
        }
        return $schema;
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        foreach ($this->getSchema()->fields() as $field) {
            $validator->add(
                $field,
                'inList',
                [
                    'rule' => ['inList', ['-1', '0', '1']]
                ]
            );
        }
        return $validator;
    }

    /**
     * Hook method to be implemented in subclasses.
     *
     * Used by `execute()` to execute the form's action.
     *
     * @param array $data Form data.
     * @return bool
     */
    protected function _execute(array $data): bool
    {
        if (empty($data['aro']) || !$data['aro'] instanceof EntityInterface) {
            throw new BadRequestException("aro must be an entity");
        }
        $aro = $data['aro'];
        unset($data['aro']);
        $success = true;
        foreach ($data as $root => $controllers) {
            foreach ($controllers as $controller => $values) {
                $hasActions = false;
                $path = "$root/$controller";
                foreach ($values as $crud => $value) {
                    if ($crud === 'action') {
                        $hasActions = true;
                        continue;
                    }
                    $success = $success && $this->setAcl(
                        $aro->id,
                        $path,
                        $value,
                        $crud
                    );
                }
                if (!$hasActions) {
                    continue;
                }
                foreach ($values['action'] as $action => $value) {
                    $success = $success && $this->setAcl(
                        $aro->id,
                        $path . '/' . $action,
                        $value
                    );
                }
            }
        }
        return $success;
    }

    /**
     * @var AclComponent
     */
    private $Acl;

    /**
     * Défini une permission
     * @param mixed      $aro   ARO The requesting object identifier. See `AclNode::node()` for possible formats
     * @param string     $path
     * @param int|string $value -1 = deny, 0 = inherit, 1 = allow
     * @param string     $crud
     * @return bool success
     */
    private function setAcl($aro, $path, $value, $crud = '*'): bool
    {
        if (empty($this->Acl)) {
            $registry = new ComponentRegistry();
            $this->Acl = new AclComponent($registry);
        }
        switch ($value) {
            case '1':
                $act = 'allow';
                break;
            case '0':
                $act = 'inherit';
                break;
            case '-1':
                $act = 'deny';
                break;
            default:
                return false;
        }
        $this->Acl->$act($aro, $path, $crud);
        return true;
    }
}
