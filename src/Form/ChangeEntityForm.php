<?php

/**
 * Versae\Form\ChangeEntityForm
 */

namespace Versae\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Exception;

/**
 * ChangeEntity Form.
 */
class ChangeEntityForm extends Form
{
    /**
     * Builds the schema for the modelless form
     *
     * @param Schema $schema From schema
     * @return Schema
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        $schema->addField('archival_agency_id', ['type' => 'integer']);
        $schema->addField('org_entity_id', ['type' => 'integer']);
        $schema->addField('user_id', ['type' => 'integer']);
        $schema->addField('reason', ['type' => 'text']);
        $schema->addField('disable_user', ['type' => 'boolean']);

        return $schema;
    }

    /**
     * Form validation builder
     *
     * @param Validator $validator to use against the form
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $Users = TableRegistry::getTableLocator()->get('Users');
        $Roles = TableRegistry::getTableLocator()->get('Roles');
        $fnExistEntity = fn($value) => $OrgEntities->exists(['id' => $value]);
        $fnExistUser = fn($value) => $Users->exists(['id' => $value]);
        $fnExistRole = fn($value) => $Roles->exists(['id' => $value]);

        $validator
            ->requirePresence('base_archival_agency_id');

        $validator
            ->requirePresence('archival_agency_id')
            ->add(
                'archival_agency_id',
                'exist',
                ['rule' => $fnExistEntity]
            );

        $validator
            ->requirePresence(
                'org_entity_id',
                function ($context) {
                    return Hash::get($context, 'data.base_archival_agency_id')
                        === Hash::get($context, 'data.archival_agency_id');
                }
            )
            ->allowEmptyString(
                'org_entity_id',
                null,
                function ($context) {
                    return Hash::get($context, 'data.base_archival_agency_id')
                        !== Hash::get($context, 'data.archival_agency_id');
                }
            )
            ->add(
                'org_entity_id',
                'childof',
                [
                    'rule' => function ($value, $context) use ($OrgEntities) {
                        $archivalAgencyId = Hash::get($context, 'data.archival_agency_id');
                        $aa = $OrgEntities->get($archivalAgencyId);
                        return $OrgEntities->exists(
                            [
                                'id' => $value,
                                'lft >=' => $aa->get('lft'),
                                'rght <=' => $aa->get('rght'),
                            ]
                        );
                    }
                ]
            );

        $validator
            ->requirePresence('user_id')
            ->add(
                'user_id',
                'exist',
                ['rule' => $fnExistUser]
            );

        $validator
            ->requirePresence(
                'role_id',
                function ($context) {
                    return Hash::get($context, 'data.base_archival_agency_id')
                        === Hash::get($context, 'data.archival_agency_id');
                }
            )
            ->allowEmptyString(
                'role_id',
                null,
                function ($context) {
                    return Hash::get($context, 'data.base_archival_agency_id')
                        !== Hash::get($context, 'data.archival_agency_id');
                }
            )
            ->add(
                'role_id',
                'exist',
                ['rule' => $fnExistRole]
            )
            ->add(
                'role_id',
                'valid_role',
                [
                    'rule' => function ($value, $context) use ($OrgEntities) {
                        $orgEntityId = Hash::get($context, 'data.org_entity_id');
                        return empty($orgEntityId) || $OrgEntities->find()
                            ->innerJoinWith('TypeEntities')
                            ->innerJoinWith('TypeEntities.RolesTypeEntities')
                            ->where(
                                [
                                    'OrgEntities.id' => $orgEntityId,
                                    'RolesTypeEntities.role_id' => $value,
                                ]
                            )
                            ->count() !== 0;
                    }
                ]
            );

        $validator
            ->allowEmptyString('reason');

        $validator
            ->allowEmptyString('disable_user');

        return $validator;
    }

    /**
     * Defines what to execute once the Form is processed
     *
     * @param array $data Form data.
     * @return bool
     * @throws Exception
     */
    protected function _execute(array $data): bool
    {
        $data['archival_agency_id'] = (int)$data['archival_agency_id'];
        $data['org_entity_id'] = isset($data['org_entity_id']) ? (int)$data['org_entity_id'] : null;
        $data['disable_user'] = isset($data['disable_user']) ? (bool)$data['disable_user'] : null;
        $data['role_id'] = isset($data['role_id']) ? (int)$data['role_id'] : null;
        $ChangeEntityRequests = TableRegistry::getTableLocator()->get('ChangeEntityRequests');

        if ($data['base_archival_agency_id'] === $data['archival_agency_id']) {
            $Users = TableRegistry::getTableLocator()->get('Users');
            $user = $Users->get($data['user_id']);
            $Users->patchEntity(
                $user,
                [
                    'org_entity_id' => $data['org_entity_id'],
                    'role_id' => $data['role_id'],
                    'active' => true,
                ]
            );
            $Users->saveOrFail($user); // beforeSave
            $ChangeEntityRequests->deleteAll(['user_id' => $data['user_id']]);
        } else {
            $changeRequest = $ChangeEntityRequests->find()
                ->where(['user_id' => $data['user_id']])
                ->first();
            if (!$changeRequest) {
                $changeRequest = $ChangeEntityRequests->newEmptyEntity();
            }
            $ChangeEntityRequests->patchEntity(
                $changeRequest,
                [
                    'base_archival_agency_id' => $data['base_archival_agency_id'],
                    'target_archival_agency_id' => $data['archival_agency_id'],
                    'user_id' => $data['user_id'],
                    'reason' => $data['reason'],
                ]
            );
            $ChangeEntityRequests->saveOrFail($changeRequest);
            if ($data['disable_user']) {
                $Users = TableRegistry::getTableLocator()->get('Users');
                $Users->updateAll(
                    ['active' => !$data['disable_user']],
                    ['id' => $data['user_id']]
                );
                $Sessions = TableRegistry::getTableLocator()->get('Sessions');
                $Sessions->deleteAll(['user_id' => $data['user_id']]);
            }
        }

        return true;
    }
}
