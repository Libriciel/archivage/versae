<?php

/**
 * Versae\Form\InstallConfigForm
 */

namespace Versae\Form;

use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\Config;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Form\Form;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Utility\Security;
use Cake\Validation\Validator;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Versae\Controller\Component\EmailsComponent;
use Versae\Model\Table\ArchivingSystemsTable;
use Versae\Model\Table\LdapsTable;
use Versae\Model\Table\OrgEntitiesTable;
use Versae\Model\Table\RolesTable;
use Versae\Model\Table\TypeEntitiesTable;
use Versae\Model\Table\UsersTable;

/**
 * Formulaire d'installation par fichier ini
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class InstallConfigForm extends Form
{
    /**
     * @var callable permet le feedback
     */
    public $stdOut;

    /**
     * @var bool Permet d'ajouter des éléments suite à une installation
     */
    public $postInstall = false;

    /**
     * overwrite mode for import
     * @var bool
     */
    public $overwrite;

    /**
     * headless mode
     * @var bool
     */
    public $headless;

    /**
     * @var array représentation flatten du data du ini
     */
    private $iniData = [];

    /**
     * Ecrit un message dans $this->output
     * @param string $output
     * @return void
     */
    public function out(string $output): void
    {
        if (is_callable($this->stdOut)) {
            call_user_func($this->stdOut, $output);
        }
    }

    /**
     * Validation pour la section config
     * @param Validator $validator
     * @return void
     */
    private function validationConfig(Validator $validator): void
    {
        $prefix = 'config__';
        $validator
            ->requirePresence($prefix . 'config_file')
            ->notEmptyString($prefix . 'config_file')
            ->add(
                'config_file',
                'is_writable',
                $this->ruleIsWritable()
            );
        $validator
            ->requirePresence($prefix . 'data_dir')
            ->notEmptyString($prefix . 'data_dir')
            ->add(
                $prefix . 'data_dir',
                'is_writable',
                $this->ruleIsWritable()
            );
        $validator
            ->requirePresence($prefix . 'admin_file')
            ->notEmptyString($prefix . 'admin_file')
            ->add(
                $prefix . 'admin_file',
                'is_writable',
                $this->ruleIsWritable()
            );
    }

    /**
     * Validation pour la section proxy
     * @param Validator $validator
     * @return void
     */
    private function validationProxy(Validator $validator): void
    {
        $prefix = 'proxy__';
        $validator
            ->allowEmptyString($prefix . 'host');
        $validator
            ->allowEmptyString($prefix . 'port')
            ->nonNegativeInteger($prefix . 'port');
        $validator
            ->allowEmptyString($prefix . 'username');
        $validator
            ->allowEmptyString($prefix . 'password');
    }

    /**
     * Vérifie qu'un fichier/son dossier soit inscriptible
     * @return array
     */
    public function ruleIsWritable(): array
    {
        return [
            'rule' => function ($v) {
                if (is_file($v)) {
                    return is_writable($v)
                        ?: __(
                            "Le fichier n'est pas inscriptible"
                        );
                }
                $dir = $v;
                while (!is_dir($dir)) {
                    $lastdir = $dir;
                    $dir = dirname($dir);
                    if ($dir === $lastdir || $dir === '/' || $dir === '.') {
                        return false;
                    }
                }
                return is_writable($dir);
            },
            'message' => __("Le dossier n'est pas inscriptible"),
        ];
    }

    /**
     * Validation pour la section app
     * @param Validator $validator
     * @return void
     */
    private function validationApp(Validator $validator): void
    {
        $prefix = 'app__';
        $validator
            ->requirePresence($prefix . 'locale')
            ->notEmptyString($prefix . 'locale');
        $validator
            ->requirePresence($prefix . 'timezone')
            ->notEmptyString($prefix . 'timezone');
        $validator
            ->requirePresence($prefix . 'url')
            ->notEmptyString($prefix . 'url');
    }

    /**
     * Validation pour la section database
     * @param Validator $validator
     * @return void
     */
    private function validationDatabase(Validator $validator): void
    {
        $prefix = 'database__';
        $validator
            ->requirePresence($prefix . 'host')
            ->notEmptyString($prefix . 'host');
        $validator
            ->requirePresence($prefix . 'database')
            ->notEmptyString($prefix . 'database');
    }

    /**
     * Validation pour la section security
     * @param Validator $validator
     * @return void
     */
    private function validationSecurity(Validator $validator): void
    {
        $prefix = 'security__';
        $validator
            ->requirePresence($prefix . 'type')
            ->notEmptyString($prefix . 'type')
            ->inList($prefix . 'type', ['set', 'random', 'hash']);
    }

    /**
     * Validation pour la section email
     * @param Validator $validator
     * @return void
     */
    private function validationEmail(Validator $validator): void
    {
        $prefix = 'email__';
        $validator
            ->requirePresence($prefix . 'from')
            ->notEmptyString($prefix . 'from')
            ->email($prefix . 'from');
        $validator
            ->requirePresence($prefix . 'method')
            ->notEmptyString($prefix . 'method')
            ->inList($prefix . 'method', ['mail', 'smtp']);
        $validator
            ->requirePresence($prefix . 'host')
            ->notEmptyString($prefix . 'host');
        $validator
            ->requirePresence($prefix . 'port')
            ->notEmptyString($prefix . 'port')
            ->nonNegativeInteger($prefix . 'port');
    }

    /**
     * Validation pour la section admins
     * @param Validator $validator
     * @return void
     */
    private function validationAdmins(Validator $validator): void
    {
        $prefix = 'admins__';
        $expanded = Hash::expand($this->_data, '__');
        foreach (array_keys(Hash::get($expanded, 'admins', [])) as $key) {
            $validator
                ->requirePresence($prefix . $key . '__username')
                ->notEmptyString($prefix . $key . '__username');
            $validator->add(
                $prefix . $key . '__password',
                'custom',
                [
                    'rule' => function (string $name, array $context) use ($prefix, $key) {
                        return !isset($context['data'][ $prefix . $key . '__hashed_password']);
                    },
                ]
            );
            $validator->add(
                $prefix . $key . '__hashed_password',
                'custom',
                [
                    'rule' => function (string $name, array $context) use ($prefix, $key) {
                        return !isset($context['data'][ $prefix . $key . '__password']);
                    }
                ]
            );
        }
    }

    /**
     * Validation pour la section exploitation_service
     * @param Validator $validator
     * @return void
     */
    private function validationExploitation(Validator $validator): void
    {
        $prefix = 'exploitation_service__';
        $validator
            ->requirePresence($prefix . 'name')
            ->notEmptyString($prefix . 'name');
        $validator
            ->requirePresence($prefix . 'identifier')
            ->notEmptyString($prefix . 'identifier');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $this->validationExploitation($validator);
        $this->validationConfig($validator);
        $this->validationProxy($validator);
        $this->validationApp($validator);
        $this->validationDatabase($validator);
        $this->validationSecurity($validator);
        $this->validationEmail($validator);
        $this->validationAdmins($validator);
        return $validator;
    }

    /**
     * Hook method to be implemented in subclasses.
     *
     * Used by `execute()` to execute the form's action.
     *
     * @param array $data Form data.
     * @return bool
     * @throws Exception
     */
    protected function _execute(array $data): bool
    {
        $this->iniData = Hash::expand($data, '__');
        if (!$this->postInstall) {
            $this->buildConfigFile();
            $this->loadConfig();
            $this->buildAdministratorsJson();
            $this->buildDatabase();
            $this->loadDatabase();
            $this->buildExploitation();
        }

        $this->iniData = Hash::expand($data, '__');
        if (isset($this->iniData['archival_agencies'])) {
            $this->buildArchivalAgencies();
        }
        if (isset($this->iniData['ldaps'])) {
            $this->buildLdaps();
        }
        if (isset($this->iniData['archiving_systems'])) {
            $this->buildArchivingSystems();
        }
        if (isset($this->iniData['org_entities'])) {
            $this->buildOrgEntities();
        }
        if (isset($this->iniData['users'])) {
            $this->buildUsers();
        }
        return true;
    }

    /**
     * Génère le fichier app_local.json
     * @throws Exception
     */
    private function buildConfigFile(): void
    {
        $configJson = [
            'debug' => (bool)$this->iniData['config']['debug'],
            'App.paths.data' => $this->iniData['config']['data_dir'],
            'App.paths.administrators_json' => $this->iniData['config']['admin_file'],
            'App.defaultLocale' => $this->iniData['app']['locale'],
            'App.timezone' => $this->iniData['app']['timezone'],
            'App.fullBaseUrl' => $this->iniData['app']['url'],
            'Ratchet.connect' => $this->iniData['ratchet']['url_ws'],
            'Datasources.default.host' => $this->iniData['database']['host'],
            'Datasources.default.port' => $this->iniData['database']['port'] ?? '5432',
            'Datasources.default.username' => $this->iniData['database']['username'],
            'Datasources.default.password' => $this->iniData['database']['password'],
            'Datasources.default.database' => $this->iniData['database']['database'],
            'Email.default.from' => $this->iniData['email']['from'],
            'EmailTransport.default.className' => ucfirst($this->iniData['email']['method']),
            'EmailTransport.default.host' => $this->iniData['email']['host'],
            'EmailTransport.default.port' => $this->iniData['email']['port'],
            'EmailTransport.default.username' => $this->iniData['email']['username'],
            'EmailTransport.default.password' => $this->iniData['email']['password'],
            'Proxy.host' => $this->iniData['proxy']['host'],
            'Proxy.port' => $this->iniData['proxy']['port'],
            'Proxy.username' => $this->iniData['proxy']['username'],
            'Proxy.password' => $this->iniData['proxy']['password'],
        ];
        switch ($this->iniData['security']['type']) {
            case 'set':
                $configJson['Security.salt'] = Hash::get($this->iniData, 'security.value');
                break;
            case 'random':
                $configJson['Security.salt'] = hash('sha256', uniqid('libriciel'));
                break;
            case 'hash':
                $configJson['Security.salt'] = hash(
                    'sha256',
                    'Libriciel-' . Hash::get($this->iniData, 'security.value')
                );
                break;
        }
        Security::setSalt($configJson['Security.salt']);
        ksort($configJson);
        $filename = Hash::get($this->iniData, 'config.config_file');
        $initialJson = file_exists($filename)
            ? (json_decode(file_get_contents($filename), true) ?: [])
            : [];
        Filesystem::dumpFile(
            $filename,
            json_encode(
                Hash::expand($configJson) + $initialJson,
                JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES
            )
        );
        $this->out('written ' . $filename);
        $pathToLocalConfig = Configure::read('App.paths.path_to_local_config');
        $escaped = addcslashes($filename, "'");
        Filesystem::dumpFile(
            $pathToLocalConfig,
            "<?php return '$escaped';?>"
        );
        $this->out('written ' . $pathToLocalConfig);
    }

    /**
     * Ne passe pas par la validation
     * @param array $data
     * @return bool
     * @throws Exception
     */
    public function force(array $data): bool
    {
        return $this->_execute($data);
    }

    /**
     * Génère le fichier administrators.json
     * @return void
     * @throws Exception
     */
    private function buildAdministratorsJson(): void
    {
        $admins = array_map(
            function ($a) {
                $hasher = new DefaultPasswordHasher();
                $a['password'] = isset($a['password']) ? $hasher->hash($a['password']) : $a['hashed_password'];
                unset($a['hashed_password']);
                return $a;
            },
            Hash::get($this->iniData, 'admins', [])
        );
        $filename = Hash::get($this->iniData, 'config.admin_file');
        Filesystem::dumpFile($filename, json_encode(array_values($admins), JSON_PRETTY_PRINT));
        $this->out('written ' . $filename);
    }

    /**
     * Initialise la base de données
     * @return void
     * @throws Exception
     */
    private function buildDatabase(): void
    {
        $this->out('installing database...');
        $Exec = Utility::get('Exec');
        $Exec->rawCommand(
            ROOT . DS . 'bin' . DS . 'cake migrations migrate 2>&1'
            . '&& ' . ROOT . DS . 'bin' . DS . 'cake migrations seed 2>&1'
            . '&& ' . ROOT . DS . 'bin' . DS . 'cake update 2>&1'
            . '&& ' . ROOT . DS . 'bin' . DS . 'cake RolesPerms import ' . RESOURCES . 'export_roles.json --keep 2>&1',
            $output,
            $code
        );
        $message = implode("<br>\n", $output);
        if ($code !== 0) {
            throw new Exception($message);
        }
        $this->out($message);
    }

    /**
     * Ajout du service d'exploitation
     * @return void
     */
    private function buildExploitation(): void
    {
        $this->out('installing exploitation service...');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $seCode = $OrgEntities->getAssociation('TypeEntities')->find()
            ->select(['id'])
            ->where(['code' => 'SE'])
            ->firstOrFail()
            ->get('id');
        if ($OrgEntities->exists(['type_entity_id' => $seCode])) {
            return;
        }
        $entity = $OrgEntities->newEntity(
            [
                'name' => $this->iniData['exploitation_service']['name'],
                'identifier' => $this->iniData['exploitation_service']['identifier'],
                'type_entity_id' => $seCode,
            ]
        );
        $OrgEntities->saveOrFail($entity);
    }

    /**
     * Charge la base de donnée fraichement créée dans ConnectionManager
     * @return void
     * @throws Exception
     */
    private function loadDatabase(): void
    {
        /** @var ConnectionManager $ConnectionManager */
        $ConnectionManager = Utility::get(ConnectionManager::class);
        $ConnectionManager::drop('default');
        $ConnectionManager::setConfig(
            'default',
            Config::readAll('Datasources.default')
        );
    }

    /**
     * Recharge la config
     * @return void
     * @throws Exception
     */
    private function loadConfig(): void
    {
        Config::reset();
        Config::write(Config::readAll());
    }

    /**
     * Ajoute les service d'archives
     * @return void
     * @throws Exception
     */
    private function buildArchivalAgencies(): void
    {
        $this->out('installing archival agencies...');

        $aas = &$this->iniData['archival_agencies'];
        $loc = TableRegistry::getTableLocator();
        /** @var OrgEntitiesTable $ArchivalAgencies */
        $ArchivalAgencies = $loc->get('OrgEntities');
        /** @var TypeEntitiesTable $TypeEntities */
        $TypeEntities = $ArchivalAgencies->getAssociation('TypeEntities');

        $saCode = $TypeEntities->find()
            ->select(['id'])
            ->where(['code' => 'SA'])
            ->firstOrFail()
            ->get('id');

        // récupération de l'id du SE
        $seCode = $TypeEntities->find()
            ->select(['id'])
            ->where(['code' => 'SE'])
            ->firstOrFail()
            ->get('id');
        $seId = $ArchivalAgencies->find()
            ->select(['id'])
            ->where(['type_entity_id' => $seCode])
            ->firstOrFail()
            ->get('id');

        foreach ($aas as $aakey => $archivalAgency) {
            $archivalAgencyEntity = $ArchivalAgencies->find()
                ->where(['identifier' => $archivalAgency['identifier']])
                ->first();
            if (!$archivalAgencyEntity) {
                $archivalAgency += [
                    'parent_id' => $seId,
                    'type_entity_id' => $saCode,
                    'active' => (bool)($archivalAgency['active'] ?? true),
                    'is_main_archival_agency' => (bool)$archivalAgency['is_main_archival_agency'],
                    'max_disk_usage' => $archivalAgency['max_disk_usage'] >= 1000000
                        ? $archivalAgency['max_disk_usage']
                        : (int)$archivalAgency['max_disk_usage'] * 1024 * 1024 * 1024,
                    'disk_usage' => 0,
                ];
                $archivalAgencyEntity = $ArchivalAgencies->saveOrFail(
                    $ArchivalAgencies->newEntity($archivalAgency)
                );
            }
            $aas[$aakey] = $archivalAgencyEntity;
        }
    }

    /**
     * Ajoute les ldaps
     * @return void
     */
    private function buildLdaps(): void
    {
        $this->out('installing ldaps...');
        $ldaps = &$this->iniData['ldaps'];
        $loc = TableRegistry::getTableLocator();
        /** @var LdapsTable $Ldaps */
        $Ldaps = $loc->get('Ldaps');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $loc->get('OrgEntities');
        foreach ($ldaps as $ldap) {
            $oe = $OrgEntities->find()
                ->where(['identifier' => $ldap['org_entity']])
                ->first();
            if (!$Ldaps->exists(['name' => $ldap['name']])) {
                $l = [
                    'org_entity_id' => $oe?->id,
                    'follow_referrals' => (bool)$ldap['follow_referrals'],
                ] + $ldap;
                $Ldaps->saveOrFail($Ldaps->newEntity($l));
            }
        }
    }

    /**
     * Ajoute les Systemes d'Archivage
     * @return void
     */
    private function buildArchivingSystems(): void
    {
        $this->out('installing archiving systems...');
        $archivingSystems = &$this->iniData['archiving_systems'];
        $loc = TableRegistry::getTableLocator();
        /** @var ArchivingSystemsTable $ArchivingSystems */
        $ArchivingSystems = $loc->get('ArchivingSystems');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $loc->get('OrgEntities');
        foreach ($archivingSystems as $archivingSystem) {
            $oe = $OrgEntities->find()
                ->where(['identifier' => $archivingSystem['org_entity']])
                ->firstOrFail();
            if (
                !$ArchivingSystems->exists(
                    [
                        'org_entity_id' => $oe->id,
                        'name' => $archivingSystem['name'],
                    ]
                )
            ) {
                $as = [
                    'org_entity_id' => $oe->id,
                ] + $archivingSystem;
                $ArchivingSystems->saveOrFail($ArchivingSystems->newEntity($as));
            }
        }
    }

    /**
     * Ajoute les entités
     * @return void
     */
    private function buildOrgEntities(): void
    {
        $this->out('installing entities...');
        $oes = &$this->iniData['org_entities'];
        $loc = TableRegistry::getTableLocator();
        /** @var TypeEntitiesTable $TypeEntities */
        $TypeEntities = $loc->get('TypeEntities');
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $loc->get('OrgEntities');
        foreach ($oes as $oe) {
            $sa = $OrgEntities->find()
                ->innerJoinWith('TypeEntities')
                ->where(['identifier' => $oe['archival_agency']])
                ->andWhere(['code' => 'SA'])
                ->firstOrFail();
            $existingEntity = (bool)count(
                $OrgEntities->find()
                    ->select(['existing' => 1])
                    ->innerJoinWith('ArchivalAgencies')
                    ->where(
                        [
                            'ArchivalAgencies.id' => $sa->id,
                            'OrgEntities.identifier' => $oe['identifier'],
                        ]
                    )
                    ->limit(1)
                    ->disableHydration()
                    ->toArray()
            );
            if (!$existingEntity) {
                $org_entity_id = $OrgEntities->find()
                    ->select(['id'])
                    ->where(['identifier' => $oe['parent']])
                    ->andWhere(
                        [
                            'lft >=' => $sa->get('lft'),
                            'rght <=' => $sa->get('rght')
                        ]
                    )
                    ->firstOrFail()
                    ->get('id');
                $type_entity_id = $TypeEntities->find()
                    ->select(['id'])
                    ->where(['code' => $oe['type_entity']])
                    ->firstOrFail()
                    ->get('id');
                $d = [
                    'parent_id' => $org_entity_id,
                    'type_entity_id' => $type_entity_id,
                ] + $oe;
                $OrgEntities->saveOrFail($OrgEntities->newEntity($d));
            }
        }
    }

    /**
     * Ajoute des utilisateurs aux service d'archives
     * @return void
     * @throws Exception
     */
    private function buildUsers(): void
    {
        $this->out('installing users...');
        $users = &$this->iniData['users'];
        $loc = TableRegistry::getTableLocator();
        /** @var UsersTable $Users */
        $Users = $loc->get('Users');
        /** @var RolesTable $Roles */
        $Roles = $loc->get('Roles');
        $defaultRoles = $Roles->find(
            'list',
            ['keyField' => 'code', 'valueField' => 'id']
        )
            ->where(['code IS NOT' => null])
            ->all()
            ->toArray();
        /** @var OrgEntitiesTable $OrgEntities */
        $OrgEntities = $loc->get('OrgEntities');
        $entities = $OrgEntities->find(
            'list',
            ['keyField' => 'identifier', 'valueField' => 'id']
        )
            ->all()
            ->toArray();
        $registry = new ComponentRegistry();
        $Emails = new EmailsComponent($registry);
        $hasher = new DefaultPasswordHasher();

        foreach ($users as $key => $user) {
            if ($Users->exists(['username' => $user['username']])) {
                continue;
            }
            $user['role_id'] = is_numeric(substr($user['role'], -1, 1))
                ? $this->iniData['roles'][$user['role']]['id']
                : $defaultRoles[$user['role']];
            $user['org_entity_id'] = $entities[$user['org_entity']];
            $user['active'] = (bool)($user['org_entity'] ?? true);
            $sendMail = false;
            if (empty($user['password']) && empty($user['hashed_password'])) {
                $user['password'] = $Users->generatePassword();
                $sendMail = true;
            } else {
                $user['password'] = isset($user['password'])
                    ? $hasher->hash($user['password'])
                    : $user['hashed_password'];
            }
            $u = $Users->newEntity($user);
            $u->set('password', $user['password'], ['setter' => false]);
            $users[$key] = $Users->saveOrFail($u);

            if ($sendMail) {
                $Emails->submitEmailNewUser($users[$key]);
            }
        }
    }
}
