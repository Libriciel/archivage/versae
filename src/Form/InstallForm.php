<?php

/**
 * Versae\Form\InstallForm
 */

namespace Versae\Form;

use Cake\Auth\DefaultPasswordHasher;
use Cake\Core\Configure;
use Cake\Database\Connection;
use Cake\Datasource\ConnectionManager;
use Cake\Database\Exception\MissingConnectionException;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use DateTime;
use DateTimeZone;
use Exception;
use PDOException;

/**
 * Formulaire d'installation
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class InstallForm extends Form
{
    /**
     * A hook method intended to be implemented by subclasses.
     *
     * You can use this method to define the schema using
     * the methods on Cake\Form\Schema, or loads a pre-defined
     * schema from a concrete class.
     *
     * @param Schema $schema The schema to customize.
     * @return Schema The schema to use.
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        $schema->addField('local_config_file', ['type' => 'string']);
        $schema->addField('Config__App__paths__data', ['type' => 'string']);
        $schema->addField('Config__App__paths__administrators_json', ['type' => 'string']);
        $schema->addField('Config__App__defaultLocal', ['type' => 'string']);
        $schema->addField('Config__App__timezone', ['type' => 'string']);
        $schema->addField('Config__App__fullBaseUrl', ['type' => 'string']);
        $schema->addField('Config__Ratchet__connect', ['type' => 'string']);
        $schema->addField('Config__Datasources__default__driver', ['type' => 'string']);
        $schema->addField('Config__Datasources__default__host', ['type' => 'string']);
        $schema->addField('Config__Datasources__default__username', ['type' => 'string']);
        $schema->addField('Config__Datasources__default__password', ['type' => 'string']);
        $schema->addField('Config__Datasources__default__database', ['type' => 'string']);
        $schema->addField('Config__Security__salt', ['type' => 'string']);
        $schema->addField('Config__Email__default__from', ['type' => 'string']);
        $schema->addField('Config__EmailTransport__default__className', ['type' => 'string']);
        $schema->addField('Config__EmailTransport__default__host', ['type' => 'string']);
        $schema->addField('Config__EmailTransport__default__port', ['type' => 'integer']);
        $schema->addField('Config__EmailTransport__default__username', ['type' => 'string']);
        $schema->addField('Config__EmailTransport__default__password', ['type' => 'string']);
        $schema->addField('Admins__username', ['type' => 'string']);
        $schema->addField('Admins__email', ['type' => 'string']);
        $schema->addField('Admins__password', ['type' => 'string']);
        $schema->addField('Admins__name', ['type' => 'string']);
        $schema->addField('ServiceExploitation__name', ['type' => 'string']);
        $schema->addField('ServiceExploitation__identifier', ['type' => 'string']);
        return $schema;
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->add(
            'local_config_file',
            'writable',
            [
                'rule' => function ($value) {
                    $dir = realpath(dirname($value));
                    return $dir && is_writable(dirname($value)) && (
                        !is_file($value) || is_writable($value)
                    );
                },
                'message' => __("Le chemin indiqué n'est pas inscriptible")
            ]
        );
        $validator->add(
            'Config__App__paths__data',
            'writable',
            [
                'rule' => function ($value) {
                    return is_writable($value);
                },
                'message' => __("Le chemin indiqué n'est pas inscriptible")
            ]
        );
        $validator->add(
            'Config__App__paths__administrators_json',
            'writable',
            [
                'rule' => function ($value) {
                    return is_file($value) ? is_writable($value) : is_writable(dirname($value));
                },
                'message' => __("Le chemin indiqué n'est pas inscriptible")
            ]
        );
        $validator->add(
            'Config__App__defaultLocale',
            'writable',
            [
                'rule' => ['custom', '/^[a-z]{2,3}(_[A-Z]{2})?/'],
                'message' => __("Mauvais pattern")
            ]
        );
        $validator->add(
            'Config__App__timezone',
            'timezone',
            [
                'rule' => function ($value) {
                    try {
                        $date = new DateTime();
                        $date->setTimezone(new DateTimeZone($value));
                        return true;
                    } catch (Exception) {
                        return false;
                    }
                },
                'message' => __("Mauvaise timezone")
            ]
        );
        $validator->add(
            'Config__App__fullBaseUrl',
            'custom',
            [
                'rule' => function ($value, $context) {
                    if ($context['data']['ignore_invalid_fullbaseurl']) {
                        return true;
                    }
                    stream_context_set_default(
                        [
                            'ssl' => [
                                'verify_peer' => false,
                                'verify_peer_name' => false,
                            ],
                        ]
                    );
                    $headers = @get_headers($value);
                    return $headers && $headers[0] !== 'HTTP/1.1 404 Not Found';
                },
                'message' => __("N'est pas un dns valide")
            ]
        );
        $validator->add(
            'Config__Datasources__default__host',
            'custom',
            [
                'rule' => function ($value, $context) {
                    try {
                        $default = include CONFIG . 'app_default.php';
                        ConnectionManager::drop('validate_datasource');
                        ConnectionManager::setConfig(
                            'validate_datasource',
                            [
                                'host' => $value,
                                'driver' => $context['data']['Config__Datasources__default__driver']
                                    ?? $default['Datasources']['default']['driver'],
                                'username' => $context['data']['Config__Datasources__default__username'],
                                'password' => $context['data']['Config__Datasources__default__password'],
                                'database' => $context['data']['Config__Datasources__default__database'],
                            ] + $default['Datasources']['default']
                        );
                        $conn = ConnectionManager::get('validate_datasource');
                        if ($conn instanceof Connection) {
                            $conn->query('select 1');
                        }
                        return true;
                    } catch (PDOException | MissingConnectionException $e) {
                        return $e->getMessage();
                    }
                }
            ]
        );
        $validator->sameAs('confirm_database_password', 'Config__Datasources__default__password');
        $validator->email('Config__Email__default__from');
        $validator->notEmptyString('Config__Email__default__from');
        $validator->numeric('Config__EmailTransport__default__port');
        $validator->notEmptyString('Config__EmailTransport__default__className');
        $validator->notEmptyString('Admins__username');
        $validator->notEmptyString('Admins__email');
        $validator->notEmptyString('Admins__password');
        $validator->sameAs('Admins__confirm-password', 'Admins__password');
        $validator->notEmptyString('ServiceExploitation__name');
        $validator->notEmptyString('ServiceExploitation__identifier');

        return $validator;
    }

    /**
     * Hook method to be implemented in subclasses.
     *
     * Used by `execute()` to execute the form's action.
     *
     * @param array $data Form data.
     * @return bool
     * @throws Exception
     */
    protected function _execute(array $data): bool
    {
        $this->configurate($data);
        $this->createAdmin($data);
        return (bool)$this->createSE($data);
    }

    /**
     * Configuration
     * @param array $data
     */
    private function configurate(array $data)
    {
        $pathToLocalConfig = Configure::read('App.paths.path_to_local_config');
        if (isset($data['local_config_file'])) {
            if (!is_file($data['local_config_file'])) {
                file_put_contents($data['local_config_file'], '{}');
            }
            $path = addcslashes($data['local_config_file'], "'\\");
            file_put_contents($pathToLocalConfig, "<?php return '$path';?>");
        }
        switch ($data['security_salt_method'] ?? '') {
            case 'apply':
                $data['Config__Security__salt'] = $data['security_salt_method_apply'];
                break;
            case 'generate':
                $data['Config__Security__salt'] = $data['security_salt_method_generate'];
                break;
            case 'hash':
                $data['Config__Security__salt'] = hash(
                    'sha256',
                    'Libriciel-' . $data['security_salt_method_hash']
                );
                break;
        }
        $pathToLocal = include $pathToLocalConfig;
        $config = is_readable($pathToLocal)
            ? (array)json_decode(file_get_contents($pathToLocal), true)
            : [];
        $configChanged = false;
        $configKeys = [
            'App__paths__data',
            'App__paths__administrators_json',
            'App__defaultLocale',
            'App__timezone',
            'App__fullBaseUrl',
            'Ratchet__connect',
            'Datasources__default__driver',
            'Datasources__default__host',
            'Datasources__default__username',
            'Datasources__default__password',
            'Datasources__default__database',
            'Security__salt',
            'Email__default__from',
            'EmailTransport__default__host',
            'EmailTransport__default__port',
            'EmailTransport__default__username',
            'EmailTransport__default__password',
        ];
        foreach ($configKeys as $key) {
            $dataKey = 'Config__' . $key;
            if (isset($data[$dataKey])) {
                $config = Hash::insert($config, str_replace('__', '.', $key), $data[$dataKey]);
                $configChanged = true;
            }
        }

        if ($configChanged) {
            ksort($config);
            file_put_contents($pathToLocal, json_encode($config, JSON_PRETTY_PRINT));
        }
    }

    /**
     * Création du premier administrateur technique
     * @param array $data
     */
    private function createAdmin(array $data)
    {
        $targetFilename = Configure::read('App.paths.administrators_json');
        if (empty($data['Admins__username']) || is_file($targetFilename)) {
            return;
        }
        $hasher = new DefaultPasswordHasher();
        file_put_contents(
            $targetFilename,
            json_encode(
                [
                    [
                        'username' => $data['Admins__username'],
                        'email' => $data['Admins__email'] ?? '',
                        'password' => $hasher->hash($data['Admins__password'] ?? ''),
                    ]
                ],
                JSON_PRETTY_PRINT
            )
        );
    }

    /**
     * Création du service d'exploitation
     * @param array $data
     * @return bool|void
     */
    private function createSE(array $data)
    {
        if (empty($data['ServiceExploitation__name'])) {
            return;
        }
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $seCode = $OrgEntities->getAssociation('TypeEntities')->find()
            ->select(['id'])
            ->where(['code' => 'SE'])
            ->firstOrFail()
            ->get('id');
        $entity = $OrgEntities->newEntity(
            [
                'name' => $data['ServiceExploitation__name'],
                'identifier' => $data['ServiceExploitation__identifier'],
                'type_entity_id' => $seCode,
            ]
        );
        $OrgEntities->saveOrFail($entity);

        return true;
    }
}
