<?php

/**
 * Versae\Form\AddInputForm
 */

namespace Versae\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Versae\Model\Entity\FormInput;
use Versae\Model\Table\FormInputsTable;
use Versae\Model\Table\FormUnitsTable;

/**
 * Formulaire d'ajout d'un element de formulaire
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AddInputForm extends Form
{
    /**
     * A hook method intended to be implemented by subclasses.
     *
     * You can use this method to define the schema using
     * the methods on Cake\Form\Schema, or loads a pre-defined
     * schema from a concrete class.
     *
     * @param Schema $schema The schema to customize.
     * @return Schema The schema to use.
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        $schema->addField('type', ['type' => 'string']);
        $schema->addField('name', ['type' => 'string']);
        $schema->addField('label', ['type' => 'string']);
        $schema->addField('default_value', ['type' => 'string']);
        $schema->addField('required', ['type' => 'boolean']);
        $schema->addField('readonly', ['type' => 'boolean']);
        $schema->addField('disable_expression', ['type' => 'string']);
        $schema->addField('pattern', ['type' => 'string']);
        $schema->addField('placeholder', ['type' => 'string']);
        $schema->addField('help', ['type' => 'string']);
        $schema->addField('cond_display_select', ['type' => 'string']);
        $schema->addField('cond_display_input', ['type' => 'string']);
        $schema->addField('cond_display_input_type', ['type' => 'string']);
        $schema->addField('cond_display_value_select', ['type' => 'string']);
        $schema->addField('cond_display_value_file', ['type' => 'string']);
        $schema->addField('select2', ['type' => 'boolean']);
        $schema->addField('empty', ['type' => 'string']);
        $schema->addField('multiple', ['type' => 'boolean']);
        $schema->addField('value', ['type' => 'string']);
        $schema->addField('checked', ['type' => 'boolean']);
        $schema->addField('formats', ['type' => 'string']);
        $schema->addField('value', ['type' => 'string']);
        $schema->addField('min', ['type' => 'float']);
        $schema->addField('max', ['type' => 'float']);
        $schema->addField('min_date', ['type' => 'string']);
        $schema->addField('max_date', ['type' => 'string']);
        $schema->addField('step', ['type' => 'float']);
        $schema->addField('p', ['type' => 'text']);
        $schema->addField('color', ['type' => 'string']);
        $schema->addField('copy', ['type' => 'integer']);
        $schema->addField('use_name_as_code', ['type' => 'boolean']);

        return $schema;
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->requirePresence('type')
            ->notEmptyString('type');

        $validator->notEmptyString(
            'name',
            null,
            function () {
                return $this->getData('type') !== FormInputsTable::TYPE_PARAGRAPH;
            }
        );

        $validator->notEmptyString(
            'label',
            null,
            function () {
                return $this->getData('type') !== FormInputsTable::TYPE_PARAGRAPH;
            }
        );

        $validator->notEmptyString(
            'p',
            null,
            function () {
                return $this->getData('type') === FormInputsTable::TYPE_PARAGRAPH;
            }
        );

        $validator->add(
            'multiple',
            'in_multiple_documents',
            [
                'rule' => function ($value, $context) {
                    return $value
                        || !Hash::get($context, 'data.id')
                        || !TableRegistry::getTableLocator()->get('FormUnits')
                            ->exists(
                                [
                                    'form_input_id' => Hash::get($context, 'data.id'),
                                    'type' => FormUnitsTable::TYPE_DOCUMENT_MULTIPLE,
                                ]
                            );
                },
                'message' => __(
                    "'Plusieurs fichiers' obligatoire car ce champ est "
                    . "sélectionné dans une unité d'archives de type Document multiple"
                )
            ]
        );

        $validator->add(
            'multiple',
            'in_simple_documents',
            [
                'rule' => function ($value, $context) {
                    return !$value
                        || !Hash::get($context, 'data.id')
                        || !TableRegistry::getTableLocator()->get('FormUnits')
                            ->exists(
                                [
                                    'form_input_id' => Hash::get($context, 'data.id'),
                                    'type' => FormUnitsTable::TYPE_DOCUMENT,
                                ]
                            );
                },
                'message' => __(
                    "'Plusieurs fichiers' interdit car ce champ est "
                    . "sélectionné dans une unité d'archives de type Document simple"
                )
            ]
        );

        return $validator;
    }

    /**
     * Hook method to be implemented in subclasses.
     *
     * Used by `execute()` to execute the form's action.
     *
     * @param array $data Form data.
     * @return bool
     */
    protected function _execute(array $data): bool
    {
        if (isset($data['required']) && $data['required'] === '0') {
            unset($data['required']);
        }
        if (isset($data['readonly']) && $data['readonly'] === '0') {
            unset($data['readonly']);
        }
        return parent::_execute($data);
    }

    /**
     * Get field data.
     * @param string|null $field The field name or null to get data array with
     *                           all fields.
     * @return mixed
     */
    public function getData(?string $field = null)
    {
        if ($field === 'typetrad') {
            $entity = new FormInput($this->_data);
            return $entity->get('typetrad');
        }
        return parent::getData($field);
    }
}
