<?php

/**
 * Versae\Form\ConfigurationForm
 */

namespace Versae\Form;

use AsalaeCore\Form\ConfigurationForm as CoreForm;
use AsalaeCore\Utility\Config;
use Cake\Form\Schema;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;

/**
 * Formulaire de modification de la configuration
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ConfigurationForm extends CoreForm
{
    public const CONFIG_MAP = [
        'app_paths_data' => 'App.paths.data',
        'ignore_invalid_fullbaseurl' => 'App.ignore_invalid_fullbaseurl',
        'ignore_invalid_datasource' => 'App.ignore_invalid_datasource',
        'app_fullbaseurl' => 'App.fullBaseUrl',
        'hash_algo' => 'hash_algo',
        'password_admin_complexity' => 'Password.admin.complexity',
        'password_complexity' => 'Password.complexity',
        'datacompressor_encodefilename' => 'DataCompressorUtility.encodeFilename',
        'filesystem_useshred' => 'FilesystemUtility.useShred',
        'tokens-duration_code' => 'Webservices.tokens-duration.code',
        'tokens-duration_access-token' => 'Webservices.tokens-duration.access-token',
        'tokens-duration_refresh-token' => 'Webservices.tokens-duration.refresh-token',
        'beanstalk_tests_timeout' => 'Beanstalk.tests.timeout',
        'downloads_asyncfilecreationtimelimit' => 'Downloads.asyncFileCreationTimeLimit',
        'downloads_tempfileconservationtimelimit' => 'Downloads.tempFileConservationTimeLimit',
        'proxy_host' => 'Proxy.host',
        'proxy_port' => 'Proxy.port',
        'paginator_limit' => 'Pagination.limit',
        'ajaxpaginator_limit' => 'AjaxPaginator.limit',
        'proxy_username' => 'Proxy.username',
        'proxy_password' => 'Proxy.password',
        'ip_enable_whitelist' => 'Ip.enable_whitelist',
        'ip_whitelist' => 'Ip.whitelist',
        'libriciel_login_logo' => 'Libriciel.login.logo-client.image',
        'libriciel_login_background' => 'Libriciel.login.logo-client.style',
        'emails_from' => 'Email.default.from',
        'emails_class' => 'EmailTransport.default.className',
        'emails_host' => 'EmailTransport.default.host',
        'emails_port' => 'EmailTransport.default.port',
        'emails_username' => 'EmailTransport.default.username',
        'emails_password' => 'EmailTransport.default.password',
        'attestations_destruction_delay' => 'Attestations.destruction_delay',
    ];

    /**
     * A hook method intended to be implemented by subclasses.
     *
     * You can use this method to define the schema using
     * the methods on Cake\Form\Schema, or loads a pre-defined
     * schema from a concrete class.
     *
     * @param Schema $schema The schema to customize.
     * @return Schema The schema to use.
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        parent::_buildSchema($schema);

        // WIP: retirer directement du core lorsque asalae aura été dockerisé
        $schema
            ->removeField('datasources_default_driver')
            ->removeField('datasources_default_host')
            ->removeField('datasources_default_username')
            ->removeField('datasources_default_password')
            ->removeField('datasources_default_database');

        $schema
            ->addField('seda_ssl_verify_peer', ['type' => 'boolean'])
            ->addField('seda_ssl_verify_peer_name', ['type' => 'boolean'])
            ->addField('seda_ssl_verify_depth', ['type' => 'integer'])
            ->addField('seda_ssl_verify_host', ['type' => 'boolean'])
            ->addField('seda_ssl_cafile', ['type' => 'string']);

        return $schema;
    }

    /**
     * Valide l'onglet SedaGenerator
     *
     * @param Validator $validator
     * @return self
     */
    protected function validateSedaGenerator(Validator $validator): self
    {
        $validator
            ->allowEmptyString('seda_ssl_verify_depth')
            ->nonNegativeInteger('seda_ssl_verify_depth');

        $validator
            ->allowEmptyString('seda_ssl_cafile')
            ->add(
                'seda_ssl_cafile',
                'is_file',
                [
                    'rule' => function ($value) {
                        return is_file($value);
                    },
                    'message' => __("N'est pas un fichier")
                ]
            )
            ->add(
                'seda_ssl_cafile',
                'readable',
                [
                    'rule' => function ($value) {
                        return !is_file(WWW_ROOT . $value) || is_readable(WWW_ROOT . $value);
                    },
                    'message' => __("Ne peut pas être lu, vérifiez les autorisations")
                ]
            );

        return $this;
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $this->validateUtilities($validator)
            ->validateAppearance($validator)
            ->validateEmails($validator)
            ->validateDatasources($validator)
            ->validateSedaGenerator($validator);
        return $validator;
    }

    /**
     * Sauvegarde l'onglet Seda Generator
     *
     * @param array $data
     * @param array $config
     * @return self
     */
    protected function saveSedaGenerator(array $data, array &$config): self
    {
        $booleans = [
            'seda_ssl_verify_peer',
            'seda_ssl_verify_peer_name',
            'seda_ssl_verify_host',
        ];
        $map = [
            'seda_ssl_verify_peer' => 'SedaGenerator.ssl_verify_peer',
            'seda_ssl_verify_peer_name' => 'SedaGenerator.ssl_verify_peer_name',
            'seda_ssl_verify_depth' => 'SedaGenerator.ssl_verify_depth',
            'seda_ssl_verify_host' => 'SedaGenerator.ssl_verify_host',
            'seda_ssl_cafile' => 'SedaGenerator.ssl_cafile',
        ];
        foreach ($map as $dataPath => $configPath) {
            if (in_array($dataPath, $booleans) && isset($data[$dataPath])) {
                $data[$dataPath] = $data[$dataPath] === '1';
            }
            if (isset($data[$dataPath])) {
                $value = $data[$dataPath];
                $config = Hash::insert($config, $configPath, $value);
            } else {
                $config = Hash::remove($config, $configPath);
            }
        }
        $config = Hash::filter(
            $config,
            function ($var) {
                return $var === 0 || $var === 0.0 || $var === '0' || !empty($var) || $var === false;
            }
        );
        return $this;
    }

    /**
     * Hook method to be implemented in subclasses.
     *
     * Used by `execute()` to execute the form's action.
     *
     * @param array $data Form data.
     * @return bool
     * @throws Exception
     */
    protected function _execute(array $data): bool
    {
        $config = json_decode($data['config'], true);

        $this->saveUtilities($data, $config)
            ->saveAppearance($data, $config)
            ->saveEmails($data, $config)
            ->saveSedaGenerator($data, $config);

        ksort($config);
        $json = json_encode($config, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

        try {
            Filesystem::begin('edit_config');
            Filesystem::dumpFile(
                include Config::read('App.paths.path_to_local_config'),
                $json
            );
            Filesystem::commit('edit_config');
            return true;
        } catch (IOException $e) {
            Filesystem::rollback('edit_config');
            trigger_error($e->getMessage());
            return false;
        }
    }
}
