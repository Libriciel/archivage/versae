<?php

/**
 * Versae\Form\InterruptForm
 */

namespace Versae\Form;

use AsalaeCore\Factory\Utility;
use Cake\Core\Configure;
use Cake\Event\EventManager;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Http\Exception\ForbiddenException;
use Cake\I18n\FrozenTime;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use DateTime;
use DateTimeInterface;
use DOMDocument;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;

/**
 * Formulaire de la vignette interruption de service
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class InterruptForm extends Form
{

    /**
     * Constructor
     *
     * @param EventManager|null $eventManager The event manager.
     *                                        Defaults to a new instance.
     * @throws Exception
     */
    public function __construct(EventManager $eventManager = null)
    {
        parent::__construct($eventManager);
        $config = Configure::read('Interruption', []) + [
            'enabled' => false,
            'scheduled' => [
                'begin' => null,
                'end' => null,
            ],
            'periodic' => [
                'begin' => null,
                'end' => null,
            ],
            'enable_whitelist' => true,
            'whitelist_headers' => [
                'X-Asalae-Webservice' => true,
            ],
            'message' => null,
            'message_periodic' => null,
        ];
        $this->_data = [
            'enabled' => $config['enabled'],
            'scheduled__begin' => $config['scheduled']['begin'] ? new DateTime($config['scheduled']['begin']) : null,
            'scheduled__end' => $config['scheduled']['end'] ? new DateTime($config['scheduled']['end']) : null,
            'periodic__begin' => $config['periodic']['begin'],
            'periodic__end' => $config['periodic']['end'],
            'enable_whitelist' => $config['enable_whitelist'],
            'whitelist_headers' => $config['whitelist_headers'],
            'whitelist_headers_inline' => implode(
                ', ',
                array_keys($config['whitelist_headers'])
            ),
            'message' =>
                $config['message']
                ?: __("Indisponibilité temporaire pour cause de maintenance. Veuillez revenir dans un instant."),
            'message_periodic' =>
                $config['message_periodic'] ?: __("Application indisponible durant cette tranche horaire"),
        ];
    }

    /**
     * A hook method intended to be implemented by subclasses.
     *
     * You can use this method to define the schema using
     * the methods on Cake\Form\Schema, or loads a pre-defined
     * schema from a concrete class.
     *
     * @param Schema $schema The schema to customize.
     * @return Schema The schema to use.
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        $schema->addField('enabled', ['type' => 'boolean']);
        $schema->addField('scheduled__begin', ['type' => 'datetime']);
        $schema->addField('scheduled__end', ['type' => 'datetime']);
        $schema->addField('periodic__begin', ['type' => 'time']);
        $schema->addField('periodic__end', ['type' => 'time']);
        $schema->addField('enable_whitelist', ['type' => 'boolean']);
        $schema->addField('whitelist_headers', ['type' => 'array']);
        $schema->addField('whitelist_headers_inline', ['type' => 'string']);
        $schema->addField('message', ['type' => 'text']);
        $schema->addField('message_periodic', ['type' => 'text']);

        return $schema;
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->requirePresence('enabled')
            ->boolean('enabled');
        $validator
            ->add(
                'scheduled__begin',
                'datetime',
                [
                    'rule' => function ($value) {
                        return $this->getDatetime($value) !== null;
                    },
                    'message' => __("Le format de date n'est pas correct"),
                ]
            )
            ->requirePresence('scheduled__begin')
            ->allowEmptyDateTime('scheduled__begin');
        $validator
            ->add(
                'scheduled__end',
                'datetime',
                [
                    'rule' => function ($value) {
                        return $this->getDatetime($value) !== null;
                    },
                    'message' => __("Le format de date n'est pas correct"),
                ]
            )
            ->requirePresence('scheduled__end')
            ->allowEmptyDateTime('scheduled__end');
        $validator
            ->time('periodic__begin')
            ->requirePresence('periodic__begin')
            ->allowEmptyTime(
                'periodic__begin',
                null,
                function ($context) {
                    return empty($context['data']['periodic__end']);
                }
            );
        $validator
            ->time('periodic__end')
            ->requirePresence('periodic__end')
            ->allowEmptyTime(
                'periodic__end',
                null,
                function ($context) {
                    return empty($context['data']['periodic__begin']);
                }
            );
        $validator
            ->requirePresence('enable_whitelist')
            ->boolean('enable_whitelist');
        $validator
            ->allowEmptyArray('whitelist_headers');
        $validator
            ->requirePresence('whitelist_headers_inline');
        $noscript = [
            'rule' => function ($value) {
                if (!$value) {
                    return true;
                }
                $dom = new DOMDocument();
                @$dom->loadHTML($value);
                return $dom->getElementsByTagName('script')->count() === 0;
            },
            'message' => __("Utilisation de script interdite"),
        ];
        $validator->add('message', 'noscript', $noscript)
            ->allowEmptyString(
                'message',
                null,
                function ($context) {
                    return !(Hash::get($context, 'data.enabled')
                        || Hash::get($context, 'data.scheduled__begin')
                        || Hash::get($context, 'data.scheduled__end')
                    );
                }
            );
        $validator->add('message_periodic', 'noscript', $noscript)
            ->allowEmptyString(
                'message_periodic',
                null,
                function ($context) {
                    return !(Hash::get($context, 'data.periodic__begin')
                        || Hash::get($context, 'data.periodic__end')
                    );
                }
            );

        return $validator;
    }

    /**
     * Hook method to be implemented in subclasses.
     *
     * Used by `execute()` to execute the form's action.
     *
     * @param array $data Form data.
     * @return bool
     * @throws Exception
     */
    protected function _execute(array $data): bool
    {
        $interruption = [
            'enabled' => (bool)$data['enabled'],
            'scheduled' => [
                'begin' => $data['scheduled__begin']
                    ? $this->getDatetime($data['scheduled__begin'])->format(DATE_RFC3339)
                    : null,
                'end' => $data['scheduled__end']
                    ? $this->getDatetime($data['scheduled__end'])->format(DATE_RFC3339)
                    : null,
            ],
            'periodic' => [
                'begin' => $data['periodic__begin'] ?: null,
                'end' => $data['periodic__end'] ?: null,
            ],
            'enable_whitelist' => (bool)$data['enable_whitelist'],
            'whitelist_headers' => array_fill_keys(
                array_map(
                    'trim',
                    explode(',', $data['whitelist_headers_inline'])
                ),
                true
            ),
            'message' => $data['message'],
            'message_periodic' => $data['message_periodic'],
        ];

        $path = $this->getAppLocalConfigFileUri();
        if (!is_writable($path)) {
            throw new ForbiddenException(sprintf("Can't write on %s", $path));
        }
        $confJson = file_get_contents($path);
        $conf = json_decode($confJson, true);
        $conf['Interruption'] = $interruption;
        ksort($conf);

        /** @var Filesystem $Filesystem */
        $Filesystem = Utility::get('Filesystem');
        try {
            $Filesystem->begin('toggle_interruption');
            $Filesystem->setSafemode(true);
            $Filesystem->dumpFile($path, json_encode($conf, JSON_PRETTY_PRINT));
            $Filesystem->commit('toggle_interruption');
            return true;
        } catch (IOException $e) {
            $Filesystem->rollback('toggle_interruption');
            trigger_error($e->getMessage());
            return false;
        }
    }

    /**
     * Retourne l'uri du fichier de configuration local
     */
    private function getAppLocalConfigFileUri()
    {
        $pathToLocalConfig = Configure::read('App.paths.path_to_local_config');
        if (is_readable($pathToLocalConfig)) {
            $appLocalConfigFileUri = include $pathToLocalConfig;
        } elseif (is_readable('/data/config/app_local.json')) {
            $appLocalConfigFileUri = '/data/config/app_local.json';
        } elseif (is_readable(CONFIG . 'app_local.json')) {
            $appLocalConfigFileUri = CONFIG . 'app_local.json';
        } else {
            $appLocalConfigFileUri = '';
        }
        return $appLocalConfigFileUri;
    }

    /**
     * Permet de créer des dates à partir du format français
     * @param string|DateTimeInterface $value
     * @return FrozenTime|null
     */
    private function getDatetime($value)
    {
        if (is_string($value)) {
            $value = FrozenTime::parseDateTime($value);
        } elseif ($value instanceof DateTimeInterface) {
            $tz = $value->getTimezone();
            $time = $value->format('Y-m-d H:i:s.u');
            $value = new FrozenTime($time, $tz);
        }

        return $value;
    }
}
