<?php

/**
 * Versae\Form\FormMakerForm
 */

namespace Versae\Form;

use AsalaeCore\Factory\Utility;
use AsalaeCore\Form\MessageSchema\Seda10Schema;
use AsalaeCore\Http\Client;
use AsalaeCore\Utility\LimitBreak;
use AsalaeCore\Utility\Translit;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Http\Client\Exception\NetworkException;
use Cake\Http\Client\Response;
use Cake\Http\Exception\HttpException;
use Cake\I18n\I18n;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\ValidationRule;
use Cake\Validation\ValidationSet;
use Cake\Validation\Validator;
use Closure;
use Datacompressor\Exception\DataCompressorException;
use Datacompressor\Utility\DataCompressor;
use DateInterval;
use DateTime;
use DateTimeZone;
use DirectoryIterator;
use DOMDocument;
use DOMElement;
use DOMXPath;
use Exception;
use Flow\JSONPath\JSONPath;
use Flow\JSONPath\JSONPathException;
use IntlDateFormatter;
use Libriciel\Filesystem\Utility\Filesystem;
use LibXMLError;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Versae\Exception\FormMakerException;
use Versae\Exception\GenericException;
use Versae\Model\Entity\Form as FormEntity;
use Versae\Model\Entity\FormExtractor;
use Versae\Model\Table\CountersTable;
use Versae\Model\Table\FormExtractorsTable;
use Versae\Model\Table\FormInputsTable;
use Versae\Model\Table\FormUnitsTable;
use Versae\Utility\Csv;
use Versae\Utility\Twig;
use ZMQSocketException;

/**
 * Formulaire du formulaire généré
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FormMakerForm extends Form
{
    public const DEFAULT_ACCESS_CODE = 'AR038';
    /**
     * @var string
     */
    public $xml;
    /**
     * @var array
     */
    public $twigVars;
    /**
     * @var int
     */
    private $form_id;
    /**
     * @var int
     */
    private $user_id;
    /**
     * @var FormEntity
     */
    private $form;
    /**
     * @var EntityInterface|null
     */
    private $deposit = null;
    /**
     * @var array
     */
    private $puids;
    /**
     * @var array
     */
    private $mimes;
    /**
     * @var array
     */
    private $files = [];
    /**
     * @var array
     */
    private $attachments = [];
    /**
     * @var array
     */
    private $sf = [];
    /**
     * @var array
     */
    private static $formatNames = [];
    /**
     * @var EntityInterface[]
     */
    private array $inputs = [];
    /**
     * @var IntlDateFormatter
     */
    private IntlDateFormatter $formatterDate;
    /**
     * @var IntlDateFormatter
     */
    private IntlDateFormatter $formatterDatetime;
    /**
     * @var DateTimeZone
     */
    private DateTimeZone $timezone;

    /**
     * Fait appel au constructeur et initialise les attributs importants
     * @param int                  $form_id
     * @param int                  $user_id
     * @param EntityInterface|null $deposit
     * @return FormMakerForm
     * @throws Exception
     */
    public static function create(int $form_id, int $user_id, EntityInterface $deposit = null): FormMakerForm
    {
        $form = new self();
        $form->form_id = $form_id;
        $form->user_id = $user_id;
        $Forms = TableRegistry::getTableLocator()->get('Forms');
        $form->form = $Forms->find()
            ->where(['Forms.id' => $form_id])
            ->contain(
                [
                    'FormTransferHeaders' => ['FormVariables'],
                    'OrgEntities' => ['ArchivalAgencies'],
                ]
            )
            ->firstOrFail();
        $archivalAgencyId = Hash::get($form->form, 'org_entity.archival_agency.id');
        if (!$archivalAgencyId) { // utile si on oubli app.TypeEntities dans les fixtures
            throw new GenericException(
                __(
                    "Service d'archives non trouvé pour le form {0} et l'org_entity {1}",
                    $form->form->get('id'),
                    Hash::get($form, 'org_entity.id')
                )
            );
        }
        if ($deposit) {
            $form->deposit = $deposit;
        }

        // Formatage des données
        $form->formatterDate = new IntlDateFormatter(
            I18n::getDefaultLocale(),
            IntlDateFormatter::SHORT,
            IntlDateFormatter::NONE
        );
        $form->formatterDatetime = new IntlDateFormatter(
            I18n::getDefaultLocale(),
            IntlDateFormatter::SHORT,
            IntlDateFormatter::MEDIUM
        );
        $form->timezone = new DateTimeZone(Configure::read('App.timezone'));

        return $form;
    }

    /**
     * Formulaire pour le preview (pas de table)
     * @param array $inputs
     * @param int   $id
     * @return FormMakerForm
     */
    public static function createPreview(array $inputs, int $id): FormMakerForm
    {
        $form = new self();
        $form->inputs = $inputs;
        $form->form_id = $id;
        return $form;
    }

    /**
     * Vérifie que sedaGenerator fonctionne
     * @return bool
     * @throws Exception
     */
    public static function pingSedaGenerator(): bool
    {
        try {
            $config = Configure::read('SedaGenerator');
            $url = $config['url'];
            unset($config['url']);
            /** @var Client $client */
            $client = Utility::get(Client::class, ['curl' => [CURLOPT_TIMEOUT => 2]]);
            $response = $client->get($url . '/ping');
            if ($response->getStatusCode() === 200) {
                return true;
            }
        } catch (NetworkException) {
        }
        return false;
    }

    /**
     * Donne la liste des fieldsets sous la forme: [$ord => [$index => true]]
     * @return array[]
     */
    private function listFieldsets(): array
    {
        $fieldsets = [];
        if ($this->deposit) {
            foreach ($this->deposit->get('deposit_values') ?: [] as $val) {
                $ord = Hash::get($val, 'form_input.form_fieldset.ord');
                $fieldsets[$ord][$val->get('fieldset_index')] = true;
            }
        }
        return $fieldsets;
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        if (empty($this->form_id)) {
            return $validator;
        }

        $fieldsets = $this->listFieldsets();

        /** @var EntityInterface $formInput */
        foreach ($this->getInputs() as $formInput) {
            if (Hash::get($formInput, 'form_fieldset.repeatable')) {
                $ord = Hash::get($formInput, 'form_fieldset.ord');
                $fieldsets[$ord][0] = true;
                foreach ($fieldsets as $ord => $indexes) {
                    foreach (array_keys($indexes) as $index) {
                        $formInputClone = clone $formInput;
                        $formInputClone->set(
                            'name',
                            sprintf(
                                'section-%d.%d.%s',
                                $ord,
                                $index,
                                $formInput->get('name')
                            )
                        );
                        $this->appendFormInputToValidation($validator, $formInputClone);
                    }
                }
            } else {
                $this->appendFormInputToValidation($validator, $formInput);
            }
        }

        // expend les validations des sections multiples
        /** @var ValidationSet $validationSet */
        foreach ($validator->getIterator() as $field => $validationSet) {
            $this->expandRepeatableInValidation($validator, $field, $validationSet);
        }

        return $validator;
    }

    /**
     * Transforme un champ nommé "section-0.0.fieldname" en rule pour "section-0"
     * @param Validator     $validator
     * @param string        $fieldName
     * @param ValidationSet $validationSet
     * @return void
     */
    private function expandRepeatableInValidation(
        Validator $validator,
        string $fieldName,
        ValidationSet $validationSet
    ) {
        if (!preg_match('/^(section-\d+)\.(\d+)\.(.*)/', $fieldName, $m)) {
            return;
        }
        /** @var ValidationRule $rule */
        foreach ($validationSet->rules() as $ruleName => $rule) {
            $path = sprintf('%d.%s', $m[2], $m[3]);
            $validator->add(
                $m[1],
                sprintf('%d-%s-%s', $m[2], $m[3], $ruleName),
                [
                    'rule' => function ($value, $context) use ($rule, $path) {
                        $val = Hash::get($value, $path);
                        return $val
                            ? $rule->process($val, $context['providers'], $context)
                            : true;
                    },
                ]
            );
        }
    }

    /**
     * Validation des champs requis
     * @param string $name
     * @return Closure
     */
    private function validateRequiredField(string $name)
    {
        return function ($context) use ($name) {
            $hiddenFields = json_decode(Hash::get($context, 'data.hidden_fields')) ?: [];
            // si le champ est dans les champs cachés
            if (in_array($name, $hiddenFields)) {
                return false;
            }
            // si le champ est dans une section répétable
            foreach ($context['data'] ?? [] as $key => $value) {
                if (!is_array($value) || !preg_match('/^section-\d+$/', $key)) {
                    continue;
                }
                foreach ($context['data'][$key] as $val) {
                    if (isset($val[$name])) {
                        return false;
                    }
                }
            }
            return true;
        };
    }

    /**
     * Vérifie une date (sans risquer une exception)
     * @param string $value
     * @return bool
     */
    public function validateDate($value): bool
    {
        $formatter = new IntlDateFormatter(
            I18n::getDefaultLocale(),
            IntlDateFormatter::SHORT,
            IntlDateFormatter::NONE
        );
        return (bool)$formatter->parse($value);
    }

    /**
     * Vérifie un datetime (sans risquer une exception)
     * @param string $value
     * @return bool
     */
    public function validateDatetime($value): bool
    {
        // Ajoute :00 à la fin s'il manque les secondes
        if (preg_match('/^(.*) (\d+):(\d+)$/', $value, $m)) {
            $value = $m[1] . " $m[2]:$m[3]:00";
        }
        $formatter = new IntlDateFormatter(
            I18n::getDefaultLocale(),
            IntlDateFormatter::SHORT,
            IntlDateFormatter::MEDIUM
        );
        return (bool)$formatter->parse($value);
    }

    /**
     * Vérifie que le formulaire est prêt à l'emploi
     * @return bool
     */
    public function isValidForm(): bool
    {
        if (empty($this->form_id)) {
            return true;
        }
        // Vérifie que les règles de gestion soit complètes
        $FormUnitManagements = TableRegistry::getTableLocator()->get('FormUnitManagements');
        $appraisalCount = $FormUnitManagements->find()
            ->select(['unit_name' => 'FormUnits.name'])
            ->innerJoinWith('FormUnits')
            ->where(['FormUnits.form_id' => $this->form_id])
            ->andWhere(['FormUnitManagements.name LIKE' => 'AppraisalRule_%'])
            ->group(['form_unit_id', 'FormUnits.name'])
            ->having(['COUNT(*) BETWEEN 1 AND 2'])
            ->all();
        if ($appraisalCount->count() === 1) {
            $this->setErrors(
                [
                    'AppraisalRule' => [
                        'count' => __(
                            "La communicabilité a partiellement été définie dans l'unité d'archives ''{0}''",
                            $appraisalCount->first()->get('unit_name')
                        ),
                    ],
                ]
            );
        } elseif ($appraisalCount->count() > 1) {
            $names = $appraisalCount->map(fn(EntityInterface $v) => h($v->get('unit_name')))->toArray();
            $this->setErrors(
                [
                    'AppraisalRule' => [
                        'count' => __(
                            "La communicabilité a partiellement été définie "
                            . "dans les unités d'archives suivantes : ''{0}''",
                            implode("', '", $names)
                        ),
                    ],
                ]
            );
        }
        $access = $FormUnitManagements->find()
            ->select(['unit_name' => 'FormUnits.name'])
            ->innerJoinWith('FormUnits')
            ->where(['FormUnits.form_id' => $this->form_id])
            ->andWhere(['FormUnitManagements.name LIKE' => 'Access%Rule_%'])
            ->group(['form_unit_id', 'FormUnits.name'])
            ->having(['COUNT(*) = 1'])
            ->all();
        if ($access->count() === 1) {
            $this->setErrors(
                [
                    'AccessRule' => [
                        'count' => __(
                            "La restriction d'accès a partiellement été définie dans l'unité d'archives ''{0}''",
                            $access->first()->get('unit_name')
                        ),
                    ],
                ]
            );
        } elseif ($access->count() > 1) {
            $names = $access->map(fn(EntityInterface $v) => h($v->get('unit_name')))->toArray();
            $this->setErrors(
                [
                    'AccessRule' => [
                        'count' => __(
                            "La restriction d'accès a partiellement été définie "
                            . "dans les unités d'archives suivantes : ''{0}''",
                            implode("', '", $names)
                        ),
                    ],
                ]
            );
        }

        return !$this->getErrors();
    }

    /**
     * A hook method intended to be implemented by subclasses.
     *
     * You can use this method to define the schema using
     * the methods on Cake\Form\Schema, or loads a pre-defined
     * schema from a concrete class.
     *
     * @param Schema $schema The schema to customize.
     * @return Schema The schema to use.
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        if (empty($this->form_id)) {
            return $schema;
        }

        $fieldsets = $this->listFieldsets();

        /** @var EntityInterface $formInput */
        foreach ($this->getInputs() as $formInput) {
            $this->appendFormInputToSchema($schema, $formInput, $fieldsets);
        }
        return $schema;
    }

    /**
     * Ajoute un input au schema
     * @param Schema          $schema
     * @param EntityInterface $formInput
     * @param array           $fieldsets
     * @return void
     * @noinspection PhpMissingBreakStatementInspection
     */
    private function appendFormInputToSchema(
        Schema $schema,
        EntityInterface $formInput,
        array $fieldsets
    ) {
        $name = $formInput->get('name');
        switch ($formInput->get('type')) {
            case FormInputsTable::TYPE_CHECKBOX:        // case à cocher
                $type = 'boolean';
                break;
            case FormInputsTable::TYPE_DATE:            // date
                $type = 'date';
                break;
            case FormInputsTable::TYPE_DATETIME:        // date et heure
                $type = 'datetime';
                break;
            case FormInputsTable::TYPE_NUMBER:          // champ numérique
                $type = 'integer';
                break;
            case FormInputsTable::TYPE_PARAGRAPH:       // paragraphe
                return;
            case FormInputsTable::TYPE_TEXTAREA:        // zone de texte sur plusieurs lignes
                $type = 'text';
                break;
            case FormInputsTable::TYPE_FILE:            // fichier
            case FormInputsTable::TYPE_TEXT:            // zone de texte simple
                if ($formInput->get('multiple')) {
                    $name .= '[]';
                }
            // intentional fallthrough
            // no break
            case FormInputsTable::TYPE_ARCHIVE_FILE:    // fichier compressé
            case FormInputsTable::TYPE_EMAIL:           // email
            case FormInputsTable::TYPE_MULTI_CHECKBOX:  // case à cocher multiple
            case FormInputsTable::TYPE_RADIO:           // bouton radio
            case FormInputsTable::TYPE_SELECT:          // sélection entre plusieurs options
            default:
                $type = 'string';
        }
        if (Hash::get($formInput, 'form_fieldset.repeatable')) {
            $ord = Hash::get($formInput, 'form_fieldset.ord');
            $fieldsets[$ord][0] = true;
            foreach ($fieldsets as $ord => $indexes) {
                foreach (array_keys($indexes) as $index) {
                    $schema->addField(
                        sprintf(
                            'section-%d.%d.%s',
                            $ord,
                            $index,
                            $name
                        ),
                        [
                            'type' => $type,
                            'default' => $formInput->get('default_value'),
                        ]
                    );
                }
            }
        } else {
            $schema->addField(
                $name,
                [
                    'type' => $type,
                    'default' => $formInput->get('default_value'),
                ]
            );
        }
    }

    /**
     * Hook method to be implemented in subclasses.
     *
     * Used by `execute()` to execute the form's action.
     *
     * @param array $data Form data.
     * @return bool
     * @throws Exception
     */
    protected function _execute(array $data): bool
    {
        if (empty($this->form_id)) {
            return false;
        }
        if (!$this->deposit) {
            throw new GenericException(
                __("FormMakerForm doit être instancié avec un deposit avant de lancer execute()")
            );
        }
        $this->puids = array_map(
            function ($v) {
                return $v['value'];
            },
            Seda10Schema::getXsdOptions('filetype')
        );
        $this->mimes = array_map(
            function ($v) {
                return $v['value'];
            },
            Seda10Schema::getXsdOptions('mime')
        );
        $additionnalData = $data['additionnalData'] ?? [];
        unset($data['additionnalData']);
        if (isset($additionnalData['archival_agency_id'])) {
            $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
            $additionnalData['archival_agency'] = $OrgEntities->find()
                ->select(['identifier', 'name'])
                ->where(['id' => $additionnalData['archival_agency_id']])
                ->disableHydration()
                ->firstOrFail();
        }
        if (isset($additionnalData['transferring_agency_id'])) {
            $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
            $additionnalData['transferring_agency'] = $OrgEntities->find()
                ->select(['identifier', 'name'])
                ->where(['id' => $additionnalData['transferring_agency_id']])
                ->disableHydration()
                ->firstOrFail();
        }
        if (isset($additionnalData['originating_agency_id'])) {
            $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
            $additionnalData['originating_agency'] = $OrgEntities->find()
                ->select(['identifier', 'name'])
                ->where(['id' => $additionnalData['originating_agency_id']])
                ->disableHydration()
                ->firstOrFail();
        }
        if (isset($additionnalData['user_id'])) {
            $Users = TableRegistry::getTableLocator()->get('Users');
            $additionnalData['user'] = $Users->find()
                ->select(['username', 'name', 'email'])
                ->where(['id' => $additionnalData['user_id']])
                ->disableHydration()
                ->firstOrFail();
        }
        if (isset($additionnalData['org_entity_id'])) {
            $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
            $additionnalData['org_entity'] = $OrgEntities->find()
                ->select(['identifier', 'name'])
                ->where(['id' => $additionnalData['org_entity_id']])
                ->disableHydration()
                ->firstOrFail();
        }

        try {
            $this->emit(__("Préparation des données..."));
            $data = $this->prepareData($data);
            $this->emit(__("Déplacement des fichiers..."));
            $this->lockFiles($data);
            $this->emit(__("Calcul des variables et extracteurs..."));
            $this->twigVars = $this->getTwigVars($data, $additionnalData);
            $this->emit(__("Structuration des unités d'archives..."));
            $requestData = Hash::filter(
                $this->getRequestData($this->twigVars),
                fn($d) => $d !== '' && $d !== null && $d !== []
            );

            $pathErrors = [];
            foreach (Hash::extract($requestData, 'ArchiveUnits.{n}.Files.{s}.Filename') as $path) {
                foreach (explode(DS, $path) as $pathElement) {
                    if (strlen($pathElement) > 254) {
                        $pathErrors[$path] = __("Le chemin contient un élément trop long : {0}", $pathElement);
                    }
                }
            }
            if (count($pathErrors)) {
                $this->setErrors(['Files' => $pathErrors]);
                return false;
            }

            $this->emit(__("Génération du message SEDA..."));

            $requestData = [
                // "seda1.0" -> "1.0"
                'version' => substr($this->form->get('seda_version'), strlen('seda')),
                'template' => 'generic',
                'mapper' => 'versae',
                'no-validation' => true,
            ] + $requestData;

            $result = $this->sedaGenerator($requestData);
            if ($result->getStatusCode() >= 400) {
                $this->handleSedaGeneratorErrors($result);
                return false;
            } else {
                file_put_contents(TMP . 'debug_last_form_generate.xml', $result->getStringBody());
                $this->xml = $result->getStringBody();
                $dom = new DOMDocument();
                $dom->loadXML($this->xml);
                switch ($this->form->get('seda_version')) {
                    case 'seda1.0':
                        $schema = SEDA_V10_XSD;
                        break;
                    case 'seda2.1':
                        $schema = SEDA_V21_XSD;
                        break;
                    case 'seda2.2':
                        $schema = SEDA_V22_XSD;
                        break;
                    default:
                        throw FormMakerException::create(
                            'Schema',
                            __("Version du seda non pris en compte")
                        );
                }
                libxml_use_internal_errors(true);
                if (!$dom->schemaValidate($schema)) {
                    $errors = [];
                    /** @var LibXMLError $err */
                    foreach (libxml_get_errors() as $i => $err) {
                        $errors['error_' . $i] = sprintf(
                            'line: %d ; column: %d ; message: %s',
                            $err->line ?? 0,
                            $err->column ?? 0,
                            $err->message ?? __("Une erreur inattendue a eu lieu")
                        );
                    }
                    libxml_clear_errors();
                    $this->setErrors(['SchemaValidation' => $errors]);
                    return false;
                }
            }
        } catch (FormMakerException $e) {
            $this->setErrors([$e->field => ['err' => $e->getMessage()]]);
            $this->emit(__("Échec lors de la génération du message"));
            return false;
        }

        $this->emit(__("Message généré avec succès."));
        return parent::_execute($data);
    }

    /**
     * Prépare les données avant d'être utilisés dans du twig
     * @param array $data
     * @return array
     * @throws Exception
     */
    private function prepareData(array $data): array
    {
        $FormInputs = TableRegistry::getTableLocator()->get('FormInputs');
        $query = $this->inputs
            ?: $FormInputs->find()->where(['form_id' => $this->form_id]);
        /** @var EntityInterface $formInput */
        foreach ($query as $formInput) {
            /** @var EntityInterface $fieldset */
            $fieldset = $formInput->get('form_fieldset');
            if ($fieldset && $fieldset->get('repeatable')) {
                $section = sprintf('section-%d', $fieldset->get('ord'));
                foreach (Hash::get($data, $section, []) as $key => $values) {
                    if (!empty($data[$section][$key][$formInput->get('name')])) {
                        $ref = &$data[$section][$key][$formInput->get('name')];
                        $ref = $this->formatDateToIso($formInput, $ref);
                    }
                }
                continue;
            }
            if (empty($data[$formInput->get('name')])) {
                continue;
            }
            $data[$formInput->get('name')] = $this->formatDateToIso(
                $formInput,
                $data[$formInput->get('name')]
            );
        }
        return $data;
    }

    /**
     * Prend une date au format fr et la converti en Y-m-d(\TH:i:s)? pour
     * qu'elle soit interprétable par twig
     * @param EntityInterface $formInput
     * @param string|array    $value
     * @return string|array
     * @throws Exception
     */
    private function formatDateToIso(EntityInterface $formInput, $value)
    {
        if (!is_string($value)) {
            return $value;
        }
        if ($formInput->get('type') === FormInputsTable::TYPE_DATE) {
            $timestamp = $this->formatterDate->parse($value);
            $date = new DateTime('@' . $timestamp);
            $date->setTimezone($this->timezone);
            return $date->format('Y-m-d');
        } elseif ($formInput->get('type') === FormInputsTable::TYPE_DATETIME) {
            // Ajoute :00 à la fin s'il manque les secondes
            if (preg_match('/^(.*) (\d+):(\d+)$/', $value, $m)) {
                $value = $m[1] . " $m[2]:$m[3]:00";
            }
            $timestamp = $this->formatterDatetime->parse($value);
            $date = new DateTime('@' . $timestamp);
            $date->setTimezone($this->timezone);
            return $date->format(DATE_RFC3339);
        }
        return $value;
    }

    /**
     * Calcul de l'ensemble des variables utilisateurs utilisables par twig
     * @param array $data
     * @param array $additionnalData
     * @return array
     * @throws LoaderError
     * @throws SyntaxError
     */
    private function getTwigVars(array $data, array $additionnalData = []): array
    {
        $twigVars = [
            'meta' => $additionnalData,
            'input' => $data,
            'extract' => $this->getExtractors($data),
            'var' => [],
        ];
        $twigVars['meta'] = $this->addFileMetadata($twigVars);
        $twigVars['meta'] = $this->addRepetableSectionMetadata($twigVars['meta'], $data);
        $twigVars['var'] = $this->getVariables($twigVars);
        return $twigVars;
    }

    /**
     * Ajoute le count de sections répétables
     * @param array $additionnalData
     * @param array $data
     * @return array
     */
    private function addRepetableSectionMetadata(array $additionnalData, array $data): array
    {
        foreach ($data as $key => $values) {
            if (preg_match('/^section-\d+$/', $key)) {
                $additionnalData['repeatable_counts'][str_replace('-', '_', $key)] = count($values);
            }
        }
        return $additionnalData;
    }

    /**
     * Donne le xml selon data
     * @param array $data
     * @return Response
     * @throws Exception
     */
    private function sedaGenerator(array $data): Response
    {
        $config = Configure::read('SedaGenerator');
        $url = $config['url'];
        unset($config['url']);

        $Forms = TableRegistry::getTableLocator()->get('Forms');
        /** @var EntityInterface $form */
        $form = $Forms->get($this->form_id);
        $action = 'generate?template=';
        switch ($form->get('seda_version')) {
            case 'seda1.0':
                $action .= 'seda_1.0_versae.xml.twig';
                break;
            case 'seda2.1':
                $action .= 'seda_2.1_versae.xml.twig';
                break;
            case 'seda2.2':
                $action .= 'seda_2.2_versae.xml.twig';
                break;
        }

        $encoded = json_encode($data);
        $neededTime = (int)(strlen($encoded) / 100000) + 30;
        $config['curl'][CURLOPT_TIMEOUT] = $neededTime; // 2.5Go ~= 7h30

        /** @var Client $client */
        $client = get_class(Utility::get(Client::class));
        $client = new $client($config);
        return $client->post(
            $url . '/' . $action,
            $encoded,
            [
                'type' => 'json',
                'curl' => [CURLOPT_TIMEOUT => $neededTime],
            ]
        );
    }

    /**
     * Extraction des données
     * @param string        $targetFile
     * @param FormExtractor $formExtractor
     * @return mixed|string|null
     * @throws FormMakerException
     */
    private function extractData(string $targetFile, FormExtractor $formExtractor)
    {
        /**@noinspection PhpSwitchCanBeReplacedWithMatchExpressionInspection mauvais remplacement
         * (a retester plus tard)*/
        switch ($formExtractor->get('data_format')) {
            case FormExtractorsTable::DATA_FORMAT_XML:
                $response = $this->extractDataXml($targetFile, $formExtractor);
                break;
            case FormExtractorsTable::DATA_FORMAT_JSON:
                $response = $this->extractDataJson($targetFile, $formExtractor);
                break;
            case FormExtractorsTable::DATA_FORMAT_CSV:
                $response = $this->extractDataCsv($targetFile, $formExtractor);
                break;
            default:
                throw FormMakerException::create(
                    'FormExtractor/' . $formExtractor->get('name'),
                    __(
                        "Erreur d'extraction : Le type de fichier "
                        . "dans l'extracteur ''{0}'' n'est pas pris en charge",
                        h(Hash::get($formExtractor, 'name'))
                    )
                );
        }
        $response = (array)$response;
        if (Hash::get($formExtractor, 'multiple')) {
            return array_unique(Hash::flatten($response));
        } else {
            return current($response);
        }
    }

    /**
     * Extraction des données XML
     * @param string        $targetFile
     * @param FormExtractor $formExtractor
     * @return mixed|string|null
     * @throws FormMakerException
     * @noinspection PhpReturnDocTypeMismatchInspection - bug, string doit faire parti du return
     */
    private function extractDataXml(string $targetFile, FormExtractor $formExtractor)
    {
        $dom = new DOMDocument();
        $initial = libxml_use_internal_errors();
        libxml_use_internal_errors(true);
        $loaded = $dom->load($targetFile);
        libxml_use_internal_errors($initial);
        if (!$loaded) {
            throw FormMakerException::create(
                'FormExtractor/' . $formExtractor->get('name'),
                __(
                    "Erreur d'extraction : le fichier dans le champ ''{0}'' n'est pas un fichier XML",
                    h(Hash::get($formExtractor, 'form_input.name'))
                )
            );
        }

        $xpath = new DOMXPath($dom);
        if ($namespaces = $formExtractor->get('namespace')) {
            $namespaces = json_decode($namespaces, true);
        }
        foreach ($namespaces ?: [] as $values) {
            $xpath->registerNamespace($values['prefix'], $values['namespace']);
        }
        $node = @$xpath->query($formExtractor->get('data_path'));
        if (!$node) {
            throw FormMakerException::create(
                'FormExtractor/' . $formExtractor->get('name'),
                __(
                    "Erreur d'extraction : Le chemin indiqué est incorrect dans l'extracteur ''{0}''",
                    h(Hash::get($formExtractor, 'name'))
                )
            );
        } elseif ($node->count() > 1) {
            $response = [];
            foreach ($node as $n) {
                $response[] = $n->nodeValue;
            }
        } elseif ($node->count() >= 1) {
            $response = $node->item(0)->nodeValue;
        } else {
            throw FormMakerException::create(
                'FormExtractor/' . $formExtractor->get('name'),
                __(
                    "Erreur d'extraction : Le chemin indiqué "
                    . "dans l'extracteur ''{0}'' n'a pas été trouvé dans le document",
                    h(Hash::get($formExtractor, 'name'))
                )
            );
        }
        return $response;
    }

    /**
     * Extraction des données JSON
     * @param string        $targetFile
     * @param FormExtractor $formExtractor
     * @return mixed|string|null
     * @throws FormMakerException
     */
    private function extractDataJson(string $targetFile, FormExtractor $formExtractor)
    {
        $json = new JSONPath(
            json_decode(file_get_contents($targetFile), true)
        );
        try {
            $node = $json->find($formExtractor->get('data_path'));
            if ($node->count() === 0) {
                throw FormMakerException::create(
                    'FormExtractor/' . $formExtractor->get('name'),
                    __(
                        "Erreur d'extraction : Le chemin indiqué "
                        . "dans l'extracteur ''{0}'' n'a pas été trouvé dans le document",
                        h(Hash::get($formExtractor, 'name'))
                    )
                );
            }
            if ($node->count() > 1) {
                $response = [];
                foreach ($node as $n) {
                    $response[] = $n instanceof JSONPath ? $n->getData() : $n;
                }
            } else {
                $response = $node->first();
            }
        } catch (JSONPathException) {
            throw FormMakerException::create(
                'FormExtractor/' . $formExtractor->get('name'),
                __(
                    "Erreur d'extraction : Le chemin indiqué est incorrect dans l'extracteur ''{0}''",
                    h(Hash::get($formExtractor, 'name'))
                )
            );
        }
        return $response;
    }

    /**
     * Extraction des données CSV
     * @param string        $targetFile
     * @param FormExtractor $formExtractor
     * @return string|null
     * @throws FormMakerException
     */
    private function extractDataCsv(string $targetFile, FormExtractor $formExtractor)
    {
        try {
            $csv = new Csv($targetFile);
            $response = $csv->get($formExtractor->get('data_path'));
        } catch (GenericException) {
            throw FormMakerException::create(
                'FormExtractor/' . $formExtractor->get('name'),
                __(
                    "Erreur d'extraction : Le chemin indiqué est incorrect dans l'extracteur ''{0}''",
                    h(Hash::get($formExtractor, 'name'))
                )
            );
        }
        if (empty($response)) {
            throw FormMakerException::create(
                'FormExtractor/' . $formExtractor->get('name'),
                __(
                    "Erreur d'extraction : Le chemin indiqué "
                    . "dans l'extracteur ''{0}'' n'a pas été trouvé dans le document",
                    h(Hash::get($formExtractor, 'name'))
                )
            );
        }
        return $response;
    }

    /**
     * Calcul des extracteurs
     * @param array $data
     * @return array
     * @throws Exception
     */
    private function getExtractors(array $data): array
    {
        $FormExtractors = TableRegistry::getTableLocator()->get('FormExtractors');
        $query = $FormExtractors->find()
            ->where(['FormExtractors.form_id' => $this->form_id])
            ->contain(
                [
                    'FormInputs' => [
                        'FormFieldsets',
                    ],
                ]
            );
        $extractors = [];
        /** @var FormExtractor $formExtractor */
        foreach ($query as $formExtractor) {
            /** @var EntityInterface $fieldset */
            $fieldset = Hash::get($formExtractor, 'form_input.form_fieldset');
            if ($fieldset && $fieldset->get('repeatable')) {
                $section = sprintf('section-%d', $fieldset->get('ord'));
                foreach (Hash::get($data, $section, []) as $key => $values) {
                    foreach ($values as $name => $value) {
                        $extractors[$section][$key]
                            = $this->extractFromFiles([$name => $value], $formExtractor);
                    }
                }
            } else {
                if ($formExtractor->get('type') === FormExtractorsTable::TYPE_WEBSERVICE) {
                    $extractors[$formExtractor->get('name')] = $this->downloadAndExtract($formExtractor);
                } else {
                    $extractors = Hash::merge(
                        $extractors,
                        $this->extractFromFiles($data, $formExtractor)
                    );
                }
            }
        }
        return $extractors;
    }

    /**
     * Appel d'un webservice pour extraction
     * @param FormExtractor $formExtractor
     * @return array|mixed|string|null
     * @throws FormMakerException
     */
    private function downloadAndExtract(FormExtractor $formExtractor)
    {
        $inputName = Hash::get($formExtractor, 'form_input.name');
        $clientData = $formExtractor->toArray();
        $params = [
            'auth' => [
                'username' => $clientData['username'],
                'password' => $clientData['password'],
            ],
            'ssl_verify_peer' => $clientData['ssl_verify_peer'],
            'ssl_verify_peer_name' => $clientData['ssl_verify_peer_name'],
            'ssl_verify_host' => $clientData['ssl_verify_host'],
            'ssl_cafile' => $clientData['ssl_cafile'],
            'headers' => [
                'Accept' => $clientData['data_format'] === 'csv'
                    ? 'text/csv'
                    : 'application/' . $clientData['data_format'],
                'X-Asalae-Webservice' => 'true',
            ],
        ];
        if ($clientData['use_proxy'] && Configure::read('Proxy.host')) {
            $params['proxy'] = [
                'proxy' => Configure::read('Proxy.host') . ':' . Configure::read('Proxy.port'),
                'username' => Configure::read('Proxy.login'),
                'password' => Configure::read('Proxy.password'),
            ];
        }
        /** @var Client $client */
        $client = get_class(Utility::get(Client::class));
        $client = new $client($params);
        $clientResponse = $client->get($clientData['url'], [], ['cookies' => []]);

        if ($clientResponse->getStatusCode() >= 400) {
            throw new HttpException(
                __(
                    "Erreur HTTP {0} sur le webservice ({1}): {2}",
                    $clientResponse->getStatusCode(),
                    $inputName,
                    $clientResponse->getStringBody()
                ),
                $clientResponse->getStatusCode()
            );
        }
        $body = $clientResponse->getBody();
        $body->rewind();

        $tmpFile = sys_get_temp_dir() . DS . uniqid('download-');
        try {
            $handle = fopen($tmpFile, 'w');
            while (!$body->eof()) {
                fwrite($handle, $body->read(4096));
            }
            fclose($handle);
            return $this->extractData($tmpFile, $formExtractor);
        } finally {
            unlink($tmpFile);
        }
    }

    /**
     * Extraction des données des fichiers contenus dans $data (fileuploads)
     * @param array         $data
     * @param FormExtractor $formExtractor
     * @return array
     * @throws FormMakerException
     */
    private function extractFromFiles(array $data, FormExtractor $formExtractor): array
    {
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $inputName = Hash::get($formExtractor, 'form_input.name');
        if (!isset($data[$inputName])) {
            return [];
        }
        $extractors = [];
        if ($formExtractor->get('multiple')) {
            $files = $Fileuploads->find()
                ->where(
                    [
                        'id IN' => $data[$inputName],
                        'user_id' => $this->user_id,
                    ]
                )
                ->toArray();
            $extractors[$formExtractor->get('name')] = [];
        } else {
            $files = [
                $Fileuploads->find()
                    ->where(
                        [
                            'id' => $data[$inputName],
                            'user_id' => $this->user_id,
                        ]
                    )
                    ->firstOrFail(),
            ];
        }
        /** @var EntityInterface $file */
        foreach ($files as $file) {
            foreach ($this->extractFromSingleFile($file, $formExtractor) as $field => $values) {
                $extractors[$field] = array_unique(
                    array_merge($extractors[$field] ?? [], (array)$values)
                );
            }
        }
        if (!Hash::get($formExtractor, 'multiple')) {
            $ref = &$extractors[Hash::get($formExtractor, 'name')];
            $ref = current($ref);
        }
        return $extractors;
    }

    /**
     * Extraction des données des fichiers contenus dans $data (fileuploads)
     * pour un fichier
     * @param EntityInterface $fileupload
     * @param FormExtractor   $formExtractor
     * @return array
     * @throws FormMakerException
     */
    private function extractFromSingleFile(
        EntityInterface $fileupload,
        FormExtractor $formExtractor
    ): array {
        // on sélectionne le fichier sur lequel faire l'extraction
        if ($formExtractor->get('type') === FormExtractorsTable::TYPE_ARCHIVE_FILE) {
            $extractDir = sprintf(
                '%s/uncompressed/%d/%d',
                $this->deposit->get('path'),
                Hash::get($formExtractor, 'form_input.id'),
                $fileupload->id
            );
            $files = glob($extractDir . '/' . trim($formExtractor->get('file_selector') ?: '*'));
            if (!$files) {
                return [];
            }
            $targetFile = $files[0];
        } else {
            $targetFile = $fileupload->get('path');
        }
        return [
            $formExtractor->get('name') => $this->extractData($targetFile, $formExtractor),
        ];
    }

    /**
     * Calcul des variables
     * @param array $twigData
     * @return array
     * @throws LoaderError
     * @throws SyntaxError
     */
    private function getVariables(array $twigData): array
    {
        $FormCalculators = TableRegistry::getTableLocator()->get('FormCalculators');
        $query = $FormCalculators->find()
            ->where(['FormCalculators.form_id' => $this->form_id])
            ->contain(
                [
                    'FormVariables' => [
                        'FormInputs' => [
                            'FormFieldsets',
                        ],
                        'FormExtractors' => [
                            'FormInputs' => [
                                'FormFieldsets',
                            ],
                        ],
                    ],
                ]
            )
            ->order(['FormCalculators.id']);

        /** @var EntityInterface $formCalculator */
        foreach ($query as $formCalculator) {
            $repeatable = false;
            foreach ($this->getFieldsetsFromFormCalculator($formCalculator) as $fieldset) {
                if (!$fieldset['repeatable']) {
                    continue;
                }
                $repeatable = true;
                $this->appendRepeatableFieldsetToVariable($formCalculator, $fieldset, $twigData);
            }
            // on ne calcule que si le calculateur n'est pas répétable
            if (!$repeatable) {
                $twigData['var'][$formCalculator->get('name')]
                    = $this->getFormCalculatorValue($formCalculator, $twigData);
            }
        }
        return $twigData['var'];
    }

    /**
     * Donne la valeur calculé d'un form_calculator
     * @param EntityInterface $formCalculator
     * @param array           $twigData
     * @return array|string|null
     * @throws LoaderError
     * @throws SyntaxError
     */
    private function getFormCalculatorValue(EntityInterface $formCalculator, array $twigData)
    {
        if ($formCalculator->get('multiple')) {
            $with = Hash::get($formCalculator, 'form_variable.multiple_with');
            if (!$with || !str_contains($with, '.')) {
                return null;
            }
            [$type, $name] = explode('.', $with);
            $value = [];
            foreach (Hash::get($twigData, "$type.$name", []) as $val) {
                $twigDataPlus = $twigData + ['multiple' => ['value' => $val]];
                $value[] = $this->twig(
                    Hash::get($formCalculator, 'form_variable.twig'),
                    $twigDataPlus
                );
            }
        } else {
            $value = $this->twig(
                Hash::get($formCalculator, 'form_variable.twig'),
                $twigData
            );
        }
        return $value;
    }

    /**
     * Ajoute les données issues des sections répétables dans les variables
     * @param EntityInterface $formCalculator
     * @param array           $fieldset
     * @param array           $twigData
     * @return void
     * @throws LoaderError
     * @throws SyntaxError
     */
    private function appendRepeatableFieldsetToVariable(
        EntityInterface $formCalculator,
        array $fieldset,
        array &$twigData
    ) {
        $section = sprintf('section-%d', $fieldset['ord']);
        $path = sprintf('input.%s', $section);
        $contextTwigData = [];
        foreach (Hash::get($twigData, $path) as $key => $values) {
            foreach ($values as $name => $value) {
                $contextTwigData[$key]['input'][$name] = $value;
            }
        }

        $path = sprintf('extract.%s', $section);
        foreach (Hash::get($twigData, $path) as $key => $values) {
            foreach ($values as $name => $value) {
                $contextTwigData[$key]['extract'][$name] = $value;
            }
        }

        foreach ($contextTwigData as $index => $data) {
            $twigData['var'][$section][$index][$formCalculator->get('name')]
                = $this->getFormCalculatorValue($formCalculator, $data + $twigData);
        }
    }

    /**
     * Donne la liste des fieldsets liés à un form calculator
     * @param EntityInterface $calculator
     * @return array
     */
    private function getFieldsetsFromFormCalculator(EntityInterface $calculator): array
    {
        $fieldsets = [];
        $path = 'form_variable.form_inputs.{n}.form_fieldset';
        $extractorPath = 'form_variable.form_extractors.{n}.form_input.form_fieldset';
        $fieldsetEntities = Hash::merge(
            Hash::extract($calculator, $path),
            Hash::extract($calculator, $extractorPath),
        );
        foreach ($fieldsetEntities as $fieldset) {
            $fieldsets[$fieldset['id']] = $fieldset;
        }
        return $fieldsets;
    }

    /**
     * Donne un array de données adapté à seda_generator
     * @param array $twigVars
     * @return array
     * @throws LoaderError|RuntimeError|SyntaxError|FormMakerException
     */
    private function getRequestData(array $twigVars): array
    {
        $requestData = $this->getTransferHeadersRequestData($twigVars);

        $FormUnits = TableRegistry::getTableLocator()->get('FormUnits');
        $formUnits = $FormUnits->find('threaded')
            ->where(['FormUnits.form_id' => $this->form_id])
            ->order(['FormUnits.lft'])
            ->contain(
                [
                    'FormInputs',
                    'FormVariables' => ['FormCalculators'],
                    'FormUnitHeaders' => ['FormVariables'],
                    'FormUnitManagements' => ['FormVariables'],
                    'FormUnitContents' => ['FormVariables'],
                    'FormUnitKeywords' => function (Query $q) {
                        return $q->order(['FormUnitKeywords.name'])
                            ->contain(
                                [
                                    'FormUnitKeywordDetails' => ['FormVariables'],
                                ]
                            );
                    },
                    'RepeatableFieldsets',
                ]
            )
            ->toArray();
        $ref = &$requestData;
        $ref['ArchiveUnits'] = [];
        $this->archiveUnitsRequestData($formUnits, $twigVars, $ref);
        if (empty($ref['ArchiveUnits'])) {
            unset($ref['ArchiveUnits']);
        }

        // Valeurs par défaut du service d'archives et du service versant
        $this->appendDefaultValuesToRequestData($twigVars, $requestData);

        return $requestData;
    }

    /**
     * Donne un array de données adapté à seda_generator pour les entêtes du transfert
     * @param array $twigVars
     * @return array
     * @throws LoaderError|SyntaxError|FormMakerException
     */
    private function getTransferHeadersRequestData(array $twigVars): array
    {
        $requestData = [];
        $FormTransferHeaders = TableRegistry::getTableLocator()
            ->get('FormTransferHeaders');
        $query = $FormTransferHeaders->find()
            ->where(['FormTransferHeaders.form_id' => $this->form_id])
            ->contain(['FormVariables']);

        $unsetIfEmpty = $this->getUnsetIfEmptyManagement();
        $unsettables = array_keys($unsetIfEmpty);
        $required = array_unique(Hash::extract($unsetIfEmpty, '{*}.{*}'));
        $completed = [];
        $unsettableRefs = [];

        /** @var EntityInterface $formTransferHeader */
        foreach ($query as $formTransferHeader) {
            $ref = &$requestData;
            foreach (explode('_', $formTransferHeader->get('name')) as $name) {
                $ref = &$ref[$name];
            }
            $ref = $this->twig(
                Hash::get($formTransferHeader, 'form_variable.twig'),
                $twigVars
            );

            if ($ref && in_array($formTransferHeader->get('name'), $required)) {
                foreach ($unsetIfEmpty as $element => $values) {
                    if (in_array($formTransferHeader->get('name'), $values)) {
                        $completed[$element] = ($completed[$element] ?? 0) + 1;
                    }
                }
            }
            if (in_array($formTransferHeader->get('name'), $unsettables)) {
                $unsettableRefs[$formTransferHeader->get('name')] = &$ref;
            }
            unset($ref);
        }
        // seconde passe pour filtrer les elements incomplets
        foreach ($unsettableRefs as $element => &$ref) {
            if (empty($completed[$element])) {
                $ref = null;
            }
        }
        $requestData = Hash::filter($requestData);

        // on copie le contenu à la racine pour seda generator...
        if (isset($requestData['DataObjectPackage']['ManagementMetadata'])) {
            $requestData += $requestData['DataObjectPackage']['ManagementMetadata'];
        }

        // valeurs par défaut sur entêtes 1er niveau
        if (empty($requestData['Comment']) && $this->deposit) {
            $requestData['Comment'] = $this->getDepositName();
        }
        if (empty($requestData['Date'])) {
            $requestData['Date'] = (new DateTime())->format(DATE_RFC3339);
        } elseif (
            !preg_match(
                '/^(1\d|20)\d{2}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01])/',
                $requestData['Date']
            )
        ) {
            $dateVar = $FormTransferHeaders->find()
                ->where(['FormTransferHeaders.form_id' => $this->form_id])
                ->andWhere(['FormTransferHeaders.name' => 'Date'])
                ->contain(['FormVariables'])
                ->firstOrFail();
            throw FormMakerException::create(
                'ArchiveTransfer/Date',
                __(
                    "La date de transfert est invalide: {0}<br>Code twig utilisé: {1}",
                    h($requestData['Date']),
                    h(Hash::get($dateVar, 'form_variable.twig'))
                )
            );
        }

        $identifierNodeName = $this->form->get('seda_version') === 'seda1.0'
            ? 'TransferIdentifier'
            : 'MessageIdentifier';
        if (!isset($requestData[$identifierNodeName])) {
            /** @var CountersTable $Counters */
            $Counters = TableRegistry::getTableLocator()->get('Counters');
            $requestData[$identifierNodeName] = $Counters->next(
                Hash::get($this->form, 'org_entity.archival_agency.id'),
                'ArchiveTransfer',
                ['save' => false]
            );
        }
        return $requestData;
    }

    /**
     * Ajoute les valeurs par défaut au $requestData
     * @param array $twigVars
     * @param array $requestData
     */
    private function appendDefaultValuesToRequestData(array $twigVars, array &$requestData)
    {
        if ($this->form->get('seda_version') === 'seda1.0') {
            if (!isset($requestData['ArchivalAgency']['Identification'])) {
                $requestData['ArchivalAgency'] = [
                    'Identification' => Hash::get($twigVars, 'meta.archival_agency.identifier'),
                    'Name' => Hash::get($twigVars, 'meta.archival_agency.name'),
                ];
            }
            if (!isset($requestData['TransferringAgency']['Identification'])) {
                $requestData['TransferringAgency'] = [
                    'Identification' => Hash::get($twigVars, 'meta.transferring_agency.identifier'),
                    'Name' => Hash::get($twigVars, 'meta.transferring_agency.name'),
                ];
            }
        } else {
            if (!isset($requestData['ArchivalAgency']['Identifier'])) {
                $requestData['ArchivalAgency'] = [
                    'Identifier' => Hash::get($twigVars, 'meta.archival_agency.identifier'),
                    'Name' => Hash::get($twigVars, 'meta.archival_agency.name'),
                ];
            }
            if (!isset($requestData['TransferringAgency']['Identifier'])) {
                $requestData['TransferringAgency'] = [
                    'Identifier' => Hash::get($twigVars, 'meta.transferring_agency.identifier'),
                    'Name' => Hash::get($twigVars, 'meta.transferring_agency.name'),
                ];
            }
            $au = &$requestData['ArchiveUnits'][0];
            $requestData['AccessRule']['Rule'] = Hash::get($au, 'AccessRule.Rule') ?: 'AR038';
            $requestData['AccessRule']['StartDate'] = Hash::get($au, 'AccessRule.StartDate')
                ?: (new DateTime())->format('Y-m-d');
            $requestData['AppraisalRule']['Rule'] = Hash::get($au, 'AppraisalRule.Rule') ?: 'APP0Y';
            $requestData['AppraisalRule']['FinalAction'] = Hash::get($au, 'AppraisalRule.FinalAction') ?: 'Keep';
            $requestData['AppraisalRule']['StartDate'] = Hash::get($au, 'AppraisalRule.StartDate')
                ?: (new DateTime())->format('Y-m-d');
        }
    }

    /**
     * Donne le request data des archive_units pour seda_generator
     * @param EntityInterface[] $formUnits
     * @param array             $twigVars
     * @param array             $requestData
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError|FormMakerException
     */
    private function archiveUnitsRequestData(array $formUnits, array $twigVars, array &$requestData)
    {
        foreach ($formUnits as $formUnit) {
            $formUnit->set('twig_data', $twigVars);
            // condition de présence
            if ($formUnit->get('form_variable')) {
                $name = Hash::get($formUnit, 'form_variable.form_calculator.name');
                if (empty($twigVars['var'][$name])) {
                    continue;
                }
            }
            switch ($formUnit->get('type')) {
                case FormUnitsTable::TYPE_SIMPLE:
                    $this->simpleFormUnitRequestData($formUnit, $twigVars, $requestData['ArchiveUnits']);
                    break;
                case FormUnitsTable::TYPE_REPEATABLE:
                    $this->repetableValues($formUnit, $twigVars, $requestData);
                    break;
                case FormUnitsTable::TYPE_DOCUMENT:
                    $this->documentFormUnitRequestData($formUnit, $twigVars, $requestData);
                    break;
                case FormUnitsTable::TYPE_DOCUMENT_MULTIPLE:
                    $this->documentMultipleFormUnitRequestData($formUnit, $twigVars, $requestData);
                    break;
                case FormUnitsTable::TYPE_TREE_ROOT:
                    $this->simpleFormUnitRequestData($formUnit, $twigVars, $requestData['ArchiveUnits']);
                    $ref = &$requestData['ArchiveUnits'][count($requestData['ArchiveUnits']) - 1];
                    $files = $this->treeRootFormUnitRequestData(
                        $formUnit,
                        $twigVars,
                        $ref
                    );
                    if ($files) {
                        $this->alterArchiveUnits($formUnit, $twigVars, $ref, $files);
                    } else {
                        $ref = null;
                    }
                    break;
            }
        }
    }

    /**
     * Gestion des champs répétables
     * @param EntityInterface $formUnit
     * @param array           $twigVars
     * @param array           $requestData
     * @throws FormMakerException
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    private function repetableValues(EntityInterface $formUnit, array $twigVars, array &$requestData)
    {
        /** @var EntityInterface $fieldset */
        $fieldset = $formUnit->get('repeatable_fieldset');
        $formUnit->unset(['repeatable_fieldset', 'repeatable_fieldset_id']);
        $formUnit->set('type', FormUnitsTable::TYPE_SIMPLE);
        $sectionData = $twigVars['input']['section-' . $fieldset->get('ord')] ?? [];
        $metaData = $twigVars['meta']['section-' . $fieldset->get('ord')] ?? [];
        $extractData = $twigVars['extract']['section-' . $fieldset->get('ord')] ?? [];
        $varData = $twigVars['var']['section-' . $fieldset->get('ord')] ?? [];
        foreach ($sectionData as $i => $value) {
            $newTwigVars = $twigVars;
            unset($newTwigVars['input']['section-' . $fieldset->get('ord')]);
            if (isset($metaData[$i])) {
                $newTwigVars['meta'] = Hash::merge($newTwigVars['meta'], $metaData[$i]);
                unset($newTwigVars['meta']['section-' . $fieldset->get('ord')]);
            }
            if (isset($extractData[$i])) {
                $newTwigVars['extract'] = Hash::merge($newTwigVars['var'], $extractData[$i]);
                unset($newTwigVars['extract']['section-' . $fieldset->get('ord')]);
            }
            if (isset($varData[$i])) {
                $newTwigVars['var'] = Hash::merge($newTwigVars['var'], $varData[$i]);
                unset($newTwigVars['var']['section-' . $fieldset->get('ord')]);
            }
            $newTwigVars['input'] = Hash::merge($newTwigVars['input'], $value);
            $newTwigVars['section']['index'] = $i + 1;
            $this->archiveUnitsRequestData([$formUnit], $newTwigVars, $requestData);
        }
    }

    /**
     * Gestion des unités d'archives type simple
     * @param EntityInterface $formUnit
     * @param array           $twigVars
     * @param array           $requestData
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError|FormMakerException
     */
    private function simpleFormUnitRequestData(EntityInterface $formUnit, array $twigVars, array &$requestData)
    {
        $currentData = Hash::merge(
            $this->getArchiveUnitSatRequestData('form_unit_headers', $formUnit, $twigVars),
            $this->getArchiveUnitSatRequestData('form_unit_managements', $formUnit, $twigVars),
            $this->getArchiveUnitContentRequestData($formUnit, $twigVars),
            $this->getArchiveUnitKeywordsRequestData($formUnit, $twigVars),
        );

        // valeurs par défaut
        $this->appendArchiveUnitsDefaultValuesToRequestData($currentData, $formUnit);
        $requestData[] = $currentData;
    }

    /**
     * Donne les éléments liés à un archive unit ($formUnit)
     * @param string          $tableName
     * @param EntityInterface $formUnit
     * @param array           $twigVars
     * @return array
     * @throws LoaderError
     * @throws SyntaxError
     */
    private function getArchiveUnitSatRequestData(
        string $tableName,
        EntityInterface $formUnit,
        array $twigVars
    ): array {
        $unsetIfEmpty = $this->getUnsetIfEmptyManagement();
        $unsettables = array_keys($unsetIfEmpty);
        $required = array_unique(Hash::extract($unsetIfEmpty, '{*}.{*}'));
        $completed = [];
        $unsettableRefs = [];

        $currentData = [];
        /** @var EntityInterface $formUnitSat */
        foreach ($formUnit->get($tableName) ?: [] as $formUnitSat) {
            if (str_contains($formUnitSat->get('name'), '_')) {
                $exp = explode('_', $formUnitSat->get('name'), 3);
                [$parentName, $currentName] = $exp;
                if (!isset($currentData[$parentName])) {
                    $currentData[$parentName] = [];
                }
                if (isset($exp[2]) && !isset($currentData[$parentName][$exp[2]])) {
                    $currentData[$parentName][$currentName][$exp[2]] = [];
                    $ref = &$currentData[$parentName][$currentName][$exp[2]];
                } else {
                    $ref = &$currentData[$parentName][$currentName];
                }
            } else {
                $ref = &$currentData[$formUnitSat->get('name')];
            }
            $ref = $this->twig(
                Hash::get($formUnitSat, 'form_variable.twig'),
                $twigVars
            );
            if ($ref && in_array($formUnitSat->get('name'), $required)) {
                foreach ($unsetIfEmpty as $element => $values) {
                    if (in_array($formUnitSat->get('name'), $values)) {
                        $completed[$element] = ($completed[$element] ?? 0) + 1;
                    }
                }
            }
            if (in_array($formUnitSat->get('name'), $unsettables)) {
                $unsettableRefs[$formUnitSat->get('name')] = &$ref;
            }
            unset($ref);
        }
        // seconde passe pour filtrer les elements incomplets
        foreach ($unsettableRefs as $element => &$ref) {
            $requiredAmount = count($unsetIfEmpty[$element]);
            $count = $completed[$element] ?? 0;
            if ($count < $requiredAmount) {
                $ref = null;
            }
        }
        return Hash::filter($currentData);
    }

    /**
     * Donne la liste des noeuds a unset si incomplet pour les ArchiveUnitContents
     * @return array[]
     */
    private function getUnsetIfEmptyContent(): array
    {
        $unsetIfEmpty = [
            'AccessRestrictionRule_Code' => ['AccessRestrictionRule_StartDate'],
            'AccessRestrictionRule_StartDate' => ['AccessRestrictionRule_Code'],
            'CustodialHistory_CustodialHistoryItem@when' => ['CustodialHistory_CustodialHistoryItem'],
            'EventIdentifier' => ['EventDateTime'],
            'EventTypeCode' => ['EventDateTime'],
            'EventType' => ['EventDateTime'],
            'EventDetail' => ['EventDateTime'],
            'Outcome' => ['EventDateTime'],
            'OutcomeDetail' => ['EventDateTime'],
            'OutcomeDetailMessage' => ['EventDateTime'],
            'EventDetailData' => ['EventDateTime'],
            'LinkingAgentIdentifier' => ['EventDateTime'],
            'Repository_BusinessType' => ['Repository_Identification'],
            'Repository_Description' => ['Repository_Identification'],
            'SubmissionAgency_Name' => ['SubmissionAgency_Identifier'],
        ];
        if ($this->form->get('seda_version') === 'seda1.0') {
            $unsetIfEmpty['OriginatingAgency_Name'] = ['OriginatingAgency_Identification'];
        } else {
            $unsetIfEmpty['OriginatingAgency_Name'] = ['OriginatingAgency_Identifier'];
        }
        return $unsetIfEmpty;
    }

    /**
     * Donne la liste des noeuds a unset si incomplet pour les ArchiveUnitManagements
     * @return array[]
     */
    private function getUnsetIfEmptyManagement(): array
    {
        if ($this->form->get('seda_version') === 'seda1.0') {
            $unsetIfEmpty = [
                'AccessRestrictionRule_Code' => [
                    'AccessRestrictionRule_StartDate',
                ],
                'AccessRestrictionRule_StartDate' => [
                    'AccessRestrictionRule_Code',
                ],
                'AppraisalRule_Code' => [
                    'AppraisalRule_Duration',
                    'AppraisalRule_StartDate',
                ],
                'AppraisalRule_Duration' => [
                    'AppraisalRule_Code',
                    'AppraisalRule_StartDate',
                ],
                'AppraisalRule_StartDate' => [
                    'AppraisalRule_Code',
                    'AppraisalRule_Duration',
                ],
            ];
        } else {
            $unsetIfEmpty = [
                'StorageRule_Rule' => [
                    'StorageRule_StartDate',
                    'StorageRule_FinalAction',
                ],
                'StorageRule_StartDate' => [
                    'StorageRule_Rule',
                    'StorageRule_FinalAction',
                ],
                'StorageRule_FinalAction' => [
                    'StorageRule_Rule',
                    'StorageRule_StartDate',
                ],
                'AppraisalRule_Rule' => [
                    'AppraisalRule_StartDate',
                    'AppraisalRule_FinalAction',
                ],
                'AppraisalRule_StartDate' => [
                    'AppraisalRule_Rule',
                    'AppraisalRule_FinalAction',
                ],
                'AppraisalRule_FinalAction' => [
                    'AppraisalRule_Rule',
                    'AppraisalRule_StartDate',
                ],
                'AccessRule_Rule' => [
                    'AccessRule_StartDate',
                ],
                'AccessRule_StartDate' => [
                    'AccessRule_Rule',
                ],
                'DisseminationRule_Rule' => [
                    'DisseminationRule_StartDate',
                ],
                'DisseminationRule_StartDate' => [
                    'DisseminationRule_Rule',
                ],
                'ReuseRule_Rule' => [
                    'ReuseRule_StartDate',
                ],
                'ReuseRule_StartDate' => [
                    'ReuseRule_Rule',
                ],
                'ClassificationRule_Rule' => [
                    'ClassificationRule_StartDate',
                ],
                'ClassificationRule_StartDate' => [
                    'ClassificationRule_Rule',
                ],
                'ClassificationRule_ClassificationAudience' => [
                    'ClassificationRule_Rule',
                    'ClassificationRule_StartDate',
                ],
            ];
        }

        // ajoute un double à tous les éléments avec le préfixe Management_
        foreach ($unsetIfEmpty as $element => $required) {
            foreach ($required as $value) {
                $unsetIfEmpty[sprintf('Management_%s', $element)][]
                    = sprintf('Management_%s', $value);
                $unsetIfEmpty[sprintf('DataObjectPackage_ManagementMetadata_%s', $element)][]
                    = sprintf('DataObjectPackage_ManagementMetadata_%s', $value);
            }
        }

        return $unsetIfEmpty;
    }

    /**
     * Donne le Content d'un archive unit ($formUnit)
     * @param EntityInterface $formUnit
     * @param array           $twigVars
     * @return array
     * @throws LoaderError
     * @throws SyntaxError
     */
    private function getArchiveUnitContentRequestData(EntityInterface $formUnit, array $twigVars): array
    {
        $currentData = [];
        $descNodeName = $this->form->get('seda_version') === 'seda1.0'
            ? 'ContentDescription'
            : 'Content';
        if ($formUnit->get('form_unit_contents') || $formUnit->get('form_unit_keywords')) {
            $currentData[$descNodeName] = [];
        }

        $unsetIfEmpty = $this->getUnsetIfEmptyContent();
        $unsettables = array_keys($unsetIfEmpty);
        $required = array_unique(Hash::extract($unsetIfEmpty, '{*}.{*}'));
        $completed = [];
        $unsettableRefs = [];

        /** @var EntityInterface $formUnitContent */
        foreach ($formUnit->get('form_unit_contents') ?: [] as $formUnitContent) {
            if (in_array($formUnitContent->get('name'), $required)) {
                foreach ($unsetIfEmpty as $element => $values) {
                    if (in_array($formUnitContent->get('name'), $values)) {
                        $completed[$element] = $element;
                    }
                }
            }
            // cas particuliers
            if ($formUnitContent->get('name') === 'CustodialHistory_CustodialHistoryItem') {
                $ref = &$currentData[$descNodeName]['CustodialHistory'];
                $ref = &$ref['CustodialHistoryItem'];
                $ref = &$ref[0];
                $ref = &$ref['value'];
            } elseif ($formUnitContent->get('name') === 'CustodialHistory_CustodialHistoryItem@when') {
                $ref = &$currentData[$descNodeName]['CustodialHistory'];
                $ref = &$ref['CustodialHistoryItem'];
                $ref = &$ref[0];
                $ref = &$ref['@when'];
            } elseif (str_contains($formUnitContent->get('name'), '_')) {
                $ref = &$this->extractDeepRef($formUnitContent->get('name'), $descNodeName, $currentData);
            } else {
                $ref = &$currentData[$descNodeName][$formUnitContent->get('name')];
                if (is_array($ref)) { // champ multiple sans index
                    $ref[0] = null;
                    ksort($ref); // replace l'index 0 au début
                    $ref = &$ref[0]; // si on a Title alors qu'on a déjà Title_1 défini, on set à Title_0
                }
            }
            $ref = $this->twig(
                Hash::get($formUnitContent, 'form_variable.twig'),
                $twigVars
            );
            if (in_array($formUnitContent->get('name'), $unsettables)) {
                $unsettableRefs[$formUnitContent->get('name')] = &$ref;
            }
            unset($ref);
        }
        // seconde passe pour filtrer les elements incomplets
        foreach ($unsettableRefs as $element => &$ref) {
            if (empty($completed[$element])) {
                $ref = null;
            }
        }
        return Hash::filter($currentData);
    }

    /**
     * Donne les entêtes liés à un archive unit ($formUnit)
     * @param EntityInterface $formUnit
     * @param array           $twigVars
     * @return array
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError|FormMakerException
     */
    private function getArchiveUnitKeywordsRequestData(EntityInterface $formUnit, array $twigVars): array
    {
        $currentData = [];

        $descNodeName = $this->form->get('seda_version') === 'seda1.0'
            ? 'ContentDescription'
            : 'Content';
        /** @var EntityInterface $formUnitKeyword */
        foreach ($formUnit->get('form_unit_keywords') ?: [] as $formUnitKeyword) {
            if ($formUnitKeyword->get('multiple_with')) {
                [$type, $name] = explode('.', $formUnitKeyword->get('multiple_with'));
                foreach (Hash::get($twigVars, "$type.$name") ?: [] as $multipleValue) {
                    $this->addKeywordToRequestData(
                        $formUnitKeyword,
                        $twigVars + ['multiple' => ['value' => $multipleValue]],
                        $descNodeName,
                        $currentData
                    );
                }
            } else {
                $this->addKeywordToRequestData(
                    $formUnitKeyword,
                    $twigVars,
                    $descNodeName,
                    $currentData
                );
            }
        }
        if (!empty($formUnit->get('children'))) {
            $currentData['ArchiveUnits'] = [];
            $ref = &$currentData;
            $this->archiveUnitsRequestData($formUnit->get('children'), $twigVars, $ref);
            if (empty($currentData['ArchiveUnits'])) {
                unset($currentData['ArchiveUnits']);
            }
        }
        return $currentData;
    }

    /**
     * Ajoute les valeurs par défaut sur une ArchiveUnit
     * @param array           $currentData
     * @param EntityInterface $formUnit
     * @return void
     */
    private function appendArchiveUnitsDefaultValuesToRequestData(array &$currentData, EntityInterface $formUnit)
    {
        if ($this->form->get('seda_version') === 'seda1.0') {
            if (!isset($currentData['Name'])) {
                $currentData['Name'] = $formUnit->get('name');
            }
            if ($formUnit->get('parent_id') === null) {
                if (!isset($currentData['AccessRestrictionRule'])) {
                    $currentData['AccessRestrictionRule'] = [];
                }
                if (!isset($currentData['AccessRestrictionRule']['Code'])) {
                    $currentData['AccessRestrictionRule']['Code'] = self::DEFAULT_ACCESS_CODE;
                }
                if (!isset($currentData['AccessRestrictionRule']['StartDate'])) {
                    $currentData['AccessRestrictionRule']['StartDate'] = (new DateTime())->format('Y-m-d');
                }
                if (!isset($currentData['ContentDescription'])) {
                    $currentData['ContentDescription'] = [];
                }
                if (!isset($currentData['ContentDescription']['DescriptionLevel'])) {
                    $currentData['ContentDescription']['DescriptionLevel'] = 'recordgrp';
                }
            } else {
                if (
                    isset($currentData['ContentDescription'])
                    && !isset($currentData['ContentDescription']['DescriptionLevel'])
                ) {
                    $currentData['ContentDescription']['DescriptionLevel'] = 'subgrp';
                }
            }
        } else {
            $currentData['Id'] = 'SID-' . self::shortUUID();
            if (!isset($currentData['Content'])) {
                $currentData['Content'] = [];
            }
            if (!isset($currentData['Content']['Title'])) {
                $currentData['Content']['Title'] = $formUnit->get('name');
            }
            if ($formUnit->get('parent_id') === null) {
                if (!isset($currentData['AccessRule'])) {
                    $currentData['AccessRule'] = [];
                }
                if (!isset($currentData['AccessRule']['Rule'])) {
                    $currentData['AccessRule']['Rule'] = self::DEFAULT_ACCESS_CODE;
                }
                if (!isset($currentData['AccessRule']['StartDate'])) {
                    $currentData['AccessRule']['StartDate'] = (new DateTime())->format('Y-m-d');
                }
                if (!isset($currentData['Content']['DescriptionLevel'])) {
                    $currentData['Content']['DescriptionLevel'] = 'RecordGrp';
                }
            } else {
                if (
                    isset($currentData['Content'])
                    && !isset($currentData['Content']['DescriptionLevel'])
                ) {
                    $currentData['Content']['DescriptionLevel'] = 'SubGrp';
                }
            }
        }
    }

    /**
     * Ajoute un mot clé dans $currentData
     * @param EntityInterface $formUnitKeyword
     * @param array           $twigVars
     * @param string          $descNodeName
     * @param array           $currentData
     * @throws LoaderError
     * @throws SyntaxError
     */
    private function addKeywordToRequestData(
        EntityInterface $formUnitKeyword,
        array $twigVars,
        string $descNodeName,
        array &$currentData
    ) {
        $keywordContent = [];
        /** @var EntityInterface $keywordDetail */
        foreach ($formUnitKeyword->get('form_unit_keyword_details') ?: [] as $keywordDetail) {
            if (str_contains($keywordDetail->get('name'), '_')) {
                [$parentName, $currentName] = explode('_', $keywordDetail->get('name'), 2);
                if (!isset($keywordContent[$parentName])) {
                    $keywordContent[$parentName] = [];
                }
                $ref = &$keywordContent[$parentName][$currentName];
            } else {
                $ref = &$keywordContent[$keywordDetail->get('name')];
            }
            $ref = $this->twig(
                Hash::get($keywordDetail, 'form_variable.twig'),
                $twigVars
            );
        }
        if (empty($keywordContent['KeywordContent'])) {
            return;
        }
        $currentData[$descNodeName]['Keyword'] = $currentData[$descNodeName]['Keyword'] ?? [];
        $currentData[$descNodeName]['Keyword'][] = $keywordContent;
    }

    /**
     * Gestion des unités d'archives type document
     * @param EntityInterface $formUnit
     * @param array           $twigVars
     * @param array           $requestData
     * @throws LoaderError
     * @throws SyntaxError|FormMakerException
     */
    private function documentFormUnitRequestData(EntityInterface $formUnit, array $twigVars, array &$requestData)
    {
        if (!isset($requestData['Files'])) {
            $requestData['Files'] = [];
        }
        $id = 'SID-' . self::shortUUID();
        if (
            !Hash::get($formUnit, 'form_input.name')
            || !isset($twigVars['input'][Hash::get($formUnit, 'form_input.name')])
        ) {
            if ($formUnit->get('presence_required')) {
                throw FormMakerException::create(
                    'Document',
                    __(
                        "Le fichié lié à un Document ({0}) en 'Présence obligatoire' n'a pas été fourni",
                        h($formUnit->get('name'))
                    )
                );
            } else {
                return;
            }
        }
        $fileupload_id = $twigVars['input'][Hash::get($formUnit, 'form_input.name')];
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $fileupload = $Fileuploads->find()
            ->where(['Fileuploads.id' => $fileupload_id])
            ->contain(['Siegfrieds'])
            ->firstOrFail();
        $filename = $this->getPath($formUnit, $fileupload->get('name'));

        $currentData = $this->getArchiveUnitSatRequestData('form_unit_headers', $formUnit, $twigVars);

        $this->attachments[$filename] = $fileupload->get('path');
        $currentData['Filename'] = $filename;
        $currentData['Size'] = $fileupload->get('size') ?: filesize($fileupload->get('path'));
        $currentData['MessageDigest'] = $fileupload->get('hash')
            ?: hash_file($fileupload->get('hash_algo'), $fileupload->get('path'));
        $currentData['LastModified'] = date(DATE_RFC3339, @filemtime($fileupload->get('path')));
        $this->appendSiegfriedToCurrentData($fileupload, $currentData);

        if ($this->form->get('seda_version') === 'seda1.0') {
            $currentData['algorithme'] = $fileupload->get('hash_algo');
            $currentData = array_merge(
                $currentData,
                $this->getArchiveUnitContentRequestData($formUnit, $twigVars)['ContentDescription'] ?? []
            );
            $currentData['Type'] = $currentData['Type'] ?? 'CDO';
        } else {
            $currentData['algorithm'] = $fileupload->get('hash_algo');
        }
        $requestData['Files'][$id] = $currentData;
    }

    /**
     * Ajoute les informations issues de siegfried aux données du Document
     * @param EntityInterface $fileupload
     * @param array           $currentData
     * @return void
     */
    private function appendSiegfriedToCurrentData(EntityInterface $fileupload, array &$currentData)
    {
        $Pronoms = TableRegistry::getTableLocator()->get('Pronoms');
        $sf = Hash::get($fileupload, 'siegfrieds.0');
        if ($sf instanceof EntityInterface) {
            $puid = $sf->get('pronom');
            $mime = $fileupload->get('mime') ?? mime_content_type($fileupload->get('path'));
            if ($puid && in_array($puid, $this->puids)) {
                if (!isset(self::$formatNames[$puid])) {
                    self::$formatNames[$puid] = Hash::get(
                        $Pronoms->find()
                            ->select(['name'])
                            ->where(['puid' => $puid])
                            ->disableHydration()
                            ->first() ?: [],
                        'name',
                        ''
                    );
                }
                if (self::$formatNames[$puid]) {
                    $currentData['FormatLitteral'] = self::$formatNames[$puid];
                }
                $currentData['FormatId'] = $puid;
            }
            if ($mime && in_array($mime, $this->mimes)) {
                $currentData['MimeType'] = $mime;
            }
        }
    }

    /**
     * Gestion des unités d'archives type document_multiple
     * @param EntityInterface $formUnit
     * @param array           $twigVars
     * @param array           $requestData
     * @throws LoaderError
     * @throws SyntaxError|FormMakerException
     */
    private function documentMultipleFormUnitRequestData(
        EntityInterface $formUnit,
        array $twigVars,
        array &$requestData
    ) {
        if (!isset($requestData['Files'])) {
            $requestData['Files'] = [];
        }
        if (
            !Hash::get($formUnit, 'form_input.name')
            || !isset($twigVars['input'][Hash::get($formUnit, 'form_input.name')][0])
        ) {
            if ($formUnit->get('presence_required')) {
                throw FormMakerException::create(
                    'Document',
                    __(
                        "Le fichié lié à un Document ({0}) en 'Présence obligatoire' n'a pas été fourni",
                        h($formUnit->get('name'))
                    )
                );
            } else {
                return;
            }
        }
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        foreach ($twigVars['input'][Hash::get($formUnit, 'form_input.name')] as $fileupload_id) {
            $fileupload = $Fileuploads->find()
                ->where(['Fileuploads.id' => $fileupload_id])
                ->contain(['Siegfrieds'])
                ->firstOrFail();
            $filename = $this->getPath($formUnit, $fileupload->get('name'));

            $currentData = $this->getArchiveUnitSatRequestData('form_unit_headers', $formUnit, $twigVars);
            $this->attachments[$filename] = $fileupload->get('path');
            $currentData['Filename'] = $filename;
            $currentData['Size'] = $fileupload->get('size') ?: filesize($fileupload->get('path'));
            $currentData['MessageDigest'] = $fileupload->get('hash')
                ?: hash_file($fileupload->get('hash_algo'), $fileupload->get('path'));
            $currentData['LastModified'] = date(DATE_RFC3339, filemtime($fileupload->get('path')));
            $this->appendSiegfriedToCurrentData($fileupload, $currentData);

            if ($this->form->get('seda_version') === 'seda1.0') {
                $currentData['algorithme'] = $fileupload->get('hash_algo');
                $currentData['Type'] = $currentData['Type'] ?? 'CDO';
            } else {
                $currentData['algorithm'] = $fileupload->get('hash_algo');
            }
            $id = 'SID-' . self::shortUUID();
            $requestData['Files'][$id] = $currentData;
        }
    }

    /**
     * Gestion des unités d'archives type tree_root
     * @param EntityInterface $formUnit
     * @param array           $twigVars
     * @param array           $requestData
     * @return array
     * @throws FormMakerException
     */
    private function treeRootFormUnitRequestData(
        EntityInterface $formUnit,
        array $twigVars,
        array &$requestData
    ): array {
        if (
            !Hash::get($formUnit, 'form_input.name')
            || !isset($twigVars['input'][Hash::get($formUnit, 'form_input.name')])
        ) {
            return [];
        }
        $fileupload_id = $twigVars['input'][Hash::get($formUnit, 'form_input.name')];
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $fileupload = $Fileuploads->find()
            ->where(['Fileuploads.id' => $fileupload_id])
            ->contain(['Siegfrieds'])
            ->firstOrFail();
        $basedir = $this->deposit->get('path');
        $filename = mb_substr($fileupload->get('path'), mb_strlen($basedir) + 1);

        $extractDir = sprintf(
            '%s/uncompressed/%d/%d',
            $this->deposit->get('path'),
            Hash::get($formUnit, 'form_input.id'),
            $fileupload_id
        );
        if (!is_dir($extractDir)) {
            throw FormMakerException::create(
                $filename,
                __("Le dossier de décompression n'a pas été trouvé")
            );
        }
        $exec = Utility::get('Exec')
            ->command('sf -json', $extractDir);
        $json = json_decode($exec->stdout, true);
        if (!$json || empty($json['files'])) {
            throw FormMakerException::create(
                $filename,
                __("Siegfried n'a pas fonctionné")
            );
        }
        $this->sf = [];
        foreach (Hash::get($json, 'files', []) as $file) {
            $format = Hash::get($file, 'matches.0.id') ?: 'UNKNOWN';
            $this->sf[$file['filename']] = [
                'format' => $format === 'UNKNOWN' ? '' : $format,
                'mime' => Hash::get($file, 'matches.0.mime') ?: mime_content_type($file['filename']),
                'extension' => pathinfo($file['filename'], PATHINFO_EXTENSION),
            ];
        }

        $requestData['_node_path'] = $formUnit->get('full_name');
        $this->recursiveTreeMaker(
            $extractDir,
            $fileupload,
            $formUnit,
            $requestData
        );
        $len = mb_strlen($extractDir . '/');
        $fn = fn($path) => $this->getPath($formUnit, substr($path, $len));
        return array_map(
            $fn,
            Filesystem::listFiles($extractDir, true)
        );
    }

    /**
     * Construit la structure d'ArchiveUnits selon la structure du dossier décompressé
     * @param string          $dir
     * @param EntityInterface $fileupload
     * @param EntityInterface $formUnit
     * @param array           $requestData
     */
    private function recursiveTreeMaker(
        string $dir,
        EntityInterface $fileupload,
        EntityInterface $formUnit,
        array &$requestData
    ) {
        /** @var DirectoryIterator $item */
        foreach (glob($dir . '/*') as $filename) {
            if (is_dir($filename)) {
                $this->treeMakeDir(
                    basename($filename),
                    $dir,
                    $fileupload,
                    $formUnit,
                    $requestData
                );
            } elseif (is_file($filename)) {
                $this->treeMakeFile(
                    $filename,
                    $fileupload,
                    $formUnit,
                    $requestData
                );
            }
        }
    }

    /**
     * Partie Dossier / ArchiveObject
     * @param string          $basename    Filename d'un dossier dans le uncompressed/
     * @param string          $dir         dossier parent de $item
     * @param EntityInterface $fileupload  fileupload du zip
     * @param EntityInterface $formUnit    formunit du tree_root
     * @param array           $requestData
     * @return void
     */
    private function treeMakeDir(
        string $basename,
        string $dir,
        EntityInterface $fileupload,
        EntityInterface $formUnit,
        array &$requestData
    ) {
        if (!isset($requestData['ArchiveUnits'])) {
            $requestData['ArchiveUnits'] = [];
        }
        $ref = &$requestData['ArchiveUnits'][count($requestData['ArchiveUnits'])];
        if ($this->form->get('seda_version') === 'seda1.0') {
            $ref = [
                'Name' => $basename,
            ];
        } else {
            $ref = [
                'Id' => 'SID-' . self::shortUUID(),
                'Content' => ['Title' => $basename],
            ];
        }
        $ref['_tree_path'] = $basename;
        $this->recursiveTreeMaker(
            $dir . DS . $basename,
            $fileupload,
            $formUnit,
            $ref
        );
    }

    /**
     * Partie Fichier / Document
     * @param string          $filename
     * @param EntityInterface $fileupload
     * @param EntityInterface $formUnit
     * @param array           $requestData
     * @return void
     */
    private function treeMakeFile(
        string $filename,
        EntityInterface $fileupload,
        EntityInterface $formUnit,
        array &$requestData
    ) {
        $Pronoms = TableRegistry::getTableLocator()->get('Pronoms');
        $algo = Configure::read('hash_algo', 'sha256');
        $algoKey = $this->form->get('seda_version') === 'seda1.0' ? 'algorithme' : 'algorithm';
        $extractDir = sprintf(
            '%s/uncompressed/%d/%d',
            $this->deposit->get('path'),
            Hash::get($formUnit, 'form_input.id'),
            $fileupload->id
        );
        $len = mb_strlen($extractDir . DS);

        $relativeName = $this->getPath($formUnit, mb_substr($filename, $len));
        if (!isset($this->files[$relativeName])) {
            $this->files[$relativeName] = self::shortUUID();
        }
        $id = 'SID-' . $this->files[$relativeName];
        $this->attachments[$relativeName] = $filename;
        $requestData['Files'] = $requestData['Files'] ?? [];
        $requestData['Files'][$id] = [
            'Filename' => $relativeName,
            'MessageDigest' => hash_file($algo, $filename),
            $algoKey => $algo,
            'Size' => filesize($filename),
            'LastModified' => date(DATE_RFC3339, filemtime($filename)),
        ];
        $fileinfo = $this->sf[$filename] ?? [];
        $puid = $fileinfo['format'] ?? null;
        $mime = $fileinfo['mime'] ?? mime_content_type($filename);
        if ($puid && in_array($puid, $this->puids)) {
            if (!isset(self::$formatNames[$puid])) {
                self::$formatNames[$puid] = Hash::get(
                    $Pronoms->find()
                        ->select(['name'])
                        ->where(['puid' => $puid])
                        ->disableHydration()
                        ->first() ?: [],
                    'name',
                    ''
                );
            }
            if (self::$formatNames[$puid]) {
                $requestData['Files'][$id]['FormatLitteral'] = self::$formatNames[$puid];
            }
            $requestData['Files'][$id]['FormatId'] = $puid;
        }
        if ($mime && in_array($mime, $this->mimes)) {
            $requestData['Files'][$id]['MimeType'] = $mime;
        }
    }

    /**
     * Ajoute des metadonnées à une cible tree_path
     * @param EntityInterface $formUnit
     * @param array           $twigVars
     * @param array           $requestData
     * @param array           $files
     * @throws FormMakerException
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    private function alterArchiveUnitByPath(
        EntityInterface $formUnit,
        array $twigVars,
        array &$requestData,
        array $files
    ) {
        $expr = trim($formUnit->get('search_expression'), '/');
        $regex = str_replace('\\*', '.+', preg_quote($expr, '/'));
        $matches = preg_grep("/^(?:[^\/]+\/)?$regex\/?$/", $files);
        if (count($matches) === 0) {
            return;
        }
        $target = current($matches);
        $ref = &$this->searchRef(trim($target, '/'), $requestData);
        if (!$ref) {
            return;
        }
        $modData = [];
        $formUnit->set('name', basename($target)); // Nom par defaut = repertoire ou fichier
        $this->simpleFormUnitRequestData($formUnit, $twigVars, $modData);
        $ref = Hash::merge($ref, current($modData));
    }

    /**
     * Ajoute des metadonnées à une cible tree_search
     * @param EntityInterface $formUnit
     * @param array           $twigVars
     * @param array           $requestData
     * @param array           $files
     * @throws FormMakerException
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    private function alterArchiveUnitBySearch(
        EntityInterface $formUnit,
        array $twigVars,
        array &$requestData,
        array $files
    ) {
        $expr = ltrim($formUnit->get('search_expression'), '/');
        $regex = str_replace('\\*', '.*', preg_quote($expr, '/'));
        $matches = preg_grep("/^(?:[^\/]+\/)?$regex\/?$/", $files);
        foreach ($matches as $target) {
            $ref = &$this->searchRef(trim($target, '/'), $requestData);
            if (!$ref) {
                continue;
            }
            $formUnit->set('name', basename($target)); // Nom par defaut = repertoire ou fichier
            $modData = [];
            $this->simpleFormUnitRequestData($formUnit, $twigVars, $modData);
            $ref = Hash::merge($ref, current($modData));
        }
    }

    /**
     * Donne la référence vers le requestData qui correspond à un filename
     * @param string $target
     * @param array  $requestData
     * @param bool   $firstNode
     * @return array|null
     * @codingStandardsIgnoreStart
     */
    private function &searchRef(string $target, array &$requestData, bool $firstNode = true): ?array
    {
        $currentNode = Hash::get($requestData, '_tree_path');
        if (!$currentNode && $firstNode && empty($requestData['_node_path'])) {
            $target = '/' . $target;
        }
        $null = null;
        if (str_contains($target, '/')) {
            [$nextNode, $nextTarget] = explode('/', $target, 2);
            if ($currentNode !== $nextNode && $firstNode === false) {
                return $null;
            }
            foreach ($requestData['ArchiveUnits'] ?? [] as $key => $values) {
                $ref = &$this->searchRef($nextTarget, $requestData['ArchiveUnits'][$key], false);
                if ($ref) {
                    return $ref;
                }
            }
        } elseif ($currentNode === $target) {
            return $requestData;
        }
        return $null;
    }

    /**
     * Prépare un transfert à partir des données du formulaire
     * @throws Exception
     */
    public function exportFiles()
    {
        $tmpDir = $this->deposit->get('path') . DS . 'tmp';
        if (!is_dir($tmpDir . DS . 'management_data')) {
            Filesystem::mkdir($tmpDir . DS . 'management_data', 0755);
        }
        if (is_dir($tmpDir . DS . 'original_data')) {
            Filesystem::remove($tmpDir . DS . 'original_data');
        }
        Filesystem::mkdir($tmpDir . DS . 'original_data', 0755);
        $dom = new DOMDocument();
        $dom->formatOutput = true;
        $dom->preserveWhiteSpace = false;
        $dom->loadXML($this->xml);
        $dom->save($this->deposit->get('path') . '/transfer.xml');

        /** @var DOMElement $element */
        foreach ($dom->getElementsByTagName('Attachment') as $element) {
            $filename = $element->getAttribute('filename');
            if (!isset($this->attachments[$filename])) {
                throw new GenericException(__("Attachment non trouvé: {0}", $filename));
            }
            $newPath = $tmpDir . DS . 'original_data' . DS . $filename;
            $newDir = dirname($newPath);
            if (!is_dir($newDir)) {
                Filesystem::mkdir($newDir, 0755);
            }
            if (!is_link($newPath)) {
                symlink(
                    $this->attachments[$filename],
                    $newPath
                );
            }
        }
    }

    /**
     * Ajoute les métadonnées de fichier
     * @param array $twigVars
     * @return array
     */
    private function addFileMetadata(array $twigVars): array
    {
        $FormInputs = TableRegistry::getTableLocator()->get('FormInputs');
        $query = $FormInputs->find()
            ->where(
                [
                    'FormInputs.form_id' => $this->form_id,
                    'FormInputs.type' => FormInputsTable::TYPE_FILE,
                    'FormInputs.multiple IS' => false,
                ]
            )
            ->contain(['FormFieldsets']);
        $meta = $twigVars['meta'] ?? [];
        /** @var EntityInterface $formInput */
        foreach ($query as $formInput) {
            if (Hash::get($formInput, 'form_fieldset.repeatable')) {
                $this->getMetaFromSection($meta, $twigVars, $formInput);
            } else {
                $fileupload_id = Hash::get($twigVars, 'input.'.$formInput->get('name'));
                if ($fileupload_id) {
                    $meta[$formInput->get('name')] = $this->extractFileMetadata($fileupload_id);
                }
            }
        }
        return $meta;
    }

    /**
     * Extraction des meta pour les sections
     * @param array           $meta
     * @param array           $twigVars
     * @param EntityInterface $formInput
     */
    private function getMetaFromSection(array &$meta, array $twigVars, EntityInterface $formInput)
    {
        $ord = Hash::get($formInput, 'form_fieldset.ord');
        $section = sprintf('section-%d', $ord);
        $path = sprintf('input.%s', $section);
        foreach (Hash::get($twigVars, $path) as $key => $values) {
            $fileupload_id = Hash::get(
                $twigVars,
                sprintf('input.%s.%d.%s', $section, $key, $formInput->get('name'))
            );
            if ($fileupload_id) {
                $meta[$section][$key][$formInput->get('name')] = $this->extractFileMetadata($fileupload_id);
            }
        }
    }

    /**
     * Donne les données liés à un fileupload
     * @param int $fileupload_id
     * @return array
     */
    private function extractFileMetadata(int $fileupload_id): array
    {
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $file = $Fileuploads->find()
            ->where(['Fileuploads.id' => $fileupload_id])
            ->contain(['Siegfrieds'])
            ->firstOrFail();
        return [
            'name' => $file->get('name'),
            'size' => $file->get('size'),
            'mime' => $file->get('mime'),
            'pronom' => Hash::get($file, 'siegfried.pronom'),
            'ext' => strtolower(pathinfo($file['filename'], PATHINFO_EXTENSION)),
        ];
    }

    /**
     * Génère un résultat du twig à partir de $this->twigData et de $template
     * @param string $template
     * @param array  $twigData
     * @return string
     * @throws LoaderError
     * @throws SyntaxError
     */
    private function twig(string $template, array $twigData): string
    {
        try {
            return htmlspecialchars_decode(
                Twig::render($template, $twigData),
                ENT_QUOTES
            );
        } catch (RuntimeError $e) {
            $this->_errors['twig'] = __(
                "Impossible d'interpréter le code twig suivant: {0}",
                h($template)
            );
            $this->_errors['exception'] = $e->getMessage();
            return '';
        }
    }

    /**
     * Envoi des informations sur le websocket
     * @param string $message
     * @return void
     * @throws ZMQSocketException
     */
    private function emit(string $message): void
    {
        Utility::get('Notify')->emit(
            sprintf(
                'form_maker_%d_%d_%s',
                $this->user_id,
                $this->form_id,
                md5($this->getDepositName() . '_salted') // NOSONAR md5 ok dans ce cas
            ),
            $message
        );
    }

    /**
     * Méthode générale pour alterArchiveUnitByPath et alterArchiveUnitBySearch
     * @param EntityInterface $formUnit
     * @param array           $twigVars
     * @param array           $requestData
     * @param array           $files
     * @return void
     * @throws FormMakerException
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    protected function alterArchiveUnits(
        EntityInterface $formUnit,
        array $twigVars,
        array &$requestData,
        array $files
    ) {
        foreach ($formUnit->get('children') as $child) {
            if ($child->get('type') === FormUnitsTable::TYPE_TREE_PATH) {
                $this->alterArchiveUnitByPath(
                    $child,
                    $twigVars,
                    $requestData,
                    $files
                );
            } elseif ($child->get('type') === FormUnitsTable::TYPE_TREE_SEARCH) {
                $this->alterArchiveUnitBySearch(
                    $child,
                    $twigVars,
                    $requestData,
                    $files
                );
            }
        }
    }

    /**
     * Validation pour les input de type fichier
     * @param Validator       $validator
     * @param EntityInterface $formInput
     * @return void
     */
    protected function validateTypeFile(Validator $validator, EntityInterface $formInput): void
    {
        $rule = function ($value) use ($formInput) {
            return $this->validationForTypeFile($formInput, $value);
        };

        $name = $formInput->get('name');
        $validator->add(
            $name,
            'custom',
            [
                'rule' => $rule,
                'message' => __(
                    "Ce format de fichier n'est pas autorisé."
                ),
            ]
        );
    }

    /**
     * Validation pour les input de type fichier (intérieur de la fonction anonyme)
     * @param EntityInterface $formInput
     * @param array|int       $value
     * @return bool
     */
    private function validationForTypeFile(EntityInterface $formInput, $value): bool
    {
        if (empty($formInput->get('formats'))) { // all
            return true;
        }

        $values = is_array($value) ? $value : [$value];

        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $valid = true;
        foreach ($values as $value) {
            $file = $Fileuploads->get($value);
            $mime = mime_content_type($file->get('path'));
            foreach ($formInput->get('formats') as $format) {
                if ($this->thisFileFormatIsValid($mime, $format)) {
                    continue 2;
                }
            }
            $valid = false; // on n'a trouvé aucun format ok
            break;
        }

        return $valid;
    }

    /**
     * Vérifi le que le format correspond au mime
     * @param string $mime
     * @param string $format
     * @return bool
     */
    private function thisFileFormatIsValid(string $mime, string $format): bool
    {
        $valid = false;
        switch ($format) {
            case 'csv':
                $valid = $mime === 'text/plain'
                    || str_contains($mime, $format);
                break;
            case 'image':
            case 'video':
            case 'audio':
            case 'xml':
            case 'json':
            case 'pdf':
                $valid = str_contains($mime, $format);
                break;
            case 'archive':
                $valid = in_array(
                    $mime,
                    [
                        'application/x-compressed',
                        'application/x-gzip',
                        'application/x-tar',
                        'application/x-zip-compressed',
                        'application/zip',
                        'multipart/x-gzip',
                        'multipart/x-zip',
                    ]
                );
                break;
            case 'rng_xsd':
            case 'rng':
            case 'xsd':
                $valid = str_contains($mime, 'xml');
                break;
            case 'text':
                $valid = in_array(
                    $mime,
                    [
                        'application/msword',
                        'application/vnd.oasis.opendocument.text',
                        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                        'text/plain',
                    ]
                );
                break;
            case 'calc':
                $valid = in_array(
                    $mime,
                    [
                        'application/excel',
                        'application/vnd.ms-excel',
                        'application/vnd.oasis.opendocument.spreadsheet',
                        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        'application/x-excel',
                        'application/x-msexcel',
                        'text/csv',
                    ]
                );
                break;
            case 'presentation':
                $valid = in_array(
                    $mime,
                    [
                        'application/mspowerpoint',
                        'application/powerpoint',
                        'application/vnd.ms-powerpoint',
                        'application/vnd.oasis.opendocument.presentation',
                        'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                        'application/x-mspowerpoint',
                    ]
                );
                break;
            case 'office':
                $valid = in_array(
                    $mime,
                    [
                        'application/excel',
                        'application/mspowerpoint',
                        'application/msword',
                        'application/powerpoint',
                        'application/vnd.ms-excel',
                        'application/vnd.ms-excel',
                        'application/vnd.ms-powerpoint',
                        'application/vnd.oasis.opendocument.presentation',
                        'application/vnd.oasis.opendocument.spreadsheet',
                        'application/vnd.oasis.opendocument.spreadsheet',
                        'application/vnd.oasis.opendocument.text',
                        'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                        'application/x-excel',
                        'application/x-msexcel',
                        'application/x-mspowerpoint',
                        'text/csv',
                        'text/plain',
                    ]
                );
                break;
        }
        return $valid;
    }

    /**
     * Ajoute un fichier au flattenData
     * @param EntityInterface|null $input
     * @param array                $flattenData
     * @param mixed                $values
     * @return void
     */
    private function flattenDataFiles(?EntityInterface $input, array &$flattenData, $values)
    {
        if (!$input) {
            return;
        }
        foreach ((array)$values as $value) {
            $flattenData[] = [
                'id' => $input->id,
                'type' => $input->get('type'),
                'fileupload_ids' => $value,
            ];
        }
    }

    /**
     * Lock les fichier uploadés et les déplacent dans le bon dossier
     * Décompresse également les inputs de type zip
     * @param array $data
     * @return void
     * @throws DataCompressorException|FormMakerException
     */
    private function lockFiles(array $data)
    {
        $FormInputs = TableRegistry::getTableLocator()->get('FormInputs');
        $query = $FormInputs->find()
            ->where(
                [
                    'FormInputs.form_id' => $this->form_id,
                    'FormInputs.type IN' => [
                        FormInputsTable::TYPE_FILE,
                        FormInputsTable::TYPE_ARCHIVE_FILE,
                    ],
                ]
            );
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $size = 0;
        $renames = [];
        $compressed = [];

        $fileInputNames = [];
        /** @var EntityInterface $input */
        foreach ($query as $input) {
            $fileInputNames[$input->get('name')] = $input;
        }
        // gestion des sections multiple, on ramène tout au même niveau
        $flattenData = [];
        foreach ($data as $key => $val) {
            if (preg_match('/^section-(\d+)$/', $key)) {
                foreach ($val as $sectionValues) {
                    foreach ($sectionValues as $inputName => $values) {
                        $this->flattenDataFiles(
                            $fileInputNames[$inputName] ?? null,
                            $flattenData,
                            $values
                        );
                    }
                }
            } else {
                $this->flattenDataFiles($fileInputNames[$key] ?? null, $flattenData, $val);
            }
        }
        $nbFiles = 0;
        // chaque fichier est déplacé selon son id et décompréssé si besoin
        foreach ($flattenData as $inputData) {
            $values = $inputData['fileupload_ids'];
            if (!$values) {
                continue;
            }
            $files = $Fileuploads->find()
                ->where(
                    [
                        'id IN' => $values,
                        'user_id' => $this->user_id,
                    ]
                )
                ->toArray();
            /** @var EntityInterface $fileupload */
            foreach ($files as $fileupload) {
                $nbFiles++;
                $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
                $inputName = $fileupload->get('name');
                $newPath = sprintf(
                    '%s/upload/%d/%d/%s',
                    $this->deposit->get('path'), // /data/versae-data/deposits/<id>_<token>
                    $inputData['id'],
                    $fileupload->id,
                    $inputName
                );

                $dir = dirname($newPath);
                if (!is_dir($dir)) {
                    mkdir($dir, 0777, true);
                }
                $renames[] = [
                    'fileupload' => $fileupload,
                    'new_path' => $newPath,
                ];
                $size += $fileupload->get('size');

                if ($inputData['type'] === FormInputsTable::TYPE_ARCHIVE_FILE) {
                    if (!file_exists($newPath)) {
                        rename($fileupload->get('path'), $newPath);
                    }
                    $uncompressedDir = sprintf(
                        '%s/uncompressed/%d/%d',
                        $this->deposit->get('path'), // /data/versae-data/deposits/<id>_<token>
                        $inputData['id'],
                        $fileupload->id
                    );
                    if (!is_dir($uncompressedDir)) {
                        mkdir($uncompressedDir, 0777, true);
                    }
                    if (
                        strpos($fileupload->get('mime'), 'zip')
                        && count(glob($uncompressedDir . '/*')) === 0
                    ) {
                        DataCompressor::sanitizeZip($newPath);
                    }
                    $compressed[] = ['zip' => $newPath, 'target_dir' => $uncompressedDir];
                }
            }
        }

        if ($nbFiles === 0) {
            throw FormMakerException::create(
                'BinaryDataObject',
                __("Aucun fichier n'est présent dans le bordereau")
            );
        }

        LimitBreak::setTimeLimit($size / 100000); // 2.5Go ~= 7h30
        LimitBreak::setMemoryLimit($size * 10); // 46Mo = 460Mo
        foreach ($renames as $rename) {
            /** @var EntityInterface $fileupload */
            $fileupload = $rename['fileupload'];
            if (
                is_file($fileupload->get('path'))
                && !file_exists($rename['new_path'])
                && rename($fileupload->get('path'), $rename['new_path'])
            ) {
                $fileupload->set('path', $rename['new_path']);
                $fileupload->set('locked', true);
                $Fileuploads->saveOrFail($fileupload);
            }
        }
        foreach ($compressed as $toUncompress) {
            if (count(glob($toUncompress['target_dir'] . '/*')) === 0) {
                DataCompressor::uncompress(
                    $toUncompress['zip'],
                    $toUncompress['target_dir'],
                    Configure::read('DataCompressor.forceCommandLine', false)
                );
            }
            $nbFiles += count(Filesystem::listFiles($toUncompress['target_dir']));
        }
        // corrige les limites en fonction des fichiers décompréssés
        LimitBreak::setTimeLimit($size / 100000 + $nbFiles);
        LimitBreak::setMemoryLimit(($size + $nbFiles) * 10);
    }

    /**
     * Donne le nom du deposit (si il existe, noname sinon)
     * @return string
     */
    private function getDepositName(): string
    {
        if ($this->deposit) {
            return $this->deposit->get('name') ?: 'noname';
        }
        return 'noname';
    }

    /**
     * Chemin issue du form_unit
     * @param EntityInterface $formUnit
     * @param string          $relative
     * @return string
     */
    private function getPath(EntityInterface $formUnit, string $relative = ''): string
    {
        $fullname = $formUnit->get('full_name');
        if ($fullname) {
            $fullname .= '/';
        }
        return $fullname.Translit::safeUri($relative);
    }

    /**
     * Donne un UUID encodé en base 64 pour diminuer sa taille
     * @param string|null $uuid si null, un nouveau uuid est fourni
     * @return string
     */
    public static function shortUUID(string $uuid = null): string
    {
        if (!$uuid) {
            $uuid = uuid_create(UUID_TYPE_TIME);
        }

        // on met les parties qui changent beaucoup au début et à la fin
        [$tlow, $tmid, $thigh, $clock, $node] = explode('-', $uuid);
        $reformated = $clock . $node . $tmid . $thigh . $tlow;

        $raw = '';
        foreach(str_split($reformated, 2) as $pair){
            $raw .= chr(hexdec($pair));
        }
        $encoded = base64_encode($raw);
        return str_replace(['+', '/', '='], ['.', '-', ''], $encoded);
    }

    /**
     * Donne un UUID encodé en base 64 pour diminuer sa taille
     * @param string $shortUUID
     * @return string
     */
    public static function decodeShortUUID(string $shortUUID): string
    {
        $base64 = str_replace(['.', '-'], ['+', '/'], $shortUUID) . '==';
        $decoded = bin2hex(base64_decode($base64));

        $clock = substr($decoded, 0, 4);
        $node = substr($decoded, 4, 12);
        $tmid = substr($decoded, 16, 4);
        $thigh = substr($decoded, 20, 4);
        $tlow = substr($decoded, 24, 8);

        return implode('-', [$tlow, $tmid, $thigh, $clock, $node]);
    }

    /**
     * Ajoute une validation à partir du type et du nom dans $formInput
     * @param Validator       $validator
     * @param EntityInterface $formInput
     * @return void
     */
    private function appendFormInputToValidation(Validator $validator, EntityInterface $formInput): void
    {
        $name = $formInput->get('name');
        switch ($formInput->get('type')) {
            case FormInputsTable::TYPE_CHECKBOX:        // case à cocher
                if ($formInput->get('value') === '1') {
                    $validator->boolean($name);
                } else {
                    $validator->scalar($name);
                }
                break;
            case FormInputsTable::TYPE_DATE:            // date
                $validator->add(
                    $name,
                    'date',
                    ['rule' => [$this, 'validateDate']]
                );
                $validator->add(
                    $name,
                    'min_date',
                    ['rule' => $this->validatorDateLimit($formInput, 'min_date')]
                );
                $validator->add(
                    $name,
                    'max_date',
                    ['rule' => $this->validatorDateLimit($formInput, 'max_date')]
                );
                break;
            case FormInputsTable::TYPE_DATETIME:        // date et heure
                $validator->add(
                    $name,
                    'datetime',
                    ['rule' => [$this, 'validateDatetime']]
                );
                $validator->add(
                    $name,
                    'min_date',
                    ['rule' => $this->validatorDateLimit($formInput, 'min_date', true)]
                );
                $validator->add(
                    $name,
                    'max_date',
                    ['rule' => $this->validatorDateLimit($formInput, 'max_date', true)]
                );
                break;
            case FormInputsTable::TYPE_MULTI_CHECKBOX:  // case à cocher multiple
                break;
            case FormInputsTable::TYPE_NUMBER:          // champ numérique
                $validator->numeric($name);
                $validator->add(
                    $name,
                    'min',
                    ['rule' => function ($value) use ($formInput) {
                        $app_meta = json_decode($formInput->get('app_meta'), true);
                        if (!isset($app_meta['min'])) {
                            return true;
                        }
                        return $value > $app_meta['min'];
                    }]
                );
                $validator->add(
                    $name,
                    'max',
                    ['rule' => function ($value) use ($formInput) {
                        $app_meta = json_decode($formInput->get('app_meta'), true);
                        if (!isset($app_meta['max'])) {
                            return true;
                        }
                        return $value < $app_meta['max'];
                    }]
                );
                break;
            case FormInputsTable::TYPE_PARAGRAPH:       // paragraphe
                return;
            case FormInputsTable::TYPE_SELECT:          // sélection entre plusieurs options
            case FormInputsTable::TYPE_TEXT:            // zone de texte simple
            if (!$formInput->get('multiple')) {
                    $validator->scalar($name);
                }
                break;
            /** @noinspection PhpMissingBreakStatementInspection */
            case FormInputsTable::TYPE_FILE:            // fichier
                $this->validateTypeFile($validator, $formInput);
                if ($formInput->get('multiple')) {
                    return;
                }
            // intentional fallthrough
            // no break
            case FormInputsTable::TYPE_TEXTAREA:        // zone de texte sur plusieurs lignes
            case FormInputsTable::TYPE_ARCHIVE_FILE:    // fichier compressé
            case FormInputsTable::TYPE_EMAIL:           // email
            case FormInputsTable::TYPE_RADIO:           // bouton radio
            default:
                $validator->scalar($name);
        }
        if ($formInput->get('required')
            && !Hash::get($formInput, 'form_fieldset.repeatable')
            && !$formInput->get('multiple')
        ) {
            $validator->requirePresence(
                $name,
                $this->validateRequiredField($name)
            );
            $validator->notEmptyString($name);
        } else {
            $validator->allowEmptyString($name);
        }
        if ($formInput->get('pattern') && !$formInput->get('multiple')) {
            $validator->regex($name, self::htmlPatternToPhpRegex($formInput->get('pattern')));
        }
    }

    /**
     * Conversion entre un regex html5 et php
     * La notation \u00C0 doit être convertie en \x{00C0}
     * @param string $pattern
     * @return string
     */
    private static function htmlPatternToPhpRegex(string $pattern): string
    {
        $regex = '/\\\\u([0-9A-Fa-f]{4})/';
        $replacement = '\\\\x{$1}';
        $convertedPattern = preg_replace($regex, $replacement, $pattern);

        return '/^' . addcslashes($convertedPattern, '/') . '$/u';
    }

    /**
     * Donne la liste des entités fileuploads à partir des ids dans $inputs
     * @param int             $user_id
     * @param array           $data
     * @param EntityInterface $formEntity
     * @param array           $fileuploads
     * @return array
     */
    public static function extractFileuploadsFromRequestData(
        $user_id,
        array $data,
        EntityInterface $formEntity,
        array $fileuploads = []
    ): array {
        $fieldsets = $formEntity->get('form_fieldsets') ?: [];
        /** @var EntityInterface $fieldset */
        foreach ($fieldsets as $fieldset) {
            $inputEntities = $fieldset->get('form_inputs');
            if ($fieldset->get('repeatable')) {
                $section = sprintf('section-%d', $fieldset->get('ord'));
                $fieldsetData = $data['inputs'][$section] ?? null;
            } else {
                $fieldsetData = [$data['inputs'] ?? null];
            }
            foreach ($fieldsetData ?? [] as $inputs) {
                self::extractFileuploadsFromInputs(
                    $inputs ?: [],
                    $inputEntities,
                    $fileuploads,
                    $user_id
                );
            }
        }

        $fileuploads = array_filter($fileuploads);
        // supprime l'identifiant
        foreach ($fileuploads as $name => $values) {
            $fileuploads[$name] = array_values($values);
        }
        return $fileuploads;
    }

    /**
     * Récupère une liste de fileuploads à partir d'une liste d'inputs
     * @param array $data
     * @param array $inputEntities
     * @param array $fileuploads
     * @param int   $user_id
     * @return void
     */
    private static function extractFileuploadsFromInputs(
        array $data,
        array $inputEntities,
        array &$fileuploads,
        int $user_id
    ): void {
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $types = [FormInputsTable::TYPE_FILE, FormInputsTable::TYPE_ARCHIVE_FILE];
        foreach ($inputEntities as $input) {
            if (
                !empty($data[$input['name']])
                && in_array($input['type'], $types)
            ) {
                foreach ((array)$data[$input['name']] as $fileupload_id) {
                    $fileuploads[$input['name']][$fileupload_id] = $Fileuploads->find()
                        ->where(
                            [
                                'id' => $fileupload_id,
                                'user_id' => $user_id,
                            ]
                        )
                        ->first();
                }
            }
        }
    }

    /**
     * Cache des inputs
     * @return EntityInterface[]
     */
    private function getInputs(): array
    {
        if (!$this->inputs) {
            $FormInputs = TableRegistry::getTableLocator()->get('FormInputs');
            $this->inputs = $FormInputs->find()
                ->where(['FormInputs.form_id' => $this->form_id])
                ->contain(['FormFieldsets'])
                ->toArray();
        }
        return $this->inputs;
    }

    /**
     * Affichage d'un message d'erreur de formulaire en fonction de la réponse
     * @param Response $result
     * @return void
     */
    private function handleSedaGeneratorErrors(Response $result)
    {
        $responseJson = json_decode($result->getStringBody(), true);
        if (!$responseJson || !isset($responseJson['validation-errors'])) {
            if (empty($responseJson['message'])) {
                $body = $result->getBody();
                $body->rewind();
                $message = $body->getContents();
            } else {
                $message = $responseJson['message'];
            }
            $this->setErrors(
                [
                    'HttpError' => [
                        'error' => 'code ' . $result->getStatusCode(),
                    ],
                    'Message' => [
                        'msg' => $message,
                    ],
                ]
            );
        } else {
            if (!empty($responseJson['xml'])) {
                file_put_contents(TMP . 'debug_last_form_generate.xml', $responseJson['xml']);
                $this->xml = $responseJson['xml'];
            }
            $errors = [];
            foreach ($responseJson['validation-errors'] as $i => $err) {
                $errors['error_' . $i] = sprintf(
                    'line: %d ; column: %d ; message: %s',
                    $err['line'] ?? 0,
                    $err['column'] ?? 0,
                    $err['message'] ?? __("Une erreur inattendue a eu lieu")
                );
            }
            $this->setErrors(['SchemaValidation' => $errors]);
        }
    }

    /**
     * Donne l'emplacement dans currentData correspondant au $name
     * examples:
     * -  cas 1: OriginatingAgency_Identifier
     * -  cas 2: RelatedObjectReference_IsPartOf_RepositoryArchiveUnitPID
     * -  cas 3: Coverage_0_Spatial
     * -  cas 4: AuthorizedAgent_0_BirthPlace_City
     * -  cas 5: Signature_0_ReferencedObject_SignedObjectDigest@algorithm
     * @param string $name
     * @param string $descNodeName
     * @param array  $currentData
     * @return array
     */
    private function &extractDeepRef(string $name, string $descNodeName, array &$currentData): array
    {
        $exp = explode('_', $name, 3);
        [$parentName, $currentName] = $exp;
        $subNode = $exp[2] ?? '';
        $subSubNode = null;
        $attr = null;
        if (str_contains($exp[2] ?? '', '_')) {
            [$subNode, $subSubNode] = explode('_', $exp[2], 2);
            if (str_contains($subSubNode, '@')) {
                [$subSubNode, $attr] = explode('@', $subSubNode);
            }
        }
        if (!isset($currentData[$descNodeName][$parentName])) {
            $currentData[$descNodeName][$parentName] = [];
        }
        if ($subNode) {
            if (!isset($currentData[$descNodeName][$parentName][$currentName][$subNode])) {
                $currentData[$descNodeName][$parentName][$currentName][$subNode] = [];
            }
            $ref = &$currentData[$descNodeName][$parentName][$currentName][$subNode];
            if (isset($subSubNode)) {
                if (!is_array($ref)) {
                    $ref = []; // ignore les anciennes valeurs incorrectes
                }
                if (isset($attr) && isset($ref[$subSubNode])) {
                    $ref[$subSubNode] = ['value' => $ref[$subSubNode]];
                }
                $ref = &$ref[$subSubNode];
                if (empty($ref)) {
                    $ref = [];
                }
                if (isset($attr)) {
                    $ref = &$ref['@' . $attr];
                } elseif (count($ref) > 0) {
                    $ref = &$ref['value'];
                }
            }
        } else {
            if (is_string($currentData[$descNodeName][$parentName])) {
                $currentData[$descNodeName][$parentName] = [$currentData[$descNodeName][$parentName]];
            }
            $ref = &$currentData[$descNodeName][$parentName][$currentName];
        }
        if (!is_array($ref)) {
            $ref = [];
        }
        return $ref;
    }

    /**
     * @param EntityInterface $formInput
     * @param string          $field
     * @param bool            $datetime
     * @return Closure
     */
    protected function validatorDateLimit(
        EntityInterface $formInput,
        string $field,
        bool $datetime = false
    ): Closure {
        return function ($value) use ($formInput, $field, $datetime) {
            $app_meta = json_decode($formInput->get('app_meta'), true);
            if (!isset($app_meta[$field])) {
                return true;
            }

            $appDate = $app_meta[$field];
            preg_match_all('/[-+]?\d+[YMWDymwd]/', $appDate, $m);
            if ($m[0]) {
                $date = new DateTime();
                $date->setTime(0, 0);
                foreach ($m[0] as $val) {
                    $unit = strtoupper(substr($val, -1));
                    $mValue = substr(
                        $val,
                        in_array(substr($val, 0, 1), ['+', '-']) ? 1 : 0,
                        -1
                    );
                    str_starts_with($val, '-')
                        ? $date->sub((new DateInterval("P$mValue$unit")))
                        : $date->add((new DateInterval("P$mValue$unit")));
                }
            } else {
                $date = new DateTime($appDate);
            }

            if ($datetime) {
                preg_match('/^(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2})$/', $value, $m);
                $dateValue = new DateTime($m[3] . '-' . $m[2] . '-' . $m[1] . ' ' . $m[4]. ':' . $m[5]);
            } else {
                preg_match('/^(\d{2})\/(\d{2})\/(\d{4})$/', $value, $m);
                $dateValue = new DateTime($m[3] . '-' . $m[2] . '-' . $m[1]);
            }

            return $field === 'min_date'
                ? $dateValue >= $date
                : $dateValue <= $date;
        };
    }
}
