#!/bin/bash

apache_user=$(sudo apachectl -S 2>/dev/null | grep User | sed 's/.*name="\([^"]*\)".*/\1/');

echo "Drop et création de la BDD mink..."
database_name=$(sudo -u "$apache_user" bin/cake configuration get Datasources.mink.database --raw)
database_username=$(sudo -u "$apache_user" bin/cake configuration get Datasources.mink.username --raw)
database_password=$(sudo -u "$apache_user" bin/cake configuration get Datasources.mink.password --raw)
database_host=$(sudo -u "$apache_user" bin/cake configuration get Datasources.mink.host --raw)
database_port=$(sudo -u "$apache_user" bin/cake configuration get Datasources.mink.port --raw)
run_sql () {
  PGPASSWORD="$database_password" psql \
    -h "$database_host" \
    -p "$database_port" \
    -U "$database_username" \
    -c "$1"
}
run_sql "drop database $database_name;"
run_sql "create database $database_name owner $database_username;"

sudo -u "$apache_user" bin/cake migrations migrate --connection mink || exit 1
sudo -u "$apache_user" bin/cake fixtures database --datasource mink --with-volumes || exit 1
sudo -u "$apache_user" vendor/bin/mink "$@"
