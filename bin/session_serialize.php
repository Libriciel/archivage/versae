#!/usr/bin/php -q
<?php
/**
 * Permet d'utiliser session_decode() dans n'importe quel contexte (ex testunit)
 * utilisé par SessionsTable pour récupérer les données de session
 */
session_start();

$data = unserialize(base64_decode($argv[1]));

$currentSession = session_encode();
$_SESSION = $data;
$serialized = session_encode();
$_SESSION = [];
session_decode($currentSession);

header('Content-Type', 'text/plain');
print $serialized.PHP_EOL;
exit;
