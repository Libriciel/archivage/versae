#!/usr/bin/php -q
<?php
$d = DIRECTORY_SEPARATOR . 'changelog' . DIRECTORY_SEPARATOR;

$files = [];
foreach (glob(dirname(__DIR__).$d.'CHANGELOG_*.md') as $filename) {
    if (preg_match('/CHANGELOG_(\d+\.\d+\.\d+).md$/', $filename, $m)) {
        $version = $m[1];
        $files[$version] = $filename;
    }
}
uksort(
    $files,
    function ($a, $b) {
        return version_compare($b, $a);
    }
);

echo <<<EOT
# Changelog


EOT;
foreach ($files as $filename) {
    include $filename;
    echo PHP_EOL.PHP_EOL;
}
