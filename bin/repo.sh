#!/bin/bash
# shellcheck disable=SC2164

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd "$DIR/.."
cd vendor/libriciel/asalae-assets || exit 1
pwd
git checkout master
git remote set-url origin git@gitlab.libriciel.fr:libriciel/pole-archivage/asalae/asalae-assets.git
git pull

cd "$DIR/.."
cd vendor/libriciel/asalae-core || exit 1
pwd
git checkout php81
git remote set-url origin git@gitlab.libriciel.fr:libriciel/pole-archivage/asalae/asalae-core.git
git pull

cd "$DIR/.."
cd vendor/libriciel/cakephp-beanstalk || exit 1
pwd
git checkout master
git remote set-url origin git@gitlab.libriciel.fr:CakePHP/cakephp-beanstalk.git
git pull

cd "$DIR/.."
cd vendor/libriciel/cakephp-datacompressor || exit 1
pwd
git checkout master
git remote set-url origin git@gitlab.libriciel.fr:CakePHP/cakephp-datacompressor.git
git pull

cd "$DIR/.."
cd vendor/libriciel/cakephp-filesystem || exit 1
pwd
git checkout master
git remote set-url origin git@gitlab.libriciel.fr:CakePHP/cakephp-filesystem.git
git pull

cd "$DIR/.."
cd vendor/libriciel/cakephp-login-page || exit 1
pwd
git checkout master
git remote set-url origin git@gitlab.libriciel.fr:CakePHP/cakephp-login-page.git
git pull

cd "$DIR/.."
cd vendor/libriciel/file-converters || exit 1
pwd
git checkout master
git remote set-url origin git@gitlab.libriciel.fr:file-converters/file-converters.git
git pull

cd "$DIR/.."
cd vendor/libriciel/file-validator || exit 1
pwd
git checkout master
git remote set-url origin git@gitlab.libriciel.fr:file-validator/file-validator.git
git pull

cd "$DIR/.."
cd vendor/libriciel/seda2pdf || exit 1
pwd
git checkout master
git remote set-url origin git@gitlab.libriciel.fr:libriciel/pole-archivage/seda2pdf.git
git pull
