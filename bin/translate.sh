#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR
ROOT=$(dirname "$DIR")
./cake i18n extract --extract-core yes --merge no --overwrite --output $ROOT/src/Locale --paths $ROOT/src/,$ROOT/vendor/libriciel/
./cake traduction