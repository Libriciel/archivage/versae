#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR/.. || exit

mv config/Migrations/schema-dump-default.lock /tmp/schema-dump-default.lock

#bin/changelog_builder.php > CHANGELOG.md

bin/cake hash-dir \
 bin/cake.php \
 src/ \
 webroot/ \
 resources/ \
 templates/ \
 -r \
 --filter webroot/org-entity-data/\* \
 --filter resources/hashes.txt \
 --filter resources/hashes.txt.md5 \
 --filter *.pot \
 -o resources/hashes.txt

if [ $# -eq 0 ]; then
  version=`git rev-parse HEAD`
else
  version=$1
fi
ZIPNAME="versae-"`date +"%Y%m%d"`"-"$version".zip"
touch tmp/empty logs/empty
cat package_list.txt | zip -q -r@ $ZIPNAME -x '*.git*'
rm tmp/empty logs/empty

mv /tmp/schema-dump-default.lock config/Migrations/schema-dump-default.lock

echo $ZIPNAME
echo curl -T $ZIPNAME https://curl.libriciel.fr
