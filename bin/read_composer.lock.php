#!/usr/bin/php -q
<?php

$lock = file_get_contents(dirname(__DIR__).DIRECTORY_SEPARATOR.'composer.lock');
foreach (json_decode($lock, true)['packages'] as $package) {
    echo sprintf("%-40s%-15s%s\n", $package['name'], $package['version'], $package['source']['reference'] ?? '');
}
