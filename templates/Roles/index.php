<?php

/**
 * @var Versae\View\AppView $this
 * @var $userSa Versae\Model\Entity\OrgEntity
 * @var $tableIdRoles string
 * @var $tableIdRolesGlobaux string
 * @var $roles Cake\Datasource\ResultSetInterface
 * @var $roles_globals Cake\Datasource\ResultSetInterface
 */

use Versae\View\AppView;

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Administration"));
$this->Breadcrumbs->add(__("Rôles utilisateur"));
echo $this->Html->tag(
    'div.container',
    $this->Html->tag('h1', $this->Fa->i('fa-users', __("Rôles utilisateur")))
    . $this->Breadcrumbs->render()
);

$buttons = [];
if ($this->Acl->check('/roles/add')) {
    $buttons[] = $this->ModalForm
        ->create('add-role')
        ->modal(__("Ajout d'un rôle spécifique"))
        ->javascriptCallback('afterAddRole')
        ->output(
            'button',
            $this->Fa->charte('Ajouter', __("Ajouter un rôle spécifique")),
            '/Roles/add/' . $userSa->get('id')
        )
        ->generate(['class' => 'btn btn-success', 'type' => 'button']);
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}

echo $this->element('modal', ['idTable' => $tableIdRoles, 'paginate' => false]);
echo $this->element('modal', ['idTable' => $tableIdRolesGlobaux, 'paginate' => false]);

$actions = [
    'view' => [
        'onclick' => "loadViewModalRole({0})",
        'class' => 'btn-link',
        'label' => $this->Fa->charte('Visualiser'),
        'title' => $title = __("Visualiser {0}", '{1}'),
        'aria-label' => $title,
        'display' => $this->Acl->check('/roles/view'),
        'params' => ['Roles.id', 'Roles.name']
    ],
    'edit' => [
        'onclick' => "loadEditModalRole({0})",
        'class' => 'btn-link',
        'label' => $this->Fa->charte('Modifier'),
        'title' => $title = __("Modifier {0}", '{1}'),
        'aria-label' => $title,
        'display' => $this->Acl->check('/roles/edit'),
        'params' => ['Roles.id', 'Roles.name'],
        'displayEval' => "data[{index}].Roles.editable"
    ],
    'perm1' => [
        'onclick' => "loadEditRolePermissions({0})",
        'class' => 'btn-link',
        'label' => $this->Fa->i('fa-ban'),
        'title' => $title = __("Gérer les droits de {0}", '{1}'),
        'aria-label' => $title,
        'display' => $this->Acl->check('/Permissions/edit'),
        'params' => ['Roles.aro.id', 'Roles.name'],
        'displayEval' => "data[{index}].Roles.editable"
    ],
    'perm2' => [
        'onclick' => 'alert("' . __("Aro non défini pour ce rôle, merci de contacter un administrateur.") . '")',
        'class' => 'btn-link',
        'label' => $this->Fa->i('fa-ban text-danger'),
        'title' => $title = __("Gérer les droits de {0}", '{1}'),
        'aria-label' => $title,
        'display' => $this->Acl->check('/Permissions/edit'),
        'displayEval' => "!data[{index}].Roles.aro",
        'params' => ['Roles.id', 'Roles.name']
    ],
];

$tableRoles = $this->Table
    ->create($tableIdRoles, ['class' => 'table table-striped table-hover'])
    ->fields(
        [
            'Roles.name' => ['label' => __("Nom")],
            'Roles.parent_role.name' => ['label' => __("Hérite des droits")],
            'Roles.description' => ['label' => __("Description")],
            'Roles.active' => ['label' => __("Actif"), 'type' => 'boolean'],
            'Roles.hierarchical_view' => [
                'label' => __("Peut visualiser les éléments des entités filles"),
                'type' => 'boolean'
            ],
        ]
    )
    ->data($roles)
    ->params(
        [
            'identifier' => 'Roles.id',
        ]
    )
    ->actions($actions)
    ->actions(
        [
            function (AsalaeCore\View\Helper\Object\Table $table, AppView $view) {
                return [
                    'type' => "button",
                    'class' => "btn-link",
                    'data-callback' => sprintf(
                        "TableGenericAction.deleteAction(%s, '%s')({0}, false)",
                        $table->tableObject,
                        $view->Url->build($url = '/roles/delete')
                    ),
                    'display' => $view->Acl->check($url),
                    'label' => $view->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => $title = __("Supprimer {0}", '{1}'),
                    'aria-label' => $title,
                    'confirm' => __("Êtes-vous sûr de vouloir supprimer ce rôle ?"),
                    'displayEval' => "data[{index}].Roles.deletable",
                    'params' => ['Roles.id', 'Roles.name']
                ];
            }
        ]
    );

$tableRolesGlobaux = $this->Table
    ->create($tableIdRolesGlobaux, ['class' => 'table table-striped table-hover fixed-left-th'])
    ->fields(
        [
            'Roles.name' => ['label' => __("Nom")],
            'Roles.description' => ['label' => __("Description"), 'display' => false],
            'Roles.hierarchical_view' => [
                'label' => __("Peut visualiser les éléments des entités filles"),
                'type' => 'boolean'
            ],
        ]
    )
    ->data($roles_globals)
    ->params(
        [
            'identifier' => 'Roles.id',
        ]
    )
    ->actions(
        [
            $actions['view'],
            $actions['edit'],
            $actions['perm1'],
        ]
    );

// @see src/Template/OrgEntities/index.ctp
$jsTableRoles = $tableRoles->tableObject;
?>
<section class="container bg-white">
    <header>
        <h2 class="h4"><?=__("Liste des rôles spécifiques à {0}", $userSa->get('name'))?></h2>
        <div class="r-actions h4">
            <?=$tableRoles->getConfigureLink()?>
        </div>
    </header>
    <?=$tableRoles->generate()?>
</section>
<?php

echo $this->Html->tag(
    'div',
    $this->ModalView
        ->create('role-view')
        ->modal(__("Visualisation d'un rôle"))
        ->output('function', 'loadViewModalRole', '/Roles/viewGlobal')
        ->generate()
    . $this->ModalForm
        ->create('role-edit')
        ->modal(__("Modification d'un rôle"))
        ->javascriptCallback('afterEditRole')
        ->output('function', 'loadEditModalRole', '/Roles/edit')
        ->generate()
    . $this->ModalForm
        ->create('role-permission')
        ->modal(__("Gestion des droits du rôle"))
        ->output('function', 'loadEditRolePermissions', '/Permissions/edit')
        ->generate(),
    ['class' => 'col-md-12 form-group']
);
?>

<section class="container bg-white">
    <header>
        <h2 class="h4"><?=__("Liste des rôles globaux")?></h2>
        <div class="r-actions h4">
            <?=$tableRolesGlobaux->getConfigureLink()?>
        </div>
    </header>
    <?=$tableRolesGlobaux->generate()?>
</section>

<script>
    function afterAddRole(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            var table;
            table = <?=$jsTableRoles?>;
            table.data.push({Roles: content});
            TableGenerator.appendActions(table.data, table.actions);
            table.generateAll();
            if (content.active) {
                $('#roles-ids')
                    .append($('<option value="'+content.id+'"></option>').text(content.name))
                    .trigger("chosen:updated");
            }
        } else {
            AsalaeGlobal.colorTabsInError();
        }
    }

    function afterEditRole(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            var table;
            table = <?=$jsTableRoles?>;
            table.getDataId(content.id).Roles = content;
            TableGenerator.appendActions(table.data, table.actions);
            table.generateAll();
            if (content.active) {
                $('#roles-ids').find('option[value='+content.id+']')
                    .text(content.name)
                    .parent()
                    .trigger("chosen:updated");
            }
        } else {
            AsalaeGlobal.colorTabsInError();
        }
    }
</script>
