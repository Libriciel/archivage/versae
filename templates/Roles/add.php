<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->Form->create($entity, ['idPrefix' => 'add-role']);
require 'addedit-common.php';
echo $this->Form->end();
?>
<script>
    AsalaeGlobal.select2($('#add-role-type-entities-ids'), __("Rechercher"));
</script>
