<?php

/**
 * @var Versae\View\AppView $this
 * @var bool $canBeDesactivated
 * @var Cake\Datasource\ResultSetInterface $roles
 * @var Cake\ORM\Query $type_entities
 */

echo $this->Form->control('name', ['label' => __("Nom du rôle")])
    . $this->Form->control(
        'active',
        [
            'label' => __("Actif"),
            'disabled' => $canBeDesactivated === false
        ]
    )
    . $this->Form->control(
        'description',
        [
            'label' => __("Description"),
        ]
    )
    . $this->Form->control(
        'parent_id',
        [
            'label' => __("Hérite des droits de"),
            'options' => $roles,
            'empty' => __("-- Sélectionner un rôle parent --")
        ]
    )
    . $this->Form->control(
        'type_entities._ids',
        [
            'label' => __("Types d'entités"),
            'data-placeholder' => __("-- Sélectionner les types d'entités disponibles --"),
            'options' => $type_entities,
            'required' => true,
            'help' => __("Types d'entités dont les utilisateurs peuvent utiliser ce rôle")
        ]
    )
    . (
        $entity->get('agent_type') !== 'software'
            ? $this->Form->control(
                'hierarchical_view',
                [
                    'label' => __("Peut visualiser les éléments des entités filles"),
                ]
            )
            : ''
    );
