<?php

/**
 * @var Versae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */

use Cake\Core\Configure;
use Cake\Utility\Hash;
use Cake\Utility\Text;

$view = $this;
$prefix = 'role-view-cat-';

$loading = $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);

$infos = $this->Html->tag('article');
$infos .= $this->Html->tag('header.bottom-space');
$infos .= $this->Html->tag('h3', h($role->get('name')));
$infos .= $this->Html->tag('/header');

$infos .= $this->Html->tag('h4', __("Rôle"));
$infos .= $this->ViewTable->generate(
    $role,
    [
        __("Nom du rôle") => 'name',
        __("Description") => 'description',
        __("Actif") => 'active',
        __("Hérite des droits de") => 'parent_role.name',
        __("Types d'entités") => 'type_entities.{n}.name',
        __("Peut visualiser les éléments des entités filles") => 'hierarchical_view',
    ]
);
$infos .= $this->Html->tag('/article');

$tabs = $this->Tabs->create('tabs-view-role', ['class' => 'row no-padding']);

$tabs->add(
    'tab-view-role-infos',
    $this->Fa->i('fa-file-code-o', __("Informations principales")),
    $infos
);

$technical = Configure::read('Permissions.display_technical_name');

$permissions = '';

// tri par groupe
$groups = [];
$optionsGroups = [];
$emptyCategory = __("-- sans catégorie --");
foreach ($controllers as $controller => $actions) {
    foreach ($actions as $action => $params) {
        $params += ['group' => ''];
        if ($params['group'] === '') {
            $params['group'] = $emptyCategory;
        }
        if (!isset($groups[$params['group']])) {
            $groups[$params['group']] = [];
        }
        if (!isset($groups[$params['group']][$controller])) {
            $groups[$params['group']][$controller] = [];
        }
        $groups[$params['group']][$controller][$action] = $params;

        $groupOptionKey = $params['group'];
        if (!isset($optionsGroups[$groupOptionKey])) {
            $optionsGroups[$groupOptionKey] = [];
        }
        $ctrlOptionKey = $prefix . mb_strtolower(
            Text::slug(($groupOptionKey ? $groupOptionKey . '-' : '') . $controller, '-')
        );
        if (!isset($optionsGroups[$groupOptionKey][$ctrlOptionKey])) {
            $trad = $this->Translate->permission($controller);
            if ($technical) {
                $optionsGroups[$groupOptionKey][$ctrlOptionKey]
                    = $controller . ($trad !== $controller ? ' (' . $trad . ')' : '');
            } else {
                $optionsGroups[$groupOptionKey][$ctrlOptionKey] = $trad;
            }
        }
    }
}
$sorter = function ($a, $b) use ($emptyCategory) {
    if (str_starts_with($a, $emptyCategory)) {
        return 1;
    } elseif (str_starts_with($b, $emptyCategory)) {
        return -1;
    }
    return $a > $b ? 1 : -1;
};
uksort($optionsGroups, $sorter);
uksort($groups, $sorter);

$permissions .= $this->Html->tag('article');
$permissions .= $this->Html->tag('header.bottom-space');
$permissions .= $this->Html->tag('h4', h($aro->get('alias')));
$permissions .= $this->Html->tag('/header');
$permissions .= $this->Form->control(
    'select',
    [
        'id' => 'role-view-group-select',
        'label' => __("Modules"),
        'options' => $optionsGroups,
        'empty' => __("-- Scroller vers un module --"),
        'onchange' => 'scrollToPermission(this)'
    ]
);
$permissions .= $this->Form->create($form, ['class' => 'no-padding', 'id' => 'view-perms-form']);

$options = [
    '0' => __("Hérite"),
    '1' => __("Oui"),
    '-1' => __("Non"),
];

foreach ($groups as $group => $ctrlrs) {
    $permissions .= '<hr>';
    $permissions .= $this->Html->tag(
        'h4',
        h($group),
        ['id' => $prefix . mb_strtolower(Text::slug($group, '-'))]
    );
    $permissions .= $this->Html->tag('table.table.table-striped.table-hover');
    $permissions .= $this->Html->tag('thead');
    $permissions .= $this->Html->tag('tr');
    if ($technical = Configure::read('Permissions.display_technical_name')) {
        $permissions .= $this->Html->tag('th', __("Nom technique"));
    }
    $permissions .= $this->Html->tag('th', __("Nom usuel"));
    $permissions .= $this->Html->tag('th', __("Accès"));
    $permissions .= $this->Html->tag('/tr');
    $permissions .= $this->Html->tag('/thead');
    $permissions .= $this->Html->tag('tbody');

    $trads = [];
    foreach (array_keys($ctrlrs) as $controller) {
        $trads[$controller] = $this->Translate->permission($controller);
    }
    asort($trads);

    foreach ($trads as $controller => $trad) {
        $actions = $ctrlrs[$controller];
        uksort(
            $actions,
            function ($a, $b) {
                $a = str_starts_with($a, 'api.') ? -1 : $a;
                $b = str_starts_with($b, 'api.') ? -1 : $b;
                return $a > $b;
            }
        );
        if ($technical) {
            $permissions .= $this->Html->tag(
                'tr',
                $this->Html->tag('th', $controller)
                . $this->Html->tag('th', $trad !== $controller ? $trad : '')
                . $this->Html->tag('td', ''),
                ['id' => $prefix . mb_strtolower(Text::slug($group . '-' . $controller, '-'))]
            );
        } else {
            $permissions .= $this->Html->tag(
                'tr',
                $this->Html->tag('th', $trad)
                . $this->Html->tag('td', ''),
                ['id' => $prefix . mb_strtolower(Text::slug($group . '-' . $controller, '-'))]
            );
        }
        foreach ($actions as $action => $params) {
            $permissions .= $this->Html->tag('tr');

            $trad = $this->Translate->permission($controller, $action);
            if ($technical) {
                $permissions .= $this->Html->tag('td', $action, ['title' => $controller]);
                $permissions .= $this->Html->tag('td', $trad !== $action ? $trad : '');
            } else {
                $permissions .= $this->Html->tag('td', $trad);
            }
            $path = "controllers.$controller.action.$action";
            $access = Hash::get($accesses, $path);
            $allowed = $access === true || $access === '1';
            $permissions .= $this->Html->tag(
                'td',
                $allowed
                    ? $this->Fa->i('fa-check-circle pull-right text-success no-margin')
                    : $this->Fa->i('fa-times-circle pull-right text-danger no-margin'),
                [
                    'class' => 'h4',
                    'title' => $allowed ? __("Autorisé") : __("Non autorisé"),
                ]
            );

            $permissions .= $this->Html->tag('/tr');
        }
    }
    $permissions .= $this->Html->tag('/tbody');
    $permissions .= $this->Html->tag('/table');
}

$permissions .= $this->Form->end();
$permissions .= $this->Html->tag('/article');

$tabs->add(
    'tab-view-role-permissions',
    $this->Fa->i('fa-ban', __("Permissions")),
    $permissions
);

echo $tabs;
?>
<script>
    function scrollToPermission(element) {
        var group = $(element).val();
        $(element).closest('.modal-body').animate({
            scrollTop: $("#"+group).offset().top - 200
        }, 1000, function() {
            $("#"+group).css('background-color', '#5cb85c')
                .animate({'background-color': 'transparent'});
        });
    }
</script>
