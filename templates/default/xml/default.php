<?php

/**
 * @var Versae\View\AppView $this
 */

$flash = $this->Flash->render();

use AsalaeCore\Utility\XmlUtility;

echo XmlUtility::arrayToXmlString(['ViewVars' => $viewVars + ($flash ? ['flash' => $flash] : [])]);
