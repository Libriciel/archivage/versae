<?php

/**
 * @var Versae\View\AppView $this
 */

$flash = $this->Flash->render();

echo json_encode($viewVars + ($flash ? ['flash' => $flash] : []), JSON_PRETTY_PRINT);
