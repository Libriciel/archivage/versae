<?php

/**
 * @var Versae\View\AppView $this
 */

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Administration"));
$this->Breadcrumbs->add(__("Compteurs"));

echo $this->Html->tag(
    'div.container',
    $this->Html->tag('h1', $this->Fa->i('fa-tachometer', __("Compteurs")))
    . $this->Breadcrumbs->render()
);

echo $this->element('modal', ['idTable' => $tableId]);

$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'Counters.name' => [
                'label' => __("Nom"),
                'order' => 'name',
                'filter' => [
                    'name[0]' => [
                        'id' => 'filter-name-0',
                        'label' => false,
                        'aria-label' => __("Nom"),
                    ],
                ],
            ],
            'Counters.identifier' => [
                'label' => __("Identifiant"),
                'order' => 'identifier',
                'filter' => [
                    'identifier[0]' => [
                        'id' => 'filter-identifier-0',
                        'label' => false,
                        'aria-label' => __("Identifiant"),
                    ],
                ],
            ],
            'Counters.definition_mask' => [
                'label' => __("Définition")
            ],
            'Counters.created' => [
                'label'   => __("Date de création"),
                'type'    => 'datetime',
                'display' => false,
                'order'   => 'created',
            ],
            'Counters.modified' => [
                'label'   => __("Date de modification"),
                'type'    => 'datetime',
                'display' => false,
                'order'   => 'modified',
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'Counters.id',
            'favorites' => true,
            'sortable' => true,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionViewCounter({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => __("Visualiser {0}", '{1}'),
                'aria-label' => __("Visualiser {0}", '{1}'),
                'display' => $this->Acl->check('counters/view'),
                'params' => ['Counters.id', 'Counters.name']
            ],
            [
                'onclick' => "actionEditCounter({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'display' => $this->Acl->check('counters/edit'),
                'params' => ['Counters.id', 'Counters.name']
            ],
        ]
    );

echo $this->ModalForm
    ->create('edit-counter')
    ->modal(__("Modifier un compteur"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $table->tableObject . ', "Counters")')
    ->output(
        'function',
        'actionEditCounter',
        '/Counters/edit'
    )
    ->generate();

echo $this->ModalView
    ->create('view-Counter')
    ->modal(__("Visualiser un compteur"))
    ->output(
        'function',
        'actionViewCounter',
        '/Counters/view'
    )
    ->generate();

if (empty($data) && $this->Acl->check('/Counters/initCounters')) {
    echo $this->Html->tag(
        'div',
        $this->Html->tag(
            'button',
            $this->Fa->charte('Ajouter', __("Initialiser les compteurs pour le service d'archives")),
            ['class' => 'btn btn-success', 'type' => 'button', 'id' => 'initializeCountersBtn']
        ),
        ['class' => 'container']
    );
    echo $this->Html->tag(
        'script',
        "$('#initializeCountersBtn').click(function() {
            $(this).disable();
            $.ajax({
                url: '" . $this->Url->build('/Counters/initCounters') . "',
                success: function() {
                    $('#initializeCountersBtn').parent().remove();
                },
                error: function() {
                    $('#initializeCountersBtn')
                        .removeClass('btn-success')
                        .addClass('btn-danger')
                        .enable();
                }
            });
        });"
    );
}

echo $this->Filter->create('counters-filter')
    ->saves($savedFilters)
    ->filter(
        'name',
        [
            'label' => __("Nom"),
            'wildcard',
        ]
    )
    ->filter(
        'identifier',
        [
            'label' => __("Identifiant"),
            'wildcard',
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked'
        ]
    )
    ->generateSection();
?>

<section class="container bg-white">
    <header>
        <h2 class="h4"><?=__("Liste des compteurs")?></h2>
        <div class="l-actions h4">
            <?=$this->Fa->charteBtn(
                'Filtrer',
                __("Réinitialiser les filtres de recherche"),
                [
                    'class' => 'btn-link display-active-filter',
                    'text' => $this->Fa->i('fa-times fa-small-append fa-hidden-append text-danger')
                ]
            )
            . $this->Fa->button(
                'fa-sort-amount-asc',
                __("Retirer le tri"),
                [
                    'class' => 'btn-link display-active-sort',
                    'text' => $this->Fa->i('fa-times fa-small-append fa-hidden-append text-danger')
                ]
            )?>
        </div>
        <div class="r-actions h4">
            <?=$table->getConfigureLink()?>
        </div>
    </header>
    <?=$table->generate()?>
</section>
<?php

if (!empty($downloadCsv)) {
    echo $this->Html->tag('section.container.text-right');
    echo $this->Html->tag(
        'a.btn.btn-primary',
        $this->Fa->charte('Télécharger', __("Télécharger au format CSV")),
        [
            'download' => $downloadCsv['download'],
            'href' => $downloadCsv['href'],
            'target' => '_blank',
        ]
    );
    echo $this->Html->tag('/section');
}
