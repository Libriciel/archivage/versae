<?php

/**
 * @var Versae\View\AppView $this
 */

$maskOptions = [
    '#AAAA#' => __("année sur 4 chiffres (ex : 2012)"),
    '#AA#' => __("année sur 2 chiffres (ex : 12)"),
    '#M#' => __("mois au format numérique, sans zéro initial (ex : 1)"),
    '#MM#' => __("mois au format numérique, avec zéro initial (ex: 01)"),
    '#J#' => __("jour du mois sans zéro initial (ex: 2)"),
    '#JJ#' => __("jour du mois avec zéro initial (ex: 02)"),
    '#s#' => __("numéro de la séquence"),
    '#ArchivalAgencyArchiveIdentifier#' => __("identifiant de l'archive (ArchivalAgencyArchiveIdentifier)"),
    'S' => __("Derniers chiffres de la séquence préfixé par le caractère '_'"),
    '0' => __("Derniers chiffres de la séquence avec zéros significatifs"),
];
$resetOptions = [
    '#AAAA#' => __("année"),
    '#MM#' => __("mois"),
    '#JJ#' => __("jour"),
];

echo $this->Form->create($entity, ['idPrefix' => 'edit-counter']);
echo $this->Form->control('identifier', ['label' => __("Identifiant"), 'disabled' => true]);
echo $this->Form->control('name', ['label' => __("Nom du compteur")]);
echo $this->Form->control('description', ['label' => __("Description")]);
echo $this->Html->tag(
    'div',
    '<b>' . __("Valeur de la séquence") . ':</b> '
    . $this->Html->tag(
        'span',
        $sequencesValues[$entity->get('sequence_id')],
        ['id' => 'span-sequence']
    )
);
echo '<br>';
echo $this->Form->control('definition_mask', ['label' => __("Définition du compteur")]);
echo $this->Form->control(
    'definition_mask_helper',
    [
        'label' => __("Ajouter un élément variable au compteur"),
        'options' => $maskOptions,
        'empty' => __("-- Ajouter un élément au compteur --")
    ]
);
echo $this->Form->control(
    'sequence_reset_mask',
    [
        'label' => __("Critère de réinitialisation de la séquence"),
        'options' => $resetOptions,
        'empty' => __("-- Sélectionner une valeur --"),
    ]
);
echo '<hr>';
echo $this->Html->tag('span', __("Exemples"), ['class' => 'fake-label']);
echo $this->Html->tag('div', '', ['id' => 'exemples-counters']);
echo $this->Form->end();
?>
<script>
    var sequences = <?=json_encode($sequencesValues)?>;
    var defmask = $('#edit-counter-definition-mask');
    var maskhelper = $('#edit-counter-definition-mask-helper');
    var cursorPos = defmask.val().length;
    // insertion dans la derniere position du curseur dans le compteur
    defmask.on('blur', function(e) {
        cursorPos = e.target.selectionStart;
    }).closest('form')
        .find('input, select, textarea')
        .not(defmask)
        .not(maskhelper)
        .on('click change keyup', function(e) {
            cursorPos = defmask.val().length;
        });
    $('#edit-counter-sequence-id').change(function() {
        $('#span-sequence').html(sequences[$(this).val()]);
    });
    maskhelper.change(function() {
        var value = $(this).val(),
            target = defmask,
            targetVal = target.val();
        if (value[0] === '#') {
            target.val(targetVal.substring(0, cursorPos) + value + targetVal.substr(cursorPos));
        } else {
            var nb = prompt(__("Veuillez saisir le nombre de chiffres du numéro de la séquence (entre 1 et 10)"));
            if (!nb || !AsalaeGlobal.is_numeric(nb) || parseInt(nb, 10) < 1 || parseInt(nb, 10) > 10) {
                alert(__("Vous n'avez pas saisi une valeur correcte"));
            } else {
                target.val(targetVal.substring(0, cursorPos) + '#'+value.repeat(nb)+'#' + targetVal.substr(cursorPos));
            }
        }
        buildExemples();
        $(this).val('');
    });
    defmask.change(function() {
        buildExemples();
    });
    function buildExemples() {
        var exemples = [],
            target = defmask,
            value = target.val(),
            d = new Date(),
            year = ''+d.getFullYear(),
            month = d.getMonth() + 1,
            day = d.getDate(),
            str,
            values = [1, 15, 203, 4096],
            posBegin,
            posEnd,
            prefixed;
        for (var i in values) {
            str = value;
            str = str.replace('#s#', values[i]);
            str = str.replace('#AAAA#', year);
            str = str.replace('#AA#', year.substring(2));
            str = str.replace('#M#', month);
            str = str.replace('#MM#', ''+('0'+month).substring((''+month).length -1));
            str = str.replace('#J#', day);
            str = str.replace('#JJ#', ('0'+day).substring((''+day).length -1));
            posBegin = 0;
            do {
                posBegin = str.indexOf('#S', posBegin);
                if (posBegin >= 0) {
                    posEnd = str.indexOf('S#', posBegin);
                    prefixed = '_'.repeat(posEnd - posBegin).substr((''+values[i]).length)+values[i];
                    str = str.substr(0, posBegin) + prefixed + str.substr(posEnd+2);
                    posBegin++;
                }
            } while (posBegin >= 0);
            posBegin = 0;
            do {
                posBegin = str.indexOf('#0', posBegin);
                if (posBegin >= 0) {
                    posEnd = str.indexOf('0#', posBegin);
                    prefixed = '0'.repeat(posEnd - posBegin).substr((''+values[i]).length)+values[i];
                    str = str.substr(0, posBegin) + prefixed + str.substr(posEnd+2);
                    posBegin++;
                }
            } while (posBegin >= 0);
            exemples.push(AsalaeGlobal.h(str));
        }
        $('#exemples-counters').html('<div>'+exemples.join('</div><div>')+'</div>');
    }
    buildExemples();
</script>
