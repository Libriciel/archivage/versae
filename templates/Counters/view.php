<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->Html->tag('article');
echo $this->Html->tag('header.bottom-space');
echo $this->Html->tag('h3', h($entity->get('name')));
echo $this->Html->tag('p', h($entity->get('description')));
echo $this->Html->tag('/header');

echo $this->Html->tag(
    'table',
    $this->Html->tag(
        'tbody',
        $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Identifiant"))
            . $this->Html->tag('td', h($entity->get('identifier')))
        )
        . $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Définition"))
            . $this->Html->tag('td', h($entity->get('definition_mask')))
        )
        . $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Critère de réinitialisation de la séquence"))
            . $this->Html->tag('td', h($entity->get('sequence_reset_mask')))
        )
        . $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Valeur actuelle de réinitialisation"))
            . $this->Html->tag('td', h($entity->get('sequence_reset_value')))
        )
        . $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Activé"))
            . $this->Html->tag('td', $entity->get('active') ? __("Oui") : __("Non"))
        )
        . $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Date de création"))
            . $this->Html->tag('td', h($entity->get('created')))
        )
        . $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Date de modification"))
            . $this->Html->tag('td', h($entity->get('modified')))
        )
        . $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Utilise la séquence"))
            . $this->Html->tag('td', h($entity->get('sequence')->get('name')))
        )
        . $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Valeur de la séquence"))
            . $this->Html->tag('td', h($entity->get('sequence')->get('value')))
        )
    ),
    ['class' => 'table table-striped table-hover fixed-left-th']
);
echo $this->Html->tag('/article');
