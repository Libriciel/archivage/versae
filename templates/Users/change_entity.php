<?php

/**
 * @var Versae\View\AppView $this
 * @var Versae\Model\Entity\User $user
 */

use Cake\Datasource\EntityInterface;

echo $this->ViewTable->generate(
    $user,
    [
        __("Nom d'utilisateur") => 'name',
        __("Email") => 'email',
        __("Rôle") => 'role.name',
        __("Identifiant") => 'username',
        __("Date de création") => 'created',
        __("Entité actuelle") => 'org_entity.name',
        __("Versements en préparation") => function (EntityInterface $v) {
            return $this->Html->tag(
                'div',
                count($v->get('deposits')),
                [
                    'title' => __("Sera supprimé lors du changement d'entité")
                ]
            );
        },
    ]
);

echo $this->Form->create($form, ['autocomplete' => 'off', 'idPrefix' => 'change-entity-user']);

echo $this->Form->control(
    'archival_agency_id',
    [
        'label' => __("Service d'archives"),
        'options' => $archivalAgencies,
    ]
);

echo $this->Html->tag('div#change-entity-user-div-same-archival-agency');
echo $this->Form->control(
    'org_entity_id',
    [
        'label' => __("Entité cible"),
        'options' => $orgEntities,
        'empty' => __("-- Sélectionner une nouvelle entité --"),
        'required' => true,
    ]
);
echo $this->Form->control(
    'role_id',
    [
        'label' => __("Rôles de l'utilisateur"),
        'options' => $roles,
        'empty' => __("-- Sélectionner un nouveau rôle --"),
        'required' => true,
    ]
);
echo $this->Html->tag('/div'); // #change-entity-user-div-same-archival-agency

echo $this->Html->tag('div#change-entity-user-div-other-archival-agency');
echo $this->Form->control(
    'reason',
    [
        'label' => __("Motif"),
    ]
);
echo $this->Form->control(
    'disable_user',
    [
        'label' => __("Désactiver immédiatement l'utilisateur"),
    ]
);
echo $this->Html->tag('/div'); // #change-entity-user-div-other-archival-agency

echo $this->Form->end();
?>
<script>
    var entitiesRoles = <?=json_encode($availablesRolesByEntities)?>;
    var selectArchivalAgency = $('#change-entity-user-archival-agency-id');
    var selectOrgEntity = $('#change-entity-user-org-entity-id');
    var divOrgEntity =$('#change-entity-user-div-same-archival-agency');
    var divReason =$('#change-entity-user-div-other-archival-agency');
    var changeUserRole = $('#change-entity-user-role-id');

    // sauvegarder les valeurs choisies si on change de SA,
    // sans pour autant les envoyer dans le form
    // (bug si on sélectionne une entité du sa courant puis change pour un SA tier)
    var userOrgEntityChoosen;
    var userRoleChoosen;

    selectArchivalAgency.on('change', function() {
        var val = parseInt($(this).val(), 10);
        if (PHP.sa_id === val) {
            divOrgEntity.show();
            divReason.hide();
            selectOrgEntity.prop('required', true);
            selectOrgEntity.val(userOrgEntityChoosen);
            changeUserRole.prop('required', true);
            changeUserRole.val(userRoleChoosen);
        } else {
            divOrgEntity.hide();
            divReason.show();
            selectOrgEntity.prop('required', false);
            userOrgEntityChoosen = selectOrgEntity.val();
            selectOrgEntity.val(null);
            changeUserRole.prop('required', false);
            userRoleChoosen = changeUserRole.val();
            changeUserRole.val(null);
        }
    }).trigger('change');

    selectOrgEntity.on('change', function() {
        var value = selectOrgEntity.val();
        if (value) {
            var enabled = entitiesRoles[value];
            changeUserRole.find('option').each(function() {
                var val = parseInt($(this).attr('value'), 10);
                if (!val || enabled.indexOf(val) > -1) {
                    $(this).enable().attr('title', '');
                } else {
                    $(this).disable()
                        .attr('title', __("Ce rôle n'est pas disponible pour le type d'entité sélectionné"));
                }
            });
            changeUserRole.val('');
        }
        changeUserRole.select2();
    });

    AsalaeGlobal.select2(changeUserRole);
</script>
