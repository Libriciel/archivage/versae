<?php

/**
 * @var Versae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */

echo $this->ViewTable->generate(
    $user,
    [
        __("Nom d'utilisateur") => 'name',
        __("Email") => 'email',
        __("Rôle") => 'role.name',
        __("Identifiant") => 'username',
        __("Date de création") => 'created',
        __("Entité actuelle") => 'org_entity.name',
    ]
);

echo $this->ViewTable->generate(
    $entity,
    [
        __("Service d'archives d'origine") => 'base_archival_agency.name',
        __("Motif du changement de service") => 'reason',
        __("Date de la demande") => 'created',
    ]
);

echo $this->Form->create($form, ['idPrefix' => 'accept-entity-user']);

echo $this->Form->control(
    'org_entity_id',
    [
        'label' => __("Entité cible"),
        'options' => $orgEntities,
        'empty' => __("-- Sélectionner une nouvelle entité --"),
        'required' => true,
    ]
);
echo $this->Form->control(
    'role_id',
    [
        'label' => __("Rôles de l'utilisateur"),
        'options' => $roles,
        'empty' => __("-- Sélectionner un nouveau rôle --"),
        'required' => true,
    ]
);

echo $this->Form->end();
?>
<script>
    var entitiesRoles = <?=json_encode($availablesRolesByEntities)?>;
    var acceptTargetEntity = $('#accept-entity-user-org-entity-id');
    var acceptUserRole = $('#accept-entity-user-role-id');

    acceptTargetEntity.on('change', function() {
        var value = acceptTargetEntity.val();
        if (value) {
            var enabled = entitiesRoles[value];
            acceptUserRole.find('option').each(function() {
                var val = parseInt($(this).attr('value'), 10);
                if (!val || enabled.indexOf(val) > -1) {
                    $(this).enable();
                } else {
                    $(this).disable();
                }
            });
            acceptUserRole.val('');
        }
    });
</script>
