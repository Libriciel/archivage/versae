<?php

/**
 * @var Versae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */

$loading = $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);

echo $this->Html->tag('article');
echo $this->Html->tag('header.bottom-space');
echo $this->Html->tag('h3', h($entity->get('name')));
echo $this->Html->tag('/header');

echo $this->Html->tag('h4', __("Utilisateur"));
echo $this->ViewTable->generate(
    $entity,
    [
        __("Nom d'utilisateur") => 'name',
        __("Email") => 'email',
        __("Rôle") => 'role.name',
        __("Actif") => 'active',
        __("Identifiant de connexion") => 'username',
        __("Date de création") => 'created',
        __("Date de modification") => 'modified',
        __("Lié à un LDAP") => 'is_linked_to_ldap',
        __("Nom du LDAP") => 'ldap.name',
    ]
);
echo $this->Html->tag('/article');
