<?php

/**
 * @var Versae\View\AppView $this
 * @var Versae\Model\Entity\User $user
 */

echo $this->Form->create($user, ['autocomplete' => 'new-password', 'idPrefix' => 'admin-edit-user']);
$labelPassword = __("Password (laisser vide pour ne rien changer)");
$idJauge = 'admin-edit-user-jauge-password';
$displayRole = true;
$displayPassword = false;

echo $this->Form->control('org_entity_id', ['type' => 'hidden']);

echo $this->Form->control(
    'orgentity',
    [
        'label' => __("Entité"),
        'options' => [$user->get('org_entity')->get('id') => $user->get('org_entity')->get('name')],
        'readonly',
    ]
);

require 'addedit-common.php';
echo $this->Form->end();
?>
<script>
    UserCommon.inputPassword = $('#admin-edit-user-password');
    UserCommon.inputConfirm = $('#admin-edit-user-confirm-password');
    UserCommon.progressBar
        = $('#admin-edit-user-jauge-password').append(UserCommon.getJauge()).find('.progress .progress-bar');
    UserCommon.inputPassword.on('change keyup', UserCommon.passwordKeyup);
    UserCommon.inputPassword.change(UserCommon.inputPasswordChange);
    UserCommon.inputConfirm.on('change keyup', UserCommon.inputConfirmKeyup);
</script>
