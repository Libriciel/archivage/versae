<?php

/**
 * @var Versae\View\AppView $this
 */

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Administration"));
$this->Breadcrumbs->add(__("Utilisateurs"));

// Titre de la page
echo $this->Html->tag(
    'div.container',
    $this->Html->tag('h1', $this->Fa->i('fa-users', __("Utilisateurs")))
    . $this->Breadcrumbs->render()
);
$jsTable = $this->Table->getJsTableObject($tableId);

$buttons = [];
if ($this->Acl->check('/users/add')) {
    $buttons[] = $this->ModalForm
        ->create('add-user-modal')
        ->modal(__("Ajouter un utilisateur"))
        ->javascriptCallback('TableGenericAction.afterAdd(' . $jsTable . ', "Users")')
        ->output(
            'button',
            $this->Fa->charte('Ajouter', __("Ajouter un utilisateur")),
            '/Users/add'
        )
        ->generate(['class' => 'btn btn-success float-none', 'type' => 'button']);
}
if ($this->Acl->check('/ldaps/import-users')) {
    $buttons[] = $this->Form->control(
        'import-users-ldap',
        [
            'label' => __("Importer des utilisateurs LDAP"),
            'options' => $ldaps,
            'empty' => __("-- Sélectionner un annuaire LDAP --"),
            'onchange' => 'importUserLink(this)'
        ]
    );
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}

echo $this->element('modal', ['idTable' => $tableId]);

echo $this->ModalForm
    ->create('index-user-edit')
    ->modal(__("Edition d'un utilisateur"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTable . ', "Users")')
    ->output('function', 'actionEditUser', '/Users/edit-by-admin')
    ->generate();

echo $this->ModalForm
    ->create('change-entity-user')
    ->modal(__("Changement d'entité de l'utilisateur"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTable . ', "Users")')
    ->output('function', 'changeUserEntity', '/users/change-entity')
    ->generate();

echo $this->ModalForm
    ->create('import-entity-user')
    ->modal(__("Changement d'entité de l'utilisateur"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTable . ', "Users")')
    ->output('function', 'importUser', '/users/accept-user')
    ->generate();

echo $this->ModalView
    ->create('view-user')
    ->modal(__("Visualiser un utilisateur"))
    ->output(
        'function',
        'loadViewUser',
        '/users/view'
    )
    ->generate();

echo $this->Filter->create('index-users-filter')
    ->saves($savedFilters)
    ->filter(
        'username',
        [
            'label' => __("Identifiant de connexion"),
            'wildcard',
        ]
    )
    ->filter(
        'email',
        [
            'label' => __("E-mail"),
            'wildcard',
            'type' => 'text',
        ]
    )
    ->filter(
        'name',
        [
            'label' => __("Nom d'utilisateur"),
            'wildcard',
        ]
    )
    ->filter(
        'active',
        [
            'label' => __("Actifs"),
            'options' => [
                '1' => __("Oui"),
                '0' => __("Non"),
            ]
        ]
    )
    ->filter(
        'role_id',
        [
            'label' => __("Rôle de l'utilisateur"),
            'type' => 'select',
            'multiple' => true,
            'options' => $roles
        ]
    )
    ->filter(
        'org_entity_id',
        [
            'label' => __("Entité"),
            'type' => 'select',
            'multiple' => true,
            'options' => $org_entities
        ]
    )
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date'
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked'
        ]
    )
    ->filter(
        'is_linked_to_ldap',
        [
            'label' => __("Lié à un LDAP"),
            'options' => [
                '1' => __("Oui"),
                '0' => __("Non"),
            ],
        ]
    )
    ->filter(
        'ldap_id',
        [
            'label' => __("Nom LDAP"),
            'options' => $ldaps,
        ]
    )
    ->generateSection();

require 'ajax_index.php';
?>
<script>
    $(function() {
        AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');
    });

    function importUserLink(element) {
        var baseUrl = '<?=$this->Url->build('/ldaps/import-users')?>';
        AsalaeGlobal.interceptedLinkToAjax(baseUrl+'/'+$(element).val());
    }

    function refuseUser(id) {
        var table = {};
        table = <?=$jsTable?>;
        var tr = $(table.table).find('tr[data-id="'+id+'"]');
        $.ajax({
            url: '<?=$this->Url->build('/users/refuse-user')?>/' + id,
            method: 'DELETE',
            success: function (message) {
                if (message.report === 'done') {
                    table.removeDataId(id);
                    tr.fadeOut(400, function() {
                        $(this).remove();
                        table.decrementPaginator();
                    });
                } else {
                    throw message.report;
                }
            },
            error: function (e, type, code) {
                var message = "<?=__("Une erreur a eu lieu lors du refus")?>";
                if (code === 'Forbidden') {
                    message = "<?=__("Vous n'avez pas la permission d'effectuer cette action")?>";
                }
                tr.addClass('danger')
                    .find('td.statetrad')
                    .text(message);
            }
        });
    }
</script>
