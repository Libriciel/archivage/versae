<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->Form->create($entity ?? null, ['idPrefix' => 'logged-out']);
if (!empty($entity)) {
    echo $this->Form->control('prev_username', ['type' => 'hidden'])
        . $this->Form->control('prev_archival_agency_id', ['type' => 'hidden'])
        . $this->Form->control('username', ['label' => __("Identifiant de connexion")])
        . $this->Form->control('password', ['label' => __("Mot de passe"), 'type' => 'password']);
}
echo $this->Form->end();
?>
<script>
    AsalaeSessionTimeout.checkingConnection = true; // Pour ignorer l'evenement "ajax_complete"
    $('#logged-out-prev-username').val(PHP?.username);
    $('#logged-out-prev-archival-agency-id').val(PHP?.archival_agency_id);
</script>
