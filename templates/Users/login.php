
<form method="post" class="form-horizontal">
    <!-- Text input-->
    <div class="form-group row">
        <label class="col-md-4 control-label login-label" for="textinput"><?=__("Identifiant")?></label>
        <div class="col-md-8">
            <input id="textinput" name="login" placeholder="<?=__("Identifiant de connexion")?>" class="form-control input-md">
        </div>
    </div>

    <!-- Password input-->
    <div class="form-group row">
        <label class="col-md-4 control-label login-label" for="passwordinput"><?=__("Mot de passe")?></label>
        <div class="col-md-8">
            <input id="passwordinput" name="password" placeholder="********" class="form-control input-md" type="password">
        </div>
    </div>

    <!-- Button -->
    <div class="form-group text-right login-btn-group row">
        <div class="col-md-12">
            <button type="submit" id="singlebutton" class="btn btn-primary"><?=__("Se connecter")?></button>
        </div>
    </div>
</form>
<script>
    var form = document.getElementsByTagName('form')[0];
    var callback = function() {
        var buttons = document.getElementsByTagName('button');
        for (var i = 0; i < buttons.length; i++) {
            buttons[i].disabled = true;
        }
    }

    if(form.addEventListener){
        form.addEventListener("submit", callback, false);  //Modern browsers
    }else if(form.attachEvent){
        form.attachEvent('onsubmit', callback);            //Old IE
    }
</script>
