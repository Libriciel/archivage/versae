<?php

/**
 * @var Versae\View\AppView $this
 * @var Versae\Model\Entity\User $user
 */

use Cake\Core\Configure;

if ($user->get('ldap_id')) {
    echo $this->Html->tag(
        'div.alert.alert-info',
        __(
            "Ce compte étant importé depuis un annuaire LDAP/AD,"
            . " il n'est pas possible d'en modifier les informations utilisateur."
        )
    );
}

if ($displayRole && !empty($roles)) {
    echo $this->Form->control(
        'role_id',
        [
            'label' => __("Rôle"),
            'required' => true,
            'options' => $roles,
            'empty' => isset($lockedRole) ? false : __("-- Veuillez sélectionner un rôle --"),
        ]
    );
}
echo $this->Form->control(
    'username',
    [
        'label' => __("Identifiant de connexion"),
        'required' => true
    ] + (
        $user->get('ldap_id')
            ? ['disabled' => true]
            : []
    )
)
    . '<label class="hide">autocomplete obliterator'
    . '<input type="password" value="false">' . '<!-- neutralise le préremplissage firefox --></label>';

if (!$user->get('ldap_id') && $displayPassword) {
    echo $this->Form->control(
        'password',
        [
            'label' => $labelPassword ?? __("Password"),
            'type' => 'password',
            'append' => '<span class="fa fa-circle-o fa-lg">',
            'escape' => false,
            'required' => false,
            'templates' => [
                'label' => '<label{{attrs}}>{{text}}</label><div id="' . ($idJauge ?? 'jauge-password') . '"></div>'
            ]
        ]
    )
    . $this->Form->control(
        'confirm-password',
        [
            'label' => __("Confirmer le password"),
            'type' => 'password',
            'append' => '<span class="fa fa-circle-o fa-lg">'
        ]
    );
}
echo $this->Form->control(
    'name',
    [
        'label' => __("Nom d'utilisateur"),
        'placeholder' => __("optionnel")
    ] + (
        $user->get('ldap_id')
            ? ['disabled' => true]
            : []
    )
)
    . $this->Form->control(
        'email',
        [
            'label' => __("Email"),
            'type' => 'email',
        ] + (
        $user->get('ldap_id')
            ? ['disabled' => true]
            : []
        )
    )
    . $this->Form->control(
        'high_contrast',
        [
            'label' => [
                'text' => $this->Fa->i('fa-blind') . $this->Fa->i('fa-wheelchair')
                . __("Utiliser un contraste élevé ?"),
                'title' => __("Modifie le visuel pour faciliter la lecture aux mal-voyants")
            ],
            'escape' => false
        ]
    );

if ($this->getRequest()->getParam('action') === 'editByAdmin') {
    echo $this->Form->control(
        'active',
        [
            'label' => __("Actif")
        ]
    );
}
?>
<script>
    var UserCommon = {
        inputPassword: '',
        inputConfirm: '',
        progressBar: ''
    };
    var minScore = <?=Configure::read('Password.complexity')?>;

    UserCommon.getJauge = function() {
        return $('<div></div>')
            .addClass('progress')
            .height('5px')
            .css({'margin-bottom': '5px'})
            .append(
                $('<div></div>')
                    .addClass('progress-bar')
                    .attr('role', 'progressbar')
                    .attr('aria-valuenow', 0)
                    .attr('aria-valuemin', 0)
                    .attr('aria-valuemax', <?=PASSWORD_STRONG?>)
            );
    }
    UserCommon.passwordKeyup = function() {
        var score = AsalaeGlobal.passwordScore($(this).val());
        UserCommon.progressBar.removeClass('progress-bar-striped');

        if (!$(this).val()) {
            $(this).parent().find('span.fa').removeClass('fa-check-circle text-success')
                .removeClass('fa-times-circle text-danger')
                .addClass('fa-circle-o')
                .attr('title', '');
        } else {
            if (score > minScore) {
                $(this).parent().find('span.fa').removeClass('fa-circle-o')
                    .removeClass('fa-times-circle text-danger')
                    .addClass('fa-check-circle text-success')
                    .attr('title', __("Valide"));
            } else {
                $(this).parent().find('span.fa').removeClass('fa-circle-o')
                    .removeClass('fa-check-circle text-success')
                    .addClass('fa-times-circle text-danger')
                    .attr('title', __("Pas assez complexe"));

            }
        }
        if (score < <?=PASSWORD_ULTRA_WEAK?>) {
            UserCommon.progressBar
                .removeClass('progress-bar-warning')
                .removeClass('progress-bar-info')
                .removeClass('progress-bar-success')
                .addClass('progress-bar-danger');
        } else if (score < <?=PASSWORD_WEAK?>) {
            UserCommon.progressBar
                .removeClass('progress-bar-danger')
                .removeClass('progress-bar-info')
                .removeClass('progress-bar-success')
                .addClass('progress-bar-warning');
        } else if (score < <?=PASSWORD_MEDIUM?>) {
            UserCommon.progressBar
                .removeClass('progress-bar-danger')
                .removeClass('progress-bar-warning')
                .removeClass('progress-bar-success')
                .addClass('progress-bar-info');
        } else {
            UserCommon.progressBar
                .removeClass('progress-bar-danger')
                .removeClass('progress-bar-warning')
                .removeClass('progress-bar-info')
                .addClass('progress-bar-success');
        }
        if (score >= <?=PASSWORD_STRONG?>) {
            UserCommon.progressBar
                .addClass('progress-bar-striped');
            score = <?=PASSWORD_STRONG?>;
        }
        UserCommon.progressBar
            .attr('aria-valuenow', score)
            .width(score / <?=PASSWORD_STRONG?> * 100 + '%');
    }
    UserCommon.inputPasswordChange = function() {
        UserCommon.inputConfirm.val('').parent().find('span.fa')
            .addClass('fa-circle-o')
            .removeClass('fa-check-circle text-success')
            .removeClass('fa-times-circle text-danger')
            .attr('title', '');
    }
    UserCommon.inputConfirmKeyup = function() {
        if (!$(this).val()) {
            $(this).parent().find('span.fa').addClass('fa-circle-o')
                .removeClass('fa-check-circle text-success')
                .removeClass('fa-times-circle text-danger')
                .attr('title', '');
        } else if ($(this).val() === UserCommon.inputPassword.val()) {
            $(this).parent().find('span.fa').removeClass('fa-circle-o')
                .removeClass('fa-times-circle text-danger')
                .addClass('fa-check-circle text-success')
                .attr('title', __("Identique au mot de passe"));
        } else {
            $(this).parent().find('span.fa').removeClass('fa-circle-o')
                .removeClass('fa-check-circle text-success')
                .addClass('fa-times-circle text-danger')
                .attr('title', __("Différent du mot de passe"));
        }
    };
</script>
