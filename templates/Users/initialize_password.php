<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Core\Configure;

if (!empty($notAuthorized)) {
    echo $this->Html->tag('div.container');
    echo $this->Html->tag(
        'div.alert.alert-info',
        __(
            "La page demandée n'est pas/plus valide. La durée d'accès "
            . "peut être dépassée ou le traitement déjà effectué."
        )
    );
    echo $this->Html->tag('/div');
    return;
}

$username = Configure::read('Libriciel.login.input.username', 'username');
$password = Configure::read('Libriciel.login.input.password', 'password');

?><div class="container text-center">
    <h1 style="display: inline-block"><i aria-hidden="true" class="fa fa-id-card-o"></i> <?=__(
        "Activation de votre compte sur versae"
    )?></h1>
</div>

<section class="container bg-white" style="max-width: 660px">
    <h2 class="h4"><?=__("Veuillez choisir un mot de passe")?></h2>
    <p>
        <?=__("Afin d'activer votre compte utilisateur, vous devez choisir un mot de passe.")?><br>
        <?=__("Votre identifiant est")?> "<b><?=h($user->get($username))?></b>"
    </p>
    <?php
        echo $this->Form->create($user)
            . '<label class="hide">autocomplete obliterator'
            . '<input type="password" value="false"><!-- neutralise le préremplissage firefox --></label>'
            . $this->Form->control(
                'code',
                [
                    'type' => 'hidden',
                    'val' => $code,
                ]
            )
            . $this->Form->control(
                $password,
                [
                    'label' => __("Mot de passe"),
                    'type' => 'password',
                    'append' => '<button type="button" tabindex="1" id="password-field-eye"'
                        . ' class="fa fa-fw fa-eye-slash field-icon toggle-password btn-link double-append">'
                        . '</button><span class="fa fa-circle-o fa-lg">',
                    'escape' => false,
                    'required' => false,
                    'value' => '',
                    'val' => '',
                    'templates' => [
                        'label' => '<label{{attrs}}>{{text}}</label><div id="jauge-password"></div>'
                    ]
                ]
            )
            . $this->Form->control(
                'confirm-' . $password,
                [
                    'label' => __("Confirmer le mot de passe"),
                    'type' => 'password',
                    'append' => '<span class="fa fa-circle-o fa-lg">'
                ]
            )
            . '<br>'
            . $this->Html->tag(
                'div',
                $this->Form->button(__("Activer mon compte"), ['class' => 'btn-primary']),
                ['class' => 'text-center']
            )
            . $this->Form->end();
        ?>
</section>
<script>
    var UserCommon = {
        inputPassword: '',
        inputConfirm: '',
        progressBar: ''
    };
    var minScore = <?=Configure::read('Password.complexity')?>;

    UserCommon.getJauge = function() {
        return $('<div></div>')
            .addClass('progress')
            .height('5px')
            .css({'margin-bottom': '5px'})
            .append(
                $('<div></div>')
                    .addClass('progress-bar')
                    .attr('role', 'progressbar')
                    .attr('aria-valuenow', 0)
                    .attr('aria-valuemin', 0)
                    .attr('aria-valuemax', <?=PASSWORD_STRONG?>)
            );
    }
    UserCommon.passwordKeyup = function() {
        var score = AsalaeGlobal.passwordScore($(this).val());
        UserCommon.progressBar.removeClass('progress-bar-striped');

        if (!$(this).val()) {
            $(this).parent().find('span.fa').removeClass('fa-check-circle text-success')
                .removeClass('fa-times-circle text-danger')
                .addClass('fa-circle-o')
                .attr('title', '');
        } else {
            if (score > minScore) {
                $(this).parent().find('span.fa').removeClass('fa-circle-o')
                    .removeClass('fa-times-circle text-danger')
                    .addClass('fa-check-circle text-success')
                    .attr('title', __("Valide"));
            } else {
                $(this).parent().find('span.fa').removeClass('fa-circle-o')
                    .removeClass('fa-check-circle text-success')
                    .addClass('fa-times-circle text-danger')
                    .attr('title', __("Pas assez complexe"));

            }
        }
        if (score < <?=PASSWORD_ULTRA_WEAK?>) {
            UserCommon.progressBar
                .removeClass('progress-bar-warning')
                .removeClass('progress-bar-info')
                .removeClass('progress-bar-success')
                .addClass('progress-bar-danger');
        } else if (score < <?=PASSWORD_WEAK?>) {
            UserCommon.progressBar
                .removeClass('progress-bar-danger')
                .removeClass('progress-bar-info')
                .removeClass('progress-bar-success')
                .addClass('progress-bar-warning');
        } else if (score < <?=PASSWORD_MEDIUM?>) {
            UserCommon.progressBar
                .removeClass('progress-bar-danger')
                .removeClass('progress-bar-warning')
                .removeClass('progress-bar-success')
                .addClass('progress-bar-info');
        } else {
            UserCommon.progressBar
                .removeClass('progress-bar-danger')
                .removeClass('progress-bar-warning')
                .removeClass('progress-bar-info')
                .addClass('progress-bar-success');
        }
        if (score >= <?=PASSWORD_STRONG?>) {
            UserCommon.progressBar
                .addClass('progress-bar-striped');
            score = <?=PASSWORD_STRONG?>;
        }
        UserCommon.progressBar
            .attr('aria-valuenow', score)
            .width(score / <?=PASSWORD_STRONG?> * 100 + '%');
    }
    UserCommon.inputPasswordChange = function() {
        UserCommon.inputConfirm.val('').parent().find('span.fa')
            .addClass('fa-circle-o')
            .removeClass('fa-check-circle text-success')
            .removeClass('fa-times-circle text-danger')
            .attr('title', '');
    }
    UserCommon.inputConfirmKeyup = function() {
        if (!$(this).val()) {
            $(this).parent().find('span.fa').addClass('fa-circle-o')
                .removeClass('fa-check-circle text-success')
                .removeClass('fa-times-circle text-danger')
                .attr('title', '');
        } else if ($(this).val() === UserCommon.inputPassword.val()) {
            $(this).parent().find('span.fa').removeClass('fa-circle-o')
                .removeClass('fa-times-circle text-danger')
                .addClass('fa-check-circle text-success')
                .attr('title', __("Identique au mot de passe"));
        } else {
            $(this).parent().find('span.fa').removeClass('fa-circle-o')
                .removeClass('fa-check-circle text-success')
                .addClass('fa-times-circle text-danger')
                .attr('title', __("Différent du mot de passe"));
        }
    };

    UserCommon.inputPassword = $('#password');
    UserCommon.inputConfirm = $('#confirm-password');
    UserCommon.progressBar = $('#jauge-password').append(UserCommon.getJauge()).find('.progress .progress-bar');
    UserCommon.inputPassword.on('change keyup', UserCommon.passwordKeyup);
    UserCommon.inputPassword.change(UserCommon.inputPasswordChange);
    UserCommon.inputConfirm.on('change keyup', UserCommon.inputConfirmKeyup);

    var form = document.getElementsByTagName('form')[0];
    var callback = function() {
        var buttons = document.getElementsByTagName('button');
        for (var i = 0; i < buttons.length; i++) {
            buttons[i].disabled = true;
        }
    }
    var password = document.getElementById('<?=Configure::read(
        'Libriciel.login.input.password',
        'password'
    )?>');
    var eye = document.getElementById('password-field-eye');
    var togglePasswordOn = function(e) {
        if (!e.keyCode || e.keyCode === 32 || e.keyCode === 13) { // click || space || enter
            eye.classList.add('fa-eye');
            eye.classList.remove('fa-eye-slash');
            password.type = 'text';
        }
    };
    var togglePasswordOff = function() {
        eye.classList.remove('fa-eye');
        eye.classList.add('fa-eye-slash');
        password.type = 'password';
    };
    if (form.addEventListener){
        form.addEventListener("submit", callback, false);  //Modern browsers
        eye.addEventListener("mousedown", togglePasswordOn, false);
        eye.addEventListener("keydown", togglePasswordOn, false);
        document.addEventListener("mouseup", togglePasswordOff, false);
        document.addEventListener("keyup", togglePasswordOff, false);
    } else if(form.attachEvent){
        form.attachEvent('onsubmit', callback);            //Old IE
        eye.attachEvent('onmousedown', togglePasswordOn);
        eye.attachEvent('onkeydown', togglePasswordOn);
        document.attachEvent('onmouseup', togglePasswordOff);
        document.attachEvent("onkeyup", togglePasswordOff);
    }
</script>
