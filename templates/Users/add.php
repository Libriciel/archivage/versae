<?php

/**
 * @var Versae\View\AppView $this
 * @var Versae\Model\Entity\User $user
 */

echo $this->Form->create($user, ['autocomplete' => 'off', 'idPrefix' => 'add-user']);
$labelPassword = __("Password (laisser vide pour ne rien changer)");
$idJauge = 'add-user-jauge-password';
$displayRole = true;
$displayPassword = false;

if ($orgEntityId) {
    echo $this->Form->control('org_entity_id', ['type' => 'hidden']);

    echo $this->Form->control(
        'orgentity',
        [
            'label' => __("Entité"),
            'options' => [$user->get('org_entity')->get('id') => $user->get('org_entity')->get('name')],
            'readonly',
        ]
    );
} else {
    echo $this->Form->control(
        'org_entity_id',
        [
            'label' => __("Entité de l'utilisateur"),
            'options' => $entities,
            'empty' => __("-- Veuillez choisir une entité --"),
            'required' => true,
        ]
    );
    echo $this->Form->control(
        'role_id',
        [
            'label' => __("Rôle"),
            'required' => true,
            'options' => $roles ?? [],
            'empty' => __("-- Veuillez sélectionner un rôle --")
        ]
    );
    echo '<hr>';
    $displayRole = false;
}
require 'addedit-common.php';
echo $this->Form->end();
?>
<script>
    UserCommon.inputPassword = $('#add-user-password');
    UserCommon.inputConfirm = $('#add-user-confirm-password');
    UserCommon.progressBar
        = $('#add-user-jauge-password').append(UserCommon.getJauge()).find('.progress .progress-bar');
    UserCommon.inputPassword.on('change keyup', UserCommon.passwordKeyup);
    UserCommon.inputPassword.change(UserCommon.inputPasswordChange);
    UserCommon.inputConfirm.on('change keyup', UserCommon.inputConfirmKeyup);

    $('#add-user-org-entity-id').change(function() {
        var value = $(this).val();
        var selectRoles = $('#add-user-role-id');
        selectRoles.find('option[value!=""]').remove();
        if (!value) {
            return;
        }
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/users/getRolesOptions')?>/'+value,
            headers: {
                Accept: 'application/json'
            },
            success: function (content) {
                for (var i in content) {
                    var option = $('<option></option>').attr('value', i).text(content[i]);
                    selectRoles.append(option);
                }
            },
            error: function() {
                alert(PHP.messages.genericError);
            }
        })
    });
</script>
