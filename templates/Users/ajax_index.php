<?php
/**
 * @var Versae\View\AppView $this
 */
if (empty($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}

$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'Users.username' => [
                'label' => __("Identifiant de connexion"),
                'order' => 'username',
                'filter' => [
                    'username[0]' => [
                        'id' => 'filter-username-0',
                        'label' => false,
                        'aria-label' => __("Utilisateur"),
                    ],
                ],
            ],
            'Users.name' => [
                'label' => __("Nom d'utilisateur"),
                'order' => 'name',
                'filter' => [
                    'name[0]' => [
                        'id' => 'filter-name-0',
                        'label' => false,
                        'aria-label' => __("Nom"),
                    ],
                ],
            ],
            'Users.created' => [
                'label' => __("Date de création"),
                'order' => 'created',
                'type'  => 'datetime',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'created[0]' => [
                        'id' => 'filter-created-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator('dateoperator_created[0]', '>='),
                        'append' => $this->Date->picker('#filter-created-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'created[1]' => [
                        'id' => 'filter-created-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_created[1]', '<='),
                        'append' => $this->Date->picker('#filter-created-1'),
                        'class' => 'datepicker with-select',
                    ],
                ]
            ],
            'Users.email' => [
                'label' => __("E-mail"),
                'order' => 'email',
                'filter' => [
                    'email[0]' => [
                        'id' => 'filter-email-0',
                        'label' => false,
                        'aria-label' => __("E-mail"),
                    ],
                ],
            ],
            'Users.active' => [
                'label' => __("Actif"),
                'type'  => 'boolean',
                'filter' => [
                    'active[0]' => [
                        'id' => 'filter-active-0',
                        'label' => false,
                        'aria-label' => __("Actif"),
                        'options' => [
                            '1' => __("Oui"),
                            '0' => __("Non"),
                        ]
                    ],
                ],
            ],
            'Users.role.name' => [
                'label' => __("Rôle"),
                'order' => 'role',
                'filter' => [
                    'role_id[0]' => [
                        'id' => 'filter-role_id-0',
                        'label' => false,
                        'aria-label' => __("Rôle"),
                        'options' => $roles,
                    ],
                ],
            ],
            'Users.org_entity.name' => [
                'label' => __("Entité"),
                'order' => 'org_entity',
                'filter' => [
                    'org_entity_id[0]' => [
                        'id' => 'filter-org_entity_id-0',
                        'label' => false,
                        'aria-label' => __("Entité"),
                        'options' => $org_entities,
                    ],
                ],
            ],
            'Users.is_linked_to_ldap' => [
                'label' => __("Lié à un LDAP"),
                'type'  => 'boolean',
                'filter' => [
                    'is_linked_to_ldap[0]' => [
                        'id' => 'filter-is_linked_to_ldap-0',
                        'label' => false,
                        'aria-label' => __("Lié à un LDAP"),
                        'options' => [
                            '1' => __("Oui"),
                            '0' => __("Non"),
                        ]
                    ],
                ],
            ],
            'Users.ldap.name' => [
                'label'   => __("Nom LDAP"),
                'display' => false,
                'order' => 'ldap',
                'filter' => [
                    'ldap_id[0]' => [
                        'id' => 'filter-ldap_id-0',
                        'label' => false,
                        'aria-label' => __("Nom LDAP"),
                        'options' => $ldaps,
                    ],
                ],
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'Users.id',
            'favorites' => true,
            'sortable' => true,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "loadViewUser({0})",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => $title = __("Visualiser {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/users/view'),
                'displayEval' => 'data[{index}].same_archival_agency !== false',
                'params' => ['Users.id', 'Users.username']
            ],
            [
                'onclick' => "actionEditUser({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/users/edit-by-admin'),
                'displayEval' => 'data[{index}].agent_type === "person" '
                    . ' && data[{index}].same_archival_agency !== false',
                'params' => ['Users.id', 'Users.username']
            ],
            [
                'type' => "button",
                'class' => "btn-link",
                'data-callback' => sprintf(
                    "TableGenericAction.deleteAction(%s, '%s')({0}, false)",
                    $jsTable,
                    $this->Url->build('/users/delete')
                ),
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'confirm' => __("Êtes-vous sûr de vouloir supprimer cet utilisateur ?"),
                'display' => $this->Acl->check('/users/delete'),
                'displayEval' => 'data[{index}].Users.deletable '
                    . ' && data[{index}].Users.id !== PHP.user_id'
                    . ' && data[{index}].same_archival_agency !== false',
                'params' => ['Users.id', 'Users.username']
            ],
            [
                'onclick' => "changeUserEntity({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-building'),
                'data-action' => __("Changer d'entité"),
                'title' => $title = __("Changer l'entité de l'utilisateur"),
                'aria-label' => $title,
                'display' => $this->Acl->check('/users/change-entity'),
                'displayEval' => 'data[{index}].same_archival_agency !== false',
                'params' => ['Users.id', 'Users.username'],
            ],
            [
                'onclick' => "importUser({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-building text-success'),
                'title' => $title = __("Accepter l'utilisateur"),
                'aria-label' => $title,
                'display' => $this->Acl->check('/users/accept-user'),
                'displayEval' => 'data[{index}].same_archival_agency === false',
                'params' => ['Users.id', 'Users.username'],
            ],
            [
                'data-callback' => "refuseUser({0})",
                'confirm' => __(
                    "L'utilisateur \"{1}\" a changé de Service d'Archives avec "
                    . "le motif : \"{2}\"\nConfirmer son refus ?"
                ),
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-times text-danger'),
                'title' => $title = __("Refuser l'utilisateur"),
                'aria-label' => $title,
                'display' => $this->Acl->check('/users/refuse-user'),
                'displayEval' => 'data[{index}].same_archival_agency === false',
                'params' => ['Users.id', 'Users.username', 'Users.change_entity_request.reason'],
            ],
        ]
    );

echo $this->element(
    'section_table',
    [
        'id' => $tableId . '-section',
        'title' => __("Liste des utilisateurs"),
        'table' => $table,
    ]
);
