<?php

/**
 * @var Versae\View\AppView $this
 */

use Authorization\Identity;
use Cake\Utility\Hash;

echo $this->Html->tag('header');

$separator = $this->Html->tag(
    'div',
    '',
    [
        'role' => 'separator',
        'class' => 'dropdown-divider'
    ]
);

echo $this->ModalForm
    ->create('archival-agency-conf', ['size' => 'modal-xxl'])
    ->modal(__("Configuration du service d'archives"))
    ->output('function', 'editArchivalAgencyConfig', '/org-entities/edit-config')
    ->generate();

echo $this->Modal->create(
    [
        'id' => 'modal-rgpd',
        'class' => 'ui-draggable-handle',
    ]
)
. $this->Modal->header(
    __("Politique de confidentialité (RGPD)")
)
. $this->Modal->body($this->element('rgpd', ['rgpdData' => $rgpdData ?? []]))
. $this->Modal->end();

try {
    /** @var Identity $identity */
    $identity = $this->getRequest()->getAttribute('identity');
    $user = $identity?->getOriginalData() ?? [];
    $saName = h(Hash::get($user, 'org_entity.archival_agency.name'));
    foreach (Hash::get($user, 'org_entity.archival_agency.configurations', []) as $configuration) {
        if ($configuration['name'] === 'header-title') {
            $saName = h($configuration['setting']);
            break;
        }
    }
    $entityName = h(Hash::get($user, 'org_entity.name'));
    if ($saName === $entityName) {
        $entityName = '';
    }
    $nom = h(trim(Hash::get($user, 'name')));
    $name = $nom ?: h(trim(Hash::get($user, 'username')));
    $user_id = Hash::get($user, 'id');

    $session = $this->getRequest()->getSession();
    $adminTech = $session->read('Admin.tech');
    if ($adminTech) {
        $entityName = __("Administrateur");
        $name = h(trim($session->read('Admin.data.username')));
    }
} catch (Cake\Database\Exception $e) {
    $saName = null;
    $entityName = null;
    $name = null;
    $adminTech = false;
}

echo $this->Html->tag('div.notification-container', '');

echo $this->Html->tag('nav.navbar.fixed-top.navbar-expand-md.navbar-ls.navbar-dark.bg-ls-dark');

// Logo
echo $this->Html->link(
    $this->fetch('logo'),
    '/',
    [
        'class' => 'navbar-brand',
        'escape' => false,
        'title' => __("Revenir à l'accueil de l'application"),
    ]
);

// header
echo $this->Html->div('navbar-brand', $saName ?? '');

echo $this->Html->tag('div.collapse.navbar-collapse.justify-content-end');

echo $this->Html->tag('ul.navbar-nav');

if ($user_id || !empty($admin)) {
    echo $this->Html->tag('li#container-number-notification.nav-item.dropdown.danger');
    echo $this->Html->tag(
        'a',
        $this->Fa->charte('Notification', '', 'animated')
        . $this->Html->tag('span.resp.resp-hide-large', __("Notifications"), ['aria-hidden' => 'true'])
        . $this->Html->tag('span.sr-only', __("Notifications"))
        . $this->Html->tag('span#number-notification.animated', 0)
        . $this->Html->tag('span.caret', ''),
        [
            'class' => 'nav-link dropdown-toggle',
            'href' => '#',
            'id' => 'notificationDropdown',
            'role' => 'button',
            'data-toggle' => 'dropdown',
            'aria-haspopup' => 'true',
            'aria-expanded' => 'false',
        ]
    );
    echo $this->Html->tag(
        'ul#notifications-ul-container',
        null,
        ['class' => 'dropdown-menu dropdown-menu-right', 'aria-labelledby' => 'notificationDropdown']
    );

    echo $this->Html->tag(
        'li',
        $this->Html->tag(
            'button.navbar-link.btn-link',
            __("Supprimer toutes les notifications"),
            ['type' => 'button', 'onclick' => "AsalaeGlobal.deleteAllNotifications(new Event('click'))"]
        )
    );

    echo $this->Html->tag('/ul');
    echo $this->Html->tag('/li'); // notifications
}

if ($name) {
    // menu utilisateur
    echo $this->Html->tag('li.nav-item.dropdown', null, ['id' => 'user-menu-dropdown']);
    echo $this->Html->tag(
        'a',
        $entityName . ' - ' . $name,
        [
            'class' => 'nav-link dropdown-toggle',
            'href' => '#',
            'id' => 'navbarDropdown',
            'role' => 'button',
            'data-toggle' => 'dropdown',
            'aria-haspopup' => 'true',
            'aria-expanded' => 'false',
        ]
    );
    echo $this->Html->tag(
        'div',
        null,
        ['class' => 'dropdown-menu dropdown-menu-right', 'aria-labelledby' => 'navbarDropdown']
    );

    /**
     * début du contenu du menu utilisateur
     */
    if ($this->Acl->check('/users/edit')) {
        echo $this->ModalForm
            ->create('user-edit-form')
            ->modal(__("Modifier mon compte utilisateur"))
            ->output(
                'button',
                $this->Fa->i('fa-address-card-o', __("Modifier mon compte utilisateur")),
                '/users/edit'
            )
            ->generate(
                [
                    'class' => 'dropdown-item',
                    'type' => 'button'
                ]
            );
        echo $this->Html->tag(
            'script',
            "$('#body-content').append($('#user-edit-form'));"
        );
    }
    echo $this->Acl->link(
        $this->Fa->i('fa-tasks', __("Mes jobs en attente")),
        '/tasks/index',
        ['class' => 'dropdown-item disable-drag', 'escape' => false]
    );
    if ($adminTech) {
        echo $this->ModalForm
            ->create('user-admin-edit-form')
            ->modal(__("Modifier mon compte administrateur"))
            ->output(
                'button',
                $this->Fa->i('fa-address-card-o', __("Modifier mon compte administrateur")),
                '/admins/editAdmin'
            )
            ->generate(
                [
                    'class' => 'dropdown-item',
                    'type' => 'button'
                ]
            );
        echo $this->Html->tag(
            'script',
            "$('#body-content').append($('#user-admin-edit-form'));"
        );
    }

    echo $separator;
    echo $this->Html->tag(
        'button',
        $this->Fa->i('fa-user-secret', __("Politique de confidentialité (RGPD)")),
        [
            'data-toggle' => 'modal',
            'data-target' => '#modal-rgpd',
            'class' => 'dropdown-item',
            'type' => 'button',
        ]
    );

    echo $separator;

    echo $this->Html->link(
        $this->Fa->charte('Se déconnecter', __("Se déconnecter")),
        $adminTech ? '/admins/logout' : '/users/logout',
        ['class' => 'dropdown-item', 'escape' => false, 'data-mode' => 'no-ajax']
    );

    // fin du contenu du menu utilisateur
    echo $this->Html->tag('/div');
    echo $this->Html->tag('/li'); // menu utilisateur
}

echo $this->Html->tag('/ul'); // ul
echo $this->Html->tag('/div'); // header
echo $this->Html->tag('/nav');

echo $this->Html->tag('/header');
