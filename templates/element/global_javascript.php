<?php

/**
 * @var Versae\View\AppView $this
 */
use Cake\Core\Configure;

$configs = [
    'Ratchet' => Configure::read('Ratchet'),
];
$configsJson = preg_replace(
    '/\n/',
    "\n            ",
    Configure::read('debug') ? json_encode($configs, JSON_PRETTY_PRINT) : json_encode($configs)
);
$user_id = $user_id ?? '';
$configsJson = $configsJson ?? '{}';
$notifications = $notifications ?? '[]';
try {
    $token = $this->getRequest()->getSession()->read('Session.token');
} catch (\Cake\Database\Exception $e) {
    $token = null;
}
?>

    <script>
        var PHP = {
            username: <?=isset($username) ? sprintf("'%s'", addcslashes($username, "'")) : 'null'?>,
            user_id: <?=$user_id ?: 'null'?>,
            sa_id: <?=$saId ?? 'null'?>,
            token: '<?=$token?>',
            urls: {
                ratchet: '<?=Configure::read('Ratchet.connect')?>',
                deleteNotification: '<?=$this->Url->build('/Notifications/delete')?>',
                resetSession: '<?=$this->Url->build('/Users/reset-session')?>',
                ajaxSaveMenu: '<?=$this->Url->build('/Users/ajax-save-menu')?>/',
                checkConnection: '<?=$this->getRequest()->getAttribute('webroot')
                . ($this->getRequest()->getParam('controller') === 'Admins'
                    ? 'conn.php?admin=true'
                    : 'conn.php')?>'
            },
            configs: <?=$configsJson?>,
            notifications: <?=$notifications?>,
            ini: {
                session_gc_maxlifetime: <?=ini_get('session.gc_maxlifetime') ?: 0?>
            },
            messages: {
                genericError: "<?=__("Une erreur a eu lieu")?>",
                msgSendSuccess: "<?=__("Message envoyé avec succès")?>",
                msgSendFailed: "<?=__("Erreur lors de l'envoi du message")?>",
                XsdForm: {
                    value: "<?=__d('xsdform', "Valeur")?>",
                    attribute: "<?=__d('xsdform', "Attribut")?>",
                    element: "<?=__d('xsdform', "Element")?>",
                    elements: "<?=__d('xsdform', "Elements")?>",
                    args: "<?=__d('xsdform', "Arguments")?>",
                    add: "<?=__d('xsdform', "Ajouter")?>",
                    viewTitle: "<?=__d('xsdform', "Voir documentation")?>",
                    doc: "<?=__d('xsdform', "Documentation")?>",
                    openTitle: "<?=__d('xsdform', "Ouvrir")?>",
                    removeTitle: "<?=__d('xsdform', "Retirer valeur")?>",
                    removeConfirm: "<?=__d(
                        'xsdform',
                        "Cette action va retirer les valeurs contenu dans l'element, Voulez-vous continuer ?"
                    )?>",
                    toggleAttributesTitle: "<?=__d('xsdform', "Afficher/Cacher les attributs")?>",
                    close: "<?=__d('xsdform', "Fermer")?>"
                },
                minifiable: {
                    compress: "<?=__("Réduire")?>",
                    expand: "<?=__("Agrandir")?>"
                }
            },
            cookies: <?=$cookies ?? 'null'?>,
            debug: <?=Configure::read('debug') ? 'true' : 'false'?>
        }
    </script>
