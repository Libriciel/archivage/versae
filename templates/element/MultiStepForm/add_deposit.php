<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->MultiStepForm->create(
    [
        __("Choix du formulaire"),
        __("Saisie du versement"),
    ],
    $step
);
