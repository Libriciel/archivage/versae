<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->MultiStepForm->create(
    [
        __("Informations générales"),
        __("Details"),
    ],
    $step
);
