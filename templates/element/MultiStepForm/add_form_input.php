<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->MultiStepForm->create(
    [
        __("Choix du type"),
        __("Paramétrage"),
    ],
    $step
);
