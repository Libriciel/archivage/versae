<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->MultiStepForm->create(
    [
        __("Création du service d'archives"),
        __("Premier utilisateur"),
    ],
    $step
);
