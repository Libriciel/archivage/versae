<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\I18n\I18n;

$css = [
//    'bootstrap.min.css', // debug - remplace as-bootstrap-4.css pour tester
    'as-bootstrap-4.css',
    '/libriciel-login/forkawesome/css/fork-awesome.css',
    'login.css',
    'font-awesome5.min.css',
    'font-awesome.min.css',
    'font-awesome-animation.min.css',
    'jquery-ui.min.css',
    'jstree/style.min.css',
    'jquery.orgchart.css',
    'chosen.css',
    'select2.css',
    'Chart.min.css',
    'asalae.css',
    'asalae.bootstrapv4fix.css',
    'versae.css',
];
try {
    $saId = $this->getRequest()->getSession()->read('Auth.org_entity.archival_agency.id');
} catch (\Cake\Database\Exception $e) {
    $saId = null;
}
if (is_file(WWW_ROOT . 'org-entity-data' . DS . $saId . DS . 'background.css')) {
    $css[] = "/org-entity-data/$saId/background.css";
}

if (!empty($high_contrast)) {
    $css[] = 'high_contrast.css';
}
echo $this->Html->css($css);

list($lang) = explode('_', $i18nLang = I18n::getLocale());
$scripts = [
    'jquery-3.2.1.js',
    'js.cookie.min.js',
    'jquery.actual.min.js',
    'bootstrap.bundle.min.js',
    'jquery-ui.min.js',
    'i18n/datepicker-fr.js',
    'moment-with-locales.min.js',
    'jquery.ui.touch-punch.min.js',
    'jquery-ui-timepicker-addon.js',
    'autobahn.min.js',
    'jsrender.min.js',
    'favico-0.3.10.min.js',
    'flow.js',
    'jstree/jstree.js',
    'jquery.orgchart.js',
    'jquery.tablednd.js',
    'jquery-serialize-object.min.js',
    'chosen.jquery.js',
    'select2/select2.full.js',
    'select2/i18n/' . $lang . '.js',
    'Chart.min.js',
    'sprintf.js',
    'tinymce/tinymce.min.js',
    'uuid.js',
    'ansi_to_html.js',
    'asalae.translation.js.php?' . filemtime(APP . 'translations.php'),
    'asalae.extended_jquery.js',
    'asalae.popup.js',
    'asalae.table-generator.js',
    'asalae.formhelper.js',
    'asalae.ruletoform.js',
    'asalae.upload.js',
    'asalae.ui.js',
    'asalae.modal.js',
    'asalae.paginator.js',
    'asalae.cookies.js',
    'asalae.global-top.js',
    'asalae.filter.js',
    'versae.global.js',
];
$mcelangDir = WWW_ROOT . 'js' . DS . 'tinymce' . DS . 'langs' . DS;
$tinymceDefaultLanguage = null;
if (file_exists($mcelangDir . $i18nLang . '.js')) {
    $scripts[] = 'tinymce' . DS . 'langs' . DS . $i18nLang . '.js';
    $tinymceDefaultLanguage = $i18nLang;
} elseif (file_exists($mcelangDir . $lang . '.js')) {
    $scripts[] = 'tinymce' . DS . 'langs' . DS . $lang . '.js';
    $tinymceDefaultLanguage = $lang;
}
?>
<?=str_replace(' ext=""', '', $this->Html->script($scripts, ['ext' => '']));?>
<script>var tinymceDefaultLanguage = '<?=$tinymceDefaultLanguage?>';</script>
<?= $this->fetch('meta') ?>
<meta http-equiv="Content-Security-Policy"
    content="default-src 'self' 'unsafe-eval' 'unsafe-inline' blob: ws: ; img-src 'self' data:">

<?= $this->fetch('css') ?>

<?= $this->fetch('script') ?>

