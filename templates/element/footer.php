<?php

/**
 * Code obtenue grace à l'utilisation de:
 *

<?=$this->Html->script(
    Configure::read('Libriciel.login.footer_js', ['/libriciel-login/ls-elements.js'])
)?>

 *
 * et l'utilisation dans le bloc suivant de:
 *

echo $this->Html->tag(
    'ls-lib-footer',
    '',
    Configure::read('Libriciel.login.lib-footer', [])
    + [
        'class' => 'ls-login-footer ls-footer-fixed-bottom',
        'application_name' => Configure::read('App.version', VERSAE_VERSION_LONG),
    ]
);

 */

use Cake\Core\Configure;

?>
<ls-lib-footer _nghost-swj-c0="" active="asalae"
               application_name="1.0.0" class="ls-login-footer ls-footer-fixed-bottom" ng-version="7.2.16">
    <footer data-ls-element="" class="ls-footer">
        <div data-ls-element="" class="ls-footer-item ls-xs-hide">
            <ul data-ls-element="" class="ls-icon-list ls-hide-when-no-login">
                <!---->
                <li data-ls-element="">
                    <a data-ls-element="" class="asalae active"
                       data-toggle="tooltip" href="https://www.libriciel.fr/asalae"
                       id="asalae"
                       target="_blank" title="En savoir plus sur le logiciel asalae">
                        <span class="sr-only">En savoir plus sur le logiciel asalae</span>
                    </a>
                </li>
                <li data-ls-element="">
                    <a data-ls-element="" class="idelibre"
                       data-toggle="tooltip" href="https://www.libriciel.fr/i-delibre"
                       id="idelibre"
                       target="_blank" title="En savoir plus sur le logiciel i-delibRE">
                        <span class="sr-only">En savoir plus sur le logiciel i-delibRE</span>
                    </a>
                </li>
                <li data-ls-element="">
                    <a data-ls-element="" class="iparapheur"
                       data-toggle="tooltip" href="https://www.libriciel.fr/i-parapheur"
                       id="iparapheur"
                       target="_blank"
                       title="En savoir plus sur le logiciel i-Parapheur">
                        <span class="sr-only">En savoir plus sur le logiciel i-Parapheur</span>
                    </a>
                </li>
                <li data-ls-element="">
                    <a data-ls-element="" class="pastell"
                       data-toggle="tooltip" href="https://www.libriciel.fr/pastell"
                       id="pastell"
                       target="_blank" title="En savoir plus sur le logiciel Pastell">
                        <span class="sr-only">En savoir plus sur le logiciel Pastell</span>
                    </a>
                </li>
                <li data-ls-element="">
                    <a data-ls-element="" class="s2low"
                       data-toggle="tooltip" href="https://www.libriciel.fr/s2low"
                       id="s2low"
                       target="_blank" title="En savoir plus sur le logiciel S²LOW">
                        <span class="sr-only">En savoir plus sur le logiciel S²LOW</span>
                    </a>
                </li>
                <li data-ls-element="">
                    <a data-ls-element="" class="webdelib"
                       data-toggle="tooltip" href="https://www.libriciel.fr/web-delib"
                       id="webdelib"
                       target="_blank" title="En savoir plus sur le logiciel web-delib">
                        <span class="sr-only">En savoir plus sur le logiciel web-delib</span>
                    </a>
                </li>
                <li data-ls-element="">
                    <a data-ls-element="" class="webdpo"
                       data-toggle="tooltip" href="https://www.libriciel.fr/web-dpo"
                       id="webdpo"
                       target="_blank" title="En savoir plus sur le logiciel web-DPO">
                        <span class="sr-only">En savoir plus sur le logiciel web-DPO</span>
                    </a>
                </li>
                <li data-ls-element="">
                    <a data-ls-element="" class="webgfc"
                       data-toggle="tooltip" href="https://www.libriciel.fr/web-gfc"
                       id="webgfc"
                       target="_blank" title="En savoir plus sur le logiciel web-GFC">
                        <span class="sr-only">En savoir plus sur le logiciel web-GFC</span>
                    </a>
                </li>
            </ul>
        </div>
        <div data-ls-element="" class="ls-footer-item ls-auto-size">
            <span data-ls-element=""><?=Configure::read('App.version', VERSAE_VERSION_LONG)?> © </span>
            <span data-ls-element="" class="ls-xs-hide"> Libriciel SCOP 2022-<?=date('Y')?> </span>
        </div>
        <div data-ls-element="" class="ls-footer-item">
            <ul data-ls-element="" class="ls-icon-list">
                <li data-ls-element="">
                    <a data-ls-element=""
                       class="ls-hide-when-no-login libriciel"
                       href="https://www.libriciel.fr" id="libriciel"
                       target="_blank" title="Accéder au site de Libriciel SCOP">
                        <span class="sr-only">Libriciel</span>
                    </a>
                    <a data-ls-element=""
                       class="active ls-hide-when-login libriciel libriciel-colors"
                       href="https://www.libriciel.fr" id="libriciel-color"
                       target="_blank" title="Accéder au site de Libriciel SCOP">
                        <span class="sr-only">Libriciel</span>
                    </a>
                </li>
            </ul>
        </div>
    </footer>
</ls-lib-footer>
