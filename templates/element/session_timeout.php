<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Core\Configure;

$button = Configure::read('Keycloak.enabled') || Configure::read('OpenIDConnect.enabled')
    ? $this->Html->link(
        $this->Fa->i('fa-key', __("Se connecter via OpenIDConnect")),
        $authUrl,
        [
            'escape' => false,
            'target' => '_blank',
            'class' => 'btn btn-primary',
            'onclick' => "$(this).closest('.modal').modal('hide')",
        ]
    )
    : $this->Form->button(
        $this->Fa->charte('Se connecter', __("Se connecter"), 'fa-fw'),
        ['bootstrap-type' => 'primary', 'class' => 'accept']
    );

echo $this->ModalForm
    ->create(
        'login-modal',
        [
            'acceptButton' => $button,
            'class' => 'login-modal'
        ]
    )
    ->modal(__("Votre session a expiré"))
    ->javascriptCallback("AsalaeSessionTimeout.displayModalSessionTimeoutCallback")
    ->output(
        'function',
        'displayModalSessionTimeout',
        $this->getRequest()->getParam('controller') === 'Admins' ? '/Admins/ajax-login' : '/Users/ajax-login'
    )
    ->generate()

    . $this->Html->script('asalae.sessiontimeout.js');
?>
<script>
    $('#login-modal').on('show.bs.modal', function() {
        var fn = window.onbeforeunload;
        $(this).one('shown.bs.modal', function() {
            window.onbeforeunload = fn;
        });
    });
</script>
