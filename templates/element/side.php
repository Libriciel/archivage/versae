<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Http\ServerRequest;
use Cake\Routing\Router;
use Laminas\Diactoros\Uri;

$menuDefinition = [
    '--------------------------------------------------',
    [
        'icon' => $this->Fa->i('fa-tachometer fa-fw mr-3'),
        'label' => __("Tableau de bord"),
        'url' => '/home/index',
    ],
    [
        'icon' => $this->Fa->i('fa-exchange fa-fw mr-3'),
        'label' => __("Versements"),
        'dropdown' => [
            [
                'icon' => $this->Fa->i('fa-plus fa-fw mr-3'),
                'label' => __("Créer un versement"),
                'url' => '/deposits/index-in-progress?add=deposit',
            ],
            [
                'icon' => $this->Fa->i('fa-code fa-fw mr-3'),
                'label' => __("Mes versements en cours"),
                'url' => '/deposits/index-in-progress',
            ],
            [
                'icon' => $this->Fa->i('fa-folder-open fa-fw mr-3'),
                'label' => __("Tous mes versements"),
                'url' => '/deposits/index-all',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-folder-open fa-fw mr-3'),
                'label' => __("Tous les versements de mon service"),
                'url' => '/deposits/index-my-entity',
            ],
        ],
    ],
    '--------------------------------------------------',
    [
        'icon' => $this->Fa->i('fa-list-alt fa-fw mr-3'),
        'label' => __("Formulaires"),
        'dropdown' => [
            [
                'icon' => $this->Fa->i('fa-code fa-fw mr-3'),
                'label' => __("Tous les formulaires"),
                'url' => '/forms/index-editing',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-globe fa-fw mr-3'),
                'label' => __("Formulaires publiés"),
                'url' => '/forms/index-published',
            ],
            [
                'icon' => $this->Fa->i('fa-folder-open fa-fw mr-3'),
                'label' => __("Formulaires toutes versions"),
                'url' => '/forms/index-all',
            ],
        ],
    ],
    '--------------------------------------------------',
    [
        'icon' => $this->Fa->i('fa-wrench fa-fw mr-3'),
        'label' => __("Administration"),
        'collapsed' => true,
        'dropdown' => [
            [
                'icon' => $this->Fa->i('fa-building fa-fw mr-3'),
                'label' => __("Entités"),
                'url' => '/org-entities/index',
            ],
            [
                'icon' => $this->Fa->i('fa-users fa-fw mr-3'),
                'label' => __("Rôles utilisateur"),
                'url' => '/roles/index',
            ],
            [
                'icon' => $this->Fa->i('fa-user fa-fw mr-3'),
                'label' => __("Utilisateurs"),
                'url' => '/users/index',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-key fa-fw mr-3'),
                'label' => __("Mots clés"),
                'url' => '/keyword-lists/index',
            ],
            [
                'icon' => $this->Fa->i('fa-tachometer fa-fw mr-3'),
                'label' => __("Compteurs"),
                'url' => '/counters/index',
            ],
            '--------------------------------------------------',
            [
                'icon' => $this->Fa->i('fa-tasks'),
                'label' => __("Jobs en cours"),
                'url' => '/tasks/admin',
            ],
            [
                'icon' => $this->Fa->i('fa-sliders fa-fw mr-3'),
                'label' => __("Configuration"),
                'url' => '/org-entities/edit-config',
                'type' => 'button',
                'onclick' => 'editArchivalAgencyConfig()'
            ],
        ],
    ],
];
$separator = $this->Html->tag('hr', '');
//
echo $this->Html->tag('div#sidebar-container.sidebar-expanded');
echo $this->Html->tag('ul.list-group');
$headerMenu = [];
$urlArray = Router::parseRequest($this->getRequest());
foreach ($menuDefinition as $i => $mainLink) {
    if (is_string($mainLink)) {
        if (!empty($headerMenu) && end($headerMenu) !== $separator) {
            $headerMenu[] = $separator;
        }
        continue;
    }
    $mainLink += [
        'icon' => '',
        'label' => 'undefined',
        'url' => '#',
        'collapsed' => false,
        'dropdown' => null,
    ];
    $show = true;
    if ($mainLink['url'] !== '#' && $this->Acl->check($mainLink['url']) === false) {
        $show = false;
    }
    $hasSublink = false;
    if (!$mainLink['dropdown']) {
        if ($show) {
            $href = $mainLink['url'];
            if ($href && $href[0] !== '#') {
                $href = $this->Url->build($mainLink['url']);
            }
            $active = '';
            // cas particulier
            if ($href === '/home/index') {
                if ($urlArray['controller'] === 'Home' && $urlArray['action'] === 'index') {
                    $active = ' active';
                }
            }
            $link = $this->Html->tag(
                'a',
                $this->Html->tag(
                    'div.d-flex.align-items-center',
                    $mainLink['icon']
                    . $this->Html->tag('span', $mainLink['label'])
                ),
                [
                    'class' => 'list-group-item list-group-item-action' . $active,
                    'href' => $href,
                ]
            );
            $headerMenu[] = $link;
        }
        continue;
    }

    $linkId = 'nav-submenu' . $i;
    $subLinks = [];
    $sublinkIsActive = false;
    foreach ($mainLink['dropdown'] as $subLink) {
        if (is_string($subLink)) {
            if (!empty($headerMenu) && end($subLinks) !== $separator) {
                $subLinks[] = $separator;
            }
            continue;
        }
        $subLink += [
            'type' => 'link',
            'onclick' => '',
        ];
        $href = $subLink['url'];
        if ($href[0] !== '#' && $this->Acl->check($href) === false) {
            continue;
        }
        $active = '';
        if ($href !== '#') {
            $uri = new Uri($href);
            $urlParams = Router::parseRequest(new ServerRequest(['url' => Router::url($href), 'uri' => $uri]));
            if (
                $urlParams['controller'] === $urlArray['controller']
                && $urlParams['action'] === $urlArray['action']
                && !isset($urlParams['?'])
            ) {
                $active = ' active';
                $sublinkIsActive = true;
            }
        }
        $subLinks[] = $subLink['type'] === 'button'
            ? $this->Html->tag(
                'button',
                $subLink['icon'] . ' ' . $subLink['label'],
                [
                    'class' => 'list-group-item list-group-item-action sublist-item' . $active,
                    'escape' => false,
                    'onclick' => $subLink['onclick']
                ]
            )
            : $this->Html->link(
                $subLink['icon'] . $subLink['label'],
                $href,
                [
                    'class' => 'list-group-item list-group-item-action sublist-item' . $active,
                    'escape' => false,
                ]
            );
    }

    // retrait des $separator en début
    $c = count($subLinks);
    for ($i = 0; $i < $c; $i++) {
        if ($subLinks[$i] === $separator) {
            unset($subLinks[$i]);
        } else {
            break;
        }
    }
    $subLinks = [...$subLinks];

    // retrait des $separator en fin
    $c = count($subLinks);
    for ($i = $c - 1; $i > 0; $i--) {
        if ($subLinks[$i] === $separator) {
            unset($subLinks[$i]);
        } else {
            break;
        }
    }

    if ($show && $subLinks) {
        $class = trim(($mainLink['collapsed'] ? 'collapsed' : '') . ($sublinkIsActive ? ' active' : ''));
        $link = $this->Html->tag(
            'a',
            $this->Html->tag(
                'div.d-flex.align-items-center',
                $mainLink['icon']
                . $this->Html->tag('span', $mainLink['label'])
                . $this->Html->tag('span.submenu-icon.ml-auto', '')
            ),
            [
                'class' => 'list-group-item list-group-item-action' . ($class ? " $class" : ''),
                'href' => '#' . $linkId,
                'data-toggle' => 'collapse',
                'aria-expanded' => $mainLink['collapsed'] ? 'false' : 'true',
            ]
        );
        $link .= $this->Html->tag("div#$linkId.collapse.sidebar-submenu" . ($mainLink['collapsed'] ? '' : '.show'));
        $link .= implode(PHP_EOL, $subLinks);
        $link .= $this->Html->tag('/div');
        $headerMenu[] = $this->Html->tag('div.wrapper', $link);
    }
}

echo '<li>' . implode('</li>' . PHP_EOL . '<li>', $headerMenu) . '</li>';

echo $this->Html->tag('/ul');

// Icone pour replier la sidebar
echo $this->Html->tag('div.ls-sidebar-toggle');
echo $this->Html->tag(
    'a',
    $this->Html->tag('div.collapse-icon.ng-star-inserted', ''),
    [
        'href' => '#',
        'class' => 'toggle-collapse',
        'data-toggle' => 'sidebar-collapse',
        'title' => $title = __("Replier le menu latéral"),
        'aria-label' => $title,
    ]
);
echo $this->Html->tag('/div');
echo $this->Html->tag('/div'); // #sidebar-container
?>
<script>
    var navContainer = $('#sidebar-container');
    // Collapse click
    $('[data-toggle=sidebar-collapse]').click(
        function () {
            $('#sidebar-container').toggleClass('sidebar-expanded sidebar-collapsed')
                .find('.fa')
                .toggleClass('mr-3');
            $('#body-row').toggleClass('sidebar-is-collapsed');
        }
    );
    navContainer.find('> ul > li > .wrapper > a').on(
        'mouseover focus',
        function () {
            // Si le menu est en mode réduit
            if (navContainer.hasClass('sidebar-collapsed')
                || window.matchMedia('(max-width: 767px)').matches
            ) {
                var target = $($(this).attr('href'));
                navContainer.find('.hover-show').not(target).removeClass('hover-show');
                target.addClass('hover-show');
                target.css('top', $(this).offset().top)
            }
        }
    );
    navContainer.on(
        'mouseleave focusout',
        function (event) {
            setTimeout(
                function () {
                    var isChildOfMenu = $(':focus').closest(navContainer).length === 1;
                    if ((event.type === 'blur' || event.type === 'focusout') && isChildOfMenu) {
                        return;
                    }
                    navContainer.find('.hover-show').removeClass('hover-show');
                },
                10
            );
        }
    );

    navContainer.find('a.list-group-item-action').on(
        'before.intercept',
        function () {
            navContainer.find('.active').removeClass('active');
            $(this).addClass('active').closest('.sidebar-submenu').each(
                function () {
                    $(this).parent().find('[href="#' + $(this).attr('id') + '"]').addClass('active');
                }
            );
        }
    );

    /**
     * Place la classe "active" dans le menu si l'url correspond
     */
    $(window).on(
        'history.change',
        function (event, url) {
            if (url.indexOf('#') !== -1) {
                url = url.substr(0, url.indexOf('#'));
            }
            if (url.indexOf('?') !== -1) {
                url = url.substr(0, url.indexOf('?'));
            }
            url = url.replace(/^\//, '');
            url = AsalaeInflector.dasherize(url);
            if (!url) {
                url = 'home/index';
            }
            navContainer.find('.active').removeClass('active');
            navContainer.find('a[href]').each(
                function () {
                    var href = AsalaeInflector.dasherize($(this).attr('href'));
                    if (href.indexOf(url) !== -1) {
                        $(this).trigger('before.intercept');
                    }
                }
            );
        }
    );
</script>
