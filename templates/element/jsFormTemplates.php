<?php

/**
 * @var Versae\View\AppView $this
 */

?>
<script id="jsFormTemplateInput" type="text/x-jsrender">
<?=str_replace(
    [' required="required"', 'form-group {{:type}} required'],
    ['{{if required}} required="required"{{/if}}', 'form-group {{:type}}{{if required}} required{{/if}}'],
    $this->Form->control(
        '{{:input}}',
        [
            'label' => '{{:label}}',
            'id' => '{{:id}}',
            'name' => '{{:name}}',
            'class' => '{{:class}}',
            'type' => '{{:type}}',
            'data-fullname' => '{{:fullname}}',
            'placeholder' => '{{:placeholder}}',
            'help' => '{{:help}}',
            'pattern' => '{{:pattern}}',
            'required' => true,
            'min' => '{{:min}}',
            'max' => '{{:max}}',
        ]
    )
)?>
</script>
<script id="jsFormTemplateSelect" type="text/x-jsrender">
<?=str_replace(
    [' required="required"', 'form-group select required'],
    ['{{if required}} required="required"{{/if}}', 'form-group select{{if required}} required{{/if}}'],
    $this->Form->control(
        '{{:input}}',
        [
            'label' => '{{:label}}',
            'id' => '{{:id}}',
            'name' => '{{:name}}',
            'class' => '{{:class}}',
            'type' => 'select',
            'data-fullname' => '{{:fullname}}',
            'help' => '{{:help}}',
            'required' => true,
            'options' => [],
            'empty' => true
        ]
    )
)?>
</script>
<script id="jsFormTemplateSelectMultiple" type="text/x-jsrender">
<?=str_replace(
    [' required="required"', 'form-group select required'],
    ['{{if required}} required="required"{{/if}}', 'form-group select{{if required}} required{{/if}}'],
    $this->Form->control(
        '{{:input}}',
        [
            'label' => '{{:label}}',
            'id' => '{{:id}}',
            'name' => '{{:name}}',
            'class' => '{{:class}}',
            'type' => 'select',
            'data-fullname' => '{{:fullname}}',
            'help' => '{{:help}}',
            'required' => true,
            'options' => [],
            'multiple' => true
        ]
    )
)?>
</script>
<script id="jsFormTemplateTextarea" type="text/x-jsrender">
<?=str_replace(
    [' required="required"', 'form-group textarea required'],
    ['{{if required}} required="required"{{/if}}', 'form-group textarea{{if required}} required{{/if}}'],
    $this->Form->control(
        '{{:input}}',
        [
            'label' => '{{:label}}',
            'id' => '{{:id}}',
            'name' => '{{:name}}',
            'class' => '{{:class}}',
            'type' => 'textarea',
            'data-fullname' => '{{:fullname}}',
            'required' => '{{:required}}',
            'help' => '{{:help}}',
        ]
    )
)?>
</script>
<script id="jsFormTemplateOption" type="text/x-jsrender">
<option value="{{:value}}">{{:traduction}}</options>
</script>
<script id="jsFormTemplateOptGroup" type="text/x-jsrender">
<optgroup label="{{:label}}"></optgroup>
</script>
<script id="jsFormTemplateClosableFieldset" type="text/x-jsrender">
<fieldset class="closable-fieldset">
    <legend>{{:legend}} <span class="pull-right">
        <i aria-hidden="true" class="fa fa-window-minimize slide-icon"></i>
        <i aria-hidden="true" class="fa fa-times slide-icon"></i>
    </span></legend>
    <div class="minifiable-target group-minifiable"></div>
</fieldset>
</script>
