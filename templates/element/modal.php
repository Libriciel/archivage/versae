<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Core\Configure;
use Cake\Utility\Inflector;

$jsVarTable = $jsVarTable ?? Inflector::camelize(
    'generated_' . preg_replace('/\W/', '_', $idTable)
);
?>
<?= $this->Modal->create(
    __("Configuration du tableau de résultats"),
    ['id' => 'modal-' . $idTable, 'class' => 'ui-draggable-handle']
);?>

<div class="cancelable">
    <h4 class="fake-label"><?=__("Méthode d'affichage")?></h4>
    <div class="text-center table-style">
        <?=$this->Html->tag(
            'button.switch-table-view.btn.columns',
            $this->Fa->i('fa-th fa-4x', '', ['title' => __("Plusieurs colonnes")])
            . $this->Html->tag('span.sr-only', __("Plusieurs colonnes")),
            ['data-view' => 'default']
        )?>
        <?=$this->Html->tag(
            'button.switch-table-view.btn.rows',
            $this->Fa->i('fa-th-list fa-4x', '', ['title' => __("Une seule colonne")])
            . $this->Html->tag('span.sr-only', __("Une seule colonne")),
            ['data-view' => 'div']
        )?>
    </div>

    <div class="form-group showed">
        <h4 class="fake-label"><?=__("Affichés")?></h4>
        <div class="drop-zone"></div>
    </div>

    <div class="form-group hidded">
        <h4 class="fake-label"><?=__("Disponibles")?></h4>
        <div class="drop-zone"></div>
    </div>
<?php if ($paginate ?? true) : ?>
    <div class="form-group hidded">
        <label><?=__("Nombre maximal de résultats par pages")?>
            <select class="nb-max-results">
                <?php echo '<option>' . implode(
                    '</option><option>',
                    array_map(
                        'trim',
                        explode(
                            ',',
                            Configure::read('Pagination.options', '10, 20, 50, 100')
                        )
                    )
                ) . '</option>' . PHP_EOL; ?>
            </select>
        </label>
    </div>
<?php endif; ?>
    <div class="form-group delete text-right">
        <button type="button" class="btn btn-default" id="delete-cookies-<?=$idTable?>">
            <?=$this->Fa->charte('Réinitialiser', __("Réinitialiser"))?>
        </button>
    </div>
</div>
<script>
    $(function() {
        var table;
        table = <?=$jsVarTable?>;
        table.insertConfiguration(table._configId);

        // Suppression des cookies
        $('#delete-cookies-<?=$idTable?>').click(function() {
            if (confirm("<?=
                __(
                    "Cette action va supprimer vos préférences de façon permanente. Voulez-vous continuer ?"
                )
                ?>")) {
                var table;
                table = <?=$jsVarTable?>;
                table.deleteCookies();
                if (confirm("<?=__(
                    "Vous devez recharger la page pour prendre en compte "
                    . "toutes les modifications. Voulez-vous le faire maintenant ?"
                )?>")) {
                    location.reload();
                }
            }
        });

        // active le type de tableau par cookies
        if (AsalaeCookies.get(table.cookies.view) === 'div') {
            $('#<?=$idTable?> button.rows').addClass('active');
        } else {
            $('#<?=$idTable?> button.columns').addClass('active');
        }
    });
</script>
<?php
echo $this->Modal->end(
    [
        $this->Form->button(
            $this->Fa->charte('Annuler', 1, __("Annuler")),
            ['data-dismiss' => 'modal', 'class' => 'cancel']
        ),
        $this->Form->button(
            $this->Fa->i('fa-level-down', __("Appliquer")),
            ['class' => 'apply']
        ),
        $this->Form->button(
            $this->Fa->charte('Enregistrer', __("Enregistrer")),
            [
                'bootstrap-type' => 'primary', 'data-dismiss' => 'modal',
                'class' => 'accept'
            ]
        )
    ]
)
. $this->ModalForm->modalScript('modal-' . $idTable);
?>
