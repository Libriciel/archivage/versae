<?php

/**
 * @var Versae\View\AppView $this
 */
use Cake\Error\Debugger;

$errorTitle = '';
if (!empty($error->queryString)) {
    $errorTitle .= $this->Html->tag(
        'p',
        '<strong>SQL Query: </strong>' . h($error->queryString),
        ['class' => 'notice']
    );
}
if (!empty($error->params)) {
    $errorTitle .= '<strong>SQL Query Params: </strong>' . Debugger::exportVar($error->params);
}
if ($error instanceof Error || $error instanceof Exception) {
    $errorTitle .= '<strong>Error in: </strong>'
        . sprintf('%s, line %s', $error->getFile(), $error->getLine());
}
?>
<section class="container bg-white">
    <header>
        <h2 class="h4"><?=$errorTitle?></h2>
    </header>
    <?=$this->element('auto_table_warning')?>
    <p><?=h($message)?></p>
    <?php if (extension_loaded('xdebug')) :
        xdebug_print_function_stack();
    endif; ?>
</section>
<script>
    $(function() {
        // Rétabli les liens après qu'ils ai été modifié par global.js
        setTimeout(function() {
            $('table.xdebug-error').find('a').off();
        }, 1);
    });
</script>
