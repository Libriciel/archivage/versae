<?php

/**
 * @var Versae\View\AppView $this
 */
use Cake\Core\Configure;

?>
<section class="container bg-white">
    <h1><i aria-hidden="true" class="fa fa-warning"></i> <?=$this->fetch('error-h1')?></h1>
</section>
<section class="container bg-white">
    <header>
        <h2 class="h4"><?=$this->fetch('error-h2')?></h2>
    </header>
    <p><?=$this->fetch('error-p')?></p>
    <?=$this->element('Error/report-table')?>
</section>

<?php if (Configure::read('debug')) : ?>
    <?=$this->element('Error/section-error')?>
    <?=$this->element('Error/stack-trace')?>
<?php endif;