<?php

/**
 * @var Versae\View\AppView $this
 */

?>
<section class="container bg-white">
    <header>
        <h2 class="h4"><?=__("Stack trace")?></h2>
    </header>
    <div class="error-nav">
        <?= $this->element('exception_stack_trace_nav') ?>
    </div>
    <div class="error-contents">
        <?php if ($this->fetch('subheading')) : ?>
            <p class="error-subheading">
                <?= $this->fetch('subheading') ?>
            </p>
        <?php endif; ?>

        <?= $this->element('exception_stack_trace'); ?>

        <div class="error-suggestion">
            <?= $this->fetch('file') ?>
        </div>
    </div>
</section>
<script>
    function bindEvent(selector, eventName, listener) {
        var els = document.querySelectorAll(selector);
        for (var i = 0, len = els.length; i < len; i++) {
            els[i].addEventListener(eventName, listener, false);
        }
    }

    function toggleElement(el) {
        if (el.style.display === 'none') {
            el.style.display = 'block';
        } else {
            el.style.display = 'none';
        }
    }

    function each(els, cb) {
        var i, len;
        for (i = 0, len = els.length; i < len; i++) {
            cb(els[i], i);
        }
    }

    $(function() {
        bindEvent('.stack-frame-args', 'click', function(event) {
            var target = this.dataset['target'];
            var el = document.getElementById(target);
            toggleElement(el);
            event.preventDefault();
        });

        var details = document.querySelectorAll('.stack-details');
        var frames = document.querySelectorAll('.stack-frame');
        bindEvent('.stack-frame a', 'click', function(event) {
            each(frames, function(el) {
                el.classList.remove('active');
            });
            this.parentNode.classList.add('active');

            each(details, function(el) {
                el.style.display = 'none';
            });

            var target = document.getElementById(this.dataset['target']);
            toggleElement(target);
            event.preventDefault();
        });

        bindEvent('.toggle-vendor-frames', 'click', function(event) {
            each(frames, function(el) {
                if (el.classList.contains('vendor-frame')) {
                    toggleElement(el);
                }
            });
            event.preventDefault();
        });
    });

    $('li.stack-frame').click(function(event) {
        $('.error-contents-clone').remove();
        if ($(this).hasClass('active')) {
            var c = $(".error-contents").clone()
                .addClass('error-contents-clone')
                .css({position: 'absolute', top: event.pageY + 10 +"px", left: event.pageX + 10 +"px"});

            var close = $('<a href="#" class="close">&nbsp;&nbsp;&nbsp;&times;</a>').click(function(event) {
                event.preventDefault();
                c.remove();
            });
            c.find('.stack-frame-file').append(close);
            c.find('.toggle-link').remove();
            $('body').append(c);
        } else {
            $(this).off('hover');
        }
    });
</script>
