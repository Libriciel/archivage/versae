<?php

/**
 * @var Versae\View\AppView $this
 */

$host = $this->getRequest()->getEnv('HTTP_HOST', '');
$actualLink = ($this->getRequest()->getEnv('HTTPS', 'off') === 'on' ? "https" : "http") . "://" . $host;
?>
<table class="table report-table">
    <tbody>
        <tr>
            <th><?=__("Previous page")?></th>
            <td><?=$this->getRequest()->referer() === '/' ? $actualLink : $this->getRequest()->referer()?></td>
        </tr>
        <tr>
            <th><?=__("Request Target")?></th>
            <td><?=trim($actualLink . $this->getRequest()->getRequestTarget(), '/')?></td>
        </tr>
        <tr>
            <th><?=__("Method")?></th>
            <td><?=$this->getRequest()->getMethod()?></td>
        </tr>
        <tr>
            <th><?=__("Timestamp")?></th>
            <td><?=date('Y-m-d h:i:s', time())?></td>
        </tr>
    </tbody>
</table>
<div>
    <button type="button" class="btn btn-primary copy-error-table"><?=__("Copy the report to Clipboard")?></button>
</div>
<!--suppress JSDeprecatedSymbols -->
<script>
    $('.copy-error-table').off().click(function() {
        var div = $(this).parent();
        var headers = div.parent().parent().find('h1:first, h2:first');
        var table = div.siblings('table');
        var reportText = '';
        headers.each(function() {
            reportText += $(this).prop("tagName") + ': ' + $(this).text().trim() + "\n";
        });
        table.find('tr').each(function() {
            reportText += $(this).find('th').text() + ': ' + $(this).find('td').text() + "\n";
        });
        var dummy = $('<textarea id="dummy"></textarea>').val(reportText);
        div.append(dummy);
        dummy.get(0).select();
        document.execCommand("copy");
        dummy.remove();
    });
</script>
