<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Core\Configure;
use Cake\Utility\Hash;

$appName = ucfirst(Configure::read('App.name'));

echo $this->Html->tag(
    'p',
    __(
        "La création de votre compte utilisateur sur {0} demande la "
        . "saisie de données personnelles vous concernant. Vous trouverez ci-dessous"
        . " le détail des informations collectées ainsi que les traitements effectués."
        . " Ces données sont collectées uniquement dans le cadre de votre activité "
        . "professionnelle et pour l'utilisation exclusive de l'application {0}.",
        $appName
    )
);
echo $this->Html->tag(
    'p',
    __(
        "Le fonctionnement dans les conditions nominales et recommandées de l'application, "
        . "ne permet pas de partager ou de diffuser à un tiers vos informations personnelles."
        . " Vous pouvez demander à tout moment à l'administrateur de la plateforme de vous"
        . " communiquer l'ensemble des informations collectées pour la création de votre compte."
    )
);
echo '<hr>';

// -----------------------------------------------------------------------------
echo $this->Html->tag('h2', __("Article 1 - Mentions légales"));
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
echo $this->Html->tag('h3', __("1.1 Logiciel (ci-après « le logiciel ») :"));
echo $this->Html->tag('p.m-4', sprintf('%s %s', $appName, VERSAE_VERSION_LONG));

// -----------------------------------------------------------------------------
echo $this->Html->tag('h3', __("1.2 Éditeur (ci-après « l'éditeur ») :"));
echo $this->Html->tag('p.m-4', VENDOR_NAME);
echo $this->Html->tag('p.m-4', VENDOR_ADDRESS);
echo $this->Html->tag('p.m-4', __("Numéro SIRET : {0}", VENDOR_SIRET));

// -----------------------------------------------------------------------------
echo $this->Html->tag('h2', __("Article 2 - Hébergement"));
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
echo $this->Html->tag('h3', __("2.1 Hébergeur / maintenance du serveur :"));
echo $this->Html->tag(
    'p.m-4',
    __("Raison sociale : {0}", h(Hash::get($rgpdData, 'host.org_name')))
);
echo $this->Html->tag(
    'p.m-4',
    __("Adresse : {0}", h(Hash::get($rgpdData, 'host.org_address')))
);
echo $this->Html->tag(
    'p.m-4',
    __("Numéro SIRET : {0}", h(Hash::get($rgpdData, 'host.org_siret')))
);
echo $this->Html->tag(
    'p.m-4',
    __("Code APE : {0}", h(Hash::get($rgpdData, 'host.org_ape')))
);
echo $this->Html->tag(
    'p.m-4',
    __("N° de téléphone : {0}", h(Hash::get($rgpdData, 'host.org_phone')))
);
echo $this->Html->tag(
    'p.m-4',
    __("E-mail : {0}", h(Hash::get($rgpdData, 'host.org_email')))
);
echo $this->Html->tag(
    'p.m-4',
    h(Hash::get($rgpdData, 'host.comment'))
);
echo $this->Html->tag('h3', __("Responsable de l'hébergement / de la maintenance :"));
echo $this->Html->tag(
    'p.m-4',
    __(
        "{0} en qualité de {1}",
        h(Hash::get($rgpdData, 'host.ceo_name')),
        h(Hash::get($rgpdData, 'host.ceo_function'))
    )
);

// -----------------------------------------------------------------------------
echo $this->Html->tag('h3', __("2.2 Responsable des traitements :"));
echo $this->Html->tag(
    'p.m-4',
    __("Raison sociale : {0}", h(Hash::get($rgpdData, 'entity.org_name')))
);
echo $this->Html->tag(
    'p.m-4',
    __("Adresse : {0}", h(Hash::get($rgpdData, 'entity.org_address')))
);
echo $this->Html->tag(
    'p.m-4',
    __("Numéro SIRET : {0}", h(Hash::get($rgpdData, 'entity.org_siret')))
);
echo $this->Html->tag(
    'p.m-4',
    __("Code APE : {0}", h(Hash::get($rgpdData, 'entity.org_ape')))
);
echo $this->Html->tag(
    'p.m-4',
    __("N° de téléphone : {0}", h(Hash::get($rgpdData, 'entity.org_phone')))
);
echo $this->Html->tag(
    'p.m-4',
    __("E-mail : {0}", h(Hash::get($rgpdData, 'entity.org_email')))
);
echo $this->Html->tag(
    'p.m-4',
    h(Hash::get($rgpdData, 'entity.comment'))
);
echo $this->Html->tag('h3', __("Responsable de la structure / Responsable des traitements :"));
echo $this->Html->tag(
    'p.m-4',
    __(
        "{0} en qualité de {1}",
        h(Hash::get($rgpdData, 'entity.ceo_name')),
        h(Hash::get($rgpdData, 'entity.ceo_function'))
    )
);
echo $this->Html->tag('h3', __("Délégué à la protection des données (DPD / DPO) :"));
echo $this->Html->tag(
    'p.m-4',
    __(
        "Vous pouvez contacter votre délégué à la protection des données"
        . " à l'adresse mail suivante :<br>{0} {1}",
        h(Hash::get($rgpdData, 'entity.dpo_name')),
        h(Hash::get($rgpdData, 'entity.dpo_email'))
    )
);

// -----------------------------------------------------------------------------
echo $this->Html->tag('h2', __("Article 3 - Collecte"));
// -----------------------------------------------------------------------------

echo $this->Html->tag('p.m-4');
echo __("Lors de la création de votre compte utilisateur, les informations suivantes sont collectées :");
echo $this->Html->tag(
    'ul',
    $this->Html->tag('li', __("Identifiant de connexion"))
    . $this->Html->tag('li', __("Nom d'utilisateur"))
    . $this->Html->tag('li', __("Adresse de messagerie électronique professionnelle (courriel)"))
);
echo $this->Html->tag('/p');

// -----------------------------------------------------------------------------
echo $this->Html->tag('h2', __("Article 4 - Traitements"));
// -----------------------------------------------------------------------------

echo $this->Html->tag('p.m-4');
echo __(
    "Vos informations personnelles sont utilisées pour le bon "
    . "fonctionnement de {0} dans les cas listés ci-après :",
    $appName
);
echo $this->Html->tag(
    'ul',
    $this->Html->tag(
        'li',
        __("Affichage de vos données personnelles :")
        . $this->Html->tag(
            'ul',
            $this->Html->tag(
                'li',
                __(
                    "lister les utilisateurs de l'application :"
                    . " votre identifiant de connexion, votre nom d'utilisateur et"
                    . " votre courriel apparaissent dans la liste"
                )
            )
            . $this->Html->tag(
                'li',
                __(
                    "vue détaillée de votre compte utilisateur :"
                    . " votre identifiant de connexion, nom d'utilisateur et courriel"
                    . " apparaissent dans la vue détaillée de votre compte utilisateur"
                )
            )
        )
    )
    . $this->Html->tag(
        'li',
        __("Suite à l'envoi de courriels par {0} :", $appName)
        . $this->Html->tag(
            'ul',
            $this->Html->tag(
                'li',
                __(
                    "votre identifiant de connexion, votre nom d'utilisateur"
                    . " et votre courriel apparaissent dans ce courriel de notification"
                    . " de création de votre compte utilisateur"
                )
            )
            . $this->Html->tag(
                'li',
                __(
                    "votre identifiant de connexion et votre courriel"
                    . " apparaissent dans le courriel de notification de modification"
                    . " de votre mot de passe"
                )
            )
        )
    )
);
echo $this->Html->tag('/p');

// -----------------------------------------------------------------------------
echo $this->Html->tag('h2', __("Article 5 - Cookies"));
// -----------------------------------------------------------------------------

echo $this->Html->tag('h3', __("5.1 Cookies strictement nécessaires :"));
echo $this->Html->tag(
    'p.m-4',
    __(
        "L'application utilise des cookies afin de sauvegarder temporairement,"
        . " le temps de la session, les préférences utilisateurs."
    )
);

// -----------------------------------------------------------------------------
echo $this->Html->tag('h2', __("Article 6 - Droit à l'oubli"));
// -----------------------------------------------------------------------------

echo $this->Html->tag(
    'p.m-4',
    __(
        "Sur votre demande ou à la demande de l’administrateur de {0},"
        . " votre compte utilisateur peut être supprimé définitivement.",
        $appName
    )
);

// -----------------------------------------------------------------------------
echo $this->Html->tag('h2', __("Article 7 - Vos droits sur les données vous concernant"));
// -----------------------------------------------------------------------------

echo $this->Html->tag(
    'p.m-4',
    __(
        "Vous pouvez accéder et obtenir une copie des données vous"
        . " concernant, vous opposer au traitement de ces données, les faire"
        . " rectifier ou les faire effacer. Vous disposez également d'un droit à"
        . " la limitation du traitement de vos données."
    )
);
echo $this->Html->tag(
    'p.m-4',
    $this->Html->link(
        __("> Comprendre vos droits informatique et libertés"),
        CNIL_INFO_URL,
        ['data-mode' => 'no-ajax', 'target' => '_blank']
    )
);
echo $this->Html->tag('h3', __("Exercer ses droits"));
echo $this->Html->tag(
    'p.m-4',
    __(
        "Pour exercer vos droits sur les données personnelles vous"
        . " concernant, vous pouvez contacter le Délégué à la Protection des"
        . " Données (DPO), à l’adresse suivante :"
    )
);
echo $this->Html->tag(
    'ul',
    $this->Html->tag(
        'li',
        sprintf(
            '%s %s',
            h(Hash::get($rgpdData, 'entity.dpo_name')),
            h(Hash::get($rgpdData, 'entity.dpo_email'))
        )
    )
);
echo $this->Html->tag('h3', __("Réclamation (plainte) auprès de la CNIL"));
echo $this->Html->tag(
    'p.m-4',
    __(
        "Si vous estimez, après nous avoir contacté, que vos droits sur"
        . " vos données ne sont pas respectés, vous pouvez {0}.",
        $this->Html->link(
            __("adresser une réclamation (plainte) à la CNIL"),
            CNIL_COMPLAINT_URL,
            ['data-mode' => 'no-ajax', 'target' => '_blank']
        )
    )
);
