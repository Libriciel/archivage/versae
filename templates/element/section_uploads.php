<?php

/**
 * @var Versae\View\AppView $this
 * @var string $title Contenu du h2
 * @var AsalaeCore\View\Helper\Object\Table $uploads
 * @var string $content
 */

?>
<section class="container bg-white">
    <header>
        <h2 class="h4"><?=$title?></h2>
        <div class="r-actions h4">
            <?=$uploads->getConfigureLink()?>
        </div>
    </header>
    <div class="no-padding">
        <?=$uploads->generate()?>
    </div>
    <div class="hide">
        <button type="button"
                class="btn btn-primary"
                onclick="GenericUploader.addSelected(<?=$uploads->table->tableObject?>)">
            <i aria-hidden="true" class="fa fa-level-down"></i> <?=__("Ajouter sélection")?>
        </button>
    </div>
</section>
