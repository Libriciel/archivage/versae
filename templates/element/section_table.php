<?php

/**
 * @var Versae\View\AppView $this
 * @var string $id identifiant de la section
 * @var string $title Contenu du h2
 * @var AsalaeCore\View\Helper\Object\Table $table
 */

echo $this->Html->tag(
    'section',
    null,
    ['id' => $id, 'class' => 'container bg-white paginable-section']
);

/**
 * header
 */
echo $this->Html->tag('header');
echo $this->Html->tag('h4', $title);
echo $this->Html->tag('div', null, ['class' => 'l-actions h4']);
echo $this->Fa->charteBtn(
    'Filtrer',
    __("Réinitialiser les filtres de recherche"),
    [
        'class' => 'btn-link display-active-filter',
        'text' => $this->Fa->i('fa-times fa-small-append fa-hidden-append text-danger')
    ]
);
echo $this->Fa->button(
    'fa-sort-amount-asc',
    __("Retirer le tri"),
    [
        'class' => 'btn-link display-active-sort',
        'text' => $this->Fa->i('fa-times fa-small-append fa-hidden-append text-danger')
    ]
);
echo $this->Html->tag('/div');
echo $this->Html->tag('div', $table->getConfigureLink(), ['class' => 'r-actions h4']);
echo $this->Html->tag('/header');

/**
 * Body
 */
echo $table->generate();

echo $this->Html->tag('div.row.d-flex');
echo $this->Paginator->blocPagination(
    $pagination['base-id'] ?? 'grouped-actions',
    $pagination['options'] ?? []
);
echo $this->Html->tag('/div');

echo $this->Html->tag('/section');

if (!empty($pagination['callback'])) {
    echo $this->Html->tag('script', '$(function() { ' . $pagination['callback'] . '(); });');
}
