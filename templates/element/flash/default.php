<?php

/**
 * @var array $params
 */

$class = 'alert alert-info';
if (!empty($params['class'])) {
    $class .= ' ' . $params['class'];
}
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<section class="container container-flash">
    <div class="<?= h($class) ?>"><?= $message ?></div>
</section>
