<?php
/**
 * @var array $params
 */
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<section class="container container-flash">
    <div class="alert alert-danger"><?= $message ?></div>
</section>
