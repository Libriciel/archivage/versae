<?php

/**
 * @var Versae\View\AppView $this
 */

?>
<script>
    function getCurrentId(id) {
        var multiple = $('#edit-form-keyword-multiple-with').val();
        return id+(multiple ? '/'+multiple : '');
    }
</script>
<?php
echo $this->Form->create(
    $entity,
    [
        'idPrefix' => 'edit-form-keyword',
        'id' => 'edit-form-keyword',
    ]
);
echo $this->Form->control(
    'name',
    [
        'label' => __("Nom du mot clé"),
        'help' => __("Le KeywordContent sera initialisé avec cette valeur (vous pourrez le modifier ensuite)"),
    ]
);
echo $this->Form->control(
    'multiple_with',
    [
        'label' => __("Ce mot-clé sera à valeurs multiples"),
        'empty' => __("Non"),
        'help' => __(
            "Créera un mot clé pour chaque valeur de la variable multiple "
            . "sélectionnée.<br>Cette valeur est accessible dans le twig : {0}",
            '{{multiple.value}}'
        ),
        'options' => $fields_multiples,
    ]
);

$jsTableVariables = $this->Table->getJsTableObject($jsTableKeywordId = 'edit-form-unit-keyword-details-table');

$tableKeywords = $this->Table
    ->create($jsTableKeywordId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'name' => [
                'label' => __("Nom"),
            ],
            'form_variable.twig' => [
                'label' => __("Twig"),
                'callback' => 'twigForm',
                'class' => 'twig',
            ],
        ]
    )
    ->data($keywordDetails)
    ->params(
        [
            'identifier' => 'name',
            'checkbox' => false,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionAddEditKeywordDetail('$id/'+getCurrentId('{0}'))",
                'type' => 'button',
                'class' => 'btn-link text-success',
                'label' => $this->Fa->charte('Ajouter'),
                'title' => $title = __("Ajouter {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/form-calculators/add-edit-keyword-detail'),
                'displayEval' => 'data[{index}].id === null',
                'params' => ['name', 'label']
            ],
            [
                'onclick' => "actionAddEditKeywordDetail('$id/'+getCurrentId('{0}'))",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/form-calculators/add-edit-keyword-detail'),
                'displayEval' => 'data[{index}].id !== null',
                'params' => ['id', 'label']
            ],
            [
                'data-callback' => sprintf(
                    "actionDeleteKeywordDetail(%s, '%s')('{0}', '{2}')",
                    $jsTableVariables,
                    $this->Url->build('/form-calculators/delete-keyword-detail')
                ),
                'type' => 'button',
                'class' => 'btn-link delete',
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'confirm' => __("Supprimer cette variable ?"),
                'display' => $this->Acl->check('/form-calculators/delete-keyword-detail'),
                'displayEval' => 'data[{index}].id !== null && data[{index}].name !== "KeywordContent"',
                'params' => ['id', 'label', 'name']
            ],
        ]
    );

echo $this->Html->tag(
    'article',
    $this->Html->tag(
        'header',
        $this->Html->tag('h2.h4', __("Contenu du mot clé"))
    )
    . $tableKeywords->generate()
);

echo $this->Form->end();
