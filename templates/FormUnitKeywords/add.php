<?php

/**
 * @var Versae\View\AppView $this
 */

$searchId = 'add-form-keyword-search';

echo $this->MultiStepForm->template('MultiStepForm/add_keyword')->step(1) . '<hr>';

echo $this->Form->create(
    $entity,
    [
        'idPrefix' => 'add-form-keyword',
        'id' => 'add-form-keyword',
    ]
);
echo $this->Form->control(
    'name',
    [
        'label' => __("Nom du mot clé"),
        'class' => 'keyword-name',
        'help' => __("Le KeywordContent sera initialisé avec cette valeur (vous pourrez le modifier ensuite)"),
    ]
);
echo $this->Form->control(
    'multiple_with',
    [
        'label' => __("Ce mot-clé sera à valeurs multiples"),
        'empty' => __("Non"),
        'help' => __(
            "Créera un mot clé pour chaque valeur de la variable multiple "
            . "sélectionnée.<br>Cette valeur est accessible dans le twig : {0}",
            '{{multiple.value}}'
        ),
        'options' => $fields_multiples,
    ]
);

echo $this->Html->tag(
    'span',
    $this->Form->control(
        'reference',
        [
            'label' => __("Référence du mot clé"), // code
            'class' => 'keyword-reference',
        ]
    )
    . $this->Form->control(
        'type',
        [
            'label' => __("Type du mot clé"),
            'class' => 'keyword-type',
            'empty' => __("-- Type --"),
            'options' => $types,
        ]
    )
    . $this->Form->control(
        'search',
        [
            'label' => __("Rechercher un mot clé"),
            'class' => 'keyword-search',
            'options' => [],
            'empty' => true,
            'data-placeholder' => __("-- Sélectionner un mot clé -- "),
            'help' => '<button type="button" onclick="loadKeywordData(this)" class="btn btn-success">'
                . '<i class="fa fa-check-square-o" aria-hidden="true"></i> '
                . __("Sélectionner")
                . '</button>'
        ]
    ),
    ['id' => 'keyword-additional-data']
);

echo $this->Form->end();
?>
<script>
    function ajaxSearchKeyword(element) {
        $(element).select2({
            ajax: {
                url: '<?=$this->Url->build('/Keywords/populateSelect')?>',
                method: 'POST',
                delay: 250
            },
            escapeMarkup: function (v) {
                return $('<span></span>').html(v).text();
            },
            dropdownParent: $(element).closest('.modal'),
            allowClear: true
        });
        var button = $(element).siblings('.help-block').find('button').hide();
        $(element).on('change', function () {
            var actualValue = $('.keyword-name').val();
            if (!actualValue) {
                button.click();
            } else {
                button.show();
            }
        });
    }

    ajaxSearchKeyword('#<?=$searchId?>');

    function loadKeywordData(button) {
        var select = $(button).closest('.form-group').find('select');
        var id = select.val();
        if (!id) {
            return;
        }
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/Keywords/getData')?>/' + id,
            headers: {
                Accept: 'application/json'
            },
            success: function (content) {
                $('.keyword-name').val(content.name);
                $('.keyword-reference').val(content.code);
            }
        });
    }

    var multipleSelect = $('#add-form-keyword-multiple-with');
    multipleSelect.on('change', function () {
        if (multipleSelect.val()) {
            $('#keyword-additional-data').hide();
        } else {
            $('#keyword-additional-data').show();
        }
    });
</script>
