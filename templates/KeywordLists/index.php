<?php

/**
 * @var Versae\View\AppView $this
 */

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Administration"));
$this->Breadcrumbs->add(__("Mots-clés"));
echo $this->Html->tag(
    'div.container',
    $this->Html->tag('h1', $this->Fa->i('fa-key', __("Mots-clés")))
    . $this->Breadcrumbs->render()
);

$jsTable = $this->Table->getJsTableObject($tableId);
$buttons = [];
if ($this->Acl->check('/KeywordLists/add')) {
    $buttons[] = $this->ModalForm
        ->create('add-keyword-lists')
        ->modal(__("Ajout d'une liste de mots clés"))
        ->javascriptCallback('TableGenericAction.afterAdd(' . $jsTable . ', "KeywordLists")')
        ->output(
            'button',
            $this->Fa->charte('Ajouter', __("Ajouter une liste de mots clés")),
            '/KeywordLists/add'
        )
        ->generate(['class' => 'btn btn-success', 'type' => 'button']);
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}

echo $this->element('modal', ['idTable' => $tableId]);

echo $this->ModalForm
    ->create('edit-keyword-list')
    ->modal(__("Modifier une liste de mots clés"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTable . ', "KeywordLists")')
    ->output(
        'function',
        'actionEditKeywordList',
        '/KeywordLists/edit'
    )
    ->generate();

echo $this->ModalView
    ->create('view-keyword-list')
    ->modal(__("Visualiser une liste de mots clés"))
    ->output(
        'function',
        'actionViewKeywordList',
        '/KeywordLists/view'
    )
    ->generate();

echo $this->Filter->create('keyword-lists-filter')
    ->saves($savedFilters)
    ->filter(
        'name',
        [
            'label' => __("Nom"),
            'wildcard',
        ]
    )
    ->filter(
        'identifier',
        [
            'label' => __("Identifiant"),
            'wildcard',
        ]
    )
    ->filter(
        'active',
        [
            'label' => __("Actifs"),
            'options' => [
                '1' => __("Oui"),
                '0' => __("Non"),
            ]
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked'
        ]
    )
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date'
        ]
    )
    ->filter(
        'modified',
        [
            'label' => __("Date de modification"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date'
        ]
    )
    ->filter(
        'version[value]',
        [
            'label' => __("Version"),
            'prepend' => $this->Input->operator(
                'version[operator]',
                '>=',
                ['id' => 'version-operator']
            ),
            'class' => 'with-select',
            'type' => 'number',
            'min' => 0,
        ]
    )
    ->filter(
        'count[value]',
        [
            'label' => __("Nombre mots-clés"),
            'prepend' => $this->Input->operator(
                'count[operator]',
                '>=',
                ['id' => 'count-operator']
            ),
            'class' => 'with-select',
            'type' => 'number',
            'min' => 0,
        ]
    )
    ->generateSection();

require 'ajax_index.php';

?>
<script>
    $(function() {
        AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');
    });
</script>
