<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->Form->create($entity, ['idPrefix' => 'add-keyword-list']);
require 'addedit-common.php';
echo $this->Form->end();
