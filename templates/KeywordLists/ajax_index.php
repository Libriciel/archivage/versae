<?php

/**
 * @var Versae\View\AppView $this
 */

$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'KeywordLists.name' => [
                'label' => __("Nom"),
                'order' => 'name',
                'filter' => [
                    'name[0]' => [
                        'id' => 'filter-name-0',
                        'label' => false,
                        'aria-label' => __("Nom"),
                    ],
                ],
            ],
            'KeywordLists.identifier' => [
                'label' => __("Identifiant"),
                'order' => 'identifier',
                'filter' => [
                    'identifier[0]' => [
                        'id' => 'filter-identifier-0',
                        'label' => false,
                        'aria-label' => __("Identifiant"),
                    ],
                ],
            ],
            'KeywordLists.active' => [
                'label'    => __("Actif"),
                'type'     => 'boolean',
                'callback' => 'TableHelper.boolean',
                'filter' => [
                    'active[0]' => [
                        'id' => 'filter-active-0',
                        'label' => false,
                        'aria-label' => __("Actif"),
                        'options' => [
                            '1' => __("Oui"),
                            '0' => __("Non"),
                        ]
                    ],
                ],
            ],
            'KeywordLists.version' => [
                'label' => __("Version"),
                'order' => 'version',
                'display' => false,
                'filter' => [
                    'version[0][value]' => [
                        'id' => 'filter-version-0',
                        'label' => __("Nombre de fichiers"),
                        'prepend' => $this->Input->operator(
                            'version[0][operator]',
                            '>=',
                            ['id' => 'filter-version-0-operator']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                    'version[1][value]' => [
                        'id' => 'filter-version-1',
                        'label' => false,
                        'aria-label' => __("Nombre 2"),
                        'prepend' => $this->Input->operator(
                            'version[1][operator]',
                            '<=',
                            ['id' => 'filter-version-1-operator']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                ]
            ],
            'KeywordLists.count' => [
                'label' => __("Nombre mots-clés"),
                'order' => 'count',
                'display' => false,
                'filter' => [
                    'count[0][value]' => [
                        'id' => 'filter-count-0',
                        'label' => __("Nombre de fichiers"),
                        'prepend' => $this->Input->operator(
                            'count[0][operator]',
                            '>=',
                            ['id' => 'filter-count-0-operator']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                    'count[1][value]' => [
                        'id' => 'filter-count-1',
                        'label' => false,
                        'aria-label' => __("Nombre 2"),
                        'prepend' => $this->Input->operator(
                            'count[1][operator]',
                            '<=',
                            ['id' => 'filter-count-1-operator']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                ]
            ],
            'KeywordLists.created' => [
                'label'   => __("Date de création"),
                'type'    => 'datetime',
                'display' => false,
                'order'   => 'created',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'created[0]' => [
                        'id' => 'filter-created-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator('dateoperator_created[0]', '>='),
                        'append' => $this->Date->picker('#filter-created-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'created[1]' => [
                        'id' => 'filter-created-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_created[1]', '<='),
                        'append' => $this->Date->picker('#filter-created-1'),
                        'class' => 'datepicker with-select',
                    ],
                ]
            ],
            'KeywordLists.modified' => [
                'label'   => __("Date de modification"),
                'type'    => 'datetime',
                'display' => false,
                'order'   => 'modified',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'modified[0]' => [
                        'id' => 'filter-modified-0',
                        'label' => __("Date de modification"),
                        'prepend' => $this->Input->operator('dateoperator_modified[0]', '>='),
                        'append' => $this->Date->picker('#filter-modified-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'modified[1]' => [
                        'id' => 'filter-modified-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_modified[1]', '<='),
                        'append' => $this->Date->picker('#filter-modified-1'),
                        'class' => 'datepicker with-select',
                    ],
                ]
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'KeywordLists.id',
            'favorites' => true,
            'sortable' => true,
        ]
    )
    ->actions(
        [
            [
                'href' => $this->Url->build("/Keywords/index/{0}"),
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-cog'),
                'title' => __("Gérer les mots-clés de la liste {0}", '{1}'),
                'aria-label' => __("Gérer les mots-clés de la liste {0}", '{1}'),
                'display' => $this->Acl->check('keywords/index'),
                'params' => ['KeywordLists.id', 'KeywordLists.name']
            ],
            [
                'onclick' => "actionViewKeywordList({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => __("Visualiser {0}", '{1}'),
                'aria-label' => __("Visualiser {0}", '{1}'),
                'display' => $this->Acl->check('keyword-lists/view'),
                'params' => ['KeywordLists.id', 'KeywordLists.name']
            ],
            [
                'onclick' => "actionEditKeywordList({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'display' => $this->Acl->check('keyword-lists/edit'),
                'params' => ['KeywordLists.id', 'KeywordLists.name']
            ],
            function ($table) {
                /** @var Versae\View\AppView $this */
                $deleteUrl = $this->Url->build('/KeywordLists/delete');
                return [
                    'onclick' => "TableGenericAction.deleteAction($table->tableObject, '$deleteUrl')({0})",
                    'type' => 'button',
                    'class' => 'btn-link',
                    'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'display' => $this->Acl->check('keyword-lists/delete'),
                    'params' => ['KeywordLists.id', 'KeywordLists.name']
                ];
            },
        ]
    );

echo $this->element(
    'section_table',
    [
        'id' => 'keyword-lists-section',
        'title' => __("Liste des listes de mots-clés"),
        'table' => $table,
    ]
);
