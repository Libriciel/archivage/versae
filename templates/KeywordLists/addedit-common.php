<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->Form->control('identifier', ['label' => __("Identifiant")])
    . $this->Form->control('name', ['label' => __("Nom")])
    . $this->Form->control('description', ['label' => __("Description")])
    . $this->Form->control('active', ['label' => __("Actif"), 'type' => 'checkbox']);
