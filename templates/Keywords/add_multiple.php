<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->Form->create(null, ['idPrefix' => 'add-multiple-keyword']);

echo $this->Html->tag('table', null, ['class' => 'table table table-striped table-hover']);

echo $this->Html->tag('thead');
echo $this->Html->tableHeaders(
    [
        __("Code"),
        __("Nom"),
    ]
);
echo $this->Html->tag('/thead');

echo $this->Html->tag('tbody');
$count = count($entities);
for ($i = 0; $i <= $count; $i++) {
    $entity = $entities[$i] ?? null;
    $this->Form->create($entity);
    echo $this->Html->tableCells(
        [
            $this->Form->control(
                'code',
                [
                    'label' => false, 'class' => 'code multiple-input-gen',
                    'required' => false,
                    'id' => 'add-multiple-keyword-' . $i . '-code',
                    'name' => $i . '[code]',
                    'value' => $this->getRequest()->getData($i . '.code')
                ]
            ),
            $this->Form->control(
                'name',
                [
                    'label' => false, 'class' => 'name multiple-input-gen',
                    'required' => false,
                    'id' => 'add-multiple-keyword-' . $i . '-name',
                    'name' => $i . '[name]',
                    'value' => $this->getRequest()->getData($i . '.name'),
                    'maxlength' => 255,
                ]
            )
        ]
    );
    echo $this->Form->end();
}
echo $this->Html->tag('/tbody');

echo $this->Html->tag('/table');
echo $this->Form->end();
?>
<script>
    function multipleInputHandler(event) {
        if (event.originalEvent.key.length > 1) {
            return;
        }
        var idMatch = /add-multiple-keyword-(\d+)-(\w+)/;
        var nameMatch = /(\d+)\[(\w+)]/;
        var value = $(this).val(),
            cssClass = $(this).hasClass('code') ? 'code' : 'name',
            empty = false,
            trs,
            i,
            j,
            tbody = $(this).closest('tbody');
        var emptyTrs = tbody.find('tr').filter(function() {
            var empty = false;
            $(this).find('input.name').each(function() {
                if (!$(this).val()) {
                    empty = true;
                    return false;
                }
            })
            return empty;
        });

        // On ne garde que le 1er lot d'input vide et on le place en dernier
        if (!value && !$(this).closest('tr').find('input').not('.'+cssClass).val()) {
            if (emptyTrs.length > 1) {
                for (i = 1; i < emptyTrs.length; i++) {
                    emptyTrs[i].remove();
                }
            }
            tbody.append(emptyTrs[0]);
        }

        // On restaure les ids/names avec les numéros dans le bon ordre
        trs = tbody.find('tr');
        for (i = 0; i < trs.length; i++) {
            $(trs[i]).find('input').each(function() {
                var match = $(this).attr('id').match(idMatch);
                if (match && parseInt(match[1], 10) !== i) {
                    $(this).attr('name', i+'['+match[2]+']')
                        .attr('id', 'add-multiple-keyword-'+i+'-'+match[2])
                }
            });
        }

        // Si il n'y a plus de lots vide Disponible, on en créer un nouveau
        if (emptyTrs.length === 0) {
            var tr = $(trs[trs.length-1]).clone();
            tr.find('input').each(function() {
                var match = $(this).attr('id').match(idMatch);
                if (match) {
                    $(this).attr('name', trs.length+'['+match[2]+']')
                        .attr('id', 'add-multiple-keyword-'+trs.length+'-'+match[2])
                        .val('')
                        .keyup(multipleInputHandler);
                }
            });
            tbody.append(tr);
        }
    }
    setTimeout(function() {
        $('.multiple-input-gen').keyup(multipleInputHandler).first().focus();
    }, 400);
</script>
