<?php

/**
 * @var Versae\View\AppView $this
 * @var Versae\Model\Entity\KeywordList $list
 */

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Administration"));
$this->Breadcrumbs->add(__("Mots-clés"), '/KeywordLists/index');
$this->Breadcrumbs->add(h($list->get('name')));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag(
        'h1',
        $this->Fa->i('fa-key', __("Mots-clés de la liste: {0}", h($list->get('name'))))
    )
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

$jsTable = $this->Table->getJsTableObject($tableId);
// Actions
$buttons = [];
$workspace = ($count === 0 || ($data && current($data)->get('version') === 0));
if (($existingWorkspace && $workspace) || !$existingWorkspace) {
    if ($workspace) {
        echo $this->Html->tag(
            'div',
            $this->Html->tag(
                'p',
                __("Cette liste est une version de travail"),
                ['class' => 'alert alert-warning']
            ),
            ['class' => 'container']
        );
        if ($this->Acl->check('/Keywords/add')) {
            $buttons[] = $this->ModalForm
                ->create('add-keyword')
                ->modal(__("Ajout d'un mot clé"))
                ->javascriptCallback('afterAddKeyword(' . $jsTable . ', "Keywords")')
                ->output(
                    'button',
                    $this->Fa->charte('Ajouter', __("Ajouter un mot clé")),
                    '/Keywords/add/' . $list->get('id')
                )
                ->generate(['class' => 'btn btn-success', 'type' => 'button']);
            $buttons[] = $this->ModalForm
                ->create('add-multiple-keyword')
                ->modal(__("Ajout d'un ensemble de mots-clés"))
                ->javascriptCallback('afterAddMultipleKeys(' . $jsTable . ', "Keywords")')
                ->output(
                    'button',
                    $this->Fa->charte('Ajouter', __("Ajouter un ensemble de mots-clés")),
                    '/Keywords/add-multiple/' . $list->get('id')
                )
                ->generate(['class' => 'btn btn-success', 'type' => 'button']);
        }
        if ($this->Acl->check('/Keywords/import')) {
            $buttons[] = $this->ModalForm
                ->create('import-keyword')
                ->modal(__("Import d'un ensemble de mots-clés"))
                ->javascriptCallback('afterImport')
                ->output(
                    'button',
                    $this->Fa->i('fa-upload', __("Importer")),
                    '/Keywords/import/' . $list->get('id')
                )
                ->generate(['class' => 'btn btn-success', 'type' => 'button']);
        }
        $buttons[] = $this->Acl->link(
            $this->Fa->i('fa-globe', __("Publier la version de travail")),
            '/KeywordLists/publishVersion/' . $list->get('id'),
            ['class' => 'btn btn-warning', 'escape' => false]
        );
        $buttons[] = $this->Acl->link(
            $this->Fa->charte('Supprimer', __("Supprimer la version de travail")),
            '/KeywordLists/removeVersion/' . $list->get('id'),
            [
                'class'   => 'btn btn-danger',
                'escape'  => false,
                'confirm' => __(
                    "Cette action va supprimer toutes les modifications
                apportées à la liste depuis la dernière version. Voulez-vous continuer ?"
                ),
            ]
        );
    } else {
        $buttons[] = $this->Acl->link(
            $this->Fa->i('fa-code-fork', __("Créer une nouvelle version de travail")),
            '/KeywordLists/newVersion/' . $list->get('id'),
            ['class' => 'btn btn-warning', 'escape' => false, 'data-mode' => 'no-ajax'],
            'div'
        );
    }
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}

$options = [];
if ($existingWorkspace) {
    $options[0] = __("Version de travail");
}

for ($i = 1; $i <= $list->get('version'); $i++) {
    $options[$i] = __("Version {0}", $i);
}
echo $this->Html->tag('div', null, ['class' => 'container']);
echo $this->Form->control(
    'choice',
    [
        'onchange' => "AsalaeGlobal.interceptedLinkToAjax('"
        . $this->Url->build('/Keywords/index/' . $list->get('id')) . "/'+$(this).val())",
        'label' => __("Sélectionner une version"),
        'options' => $options,
        'default' => $version
    ]
);
echo $this->Html->tag('/div');

echo $this->element('modal', ['idTable' => $tableId]);

echo $this->ModalForm
    ->create('edit-keyword-list')
    ->modal(__("Modification d'un mot clé"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTable . ', "Keywords")')
    ->output(
        'function',
        'actionEditKeyword',
        '/Keywords/edit'
    )
    ->generate();

echo $this->ModalView
    ->create('view-keyword-list')
    ->modal(__("Visualisation d'un mot clé"))
    ->output(
        'function',
        'actionViewKeyword',
        '/Keywords/view'
    )
    ->generate();

echo $this->Filter->create('keyword-filter')
      ->saves($savedFilters)
    ->filter(
        'name',
        [
            'label' => __("Nom"),
            'wildcard',
        ]
    )
    ->filter(
        'code',
        [
            'label' => __("Code"),
            'wildcard',
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked'
        ]
    )
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date'
        ]
    )
    ->filter(
        'modified',
        [
            'label' => __("Date de modification"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date'
        ]
    )
      ->generateSection();

require 'ajax_index.php';

echo $this->Html->tag('div.container');
echo $this->Html->link(
    $this->Fa->i('fa-arrow-left', __("Retour aux listes de mots-clés")),
    $this->Url->build('/keyword-lists'),
    ['escape' => false]
);
echo $this->Html->tag('/div');

$deleteUrl = $this->Url->build('/Keywords/delete');
$deleteSelectedScript = "TableGenericAction.deleteAction($jsTable, '$deleteUrl')($(this).val(), false)";
?>
<script>
    $(function() {
        AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');
    });

    function _defineProperty(obj, key, value) {
        if (key in obj) {
            Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true });
        } else {
            obj[key] = value;
        }
        return obj;
    }
    function afterAddMultipleKeys(table, model) {
        return function(content, textStatus, jqXHR) {
            if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                for (var i = 0; i < content.length; i++) {
                    table.data.push(_defineProperty({}, model, content[i]));
                }
                TableGenerator.appendActions(table.data, table.actions);
                table.generateAll();
                removeFlashAlert();
            }
        }
    }

    /**
     * Callback d'après ajout d'un mot clef
     * @param table
     * @param model
     * @return {Function}
     */
    function afterAddKeyword(table, model) {
        return function(content, textStatus, jqXHR) {
            TableGenericAction.afterAdd(table, model)(content, textStatus, jqXHR);
            if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                removeFlashAlert();
            }
        }
    }

    /**
     * Suppression du flash disant que la liste ne peut être publiée
     */
    function removeFlashAlert() {
        $('section.container.container-flash > div.alert.alert-danger').parent().remove()
    }

    /**
     * Action de groupe sur le tableau
     */
    function updatePaginationAjaxCallback() {
        var table = $('#<?=$tableId?>');
        $('#btn-grouped-actions').off('click').click(function() {
            var visibleCheckedCheckboxes = table.find('.td-checkbox > input:checked');
            $(this).disable();
            if ($('#select-grouped-actions').val() === '1'
                && confirm(__("Êtes-vous sûr de vouloir supprimer les mots-clés sélectionnés ?"))
            ) {
                visibleCheckedCheckboxes.each(function () {
                    <?=$deleteSelectedScript?>
                });
            }
            $(this).enable();
            table.find('> thead th input.checkall').prop('checked', false);
        });
    }
    updatePaginationAjaxCallback();
    $(window).off('history.change.pagination').on('history.change.pagination', function() {
        updatePaginationAjaxCallback();
    });

    function afterImport(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            setTimeout(function() {
                AsalaeGlobal.updatePaginationAjax('#keyword-section');
                alert(__("Import de {0} mots-clés effectué avec succès", content.count));
                if (content.count) {
                    removeFlashAlert();
                }
            }, 400); // Attend la fin de l'animation de fermeture de la modale
        } else {
            alert(__("Une erreur a eu lieu lors de l'import"));
        }
    }
</script>
