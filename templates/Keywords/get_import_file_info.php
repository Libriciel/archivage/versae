<?php

/**
 * @var Versae\View\AppView $this
 */

$loading = $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);

// Pour l'idPrefix
$this->Form->create(null, ['idPrefix' => 'import-keywords']);

echo $this->Form->control(
    'fileupload_id',
    [
        'type' => 'hidden',
        'value' => $this->getRequest()->getParam('pass.1')
    ]
);

echo $this->Form->control(
    'count',
    [
        'label' => __("Nombre de mots-clés à importer"),
        'value' => $count,
        'disabled' => true
    ]
);

if ($type === 'csv') {
    echo $this->Form->control(
        'Csv.separator',
        [
            'label' => __("Séparateur de champs"),
            'default' => ',',
            'maxlength' => 1,
            'required' => true,
            'help' => __(
                "Le plus souvent une virgule (,) mais il est possible de rencontrer "
                . "des points-virgules (;), des tabulations (\t), des espaces ( ) voir autre chose (x)."
            )
        ]
    );
    echo $this->Form->control(
        'Csv.delimiter',
        [
            'label' => __("Délimiteur de texte"),
            'options' => [
                '"' => __("Guillemet (\")"),
                "'" => __("Apostrophe (')"),
            ],
            'help' => __("Délimite le texte. ex: \"mon texte\",\"un autre champ\"")
        ]
    );

    echo $this->Form->control(
        'Csv.way',
        [
            'label' => __("Disposition des données"),
            'options' => [
                'row' => __("Chaques ligne = un mot clé"),
                'col' => __("Chaques colonne = un mot clé"),
            ]
        ]
    );

    echo $this->Form->control(
        'Csv.pos_label_col',
        [
            'label' => __("Numéro de colonne pour le 1er nom (A=1, B=2, etc...)"),
            'type' => 'number',
            'min' => '1',
            'default' => 1
        ]
    );
    echo $this->Form->control(
        'Csv.pos_label_row',
        [
            'label' => __("Numéro de ligne pour le 1er nom"),
            'type' => 'number',
            'min' => '1',
            'default' => 1
        ]
    );

    echo $this->Form->control(
        'Csv.auto_code',
        [
            'label' => __("Définition automatique du code à partir du label"),
            'type' => 'checkbox'
        ]
    );

    echo $this->Html->tag('div', null, ['id' => 'pos-code']);

        echo $this->Form->control(
            'Csv.pos_code_col',
            [
                'label' => __("Numéro de colonne pour le 1er code (A=1, B=2, etc...)"),
                'type' => 'number',
                'min' => '1',
                'default' => 2
            ]
        );
        echo $this->Form->control(
            'Csv.pos_code_row',
            [
                'label' => __("Numéro de ligne pour le 1er code"),
                'type' => 'number',
                'min' => '1',
                'default' => 1
            ]
        );

    echo $this->Html->tag('/div');
}

echo $this->Html->tag(
    'button',
    $this->Fa->i('fa-calendar-check-o', __("Tester l'import")),
    ['class' => 'btn btn-success', 'type' => 'button', 'id' => 'btn-test-import']
);

echo $this->Html->tag('div', '', ['id' => 'zone-test-import']);
echo $this->Form->end();
?>
<script>
    $('#import-keywords-auto-code').change(function() {
        $('#import-keywords-pos-code').toggle(!$(this).prop('checked'));
    });
    $('#btn-test-import').click(function() {
        $('#zone-test-import').html('<?=$loading?>');
        $('#import-keyword').find('button.accept').enable();
        $.ajax({
            url: '<?=$this->Url->build(
                '/Keywords/get-import-file-info/'
                . $this->getRequest()->getParam('pass.0') . '/'
                . $this->getRequest()->getParam('pass.1')
            )?>',
            method: 'POST',
            data: $(this).closest('form').serialize(),
            success: function(content) {
                $('#zone-test-import').append(content);
            },
            error: function() {
                alert(PHP.messages.genericError);
            },
            complete: function() {
                $('#zone-test-import').find('.loading-container').remove();
            }
        });
    });
</script>
