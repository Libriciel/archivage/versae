<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->Form->control('code', ['label' => __("Code")])
    . $this->Form->control('name', ['label' => __("Nom")]);
