<?php

/**
 * @var Versae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $users
 */

use Cake\Utility\Hash;

echo $this->Html->tag('article');
echo $this->Html->tag('header.bottom-space');
echo $this->Html->tag('h3', h($entity->get('name')));
echo $this->Html->tag('/header');

echo $this->Html->tag(
    'table',
    $this->Html->tag(
        'tbody',
        $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Code"))
            . $this->Html->tag('td', h($entity->get('code')))
        )
        . $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Version"))
            . $this->Html->tag('td', h($entity->get('version')))
        )
        . $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Créé par"))
            . $this->Html->tag('td', h($users->get('name') ?: $users->get('username')))
        )
        . $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Modifié par"))
            . $this->Html->tag('td', h(Hash::get($users, 'EditUser.name') ?: Hash::get($users, 'EditUser.username')))
        )
        . $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Date de création"))
            . $this->Html->tag('td', h($entity->get('created')))
        )
        . $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Date de modification"))
            . $this->Html->tag('td', h($entity->get('modified')))
        )
    ),
    ['class' => 'table table-striped table-hover fixed-left-th']
);
echo $this->Html->tag('/article');
