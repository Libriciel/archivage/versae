<?php

/**
 * @var Versae\View\AppView $this
 */

use Versae\Model\Entity\Keyword;

?>
<script>
    $('#import-keywords-count').remove();

    function errorsCallback(content, context) {
        return content.length ? '<ul><li>'+content.join('</li><li>')+'</li></ul>' : '';
    }
</script>
<?php
echo '<hr id="test-import-hr">';

echo $this->Form->control(
    'count',
    [
        'label' => __("Nombre de mots-clés à importer"),
        'value' => $count,
        'disabled' => true
    ]
);

if ($conflicts) {
    echo $this->Html->tag('div', null, ['class' => 'alert alert-danger']);
    echo $this->Html->tag('div', __("Des collisions ont été détecté"));
    echo $this->Form->control(
        'conflicts',
        [
            'label' => __("Stratégie de correction"),
            'options' => [
                'overwrite' => __("Écraser les mots-clés du même nom/code"),
                'ignore' => __("Ne pas ajouter le mot-clé en cas de conflit"),
                'rename' => __("Ajouter un numéro sur les noms/codes en conflits (ex: test_1)")
            ]
        ]
    );
    echo $this->Html->tag('/div');
} else {
    echo $this->Html->tag('div', __("Aucun conflit n'a été détecté"), ['class' => 'alert alert-success']);
}

/**
 * @var string $key
 * @var Keyword $value
 */
foreach ($imports as $key => $value) {
    $imports[$key] = $value->toArray();
    $invalid = false;
    $errors = [];
    if ($code = $value->getInvalidField('code')) {
        $imports[$key]['code'] = $code;
        $invalid = true;
        $errors[] = current($value->getError('code'));
    }
    if ($code = $value->getInvalidField('name')) {
        $imports[$key]['name'] = $code;
        $invalid = true;
        $errors[] = current($value->getError('name'));
    }
    $imports[$key]['invalid'] = $invalid;
    $imports[$key]['errors'] = $errors;
}

echo $this->Table->create('test-import-table')
    ->fields(
        [
            'name' => ['label' => __("Nom")],
            'code' => ['label' => __("Code")],
            'errors' => ['label' => __("Erreur"), 'callback' => 'errorsCallback']
        ]
    )
    ->data($imports)
    ->params(
        [
            'identifier' => 'code',
            'classEval' => 'data[{index}].invalid ? "danger" : ""',
        ]
    )
    ->generate();
