<?php

/**
 * @var Versae\View\AppView $this
 */

$workspace = empty($data) || current($data)->get('version') === 0;
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover'])
    ->fields(
        [
            'Keywords.code' => [
                'label' => __("Code"),
                'order' => 'code',
                'filter' => [
                    'code[0]' => [
                        'id' => 'filter-code-0',
                        'label' => false,
                        'aria-label' => __("Code"),
                    ],
                ],
            ],
            'Keywords.name' => [
                'label' => __("Nom"),
                'order' => 'name',
                'filter' => [
                    'name[0]' => [
                        'id' => 'filter-name-0',
                        'label' => false,
                        'aria-label' => __("Nom"),
                    ],
                ],
            ],
            'Keywords.created' => [
                'label'   => __("Date de création"),
                'type'    => 'datetime',
                'display' => false,
                'order'   => 'created',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'created[0]' => [
                        'id' => 'filter-created-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator('dateoperator_created[0]', '>='),
                        'append' => $this->Date->picker('#filter-created-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'created[1]' => [
                        'id' => 'filter-created-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_created[1]', '<='),
                        'append' => $this->Date->picker('#filter-created-1'),
                        'class' => 'datepicker with-select',
                    ],
                ]
            ],
            'Keywords.modified' => [
                'label'   => __("Date de modification"),
                'type'    => 'datetime',
                'display' => false,
                'order'   => 'modified',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'modified[0]' => [
                        'id' => 'filter-modified-0',
                        'label' => __("Date de modification"),
                        'prepend' => $this->Input->operator('dateoperator_modified[0]', '>='),
                        'append' => $this->Date->picker('#filter-modified-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'modified[1]' => [
                        'id' => 'filter-modified-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_modified[1]', '<='),
                        'append' => $this->Date->picker('#filter-modified-1'),
                        'class' => 'datepicker with-select',
                    ],
                ]
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'Keywords.id',
            'favorites' => true,
            'checkbox' => $workspace,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionViewKeyword({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => __("Visualiser {0}", '{1}'),
                'aria-label' => __("Visualiser {0}", '{1}'),
                'display' => $this->Acl->check('keywords/view'),
                'params' => ['Keywords.id', 'Keywords.name']
            ],
            [
                'onclick' => "actionEditKeyword({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'display' => $this->Acl->check('keywords/edit'),
                'displayEval' => "data[{index}].Keywords.version === 0",
                'params' => ['Keywords.id', 'Keywords.name', 'Keywords.version']
            ],
            function ($table) {
                /** @var Versae\View\AppView $this */
                $deleteUrl = $this->Url->build('/Keywords/delete');
                return [
                    'onclick' => "TableGenericAction.deleteAction($table->tableObject, '$deleteUrl')({0})",
                    'type' => 'button',
                    'class' => 'btn-link',
                    'display' => $this->Acl->check('/keywords/delete'),
                    'displayEval' => "data[{index}].Keywords.version === 0",
                    'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'params' => ['Keywords.id', 'Keywords.name']
                ];
            },
        ]
    );

$pagination = [];
if ($workspace) {
    $pagination = [
        'options' => [
            'actions' => [
                '0' => __("-- Action groupée --"),
                '1' => __("Supprimer"),
            ],
        ],
        'callback' => 'updatePaginationAjaxCallback'
    ];
}

echo $this->element(
    'section_table',
    [
        'id' => 'keyword-section',
        'title' => __("Liste de mots-clés"),
        'table' => $table,
        'pagination' => $pagination
    ]
);
