<?php

/**
 * @var Versae\View\AppView $this
 */

$loading = $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);
$formId = 'form-import-keywords';
echo $this->Form->create(null, ['id' => $formId, 'idPrefix' => 'import-keywords']);

$uploads = $this->Upload
    ->create($uploadId, ['class' => 'table table-striped table-hover hide'])
    ->fields(
        [
            'ext' => ['label' => false, 'callback' => 'renderExt'],
            'filename' => ['label' => __("Nom de fichier"), 'callback' => 'TableHelper.wordBreak()'],
            'size' => ['label' => __("Taille"), 'callback' => 'TableHelper.readableBytes'],
            'message' => ['label' => __("Message"), 'style' => 'min-width: 250px', 'class' => 'message'],
        ]
    )
    ->data([])
    ->params(
        [
            'identifier' => 'id',
            'classEval' => 'data[{index}].valid !== false ? "" : "danger"',
        ]
    );
$jsUpload = $uploads->jsObject;

echo $this->Html->tag(
    'p',
    __("Déposer un fichier CSV ou RDF (Skos) afin d'importer une liste de mots-clés"),
    ['class' => 'alert alert-info']
);

echo $uploads->generate(
    [
        'attributes' => ['accept' => '.rdf, .csv'],
        'singleFile' => true,
        'allowDuplicateUploads' => true,
        'target' => $this->Url->build('/Upload/index/importKeywords')
    ]
);

echo $this->Html->tag('div', $loading, ['id' => 'upload-info', 'class' => 'hide']);

echo $this->Form->end();
?>
<script>
    var uploadTable = {table: ''};
    uploadTable.table = '#<?=$uploadId?>';
    var uploader;
    uploader = <?=$jsUpload?>;

    setTimeout(function() {
        $('div.dropbox').off('.draganimation').on('dragover.draganimation', function() {
            $(this).addClass('dragover');
        }).on('dragleave.draganimation dragend.draganimation drop.draganimation', function() {
            $(this).removeClass('dragover');
        });
    }, 0);

    $('#'+uploader.config.id+'.dropbox').on('fileAdded', function() {
        $(uploadTable.table).removeClass('hide').get(0).scrollIntoView();
    }).on('fileSuccess', function(event, file, message) {
        if (!AsalaeGlobal.is_numeric(message.report.id)) {
            return;
        }
        $('#upload-field').slideUp();
        $('#upload-info').removeClass('hide');
        $('#form-import-keywords').find('p').first().fadeOut();
        $.ajax({
            url: '<?=$this->Url->build(
                '/Keywords/get-import-file-info/' . $this->getRequest()->getParam('pass.0')
            )?>/'+message.report.id,
            error: function() {
                alert(PHP.messages.genericError);
            },
            success: function(content) {
                $('#upload-info').append(content);
            },
            complete: function() {
                $('#upload-info').find('.loading-container').remove();
            }
        });
    });
    if ($(uploadTable.table).find('tbody tr').length >= 1) {
        $(uploadTable.table).removeClass('hide');
    }

    var fileuploadsCheckboxes = $('#<?=$formId?>').find('input[name="fileuploads[_ids][]"]');
    function filechanged() {
        var error = '';
        if (fileuploadsCheckboxes.filter(':checked').length === 0) {
            error = __("Veuillez sélectionner un fichier");
        }
        fileuploadsCheckboxes.each(function() {
            this.setCustomValidity(error);
        });
    }
    filechanged();

    function renderExt(value, context) {
        var ext = context.filename.split('.').pop().toLowerCase();
        return '<div class="prev-ext"><i class="fa fa-file-o" aria-hidden="true"></i><span>'+ext+'</span></div>';
    }

    $(function() {
        $('#import-keyword').find('button.accept').disable();
    });
</script>
