<?php

/**
 * @var Versae\View\AppView $this
 */

?>
<script>
    function implodeParents(value) {
        if (!value || !Array.isArray(value)) {
            return '';
        }
        return $('<span>').text(value.join(' - ')).html();
    }
</script>
<?php
$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Administration"));
$this->Breadcrumbs->add(__("Entités"));

// Titre de la page
echo $this->Html->tag(
    'div.container',
    $this->Html->tag('h1', $this->Fa->i('fa-building', __("Entités")))
    . $this->Breadcrumbs->render()
);

echo $this->element('modal', ['idTable' => $tableId]);
$jsTable = $this->Table->getJsTableObject($tableId);

$buttons = [];
if ($this->Acl->check('/OrgEntities/add')) {
    $buttons[] = $this->ModalForm
        ->create('add-org-entity-modal')
        ->modal(__("Ajouter une entité"))
        ->javascriptCallback('afterAddEntity')
        ->output(
            'button',
            $this->Fa->charte('Ajouter', __("Ajouter une entité")),
            '/OrgEntities/add'
        )
        ->generate(['class' => 'btn btn-success', 'type' => 'button']);
}

echo $this->Html->tag(
    'div.container.text-center',
    ($buttons
            ? $this->Html->tag(
                'div.float-left.btn-separator',
                implode(PHP_EOL, $buttons),
                ['style' => 'width: 0']
            )
            : '')
    . $this->Html->tag(
        'div.btn-group',
        $this->Html->link(
            $this->Fa->i('fa-sitemap', __("Organigramme")),
            '/org-entities/index',
            ['class' => 'btn btn-default', 'escape' => false]
        )
        . $this->Html->tag(
            'button',
            $this->Fa->i('fa-table', __("Tableau")),
            [
                'type' => 'button',
                'class' => 'btn btn-default active',
            ]
        )
    )
);

echo $this->ModalForm
    ->create('edit-org-entity', ['size' => 'modal-xxl'])
    ->modal(__("Modification d'une entité"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTable . ', "OrgEntities")')
    ->output(
        'function',
        'editOrgEntity',
        '/OrgEntities/edit'
    )
    ->generate();

echo $this->Filter->create('org-entities-filter')
    ->saves($savedFilters)
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked'
        ]
    )
    ->filter(
        'identifier',
        [
            'label' => __("Identifiant"),
            'wildcard',
        ]
    )
    ->filter(
        'name',
        [
            'label' => __("Nom"),
            'wildcard',
        ]
    )
    ->filter(
        'type_entity_id',
        [
            'label' => __("Type d'entité"),
            'options' => $types,
            'multiple' => true
        ]
    )
    ->generateSection();
?>
<script>
    $(function() {
        AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');
    });
    function updatePaginationAjaxCallback() {
        $('#btn-paginate-<?=$tableId?>').click(function () {
            var table;
            table = <?=$jsTable?>;
            var visibleCheckedCheckboxes = table.table.find('tbody td.td-checkbox input[type=checkbox]:checked');
            if (!visibleCheckedCheckboxes.length) {
                return;
            }
            $(this).disable();
            if ($('#select-paginate-<?=$tableId?>').val() === '1'
                && confirm("<?=__("Êtes-vous sûr de vouloir supprimer les entités sélectionnés ?")?>")
            ) {
                visibleCheckedCheckboxes.each(function () {
                    eval($(this).closest('tr').find('button.delete:enabled').attr('data-callback'));
                });
            }
            $(this).removeAttr('disabled');
            table.table.find('> thead th input.checkall').prop('checked', false);
        });
    }
    $(window).off('history.change.pagination').on('history.change.pagination', function() {
        updatePaginationAjaxCallback();
    });

    /**
     * Callback de l'action ajout
     *
     * @param {string|object} content
     * @param {string} textStatus
     * @param {object} jqXHR
     */
    function afterAddEntity(content, textStatus, jqXHR) {
        TableGenericAction.afterAdd(<?=$jsTable?>, "OrgEntities")(content, textStatus, jqXHR);
    }
    /**
     * Callback de l'action edit
     *
     * @param {string|object} content
     * @param {string} textStatus
     * @param {object} jqXHR
     */
    function afterEditOrgEntity(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Reload') === 'true') {
            AsalaeGlobal.interceptedLinkToAjax(''+location);
        } else if (afterEditOrgEntity.forceReload) {
            afterEditOrgEntity.forceReload = false;
            AsalaeGlobal.interceptedLinkToAjax(''+location);
        }
    }
</script>
<?php
require 'ajax_index.php';
