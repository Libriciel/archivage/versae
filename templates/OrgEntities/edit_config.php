<?php

/**
 * @var Versae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */

use Versae\Model\Table\ConfigurationsTable;

?>
<!--suppress RequiredAttributes -->
<script>
    function selectableUploadRadio(value, context) {
        if (!AsalaeGlobal.is_numeric(context.id) && context.id !== 'local') {
            return '';
        }
        var input = $('<input type="radio" name="fileuploads[id]">').val(context.id);
        setTimeout(function() {
            $('#entity-upload-background-table')
                .find('input[name="fileuploads[id]"]')
                .prop('checked', false)
                .first()
                .prop('checked', true);
        }, 0);
        return input;
    }

    function urlToImg(value) {
        if (value && value.substr(0, 4) !== '<img') {
            return $('<img alt="prev">')
                .attr('src', value)
                .css({
                    "max-width": "160px",
                    "max-height": "160px"
                });
        }
        return value;
    }
</script>
<?php
$uploads = $this->Upload
    ->create('entity-upload-background', ['class' => 'table table-striped table-hover hide'])
    ->fields(
        [
            'select' => [
                'label' => __("Sélectionner"),
                'class' => 'radio-td',
                'callback' => 'selectableUploadRadio',
            ],
            'prev' => ['label' => __("Prévisualiser"), "callback" => "urlToImg", 'escape' => false],
            'name' => ['label' => __("Nom de fichier"), 'callback' => 'TableHelper.wordBreak()'],
            'size' => ['label' => __("Taille"), 'callback' => 'TableHelper.readableBytes'],
            'message' => ['label' => __("Message"), 'style' => 'min-width: 100px', 'class' => 'message'],
        ]
    )
    ->data($uploadFiles)
    ->params(
        [
            'identifier' => 'id',
            'classEval' => 'data[{index}].valid !== false ? "" : "danger"',
        ]
    )
    ->actions(
        [
            function ($table) {
                $url = $this->Url->build('/Upload/delete');
                return [
                    'onclick' => "GenericUploader.fileDelete($table->tableObject, '$url', {0})",
                    'type' => 'button',
                    'class' => 'btn-link delete',
                    'displayEval' => 'AsalaeGlobal.is_numeric(data[{index}].id)',
                    'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'params' => ['id', 'name']
                ];
            },
            function ($table) use ($entity) {
                $url = $this->Url->build('/OrgEntities/deleteBackground');
                return [
                    'onclick' => "deleteLocalBackground($table->tableObject, '$url', {$entity->get('id')})",
                    'type' => 'button',
                    'class' => 'btn-link delete',
                    'displayEval' => 'data[{index}].webroot',
                    'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'params' => ['id', 'name']
                ];
            }
        ]
    )
    ->generate(
        [
            'attributes' => ['accept' => 'image/*'],
            'allowDuplicateUploads' => true,
            'target' => $this->Url->build('/upload/index/?replace=true')
        ]
    );


$tabs = $this->Tabs->create('tabs-edit-archival-agency-configuration', ['class' => 'row no-padding compact']);

echo $this->Form->create(
    $form,
    [
        'id' => 'edit-archival-agency-config-form',
        'idPrefix' => 'edit-archival-agency-config',
    ]
);

/**
 * Apparence
 */
$tabs->add(
    'tab-archival-agency-configuration-style',
    $this->Fa->i('fa-picture-o', $this->Html->tag('span.text', __("Apparence"))),
    // -----------------------------------------------------------------------------

    $this->Form->control(
        ConfigurationsTable::CONF_HEADER_TITLE,
        [
            'label' => __("Titre affiché dans l'entête des pages"),
            'placeholder' => $entity->get('name'),
        ]
    )
    . $this->Html->tag(
        'div.form-group.fake-input',
        $this->Html->tag('span.fake-label', __("Logo personnalisable (visible sur toutes les pages)"))
        . $uploads
    )
    . $this->Form->control(
        ConfigurationsTable::CONF_BACKGROUND_SIZE,
        [
            'label' => __("Hauteur en pixels de la zone réservée au logo"),
            'placeholder' => '75',
            'type' => 'number',
            'min' => 50,
            'max' => 150,
        ]
    )
);

/**
 * E-mails
 */
$tabs->add(
    'tab-archival-agency-configuration-email',
    $this->Fa->i('fa-envelope', __("E-mails")),
    // -----------------------------------------------------------------------------

    $this->Form->control(
        ConfigurationsTable::CONF_MAIL_TITLE_PREFIX,
        [
            'label' => __("Préfixe des objets des mails envoyés par la plateforme"),
            'placeholder' => '[versae]',
        ]
    )
    . $this->Form->control(
        ConfigurationsTable::CONF_MAIL_HTML_SIGNATURE,
        [
            'label' => __("Signature des mails au format html"),
            'placeholder' => '<strong>' . $entity->get('name') . '</strong>',
            'class' => 'mce mce-small mail-html',
        ]
    )
    . $this->Form->control(
        ConfigurationsTable::CONF_MAIL_TEXT_SIGNATURE,
        [
            'label' => __("Signature des mails au format text"),
            'placeholder' => '-- ' . $entity->get('name') . ' --',
        ]
    )
);

echo $tabs;

echo $this->Form->end();
?>
<script>
    AsalaeGlobal.colorTabsInError();

    $('#edit-archival-agency-config-background-position').keyup(function() {
        $('#background-position-prev').css('background-position', $(this).val());
    }).keyup();

    /**
     * Suppression d'un background-image local (!= TableGenericAction.deleteAction)
     * @param table
     * @param url
     * @param id
     */
    function deleteLocalBackground(table, url, id) {
        var useConfirm = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
        if (useConfirm && !confirm(__("Êtes-vous sûr de vouloir supprimer cet enregistrement ?"))) {
            return;
        }
        $.ajax({
            url: url.replace(/\/$/, '') + '/' + id,
            method: 'DELETE',
            success: function (message) {
                if (message.report === 'done') {
                    table.removeDataId('local');
                    $(table.table)
                        .find('tr[data-id="local"]')
                        .fadeOut(400, function() {
                            $(this).remove();
                            table.decrementPaginator();
                        });
                } else {
                    throw message.report;
                }
            },
            error: function () {
                $(table.table).find('tr[data-id="'+id+'"]').addClass('danger');
            }
        });
    }

    $('#edit-archival-agency-config-form').accordion({header: "h3", heightStyle: "fill"});

    new AsalaeMce({selector: 'textarea.mail-html'});
</script>
