<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->Form->create($entity, ['idPrefix' => 'add-org-entity'])
    . $this->Form->control('name', ['label' => __("Nom"), 'required'])
    . $this->Form->control('short_name', ['label' => __("Nom abrégé")])
    . $this->Form->control('identifier', ['label' => __("Identifiant unique"), 'required'])
    . $this->Form->control('description', ['label' => __("Description")])
    . $this->Form->control(
        'type_entity_id',
        [
            'label' => __("Type d'entité"),
            'required' => true,
            'options' => $typeEntitiesOptions,
            'empty' => __("-- Sélectionner un type d'entité --"),
            'help' => $this->Html->tag('span#info-type-entities-add', ''),
        ]
    );

echo $this->Html->tag(
    'div',
    $this->Form->control(
        'forms_availables._ids',
        [
            'id' => 'versant-forms-availables-select',
            'label' => __("Formulaires disponibles"),
            'options' => $formsOptions,
            'multiple' => true,
            'data-placeholder' => __("-- Sélectionner les formulaires disponibles --"),
        ]
    ),
    ['id' => 'versant-forms-availables']
);

if (!$parentId) {
    echo $this->Form->control(
        'parent_id',
        [
            'label' => __("Entité parente"),
            'required' => true,
            'options' => $parents,
            'empty' => __("-- Sélectionner une entité parente --"),
        ]
    );
}

echo $this->Form->end();
?>
<script>
    var typeTransferringAgencies = <?=json_encode(array_values($typeTransferringAgencies))?>;
    AsalaeGlobal.select2($('#add-org-entity-type-entity-id'));
    AsalaeGlobal.select2($('#versant-forms-availables-select'));
    $('#versant-forms-availables').hide();

    selectTypeEntities.change(function() {
        var help = $('#info-type-entities-add');
        var value = selectTypeEntities.val();
        if (!value) {
            help.text('');
            return;
        }
        var info = selectTypeEntities.find('option[value='+selectTypeEntities.val()+']').attr('title').split('\n');
        var prefix = info.shift();
        help.text(prefix+' '+info.join(', '));

        if (typeTransferringAgencies.indexOf(parseInt(value, 10)) !== -1) {
            $('#versant-forms-availables').show();
        } else {
            $('#versant-forms-availables-select').val('').change();
            $('#versant-forms-availables').hide();
        }
    }).trigger('change');
</script>
