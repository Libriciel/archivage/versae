<?php
/**
 * @var Versae\View\AppView $this
 */
if (empty($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}
$deleteUrl = $this->Url->build('/OrgEntities/delete');
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'OrgEntities.name' => [
                'label' => __("Nom"),
                'order' => 'name',
                'callback' => 'TableHelper.wordBreak()',
                'filter' => [
                    'name[0]' => [
                        'id' => 'filter-name-0',
                        'label' => false,
                        'aria-label' => __("Nom"),
                    ],
                ],
            ],
            'OrgEntities.identifier' => [
                'label' => __("Identifiant"),
                'order' => 'identifier',
                'callback' => 'TableHelper.wordBreak()',
                'filter' => [
                    'identifier[0]' => [
                        'id' => 'filter-identifier-0',
                        'label' => false,
                        'aria-label' => __("Identifiant"),
                    ],
                ],
            ],
            'OrgEntities.parents' => [
                'label'    => __("Parents"),
                'callback' => 'implodeParents',
            ],
            'OrgEntities.type_entity.name' => [
                'label' => __("Type"),
                'order' => 'type_entity',
                'filter' => [
                    'type_entity_id[0]' => [
                        'id' => 'filter-type_entity_id-0',
                        'label' => false,
                        'aria-label' => __("Type"),
                        'options' => $types,
                    ],
                ],
            ],
            'OrgEntities.description' => [
                'label' => __("Description"),
                'display' => false,
            ],
            'OrgEntities.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'display' => false,
                'order' => 'created',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'created[0]' => [
                        'id' => 'filter-created-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator('dateoperator_created[0]', '>='),
                        'append' => $this->Date->picker('#filter-created-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'created[1]' => [
                        'id' => 'filter-created-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_created[1]', '<='),
                        'append' => $this->Date->picker('#filter-created-1'),
                        'class' => 'datepicker with-select',
                    ],
                ]
            ],
            'OrgEntities.active' => [
                'label' => __("Actif"),
                'type' => 'boolean',
                'display' => false,
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'OrgEntities.id',
            'favorites' => true,
            'sortable' => true,
            'checkbox' => true,
        ]
    )
    ->actions(
        [
            [
                'onclick' => 'editOrgEntity({0})',
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'display' => $this->Acl->check('/org-entities/edit'),
                'params' => ['OrgEntities.id', 'OrgEntities.name']
            ],
            [
                'data-callback' => "TableGenericAction.deleteAction($jsTable, '$deleteUrl')({0}, false)",
                'type' => 'button',
                'class' => 'btn-link delete',
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => __("Supprimer {0}", '{1}'),
                'aria-label' => __("Supprimer {0}", '{1}'),
                'confirm' => __("Êtes-vous sûr de vouloir supprimer cette entité ?"),
                'display' => $this->Acl->check('/orgEntities/delete'),
                'displayEval' => 'data[{index}].OrgEntities.deletable',
                'params' => ['OrgEntities.id', 'OrgEntities.name'],
            ],
        ]
    );

// @see src/Template/OrgEntities/index.php
$jsTable = $table->tableObject;

echo $this->element(
    'section_table',
    [
        'id' => $tableId . '-section',
        'title' => __("Liste des entités"),
        'aria-label' => __("Liste des entités"),
        'table' => $table,
        'pagination' => [
            'base-id' => 'paginate-' . $tableId,
            'options' => [
                'actions' => [
                    '0' => __("-- action groupée --"),
                    '1' => __("Supprimer"),
                ],
            ],
            'callback' => 'updatePaginationAjaxCallback'
        ]
    ]
);
