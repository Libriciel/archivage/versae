<?php

/**
 * @var Versae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */

use Cake\Utility\Hash;

echo $this->element('modal', ['idTable' => $tableIdUser, 'paginate' => false]);

$Html = $this->Html;

/**
 * Génère le "-- action groupée --" en bas du tableau
 * @param string $id
 * @return string
 */
$generateSubTableActions = function (string $id) use ($Html): string {
    $options = [
        '0' => __("-- action groupée --"),
        '1' => __("Télécharger en ZIP"),
        '2' => __("Supprimer"),
    ];
    $first = $Html->tag('div', null, ['class' => 'form-group form-group-sm'])
        . $Html->tag(
            'label',
            __("Action sur les cases à cocher"),
            ['class' => 'sr-only', 'for' => 'select-' . $id]
        )
        . $Html->tag('select', null, ['class' => 'form-control w-sm inline', 'id' => 'select-' . $id]);
    foreach ($options as $value => $message) {
        $first .= $Html->tag('option', $message, compact('value'));
    }
    $first .= $Html->tag('/select') . $Html->tag('/div');
    $second = $Html->tag(
        'button',
        '<i class="fa fa-cog" aria-hidden="true"></i> ' . __("Executer"),
        ['class' => 'btn btn-primary', 'id' => 'btn-' . $id]
    );

    return '
    <div class="d-flex">
        <div class="col-md-2">' . $first . '</div>
        <div class="col-md-2">' . $second . '</div>
    </div>';
};

$tableUser = $this->Table
    ->create($tableIdUser, ['class' => 'table table-striped table-hover smart-td-size'])
    ->url(
        $url = [
            'controller' => 'org-entities',
            'action' => 'paginate-users',
            $id,
            '?' => [
                'sort' => 'username',
                'direction' => 'asc',
            ]
        ]
    )
    ->fields(
        [
            'username' => ['label' => __("Identifiant de connexion")],
            'name' => ['label' => __("Nom d'utilisateur")],
            'role.name' => ['label' => __("Rôle")],
            'active' => ['label' => __("Actif"), 'type' => 'boolean'],
            'created' => ['label' => __("Date de création"), 'type' => 'datetime', 'display' => false],
            'modified' => ['label' => __("Date de modification"), 'type' => 'datetime', 'display' => false],
        ]
    )
    ->data($entity->get('users'))
    ->params(
        [
            'identifier' => 'id',
            'checkbox' => false,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "loadEditModalUser({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'displayEval' => 'data[{index}].agent_type === "person"',
                'params' => ['id', 'name']
            ],
            [
                'onclick' => "actionDeleteUser({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => __("Supprimer {0}", '{1}'),
                'aria-label' => __("Supprimer {0}", '{1}'),
                'displayEval' => 'data[{index}].deletable',
                'params' => ['id', 'name']
            ],
        ]
    );

$jsTableUser = $tableUser->tableObject;
$paginator = $this->AjaxPaginator->create('pagination-org-entity-users')
    ->url($url)
    ->table($tableUser)
    ->count($countUsers);

$addBtn = '';
if ($rolesUsers) {
    $addBtn .= $this->ModalForm
        ->create('edit-entity-add-user-modal')
        ->modal(__("Ajout d'un utilisateur"))
        ->javascriptCallback('afterAddUser')
        ->output(
            'button',
            $this->Fa->charte('Ajouter', __("Ajouter un utilisateur")),
            '/Users/add/' . $entity->get('id')
        )
        ->generate(['class' => 'btn btn-success', 'type' => 'button']);
}

$usersTab = $this->Html->tag(
    'div',
    $paginator->scrollBottom(['style' => 'position: absolute; right: 20px; z-index:100'])
        . $addBtn
        . $this->ModalForm
            ->create('edit-entity-user-edit')
            ->modal(__("Modification d'un utilisateur"))
            ->javascriptCallback('afterEditUser')
            ->output('function', 'loadEditModalUser', '/Users/edit-by-admin')
            ->generate(),
    ['class' => 'separator padding10']
)
    . $this->Html->tag(
        'header',
        $this->Html->tag('h2.h4', __("Liste des utilisateurs de l'entité"))
        . $this->Html->tag('div.r-actions.h4', $tableUser->getConfigureLink())
    )
    . $tableUser . $paginator . $paginator->scrollTop();

$code = $entity->get('type_entity') ? $entity->get('type_entity')->get('code') : '';
$infosTab = $this->Form->create(
    $entity,
    [
        'id' => 'edit-org-entity-form',
        'idPrefix' => 'edit-org-entity',
        'url' => $this->Url->build('/org-entities/edit/' . $id),
    ]
)
    . $this->Form->control(
        '_tab',
        [
            'type' => 'hidden',
            'value' => 'main',
        ]
    )
    . $this->Form->control('name', ['label' => __("Nom"), 'required'])
    . $this->Form->control('short_name', ['label' => __("Nom abrégé")])
    . $this->Form->control(
        'identifier',
        [
            'label' => __("Identifiant unique"),
        ]
    )
    . ( // concerne uniquement les services d'archives
    Hash::get($entity, 'type_entity.code') === 'SA'
        ? $this->Form->control(
            'max_disk_usage_conv',
            [
                'label' => __("Limite logicielle de taille du disque"),
                'append' => $this->Input->mult('mult', 'go'),
                'class' => 'with-select',
                'type' => 'number',
                'min' => 1,
                'required' => true,
            ]
        )
        : ''
    )
    . $this->Form->control(
        'description',
        [
            'label' => __("Description"),
        ]
    )
    . $this->Form->control(
        'type_entity_id',
        [
            'id' => false,
            'type' => 'hidden',
        ]
    )
    . $this->Form->control(
        'type_entity_id',
        [
            'label' => __("Type d'entité"),
            'required' => true,
            'options' => ($code === 'SA' || count($lockedForms)) ? $typeEntities : $typeEntitiesOptions,
            'empty' => __("-- Sélectionner un type d'entité --"),
            'disabled' => $code === 'SA' || count($lockedForms),
            'help' => $this->Html->tag('span#info-type-entities-edit', ''),
        ]
    )
    . $this->Html->tag(
        'div',
        $this->Form->control(
            'forms_availables._ids',
            [
                'id' => 'versant-forms-availables-select',
                'label' => __("Formulaires disponibles"),
                'options' => $formsOptions,
                'multiple' => true,
                'data-placeholder' => __("-- Sélectionner les formulaires disponibles --"),
            ]
        ),
        ['id' => 'versant-forms-availables']
    )
    . $this->Form->control(
        'parent_id',
        [
            'label' => __("Entité parente"),
            'options' => $parents,
            'required' => $code !== 'SA',
            'empty' => __("-- Sélectionner une entité parente --"),
            'disabled' => $code === 'SA',
        ]
    )
    . $this->Form->control(
        'active',
        [
            'label' => __("Actif"),
        ]
    )
    . $this->Html->tag(
        'div',
        $this->Form->button(
            $this->Fa->charte(
                'Enregistrer',
                __("Enregistrer")
            ),
            ['bootstrap-type' => 'primary']
        ),
        ['class' => 'text-right']
    )
    . $this->Form->end();

$rgpdTab = $this->Form->create(
    $entity,
    ['id' => 'edit-org-entity-sa-rgpd', 'idPrefix' => 'edit-org-entity-sa-rgpd']
)
    . $this->Html->tag(
        'div.alert.alert-info',
        $this->Html->tag(
            'p',
            __(
                "Les informations ci-dessous seront affichées dans la section \""
                . "Responsable des traitements\" dans la politique de confidentialité."
            )
        )
        . $this->Html->tag(
            'p',
            __("Les sous entités héritent de ces informations.")
        )
    );
$rgpdTab .= $this->Form->fieldset(
    $this->Form->control(
        '_tab',
        [
            'type' => 'hidden',
            'value' => 'rgpd',
        ]
    )
    . $this->Form->control(
        'rgpd_info.org_name',
        [
            'label' => __("Raison sociale"),
        ]
    )
    . $this->Form->control(
        'rgpd_info.org_address',
        [
            'label' => __("Adresse"),
        ]
    )
    . $this->Form->control(
        'rgpd_info.org_siret',
        [
            'label' => __("Numéro SIRET"),
        ]
    )
    . $this->Form->control(
        'rgpd_info.org_ape',
        [
            'label' => __("Code APE"),
        ]
    )
    . $this->Form->control(
        'rgpd_info.org_phone',
        [
            'label' => __("N° de téléphone"),
        ]
    )
    . $this->Form->control(
        'rgpd_info.org_email',
        [
            'label' => __("E-mail"),
        ]
    ),
    ['legend' => __("Entité")]
);

$rgpdTab .= $this->Form->fieldset(
    $this->Form->control(
        'rgpd_info.ceo_name',
        [
            'label' => __("Nom du responsable"),
        ]
    )
    . $this->Form->control(
        'rgpd_info.ceo_function',
        [
            'label' => __("Qualité/fonction du responsable"),
        ]
    ),
    ['legend' => __("Responsable de l'entité/Responsable des traitements")]
);

$rgpdTab .= $this->Form->fieldset(
    $this->Form->control(
        'rgpd_info.dpo_name',
        [
            'label' => __("Nom du délégué"),
        ]
    )
    . $this->Form->control(
        'rgpd_info.dpo_email',
        [
            'label' => __("Adresse e-mail du délégué"),
        ]
    ),
    ['legend' => __("Délégué à la protection des données de l'entité (DPD / DPO)")]
);
$rgpdTab .= $this->Form->end();

$tabs = $this->Tabs->create('tab-entity-container', ['class' => 'row no-padding']);
$tabs->add(
    'entity-section',
    $this->Fa->charte('Modifier', __("Informations générales")),
    $infosTab
);
$tabs->add(
    'users-section',
    $this->Fa->charte('Utilisateurs', __x('tab', "Utilisateurs")),
    $usersTab,
    ['div' => ['class' => 'no-padding']]
);
$tabs->add(
    'rgpd-section',
    $this->Fa->i('fa-user-secret', __("RGPD")),
    $rgpdTab
);
echo $tabs;
?>
<!--suppress JSJQueryEfficiency -->
<script>
    var typeTransferringAgencies = <?=json_encode(array_values($typeTransferringAgencies))?>;
    var selectTypeEntities = $('#edit-org-entity-type-entity-id');

    var formAvailablesSelect = $('#versant-forms-availables-select');
    // lock des forms utilisés seulement par cette entité
    formAvailablesSelect.children().each(function() {
        if (<?=json_encode($lockedForms)?>.includes($(this).val())) {
            $(this).attr('locked', true);
        }
    })
    AsalaeGlobal.select2(formAvailablesSelect);

    $('#versant-forms-availables').hide();

    /**
     * Callback de l'action ajout
     *
     * @param {string|object} content
     * @param {string} textStatus
     * @param {object} jqXHR
     */
    function afterAddUser(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            var table;
            table = <?=$jsTableUser?>;
            table.data.push(content);
            TableGenerator.appendActions(table.data, table.actions);
            table.generateAll();
            if (typeof content.code === 'string') {
                setTimeout(function () {
                    window.open('<?=$this->Url->build('/auth-urls/activate')?>/' + content.code, '_blank');
                }, 100);// laisse le temps à la db d'enregistrer l'url
            }
        }
    }

    /**
     * Callback de l'action edit
     *
     * @param {string|object} content
     * @param {string} textStatus
     * @param {object} jqXHR
     */
    function afterEditUser(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            var table;
            table = <?=$jsTableUser?>;
            table.replaceDataId(content.id, content);
            TableGenerator.appendActions(table.data, table.actions);
            table.generateAll();
        }
    }

    /**
     * Action delete
     *
     * @param {number} id
     */
    function actionDeleteUser(id) {
        var useConfirm = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
        var update = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
        if (useConfirm && !confirm("<?=__("Êtes-vous sûr de vouloir supprimer cet utilisateur ?")?>")) {
            return;
        }
        var table = {};
        table = <?=$jsTableUser ?? '{}'?>;
        $.ajax({
            url: '<?=$this->Url->build('/Users/delete')?>/' + id,
            success: function (message) {
                if (message.report === 'done') {
                    table.removeDataId(id);
                    $(table.table)
                        .find('tr[data-id="' + id + '"]')
                        .fadeOut(400, function () {
                            $(this).remove();
                            table.decrementPaginator();
                        });
                } else {
                    throw message.report;
                }
            },
            error: function () {
                $(table.table)
                    .find('tr[data-id="' + id + '"]')
                    .addClass('danger');
            }
        });
    }
    var entityForm = $('#edit-org-entity-form');

    /**
     * Cache les options interdites mais la laisse si le select est déjà
     * @param select
     * @param ids
     * @return {*}
     */
    function hideOptions(select, ids) {
        var selectValue = select.val();
        select.find('option').each(function () {
            var value = parseInt($(this).attr('value'), 10);
            if (value !== selectValue && ids.indexOf(value) !== -1) {
                $(this).hide();
            }
        });
        return select;
    }

    AsalaeGlobal.select2(
        hideOptions(selectTypeEntities, [<?=implode(', ', $typeEntitiesDisabled)?>])
    );
    AsalaeGlobal.select2(
        hideOptions($('#edit-org-entity-parent-id'), [<?=implode(', ', $parentsDisabled)?>])
    );

    var initialFormValue = entityForm.serialize();
    $('.save-on-change').click(function () {
        var form = $('#edit-org-entity-form'),
            formValue = form.serialize();

        if (formValue !== initialFormValue
            && confirm(__("Vous avez apporté des modifications au formulaire. Souhaitez-vous les sauvegarder ?"))
        ) {
            afterEditOrgEntity.forceReload = true;
            form.trigger('submit');
        }
        initialFormValue = formValue;
    });

    selectTypeEntities.change(function () {
        var help = $('#info-type-entities-edit');
        var value = selectTypeEntities.val();
        if (!value) {
            help.text('');
            return;
        }
        var info = selectTypeEntities.find('option[value=' + value + ']').attr('title').split('\n');
        var prefix = info.shift();
        help.text(prefix + ' ' + info.join(', '));

        if (typeTransferringAgencies.indexOf(parseInt(value, 10)) !== -1) {
            $('#versant-forms-availables').show();
        } else {
            $('#versant-forms-availables-select').val('').change();
            $('#versant-forms-availables').hide();
        }
    }).trigger('change');
</script>
