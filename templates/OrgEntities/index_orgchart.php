<?php

/**
 * @var Versae\View\AppView $this
 */

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Administration"));
$this->Breadcrumbs->add(__("Entités"));

// Titre de la page
echo $this->Html->tag(
    'div.container',
    $this->Html->tag('h1', $this->Fa->i('fa-building', __("Entités")))
    . $this->Breadcrumbs->render()
);

$loading = $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);

echo $this->ModalForm
    ->create('add-org-entity-modal')
    ->modal(__("Ajouter une entité"))
    ->javascriptCallback('afterAddEntity')
    ->output('function', 'addChild', '/OrgEntities/add')
    ->generate(['class' => 'btn btn-success']);

echo $this->ModalForm
    ->create('edit-org-entity', ['size' => 'modal-xxl'])
    ->modal(__("Modification d'une entité"))
    ->javascriptCallback('afterEditOrgEntity')
    ->output(
        'function',
        'editOrgEntity',
        '/OrgEntities/edit'
    )
    ->generate();

echo $this->Modal->create(['id' => 'roles-index-modal'])
    . $this->Modal->header(__("Modifier un rôle"))
    . $this->Modal->body($loading, ['class' => 'no-padding'])
    . $this->Modal->end()
    . $this->ModalForm->modalScript('roles-index-modal');

echo $this->Html->tag(
    'div.container.text-center',
    $this->Html->tag(
        'div.btn-group',
        $this->Html->tag(
            'button',
            $this->Fa->i('fa-sitemap', __("Organigramme")),
            [
                'type' => 'button',
                'class' => 'btn btn-default active',
            ]
        )
        . $this->Html->link(
            $this->Fa->i('fa-table', __("Tableau")),
            '/org-entities/index/table',
            ['class' => 'btn btn-default', 'escape' => false]
        )
    )
);

echo $this->Html->tag(
    'div.container',
    $this->Html->tag('p#search-node-msg', '')
    . $this->Form->control(
        'search-select-name',
        [
            'label' => __("Rechercher une entité"),
            'options' => $allNames,
            'empty' => __("-- Sélectionner une entité --"),
        ]
    )
    . $this->Html->tag(
        'div.btn-group',
        $this->Html->tag(
            'button',
            $this->Fa->i('fa-sitemap', __("Déplier tout")),
            [
                'id' => 'reset-orgchart-btn',
                'type' => 'button',
                'class' => 'btn btn-default',
                'onclick' => 'resetOrgchart()',
            ]
        )
    )
);
?>
<div id="chart-container"></div>
<script id="jsTemplateOrgChartNode" type="text/x-jsrender">
    <div class="border{{if active}} active{{else}} inactive{{/if}}">
        <div class="title">
            {{if children.length >= 1}}
                <button type="button"
                        class="btn-link color-white"
                        title="<?=$title = __("Possède des entités filles")?>"
                        aria-label="<?=$title?>" onclick="focusOneNode($('#{{:html_id}}'))">
                    <i class="fa fa-sitemap" aria-hidden="true">&nbsp;</i>
                </button>
            {{else}}
                <i>&nbsp;</i><!-- pour alignement flex -->
            {{/if}}
            <span class="orgchart-title" title="{{:identifier}}">
                {{if short_name}}{{:short_name}}{{else}}{{:name}}{{/if}}
            </span>
            <?php if ($this->Acl->check('/OrgEntities/add')) : ?>
            <button type="button"
                    class="btn-link add-child"
                    onclick="addChild({{:id}})"
                    title="<?=__("Ajouter un descendant")?>">
                <?=$this->Fa->charte('Ajouter', '', 'text-success pale')?>
                <span class="sr-only"><?=__("Ajouter un descendant sur {0}", "{{:name}}")?></span>
            </button>
            <?php else : ?>
            <i>&nbsp;</i><!-- pour alignement flex -->
            <?php endif; ?>
        </div>
        {{if identifier}}
        <input type="hidden" value="{{:id}}" class="hidden-entity-identifier">
        <div class="content">
            <div class="text">{{:type_entity.name}}</div>
            <div class="td-group">
                <?php if ($this->Acl->check('/OrgEntities/edit')) : ?>
                {{if type_entity.code === "SA" && !rgpd_info}}
                <div class="td-group-sub-sub">
                    <div class="td-group-fieldvalue" title="<?=__("Il manque les informations RGPD")?>">
                        <i aria-hidden="true" class="fa fa-warning text-warning"></i>
                        <span class="sr-only"><?=__("Il manque les informations RGPD")?></span>
                    </div>
                </div>
                {{/if}}
                <div class="td-group-sub-sub">
                    <div class="td-group-fieldvalue">
                        <button type="button"
                                class="btn-link"
                                onclick="editOrgEntity({{:id}})"
                                title="<?=__("Modifier {0}", "{{:name}}")?>">
                            <?=$this->Fa->charte('Modifier')?>
                            <span class="sr-only"><?=__("Modifier {0}", "{{:name}}")?></span>
                        </button>
                    </div>
                </div>
                <?php endif; if ($this->Acl->check('/OrgEntities/delete')) : ?>
                {{if deletable}}
                <div class="td-group-sub-sub">
                    <div class="td-group-fieldvalue">
                        <button type="button"
                                class="btn-link"
                                onclick="actionDelete({{:id}})"
                                title="<?=__("Supprimer {0}", "{{:name}}")?>">
                            <?=$this->Fa->charte('Supprimer', '', 'text-danger')?>
                            <span class="sr-only"><?=__("Supprimer {0}", "{{:name}}")?></span>
                        </button>
                    </div>
                </div>
                {{/if}}
                <?php endif; ?>
            </div>
        </div>
        {{/if}}
    </div>
</script>
<script>
    setTimeout(function() {
        $('#search-select-name').chosen({
            width: '',
            allow_single_deselect: true
        }).on('change', function() {
            findNode('#OrgEntity_'+$(this).val());
        }).parent().find('input.chosen-search-input').attr('aria-label', __("Rechercher"));
    }, 10);

    var datasource = {};
    var nodeTemplate = function(data) {
        var tmpl = $.templates('#jsTemplateOrgChartNode');
        return tmpl.render(data);
    };

    /**
     * Action de recherche lors de la sélection d'une valeur dans le select
     * @var {string|jQuery} element
     */
    function findNode(element) {
        var msg = $('#search-node-msg').empty();
        element = $(element);
        if (element.length === 0) {
            resetOrgchart();
            return;
        }
        focusOneNode(element);
    }

    /**
     * Règle le scroll du container afin de centrer l'élément dans le navigateur
     * @var {string|jQuery} element
     */
    function centerNodeToScreen(element) {
        element = $(element);
        var position = element.position(),
            posX = Math.round(position.left + element.outerWidth() / 2),
            posY = Math.round(position.top + element.outerHeight() / 2),
            body = $('body').first(),
            grille = element.closest('.orgchart'),
            container = grille.parent(),
            actualScrollX = container.scrollLeft(),
            actualScrollY = container.scrollTop(),
            windowX = body.width(),
            windowY = grille.height() + grille.position().top,
            targetPosX = Math.round(windowX / 2),
            targetPosY = Math.round(windowY / 2),
            diffX = posX - targetPosX,
            diffY = posY - targetPosY;

        container.scrollLeft(actualScrollX + diffX)
            .scrollTop(actualScrollY + diffY)
            .find('.active').removeClass('active');

        element.addClass('active');
    }

    /**
     * Retire toutes les modifications de l'affichage de l'organigramme
     * @var {string|jQuery} element
     */
    function resetOrgchart() {
        var containter = $('#chart-container');
        containter.find('tr.hidden, td.hidden')
            .removeClass('hidden')
            .css('visibility', '');

        containter.find('div[class*="slide-"]')
            .removeClass('slide-up')
            .removeClass('slide-down')
            .removeClass('slide-left')
            .removeClass('slide-right');

    }

    /**
     * Effectue un resetOrgchart() et un centerNodeToScreen() tout en repliant
     * tous les éléments sauf l'élément en question, son parent, ses frères et
     * ses enfants
     * @var {string|jQuery} element
     */
    function focusOneNode(element) {
        element = $(element);
        if (!element || element.length === 0) {
            return;
        }

        resetOrgchart();
        setTimeout(function() {
            organigramme.hideChildren(element);
            organigramme.hideParent(element);
            var parent = organigramme.getRelatedNodes(element, 'parent');
            parent.one('transitionend', function(event) {
                setTimeout(function() {
                    organigramme.showParent(element);
                    organigramme.showChildren(element);
                    organigramme.showSiblings(element);
                    parent.one('transitionend', function(event) {
                        setTimeout(function () {
                            centerNodeToScreen(element);
                            $('#search-node-msg').empty();
                        }, 10);
                    });
                }, 10);
            });
        }, 100);
    }

    var organigramme;

    /**
     * Initialise l'organigramme
     */
    function loadChart() {
        AsalaeLoading.start();
        setTimeout(function () {
            var container = $('#chart-container').empty()
                initialized = false;

            organigramme = container.orgchart({
                'nodeId': 'html_id',
                'data': datasource,
                'nodeTemplate': nodeTemplate,
                'draggable': true,
                'initCompleted': function ($orgChart) {
                    initialized = true;
                    $('html').trigger('orgchart.init');
                    AsalaeLoading.stop();
                }
            });
            setTimeout(function() {
                if (initialized === false) {
                    $('html').trigger('orgchart.init');
                    AsalaeLoading.stop();
                    console.warn("orgchart initCompleted is not triggered (ok on IE10)");
                }
            }, 10000);
            organigramme.$chart.on('nodedrop.orgchart', function (event, nodes) {
                AsalaeGlobal.grabHandlers.mouseup(event);
                var node = $(nodes.draggedNode),
                    id = node.addClass('warning').find('input').first().val();
                $.ajax({
                    url: '<?=$this->Url->build('/OrgEntities/editParentId')?>/' + id,
                    method: "PUT",
                    data: {
                        parent_id: $(nodes.dropZone).find('input').first().val()
                    },
                    headers: {
                        Accepts: 'application/json'
                    },
                    success: function (content) {
                        if (content.success) {
                            node.removeClass('warning');
                            centerNodeToScreen(node);
                        } else {
                            console.warning(content);
                            this.error();
                        }
                    },
                    error: function () {
                        node.removeClass('warning').addClass('danger');
                        alert(PHP.messages.genericError);
                    }
                });
            });
            // scroll à la souris (pan custom)
            var orgchartElement = container.find('> .orgchart');
            orgchartElement.on('mousedown', AsalaeGlobal.grabHandlers.mousedown)
                .on('mousemove', AsalaeGlobal.grabHandlers.mousemove);

            $(document).on('mouseup', AsalaeGlobal.grabHandlers.mouseup);
            orgchartElement.find('.node').on('dragstart', AsalaeGlobal.grabHandlers.mouseup);
        }, 100); // Laisse le temps à l'animation de se lancer
    }
    $(function() {
        datasource = <?=json_encode($chartDatasource)?>;
        loadChart();
    });
</script>

<script>
    /**
     * Action delete
     *
     * @param {number} id
     */
    function actionDelete(id) {
        var useConfirm = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
        var update = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
        if (useConfirm && !confirm("<?=__("Êtes-vous sûr de vouloir supprimer cette entité ?")?>")) {
            return;
        }
        $.ajax({
            url: '<?=$this->Url->build('/OrgEntities/delete')?>/' + id,
            method: 'DELETE',
            success: function (message) {
                if (message.report === 'done') {
                    organigramme.removeNodes($('.hidden-entity-identifier[value="'+id+'"]'));
                } else {
                    throw message.report;
                }
            },
            error: function () {
                alert(PHP.messages.genericError);
            }
        });
    }

    /**
     * Callback de l'action ajout
     *
     * @param {string|object} content
     * @param {string} textStatus
     * @param {object} jqXHR
     */
    function afterAddEntity(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            var $node = $('#OrgEntity_'+content.parent_id),
                hasChild = $node.parent().attr('colspan') > 0;

            if (!hasChild) {
                organigramme.addChildren($node, {'children': [content]});
            } else {
                organigramme.addSiblings($node.closest('tr').siblings('.nodes').find('.node:first'),
                    {'siblings': [content]}
                );
            }
            $('#search-select-name').append(
                $('<option></option>').attr('value', content.id).text(content.name)
            ).trigger("chosen:updated");
            setTimeout(function() {
                var hiddenInput = $('input.hidden-entity-identifier[value="'+content.id+'"]');
                centerNodeToScreen(hiddenInput.closest('.node').attr('id', 'OrgEntity_'+content.id));
            }, 500);
        }
    }
    /**
     * Callback de l'action edit
     *
     * @param {string|object} content
     * @param {string} textStatus
     * @param {object} jqXHR
     */
    function afterEditOrgEntity(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Reload') === 'true') {
            AsalaeGlobal.interceptedLinkToAjax(''+location);
        } else if (afterEditOrgEntity.forceReload) {
            afterEditOrgEntity.forceReload = false;
            AsalaeGlobal.interceptedLinkToAjax(''+location);
        }
    }
</script>
