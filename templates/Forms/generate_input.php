<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Utility\Text;
use Versae\Model\Table\FormInputsTable;

/*
 * tri les données de la table ($inputData) des données de formulaire (FormHelper)
 */
$data = [];
$whiteList = [
    'id', 'class', 'type', 'label', 'default_value', 'required', 'readonly',
    'pattern', 'placeholder', 'help', 'multiple', 'name', 'append', 'prepend',
    'options', 'templates', 'empty', 'value', 'checked', 'min', 'max', 'step',
];
$blackList = [
    'form_fieldset_id', 'disable_expression', 'form_id', 'typetrad', 'ord',
    'select2', 'p', 'color', 'formats', 'form', 'fieldset', 'keyword_list_id',
    'use_name_as_code', 'copy', 'field', 'min_date', 'max_date',
];

foreach ($whiteList as $key) {
    if (isset($inputData[$key])) {
        $data[$key] = $inputData[$key];
    }
}
foreach ($inputData as $key => $value) {
    if (str_starts_with($key, 'data-') || str_starts_with($key, 'aria-')) {
        $data[$key] = $value;
    } elseif (
        !in_array($key, array_merge($whiteList, $blackList))
        && !str_starts_with($key, 'cond_display_')
    ) {
        trigger_error("missing '$key' in white or black list");
    }
}

$class = $data['class'] ?? '';
if ((string)($inputData['disable_expression'] ?? ($inputData['cond_display_input'] ?? '[]')) !== '[]') {
    $class = trim("$class hiddable-input");
}

switch ($data['type']) {
    case FormInputsTable::TYPE_MULTI_CHECKBOX:
        $data['type'] = FormInputsTable::TYPE_SELECT;
        $data['multiple'] = FormInputsTable::TYPE_CHECKBOX;
        break;
    case FormInputsTable::TYPE_DATE:
    case FormInputsTable::TYPE_DATETIME:
        $data['type'] = FormInputsTable::TYPE_TEXT;
        $data['append'] = $this->Fa->i('fa-calendar');
        break;
}
if (!empty($data['options']) && is_string($data['options'])) {
    $data['options'] = json_decode($data['options'], true);
}
$select2 = !empty($inputData['select2']);
if ($select2) {
    $data['data-mount-select2'] = 'true';
    $data['data-select2-placeholder'] = !empty($data['multiple']) ? ($data['empty'] ?? '') : '';
}
$inputId = $data['id'] ?? null;
$data['id'] = isset($data['name']) ? Text::slug(uniqid($data['name'])) : null;
if (!$inputId) {
    $inputId = $data['id'];
}
if (isset($formInput)) {
    $linked = $formInput->get('linked');
    $data['data-deletable'] = empty($linked) ? 'true' : 'false';
    $data['data-linked-to'] = json_encode($linked);
} else {
    $data['data-deletable'] = 'true';
    $data['data-linked-to'] = '[]';
}
$deletableArray = [
    'data-deletable' => $data['data-deletable'],
    'data-linked-to' => $data['data-linked-to'],
];

if ($data['type'] === FormInputsTable::TYPE_FILE) {
    $multiple = $data['multiple'] ?? false;
    $name = $data['name'] . ($multiple ? '[]' : '');
    $type = $multiple ? 'checkbox' : 'radio';
    $required = ($data['required'] ?? false) ? 'true' : 'false';
    /** @noinspection UnreachableCodeJS - bug phpstorm */
    $script = <<<EOT
    <script>
        function selectableFileupload(value, context) {
            return $('<input>').val(context.id)
                .attr('name', '$name')
                .attr('aria-label', context.name)
                .attr('type', '$type')
                .prop('required', $required)
                .prop('checked', true);
        }
    </script>
EOT;
    $uploads = $this->Upload
        ->create('upload-file-' . $data['id'], ['class' => 'table table-striped table-hover hide'])
        ->fields(
            [
                'selection' => [
                    'label' => __("Sélection"),
                    'callback' => 'selectableFileupload',
                    'class' => 'selection',
                ],
                'name' => [
                    'label' => __("Nom de fichier"),
                    'callback' => 'TableHelper.filenameToPopup',
                ],
                'message' => [
                    'label' => __("Message"),
                    'style' => 'min-width: 200px; max-width: 400px',
                    'class' => 'message',
                ],
            ]
        )
        ->data([])
        ->params(
            [
                'identifier' => 'id',
                'classEval' => 'data[{index}].valid !== false ? "" : "danger"',
            ]
        )
        ->actions(
            [
                function ($table) {
                    $url = $this->Url->build('/Upload/delete');
                    return [
                        'onclick' => "GenericUploader.fileDelete($table->tableObject, '$url', {0})",
                        'type' => 'button',
                        'class' => 'btn-link delete',
                        'displayEval' => 'AsalaeGlobal.is_numeric(data[{index}].id)',
                        'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                        'title' => __("Supprimer {0}", '{1}'),
                        'aria-label' => __("Supprimer {0}", '{1}'),
                        'params' => ['id', 'name']
                    ];
                },
            ]
        );
    $accept = [];
    foreach (empty($data['formats']) ? [] : $data['formats'] as $format) {
        switch ($format) {
            case 'image':
                $accept[] = 'image/*';
                break;
            case 'video':
                $accept[] = 'video/*';
                break;
            case 'audio':
                $accept[] = 'audio/*';
                break;
            case 'archive':
                $accept[] = '.zip, .tar.gz, .rar, .7z';
                break;
            case 'xml':
                $accept[] = 'application/xml, .xml';
                break;
            case 'json':
                $accept[] = 'application/json, .json';
                break;
            case 'csv':
                $accept[] = 'text/csv, .csv';
                break;
            case 'rng_xsd':
                $accept[] = '.rng, .xsd';
                break;
            case 'rng':
                $accept[] = '.rng';
                break;
            case 'xsd':
                $accept[] = '.xsd';
                break;
            case 'text':
                $accept[] = '.txt, .odt, .doc, .docx';
                break;
            case 'calc':
                $accept[] = '.csv, .ods, .xls, .xlsx';
                break;
            case 'presentation':
                $accept[] = '.odp, .ppt, .pptx';
                break;
            case 'office':
                $accept[] = '.txt, .odt, .doc, .docx, .csv, .ods, '
                    . '.xls, .xlsx, .csv, .ods, .xls, .xlsx, .odp, .ppt, .pptx';
                break;
            case 'pdf':
                $accept[] = 'application/pdf, .pdf';
                break;
        }
    }
    $formats = implode(',', empty($data['formats']) ? [] : $data['formats']);
    echo $this->Html->tag(
        'div.form-group.fake-input' . (($data['required'] ?? false) ? '.required' : ''),
        $this->Html->tag(
            'label.control-label' . ($class ? '.' . $class : ''),
            h($data['label']),
            $deletableArray
        )
        . $script
        . $uploads->generate(
            [
                'attributes' => ['accept' => implode(', ', $accept)]
                    + (($data['required'] ?? false) ? ['required' => true] : []),
                'allowDuplicateUploads' => true,
                'singleFile' => (bool)($data['multiple'] ?? false) === false,
                'data-deletable' => $data['data-deletable'],
                'target' => $this->Url->build(
                    "/upload/form-file/$inputId?replace=true&formats=$formats"
                ),
            ]
        )
        . (!empty($data['help']) ? $this->Html->tag('p.help-block', h($data['help'])) : '')
    );
} elseif ($data['type'] === FormInputsTable::TYPE_ARCHIVE_FILE) {
    $name = $data['name'];
    $required = ($data['required'] ?? false) ? 'true' : 'false';
    /** @noinspection UnreachableCodeJS - bug phpstorm */
    $script = <<<EOT
    <script>
        function selectableFileupload(value, context) {
            return $('<input>').val(context.id)
                .attr('name', '$name')
                .attr('aria-label', context.name)
                .attr('type', 'radio')
                .prop('required', $required)
                .prop('checked', true);
        }
    </script>
EOT;
    $uploads = $this->Upload
        ->create(
            'upload-file-' . $data['id'],
            [
                'class' => 'table table-striped table-hover hide',
                'iClass' => 'fas fa-file-archive fa-3x text-warning',
                'pText' => __("Glissez-déposez votre fichier ici"),
            ]
        )
        ->fields(
            [
                'selection' => [
                    'label' => __("Sélection"),
                    'callback' => 'selectableFileupload',
                    'class' => 'selection',
                ],
                'info' => [
                    'label' => __("Fichier"),
                    'target' => '',
                    'thead' => [
                        'filename' => ['label' => __("Nom de fichier"), 'callback' => 'TableHelper.wordBreak()'],
                        'mime' => ['label' => __("Type MIME")],
                        'hash' => ['label' => __("Empreinte du fichier")],
                        'size' => ['label' => __("Taille"), 'callback' => 'TableHelper.readableBytes'],
                        'xpath' => ['label' => __("Emplacement XPath")],
                    ],
                ],
                'message' => [
                    'label' => __("Message"),
                    'style' => 'min-width: 200px; max-width: 400px',
                    'class' => 'message',
                ],
            ]
        )
        ->data([])
        ->params(
            [
                'identifier' => 'id',
                'classEval' => 'data[{index}].valid !== false ? "" : "danger"',
            ]
        )
        ->actions(
            [
                function ($table) {
                    $url = $this->Url->build('/Upload/delete');
                    return [
                        'onclick' => "GenericUploader.fileDelete($table->tableObject, '$url', {0})",
                        'type' => 'button',
                        'class' => 'btn-link delete',
                        'displayEval' => 'AsalaeGlobal.is_numeric(data[{index}].id)',
                        'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                        'title' => __("Supprimer {0}", '{1}'),
                        'aria-label' => __("Supprimer {0}", '{1}'),
                        'params' => ['id', 'name']
                    ];
                },
            ]
        );

    if (!empty($class)) {
        $deletableArray['class'] = trim($class);
    }
    echo $this->Html->tag(
        'div.form-group.fake-input' . (($data['required'] ?? false) ? '.required' : ''),
        $this->Html->tag(
            'label.control-label' . ($class ? '.' . $class : ''),
            h($data['label']),
            $deletableArray
        )
        . $script
        . $uploads->generate(
            [
                'attributes' => ['accept' => '.zip, .tar.gz'],
                'allowDuplicateUploads' => true,
                'singleFile' => true,
                'target' => $this->Url->build(
                    "/upload/form-file/$inputId?replace=true&formats=archive"
                ),
            ]
        )
        . (!empty($data['help']) ? $this->Html->tag('p.help-block', h($data['help'])) : '')
    );
} elseif ($data['type'] === FormInputsTable::TYPE_PARAGRAPH) {
    $color = ($inputData['color'] ?? false) ? 'alert ' . $inputData['color'] : '';
    $class = trim("$class user-paragraph form-group");
    echo $this->Html->tag(
        'div',
        $this->Html->tag('label.hide', 'paragraph')
        . $this->Html->tag('div', $inputData['p'] ?? '', $color ? ['class' => $color] : []),
        ['class' => $class]
    );
} else {
    if ($data['type'] === FormInputsTable::TYPE_EMAIL) {
        $data['append'] = $this->Fa->i('fa-envelope-o');
    } elseif ($data['type'] === FormInputsTable::TYPE_CHECKBOX) {
        $data['templates'] = [
            'checkboxContainer' => '<div class="form-group checkbox {{required}}">{{content}}<br>{{append}}</div>',
        ];
    }
    // cake n'échape pas help
    if (isset($data['help'])) {
        $data['help'] = h($data['help']);
    }
    if (!empty($class)) {
        $label = is_array($data['label']) ? $data['label'] : ['text' => $data['label'] ?? ''];
        $label['class'] = trim($class . ' ' . ($label['class'] ?? ''));
        $data['label'] = $label;
    }
    echo $this->Form->control(
        $data['name'],
        ['val' => $data['default_value'] ?? ''] + $data
    );
}
