<?php

/**
 * @var Versae\View\AppView $this
 * @var Versae\Form\AddInputForm $form
 */

use Versae\Utility\Twig;

echo $this->Form->create($form, ['id' => 'edit-input-form', 'idPrefix' => $idPrefix = 'edit-input']);
echo $this->Form->control(
    'id',
    ['type' => 'hidden']
);
echo $this->Form->control(
    'is_submit',
    ['type' => 'hidden', 'value' => '1']
);
echo $this->Form->control(
    'type',
    ['type' => 'hidden', 'value' => $type]
);

$inputsData = array_filter($inputsData, fn($v) => isset($v['name']) && $v['name'] !== $form->getData('name'));

foreach ($this->getRequest()->getData('field', []) as $key => $value) {
    echo $this->Form->control(
        'field.' . $key,
        ['type' => 'hidden', 'value' => $value]
    );
}

require 'inputs/' . basename($type) . '.php';
?>
<script>
    var formInput2 = $('#edit-input-form');
    // évite les conflits dom
    formInput2.closest('.modal').one(
        'hidden.bs.modal',
        function () {
            $(this).find('form [id]').removeAttr('id');
        }
    );
    formInput2.find('input[name=label]').keyup(
        function () {
            formInput2.find('input[name=name]').val(
                AsalaeGlobal.removeDiacritics($(this).val())
                    .replace(/[ -]+/g, '_')
                    .replace(/\W/g, '')
            ).change();
        }
    );
    // évite les conflits dom
    formInput2.closest('.modal').one(
        'hidden.bs.modal',
        function() {
            $(this).find('form [id]').find('[id]').removeAttr('id');
        }
    );
    formInput2.find('input[name=name]').on('input change', function(e) {
        for (const i in inputsData) {
            if ($(this).val() === inputsData[i].name) {
                this.setCustomValidity(__("Un champ avec ce nom existe déjà"));
                return;
            }
        }
        if (<?=json_encode(Twig::TOKEN_PROG_LIST)?>.includes($(this).val())) {
            this.setCustomValidity(__("Ce nom est réservé"));
            return;
        }
        this.setCustomValidity('');
    });

    var modalTitle = $('#edit-form-inputLabel');
    modalTitle.find('span.type').remove();
    modalTitle.append(
        $('<span class="type">').text(" (<?=$form->getData('typetrad')?>)")
    );

    // empêche l'envoi du formulaire d'exemple
    $('#<?=$idPrefix . '-example-form'?>').on('submit', () => false);
</script>
