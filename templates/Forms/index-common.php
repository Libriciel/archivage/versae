<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->element('modal', ['idTable' => $tableId]);

if (empty($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}

echo $this->ModalView
    ->create('view-form-modal', ['size' => 'modal-4xl'])
    ->modal(__("Visualisation du formulaire"))
    ->output(
        'function',
        'viewForm',
        '/forms/view'
    )
    ->generate();

echo $this->ModalForm
    ->create('edit-form-modal', ['size' => 'modal-4xl'])
    ->modal(__("Modification du formulaire"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTable . ', "Forms")')
    ->output(
        'function',
        'actionEditForm',
        '/forms/edit'
    )
    ->generate();

echo $this->ModalForm
    ->create('test-form-modal', ['size' => 'large'])
    ->modal(__("Tester un formulaire"))
    ->step('/forms/test', 'actionTestForm')
    ->step('/forms/after-test', 'actionAfterTest')
    ->javascriptCallback('AsalaeModal.stepCallback')
    ->output('function')
    ->generate();

echo $this->ModalForm
    ->create('version-form-modal')
    ->modal(__("Versionner un formulaire"))
    ->output(
        'function',
        'actionVersionForm',
        '/forms/version'
    )
    ->javascriptCallback('afterVersionForm')
    ->generate();

$filters = $this->Filter->create('forms-filter')
    ->saves($savedFilters)
    ->filter(
        'name',
        [
            'label' => __("Nom"),
            'wildcard',
        ]
    )
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'version_number[value]',
        [
            'label' => __("Version"),
            'prepend' => $this->Input->operator(
                'version_number[operator]',
                '>=',
                ['id' => 'version-number-operator']
            ),
            'class' => 'with-select',
            'type' => 'number',
            'min' => 1,
        ]
    )
    ->filter(
        'state',
        [
            'label' => __("Etats"),
            'options' => $states,
            'multiple' => true
        ]
    )
    ->filter(
        'archiving_system_id',
        [
            'label' => __("SAE"),
            'options' => $archivingSystemsOptions,
            'empty' => __("-- Sélectionner un Système d'Archivage Électronique --"),
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked',
        ]
    );

if ($tableId === 'forms-index-all-table') {
    $filters->filter(
        'last_version',
        [
            'label' => __("Uniquement la dernière version"),
            'type' => 'checkbox',
            'checked',
        ]
    );
}

echo $filters->generateSection();

require 'ajax_index.php';

$popupPublishContent = $this->Html->tag('h3', __("Attention, formulaire non testé"))
    . $this->Html->tag('div.btn-group-vertical')
    . $this->Html->tag(
        'button',
        $this->Fa->i('fa-calendar-check-o', __("Tester le formulaire")),
        [
            'class' => 'btn btn-primary btn-validity popup-test',
            'type' => 'button',
        ]
    )
    . $this->Html->tag(
        'button',
        $this->Fa->i('fa-globe-europe', __("Publier sans tester")),
        [
            'class' => 'btn btn-default btn-validity popup-publish',
            'type' => 'button',
        ]
    )
    . $this->Html->tag(
        'button',
        $this->Fa->charte('Annuler', __("Annuler")),
        [
            'class' => 'btn btn-default btn-validity popup-cancel',
            'type' => 'button',
        ]
    )
    . $this->Html->tag('/div');
?>
<script>
    $(function () {
        AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');
    });

    /**
     * Callback de l'action versionner :
     * il est nécessaire de recharger le tableau afin que le versionnable de l'entité soit recalculé
     * (sinon on peut essayer de versionner une nouvelle fois et on provoque une erreur)
     * @param {string|object} content
     * @param {string} textStatus
     * @param {object} jqXHR
     */
    function afterVersionForm(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            let table = <?= $jsTable ?>;
            table.data.unshift({Forms: content});
            TableGenerator.appendActions(table.data, table.actions);
            table.generateAll();
            actionEditForm(content.id);
        } else {
            alert(content.errors);
        }
    }

    /**
     * Suppression custom pour mettre à jour le tableau :
     *      besoin d'afficher le nouvel état d'une version précédente lorsqu'on est dans index-all
     *      (la version précédente passe de dépréciée à désactivée)
     * @param table
     * @param url
     * @returns {(function(*): void)|*}
     */
    function deleteForm(table, url) {
        $(table.table).one(
            'deleted_data.table',
            () => {
                AsalaeGlobal.updatePaginationAjax('#<?=$tableId?>-section');
            }
        );
        return TableGenericAction.deleteAction(table, url);
    }

    /**
     * Publication, avec éventuelle popup si le formulaire n'est pas encore testé
     * @param {int} id
     * @param {boolean} tested
     */
    function publishForm(id, tested) {
        if (tested) {
            return ajaxPublish(id);
        }

        var popup = new AsalaePopup($('tr[data-id=' + id + '] td.action'));
        popup.element()
            .append('<?= $popupPublishContent ?>')
            .addClass('popup-not-tested');
        popup.show();
        popup.element()
            .css({
                top: '',
                bottom: 50,
                left: -165,
                'text-align': 'left'
            });

        $('.popup-test').off().on('click', function (e) {
            popup.hide();
            actionTestForm(id);
        });

        $('.popup-publish').off().on('click', function (e) {
            popup.hide();
            ajaxPublish(id);
        });

        $('.popup-cancel').off().on('click', function (e) {
            popup.hide();
        });
    }

    function ajaxPublish(id) {
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/forms/publish')?>/' + id,
            method: 'POST',
            headers: {
                Accept: 'application/json'
            },
            success: function (content, textStatus, jqXHR) {
                TableGenericAction.afterEdit(<?= $jsTable ?>, 'Forms')(content, textStatus, jqXHR);
                if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                    AsalaeGlobal.updatePaginationAjax('#<?=$tableId?>-section');
                }
            }
        })
    }

    function unpublishForm(id) {
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/forms/unpublish')?>/' + id,
            method: 'POST',
            headers: {
                Accept: 'application/json'
            },
            success: TableGenericAction.afterEdit(<?= $jsTable ?>, 'Forms')
        })
    }

    function deactivateForm(id) {
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/forms/deactivate')?>/' + id,
            method: 'POST',
            headers: {
                Accept: 'application/json'
            },
            success: TableGenericAction.afterEdit(<?= $jsTable ?>, 'Forms')
        })
    }

    function activateForm(id) {
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/forms/activate')?>/' + id,
            method: 'POST',
            headers: {
                Accept: 'application/json'
            },
            success: TableGenericAction.afterEdit(<?= $jsTable ?>, 'Forms')
        })
    }

    function duplicateForm(id) {
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/forms/duplicate')?>/' + id,
            method: 'POST',
            headers: {
                Accept: 'application/json'
            },
            success: function (content, textStatus, jqXHR) {
                if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                    let table = <?= $jsTable ?>;
                    table.data.unshift({Forms: content});
                    TableGenerator.appendActions(table.data, table.actions);
                    table.generateAll();
                    actionEditForm(content.id);
                } else {
                    alert(content.errors);
                }
            }
        })
    }
</script>
