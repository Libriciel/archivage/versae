<script>
    $('#<?=$idPrefix?>-form').find('input, textarea, select').on(
        'keyup change',
        changeEventHandle
    );
    changeEventHandle();

    function validateForm(btn)
    {
        var f = exampleForm.get(0);
        btn = $(btn);
        btn.removeClass('btn-default');
        if (f.checkValidity()) {
            btn.addClass('btn-success');
            btn.removeClass('btn-danger');
        } else {
            f.reportValidity();
            btn.addClass('btn-danger');
            btn.removeClass('btn-success');
        }
    }

    function handleEditOption() {
        var tr = $(this).closest('.row');
        var prevValue = $(this).attr('data-value');
        for (let i = 0; i < selectOptions.length; i++) {
            if (selectOptions[i].value === prevValue) {
                selectOptions[i].value = tr.find('input.value').val();
                selectOptions[i].text = tr.find('input.text').val();
                var defaultValueValue = defaultValue.val();
                defaultValue.find('option').each(
                    function() {
                        if ($(this).attr('value') === prevValue) {
                            $(this).attr('value', selectOptions[i].value).text(selectOptions[i].text);
                            return false;
                        }
                    }
                );
                if (defaultValueValue === prevValue) {
                    defaultValue.val(selectOptions[i].value);
                }
                break;
            }
        }
        optionsInput.val(JSON.stringify(selectOptions));
        changeEventHandle();
    }

    function removeOptionHandler() {
        var div = $(this).closest('div.tmpl-opt-wrapper');
        var value = div.find('[data-value]').first().attr('data-value');
        div.slideUp(
            200,
            function () {
                var value = $(this).find('input.value').val();
                for (let i = 0; i < selectOptions.length; i++) {
                    if (selectOptions[i].value === value) {
                        selectOptions.splice(i, 1);
                        break;
                    }
                }
                optionsInput.val(JSON.stringify(selectOptions));
                exampleContainer.find('> div.checkbox > label > input[value="' + CSS.escape(value) + '"]')
                    .closest('div.checkbox')
                    .remove();
                activeModal.find('#<?=$idPrefix?>-default-value option[value="' + CSS.escape(value) + '"]')
                    .remove();
                $(this).remove();
                changeEventHandle();
            }
        );
    }

    function insertOption(value, text) {
        let div = optionTmplDiv.clone();
        div.addClass('tmpl-opt-wrapper');
        div.find('input.value')
            .attr('value', value) // .val() ne fonctionne pas
            .prop('required', true)
            .attr('data-value', value)
            .attr('onchange', 'handleEditOption.call($(this))');
        div.find('input.text')
            .attr('value', text)
            .prop('required', true)
            .attr('data-value', value)
            .attr('onchange', 'handleEditOption.call($(this))');
        div.removeClass('hide');
        div.removeClass('display_options_tmpl');
        div.find('button.delete').attr('onclick', 'removeOptionHandler.call($(this))');
        displayOptionsDiv.append(div);
        defaultValue.append($('<option>').attr('value', value).text(text));
    }

    function buildOptionList(options) {
        selectOptions = options;
        var useNameAsCode = useNameAsCodeInput.prop('checked');
        for (let i = 0; i < selectOptions.length; i++) {
            selectOptions[i].value = useNameAsCode ? selectOptions[i].text : selectOptions[i].value;
            insertOption(
                selectOptions[i].value,
                selectOptions[i].text
            );
        }
        changeEventHandle();
    }

    var selectKeywordList = $('#<?=$idPrefix?>-keyword-list-id');
    $('#<?=$idPrefix?>-use-name-as-code').on(
        'change',
        function () {
            var prev = previousKeywordListId.val();
            var current = selectKeywordList.val();
            if (!prev || !current) {
                return;
            }
            var that = $(this);
            previousKeywordListId.val('-1');
            // si l'ajax ne s'est pas lancé en 100 milisecondes, on revert l'état
            var timeout = setTimeout(
                function () {
                    $(document).off('.useNameAsCode');
                    previousKeywordListId.val(prev);
                    selectKeywordList.val(current);
                    that.prop('checked', that.prop('checked') === false);
                },
                100
            );
            $(document).one('ajaxSend.useNameAsCode', function() {
                clearTimeout(timeout);
            });
            selectKeywordList.change();
        }
    );
    selectKeywordList.on(
        'change',
        function () {
            if (!$(this).val()) {
                if (previousKeywordListId.val()) {
                    if (confirm(
                        __("Retirer la liste de mots-clés supprimera les mots-clés existants. Voulez-vous continuer ?")
                    )
                    ) {
                        buildOptionList({});
                        displayOptionsDiv.empty();
                    } else {
                        $(this).val(previousKeywordListId.val());
                    }
                    return;
                }
            } else if (!confirm(
                __("Importer cette liste supprimera les mots-clés existants. Voulez-vous continuer ?")
            )
            ) {
                $(this).val(previousKeywordListId.val());
                return;
            }
            previousKeywordListId.val($(this).val());
            var loading = $('<div class="text-center loading-div">')
                .append('<i aria-hidden="true" class="fa fa-4x fa-spinner faa-spin animated">');
            displayOptionsDiv.empty().append(loading);
            $.ajax(
                {
                    url: '<?=$this->Url->build('/forms/keyword-list-content')?>/'+$(this).val(),
                    headers: {Accept: 'application/json'},
                    success: function (content) {
                        buildOptionList(content);
                        optionsInput.val(JSON.stringify(selectOptions));
                    },
                    error: function () {
                        alert(PHP.messages.genericError);
                    },
                    complete: function () {
                        loading.remove();
                    }
                }
            );
        }
    );

    if (selectOptions) {
        let originalDefaultVal = defaultValue.val();
        defaultValue.find('> option:not(:first)').remove();
        buildOptionList(selectOptions);
        defaultValue.val(originalDefaultVal);
    }

    var inputAddOptionValue = $('#value-add-option');
    var inputAddOptionText = $('#text-add-option');
    var inputAddOptionBtn = activeModal.find('.add-option-inputs input.hidden-required');
    $('.add-option-inputs button.add').off('click').on(
        'click',
        function () {
            var value = inputAddOptionValue.prop('required', true);
            var text = inputAddOptionText.prop('required', true);

            if (value.is(':invalid')) {
                value.get(0).reportValidity();
                value.prop('required', false);
                text.prop('required', false);
                return;
            }
            if (text.is(':invalid')) {
                text.get(0).reportValidity();
                value.prop('required', false);
                text.prop('required', false);
                return;
            }
            value.prop('required', false);
            text.prop('required', false);
            insertOption(value.val(), text.val());
            selectOptions.push({value: value.val(), text: text.val()});
            optionsInput.val(JSON.stringify(selectOptions));
            value.val('');
            text.val('');
            setAddOptionRequired();
            changeEventHandle();
            displayOptionsDiv.stop().animate({
                scrollTop: displayOptionsDiv[0].scrollHeight
            }, 200);
            value.focus();
        }
    );
    // champ d'ajout de valeur / texte affiché
    inputAddOptionText.data('edited', false);
    inputAddOptionText.on(
        'change',
        function() {
            inputAddOptionText.data(
                'edited',
                inputAddOptionText.val() !== inputAddOptionValue.val()
            );
        }
    );
    // si le champ text est identique au champ value,
    // les modifications du champ value se propageront sur le champ text
    inputAddOptionValue.on(
        'keyup',
        function() {
            if (!inputAddOptionText.data('edited')) {
                inputAddOptionText.val(inputAddOptionValue.val());
                setAddOptionRequired();
            }
        }
    );
    inputAddOptionValue.add(inputAddOptionText).on(
        'keypress',
        function (event) {
            setAddOptionRequired();

            // on intercepte la touche "entrée" pour ajouter l'option
            if (event.originalEvent.keyCode === 13) { // Enter
                $('.add-option-inputs button.add').click();
                event.preventDefault();
            }
        }
    );

    function setAddOptionRequired()
    {
        var hasValue = inputAddOptionValue.val() || inputAddOptionText.val();
        inputAddOptionValue.prop('required', hasValue);
        inputAddOptionText.prop('required', hasValue);
        if (hasValue) {
            inputAddOptionBtn.get(0).setCustomValidity(__("Ajouter la valeur ?"));
        } else {
            inputAddOptionBtn.get(0).setCustomValidity('');
        }
        inputAddOptionBtn.prop('required', hasValue);
    }

    if (selectOptions.length > 1000) {
        displayOptionsDiv.parent()
            .prepend(
                $('<p class="alert alert-warning">')
                    .text(
                        __(
                            "Le nombre d'options dépasse 1000. Pour garantir " +
                            "de bonnes performances dans votre navigateur, le " +
                            "déplacement des options avec la souris a été désactivé."
                        )
                    )
            );
    } else {
        displayOptionsDiv.sortable(
            {
                cursor: "grabbing",
                update: function() {
                    var lastDefaultVal = defaultValue.val();
                    defaultValue.find('> option:not(:first)').remove();
                    selectOptions = [];
                    displayOptionsDiv.find('> div').each(
                        function () {
                            var value = $(this).find('input.value').val();
                            var text = $(this).find('input.text').val();
                            selectOptions.push({value: value, text: text});
                            defaultValue.append($('<option>').attr('value', value).text(text));
                        }
                    );
                    defaultValue.val(lastDefaultVal);
                    optionsInput.val(JSON.stringify(selectOptions));
                    changeEventHandle();
                }
            }
        );
    }

    function showSortPopup(btn) {
        var popup = new AsalaePopup($(btn).closest('div'));

        var id = 'select-sort-' + uuid.v4();
        var select = $('<select class="form-control">').attr('id', id);
        var opts = [
            $('<option value="text-alpha-asc">').text(__("Ordre alphabétique sur texte affiché")),
            $('<option value="text-alpha-desc">').text(__("Ordre alphabétique inverse sur texte affiché")),
            $('<option value="value-alpha-asc">').text(__("Ordre alphabétique sur valeur")),
            $('<option value="value-alpha-desc">').text(__("Ordre alphabétique inverse sur valeur")),
            $('<option value="value-number-asc">').text(__("Ordre numérique sur valeur")),
            $('<option value="value-number-desc">').text(__("Ordre numérique inverse sur valeur")),
        ];
        for (let i in opts) {
            select.append(opts[i]);
        }
        var label = $('<label>').attr('for', id).text(__("Changer l'ordre des options"));
        var formGroup = $('<div class="form-group select">')
            .append(label)
            .append($('<div class="form-group-wrapper">').append(select));

        popup.element().append(formGroup).css({top: 0, left: 0});

        var submit = $('<button class="btn btn-default btn-small" type="button">')
            .append('<i class="fa fa-sort-amount-down" aria-hidden="true"></i>')
            .append($('<span>').text(__("Filtrer")))
            .on('click', function() {
                popup.hide();
                var lastDefaultVal = defaultValue.val();
                defaultValue.find('> option:not(:first)').remove();
                var [field, func, dir] = select.val().split('-');

                selectOptions.sort(function(a, b) {
                    var res;
                    if (func === 'alpha') {
                        res = a[field].localeCompare(b[field]);
                    } else {
                        res = parseInt(a[field]) - parseInt(b[field]);
                    }
                    if (dir === 'desc') {
                        res = res * -1;
                    }
                    return res;
                });
                optionsInput.val(JSON.stringify(selectOptions));

                var loading = $('<div class="text-center loading-div">')
                    .append('<i aria-hidden="true" class="fa fa-4x fa-spinner faa-spin animated">');

                // reconstruit le tableau
                displayOptionsDiv.empty().append(loading);
                for (let i = 0; i < selectOptions.length; i++) {
                    insertOption(
                        selectOptions[i].value,
                        selectOptions[i].text
                    );
                }
                loading.remove();

                defaultValue.val(lastDefaultVal);
                changeEventHandle();
            });
        popup.element().append(submit);

        popup.show();
    }
</script>
