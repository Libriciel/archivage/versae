<?php

/**
 * @var Versae\View\AppView $this
 */

$tabs = $this->Tabs->create('tabs-add-input-text', ['class' => 'row no-padding']);

$infos = $this->Form->control(
    'label',
    [
        'label' => __("Nom du champ affiché"),
    ]
);
$infos .= $this->Form->control(
    'name',
    [
        'label' => __("Attribut `name` du champ (identifiant twig)"),
        'pattern' => '[a-zA-Z][a-zA-Z0-9_]*',
        'help' => __(
            "Autorisés: lettres, chiffres et underscores `_` (obligatoirement une lettre en premier caractère)"
        ),
    ]
);
$infos .= $this->Form->control(
    'default_value',
    [
        'label' => __("Valeur par défaut"),
    ]
);
$infos .= $this->Form->control(
    'placeholder',
    [
        'label' => __("Attribut `placeholder`"),
        'placeholder' => __("Ceci est un placeholder"),
        'help' => __("Message affiché en gris à l'intérieur du champ lorsqu'il est vide"),
    ]
);
$infos .= $this->Form->control(
    'help',
    [
        'label' => __("Message d'aide sous le champ"),
        'help' => __("Ceci est le message d'aide"),
    ]
);
$infos .= $this->Form->control(
    'required',
    [
        'label' => __("Champ obligatoire"),
    ]
);
$infos .= $this->Form->control(
    'readonly',
    [
        'label' => __("Lecture seule"),
    ]
);
$infos .= $this->Form->control(
    'pattern',
    [
        'label' => __("Attribut `pattern`"),
        'help' => __("Permet la validation par expression régulière")
            . '<br>'
            . __("exemple: date au format yyyy-mm-dd: [0-9]{4}-[0-9]{2}-[0-9]{2}")
            . '<br>'
            . __("exemple: url: http(s)?://[\w.-]+\.(com|fr)"),
    ]
);
$infos .= $this->Form->control(
    'pattern_select',
    [
        'label' => __("Ajouter un élément au pattern"),
        'empty' => __("-- Ajouter un élément au pattern --"),
        'options' => [
            '[0-9]*' => __("zéro à plusieurs chiffres"),
            '[0-9]+' => __("un à plusieurs chiffres"),
            '[0-9]{2}' => __("deux chiffres consécutifs"),
            '(0[1-9]|1[0-2])' => __("chiffres de 01 à 12"),
            '(0[1-9]|[1-2][0-9]|3[0-1])' => __("chiffres de 01 à 31"),
            '[0-9]{4}' => __("quatre chiffres consécutifs"),
            '(19|20)[0-9]{2}' => __("chiffres de 1900 à 2099"),
            '-' => __("Séparateur tiret `-`"),
            '_' => __("Séparateur underscore `_`"),
            '\.' => __("Séparateur point `.`"),
            ' ' => __("Séparateur espace ` `"),
            '@' => __("Séparateur arobase `@`"),
            '/' => __("Séparateur slash `/`"),
            ':' => __("Séparateur deux points `:`"),
            '[a-z\u00E0-\u00FF]*' => __("zéro à plusieurs lettres en minuscules"),
            '[A-Z\u00C0-\u00D9]*' => __("zéro à plusieurs lettres en majuscules"),
            '[a-zA-Z\u00C0-\u00FF]*' => __("zéro à plusieurs lettres (majuscules et minuscules)"),
            '[\w\u00C0-\u00FF-]*' => __("zéro à plusieurs caractères (lettres, chiffres, tiret et underscores)"),
            '.*' => __("zéro ou n'importe quelle suite de caractères"),
            '[a-z\u00E0-\u00FF]+' => __("une à plusieurs lettres en minuscules"),
            '[A-Z\u00C0-\u00D9]+' => __("une à plusieurs lettres en majuscules"),
            '[a-zA-Z\u00C0-\u00FF]+' => __("une à plusieurs lettres (majuscules et minuscules)"),
            '[\w\u00C0-\u00FF-]+' => __("un à plusieurs caractères (lettres, chiffres, tiret et underscores)"),
            '.+' => __("un à n'importe quelle suite de caractères"),
        ],
    ]
);

$readonly = ($linked ?? false)
    ? [
        'readonly' => true,
        'onclick' => 'return false',
        'style' => 'cursor: not-allowed',
    ]
    : [];
if ($readonly) {
    $infos .= $this->Html->tag('div', null, ['title' => __("Champ utilisé, cet attribut ne peut être changé")]);
}
$infos .= $this->Form->control(
    'multiple',
    [
        'label' => __("Champ à valeur multiple"),
    ] + $readonly
);
if ($readonly) {
    $infos .= $this->Html->tag('/div');
}

$tabs->add(
    'tab-add-input-text-infos',
    $this->Fa->i('fa-file-code-o', __("Informations principales")),
    $infos
);
$tabs->add(
    'tab-add-input-visibility',
    $this->Fa->i('fa-eye-slash', __("Conditions de masquage")),
    require 'conditions.php'
);

echo $tabs;

echo $this->Form->end();

echo $this->Html->tag(
    'div',
    '',
    [
        'class' => 'row',
        'style' => 'height: 5px;background-color: #e9e9e9;border-top: 1px solid #ddd;'
            . 'border-bottom: 1px solid #ddd;margin-bottom: 15px;',
    ]
);
echo $this->Form->create(null, ['id' => $idPrefix . '-example-form', 'idPrefix' => $idPrefix . '-example']);
echo $this->Form->fieldset(
    $this->Form->control(
        'example',
        [
            'label' => __("Exemple"),
            'class' => 'example-input',
            'help' => '&nbsp;',
        ]
    )
    . $this->Html->tag(
        'button',
        __("Tester la validation du champ"),
        [
            'class' => 'btn btn-default btn-validity',
            'type' => 'button',
            'onclick' => 'validateForm(this)'
        ]
    ),
    ['legend' => __("Prévisualisation du champ de formulaire")]
);
echo $this->Form->end();
?>
<script>
    var exampleInput = $('#<?=$idPrefix?>-example-example');
    var exampleContainer = exampleInput.closest('.form-group');
    var exampleLabel = exampleContainer.find('label');
    var exampleHelp = exampleContainer.find('.help-block');
    var form = exampleInput.closest('form');
    var btnValidity = form.find('.btn-validity');
    var defaultLabel = exampleLabel.text();

    $('#<?=$idPrefix?>-form').find('input, textarea, select').on(
        'keyup change',
        function () {
            var label = $('#<?=$idPrefix?>-label').val().trim();
            var required = $('#<?=$idPrefix?>-required').is(':checked');
            $('#<?=$idPrefix?>-readonly').prop('disabled', required);
            var readonly = $('#<?=$idPrefix?>-readonly').is(':checked');
            $('#<?=$idPrefix?>-required').prop('disabled', readonly);
            if (!label) {
                label = defaultLabel;
            }
            exampleLabel.text(label);
            exampleInput.val($('#<?=$idPrefix?>-default-value').val());
            exampleInput.attr('placeholder', $('#<?=$idPrefix?>-placeholder').val());
            var pattern = $('#<?=$idPrefix?>-pattern').val();
            exampleInput.attr('pattern', pattern ? pattern : null);
            exampleHelp.text($('#<?=$idPrefix?>-help').val());
            exampleContainer.toggleClass('required', required);
            exampleInput.prop('required', required);
            exampleInput.prop('readonly', readonly);
            btnValidity.removeClass('btn-danger');
            btnValidity.removeClass('btn-success');
            btnValidity.addClass('btn-default');
            handleMultipleText();
        }
    );

    function validateForm(btn)
    {
        var f = exampleInput.get(0);
        btn = $(btn);
        btn.removeClass('btn-default');
        if (f.checkValidity()) {
            btn.addClass('btn-success');
            btn.removeClass('btn-danger');
        } else {
            f.reportValidity();
            btn.addClass('btn-danger');
            btn.removeClass('btn-success');
        }
    }

    $('#<?=$idPrefix?>-pattern-select').on(
        'change',
        function() {
            var pattern = $('#<?=$idPrefix?>-pattern');
            pattern.val(pattern.val() + $(this).val());
            $(this).val('');
            pattern.trigger('change');
        }
    );

    $('#<?=$idPrefix?>-label').change();

    function handleMultipleText()
    {
        var span = $('<button type="button" class="input-group-addon">');
        span.attr('title', __("Ajouter"));
        span.append($('<i aria-hidden="true" class="fa fa-plus">'));
        span.append($('<span class="sr-only">').text(__("Ajouter")));
        var multiple = $('#<?=$idPrefix?>-multiple').prop('checked');
        var exemple = $('#<?=$idPrefix?>-example-example').closest('.form-group-wrapper');
        var inputGroup = exemple.find('.input-group');
        var help = exemple.find('.help-block');
        if (!multiple) {
            inputGroup.find('.input-group-addon').remove();
            inputGroup.find('input').prependTo(inputGroup.parent());
            inputGroup.remove();
            return;
        }
        if (inputGroup.length === 0) {
            inputGroup = $('<div class="input-group">');
            inputGroup.append(exemple.find('input'));
            inputGroup.prependTo(exemple);
        }
        inputGroup.find('.input-group-addon').remove();
        inputGroup.append(span);
    }
</script>
