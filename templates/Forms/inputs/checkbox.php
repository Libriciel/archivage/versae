<?php

/**
 * @var Versae\View\AppView $this
 */

$tabs = $this->Tabs->create('tabs-add-input-text', ['class' => 'row no-padding']);

$infos = $this->Form->control(
    'label',
    [
        'label' => __("Nom du champ affiché"),
    ]
);
$infos .= $this->Form->control(
    'name',
    [
        'label' => __("Attribut `name` du champ (identifiant twig)"),
        'pattern' => '[a-zA-Z][a-zA-Z0-9_]*',
        'help' => __(
            "Autorisés: lettres, chiffres et underscores `_` (obligatoirement une lettre en premier caractère)"
        ),
    ]
);
$infos .= $this->Form->control(
    'help',
    [
        'label' => __("Message d'aide sous le champ"),
        'help' => __("Ceci est le message d'aide"),
    ]
);
$infos .= $this->Form->control(
    'value',
    [
        'label' => __("Valeur si coché"),
        'required' => true,
        'default' => '1',
    ]
);
$infos .= $this->Form->control(
    'checked',
    [
        'label' => __("Coché par défaut"),
    ]
);

$tabs->add(
    'tab-add-input-text-infos',
    $this->Fa->i('fa-file-code-o', __("Informations principales")),
    $infos
);
$tabs->add(
    'tab-add-input-visibility',
    $this->Fa->i('fa-eye-slash', __("Conditions de masquage")),
    require 'conditions.php'
);

echo $tabs;

echo $this->Form->end();

echo $this->Html->tag(
    'div',
    '',
    [
        'class' => 'row',
        'style' => 'height: 5px;background-color: #e9e9e9;border-top: 1px solid #ddd;'
            . 'border-bottom: 1px solid #ddd;margin-bottom: 15px;',
    ]
);
echo $this->Form->create(null, ['id' => $idPrefix . '-example-form', 'idPrefix' => $idPrefix . '-example']);
echo $this->Form->fieldset(
    $this->Form->control(
        'example',
        [
            'label' => __("Exemple"),
            'class' => 'example-input',
            'type' => 'checkbox',
            'help' => '&nbsp;',
            'templates' => [
                'checkboxContainer' => '<div class="form-group checkbox {{required}}">{{content}}<br>{{append}}</div>',
            ],
        ]
    ),
    ['legend' => __("Prévisualisation du champ de formulaire")]
);
echo $this->Form->end();
?>
<script>
    var exampleInput = $('#<?=$idPrefix?>-example-example');
    var exampleContainer = exampleInput.closest('.form-group');
    var exampleLabel = exampleContainer.find('label');
    var exampleHelp = exampleContainer.find('.help-block');
    var form = exampleInput.closest('form');
    var btnValidity = form.find('.btn-validity');
    var defaultLabel = exampleLabel.text();

    $('#<?=$idPrefix?>-form').find('input, textarea, select').on(
        'keyup change',
        function () {
            var label = $('#<?=$idPrefix?>-label').val().trim();
            if (!label) {
               label = defaultLabel;
            }
            exampleLabel.contents()
                .filter(function() {
                    return this.nodeType === 3; //Node.TEXT_NODE
                })
                .first()
                .get(0)
                .nodeValue = $('<div>').html(label).text();
            exampleHelp.text($('#<?=$idPrefix?>-help').val());
            exampleInput.attr('value', $('#<?=$idPrefix?>-value').val());
            exampleInput.prop('checked', $('#<?=$idPrefix?>-checked').is(':checked'));
        }
    );

    $('#<?=$idPrefix?>-label').change();
</script>
