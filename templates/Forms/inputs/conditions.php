<?php

/**
 * @var Versae\View\AppView $this
 */

use Versae\Model\Table\FormInputsTable;

$type = $type ?? '';

$conditions = $this->Form->fieldset(
    $this->Form->control(
        'cond_display_input',
        [
            'label' => __("L'affichage est conditionné par le champ suivant"),
            'empty' => __("-- Sélectionner le champ ciblé --"),
            'options' => $targets,
        ]
    )
    . $this->Form->control(
        'cond_display_way',
        [
            'label' => __("Sens"),
            'type' => 'radio',
            'options' => [
                'show_if' => __("Afficher l'élément si"),
                'hide_if' => __("Cacher l'élément si"),
            ],
            'default' => 'show_if',
        ]
    )
    . $this->Form->control(
        'cond_display_field',
        [
            'label' => __("Le champ"),
            'type' => 'radio',
            'options' => [
                [
                    'text' => __("N'est pas vide"),
                    'value' => 'standard_not_empty',
                    'data-type' => 'standard',
                ],
                [
                    'text' => __("Est vide"),
                    'value' => 'standard_empty',
                    'data-type' => 'standard',
                ],
                [
                    'text' => __("A Comme valeur :"),
                    'value' => 'standard_value',
                    'data-type' => 'standard',
                    'data-custom-value' => '1',
                ],
                [
                    'text' => __("A une valeur sélectionnée"),
                    'value' => 'select_not_empty',
                    'data-type' => 'select',
                ],
                [
                    'text' => __("N'a aucune valeur sélectionnée"),
                    'value' => 'select_empty',
                    'data-type' => 'select',
                ],
                [
                    'text' => __("A comme valeur sélectionnée :"),
                    'value' => 'select_value',
                    'data-type' => 'select',
                    'data-custom-value' => '1',
                ],
                [
                    'text' => __("A au moins une valeur sélectionnée"),
                    'value' => 'select_multiple_not_empty',
                    'data-type' => 'select_multiple',
                ],
                [
                    'text' => __("N'a aucune valeur sélectionnée"),
                    'value' => 'select_multiple_empty',
                    'data-type' => 'select_multiple',
                ],
                [
                    'text' => __("A parmi les valeurs sélectionnées :"),
                    'value' => 'select_multiple_value',
                    'data-type' => 'select_multiple',
                    'data-custom-value' => '1',
                ],
                [
                    'text' => __("Est coché"),
                    'value' => 'checkbox_not_empty',
                    'data-type' => 'checkbox',
                ],
                [
                    'text' => __("N'est pas coché"),
                    'value' => 'checkbox_empty',
                    'data-type' => 'checkbox',
                ],
            ],
        ]
    )
    . $this->Form->control(
        'cond_display_value',
        [
            'label' => __("La valeur suivante"),
            'required' => true,
        ]
    )
    . $this->Form->control(
        'cond_display_value_select',
        [
            'label' => __("Au moins l'une des valeurs suivantes"),
            'empty' => __("-- Sélectionner les valeurs ---"),
            'options' => [],
            'required' => true,
            'multiple' => true,
        ]
    )
    . $this->Form->control(
        'cond_display_value_file',
        [
            'label' => __("Un ou des fichier(s)"),
            'required' => true,
            'options' => [
                'is_selected' => __("ont été séléctionnés"),
                'is_not_selected' => __("n'ont pas été séléctionnés"),
            ],
        ]
    ),
    ['legend' => $legend ?? __("Conditions de masquage du champ de formulaire")]
);

$jsonInputsData = json_encode(
    array_values(
        array_filter($inputsData, fn($v) => $v['type'] !== FormInputsTable::TYPE_PARAGRAPH)
    )
);
$initialCondDisplayValue = isset($entity)
    ? $entity->get('cond_display_value')
    : $form->getData('cond_display_value');
$initialCondDisplayValueSelect = isset($entity)
    ? $entity->get('cond_display_value_select')
    : $form->getData('cond_display_value_select');
$initialCondDisplayValueFile = isset($entity)
    ? $entity->get('cond_display_value_file')
    : $form->getData('cond_display_value_file');
$jsonFieldsetsData = json_encode($fieldsets ?: []);

if (is_string($initialCondDisplayValueSelect)) {
    $initialCondDisplayValueSelect = (array)$initialCondDisplayValueSelect;
}
$initialCondDisplayValueSelect = json_encode($initialCondDisplayValueSelect);
$currentFieldset = $this->getRequest()->getData('fieldset');
if ($currentFieldset) {
    $currentFieldset = json_encode($currentFieldset) ?: 'null';
} else {
    $currentFieldset = 'null';
}

/** @noinspection JSUnusedAssignment JSXUnresolvedComponent */
$conditions .= <<<JS
<script>
    var inputsData = $jsonInputsData;
    var fieldsetsData = $jsonFieldsetsData;
    var currentFieldset = $currentFieldset;
    var disableExpInputSelect = $('#$idPrefix-cond-display-input');
    var disableExpInputWayDiv = $('#$idPrefix-cond-display-way').closest('.form-group');
    var disableExpInputFieldDiv = $('#$idPrefix-cond-display-field').closest('.form-group');
    var disableExpInputValue = $('#$idPrefix-cond-display-value');
    var disableExpInputValueSelect = $('#$idPrefix-cond-display-value-select');
    var disableExpInputValueFile = $('#$idPrefix-cond-display-value-file');

   // liste les inputs liés aux sections répétables
   var repeatableInputs = [];
   for (let i = 0; i < fieldsetsData.length; i++) {
       if (!fieldsetsData[i].repeatable 
           || fieldsetsData[i].repeatable === '0'
           || fieldsetsData[i].repeatable === 'false'
           || !fieldsetsData[i].inputs
       ) {
           continue;
       }
       for (let j = 0; j < fieldsetsData[i].inputs.length; j++) {
           let inputData = JSON.parse(fieldsetsData[i].inputs[j]);
           repeatableInputs.push(
               {name: inputData.name, fieldset: fieldsetsData[i].legend}
           );
       }
   }
   disableExpInputSelect.find('> option').each(
       function() {
           var value = $(this).attr('value');
           for (let i = 0; i < repeatableInputs.length; i++) {
               if (repeatableInputs[i].name !== value) {
                   continue;
               }
               if (!currentFieldset) {
                   $(this).disable()
                       .attr('title', __("Cette option fait partie d'une section répétable"))
                       .attr('data-name', repeatableInputs[i].name);
               } else if (currentFieldset.legend !== repeatableInputs[i].fieldset) {
                   $(this).disable().attr('title', __("Cette option fait partie d'une autre section répétable"));
               }
           }
       }
   );

    function getInputDataByName(name)
    {
        for (let i = 0; i < inputsData.length; i++) {
            if (inputsData[i].name === name) {
                return inputsData[i];
            }
        }
        return {};
    }

   var disableInputData;
   disableExpInputSelect.on(
       'change',
       function() {
           var value = $(this).val();
           // si il n'y a pas de valeur dans "L'affichage est conditionné par le champ suivant" on cache tout
           if (!value) {
               disableExpInputWayDiv.hide().find('input').prop('disabled', true);
               disableExpInputFieldDiv.hide().find('input').prop('disabled', true);
               disableExpInputValue.prop('disabled', true).closest('.form-group').hide();
               disableExpInputValueSelect.closest('.form-group').hide().find('input, select').prop('disabled', true);
               disableExpInputValueFile.prop('disabled', true).closest('.form-group').hide();
               return;
           }
           disableExpInputWayDiv.show().find('input').prop('disabled', false);
           if (disableExpInputWayDiv.find(':checked').length === 0) {
               disableExpInputWayDiv.find('input[type="radio"]').first().prop('checked', true);
           }
           
           // On affiche les options du "Le champ" en fonction du type de l'input sélectionné
           disableExpInputFieldDiv.show();
           var fieldValue = disableExpInputFieldDiv.find('input:checked').val();
           disableExpInputFieldDiv.find('input[type="radio"]').prop('disabled', true).prop('checked', false).each(
               function () {
                   $(this).closest('div.radio').hide(); // label du radio
               }
           );
           disableInputData = getInputDataByName(value);
           var fieldType;
           if (['text', 'number', 'email', 'textarea', 'date', 'datetime'].indexOf(disableInputData.type) !== -1) {
               fieldType = 'standard';
           } else if (['select', 'multi_checkbox'].indexOf(disableInputData.type) !== -1
               && disableInputData.multiple
           ) {
               fieldType = 'select_multiple';
           } else if (['select', 'radio'].indexOf(disableInputData.type) !== -1) {
               fieldType = 'select';
           } else if (disableInputData.type === 'checkbox') {
               fieldType = 'checkbox';
           } else { // type fichier
               disableExpInputFieldDiv.hide().find('input').prop('disabled', true);
               disableExpInputValueFile.prop('disabled', false).closest('.form-group').show();
               disableExpInputValue.prop('disabled', true).closest('.form-group').hide();
               disableExpInputValueSelect.closest('.form-group').hide().find('input, select').prop('disabled', true);
               return;
           }
           disableExpInputFieldDiv.find('input[data-type="'+fieldType+'"]').prop('disabled', false).each(
               function () {
                   $(this).closest('div.radio').show(); // label du radio
               }
           );
           // remet la valeur si elle n'est pas caché par un changement de type
           if (disableExpInputFieldDiv.find('input[value="'+fieldValue+'"]:visible').length > 0) {
               disableExpInputFieldDiv.find('input[value="'+fieldValue+'"]:visible').prop('checked', true);
           } else {
               disableExpInputFieldDiv.find('input[type="radio"]:visible').first().prop('checked', true);
           }
           disableExpInputFieldDiv.find('input[type="radio"]').first().change();
       }
   ).change();
    disableExpInputFieldDiv.find('input[type="radio"]').on(
        'change', 
        function() {
            disableExpInputValue.prop('disabled', true).closest('.form-group').hide();
            disableExpInputValueFile.prop('disabled', true).closest('.form-group').hide();
            disableExpInputValueSelect.closest('.form-group').hide().find('input, select').prop('disabled', true);
            if (!disableInputData) {
               return;
            }
            if ($(this).attr('data-custom-value')) {
                switch ($(this).attr('data-type')) {
                    case 'standard':
                        disableExpInputValue.val("$initialCondDisplayValue");
                        disableExpInputValue.prop('disabled', false).closest('.form-group').show();
                        break;
                    case 'select':
                    case 'select_multiple':
                        disableExpInputValueSelect.closest('.form-group')
                            .show()
                            .find('input, select')
                            .prop('disabled', false);
                        if (disableInputData.options) {
                            let opts = JSON.parse(disableInputData.options);
                            disableExpInputValueSelect.val('').empty();
                            disableExpInputValueSelect.prop('multiple', true);
                            for (let i = 0; i < opts.length; i++) {
                                disableExpInputValueSelect.append(
                                    $('<option>').attr('value', opts[i].value)
                                        .text(opts[i].value + ' - ' + opts[i].text)
                                );
                            }
                            let selectValues = $initialCondDisplayValueSelect;
                            disableExpInputValueSelect.val(selectValues);
                            AsalaeGlobal.select2(disableExpInputValueSelect);
                        }
                        break;
                    case 'checkbox':
                        disableExpInputValueSelect.empty();
                        disableExpInputValueSelect.prop('multiple', false);
                        disableExpInputValueSelect.append(
                            $('<option>').attr('value', 'checked')
                                .text(__("La case est cochée"))
                        );
                        disableExpInputValueSelect.append(
                            $('<option>').attr('value', 'not_checked')
                                .text(__("La case n'est pas cochée"))
                        );
                        let selectValues = $initialCondDisplayValueSelect;
                        if (selectValues && selectValues.length) {
                            disableExpInputValueSelect.val(selectValues);
                        } else {
                            disableExpInputValueSelect.val("checked");
                        }
                        disableExpInputValueSelect.closest('.form-group')
                            .show()
                            .find('input, select')
                            .prop('disabled', false);
                        AsalaeGlobal.select2(disableExpInputValueSelect);
                        break;
                }
            }
        }
    );
    setTimeout(() => disableExpInputFieldDiv.find(':checked').prop('checked', false).click(), 10);
</script>
JS;

return $conditions;
