<?php

/**
 * @var Versae\View\AppView $this
 */

$tabs = $this->Tabs->create('tabs-add-input-email', ['class' => 'row no-padding']);

$infos = $this->Form->control(
    'label',
    [
        'label' => __("Nom du champ affiché"),
    ]
);
$infos .= $this->Form->control(
    'name',
    [
        'label' => __("Attribut `name` du champ (identifiant twig)"),
        'pattern' => '[a-zA-Z][a-zA-Z0-9_]*',
        'help' => __(
            "Autorisés: lettres, chiffres et underscores `_` (obligatoirement une lettre en premier caractère)"
        ),
    ]
);
$infos .= $this->Form->control(
    'default_value',
    [
        'label' => __("Valeur par défaut"),
    ]
);
$infos .= $this->Form->control(
    'placeholder',
    [
        'label' => __("Attribut `placeholder`"),
        'placeholder' => __("Ceci est un placeholder"),
        'help' => __("Message affiché en gris à l'intérieur du champ lorsqu'il est vide"),
    ]
);
$infos .= $this->Form->control(
    'help',
    [
        'label' => __("Message d'aide sous le champ"),
        'help' => __("Ceci est le message d'aide"),
    ]
);
$infos .= $this->Form->control(
    'required',
    [
        'label' => __("Champ obligatoire"),
    ]
);
$infos .= $this->Form->control(
    'readonly',
    [
        'label' => __("Lecture seule"),
    ]
);

$tabs->add(
    'tab-add-input-email-infos',
    $this->Fa->i('fa-file-code-o', __("Informations principales")),
    $infos
);
$tabs->add(
    'tab-add-input-visibility',
    $this->Fa->i('fa-eye-slash', __("Conditions de masquage")),
    require 'conditions.php'
);

echo $tabs;

echo $this->Form->end();

echo $this->Html->tag(
    'div',
    '',
    [
        'class' => 'row',
        'style' => 'height: 5px;background-color: #e9e9e9;border-top: 1px solid #ddd;'
            . 'border-bottom: 1px solid #ddd;margin-bottom: 15px;',
    ]
);
echo $this->Form->create(null, ['id' => $idPrefix . '-example-form', 'idPrefix' => $idPrefix . '-example']);
echo $this->Form->fieldset(
    $this->Form->control(
        'example',
        [
            'label' => __("Exemple"),
            'class' => 'example-input',
            'help' => '&nbsp;',
            'type' => 'email',
            'append' => $this->Fa->i('fa-envelope-o'),
        ]
    )
    . $this->Html->tag(
        'button',
        __("Tester la validation du champ"),
        [
            'class' => 'btn btn-default btn-validity',
            'type' => 'button',
            'onclick' => 'validateForm(this)',
        ]
    ),
    ['legend' => __("Prévisualisation du champ de formulaire")]
);
echo $this->Form->end();
?>
<script>
    var exampleInput = $('#<?=$idPrefix?>-example-example');
    var exampleContainer = exampleInput.closest('.form-group');
    var exampleLabel = exampleContainer.find('label');
    var exampleHelp = exampleContainer.find('.help-block');
    var form = exampleInput.closest('form');
    var btnValidity = form.find('.btn-validity');
    var defaultLabel = exampleLabel.text();

    $('#<?=$idPrefix?>-form').find('input, textarea, select').on(
        'keyup change',
        function () {
            var label = $('#<?=$idPrefix?>-label').val().trim();
            var required = $('#<?=$idPrefix?>-required').is(':checked');
            $('#<?=$idPrefix?>-readonly').prop('disabled', required);
            var readonly = $('#<?=$idPrefix?>-readonly').is(':checked');
            $('#<?=$idPrefix?>-required').prop('disabled', readonly);
            if (!label) {
                label = defaultLabel;
            }
            exampleLabel.text(label);
            exampleInput.val($('#<?=$idPrefix?>-default-value').val());
            exampleInput.attr('placeholder', $('#<?=$idPrefix?>-placeholder').val());
            exampleHelp.text($('#<?=$idPrefix?>-help').val());
            exampleContainer.toggleClass('required', required);
            exampleInput.prop('required', required);
            exampleInput.prop('readonly', readonly);
            btnValidity.removeClass('btn-danger');
            btnValidity.removeClass('btn-success');
            btnValidity.addClass('btn-default');
        }
    );

    function validateForm(btn)
    {
        var f = exampleInput.get(0);
        btn = $(btn);
        btn.removeClass('btn-default');
        if (f.checkValidity()) {
            btn.addClass('btn-success');
            btn.removeClass('btn-danger');
        } else {
            f.reportValidity();
            btn.addClass('btn-danger');
            btn.removeClass('btn-success');
        }
    }

    $('#<?=$idPrefix?>-label').change();
</script>
