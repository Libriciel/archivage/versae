<?php

/**
 * @var Versae\View\AppView $this
 */

$tabs = $this->Tabs->create('tabs-add-input-hidden', ['class' => 'row no-padding']);

$infos = $this->Form->control(
    'name',
    [
        'label' => __("Attribut `name` du champ (identifiant twig)"),
        'pattern' => '[a-zA-Z][a-zA-Z0-9_]*',
        'help' => __(
            "Autorisés: lettres, chiffres et underscores `_` (obligatoirement une lettre en premier caractère)"
        ),
    ]
);
$infos .= $this->Form->control(
    'value',
    [
        'label' => __("Valeur"),
    ]
);

$tabs->add(
    'tab-add-input-hidden-infos',
    $this->Fa->i('fa-file-code-o', __("Informations principales")),
    $infos
);
$tabs->add(
    'tab-add-input-visibility',
    $this->Fa->i('fa-eye-slash', __("Conditions de masquage")),
    require 'conditions.php'
);

echo $tabs;

echo $this->Form->end();

echo $this->Html->tag(
    'div',
    '',
    [
        'class' => 'row',
        'style' => 'height: 5px;background-color: #e9e9e9;border-top: 1px solid #ddd;'
            . 'border-bottom: 1px solid #ddd;margin-bottom: 15px;',
    ]
);
echo $this->Form->create(null, ['id' => $idPrefix . '-example-form', 'idPrefix' => $idPrefix . '-example']);
echo $this->Form->fieldset(
    $this->Html->tag('div.opacity-1')
    . $this->Form->control(
        'example',
        [
            'label' => false,
            'aria-disabled' => 'true',
            'class' => 'example-input',
        ]
    )
    . $this->Html->tag('/div'),
    ['legend' => __("Prévisualisation du champ de formulaire")]
);
echo $this->Form->end();
?>
<script>
    var exampleInput = $('#<?=$idPrefix?>-example-example');
    var exampleContainer = exampleInput.closest('.form-group');
    var form = exampleInput.closest('form');
    var btnValidity = form.find('.btn-validity');

    $('#<?=$idPrefix?>-form').find('input, textarea, select').on(
        'keyup change',
        function () {
            exampleInput.val($('#<?=$idPrefix?>-value').val());
        }
    );

    $('#<?=$idPrefix?>-value').change();
</script>
