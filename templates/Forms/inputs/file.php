<?php

/**
 * @var Versae\View\AppView $this
 */

?>
<script>
    var required = false;
    function selectableFileupload(value, context) {
        var multiple = $('#<?=$idPrefix?>-multiple').prop('checked');
        return $('<input>').val(context.id)
            .attr('name', $('#<?=$idPrefix?>-name').val()+(multiple ? '[]' : ''))
            .attr('aria-label', context.name)
            .attr('type', multiple ? 'checkbox' : 'radio')
            .prop('required', required)
            .prop('checked', true);
    }
</script>
<?php
$tabs = $this->Tabs->create('tabs-add-input-file', ['class' => 'row no-padding']);

$infos = $this->Form->control(
    'label',
    [
        'label' => __("Nom du champ affiché"),
    ]
);
$infos .= $this->Form->control(
    'name',
    [
        'label' => __("Attribut `name` du champ (identifiant twig)"),
        'pattern' => '[a-zA-Z][a-zA-Z0-9_]*',
        'help' => __(
            "Autorisés: lettres, chiffres et underscores `_` (obligatoirement une lettre en premier caractère)"
        ),
    ]
);
$infos .= $this->Form->control(
    'formats',
    [
        'label' => __("Format(s) autorisé(s)"),
        'options' => [
            'image' => __("Image"),
            'video' => __("Video"),
            'audio' => __("Audio"),
            'archive' => __("Archives (zip/tar.gz)"),
            'xml' => __("Fichier XML"),
            'json' => __("Fichier JSON"),
            'csv' => __("Fichier CSV"),
            '---------------------------- ' => [],
            'rng_xsd' => __("Fichier schémas (RNG ou XSD)"),
            'rng' => __("Fichier schéma RNG"),
            'xsd' => __("Fichier schéma XSD"),
            '----------------------------  ' => [],
            'text' => __("Fichier texte"),
            'calc' => __("Fichier calc"),
            'presentation' => __("Fichier présentation"),
            'office' => __("Fichiers office"),
            'pdf' => __("Fichiers pdf"),
        ],
        'multiple' => true,
        'data-placeholder' => __("-- Laissez vide pour autoriser tous les formats --"),
    ]
);
$infos .= $this->Form->control(
    'help',
    [
        'label' => __("Message d'aide sous le champ"),
        'help' => __("Ceci est le message d'aide"),
    ]
);
$infos .= $this->Form->control(
    'required',
    [
        'label' => __("Champ obligatoire"),
    ]
);

$readonly = ($linked ?? false)
    ? [
        'readonly' => true,
        'onclick' => 'return false',
        'style' => 'cursor: not-allowed',
    ]
    : [];

if ($readonly) {
    $infos .= $this->Html->tag('div', null, ['title' => __("Champ utilisé, cet attribut ne peut être changé")]);
}
$infos .= $this->Form->control(
    'multiple',
    [
        'label' => __("Champ à valeur multiple"),
    ] + $readonly
);
if ($readonly) {
    $infos .= $this->Html->tag('/div');
}

$tabs->add(
    'tab-add-input-file-infos',
    $this->Fa->i('fa-file-code-o', __("Informations principales")),
    $infos
);
$tabs->add(
    'tab-add-input-visibility',
    $this->Fa->i('fa-eye-slash', __("Conditions de masquage")),
    require 'conditions.php'
);

echo $tabs;

echo $this->Form->end();

echo $this->Html->tag(
    'div',
    '',
    [
        'class' => 'row',
        'style' => 'height: 5px;background-color: #e9e9e9;border-top: 1px solid #ddd;'
            . 'border-bottom: 1px solid #ddd;margin-bottom: 15px;',
    ]
);

$uploads = $this->Upload
    ->create('create-upload-file', ['class' => 'table table-striped table-hover hide'])
    ->fields(
        [
            'selection' => [
                'label' => __("Sélection"),
                'callback' => 'selectableFileupload',
                'class' => 'selection',
            ],
            'name' => [
                'label' => __("Nom de fichier"),
                'callback' => 'TableHelper.filenameToPopup',
            ],
            'message' => [
                'label' => __("Message"),
                'style' => 'min-width: 200px; max-width: 400px',
                'class' => 'message',
            ],
        ]
    )
    ->data([])
    ->params(
        [
            'identifier' => 'id',
            'classEval' => 'data[{index}].valid !== false ? "" : "danger"',
        ]
    )
    ->actions(
        [
            function ($table) {
                $url = $this->Url->build('/Upload/delete');
                return [
                    'onclick' => "GenericUploader.fileDelete($table->tableObject, '$url', {0})",
                    'type' => 'button',
                    'class' => 'btn-link delete',
                    'displayEval' => 'AsalaeGlobal.is_numeric(data[{index}].id)',
                    'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'params' => ['id', 'name']
                ];
            },
        ]
    );

echo $this->Form->create(null, ['id' => $idPrefix . '-example-form', 'idPrefix' => $idPrefix . '-example']);
echo $this->Form->fieldset(
    $this->Html->tag(
        'div.form-group.fake-input',
        $this->Html->tag('span.fake-label', __("Exemple"))
        . $uploads->generate(
            [
                'attributes' => ['accept' => 'image/*'],
                'allowDuplicateUploads' => true,
                'target' => $this->Url->build(
                    "/upload/form-file/add-input?replace=true"
                ),
            ]
        )
        . $this->Html->tag('p.help-block', '&nbsp')
    )
    . $this->Html->tag(
        'button',
        __("Tester la validation du champ"),
        [
            'class' => 'btn btn-default btn-validity',
            'type' => 'button',
            'onclick' => 'validateForm(this)'
        ]
    ),
    ['legend' => __("Prévisualisation du champ de formulaire")]
);
echo $this->Form->end();
?>
<script>
    var form = $('#<?=$idPrefix?>-example-form');
    var exampleInput = form.find('input');
    var exampleContainer = exampleInput.closest('.form-group');
    var exampleLabel = exampleContainer.find('.fake-label');
    var exampleHelp = exampleContainer.find('.help-block');
    var btnValidity = form.find('.btn-validity');
    var defaultLabel = exampleLabel.text();
    var tableUploads = $('#create-upload-file-table');

    AsalaeGlobal.select2($('#<?=$idPrefix?>-formats'));

    $('#<?=$idPrefix?>-form').find('input, textarea, select').on(
        'keyup change',
        function () {
            var label = $('#<?=$idPrefix?>-label').val().trim();
            required = $('#<?=$idPrefix?>-required').is(':checked');
            if (!label) {
                label = defaultLabel;
            }
            exampleLabel.text(label);
            exampleHelp.text($('#<?=$idPrefix?>-help').val());
            exampleContainer.toggleClass('required', required);
            exampleInput.prop('required', required);
            btnValidity.removeClass('btn-danger');
            btnValidity.removeClass('btn-success');
            btnValidity.addClass('btn-default');

            var table = TableGenerator.instance[tableUploads.attr('data-table-uid')];
            tableUploads.find('tr[data-id] td.selection input').each(
                function () {
                    var data = table.getDataId($(this).attr('value'));
                    $(this).parent().append(
                        selectableFileupload('', data).prop('required', required)
                    );
                    $(this).remove();
                }
            );
            var accept = '';
            for (let format of $('#<?=$idPrefix?>-formats').val()) {
                switch (format) {
                    case 'image':
                        accept += 'image/*, ';
                        break;
                    case 'video':
                        accept += 'video/*, ';
                        break;
                    case 'audio':
                        accept += 'audio/*, ';
                        break;
                    case 'archive':
                        accept += '.zip, .tar.gz, .rar, .7z, ';
                        break;
                    case 'xml':
                        accept += 'application/xml, .xml, ';
                        break;
                    case 'json':
                        accept += 'application/json, .json, ';
                        break;
                    case 'csv':
                        accept += 'text/csv, .csv, ';
                        break;
                    case 'rng_xsd':
                        accept += '.rng, .xsd, ';
                        break;
                    case 'rng':
                        accept + '.rng, ';
                        break;
                    case 'xsd':
                        accept += '.xsd, ';
                        break;
                    case 'text':
                        accept += '.txt, .odt, .doc, .docx, ';
                        break;
                    case 'calc':
                        accept += '.csv, .ods, .xls, .xlsx, ';
                        break;
                    case 'presentation':
                        accept += '.odp, .ppt, .pptx, ';
                        break;
                    case 'office':
                        accept += '.txt, .odt, .doc, .docx, .csv, .ods, '
                            +'.xls, .xlsx, .csv, .ods, .xls, .xlsx, .odp, .ppt, .pptx, ';
                        break;
                    case 'pdf':
                        accept += 'application/pdf, .pdf, ';
                        break;
                }
            }
            var uploader = AsalaeUploader.instance[exampleContainer.find('.dropbox').attr('data-uploader-uid')];
            uploader.dropbox.find('input').attr('accept', accept).prop('required', required);
            form.closest('.modal').find('form').not(form) // variable formInput
                .find('input[name="field[formats]"]').val($('#<?=$idPrefix?>-formats').val());
        }
    );

    function validateForm(btn)
    {
        var f = form.get(0);
        btn = $(btn);
        btn.removeClass('btn-default');
        if (f.checkValidity()) {
            btn.addClass('btn-success');
            btn.removeClass('btn-danger');
        } else {
            f.reportValidity();
            btn.addClass('btn-danger');
            btn.removeClass('btn-success');
        }
    }

    $('#<?=$idPrefix?>-label').change();
</script>
