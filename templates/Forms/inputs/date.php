<?php

/**
 * @var Versae\View\AppView $this
 */

$tabs = $this->Tabs->create('tabs-add-input-text', ['class' => 'row no-padding']);

$infos = $this->Form->control(
    'label',
    [
        'label' => __("Nom du champ affiché"),
    ]
);
$infos .= $this->Form->control(
    'name',
    [
        'label' => __("Attribut `name` du champ (identifiant twig)"),
        'pattern' => '[a-zA-Z][a-zA-Z0-9_]*',
        'help' => __(
            "Autorisés: lettres, chiffres et underscores `_` (obligatoirement une lettre en premier caractère)"
        ),
    ]
);
$infos .= $this->Form->control(
    'default_value',
    [
        'label' => __("Valeur par défaut"),
        'help' => __(
            "Mettre une date au format JJ/MM/AAAA ou `now` pour la date du jour par défaut"
        ),
        'pattern' => ($method ?? null) === 'datetimepicker'
            ? '[0-9]{2}/[0-9]{2}/[0-9]{4} [0-9]{2}:[0-9]{2}|now'
            : '[0-9]{2}/[0-9]{2}/[0-9]{4}|now',
    ]
);
$infos .= $this->Form->control(
    'min_date',
    [
        'label' => __("Date min"),
        'placeholder' => '-7',
        'help' => __(
            "Nombre de jour après aujourd'hui - ou expression sous forme: +1M +10D. "
            . "Accepte les valeurs négatives. Accepte également les dates en dur au format ISO (AAAA-MM-JJ)."
        ),
        'append' => $this->Fa->i('fa-calendar'),
        'class' => 'datepicker with-select',
    ]
);
$infos .= $this->Form->control(
    'max_date',
    [
        'label' => __("Date max"),
        'placeholder' => '+1W',
        'help' => __(
            "Nombre de jour après aujourd'hui - ou expression sous forme: +1M +10D. "
            . "Accepte les valeurs négatives. Accepte également les dates en dur au format ISO (AAAA-MM-JJ)."
        ),
        'append' => $this->Fa->i('fa-calendar'),
        'class' => 'datepicker with-select',
    ]
);
$infos .= $this->Form->control(
    'placeholder',
    [
        'label' => __("Attribut `placeholder`"),
        'placeholder' => __("Ceci est un placeholder"),
        'help' => __("Message affiché en gris à l'intérieur du champ lorsqu'il est vide"),
    ]
);
$infos .= $this->Form->control(
    'help',
    [
        'label' => __("Message d'aide sous le champ"),
        'help' => __("Ceci est le message d'aide"),
    ]
);
$infos .= $this->Form->control(
    'required',
    [
        'label' => __("Champ obligatoire"),
    ]
);
$infos .= $this->Form->control(
    'readonly',
    [
        'label' => __("Lecture seule"),
    ]
);

$tabs->add(
    'tab-add-input-text-infos',
    $this->Fa->i('fa-file-code-o', __("Informations principales")),
    $infos
);
$tabs->add(
    'tab-add-input-visibility',
    $this->Fa->i('fa-eye-slash', __("Conditions de masquage")),
    require 'conditions.php'
);

echo $tabs;

echo $this->Form->end();

echo $this->Html->tag(
    'div',
    '',
    [
        'class' => 'row',
        'style' => 'height: 5px;background-color: #e9e9e9;border-top: 1px solid #ddd;'
            . 'border-bottom: 1px solid #ddd;margin-bottom: 15px;',
    ]
);
echo $this->Form->create(null, ['id' => $idPrefix . '-example-form', 'idPrefix' => $idPrefix . '-example']);
echo $this->Form->fieldset(
    $this->Form->control(
        'example',
        [
            'label' => __("Exemple"),
            'class' => 'example-input',
            'append' => $this->Fa->i('fa-calendar'),
            'help' => '&nbsp;',
        ]
    )
    . $this->Html->tag(
        'button',
        __("Tester la validation du champ"),
        [
            'class' => 'btn btn-default btn-validity',
            'type' => 'button',
            'onclick' => 'validateForm(this)'
        ]
    ),
    ['legend' => __("Prévisualisation du champ de formulaire")]
);
echo $this->Form->end();
?>
<script>
    var exampleInput = $('#<?=$idPrefix?>-example-example');
    var activeModal = exampleInput.closest('.modal');
    var exampleContainer = exampleInput.closest('.form-group');
    var exampleLabel = exampleContainer.find('label');
    var exampleHelp = exampleContainer.find('.help-block');
    var form = exampleInput.closest('form');
    var btnValidity = form.find('.btn-validity');
    var defaultLabel = exampleLabel.text();
    var method = '<?=$method ?? 'datepicker'?>';

    // date picker sur min et max Date
    var defaultOptions = <?=$this->Date->datePickerOptions?>;
    var minInput = $('[name=min_date]');
    var minInputId = '#' + minInput.attr('id');
    var fakeMinInput = $('<input>')
        .css({width: 0, height: 0, "aria-hidden": true, position: "absolute", "z-index": -1})
        .datepicker(
            $.extend(true, {}, <?=$this->Date->datePickerOptions?>, {altField: minInputId, altFormat: "yy-mm-dd"})
        );
    minInput
        .siblings("span.input-group-addon")
        .last()
        .append(fakeMinInput)
        .on('click', function() { fakeMinInput.focus(); });
    var maxInput = $('[name=max_date]');
    var maxInputId = '#' + maxInput.attr('id');
    var fakeMaxInput = $('<input>')
        .css({width: 0, height: 0, "aria-hidden": true, position: "absolute", "z-index": -1})
        .datepicker(
            $.extend(true, {}, <?=$this->Date->datePickerOptions?>, {altField: maxInputId, altFormat: "yy-mm-dd"})
        );
    maxInput
        .siblings("span.input-group-addon")
        .last()
        .append(fakeMaxInput)
        .on('click', function() { fakeMaxInput.focus(); });

    $('#<?=$idPrefix?>-form').find('input, textarea, select').on(
        'keyup change',
        function () {
            var label = $('#<?=$idPrefix?>-label').val().trim();
            var required = $('#<?=$idPrefix?>-required').is(':checked');
            $('#<?=$idPrefix?>-readonly').prop('disabled', required);
            var readonly = $('#<?=$idPrefix?>-readonly').is(':checked');
            $('#<?=$idPrefix?>-required').prop('disabled', readonly);
            if (!label) {
                label = defaultLabel;
            }
            exampleLabel.text(label);
            exampleInput.val($('#<?=$idPrefix?>-default-value').val());
            exampleInput.attr('placeholder', $('#<?=$idPrefix?>-placeholder').val());
            exampleHelp.text($('#<?=$idPrefix?>-help').val());
            exampleContainer.toggleClass('required', required);
            exampleInput.prop('required', required);
            exampleInput.prop('readonly', readonly);
            btnValidity.removeClass('btn-danger');
            btnValidity.removeClass('btn-success');
            btnValidity.addClass('btn-default');

            var opts = AsalaeGlobal.datepickerOpts;
            opts.minDate = $('#<?=$idPrefix?>-min-date').val();
            if (!opts.minDate) {
                delete opts.minDate;
            } else if (/^-?\d+$/.test(opts.minDate)) {
                opts.minDate = parseInt(opts.minDate, 10);
            } else if (/^\d{4}-\d{2}-\d{2}$/.test(opts.minDate)) {
                let m = opts.minDate.match(/^(\d{4})-(\d{2})-(\d{2})$/);
                opts.minDate = new Date(m[1], m[2] - 1, m[3]);
            }
            opts.maxDate = $('#<?=$idPrefix?>-max-date').val();
            if (!opts.maxDate) {
                delete opts.maxDate;
            } else if (/^-?\d+$/.test(opts.maxDate)) {
                opts.maxDate = parseInt(opts.maxDate, 10);
            } else if (/^\d{4}-\d{2}-\d{2}$/.test(opts.maxDate)) {
                let m = opts.maxDate.match(/^(\d{4})-(\d{2})-(\d{2})$/);
                opts.maxDate = new Date(m[1], m[2] - 1, m[3]);
            }
            if (exampleInput.data('datepicker')) {
                exampleInput[method]('destroy');
            }
            exampleInput[method](opts)
                .siblings("span.input-group-addon")
                .last()
                .click(function() {
                    $(this).siblings("input").focus();
                });
        }
    );

    function validateForm(btn)
    {
        var f = exampleInput.get(0);
        btn = $(btn);
        btn.removeClass('btn-default');
        if (f.checkValidity()) {
            btn.addClass('btn-success');
            btn.removeClass('btn-danger');
        } else {
            f.reportValidity();
            btn.addClass('btn-danger');
            btn.removeClass('btn-success');
        }
    }

    $('#<?=$idPrefix?>-label').change();

    activeModal.one(
        'hide.bs.modal',
        function () {
            if (exampleInput.data('datepicker')) {
                exampleInput[method]('destroy');
            }
            exampleInput.data('datepicker', '');
            fakeMinInput.datepicker('destroy');
            fakeMaxInput.datepicker('destroy');
        }
    );
</script>
