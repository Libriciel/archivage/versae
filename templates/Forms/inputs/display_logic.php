<?php

/**
 * @var Versae\View\AppView $this
 */

?>
<script>
    // désactive les animations lors du popup de la modale
    var animateHiddenInputs = false;
    setTimeout(() => animateHiddenInputs = true, 500);

    function animateHidden(element) {
        if (!animateHiddenInputs || !element.is(':visible')) {
            return;
        }
        AsalaeGlobal.blink(element);
    }

    function arrayHasOneOf(arr1, arr2)
    {
        return arr1.filter(value => arr2.includes(value)).length > 0;
    }

    function getValueByOriginalName(name, fieldset)
    {
        var values = [];
        var target;
        if (fieldset.attr('data-repeatable') === 'true') {
            target = fieldset.find('[data-original-name="' + name + '"]');
        } else {
            target = testFormForm.find('[data-original-name="' + name + '"]');
        }
        target.each(
            function() {
                var value = $(this).val();
                switch ($(this).get(0).tagName) {
                    case 'DIV': // fichier, archive_file : on cherche les lignes cochées
                        $(this).find(':checked').each(
                            function () {
                                values.push($(this).val());
                            }
                        )
                        break;
                    case 'INPUT':
                        let type = $(this).attr('type');
                        if (type === 'checkbox') {
                            if ($(this).prop('checked')) {
                                values.push(value);
                                values.push('checked');
                            }
                        } else if (type === 'radio') {
                            if ($(this).prop('checked')) {
                                values.push(value);
                            }
                        } else {
                            values.push(value);
                        }
                        break;
                    case 'SELECT':
                        if (Array.isArray(value)) {
                            values = values.concat(value);
                        } else {
                            values.push(value);
                        }
                        break;
                    default:
                        values.push(value);
                }
            }
        );
        return values;
    }

    var handleHiddenFormCooldown = {};
    function checkIfTargetIsEmpty(targetValues)
    {
        let targetIsEmpty = true;
        for (let j = 0; j < targetValues.length; j++) {
            if (targetValues[j]) {
                targetIsEmpty = false;
                break;
            }
        }
        return targetIsEmpty;
    }
    function checkIfTargetConditionIsTrue(disableExp, targetValues)
    {
        let conditionIsTrue = false;
        let targetIsEmpty = checkIfTargetIsEmpty(targetValues);
        switch (disableExp.cond_display_field) {
            case 'standard_not_empty':
            case 'select_not_empty':
            case 'select_multiple_not_empty':
            case 'checkbox_not_empty':
                conditionIsTrue = targetIsEmpty === false;
                break;
            case 'standard_empty':
            case 'select_empty':
            case 'select_multiple_empty':
            case 'checkbox_empty':
                conditionIsTrue = targetIsEmpty === true;
                break;
            case 'standard_value':
                conditionIsTrue = arrayHasOneOf(targetValues, [disableExp.cond_display_value]);
                break;
            case 'select_value':
            case 'select_multiple_value':
                conditionIsTrue = arrayHasOneOf(targetValues, disableExp.cond_display_value_select);
                break;
            case 'file':
            case null:
                if (disableExp.cond_display_value_file === 'is_selected') {
                    conditionIsTrue = targetValues.length > 0;
                } else {
                    conditionIsTrue = targetValues.length === 0;
                }
        }
        return conditionIsTrue;
    }

    function handleHiddenFormElements(target)
    {
        target = $(target);
        var targetOriginalName = target.attr('data-original-name');
        if (handleHiddenFormCooldown[targetOriginalName]) {
            return;
        }
        handleHiddenFormCooldown[targetOriginalName] = setTimeout(
            () => handleHiddenFormCooldown[targetOriginalName] = 0,
            10
        );
        var targetFieldset = target.closest('fieldset');
        var inRepeatable = targetFieldset.attr('data-repeatable') === 'true';
        var targetValues = getValueByOriginalName(targetOriginalName, targetFieldset);
        for (let i = 0; i < targetsOfDisableExps[targetOriginalName].length; i++) {
            let disableExp = targetsOfDisableExps[targetOriginalName][i];
            let conditionIsTrue = checkIfTargetConditionIsTrue(disableExp, targetValues);
            let display = disableExp.cond_display_way === 'show_if'
                ? conditionIsTrue
                : !conditionIsTrue;

            let visibilityTarget;
            if (inRepeatable) {
                visibilityTarget = targetFieldset.find(
                    '[data-original-name="%s"]'.format(disableExp.input)
                ).closest('.form-group');

            } else {
                visibilityTarget = disableExp.input
                    ? $('[data-original-name="%s"]'.format(disableExp.input)).closest('.form-group')
                    : $(disableExp.fieldset);
            }
            if (disableExp.fieldset && visibilityTarget.attr('data-repeatable')) {
                visibilityTarget = visibilityTarget.closest('form')
                    .find('fieldset.repeatable-fieldset[data-name="'+visibilityTarget.attr('data-name')+'"]');
            }

            visibilityTarget.toggle(display);
            if (disableExp.input) {
                visibilityTarget.find('input, select, textarea').disable(display === false);
            } else {
                visibilityTarget.disable(display === false)
            }
            let visibilityTargetIsVisible = visibilityTarget.is(':visible');
            if (display && !visibilityTargetIsVisible) {
                visibilityTarget.stop(true, true)
                    .css({display: "block", opacity: 0})
                    .animate({'opacity': '1'}, 50);
                setTimeout(() => visibilityTarget.css('opacity', '').show(), 50);
            } else if (!display && visibilityTargetIsVisible) {
                visibilityTarget.stop(true, true)
                    .css({display: "block", opacity: 1})
                    .animate(
                        {'opacity': '0'},
                        50,
                        function () {
                            if (target.hasClass('select2-hidden-accessible')) {
                                target.select2('close');
                            }
                        }
                    );
                setTimeout(() => visibilityTarget.css('opacity', '').hide(), 50);
            } else {
                visibilityTarget.toggle(display);
            }
        }
    }

    testFormForm.on(
        'beforeSubmit.modalForm',
        function (event, prevEvent) {
            var value = [];
            testFormForm.find('fieldset:not([data-repeatable])')
                .find('input:not([type="hidden"]), select, textarea').each(
                function() {
                    var formGroup = $(this).closest('div.form-group');
                    if (formGroup.length === 0) {
                        return;
                    }
                    var name = $(this).attr('name');
                    if (!name) {
                        name = $(this).attr('data-name');
                    }
                    if (!name) {
                        return;
                    }
                    // transforme un name: 'input[a][b]' en 'a[b]'
                    var m = name.match(/^inputs\[([^\]]+)](.*)$/)
                    if (m) {
                        name = m[1] + m[2];
                    }
                    if (name && value.indexOf(name) === -1 && !formGroup.is(':visible')) {
                        value.push(name);
                    }
                }
            );

            $(this).find('input[type="hidden"].hidden_fields')
                .val(JSON.stringify(value));
        }
    );

    fieldsetTmpls = {};
    testFormForm.find('fieldset[data-repeatable]').each(
        function() {
            $(this).find('.select2-hidden-accessible').each(
                function() {
                    $(this).select2('destroy');
                }
            );
            $(this).hide().disable();
            var newFieldset = $('<fieldset>');
            var legend = $(this).find('legend');
            newFieldset.addClass('repeatable-fieldset');
            newFieldset.attr('data-name', $(this).attr('data-name'));
            if (legend.length) {
                newFieldset.append($(this).find('legend').first().clone());
            }
            var buttonName = $(this).attr('data-button-name');
            var button = $('<button type="button" class="btn btn-success add-fieldset">');
            button.append('<?=$this->Fa->charte('Ajouter')?>');
            button.append(" ");
            button.append($('<span>').text(buttonName ? buttonName : __("Ajouter une section")));
            button.on('click', () => appendFieldset.call($(this)));
            newFieldset.append(button);
            newFieldset.insertAfter($(this));
            fieldsetTmpls[$(this).attr('id')] = {
                childs: {},
                button: button
            };
            if ($(this).attr('data-required') === 'true') {
                appendFieldset.call($(this));
            }
        }
    );

    function appendFieldset()
    {
        var newFieldset = $(this).clone(true);
        newFieldset.find('legend').remove();
        var childs = fieldsetTmpls[$(this).attr('id')].childs;
        var index = Object.keys(childs).length;
        var fieldsetName = $(this).attr('data-name');
        newFieldset.attr('id', '%s-%d'.format(newFieldset.attr('id'), index));
        newFieldset.attr('data-index', index);
        newFieldset.addClass('repeatable-fieldset-item');
        newFieldset.find('[id]').each(
            function() {
                var initialId = $(this).attr('id');
                var newId = '%s-%d'.format(initialId, index);
                $(this).attr('id', newId);
                $(this).attr('data-initial-id', initialId);
                newFieldset.find('[for="%s"]'.format(initialId)).attr('for', newId);
                var dataId = $(this).attr('data-id');
                if (dataId) {
                    newId = '%s-%d'.format(dataId, index);
                    $(this).attr('id', '%s-0'.format(newId));
                    $(this).attr('data-id', newId);
                }
            }
        );
        newFieldset.find('[name]').each(
            function() {
                var initialName = $(this).attr('name');
                var m = initialName.match(/inputs(.*)/);
                var newName = 'inputs[%s][%d]%s'.format(fieldsetName, index, m[1]);
                $(this).attr('name', newName);
                var dataName = $(this).attr('data-name');
                if (dataName) {
                    m = dataName.match(/inputs(.*)/);
                    newName = 'inputs[%s][%d]%s'.format(fieldsetName, index, m[1]);
                    $(this).attr('data-name', newName);
                }
            }
        );
        newFieldset.find('label, .fake-label').not('.select .checkbox label').each(
            function() {
                $(this).append(' ');
                $(this).append($('<span class="index">').text(index + 1));
            }
        );
        newFieldset.find('script').remove();

        // ajoute le nom corrigé sur le div (reconstruction formulaire avec fichiers)
        newFieldset.find('.form-group.fake-input[data-name]').each(
            function () {
                var initialName = $(this).attr('data-name');
                var m = initialName.match(/inputs(.*)/);
                var newName = 'inputs[%s][%d]%s'.format(fieldsetName, index, m[1]);
                $(this).attr('data-input-name', newName);
            }
        );

        // fieldset requis
        var required = $(this).attr('data-required');
        if (required === 'false' || index > 0) {
            var btnClose = $('<button type="button" class="close">')
                .css({"z-index": 10}) // rend clickable le bouton
                .attr('title', __("Supprimer cette section de formulaire"))
                .append('<span>×</span>')
                .attr('aria-label', __("Supprimer cette section de formulaire"))
                .on('click', () => deleteFieldset.call($(this), newFieldset));
            newFieldset.prepend(btnClose);
        }

        newFieldset.show().enable();
        newFieldset.insertBefore(fieldsetTmpls[$(this).attr('id')].button);
        childs[newFieldset.attr('id')] = {
            element: newFieldset,
            index: index
        };

        // controle de cardinalité
        var cardinality = $(this).attr('data-cardinality');
        if (cardinality !== 'n' && index + 1 >= cardinality) {
            fieldsetTmpls[$(this).attr('id')].button.disable();
        }

        // rétablissement des champs d'uploads
        newFieldset.find('.dropbox[id]').each(
            function() {
                var inputDiv = $(this).closest('.form-group');
                inputDiv.find('input[type="file"]').remove();
                var inputTable = inputDiv.find('table');
                var tableObject = TableGenerator.instance[inputTable.attr('data-table-uid')];
                var dataName = inputDiv.attr('data-name');
                var m = dataName.match(/inputs(.*)/);
                var newName = 'inputs[%s][%d]%s'.format(fieldsetName, index, m[1]);
                var newTable = AsalaeUploader.createTable(
                    {
                        selectionName: newName,
                        selectionType: inputDiv.attr('data-singlefile') === 'true'
                            ? 'radio'
                            : 'checkbox',
                    }
                );
                newTable.addClass('hide');
                newTable.insertAfter(inputTable);
                inputTable.remove();
                AsalaeUploader.inputScripts(
                    inputDiv,
                    {
                        target: inputDiv.attr('data-target'),
                        simultaneousUploads: <?= Cake\Core\Configure::read('Flow.simultaneousUploads', 3) ?>,
                        chunkSize: <?= Cake\Core\Configure::read('Flow.chunkSize', 1048576) ?>,
                        attributes: {
                            accept: inputDiv.attr('data-accept'),
                            singleFile: inputDiv.attr('data-singleFile')
                        }
                    }
                );
            }
        );

        newFieldset.find('select[data-mount-select2]').each(
            function() {
                AsalaeGlobal.select2(this);
            }
        );

        newFieldset.find('[data-type="date"], [data-type="datetime"]').each(
            function() {
                initializeTimepicker($(this).attr('id'), $(this).attr('data-initial-id'));
            }
        );

        newFieldset.find('input, select, textarea').first().focus();
    }

    function deleteFieldset(section) {
        var fieldset = $(this);
        if (confirm(__("Voulez-vous supprimer cette section ?"))) {
            section.slideUp(
                200,
                'swing',
                function () {
                    var tmpl = fieldsetTmpls[fieldset.attr('id')];
                    var childs = tmpl.childs;
                    var index = $(this).attr('data-index');
                    delete childs[$(this).attr('id')];
                    $(this).remove();

                    for (let key in childs) {
                        if (childs[key].index > index) {
                            removeOneFromIndex(fieldset, childs[key].element);
                        }
                    }

                    // controle de cardinalité
                    var cardinality = fieldset.attr('data-cardinality');
                    if (cardinality === 'n'
                        || Object.keys(childs).length < cardinality
                    ) {
                        tmpl.button.enable();
                    }
                }
            );
        }
    }

    function removeOneFromIndex(fieldset, child)
    {
        var tmpl = fieldsetTmpls[fieldset.attr('id')];
        var childs = tmpl.childs;
        delete childs[child.attr('id')];
        var initialIndex = parseInt(child.attr('data-index'));
        var index = initialIndex - 1;
        var fieldsetName = fieldset.attr('data-name');
        child.attr('data-index', index);
        child.attr('id', '%s-%d'.format(fieldset.attr('id'), index));
        child.find('[id]').each(
            function() {
                var initialId = $(this).attr('id');
                var idWithoutIndex = initialId.substring(0, initialId.length - ('-' + initialIndex).length);
                var newId = '%s-%d'.format(idWithoutIndex, index);
                $(this).attr('id', newId);
                child.find('[for="%s"]'.format(initialId)).attr('for', newId);
                var dataId = $(this).attr('data-id');
                if (dataId) {
                    $(this).attr('data-id', '%s-%d'.format(dataId, index));
                }
            }
        );
        child.find('[name]').each(
            function() {
                var initialName = $(this).attr('name');
                var namePrefix = 'inputs[%s][%d]'.format(fieldsetName, initialIndex);
                var nameWithoutIndex = initialName.substring(namePrefix.length);
                var newName = 'inputs[%s][%d]%s'.format(fieldsetName, index, nameWithoutIndex);
                $(this).attr('name', newName);
                var dataName = $(this).attr('data-name');
                if (dataName) {
                    m = dataName.match(/inputs(.*)/);
                    newName = 'inputs[%s][%d]'.format(fieldsetName, index, m[1]);
                    $(this).attr('data-name', newName);
                }
            }
        );
        child.find('label .index, .fake-label .index').remove();
        child.find('label, .fake-label').each(
            function() {
                $(this).append($('<span class="index">').text(index + 1));
            }
        );
        childs[child.attr('id')] = {
            element: child,
            index: index
        };
    }

    /**
     * Gestion de l'affichage conditionnel
     */
    var targetsOfDisableExps = {};
    for (let i = 0; i < disableExps.length; i++) {
        var targetOriginalName = '[data-original-name="%s"]'.format(disableExps[i].cond_display_input);
        if (!targetsOfDisableExps[disableExps[i].cond_display_input]) {
            targetsOfDisableExps[disableExps[i].cond_display_input] = [];
        }
        targetsOfDisableExps[disableExps[i].cond_display_input].push(disableExps[i]);

        (
            (targetOriginalName) => setTimeout(
                () => {
                    testFormForm.find(targetOriginalName).each(
                        function() {
                            $(this).attr('onchange', 'handleHiddenFormElements(this)');
                            $(this).attr('onkeyup', 'handleHiddenFormElements(this)');
                            if (handleHiddenFormCooldown[disableExps[i].cond_display_input]) {
                                clearTimeout(handleHiddenFormCooldown[disableExps[i].cond_display_input]);
                                handleHiddenFormCooldown[disableExps[i].cond_display_input] = 0;
                            }
                            handleHiddenFormElements(this);
                        }
                    )
                }
            )
        )(targetOriginalName);
    }
</script>
