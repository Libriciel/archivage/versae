<?php

/**
 * @var Versae\View\AppView $this
 */

$tabs = $this->Tabs->create('tabs-add-input-select', ['class' => 'row no-padding']);

$infos = $this->Form->control(
    'multiple',
    [
        'type' => 'hidden',
        'value' => '1',
    ]
);
$infos .= $this->Form->control(
    'label',
    [
        'label' => __("Nom du champ affiché"),
    ]
);
$infos .= $this->Form->control(
    'name',
    [
        'label' => __("Attribut `name` du champ (identifiant twig)"),
        'pattern' => '[a-zA-Z][a-zA-Z0-9_]*',
        'help' => __(
            "Autorisés: lettres, chiffres et underscores `_` (obligatoirement une lettre en premier caractère)"
        ),
    ]
);
$infos .= $this->Form->control(
    'help',
    [
        'label' => __("Message d'aide sous le champ"),
        'help' => __("Ceci est le message d'aide"),
    ]
);
$infos .= $this->Form->control(
    'required',
    [
        'label' => __("Champ obligatoire"),
    ]
);

$tabs->add(
    'tab-add-input-select-infos',
    $this->Fa->i('fa-file-code-o', __("Informations principales")),
    $infos
);

$options = $this->Form->control('options', ['type' => 'hidden']);
$options .= $this->Form->control(
    'keyword_list_id_hidden',
    [
        'type' => 'hidden',
        'name' => 'keyword_list_id',
        'val' => $form->getData('keyword_list_id'),
    ]
);
$nameAsCode = $this->Form->control(
    'use_name_as_code',
    [
        'label' => ' ' . __("valeur = nom"),
        'templates' => [
            'checkboxContainer' => '{{content}}',
        ],
    ]
);
$options .= $this->Form->control(
    'keyword_list_id',
    [
        'label' => __("Utiliser une liste de mot clés existante"),
        'empty' => __("-- Sélectionner une liste de mots clés --"),
        'options' => $keywordLists,
        'prepend' => $nameAsCode,
    ]
);
$options .= '<hr>';
$options .= $this->Html->tag('div.row.border-bottom');
$options .= $this->Html->tag(
    'label.col-5.font-weight-bold',
    __("Valeur"),
    ['for' => 'value-add-option']
);
$options .= $this->Html->tag(
    'label.col-5.font-weight-bold',
    __("Texte affiché"),
    ['for' => 'text-add-option']
);
$btnSort = $this->Html->tag(
    'span.action',
    $this->Html->tag(
        'button.btn.btn-link.sort',
        $this->Fa->i('fa-sort-amount-down')
        . $this->Html->tag('span.sr-only', __("Trier")),
        ['type' => 'button', 'title' => __("Trier"), 'onclick' => 'showSortPopup(this)']
    )
);
$options .= $this->Html->tag('div.col-2', $btnSort, ['style' => 'position: relative;']);
$options .= $this->Html->tag('/div');
$options .= $this->Html->tag(
    'div.display_options.div-striped',
    '',
    ['style' => 'max-height: 400px; overflow-x: hidden;']
);
$options .= $this->Html->tag(
    'div.display_options_tmpl.row.pt-1.pb-1.hide',
    $this->Html->tag('div.col-lg-5', $this->Html->tag('input.value', '', ['value' => 'exemple']))
    . $this->Html->tag('div.col-lg-5', $this->Html->tag('input.text', '', ['value' => 'exemple']))
    . $this->Html->tag(
        'div.col-lg-2.d-flex.align-items-center',
        $this->Html->tag(
            'span.action',
            $this->Html->tag(
                'button.btn.btn-link.delete',
                $this->Fa->charte('Supprimer')
                . $this->Html->tag('span.sr-only', __("Supprimer")),
                ['type' => 'button', 'title' => __("Supprimer")]
            )
        )
    )
);
$options .= $this->Html->tag(
    'div.row.mt-4.add-option-inputs',
    $this->Html->tag(
        'div.col-lg-5',
        $this->Html->tag('input', '', ['id' => 'value-add-option', 'class' => 'value'])
    )
    . $this->Html->tag(
        'div.col-lg-5',
        $this->Html->tag('input', '', ['id' => 'text-add-option', 'class' => 'text'])
    )
    . $this->Html->tag(
        'div.col-lg-2.d-flex.align-items-center',
        $this->Html->tag(
            'span.action',
            $this->Html->tag(
                'button.btn.btn-link.add',
                $this->Fa->i('fa-plus text-success')
                . $this->Html->tag('span.sr-only', __("Ajouter une option")),
                ['type' => 'button']
            )
        )
    )
    . $this->Html->tag(
        'input.hidden-required',
        '',
        [
            'aria-hidden' => 'true',
            'tabindex' => '-1',
            'style' => 'width: 0!important; opacity: 0!important; position: absolute; z-index: -1',
        ]
    )
);

$tabs->add(
    'tab-add-input-select-options',
    $this->Fa->i('fa-code', __("Options")),
    $options
);
$tabs->add(
    'tab-add-input-visibility',
    $this->Fa->i('fa-eye-slash', __("Conditions de masquage")),
    require 'conditions.php'
);

echo $tabs;

echo $this->Form->end();

echo $this->Html->tag(
    'div',
    '',
    [
        'class' => 'row',
        'style' => 'height: 5px;background-color: #e9e9e9;border-top: 1px solid #ddd;'
            . 'border-bottom: 1px solid #ddd;margin-bottom: 15px;',
    ]
);
echo $this->Form->create(null, ['id' => $idPrefix . '-example-form', 'idPrefix' => $idPrefix . '-example']);
echo $this->Form->fieldset(
    $this->Form->control(
        'example',
        [
            'label' => __("Exemple"),
            'options' => [
                ['value' => 'example', 'text' => 'example'],
            ],
            'class' => 'example-input',
            'help' => '&nbsp;',
            'multiple' => 'checkbox',
        ]
    ),
    ['legend' => __("Prévisualisation du champ de formulaire")]
);
echo $this->Form->end();
$exempleId = $idPrefix . '-example-example-example';
?>
<script>
    var exampleInput = $('#<?=$idPrefix?>-example-example-example');
    var activeModal = exampleInput.closest('.modal');
    var exampleContainer = exampleInput.closest('.form-group');
    var exampleLabel = exampleContainer.find('label').first();
    var exampleHelp = exampleContainer.find('.help-block');
    var exampleForm = exampleInput.closest('form');
    var btnValidity = exampleForm.find('.btn-validity');
    var defaultId = exampleLabel.attr('for');
    var defaultLabel = exampleLabel.text();
    var previousKeywordListId = $('#<?=$idPrefix?>-keyword-list-id-hidden');
    var useNameAsCodeInput = $('#<?=$idPrefix?>-use-name-as-code');
    var displayOptionsDiv = activeModal.find('.display_options');
    var optionsInput = $('#<?=$idPrefix?>-options');
    var selectOptions = optionsInput.val() ? JSON.parse(optionsInput.val()) : [];
    var optionTmplDiv = activeModal.find('.display_options_tmpl');
    var defaultValue = $('#<?=$idPrefix?>-default-value');
    var tmplOption = $('<div>').append(exampleInput.closest('div.checkbox')).html();
    function changeEventHandle() {
        var label = $('#<?=$idPrefix?>-label').val().trim();
        var baseId = $('#<?=$idPrefix?>-name').val();
        if (!label && defaultLabel) {
            label = defaultLabel;
        }
        if (!baseId && defaultId) {
            baseId = defaultId;
        }
        exampleLabel.text(label);
        exampleHelp.text($('#<?=$idPrefix?>-help').val());

        exampleContainer.find('input.example-input').closest('div.checkbox').remove();

        for (let i = 0; i < selectOptions.length; i++) {
            let input = $(tmplOption).insertBefore(exampleHelp);
            let inputId = baseId + '-' + i;
            input.find('label')
                .attr('for', inputId)
                .contents()
                .filter(function () {
                    return this.nodeType === 3; //Node.TEXT_NODE
                })
                .first()
                .get(0)
                .nodeValue = $('<div>').append(selectOptions[i].text).text();
            input.find('input')
                .attr('id', inputId)
                .val(selectOptions[i].value);
        }

        exampleContainer.find('input.example-input');

        btnValidity.removeClass('btn-danger');
        btnValidity.removeClass('btn-success');
        btnValidity.addClass('btn-default');
    }
</script>
<?php
require 'options-script.php';
