<?php

/**
 * @var Versae\View\AppView $this
 */

$tabs = $this->Tabs->create('tabs-add-input-text', ['class' => 'row no-padding']);

$infos = $this->Form->control(
    'color',
    [
        'empty' => __("Pas de couleur"),
        'options' => [
            'alert-info' => __("bleu"),
            'alert-success' => __("vert"),
            'alert-danger' => __("rouge"),
            'alert-warning' => __("jaune"),
        ],
        'label' => __("Couleur de fond")
    ]
);
$infos .= $this->Form->control(
    'p',
    [
        'label' => __("Paragraphe"),
        'class' => 'mce mce-small',
    ]
);

$tabs->add(
    'tab-add-input-text-infos',
    $this->Fa->i('fa-file-code-o', __("Informations principales")),
    $infos
);
$tabs->add(
    'tab-add-input-visibility',
    $this->Fa->i('fa-eye-slash', __("Conditions de masquage")),
    require 'conditions.php'
);

echo $tabs;

echo $this->Form->end();

echo $this->Html->tag(
    'div',
    '',
    [
        'class' => 'row',
        'style' => 'height: 5px;background-color: #e9e9e9;border-top: 1px solid #ddd;'
            . 'border-bottom: 1px solid #ddd;margin-bottom: 15px;',
    ]
);
echo $this->Form->create(null, ['id' => $idPrefix . '-example-form', 'idPrefix' => $idPrefix . '-example']);
echo $this->Form->fieldset(
    $this->Html->tag(
        'p',
        '',
        ['id' => $idPrefix . '-example-example', 'class' => 'example-input']
    ),
    ['legend' => __("Prévisualisation du champ de formulaire")]
);
echo $this->Form->end();
?>
<script>
    var exampleInput = $('#<?=$idPrefix?>-example-example');
    var exampleContainer = exampleInput.closest('.form-group');
    var exampleLabel = exampleContainer.find('label');
    var exampleHelp = exampleContainer.find('.help-block');
    var form = exampleInput.closest('form');
    var btnValidity = form.find('.btn-validity');
    var defaultLabel = exampleLabel.text();

    $('#<?=$idPrefix?>-form').find('input, textarea, select').on(
        'keyup change',
        function () {
            var html = $('<div>').html($('#<?=$idPrefix?>-p').val());
            html.find('script').remove();
            exampleInput.html(html.html());
            exampleInput.removeAttr('class');
            exampleInput.addClass('paragraph');
            var color = $('#<?=$idPrefix?>-color').val();
            if (color) {
                exampleInput.addClass('alert '+color);
            }
        }
    );

    function validateForm(btn)
    {
        var f = exampleInput.get(0);
        btn = $(btn);
        btn.removeClass('btn-default');
        if (f.checkValidity()) {
            btn.addClass('btn-success');
            btn.removeClass('btn-danger');
        } else {
            f.reportValidity();
            btn.addClass('btn-danger');
            btn.removeClass('btn-success');
        }
    }

    $('#<?=$idPrefix?>-pattern-select').on(
        'change',
        function() {
            var pattern = $('#<?=$idPrefix?>-pattern');
            pattern.val(pattern.val() + $(this).val());
            $(this).val('');
            pattern.trigger('change');
        }
    );

    $('#<?=$idPrefix?>-label').change();

    new AsalaeMce({selector: '#<?=$idPrefix?>-p'}, $('#<?=$idPrefix?>-color'));
</script>
