<?php

/**
 * @var Versae\View\AppView $this
 * @var Versae\Form\FormMakerForm $form
 */

use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;
use Cake\Utility\Text;
use Versae\Model\Table\FormInputsTable;

$idPrefix = $idPrefix ?? 'test';

// changement de contexte de entity vers form
$this->Form->create($form);

$disableExps = [];
$indexFieldset = 0;

$paragraphIndexes = 0;
$datepickers = [];

$multipleTextValues = [];
/** @var EntityInterface $fieldset */
foreach ($formEntity->get('form_fieldsets') ?: [] as $fieldset) {
    $inputs = '';
    /** @var EntityInterface $input */
    foreach ($fieldset->get('form_inputs') as $input) {
        // un input nouvellement ajouté contient des données mal interprétées
        unset($input['form'], $input['fieldset'], $input['form_fieldset'], $input['typetrad']);
        // cas possible où l'input n'a pas été encore enregistré en base si on prévisualise directement
        $inputId = $input['id'] ?? 'preview_form';
        $input = $input->toArray();
        $fieldname = $input['name'] ?? 'paragraph';
        $input['data-original-name'] = $fieldname;
        if (preg_match('/(.*)(\[.*])?/', $fieldname, $m)) {
            $input['name'] = 'inputs[' . $m[1] . ']' . ($m[2] ?? '');
        }
        $input['id'] = isset($input['name']) ? $idPrefix . '-' . Text::slug($input['name']) : null;
        $input['data-type'] = $input['type'];
        switch ($input['type']) {
            case FormInputsTable::TYPE_CHECKBOX:
                $input['templates'] = [
                    'checkboxContainer'
                    => '<div class="form-group checkbox {{required}}">{{content}}<br>{{append}}</div>',
                ];
                $input['checked'] = Hash::get($formEntity, "inputs.$fieldname") !== null
                    ? Hash::get($formEntity, "inputs.$fieldname")
                    : ($input['checked'] ?? null);
                break;
            case FormInputsTable::TYPE_RADIO:
                $input['checked'] = Hash::get($formEntity, "inputs.$fieldname") !== null
                    ? Hash::get($formEntity, "inputs.$fieldname")
                    : ($input['checked'] ?? null);
                break;
            case FormInputsTable::TYPE_MULTI_CHECKBOX:
                $input['type'] = 'select';
                $input['multiple'] = 'checkbox';
                break;
            case FormInputsTable::TYPE_DATE:
            case FormInputsTable::TYPE_DATETIME:
                $input['append'] = $this->Fa->i('fa-calendar');
                $datepickers[] = [
                    'type' => $input['type'],
                    'id' => $input['id'],
                    'minDate' => $input['min_date'] ?? null,
                    'maxDate' => $input['max_date'] ?? null,
                ];
                $input['type'] = 'text';
                break;
            case FormInputsTable::TYPE_EMAIL:
                $input['append'] = $this->Fa->i('fa-envelope-o');
                break;
            case FormInputsTable::TYPE_TEXT:
                if ($input['multiple'] ?? false) {
                    $input['append'] = $this->Fa->i('fa-plus')
                        . $this->Html->tag(
                            'span.sr-only',
                            __("Ajouter une valeur ''{0}''", h($input['label']))
                        );
                    $input['templates']['inputGroupAddons'] = $this->Html->tag(
                        'button',
                        '{{content}}',
                        [
                            'type' => 'button',
                            'onclick' => 'duplicateInput.call(this)',
                            'class' => 'input-group-addon',
                        ]
                    );
                    $input['data-id'] = $input['id'];
                    $input['data-name'] = $input['name'];
                    $input['id'] .= '-0';
                    $input['name'] .= '[0]';
                }
                break;
        }
        if (isset($input['options'])) {
            $input['options'] = json_decode($input['options'], true);
        }
        if (!empty($input['select2'])) {
            $input['data-mount-select2'] = true;
            $input['data-select2-placeholder'] = $input['empty'] ?? '';
        }
        unset($input['select2']);
        if (isset($input['default_value'])) {
            $input['default'] = $input['default_value'];
            unset($input['default_value']);
        }
        if ($input['type'] === FormInputsTable::TYPE_PARAGRAPH) {
            $class = ($input['color'] ?? false) ? 'paragraph alert ' . $input['color'] : 'paragraph';
            $paragraphIndexes++;
            $input['id'] = $idPrefix . '-' . 'paragraph_' . $paragraphIndexes;
            $inputs .= $this->Html->tag(
                'div.user-paragraph.form-group',
                $this->Html->tag(
                    'div',
                    $input['p'],
                    ($class ? ['class' => $class] : [])
                    + ['id' => $input['id']]
                )
            );
        } elseif ($input['type'] === FormInputsTable::TYPE_FILE) {
            $multiple = $input['multiple'] ?? false;
            $name = $input['name'] . ($multiple ? '[]' : '');
            $type = $multiple ? 'checkbox' : 'radio';
            $jsFn = 'selectableFileupload' . str_replace('-', '_', $input['id']);
            /** @noinspection UnreachableCodeJS - bug phpstorm */
            $script = <<<EOT
    <script>
        var $jsFn = function (value, context) {
            return $('<input>').val(context.id)
                .attr('name', '$name')
                .attr('aria-label', context.name)
                .attr('type', '$type')
                .prop('checked', true);
        }
    </script>
EOT;
            $inputRequired = $input['required'] ?? false;
            if (!empty($fileuploads[$fieldname])) {
                $inputRequired = false;
            }
            $uploads = $this->Upload
                ->create(
                    $input['id'],
                    [
                        'class' => 'table table-striped table-hover hide',
                        'inputName' => $name,
                        'inputRequired' => $inputRequired,
                    ]
                )
                ->fields(
                    [
                        'selection' => [
                            'label' => __("Sélection"),
                            'callback' => $jsFn,
                            'class' => 'selection',
                        ],
                        'name' => [
                            'label' => __("Nom de fichier"),
                            'callback' => 'TableHelper.filenameToPopup',
                        ],
                        'message' => [
                            'label' => __("Message"),
                            'style' => 'min-width: 200px; max-width: 400px',
                            'class' => 'message',
                        ],
                    ]
                )
                ->data($fileuploads[$fieldname] ?? [])
                ->params(
                    [
                        'identifier' => 'id',
                        'classEval' => 'data[{index}].valid !== false ? "" : "danger"',
                    ]
                )
                ->actions(
                    [
                        function ($table) {
                            $url = $this->Url->build('/Upload/delete');
                            return [
                                'onclick' => "GenericUploader.fileDelete($table->tableObject, '$url', {0})",
                                'type' => 'button',
                                'class' => 'btn-link delete',
                                'displayEval' => 'AsalaeGlobal.is_numeric(data[{index}].id)',
                                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                                'title' => __("Supprimer {0}", '{1}'),
                                'aria-label' => __("Supprimer {0}", '{1}'),
                                'params' => ['id', 'name'],
                            ];
                        },
                    ]
                );
            $accept = [];
            foreach ($input['formats'] ?? [] as $format) {
                switch ($format) {
                    case 'image':
                        $accept[] = 'image/*';
                        break;
                    case 'video':
                        $accept[] = 'video/*';
                        break;
                    case 'audio':
                        $accept[] = 'audio/*';
                        break;
                    case 'archive':
                        $accept[] = '.zip, .tar.gz, .rar, .7z';
                        break;
                    case 'xml':
                        $accept[] = 'application/xml, .xml';
                        break;
                    case 'json':
                        $accept[] = 'application/json, .json';
                        break;
                    case 'csv':
                        $accept[] = 'text/csv, .csv';
                        break;
                    case 'rng_xsd':
                        $accept[] = '.rng, .xsd';
                        break;
                    case 'rng':
                        $accept[] = '.rng';
                        break;
                    case 'xsd':
                        $accept[] = '.xsd';
                        break;
                    case 'text':
                        $accept[] = '.txt, .odt, .doc, .docx';
                        break;
                    case 'calc':
                        $accept[] = '.csv, .ods, .xls, .xlsx';
                        break;
                    case 'presentation':
                        $accept[] = '.odp, .ppt, .pptx';
                        break;
                    case 'office':
                        $accept[] = '.txt, .odt, .doc, .docx, .csv, .ods, '
                            . '.xls, .xlsx, .csv, .ods, .xls, .xlsx, .odp, .ppt, .pptx';
                        break;
                    case 'pdf':
                        $accept[] = 'application/pdf, .pdf';
                        break;
                }
            }
            $formats = implode(',', $input['formats'] ?? []);
            $inputs .= $this->Html->tag(
                'div.form-group.fake-input' . (($input['required'] ?? false) ? '.required' : ''),
                $this->Html->tag('span.fake-label', $input['label'])
                . $script
                . $uploads->generate(
                    [
                        'attributes' => ['accept' => implode(', ', $accept), 'data-name' => $input['name']],
                        'allowDuplicateUploads' => true,
                        'singleFile' => (bool)($input['multiple'] ?? false) === false,
                        'target' => $this->Url->build(
                            "/upload/form-file/$inputId?replace=true&formats=$formats"
                        ),
                        'inputName' => $name,
                        'inputRequired' => $input['required'] ?? false,
                    ]
                )
                . ($input['help'] ?? false ? $this->Html->tag('p.help-block', h($input['help'])) : ''),
                [
                    'data-name' => $input['name'],
                    'data-accept' => implode(', ', $accept),
                    'data-singleFile' => ((bool)($input['multiple'] ?? false) === false) ? 'true' : 'false',
                    'data-target' => "/upload/form-file/$inputId?replace=true&formats=archive",
                    'data-original-name' => $input['data-original-name'],
                    'data-type' => $input['data-type'],
                ]
            );
        } elseif ($input['type'] === FormInputsTable::TYPE_ARCHIVE_FILE) {
            $name = $input['name'];
            $jsFn = 'selectableFileupload' . str_replace('-', '_', $input['id']);
            /** @noinspection UnreachableCodeJS - bug phpstorm */
            $script = <<<EOT
    <script>
        var $jsFn = function (value, context) {
            return $('<input>').val(context.id)
                .attr('name', '$name')
                .attr('aria-label', context.name)
                .attr('type', 'radio')
                .prop('checked', true);
        }
    </script>
EOT;
            $uploads = $this->Upload
                ->create(
                    $input['id'],
                    [
                        'class' => 'table table-striped table-hover hide',
                        'iClass' => 'fas fa-file-archive fa-3x text-warning',
                        'pText' => __("Glissez-déposez votre fichier ici"),
                        'inputName' => $name,
                        'inputRequired' => $input['required'] ?? false,
                    ]
                )
                ->fields(
                    [
                        'selection' => [
                            'label' => __("Sélection"),
                            'callback' => $jsFn,
                            'class' => 'selection',
                        ],
                        'info' => [
                            'label' => __("Nom de fichier"),
                            'target' => '',
                            'callback' => 'TableHelper.filenameToPopup',
                        ],
                        'message' => [
                            'label' => __("Message"),
                            'style' => 'min-width: 200px; max-width: 400px',
                            'class' => 'message',
                        ],
                    ]
                )
                ->data($fileuploads[$fieldname] ?? [])
                ->params(
                    [
                        'identifier' => 'id',
                        'classEval' => 'data[{index}].valid !== false ? "" : "danger"',
                    ]
                )
                ->actions(
                    [
                        function ($table) {
                            $url = $this->Url->build('/Upload/delete');
                            return [
                                'onclick' => "GenericUploader.fileDelete($table->tableObject, '$url', {0})",
                                'type' => 'button',
                                'class' => 'btn-link delete',
                                'displayEval' => 'AsalaeGlobal.is_numeric(data[{index}].id)',
                                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                                'title' => __("Supprimer {0}", '{1}'),
                                'aria-label' => __("Supprimer {0}", '{1}'),
                                'params' => ['id', 'name'],
                            ];
                        },
                    ]
                );
            $inputs .= $this->Html->tag(
                'div.form-group.fake-input' . (($input['required'] ?? false) ? '.required' : ''),
                $this->Html->tag('span.fake-label', $input['label'])
                . $script
                . $uploads->generate(
                    [
                        'attributes' => ['accept' => '.zip, .tar.gz', 'data-name' => $input['name']],
                        'allowDuplicateUploads' => true,
                        'singleFile' => true,
                        'target' => $this->Url->build(
                            "/upload/form-file/$inputId?replace=true&formats=archive"
                        ),
                        'inputName' => $name,
                        'inputRequired' => $input['required'] ?? false,
                    ]
                )
                . ($input['help'] ?? false ? $this->Html->tag('p.help-block', h($input['help'])) : ''),
                [
                    'data-name' => $input['name'],
                    'data-accept' => '.zip, .tar.gz',
                    'data-singleFile' => 'true',
                    'data-target' => "/upload/form-file/$inputId?replace=true&formats=archive",
                    'data-original-name' => $input['data-original-name'],
                    'data-type' => $input['data-type'],
                ]
            );
        } else {
            $controlInput = $input;
            $u = 'form_fieldset_id,ord,disable_expression,app_meta,formats,form_id,date_formattrad' .
                ',cond_display_input,cond_display_value,cond_display_way,cond_display_value_select'
                . ',cond_display_value_file,cond_display_field';
            foreach (explode(',', $u) as $param) {
                unset($controlInput[$param]);
            }
            if (!empty($controlInput['help'])) {
                $controlInput['help'] = h($controlInput['help']);
            }
            unset($controlInput['form_fieldset']['inputs']);
            if ($controlInput['type'] === 'checkbox' && $form->getData($fieldname) !== null) {
                $controlInput['checked'] = (bool)$form->getData($fieldname);
            }
            if (
                $input['type'] === FormInputsTable::TYPE_TEXT
                && ($input['multiple'] ?? false)
                && isset($inputData) // valeur remonté lors de l'édition du champ
                && count($v = (array)($inputData[$input['data-original-name']] ?? []))
            ) {
                $multipleTextValues[$input['data-original-name']] = $v;
            }

            $inputs .= $this->Form->control($fieldname, $controlInput);
        }
        if ($input['cond_display_input'] ?? null) {
            $expr = [
                'cond_display_input' => $input['cond_display_input'],
                'cond_display_way' => $input['cond_display_way'] ?? null,
                'cond_display_field' => $input['cond_display_field'] ?? null,
                'input' => $input['data-original-name'],
            ];
            if (!empty($input['cond_display_value'])) {
                $expr['cond_display_value'] = $input['cond_display_value'];
            } elseif (isset($input['cond_display_value_select'])) {
                $expr['cond_display_value_select'] = $input['cond_display_value_select'];
            } elseif (isset($input['cond_display_value_file'])) {
                $expr['cond_display_value_file'] = $input['cond_display_value_file'];
            }
            $disableExps[] = $expr;
        }
    }
    $fieldsetId = $idPrefix . '-fielset' . $indexFieldset;

    if ($fieldset['cond_display_input'] ?? null) {
        $expr = [
            'cond_display_input' => $fieldset['cond_display_input'],
            'cond_display_way' => $fieldset['cond_display_way'],
            'cond_display_field' => $fieldset['cond_display_field'] ?? null,
            'fieldset' => '#' . $fieldsetId,
        ];
        if (!empty($fieldset['cond_display_value']) && $fieldset['cond_display_value'] !== 'null') {
            $expr['cond_display_value'] = $fieldset['cond_display_value'];
        } elseif (!empty($fieldset['cond_display_value_select'])) {
            $expr['cond_display_value_select'] = $fieldset['cond_display_value_select'];
        } elseif (!empty($fieldset['cond_display_value_file']) && $fieldset['cond_display_value_file'] !== 'null') {
            $expr['cond_display_value_file'] = $fieldset['cond_display_value_file'];
        }
        $disableExps[] = $expr;
    }
    $repeatable = [];
    if ($fieldset['repeatable']) {
        $repeatable = [
            'data-repeatable' => 'true',
            'data-required' => $fieldset['required'] !== null
                ? ($fieldset['required'] ? 'true' : 'false')
                : 'true',
            'data-cardinality' => $fieldset['cardinality'] ?: '1',
            'data-name' => 'section-' . $fieldset['ord'],
            'data-button-name' => $fieldset['button_name'],
        ];
    }
    echo $this->Form->fieldset(
        $inputs,
        [
            'fieldset' => [
                'id' => $fieldsetId,
            ] + $repeatable,
            'legend' => $fieldset['legend'] ?? '',
        ]
    );
    $indexFieldset++;
}
echo $this->Form->control(
    'hidden_fields',
    [
        'type' => 'hidden',
        'class' => 'hidden_fields',
    ]
);
?>
<script>
    var testFormForm = $('#<?=$formId?>');
    testFormForm.css('opacity', 0);
    // laisse le temps pour le calcul de l'affichage des champs cachés
    setTimeout(() => testFormForm.animate({'opacity': 1}, 150), 15);

    var testModal = testFormForm.closest('.modal');
    var disableExps = <?=json_encode($disableExps)?>;
    var timepickers = <?=json_encode($datepickers)?>;
    var footer = testModal.find('.modal-footer');

    /**
     * Gestion des fichiers multiples obligatoire
     */
    var fileMandaryInputs = $('.form-group.fake-input.required')
    function updateMandatoryMultipleFile() {
        fileMandaryInputs.each(function() {
            let req = $(this).find('input:checkbox:checked').length === 0;
            $(this).next(':radio').prop('required', req);
        })
    }
    fileMandaryInputs.find('.table.table-striped.table-hover').change(updateMandatoryMultipleFile);

    /**
     * select2 doit avoir lieu avant l'affichage conditionnel
     */
    testFormForm.find('select[data-mount-select2]').each(function() {
        $(this).select2(
            {
                dropdownParent: testModal,
                placeholder: $(this).attr('data-select2-placeholder'),
                allowClear: true,
            }
        ).on('change', function() {
            var value = $(this).val();
            if (Array.isArray(value) && value.length === 1 && value[0] === '') {
                $(this).val([]);
            }
        });
    });
</script>

<?php
// gère également la logique des sections répétables
require 'inputs/display_logic.php';
?>

<script>
    /**
     * Gestion des dates
     */
    for (let i = 0; i < timepickers.length; i++) {
        initializeTimepicker(timepickers[i].id)
    }

    /**
     * @param id
     * @param pickerId si null, identique à id (utilisé juste pour les sections répétables)
     */
    function initializeTimepicker(id, pickerId = null)
    {
        let input = $('#' + id);
        if (pickerId === null) {
            pickerId = id;
        }
        let timepicker = timepickers.find(e => e.id === pickerId)

        let method = timepicker.type === 'date' ? 'datepicker' : 'datetimepicker';
        let opts = AsalaeGlobal.datepickerOpts;
        if (input.data('datepicker')) {
            input[method]('destroy');
        }
        input.data('datepicker', '');
        opts.minDate = timepicker.minDate;
        if (!opts.minDate) {
            delete opts.minDate;
        } else if (/^-?\d+$/.test(opts.minDate)) {
            opts.minDate = parseInt(opts.minDate, 10);
        } else if (/^\d{4}-\d{2}-\d{2}$/.test(opts.minDate)) {
            let m = opts.minDate.match(/^(\d{4})-(\d{2})-(\d{2})$/);
            opts.minDate = new Date(m[1], m[2] - 1, m[3]);
        }
        opts.maxDate = timepicker.maxDate;
        if (!opts.maxDate) {
            delete opts.maxDate;
        } else if (/^-?\d+$/.test(opts.maxDate)) {
            opts.maxDate = parseInt(opts.maxDate, 10);
        } else if (/^\d{4}-\d{2}-\d{2}$/.test(opts.maxDate)) {
            let m = opts.maxDate.match(/^(\d{4})-(\d{2})-(\d{2})$/);
            opts.maxDate = new Date(m[1], m[2] - 1, m[3]);
        }

        input[method](opts);

        let minDate;
        let minDateRegex = /[-+]?\d+[YMWDymwd]/.test(opts.minDate) ? opts.minDate.match(/[-+]?\d+[YMWDymwd]/g) : null;
        if (minDateRegex) {
            minDate = new Date();
            minDate.setHours(0, 0, 0, 0);
            minDateRegex.forEach(
                (v, i) => {
                    let unit = v.substring(v.length - 1);
                    let sign = v.substring(0, 1) === '-' ? -1 : 1;
                    let val = v.substring(['+', '-'].includes(v.substring(0, 1)) ? 1 : 0, v.length - 1);
                    switch (unit) {
                        case 'Y':
                        case 'y':
                            minDate.setFullYear(minDate.getFullYear() + (sign * val));
                            return;
                        case 'm':
                        case 'M':
                            minDate.setMonth(minDate.getMonth() + (sign * val));
                            return;
                        case 'w':
                        case 'W':
                            minDate.setDate(minDate.getDate() + (sign * val * 7));
                            return;
                        case 'd':
                        case 'D':
                            minDate.setDate(minDate.getDate() + (sign * val));
                            return;
                    }
                }
            );
        } else {
            minDate = opts.minDate;
        }

        let maxDate;
        let maxDateRegex = /[-+]?\d+[YMWDymwd]/.test(opts.maxDate) ? opts.maxDate.match(/[-+]?\d+[YMWDymwd]/g) : null;
        if (maxDateRegex) {
            maxDate = new Date()
            maxDate.setHours(0, 0, 0, 0);
            maxDateRegex.forEach(
                (v, i) => {
                    let unit = v.substring(v.length - 1);
                    let sign = v.substring(0, 1) === '-' ? -1 : 1;
                    let val = v.substring(['+', '-'].includes(v.substring(0, 1)) ? 1 : 0, v.length - 1);
                    switch (unit) {
                        case 'Y':
                        case 'y':
                            maxDate.setFullYear(maxDate.getFullYear() + (sign * val));
                            return;
                        case 'm':
                        case 'M':
                            maxDate.setMonth(maxDate.getMonth() + (sign * val));
                            return;
                        case 'w':
                        case 'W':
                            maxDate.setDate(maxDate.getDate() + (sign * val * 7));
                            return;
                        case 'd':
                        case 'D':
                            maxDate.setDate(maxDate.getDate() + (sign * val));
                            return;
                    }
                }
            );
        } else {
            maxDate = opts.maxDate;
        }

        input.on('change keyup',  function() {
            let m = $(this).val().match(/^(\d{2})\/(\d{2})\/(\d{4})/);
            if (!m) {
                return;
            }
            let d = new Date(m[3], m[2] - 1, m[1]);
            if (d < minDate || d > maxDate) {
                this.setCustomValidity(__("La date n'est pas dans l'intervalle demandé"));
            }
        }).change();
    }

    /**
     * Validation custom des checkbox multiples obligatoires
     */
    $(function() {
        let requiredCheckboxes = testFormForm.find(':checkbox[required]');
        requiredCheckboxes.change(function() {
            var boxes = $(this).parentsUntil('.control-label', '.form-group-wrapper').find(':checkbox');
            if (boxes.filter(':checked').length) {
                boxes.prop('required', false);
                for (let e of boxes) {
                    e.setCustomValidity('');
                }
            } else {
                boxes.prop('required', true);
                for (let e of boxes) {
                    e.setCustomValidity(__("Veuillez sélectionner au moins une case à cocher"));
                }
            }
        });
        requiredCheckboxes.change();
    });

    function duplicateInput()
    {
        var addWrapper = $(this).closest('.form-group-wrapper');
        var label = addWrapper.parent().find('label');
        var firstInput = addWrapper.find('[data-id]').first();
        var baseId = firstInput.attr('data-id');
        var baseName = firstInput.attr('data-name');
        label.attr('id', '%s-label'.format(baseId))

        // on commence à redéfinir les ids et names
        var index = 0;
        var emptyExists = false;
        addWrapper.parent().find('.form-group-wrapper').each(
            function () {
                var input = $(this).find('[id]').first();
                input.attr('id', '%s-%d'.format(baseId, index));
                if (index > 0) {
                    input.attr('aria-labelledby', baseId+'-0');
                }
                input.attr('name', '%s[]'.format(baseName));
                index++;
                if (input.val() === '') {
                    emptyExists = input;
                }
            }
        );
        if (emptyExists) {
            emptyExists.focus();
            return;
        }

        // On copie le champ et on lui attribut un nouvel id et name
        var newWrapper = addWrapper.clone(true);
        newWrapper.find('.fa-plus')
            .removeClass('fa-plus')
            .addClass('fa-minus');
        var newInput = newWrapper.find('[id]').first();
        newInput.attr('id', '%s-%d'.format(baseId, index));
        newInput.attr('aria-labelledby', baseId+'-label');
        newInput.attr('name', '%s[%d]'.format(baseName, index));
        newInput.on('keyup change', function(e) {
            let button = $(this).parent().find('button');
            let icon = button.find('i');
            if ($(this).val() && isLastLine(this)) {
                icon.removeClass('fa-minus');
                icon.addClass('fa-plus');
                button.attr('onclick', 'duplicateInput.call(this)');
            } else {
                icon.removeClass('fa-plus');
                icon.addClass('fa-minus');
                button.attr('onclick', 'removeInput.call(this)');
            }
        });
        // on remplace le bouton d'ajout par un bouton de retrait
        addWrapper.parent().append(newWrapper);
        newWrapper.find('input').val('').focus();
        var button = addWrapper.find('.input-group-addon');
        var icon = addWrapper.find('.fa-plus');
        icon.removeClass('fa-plus');
        icon.addClass('fa-minus');
        button.attr('onclick', 'removeInput.call(this)');
        newWrapper.find('.input-group-addon').attr('onclick', 'removeInput.call(this)');
    }

    function removeInput()
    {
        let group = $(this).closest('.form-group');

        $(this).closest('.form-group-wrapper').remove();

        // s'il ne reste plus qu'une ligne, on y garde un bouton +
        if (group.find('.form-group-wrapper').length === 1) {
            let input = group.find('.form-group-wrapper input');
            input.off();

            let button = input.parent().find('button');
            button.attr('onclick', 'duplicateInput.call(this)');

            let icon = button.find('i');
            icon.removeClass('fa-minus');
            icon.addClass('fa-plus');
        }

        group.find('.form-group-wrapper').find('input').change();
    }

    function isLastLine(line)
    {
        let group = $(line).closest('.form-group').find('.form-group-wrapper');
        let last = group.last().find('input');

        return last.attr('id') === $(line).attr('id');
    }

    $(document).off('.formGenerator').on('fileAdded.AsalaeUploader.formGenerator', function(e, data) {
        var fileModal = data.instance?.asalae?.dropbox?.closest('.modal');
        // si le fichier est ajouté est sur cette modale
        if (fileModal.get(0) !== testModal.get(0)) {
            return;
        }
        // on parcours les uploaders de cette même modale pour lister les fichiers déjà uploadés
        for (let i = 0; i < AsalaeUploader.instance.length; i++) {
            if (AsalaeUploader.instance[i].dropbox?.closest('.modal').get(0) !== testModal.get(0)) {
                continue;
            }
            for (let j = 0; j < AsalaeUploader.instance[i].uploader.files.length; j++) {
                if (data.data?.name === AsalaeUploader.instance[i].uploader.files[j].name) {
                    alert(
                        __(
                            "Attention, il semble que le fichier {0} soit déjà présent dans le formulaire",
                            data.data?.name
                        )
                    );
                }
            }
        }
    });
    testModal.one('hide.bs.modal', function () {
        $(document).off('.formGenerator');
    });

    $(function() {
        // remplissage manuel des inputs texte multiple
        let multipleTextValues = <?= json_encode($multipleTextValues)?>;
        for (let name in multipleTextValues) {
            let input = testFormForm.find('[data-original-name="' + name + '"]');
            let i = 0;
            for (let val of multipleTextValues[name]) {
                input = input.closest('.form-group').find('input').last();
                input.val(val);
                i++;
                if (i < multipleTextValues[name].length) {
                    duplicateInput.call(input);
                } else {
                    input.change();
                }
            }
        }
    });

    function nowDate(type) {
        let local = new Date();
        let date = local.getDate().toString().padStart(2, '0') + '/'
            + (local.getMonth() + 1).toString().padStart(2, '0') + '/' + local.getFullYear();
        if (type === 'datetime') {
            date += ' ' + local.getHours().toString().padStart(2, '0') + ':'
                + local.getMinutes().toString().padStart(2, '0');
        }
        return date;
    }

    $('.hasDatepicker').each(
        (i, e) => $(e).val() === 'now' ? $(e).val(nowDate($(e).data('type'))) : null
    );
</script>
