<?php

/**
 * @var Versae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */

/**
 * Informations principales
 */

use Cake\Datasource\EntityInterface;
use Versae\Model\Table\FormInputsTable;

?>
<script>
    function inlineTwig(value) {
        return value && value.replace(/\n/g, '<span class="text-info">\\n</span><wbr/>')
    }

    function twigForm(value, context) {
        value = value && value.replace(/\n/g, '<span class="text-info">\\n</span><wbr/>');
        if (context.required) {
            return $('<div>').append(
                $('<input required>')
                    .css(
                        {
                            height: 0,
                            width: 0,
                            padding: 0,
                            border: 0,
                            opacity: 0
                        }
                    )
                    .attr('aria-hidden', 'true')
                    .val(value)
            ).append(value);
        }
        return value;
    }
</script>
<?php
$tables = [
    __("Informations principales") => [
        [
            __("Nom") => 'name',
            __("Description") => 'description',
            __("Identifiant") => 'identifier',
            __("Version du SEDA") => 'seda_version',
            __("Système d'Archivage Electronique (SAE) destinataire")
                => '{archiving_system.name}, {archiving_system.url}',
            __("Date de création") => 'created',
            __("État") => function (EntityInterface $form) {
                return $form->get('statetrad')
                    . ($form->get('last_state_update') ? __(" depuis le {0}", $form->get('last_state_update')) : '');
            },
            __("Publiable") => function (EntityInterface $form) {
                return $form->get('publishable')
                    ? __("Oui")
                    : __("Non : {0}", $form->get('not_publishable_reason'));
            },
            __("État") => function (EntityInterface $form) {
                return $form->get('statetrad')
                    . ($form->get('last_state_update') ? __(" depuis le {0}", $form->get('last_state_update')) : '');
            },
            __("Testé") => 'tested',
            __("Guide utilisateur") => function (EntityInterface $form) {
                $dom = new DOMDocument();
                set_error_handler(
                    function ($errno, $errstr, $errfile, $errline) {
                        throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
                    }
                );
                try {
                    $dom->loadHtml($form->get('user_guide'));
                } catch (Throwable) {
                }
                restore_error_handler();
                $xpath = new DOMXPath($dom);
                while ($node = $xpath->query('//script')->item(0)) {
                    $node->parentNode->removeChild($node);
                }
                return $dom->saveHTML();
            },
            __("Numéro de version") => 'version_number',
        ],
    ],
];

$versions = $this->Table->create('versions', ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'version_number' => [
                'label' => __("Version"),
            ],
            'created' => [
                'label' => __("Date"),
                'type' => 'datetime',
            ],
            'name' => [
                'label' => __("Nom"),
            ],
            'statetrad' => [
                'label' => __("État"),
            ],
            'version_note' => [
                'label' => __("Note"),
            ],
        ]
    )
    ->data($entity->get('all_versions'))
    ->params(['identifier' => 'id']);

$tabs = $this->Tabs->create('view-transfer', ['class' => 'row no-padding']);
$tabs->add(
    'tab-view-form-details',
    $this->Fa->i('fa-file-code-o', __("Détails")),
    $this->Html->tag('h3', h($entity->get('name')))
    . $this->ViewTable->multiple($entity, $tables)
    . $this->Html->tag('h4', __("Versions"))
    . $versions
);

/**
 * Zones de saisies
 */
$fieldsets = [];
$inputTab = [
    'name' => [
        'label' => __("Nom"),
    ],
    'type' => [
        'label' => __("Type"),
    ],
    'label' => [
        'label' => __("Titre"),
    ],
    'required' => [
        'label' => __("Requis"),
        'type' => 'boolean',
    ],
    'readonly' => [
        'label' => __("Lecture seule"),
        'type' => 'boolean',
    ],
    'default_value' => [
        'label' => __("Valeur initiale"),
    ],
];
foreach ($entity->get('form_fieldsets') as $fieldset) {
    // suppression des valeurs arbitraires dans name et label
    $cleanedInputs = array_map(
        function (EntityInterface $input) {
            if ($input->get('type') === FormInputsTable::TYPE_PARAGRAPH) {
                $input->unset(['name', 'label']);
            }
            return $input;
        },
        $fieldset->get('form_inputs')
    );

    $fieldsets[] = $this->Html->tag('h3', __("Groupe de zones de saisie #{0}", $fieldset->get('ord')))
        . $this->ViewTable->generate(
            $fieldset,
            [
                __("Légende") => 'legend',
                __("Conditions de masquage") => 'readable_disable_expression',
            ]
        )
        . $this->Html->tag('h4', __("Zones de saisie"))
        . $this->Table->create(
            'input_' . $fieldset->get('ord'),
            ['class' => 'table table-striped table-hover smart-td-size']
        )
            ->fields($inputTab)
            ->data($cleanedInputs)
            ->params(['identifier' => 'id']);
}

$tabs->add(
    'tab-view-form-fieldsets',
    $this->Fa->i('fa-object-group', __("Zones de saisie")),
    implode($this->Html->tag('hr', ''), $fieldsets)
);

/**
 * Extracteurs
 */
$tableExtractors = $this->Table
    ->create('view-form-extractor-table', ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'name' => [
                'label' => __("Nom"),
            ],
            'type' => [
                'label' => __("Type"),
            ],
            'form_input.name' => [
                'label' => __("Nom du champ de formulaire"),
            ],
            'url' => [
                'label' => __("Url du webservice"),
            ],
            'data_format' => [
                'label' => __("Format"),
            ],
            'data_path' => [
                'label' => __("Chemin"),
            ],
        ]
    )
    ->data($entity->get('form_extractors'))
    ->params(['identifier' => 'id']);

$tabs->add(
    'tab-view-form-extractors',
    $this->Fa->i('fa-external-link', __("Données à extraire")),
    $tableExtractors
);

/**
 * Variables
 */
$tableVariables = $this->Table
    ->create('view-form-variable-table', ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'name' => [
                'label' => __("Nom"),
            ],
            'form_variable.type' => [
                'label' => __("Type"),
            ],
            'form_variable.twig' => [
                'label' => __("Twig"),
                'callback' => 'inlineTwig',
            ],
        ]
    )
    ->data($entity->get('form_calculators'))
    ->params(['identifier' => 'id']);

$tabs->add(
    'tab-view-form-variables',
    $this->Fa->i('fa-cube', __("Données à calculer")),
    $tableVariables
);

/**
 * Entête du transfert
 */
$tableHeaders = $this->Table
    ->create('view-form-transfer-header-table', ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'label' => ['label' => __("Nom")],
            'form_variable.twig' => [
                'label' => __("Twig"),
                'callback' => 'twigForm',
                'class' => 'twig',
            ],
        ]
    )
    ->data($dataTransferHeaders)
    ->params(['identifier' => 'name']);

$tabs->add(
    'tab-view-form-headers',
    $this->Fa->i('fa-newspaper-o', __("Transfert")),
    $tableHeaders
);

$tabs->add(
    'tab-view-form-units',
    $this->Fa->i('fa-archive', __("Arborescence")),
    require 'view-form-units.php'
);

echo $tabs;
