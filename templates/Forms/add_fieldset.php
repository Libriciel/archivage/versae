<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->Form->create($entity, ['idPrefix' => $idPrefix = 'form-add-fieldset']);
$infos = $this->Form->control(
    'is_submit',
    ['type' => 'hidden', 'value' => '1']
);
$infos .= $this->Form->control(
    'legend',
    [
        'label' => __("Titre de la section"),
        'placeholder' => __("Sans titre"),
    ]
);
$infos .= $this->Form->control(
    'repeatable',
    [
        'label' => __("Section répétable"),
    ]
);
$repeatableFieldset = $this->Html->tag('fieldset.repeatable');
$repeatableFieldset .= $this->Html->tag(
    'legend',
    __("Section répétable")
    . $this->Html->tag('span.cardinality', ' 0..n')
);
$repeatableFieldset .= $this->Form->control(
    'required',
    [
        'label' => __("Au moins une fois"),
    ]
);
$repeatableFieldset .= $this->Form->control(
    'cardinality',
    [
        'label' => __("Jusqu'à X fois"),
        'pattern' => '^(n|[1-9][0-9]*)$',
        'help' => __("Tapez un chiffre ou la lettre 'n' pour ne pas mettre de limite"),
        'required' => true,
        'default' => 'n',
    ]
);
$repeatableFieldset .= $this->Form->control(
    'button_name',
    [
        'label' => __("Nom du bouton ajouter"),
        'default' => __("Ajouter une section"),
        'maxlength' => 80,
    ]
);
$repeatableFieldset .= $this->Html->tag('/fieldset');
$infos .= $repeatableFieldset;

$legend = __("Conditions de masquage de la section");
$label = __("Masquer la section si");
$empty = __("Toujours afficher la section");

$tabs = $this->Tabs->create('tabs-add-fieldset', ['class' => 'row no-padding']);
$tabs->add(
    'tab-add-input-text-infos',
    $this->Fa->i('fa-file-code-o', __("Informations principales")),
    $infos
);
$tabs->add(
    'tab-add-input-visibility',
    $this->Fa->i('fa-eye-slash', __("Conditions de masquage")),
    require 'inputs/conditions.php'
);
echo $tabs;

echo $this->Form->end();
?>
<script>
    var repeatableFieldset = $('#tabs-add-fieldset').find('fieldset.repeatable');
    $('#form-add-fieldset-repeatable').change(
        function() {
            var checked = $(this).prop('checked');
            repeatableFieldset.toggle(checked);
            repeatableFieldset.prop('disabled', !checked);
            setCardinalitySpan();
            $('#form-add-fieldset-legend').prop('required', checked)
                .closest('.form-group').toggleClass('required', checked);
        }
    ).change();

    function setCardinalitySpan()
    {
        var required = $('#form-add-fieldset-required').prop('checked') ? 1 : 0;
        var cardinality = $('#form-add-fieldset-cardinality').filter(':valid').val();
        repeatableFieldset.find('.cardinality').text(' %d..%s'.format(required, String(cardinality ?? '')));
    }
    repeatableFieldset.find('input').on('change keyup', setCardinalitySpan).change();
</script>
