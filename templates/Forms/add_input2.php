<?php

/**
 * @var Versae\View\AppView $this
 */

use Versae\Model\Table\FormInputsTable;
use Versae\Utility\Twig;

echo $this->MultiStepForm->template('MultiStepForm/add_form_input')->step(2) . '<hr>';

echo $this->Form->create($form, ['id' => 'add-input2-form', 'idPrefix' => $idPrefix = 'add-input2']);
echo $this->Form->control(
    'is_submit',
    ['type' => 'hidden', 'value' => '1']
);
echo $this->Form->control(
    'type',
    ['type' => 'hidden', 'value' => $type]
);
echo $this->Form->control(
    'request',
    ['type' => 'hidden', 'value' => json_encode($this->getRequest()->getData())]
);
require 'inputs/' . basename($type) . '.php';
echo $this->Form->end();

$jsonInputsData = json_encode(
    array_values(
        array_filter($inputsData, fn($v) => $v['type'] !== FormInputsTable::TYPE_PARAGRAPH)
    )
);
?>
<script>
    var inputsData = <?=$jsonInputsData?>;
    var formInput2 = $('#add-input2-form');
    // évite les conflits dom
    formInput2.closest('.modal').one(
        'hidden.bs.modal',
        function () {
            $(this).find('form [id]').removeAttr('id');
        }
    );
    formInput2.find('input[name=label]').keyup(
        function () {
            formInput2.find('input[name=name]').val(
                AsalaeGlobal.removeDiacritics($(this).val())
                    .replace(/[ -]+/g, '_')
                    .replace(/\W/g, '')
            ).change();
        }
    );
    formInput2.find('input[name=name]').on('input change', function(e) {
        for (const i in inputsData) {
            if ($(this).val() === inputsData[i].name) {
                this.setCustomValidity(__("Un champ avec ce nom existe déjà"));
                return;
            }
        }
        if (<?=json_encode(Twig::TOKEN_PROG_LIST)?>.includes($(this).val())) {
            this.setCustomValidity(__("Ce nom est réservé"));
            return;
        }
        this.setCustomValidity('');
    })
        .change();

    var modalTitle = $('#edit-form-input-2Label');
    modalTitle.find('span.type').remove();
    modalTitle.append(
        $('<span class="type">').text(" (<?=$form->getData('typetrad')?>)")
    );

    // déclencher la validation car le champ name est forcément en erreur (doublon)
    <?= isset($copy) ? '$(() => formInput2.get(0).reportValidity());' : ''?>

    // empêche l'envoi du formulaire d'exemple
    $('#<?=$idPrefix . '-example-form'?>').on('submit', () => false);
</script>
