<?php

/**
 * @var Versae\View\AppView $this
 */

?>
<script>
    function parseArrayData(value, context) {
        if (!value) {
            return;
        }
        var table = $('<table class="parsed-json-data"></table>');
        var tbody = $('<tbody></tbody>');
        table.append(tbody);
        var tr;
        if (typeof value === 'string') {
            tbody.append($('<tr></tr>').append($('<td colspan="2">').append(value)));
        } else {
            for (var key in value) {
                tbody.append(
                    $('<tr></tr>')
                        .append($('<th></th>').append(key+':'))
                        .append($('<td></td>').append(value[key]))
                );
            }
        }
        return table;
    }
    function inlineTwig(value) {
        return value && value.replace(/\n/g, '<span class="text-info">\\n</span><wbr/>')
    }

    function twigForm(value, context) {
        value = value && value.replace(/\n/g, '<span class="text-info">\\n</span><wbr/>');
        if (context.required) {
            return $('<div>').append(
                $('<input required>')
                    .css(
                        {
                            height: 0,
                            width: 0,
                            padding: 0,
                            border: 0,
                            opacity: 0
                        }
                    )
                    .attr('aria-hidden', 'true')
                    .val(value)
            ).append(value);
        }
        return value;
    }

    function requiredCallback(value, context) {
        if (context.required) {
            return $('<div class="form-group required">').append(value);
        }
        return value;
    }
</script>
<?php
use Cake\Datasource\EntityInterface;

echo $this->ModalForm
    ->create('add-form-fieldset', ['size' => 'large'])
    ->modal(__("Ajout d'une section de formulaire"))
    ->javascriptCallback('afterAddSection')
    ->output(
        'function',
        'addFormFieldset',
        '/forms/add-fieldset'
    )
    ->generate(
        [
            'ajaxParams' => [
                'method' => '"POST"',
                'data' => 'getCurrentFormData(true)',
            ],
        ]
    );
echo $this->ModalForm
    ->create('edit-form-fieldset', ['size' => 'large'])
    ->modal(__("Modification d'une section de formulaire"))
    ->javascriptCallback('afterEditSection')
    ->output(
        'function',
        'editFormFieldset',
        '/forms/edit-fieldset'
    )
    ->generate(
        [
            'ajaxParams' => [
                'method' => '"POST"',
                'data' => 'currentData',
            ],
        ]
    );
echo $this->ModalForm
    ->create('preview-form-modal', ['size' => 'large'])
    ->modal(__("Prévisualisation du formulaire"))
    ->output(
        'function',
        'previewForm',
        '/forms/preview/' . $entity->id
    )
    ->generate(
        [
            'ajaxParams' => [
                'method' => '"POST"',
                'data' => 'getCurrentFormData(true)',
            ],
        ]
    );
echo $this->ModalView
    ->create('preview-form-tree-modal', ['size' => 'large'])
    ->modal(__("Prévisualisation de l'arborescence"))
    ->output(
        'function',
        'previewFormTree',
        '/forms/previewFormTree'
    )
    ->generate();

echo $this->ModalForm
    ->create('edit-form-input', ['size' => 'large'])
    ->modal(__("Ajout d'un champ de formulaire"))
    ->step('/forms/add-input', 'addFormInput1')
    ->step('/forms/add-input', 'addFormInput2')
    ->javascriptCallback('afterAddInput')
    ->output('function')
    ->generate(
        [
            'ajaxParams' => [
                'method' => '"POST"',
                'data' => 'currentData',
            ],
        ]
    );

echo $this->ModalForm
    ->create('edit-form-input', ['size' => 'large'])
    ->modal(__("Modification d'un champ de formulaire"))
    ->javascriptCallback('afterEditInput')
    ->output(
        'function',
        'editFormInput',
        '/forms/edit-input'
    )
    ->generate(
        [
            'ajaxParams' => [
                'method' => '"POST"',
                'data' => 'currentData',
            ],
        ]
    );

$jsTableExtractors = $this->Table->getJsTableObject('edit-form-extractor-table');
echo $this->ModalForm->create('edit-extract', ['size' => 'large'])
    ->modal(__("Modification d'un extracteur"))
    ->javascriptCallback("refreshEditFormModal")
    ->output('function', 'actionEditExtractor', '/form-extractors/edit')
    ->generate();

echo $this->ModalView->create('extract-xpath', ['size' => 'large'])
    ->modal(__("Extraction d'un XPath"))
    ->output('function', 'actionExtractXpath', '/form-extractors/extract-xpaths')
    ->generate();

echo $this->ModalView->create('extract-json-path', ['size' => 'large'])
    ->modal(__("Extraction d'un JSON Path"))
    ->output('function', 'actionExtractJsonPath', '/form-extractors/extract-json-path')
    ->generate();

echo $this->ModalView->create('extract-csv-path', ['size' => 'large'])
    ->modal(__("Extraction d'un CSV Path"))
    ->output('function', 'actionExtractCsvPath', '/form-extractors/extract-csv-path')
    ->generate();

$jsTableVariables = $this->Table->getJsTableObject('edit-form-variable-table');
echo $this->ModalForm->create('edit-var', ['size' => 'large'])
    ->modal(__("Modification d'une variable"))
    ->javascriptCallback("refreshEditFormModal")
    ->output('function', 'actionEditVariable', '/form-calculators/edit')
    ->generate();

$jsTableTransferHeader = $this->Table->getJsTableObject('edit-form-transfer-header-table');
$jsTableTransferManagement = $this->Table->getJsTableObject('edit-form-transfer-management-table');
$jsTableTransferCodeListVersions = $this->Table->getJsTableObject('edit-form-transfer-version-table');

echo $this->ModalForm->create('edit-form-header-var', ['size' => 'large'])
    ->modal(__("Modification d'une variable d'entête"))
    ->javascriptCallback('afterEditHeader')
    ->output('function', 'actionAddEditVariable', '/form-calculators/add-edit-header-var')
    ->generate();

$infosTab = $this->Form->create(
    $entity,
    [
        'idPrefix' => 'edit-form-form',
        'id' => 'edit-form-form',
    ]
);
$infosTab .= $this->Form->control(
    'tab',
    [
        'type' => 'hidden',
        'value' => 'main',
    ]
);
$infosTab .= $this->Form->control(
    'name',
    [
        'label' => __("Nom du formulaire"),
    ]
);
$infosTab .= $this->Form->control(
    'description',
    [
        'label' => __("Description"),
    ]
);
$infosTab .= $this->Form->control(
    'seda_version',
    [
        'label' => __("Version du SEDA"),
        'readonly' => true,
    ]
);

$infosTab .= $this->Form->control(
    'transferring_agencies._ids',
    [
        'label' => __("Services versants autorisés à utiliser le formulaire pour les versements"),
        'options' => $transferringAgencies,
        'data-placeholder' => __("-- Sélectionner un/plusieurs service(s) versant(s) --"),
        'multiple' => true,
        'required' => true,
    ]
);

$infosTab .= $this->Form->control(
    'archiving_system_id',
    [
        'label' => __("Système d'Archivage Electronique (SAE) destinataire"),
        'empty' => __("-- Choisir un SAE --"),
        'options' => $archivingSystems,
    ]
);

$infosTab .= $this->Form->control(
    'user_guide',
    [
        'label' => __("Guide utilisateur"),
        'class' => 'mce mce-small',
    ]
);
$infosTab .= $this->Form->end();

/**
 * Zones de saisies
 */
$fieldsets = $this->Form->create(
    $entity,
    [
        'idPrefix' => 'edit-form-fieldsets',
        'id' => 'edit-form-fieldsets',
    ]
);
$fieldsets .= $this->Form->control(
    'tab',
    [
        'type' => 'hidden',
        'value' => 'fieldsets',
    ]
);
$fieldsets .= $this->Html->tag(
    'fieldset#add-form-section-example.visible-fieldset.hidden',
    $this->Html->tag('div.drag-handle')
    . $this->Html->tag('h3', 'Example')
    . $this->Html->tag(
        'button',
        $this->Fa->i('fa-angle-double-up')
        . $this->Html->tag('span.sr-only', __("Replier")),
        [
            'type' => 'button',
            'class' => 'btn btn-link absolute-center',
            'onclick' => 'AsalaeFilter.toggleFilters(this)',
            'title' => __("Replier la section"),
        ]
    )
    . $this->Html->tag(
        'button',
        $this->Fa->i('fa-edit')
        . $this->Html->tag('span.sr-only', __("Modifier")),
        [
            'type' => 'button',
            'class' => 'btn btn-link edit',
            'onclick' => 'editSection(this)',
            'title' => __("Modifier la section"),
        ]
    )
    . $this->Html->tag(
        'button.close',
        $this->Html->tag('span', '×'),
        [
            'type' => 'button',
            'aria-label' => __("Supprimer la section"),
            'title' => __("Supprimer la section"),
            'onclick' => 'deleteSection.call(this)',
        ]
    )
    . $this->Form->control(
        'example_fieldset_legend',
        [
            'name' => false,
            'class' => 'legend',
            'id' => 'example-section-legend',
            'type' => 'hidden',
        ]
    )
    . $this->Form->control(
        'example_fieldset_cond_display_input',
        [
            'name' => false,
            'class' => 'cond_display_input',
            'id' => 'example-section-cond_display_input',
            'type' => 'hidden',
        ]
    )
    . $this->Form->control(
        'example_fieldset_cond_display_way',
        [
            'name' => false,
            'class' => 'cond_display_way',
            'id' => 'example-section-cond_display_way',
            'type' => 'hidden',
        ]
    )
    . $this->Form->control(
        'example_fieldset_cond_display_value',
        [
            'name' => false,
            'class' => 'cond_display_value',
            'id' => 'example-section-cond_display_value',
            'type' => 'hidden',
        ]
    )
    . $this->Form->control(
        'example_fieldset_cond_display_value_select',
        [
            'name' => false,
            'class' => 'cond_display_value_select',
            'id' => 'example-section-cond_display_value_select',
            'type' => 'hidden',
        ]
    )
    . $this->Form->control(
        'example_fieldset_cond_display_value_file',
        [
            'name' => false,
            'class' => 'cond_display_value_file',
            'id' => 'example-section-cond_display_value_file',
            'type' => 'hidden',
        ]
    )
    . $this->Form->control(
        'example_fieldset_cond_display_field',
        [
            'name' => false,
            'class' => 'cond_display_field',
            'id' => 'example-section-cond_display_field',
            'type' => 'hidden',
        ]
    )
    . $this->Form->control(
        'example_fieldset_repeatable',
        [
            'name' => false,
            'class' => 'repeatable',
            'id' => 'example-section-repeatable',
            'type' => 'hidden',
        ]
    )
    . $this->Form->control(
        'example_fieldset_required',
        [
            'name' => false,
            'class' => 'required',
            'id' => 'example-section-required',
            'type' => 'hidden',
        ]
    )
    . $this->Form->control(
        'example_fieldset_cardinality',
        [
            'name' => false,
            'class' => 'cardinality',
            'id' => 'example-section-cardinality',
            'type' => 'hidden',
        ]
    )
    . $this->Form->control(
        'example_fieldset_button_name',
        [
            'name' => false,
            'class' => 'button_name',
            'id' => 'example-section-button_name',
            'type' => 'hidden',
        ]
    )
    . $this->Html->tag('/div') // .drag-handle
    . $this->Html->tag('div.filters')
    . '<hr>'
    . $this->Html->tag('div.insert-inputs', '')
    . $this->Html->tag(
        'button.btn.btn-success.add-input',
        $this->Fa->charte('Ajouter', __("Ajouter un champ de formulaire")),
        ['type' => 'button', 'onclick' => 'addInput.call(this)']
    )
    . $this->Html->tag('/div') // .filters (zone repliable)
);

$fieldsets .= $this->Html->tag('div#edit-form-sections');

/** @var EntityInterface $formFieldset */
foreach ($entity->get('form_fieldsets') as $formFieldset) {
    $i = $formFieldset->get('ord');
    $repeatable = [];
    if ($formFieldset->get('repeatable')) {
        $repeatable['data-required'] = $formFieldset->get('required') ? '1' : '0';
        $repeatable['data-cardinality'] = $formFieldset->get('cardinality');
        $repeatable['data-button-name'] = $formFieldset->get('button_name');
    }
    $class = 'default-fieldset';
    if ((string)$formFieldset->get('disable_expression') !== '[]') {
        $class = 'hiddable-fieldset';
    }
    $fieldsets .= $this->Html->tag(
        "fieldset.visible-fieldset.$class",
        null,
        [
            'data-index' => $i,
            'data-repeatable' => $formFieldset->get('repeatable'),
        ] + $repeatable
    );
    $legend = $formFieldset->get('legend')
        ? h($formFieldset->get('legend'))
        : __("Section sans titre #{0}", $i + 1);
    $fieldsets .= $this->Html->tag('div.drag-handle');

    $fieldsets .= $this->Html->tag('h3', $legend);
    $fieldsets .= $this->Html->tag(
        'button',
        $this->Fa->i('fa-angle-double-up')
        . $this->Html->tag('span.sr-only', __("Replier")),
        [
            'type' => 'button',
            'class' => 'btn btn-link absolute-center',
            'onclick' => 'AsalaeFilter.toggleFilters(this)',
            'title' => __("Replier la section"),
        ]
    )
    . $this->Html->tag(
        'button',
        $this->Fa->i('fa-edit')
        . $this->Html->tag('span.sr-only', __("Modifier")),
        [
            'type' => 'button',
            'class' => 'btn btn-link edit',
            'onclick' => 'editSection(this)',
            'title' => __("Modifier la section"),
        ]
    )
    . $this->Html->tag(
        'button.close',
        $this->Html->tag('span', '×'),
        [
            'type' => 'button',
            'aria-label' => __("Supprimer la section"),
            'onclick' => 'deleteSection.call(this)',
            'title' => __("Supprimer la section"),
        ]
    );
    $fieldsets .= $this->Form->control(
        "fieldsets.$i.id",
        [
            'type' => 'hidden',
            'value' => $formFieldset->id,
        ]
    );
    $fieldsets .= $this->Form->control(
        "fieldsets.$i.legend",
        [
            'type' => 'hidden',
            'value' => $formFieldset->get('legend'),
            'class' => 'legend',
        ]
    );
    $fieldsets .= $this->Form->control(
        "fieldsets.$i.cond_display_input",
        [
            'type' => 'hidden',
            'value' => $formFieldset->get('cond_display_input'),
            'class' => 'cond_display_input',
        ]
    );
    $fieldsets .= $this->Form->control(
        "fieldsets.$i.cond_display_way",
        [
            'type' => 'hidden',
            'value' => $formFieldset->get('cond_display_way'),
            'class' => 'cond_display_way',
        ]
    );
    $fieldsets .= $this->Form->control(
        "fieldsets.$i.cond_display_value",
        [
            'type' => 'hidden',
            'value' => $formFieldset->get('cond_display_value'),
            'class' => 'cond_display_value',
        ]
    );
    $fieldsets .= $this->Form->control(
        "fieldsets.$i.cond_display_value_select",
        [
            'type' => 'hidden',
            'value' => $formFieldset->get('cond_display_value_select'),
            'class' => 'cond_display_value_select',
        ]
    );
    $fieldsets .= $this->Form->control(
        "fieldsets.$i.cond_display_value_file",
        [
            'type' => 'hidden',
            'value' => $formFieldset->get('cond_display_value_file'),
            'class' => 'cond_display_value_file',
        ]
    );
    $fieldsets .= $this->Form->control(
        "fieldsets.$i.cond_display_field",
        [
            'type' => 'hidden',
            'value' => $formFieldset->get('cond_display_field'),
            'class' => 'cond_display_field',
        ]
    );
    $fieldsets .= $this->Form->control(
        "fieldsets.$i.repeatable",
        [
            'type' => 'hidden',
            'value' => $formFieldset->get('repeatable'),
            'class' => 'repeatable',
        ]
    );
    $fieldsets .= $this->Form->control(
        "fieldsets.$i.required",
        [
            'type' => 'hidden',
            'value' => $formFieldset->get('required'),
            'class' => 'required',
        ]
    );
    $fieldsets .= $this->Form->control(
        "fieldsets.$i.cardinality",
        [
            'type' => 'hidden',
            'value' => $formFieldset->get('cardinality'),
            'class' => 'cardinality',
        ]
    );
    $fieldsets .= $this->Form->control(
        "fieldsets.$i.button_name",
        [
            'type' => 'hidden',
            'value' => $formFieldset->get('button_name'),
            'class' => 'button_name',
        ]
    );
    $fieldsets .= $this->Html->tag('/div');//.drag-handle
    $fieldsets .= $this->Html->tag('div.filters');
    $fieldsets .= '<hr>';

    $fieldsets .= $this->Html->tag('div.insert-inputs.ui-sortable');
    /** @var EntityInterface $formInput */
    foreach ($formFieldset->get('form_inputs') as $formInput) {
        $j = $formInput->get('ord');
        $inputData = array_filter($formInput->toArray());
        $inputData['disable_expression'] = (string)$inputData['disable_expression'];
        unset($inputData['app_meta']);
        ob_start();
        include 'generate_input.php';
        $fieldsets .= substr(ob_get_clean(), 0, -6);// strlen('</div>') == 6
        $fieldsets .= $this->Form->control(
            "fieldsets.$i.inputs.$j",
            [
                'name' => "fieldsets[$i][inputs][]",
                'type' => 'hidden',
                'value' => json_encode($inputData),
                'class' => 'hidden-data',
            ]
        );
        $fieldsets .= '</div>';
    }
    $fieldsets .= $this->Html->tag('/div');//.insert-inputs.ui-sortable

    $fieldsets .= $this->Html->tag(
        'button.btn.btn-success.add-input',
        $this->Fa->charte('Ajouter', __("Ajouter un champ de formulaire")),
        ['type' => 'button', 'onclick' => 'addInput.call(this)']
    );
    $fieldsets .= $this->Html->tag('/div');//.filters
    $fieldsets .= $this->Html->tag('/fieldset');
}

$fieldsets .= $this->Html->tag('/div');

$fieldsets .= $this->Html->tag('div.btn-separator');
$fieldsets .= $this->Html->tag(
    'button.btn.btn-success',
    $this->Fa->charte('Ajouter', __("Ajouter une section de formulaire")),
    ['type' => 'button', 'id' => 'edit-form-add-section']
);
$fieldsets .= $this->Html->tag(
    'button.btn.btn-primary',
    $this->Fa->charte('Visualiser', __("Prévisualiser le formulaire")),
    ['type' => 'button', 'onclick' => 'previewForm()']
);
$fieldsets .= $this->Html->tag('/div');

$fieldsets .= $this->Form->end();

/**
 * Extracteurs
 */
$tableExtractors = $this->Table
    ->create('edit-form-extractor-table', ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'name' => [
                'label' => __("Nom"),
            ],
            'type' => [
                'label' => __("Type"),
            ],
            'form_input.name' => [
                'label' => __("Nom du champ de formulaire"),
            ],
            'url' => [
                'label' => __("Url du webservice"),
            ],
            'data_format' => [
                'label' => __("Format"),
            ],
            'data_path' => [
                'label' => __("Chemin"),
            ],
            'linked' => [
                'label' => __("Lié à"),
                'callback' => 'TableHelper.array',
            ],
        ]
    )
    ->data($entity->get('form_extractors'))
    ->params(
        [
            'identifier' => 'id',
            'checkbox' => false,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionEditExtractor({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/form-extractors/edit'),
                'params' => ['id', 'name']
            ],
            [
                'data-callback' => sprintf(
                    "actionDeleteExtractor(%s, '%s')('{0}')",
                    $jsTableExtractors,
                    $this->Url->build('/form-extractors/delete')
                ),
                'type' => 'button',
                'class' => 'btn-link delete',
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'confirm' => __("Supprimer cette extractor ?"),
                'display' => $this->Acl->check('/form-extractors/delete'),
                'displayEval' => 'data[{index}].deletable',
                'params' => ['id', 'name']
            ],
        ]
    );

$addBtn = $this->ModalForm
    ->create('edit-entity-add-extractor-modal', ['size' => 'large'])
    ->modal(__("Ajout d'un extracteur"))
    ->javascriptCallback('refreshEditFormModal')
    ->output(
        'button',
        $this->Fa->charte('Ajouter', __("Ajouter un extracteur")),
        '/FormExtractors/add/' . $entity->get('id')
    )
    ->generate(['class' => 'btn btn-success', 'type' => 'button']);

$extractorsTabContent = $this->Html->tag(
    'div',
    $addBtn,
    ['class' => 'separator padding10']
)
    . $this->Html->tag(
        'header',
        $this->Html->tag('h2.h4', __("Liste des données à extraire"))
    )
    . $tableExtractors;

$extractorsTabContent .= $this->Form->create($entity, ['idPrefix' => 'edit-forms-extractors-']);
$extractorsTabContent .= $this->Form->control(
    'tab',
    [
        'type' => 'hidden',
        'value' => 'extractors',
    ]
);
$extractorsTabContent .= $this->Form->end();

/**
 * Variables
 */
$tableVariables = $this->Table
    ->create('edit-form-variable-table', ['class' => 'table table-striped table-hover smart-td-size'])
    ->url(
        $url = [
            'controller' => 'forms',
            'action' => 'paginate-variables',
            $id,
            '?' => [
                'sort' => 'ord',
                'direction' => 'asc',
            ]
        ]
    )
    ->fields(
        [
            'name' => [
                'label' => __("Nom"),
            ],
            'form_variable.type' => [
                'label' => __("Type"),
            ],
            'form_variable.twig' => [
                'label' => __("Twig"),
                'callback' => 'inlineTwig',
            ],
            'linked' => [
                'label' => __("Lié à"),
                'callback' => 'TableHelper.array',
            ],
        ]
    )
    ->data($entity->get('form_calculators'))
    ->params(
        [
            'identifier' => 'id',
            'checkbox' => false,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionEditVariable({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/form-calculators/edit'),
                'params' => ['id', 'name']
            ],
            [
                'data-callback' => sprintf(
                    "actionDeleteVariable(%s, '%s')('{0}')",
                    $jsTableVariables,
                    $this->Url->build('/form-calculators/delete')
                ),
                'type' => 'button',
                'class' => 'btn-link delete',
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'confirm' => __("Supprimer cette variable ?"),
                'display' => $this->Acl->check('/form-calculators/delete'),
                'displayEval' => 'data[{index}].deletable',
                'params' => ['id', 'name']
            ],
        ]
    );

$addBtn = $this->ModalForm
    ->create('add3-entity-add-variable-modal', ['size' => 'large'])
    ->modal(__("Ajout d'une variable"))
    ->javascriptCallback('refreshEditFormModal')
    ->output(
        'button',
        $this->Fa->charte('Ajouter', __("Ajouter une variable")),
        '/form-calculators/add/' . $entity->get('id')
    )
    ->generate(['class' => 'btn btn-success', 'type' => 'button']);

$variablesTabContent = $this->Html->tag(
    'div',
    $addBtn,
    ['class' => 'separator padding10']
)
    . $this->Html->tag(
        'header',
        $this->Html->tag('h2.h4', __("Liste des données à calculer"))
    )
    . $tableVariables;

$variablesTabContent .= $this->Form->create($entity, ['idPrefix' => 'edit-forms-variables-']);
$variablesTabContent .= $this->Form->control(
    'tab',
    [
        'type' => 'hidden',
        'value' => 'variables',
    ]
);
$variablesTabContent .= $this->Form->end();

/**
 * Entête du transfert
 */
$formHeaders = $this->Form->create(
    $entity,
    [
        'id' => 'form-edit-transfer-headers',
        'idPrefix' => 'edit-transfer-headers-',
    ]
);
$formHeaders .= $this->Html->tag('h2.h4', __("Entête du transfert"));
$tableHeaders = $this->Table
    ->create('edit-form-transfer-header-table', ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'label' => [
                'label' => __("Nom"),
                'callback' => 'requiredCallback',
                'titleEval' => 'name',
            ],
            'form_variable.twig' => [
                'label' => __("Twig"),
                'callback' => 'twigForm',
                'class' => 'twig',
            ],
        ]
    )
    ->data(array_filter($dataTransferHeaders, fn($v) => $v['section'] === 'header'))
    ->params(
        [
            'identifier' => 'name',
            'checkbox' => false,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionAddEditVariable('$id/{0}')",
                'type' => 'button',
                'class' => 'btn-link text-success',
                'label' => $this->Fa->charte('Ajouter'),
                'title' => $title = __("Ajouter {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/form-calculators/add-edit-header-var'),
                'displayEval' => 'data[{index}].id === null',
                'params' => ['name', 'label']
            ],
            [
                'onclick' => "actionAddEditVariable('$id/{0}')",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/form-calculators/add-edit-header-var'),
                'displayEval' => 'data[{index}].id !== null',
                'params' => ['id', 'label']
            ],
            [
                'data-callback' => sprintf(
                    "actionDeleteHeaderVar(%s, '%s')('{0}', '{2}')",
                    $jsTableTransferHeader,
                    $this->Url->build('/form-calculators/delete-transfer-header')
                ),
                'type' => 'button',
                'class' => 'btn-link delete',
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'confirm' => __("Supprimer cette variable ?"),
                'display' => $this->Acl->check('/form-calculators/delete-transfer-header'),
                'displayEval' => 'data[{index}].id !== null',
                'params' => ['id', 'label', 'name']
            ],
        ]
    );
$formHeaders .= $tableHeaders;

if (in_array($entity->get('seda_version'), ['seda2.1', 'seda2.2'])) {
    $formHeaders .= $this->Html->tag(
        'h2.h4.mt-5',
        __("Métadonnées de gestion par défaut : ManagementMetadata")
    );
    $tableTransferManagement = $this->Table
        ->create('edit-form-transfer-management-table', ['class' => 'table table-striped table-hover smart-td-size'])
        ->fields(
            [
                'label' => [
                    'label' => __("Nom"),
                    'callback' => 'requiredCallback',
                    'titleEval' => 'name',
                ],
                'form_variable.twig' => [
                    'label' => __("Twig"),
                    'callback' => 'twigForm',
                    'class' => 'twig',
                ],
            ]
        )
        ->data(
            array_values(
                array_filter(
                    $dataTransferHeaders,
                    fn($v) => $v['section'] === 'management'
                    && (!$v['additionnal'] || $v['id'])
                )
            )
        )
        ->params(
            [
                'identifier' => 'name',
                'checkbox' => false,
            ]
        )
        ->actions(
            [
                [
                    'onclick' => "actionAddEditVariable('$id/{0}')",
                    'type' => 'button',
                    'class' => 'btn-link text-success',
                    'label' => $this->Fa->charte('Ajouter'),
                    'title' => $title = __("Ajouter {0}", '{1}'),
                    'aria-label' => $title,
                    'display' => $this->Acl->check('/form-calculators/add-edit-header-var'),
                    'displayEval' => 'data[{index}].id === null',
                    'params' => ['name', 'label']
                ],
                [
                    'onclick' => "actionAddEditVariable('$id/{0}')",
                    'type' => 'button',
                    'class' => 'btn-link',
                    'label' => $this->Fa->charte('Modifier'),
                    'title' => $title = __("Modifier {0}", '{1}'),
                    'aria-label' => $title,
                    'display' => $this->Acl->check('/form-calculators/add-edit-header-var'),
                    'displayEval' => 'data[{index}].id !== null',
                    'params' => ['id', 'label']
                ],
                [
                    'data-callback' => sprintf(
                        "actionDeleteHeaderVar(%s, '%s')('{0}', '{2}')",
                        $jsTableTransferManagement,
                        $this->Url->build('/form-calculators/delete-transfer-header')
                    ),
                    'type' => 'button',
                    'class' => 'btn-link delete',
                    'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => $title = __("Supprimer {0}", '{1}'),
                    'aria-label' => $title,
                    'confirm' => __("Supprimer cette variable ?"),
                    'display' => $this->Acl->check('/form-calculators/delete-transfer-header'),
                    'displayEval' => 'data[{index}].id !== null',
                    'params' => ['id', 'label', 'name']
                ],
            ]
        );

    $formHeaders .= $tableTransferManagement;

    $missingAdditionnals = [];
    foreach ($dataTransferHeaders as $value) {
        if ($value['additionnal'] && $value['section'] === 'management' && !$value['id']) {
            $missingAdditionnals[$value['name']] = $value['label'];
        }
    }
    $formHeaders .= $this->Form->control(
        'add_managament_header',
        [
            'label' => __("Ajouter une métadonnée"),
            'options' => $missingAdditionnals,
            'empty' => __("-- Sélectionner une métadonnée à ajouter --"),
            'onchange' => "addAdditionnalManagementData($tableTransferManagement->tableObject, $(this))",
        ]
    );

    $formHeaders .= $this->Html->tag(
        'h2.h4.mt-5',
        __("Versions des référentiels : CodeListVersions")
    );
    $tableTransferCodeListVersions = $this->Table
        ->create('edit-form-transfer-version-table', ['class' => 'table table-striped table-hover smart-td-size'])
        ->fields(
            [
                'label' => [
                    'label' => __("Nom"),
                    'callback' => 'requiredCallback',
                    'titleEval' => 'name',
                ],
                'form_variable.twig' => [
                    'label' => __("Twig"),
                    'callback' => 'twigForm',
                    'class' => 'twig',
                ],
            ]
        )
        ->data(
            array_values(
                array_filter(
                    $dataTransferHeaders,
                    fn($v) => $v['section'] === 'version'
                    && (!$v['additionnal'] || $v['id'])
                )
            )
        )
        ->params(
            [
                'identifier' => 'name',
                'checkbox' => false,
            ]
        )
        ->actions(
            [
                [
                    'onclick' => "actionAddEditVariable('$id/{0}')",
                    'type' => 'button',
                    'class' => 'btn-link text-success',
                    'label' => $this->Fa->charte('Ajouter'),
                    'title' => $title = __("Ajouter {0}", '{1}'),
                    'aria-label' => $title,
                    'display' => $this->Acl->check('/form-calculators/add-edit-header-var'),
                    'displayEval' => 'data[{index}].id === null',
                    'params' => ['name', 'label']
                ],
                [
                    'onclick' => "actionAddEditVariable('$id/{0}')",
                    'type' => 'button',
                    'class' => 'btn-link',
                    'label' => $this->Fa->charte('Modifier'),
                    'title' => $title = __("Modifier {0}", '{1}'),
                    'aria-label' => $title,
                    'display' => $this->Acl->check('/form-calculators/add-edit-header-var'),
                    'displayEval' => 'data[{index}].id !== null',
                    'params' => ['id', 'label']
                ],
                [
                    'data-callback' => sprintf(
                        "actionDeleteHeaderVar(%s, '%s')('{0}', '{2}')",
                        $jsTableTransferCodeListVersions,
                        $this->Url->build('/form-calculators/delete-transfer-header')
                    ),
                    'type' => 'button',
                    'class' => 'btn-link delete',
                    'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => $title = __("Supprimer {0}", '{1}'),
                    'aria-label' => $title,
                    'confirm' => __("Supprimer cette variable ?"),
                    'display' => $this->Acl->check('/form-calculators/delete-transfer-header'),
                    'displayEval' => 'data[{index}].id !== null',
                    'params' => ['id', 'label', 'name']
                ],
            ]
        );

    $formHeaders .= $tableTransferCodeListVersions;

    $missingAdditionnals = [];
    foreach ($dataTransferHeaders as $value) {
        if ($value['additionnal'] && $value['section'] === 'version' && !$value['id']) {
            $missingAdditionnals[$value['name']] = $value['label'];
        }
    }
    $formHeaders .= $this->Form->control(
        'add_version_header',
        [
            'label' => __("Ajouter une métadonnée"),
            'options' => $missingAdditionnals,
            'empty' => __("-- Sélectionner une métadonnée à ajouter --"),
            'onchange' => "addAdditionnalVersion($tableTransferCodeListVersions->tableObject, $(this))",
        ]
    );
}
$formHeaders .= $this->Form->end();

/**
 * Tabs
 */
$tabs = $this->Tabs->create('tabs-edit-form', ['class' => 'row no-padding']);
$tabs->add(
    'tab-edit-form-infos',
    $this->Fa->i('fa-file-code-o', __("Informations principales")),
    $infosTab,
    ['class' => 'save-on-change']
);
$tabs->add(
    'tab-edit-form-fieldsets',
    $this->Fa->i('fa-object-group', __("Zones de saisie")),
    $fieldsets,
    ['class' => 'save-on-change']
);
$tabs->add(
    'tab-edit-form-extractors',
    $this->Fa->i('fa-external-link', __("Données à extraire")),
    $extractorsTabContent,
    ['class' => 'save-on-change']
);
$tabs->add(
    'tab-edit-form-variables',
    $this->Fa->i('fa-cube', __("Données à calculer")),
    $variablesTabContent,
    ['class' => 'save-on-change']
);
$tabs->add(
    'tab-edit-form-transfer-headers',
    $this->Fa->i('fa-newspaper-o', __("Transfert")),
    $formHeaders,
    ['class' => 'save-on-change']
);
$tabs->add(
    'tab-edit-form-units',
    $this->Fa->i('fa-archive', __("Arborescence")),
    require 'edit-form-units.php',
    ['class' => 'save-on-change']
);

echo $tabs;
?>
<script>
    /*
    globals
    actionEditForm,
    initialArchiveUnitForm,
    addFormInput1,
    editFormInput,
    addFormFieldset,
    editFormFieldset
    */
    var sectionContainer;
    var btnAddSection;
    var sectionCount;
    var addInputToSection;
    var currentData;
    var editingInput;
    var editingFieldset;
    var tabsDiv;
    var fieldsetForm;
    var formForm;
    var initialFormFormValue;
    var initialFieldsetFormValue;
    var editFormModal;
    var archiveUnitForm;
    var initialArchiveUnitForm;
    var tableAddTransferHeaders;
    var tableExtractors;
    var tableVariables;
    var formAddTransferHeaders;
    var modal;
    var acceptBtn;
    var cancelBtn;

    formAddTransferHeaders = $('#form-edit-transfer-headers');
    modal = formAddTransferHeaders.closest('.modal');

    function disabledDeleteButtonTitle(element)
    {
        var linkedToJson = element.find('[data-deletable="false"]').first().attr('data-linked-to');
        var linkedTo = linkedToJson ? JSON.parse(linkedToJson) : null;
        var str = __("Cet élément est utilisé et ne peut donc pas être supprimé");
        if (!linkedTo || linkedTo.length === 0) {
            return str;
        }
        return str + "\n - " + linkedTo.join("\n - ");
    }

    function initEditForm() {
        AsalaeGlobal.select2($('#edit-form-form-transferring-agencies-ids'));
        sectionContainer = $('#edit-form-sections');
        btnAddSection = $('#edit-form-add-section');
        sectionCount = sectionContainer.find('> fieldset').length;
        addInputToSection = null;
        currentData = null;
        editingInput = null;
        editingFieldset = null;
        tabsDiv = $('#tabs-edit-form');
        fieldsetForm = $('#edit-form-fieldsets');
        formForm = $('#edit-form-form');
        initialFormFormValue = '';
        initialFieldsetFormValue = '';
        editFormModal = fieldsetForm.closest('.modal');
        archiveUnitForm = undefined; // défini lors de l'ajout/modification d'un archive_unit
        initialArchiveUnitForm = undefined; // défini lors de l'ajout/modification d'un archive_unit

        new AsalaeMce({selector: '#edit-form-form-user-guide'});

        setTimeout(setFormInitialValues, 0);

        $('.save-on-change').off('.save-on-change').on(
            'mousedown.save-on-change keydown.save-on-change',
            function (e) {
                e.preventDefault();
                var activetab = tabsDiv.find('> ul > li.ui-state-active').first().attr('aria-controls');
                var msg = __("Vous avez apporté des modifications au formulaire. Souhaitez-vous les sauvegarder ?");
                var formValue;
                switch (activetab) {
                    case 'tab-edit-form-infos':
                        formValue = formForm.serialize();
                        if (formValue !== initialFormFormValue && confirm(msg)) {
                            submitFormForm.call(formForm, $.Event('submit'), false);
                        }
                        initialFormFormValue = formValue;
                        break;
                    case 'tab-edit-form-fieldsets':
                        formValue = fieldsetForm.serialize();
                        if (formValue !== initialFieldsetFormValue && confirm(msg)) {
                            submitFieldsetForm.call(fieldsetForm, $.Event('submit'), false);
                        }
                        initialFieldsetFormValue = formValue;
                        break;
                }
            }
        );

        fieldsetForm.find('.insert-inputs .form-group').each(
            function () {
                var element = $(this);
                var dataIndex = element.closest('.visible-fieldset').attr('data-index');
                var btnClose = $('<button type="button" onclick="deleteInput.call(this)" class="close">')
                    .attr('title', __("Supprimer le champ de formulaire"))
                    .append('<span>×</span>')
                    .attr('aria-label', __("Supprimer le champ de formulaire"));
                var btnEdit = $('<button type="button" onclick="editInput(this)" class="btn btn-link edit">')
                    .attr('title', __("Modifier le champ de formulaire"))
                    .append($('<i class="fa fa-edit" aria-hidden="true">'))
                    .append($('<span class="sr-only">').text(__("Modifier")));
                element.find('[name]')
                    .not('.hidden-data')
                    .removeAttr('name')
                    .addClass('disabled')
                    .attr('onclick', 'return false');
                if (element.find('[data-deletable="false"]').length) {
                    btnClose.disable()
                        .attr('title', disabledDeleteButtonTitle(element))
                        .find('span')
                        .empty()
                        .append('<i class="fa fa-lock" aria-hidden="true"></i>');
                    element.closest('fieldset').find('> div > button.close')
                        .disable()
                        .attr('title', __("Cet élément est utilisé et ne peut donc pas être supprimé"))
                        .find('span')
                        .empty()
                        .append('<i class="fa fa-lock" aria-hidden="true"></i>')
                }
                element.prepend(btnClose);
                btnEdit.insertAfter(element.find('.fake-label, label').first());
                element.addClass('ui-sortable-handle');
                element.find('[required]').prop('required', false);
                if (element.find('select[data-mount-select2]').length) {
                    element.find('option[value=""]').remove();
                    element.find('select[data-mount-select2]').select2(
                        {
                            dropdownParent: editFormModal,
                            placeholder: element.find('select[data-mount-select2]').attr('data-select2-placeholder'),
                            width: '100%'
                        }
                    );
                }
            }
        );

        /**
         * Ajout d'une section
         */
        btnAddSection.on(
            'click',
            function () {
                addFormFieldset();
            }
        );

        sectionContainer.sortable({handle: ".drag-handle", cursor: "grabbing"});
        $('.insert-inputs').sortable({cursor: "grabbing"});

        tableAddTransferHeaders = <?=$jsTableTransferHeader?>;
        tableExtractors = <?=$jsTableExtractors?>;
        tableVariables = <?=$jsTableVariables?>;
        modal = formAddTransferHeaders.closest('.modal');
        acceptBtn = modal.find('button.accept:not(.clone)');
        cancelBtn = modal.find('button.cancel:not(.clone)');
    }
    initEditForm();

    function setFormInitialValues() {
        initialFormFormValue = formForm.serialize();
        initialFieldsetFormValue = fieldsetForm.serialize();
    }

    function submitForm(form, tab) {
        return function (event) {
            var submit = form.find('button[type=submit]').disable();
            event.preventDefault();
            if (form.get(0).checkValidity()) {
                $.ajax({
                    url: form.attr('action'),
                    method: 'POST',
                    data: form.serialize(),
                    headers: {"No-Layout": true},
                    success: function (content, textStatus, jqXHR) {
                        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                            form.find('.has-error')
                                .removeClass('has-error')
                                .find('.error-message').remove();

                            setTimeout(function () {
                                form.parent()
                                    .css('background-color', '#5cb85c')
                                    .animate({'background-color': 'transparent'});

                            }, 0);
                        } else {
                            var submitFunc = form.data('events')[0].fn,
                                modalBody = form.closest('div.modal-body');

                            modalBody
                                .find('*').off().remove().end()
                                .html(content);

                            modalBody.find('form').off().submit(submitFunc)
                                .find('[type=submit]').remove();

                            tab.click();
                        }
                    },
                    error: function () {
                        alert(PHP.messages.genericError);
                    },
                    complete: function () {
                        submit.enable();
                    }
                });
            } else {
                tab.click();
                form.get(0).reportValidity();
            }
        }
    }

    function submitFormForm(event) {
        submitForm(formForm, $('a[href="#tab-edit-form-infos"]'))(event);
    }

    function submitFieldsetForm(event) {
        submitForm(fieldsetForm, $('a[href="#tab-edit-form-fieldset"]'))(event);
    }

    function getCurrentFormData(raw = false) {
        if (raw) {
            return fieldsetForm.serialize();
        } else {
            return fieldsetForm.serializeObject();
        }
    }

    function addInput() {
        $('.modal-header h3 span.type').remove();
        addInputToSection = $(this).closest('fieldset');
        currentData = {
            form: getCurrentFormData(),
            fieldset: getFieldsetData(this)
        };
        addFormInput1();
    }

    function deleteSection() {
        if ($(this).closest('#add-form-section-example').length === 1) {
            return;
        }
        if (confirm(
            __("Supprimer la section supprimera également tout ce qu'elle contient. Voulez-vous continuer ?")
        )
        ) {
            $(this).closest('fieldset').remove();
            if (isEmptySectionExists() === false) {
                btnAddSection.enable();
            }
        }
    }

    function isEmptySectionExists() {
        var emptySectionExists = false;
        sectionContainer.find('.insert-inputs .form-group').each(
            function () {
                if ($(this).find('input[type=hidden]').length === 0) {
                    emptySectionExists = true;
                    return false;
                }
            }
        );
        return emptySectionExists;
    }

    function afterAddSection(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success')) {
            var fieldset = $('#add-form-section-example').clone();
            fieldset.removeAttr('id');
            fieldset.find('[id]').each(
                function () {
                    $(this).removeAttr('id');
                }
            );
            fieldset.find('h3').text(
                content.legend
                    ? content.legend
                    : __("Section sans titre #{0}", sectionCount + 1)
            );
            let fieldsetHiddenInputs = [
                'legend', 'repeatable', 'required', 'cardinality', 'button_name',
                'cond_display_input', 'cond_display_way', 'cond_display_value',
                'cond_display_value_select', 'cond_display_value_file',
                'cond_display_field'
            ];
            $(fieldsetHiddenInputs).each(
                function (key, value) {
                    let val = content[value];
                    if (typeof content[value] === 'object') {
                        val = JSON.stringify(content[value]);
                    } else if (typeof content[value] === 'boolean') {
                        val = content[value] === true;
                    }
                    fieldset.find('input.%s'.format(value))
                        .attr('name', 'fieldsets[%d][%s]'.format(sectionCount, value))
                        .attr('id', 'fieldsets-%d-%s'.format(sectionCount, value))
                        .val(val);
                }
            );
            fieldset.removeClass('hidden');
            fieldset.attr('data-index', sectionCount);
            if (content.repeatable) {
                fieldset.attr('data-repeatable', 'true');
                fieldset.find('input.repeatable').val(content.repeatable ? '1' : '0');
                fieldset.find('input.required').val(content.required ? '1' : '0');
                fieldset.attr('data-required', content.required ? 'true' : 'false');
                fieldset.attr('data-cardinality', content.cardinality);
                fieldset.attr('data-button-name', content.button_name);
            }
            fieldset.find('.insert-inputs').sortable({cursor: "grabbing"});
            sectionCount++;
            sectionContainer.append(fieldset);
            btnAddSection.disable();

            if (content.disable_expression?.cond_display_input) {
                fieldset.addClass('hiddable-fieldset');
            }
        }
    }

    function editSection(element) {
        currentData = getFieldsetData(element);
        editFormFieldset();
    }

    function afterEditSection(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            let fieldsetHiddenInputs = [
                'legend', 'repeatable', 'required', 'cardinality', 'button_name',
                'cond_display_input', 'cond_display_way', 'cond_display_value',
                'cond_display_value_select', 'cond_display_value_file',
                'cond_display_field'
            ];
            $(fieldsetHiddenInputs).each(
                function (key, value) {
                    let val = content[value];
                    if (typeof content[value] === 'object') {
                        val = JSON.stringify(content[value]);
                    } else if (typeof content[value] === 'boolean') {
                        val = content[value] === true;
                    }
                    editingFieldset.find('input.%s'.format(value)).val(val);
                }
            );
            editingFieldset.find('h3')
                .text(
                    content.legend
                        ? content.legend
                        : __("Section sans titre #{0}", parseInt(editingFieldset.attr('data-index'), 10) + 1)
                );
            if (typeof content.required === 'undefined') {
                editingFieldset.find('input.required').val('');
            }
            if (content.disable_expression?.cond_display_input) {
                editingFieldset.addClass('hiddable-fieldset');
            } else {
                editingFieldset.removeClass('hiddable-fieldset');
            }

            if (content.repeatable) {
                editingFieldset.attr('data-repeatable', 'true');
            } else {
                editingFieldset.removeAttr('data-repeatable');
            }
            editingFieldset.find('input.repeatable').val(content.repeatable ? '1' : '0');
            editingFieldset.find('input.required').val(content.required ? '1' : '0');
            editingFieldset.attr('data-required', content.required ? 'true' : 'false');
            editingFieldset.attr('data-cardinality', content.cardinality);
            editingFieldset.attr('data-button-name', content.button_name);
        }
    }

    function afterAddInput(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Step')) {
            AsalaeModal.stepCallback(content, textStatus, jqXHR);
        } else if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            AsalaeLoading.ajax(
                {
                    url: '<?=$this->Url->build('/forms/generate-input')?>',
                    method: 'POST',
                    data: content,
                    success: function (domElement) {
                        var element = $(domElement);
                        var btnClose = $('<button type="button" onclick="deleteInput.call(this)" class="close">')
                            .attr('title', __("Supprimer le champ de formulaire"))
                            .append('<span>×</span>')
                            .attr('aria-label', __("Supprimer le champ de formulaire"));
                        var btnEdit = $('<button type="button" onclick="editInput(this)" class="btn btn-link edit">')
                            .attr('title', __("Modifier le champ de formulaire"))
                            .append($('<i class="fa fa-edit" aria-hidden="true">'))
                            .append($('<span class="sr-only">').text(__("Modifier")));
                        element.find('[name]').removeAttr('name')
                            .addClass('disabled')
                            .attr('onclick', 'return false');
                        addInputToSection.find('.insert-inputs').append(element);
                        element.prepend(btnClose);
                        btnEdit.insertAfter(element.find('.fake-label, label').first());
                        var input = $('<input type="hidden">')
                            .val(JSON.stringify(content))
                            .attr('name', 'fieldsets[' + addInputToSection.attr('data-index') + '][inputs][]')
                            .addClass('hidden-data');
                        element.append(input);
                        element.addClass('ui-sortable-handle');
                        element.find('[required]').prop('required', false);
                        if (isEmptySectionExists() === false) {
                            btnAddSection.enable();
                        }
                        if (element.find('select[data-mount-select2]').length) {
                            let select2 = element.find('select[data-mount-select2]');
                            select2.find('option[value=""]').remove();
                            select2.select2(
                                {
                                    dropdownParent: editFormModal,
                                    placeholder: select2.attr('data-select2-placeholder')
                                }
                            );
                        }
                    }
                }
            );
        }
    }

    function afterEditInput(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            AsalaeLoading.ajax(
                {
                    url: '<?=$this->Url->build('/forms/generate-input')?>',
                    method: 'POST',
                    data: content,
                    success: function (domElement) {
                        var element = $(domElement);
                        var btnClose = $('<button type="button" onclick="deleteInput.call(this)" class="close">')
                            .attr('title', __("Supprimer le champ de formulaire"))
                            .append('<span>×</span>')
                            .attr('aria-label', __("Supprimer le champ de formulaire"));
                        var btnEdit = $('<button type="button" onclick="editInput(this)" class="btn btn-link edit">')
                            .attr('title', __("Modifier le champ de formulaire"))
                            .append($('<i class="fa fa-edit" aria-hidden="true">'))
                            .append($('<span class="sr-only">').text(__("Modifier")));
                        element.find('[name]').removeAttr('name')
                            .addClass('disabled')
                            .attr('onclick', 'return false');
                        if (element.find('[data-deletable="false"]').length) {
                            btnClose.disable()
                                .attr('title', disabledDeleteButtonTitle(element))
                                .find('span')
                                .empty()
                                .append('<i class="fa fa-lock" aria-hidden="true"></i>');
                            element.closest('fieldset').find('> div > button.close')
                                .disable()
                                .attr('title', __("Cet élément est utilisé et ne peut donc pas être supprimé"))
                                .find('span')
                                .empty()
                                .append('<i class="fa fa-lock" aria-hidden="true"></i>')
                        }
                        element.prepend(btnClose);
                        var index = editingInput.closest('fieldset').attr('data-index');
                        editingInput.find('[id]').each(
                            function () {
                                $(this).removeAttr('id');
                            }
                        );
                        element.insertBefore(editingInput);
                        editingInput.remove();
                        btnEdit.insertAfter(element.find('.fake-label, label').first());
                        var input = $('<input type="hidden">')
                            .val(JSON.stringify(content))
                            .attr('name', 'fieldsets[' + index + '][inputs][]')
                            .addClass('hidden-data');
                        element.append(input);
                        element.addClass('ui-sortable-handle');
                        element.find('[required]').prop('required', false);
                        if (element.find('select[data-mount-select2]').length) {
                            let select2 = element.find('select[data-mount-select2]');
                            select2.find('option[value=""]').remove();
                            select2.select2(
                                {
                                    dropdownParent: editFormModal,
                                    placeholder: select2.attr('data-select2-placeholder')
                                }
                            );
                        }
                    }
                }
            );
        }
    }

    function deleteInput(element) {
        if (confirm(__("Voulez-vous supprimer ce champ ?"))) {
            $(this).closest('.form-group').slideUp(
                200,
                'swing',
                function () {
                    $(this).remove();
                }
            );
            if (isEmptySectionExists()) {
                btnAddSection.disable();
            }
        }
    }

    function editInput(element) {
        $('.modal-header h3 span.type').remove();
        editingInput = $(element).closest('.form-group');
        currentData = {
            field: JSON.parse(editingInput.find('input.hidden-data').val()),
            fieldset: getFieldsetData(element),
            form: getCurrentFormData()
        };
        editFormInput();
    }

    function getFieldsetData(element) {
        editingFieldset = $(element).closest('fieldset');
        fieldsetData = {
            legend: editingFieldset.find('input.legend').val(),
            form: getCurrentFormData(),
            repeatable: editingFieldset.find('input.repeatable').val(),
            current_fieldset: editingFieldset.serializeObject()
        };
        if (fieldsetData.repeatable === '1') {
            fieldsetData.required = editingFieldset.find('input.required').val();
            fieldsetData.cardinality = editingFieldset.find('input.cardinality').val();
            fieldsetData.button_name = editingFieldset.find('input.button_name').val();
        }

        let condVars = [
            'cond_display_input',
            'cond_display_way',
            'cond_display_value',
            'cond_display_value_select',
            'cond_display_value_file',
            'cond_display_field',
        ];
        for (let i = 0; i < condVars.length; i++) {
            let val = editingFieldset.find('input.' + condVars[i]).val();
            if (val) {
                fieldsetData[condVars[i]] = val;
            }
        }

        return fieldsetData;
    }

    function replaceBtn(button, text = null, icon = null)
    {
        modal.find('button.clone.'+button.attr('class').replace(/ /g, '.')).remove();
        var clone = button.clone().show().addClass('clone');
        if (text) {
            clone.contents()
                .filter(function() {
                    return this.nodeType === 3; //Node.TEXT_NODE
                })
                .first()
                .get(0)
                .nodeValue = ' '+text;
        }
        if (icon) {
            clone.find('.fa').attr('class', '').addClass('fas '+icon);
        }
        clone.insertBefore(button);
        button.hide();
        return clone;
    }
    function restoreBtn(button)
    {
        modal.find('button.clone.'+button.attr('class').replace(/ /g, '.')).remove();
        button.show();
    }

    /**
     * transfer_header_comment -> var_comment_id
     * @var {string} var_name
     * @return {string}
     */
    function var_name_to_id(var_name) {
        return 'var_' + (var_name.substr(16)) + '_id';
    }

    /**
     * Callback d'après ajout d'un entête de transfert
     * @param {string|object} content
     * @param {string} textStatus
     * @param {object} jqXHR
     */
    function afterEditHeader(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            refreshEditFormModal(content.edit_data, textStatus, jqXHR);
        }
    }

    function actionDeleteExtractor(table, url) {
        return function (id, var_name) {
            $.ajax(
                {
                    url: url + '/' + id,
                    method: 'DELETE',
                    success: function (content, textStatus, jqXHR) {
                        jqXHR = {getResponseHeader: () => 'true'};
                        refreshEditFormModal(content.edit_data, textStatus, jqXHR);
                    }
                }
            );
        }
    }

    function actionDeleteVariable(table, url) {
        return actionDeleteExtractor(table, url);
    }

    function actionDeleteHeaderVar(table, url) {
        return function (id, name) {
            var data = table.getDataId(name);
            data.id = null;
            data.form_variable = null;
            formAddTransferHeaders.find('[name="' + name + '"]').val('');
            $.ajax(
                {
                    url: url + '/' + id,
                    method: 'DELETE',
                    success: function (content, textStatus, jqXHR) {
                        jqXHR = {getResponseHeader: () => 'true'};
                        refreshEditFormModal(content.edit_data, textStatus, jqXHR);
                    }
                }
            );
            TableGenerator.appendActions(table.data, table.actions);
            table.generateAll();
        }
    }

    function refreshEditFormModal(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            var href = $('.ui-tabs-active:visible').find('a').attr('href');
            $('#edit-form-modal').one(
                'ajax.complete',
                function() {
                    $('#edit-form-modal').find('a[href="'+href+'"]').click();
                }
            );
            actionEditForm(<?=$entity->id?>);
        }
    }

    function afterAddKeyword(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            tableAddUnitKeywords.data.push(content);
            TableGenerator.appendActions(tableAddUnitKeywords.data, tableAddUnitKeywords.actions);
            tableAddUnitKeywords.generateAll();
            if (jqXHR.getResponseHeader('X-Asalae-Step')) {
                AsalaeModal.stepCallback(content, textStatus, jqXHR);
            }
        }
    }

    function addAdditionnalManagementData(table, element) {
        var value = element.val();
        table.data.push({id: null, name: value, label: value.substring(37)});
        table.generateAll();
        element.val('');
        actionAddEditVariable('<?=$entity->id?>/'+value);
    }

    function addAdditionnalVersion(table, element) {
        var value = element.val();
        table.data.push({id: null, name: value, label: value.substring(17)});
        table.generateAll();
        element.val('');
        actionAddEditVariable('<?=$entity->id?>/'+value);
    }
</script>
