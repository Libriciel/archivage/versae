<?php

/**
 * @var Versae\View\AppView $this
 * @var Versae\Form\ImportForm $form
 */

?>
<!--suppress RequiredAttributes -->
<script>
    function selectableUploadRadio(value, context) {
        if (!AsalaeGlobal.is_numeric(context.id) && context.id !== 'local') {
            return '';
        }
        var input = $('<input type="radio" name="fileuploads[id]">').val(context.id);
        setTimeout(function () {
            $('#import-upload-form-table')
                .find('input[name="fileuploads[id]"]')
                .prop('checked', false)
                .first()
                .prop('checked', true);
        }, 0);
        return input;
    }
</script>
<?php
echo $this->Form->create(
    $form,
    [
        'idPrefix' => 'import-form',
        'id' => 'import-form-form',
    ]
);

$uploads = $this->Upload
    ->create($uploadId = 'import-upload-form', ['class' => 'table table-striped table-hover hide'])
    ->fields(
        [
            'select' => [
                'label' => __("Sélectionner"),
                'class' => 'radio-td',
                'callback' => 'selectableUploadRadio'
            ],
            'name' => ['label' => __("Nom de fichier"), 'callback' => 'TableHelper.wordBreak()'],
            'message' => ['label' => __("Message"), 'class' => 'message', 'style' => 'min-width: 250px'],
            'size' => ['label' => __("Taille"), 'callback' => 'TableHelper.readableBytes'],
        ]
    )
    ->data($uploadData ?? [])
    ->params(
        [
            'identifier' => 'id',
            'classEval' => 'data[{index}].valid !== false ? "" : "danger"',
        ]
    );
$jsUpload = $uploads->jsObject;
$jsTableUpload = $this->Upload->getTableId($uploadId);

echo $this->Form->control(
    'transferring_agencies',
    [
        'label' => __("Services versants autorisés à utiliser le formulaire pour les versements"),
        'options' => $transferringAgencies,
        'data-placeholder' => __("-- Sélectionner un/plusieurs service(s) versant(s) --"),
        'multiple' => true,
        'required' => true,
    ]
);

echo $this->Form->control(
    'archiving_system_id',
    [
        'label' => __("Système d'Archivage Electronique (SAE) destinataire"),
        'empty' => __("-- Choisir un SAE --"),
        'options' => $archivingSystems,
        'required' => true,
    ]
);

echo '<hr>'
    . $this->Html->tag('span', __("Fichier d'export d'un formulaire"), ['class' => 'fake-label'])
    . $uploads->generate(
        [
            'attributes' => ['accept' => '.json, application/json'],
            'target' => $this->Url->build('/Upload/index?replace=true'),
            'autoretry' => false,
        ]
    );

echo $this->Form->control(
    'import',
    [
        'type' => 'hidden',
        'val' => 'last',
    ]
);
echo $this->Form->control(
    'check',
    [
        'label' => __("Options d'import"),
        'options' => [
            'strict' => __("Échec si un formulaire existe avec le même identifiant"),
            'add' => __("Ajouter la nouvelle version à la suite si formulaire existant"),
            'new' => __("Nouvel identifiant (pas de collision sur formulaires existants)"),
        ],
        'required' => true,
    ]
);
echo $this->Form->control(
    'name',
    [
        'label' => __("Nom du formulaire"),
        'help' => __("Laisser vide pour récupérer celui indiqué dans le fichier"),
        'val' => $this->getRequest()->getData('name')
            ?: $form->getData('name'), // récupère le nom dans le fichier en cas d'erreur
    ]
);
echo $this->Form->control(
    'version_note',
    [
        'label' => __("Note de version"),
        'required' => true,
        'default' => __("Création du formulaire"),
    ]
);
echo $this->Form->end();
?>
<script>
    AsalaeGlobal.select2($('#import-form-transferring-agencies'));
</script>
