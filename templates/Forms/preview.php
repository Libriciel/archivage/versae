<?php

/**
 * @var Versae\View\AppView $this
 */

$formId = 'add-form-preview-form';
echo $this->Form->create($form, ['id' => $formId, 'idPrefix' => 'form-preview']);
echo $this->Form->control(
    'is_submit',
    ['type' => 'hidden', 'value' => '1']
);
$idPrefix = 'preview-form';

require 'generate-form.php';

echo $this->Form->end();
