<?php

/**
 * @var Versae\View\AppView $this
 */

$jstreeId = 'jstree-edit-archive-units';
$dataId = 'data-edit-archive-units';
$searchbarId = 'searchbar-edit-archive-units';
$toggleAddablesId = 'toggle-addables-edit-archive-units';

$loading = $this->Html->tag(
    'div',
    $this->Html->tag(
        'i',
        '',
        ['class' => 'fa fa-4x fa-spinner faa-spin animated']
    ),
    ['class' => 'text-center loading-container']
);

/**
 * Aide: Recherche
 */
$tuto = $this->Html->tag(
    'div.alert.alert-info.alert-soft',
    null,
    ['style' => 'position: relative']
);
$tuto .= $this->Html->tag(
    'h4',
    $this->Fa->i('fa-info-circle', __("Barre de recherche"))
);
$tuto .= $this->Html->tag(
    'button.close',
    $this->Html->tag('span', '×'),
    [
        'type' => 'button',
        'aria-label' => __("Fermer cette alerte"),
        'onclick' => '$(this).parent().parent().find(".alert-soft").slideUp()',
        'style' => 'position: absolute; top: 8px; right: 14px;'
    ]
);
$tuto .= $this->Html->tag('ul');
$tuto .= $this->Html->tag(
    'li',
    $this->Fa->i(
        'fa-i-cursor',
        __("Le terme saisi est recherché dans l'arbre courant")
    )
);
$tuto .= $this->Html->tag('/ul');
$tuto .= $this->Html->tag('/div');

/**
 * Aide: Navigation
 */
$tuto .= $this->Html->tag(
    'div.alert.alert-info.alert-soft',
    null,
    ['style' => 'position: relative']
);
$tuto .= $this->Html->tag(
    'h4',
    $this->Fa->i('fa-info-circle', __("Navigation"))
);
$tuto .= $this->Html->tag(
    'button.close',
    $this->Html->tag('span', '×'),
    [
        'type' => 'button',
        'aria-label' => __("Fermer cette alerte"),
        'onclick' => '$(this).parent().parent().find(".alert-soft").slideUp()',
        'style' => 'position: absolute; top: 8px; right: 14px;'
    ]
);
$tuto .= $this->Html->tag('ul');
$tuto .= $this->Html->tag(
    'li',
    $this->Html->tag(
        'i',
        '',
        [
            'style' => 'background-image: url(/css/jstree/32px.png); background-position: -132px -4px;'
                . ' background-repeat: no-repeat; background-color: transparent; '
                . 'height: 17px; width: 30px; display: inline-block'
        ]
    )
    . __("Élément contenant d'autres éléments (ouverts)")
);
$tuto .= $this->Html->tag(
    'li',
    $this->Html->tag(
        'i',
        '',
        [
            'style' => 'background-image: url(/css/jstree/32px.png); background-position: -100px -4px;'
                . ' background-repeat: no-repeat; background-color: transparent; height: 17px;'
                . ' width: 30px; display: inline-block'
        ]
    )
    . __("Élément contenant d'autres éléments (fermés) - Cliquer pour ouvrir")
);
$tuto .= $this->Html->tag(
    'li',
    $this->Fa->i('fa-archive', __("Unité d'archives"))
);
$tuto .= $this->Html->tag(
    'li',
    $this->Fa->i('fa-paperclip', __("Document"))
);
$tuto .= $this->Html->tag(
    'li',
    $this->Fa->i('fa-files-o', __("Lot de documents"))
);
$tuto .= $this->Html->tag(
    'li',
    $this->Fa->i('fa-code-fork', __("Racine d'une arborescence obtenu d'un fichier compressé (ZIP)"))
);
$tuto .= $this->Html->tag(
    'li',
    $this->Fa->i('fa-search', __("Modification des noeuds de l'arborescence correspondant à la recherche"))
);
$tuto .= $this->Html->tag(
    'li',
    $this->Fa->i('fa-code', __("Modification du noeud de l'arborescence correspondant au chemin indiqué"))
);
$tuto .= $this->Html->tag('/ul');
$tuto .= $this->Html->tag('/div');

/**
 * Aide: Actions
 */
$tuto .= $this->Html->tag(
    'div.alert.alert-info.alert-soft',
    null,
    ['style' => 'position: relative']
);
$tuto .= $this->Html->tag(
    'button.close',
    $this->Html->tag('span', '×'),
    [
        'type' => 'button',
        'aria-label' => __("Fermer cette alerte"),
        'onclick' => '$(this).parent().parent().find(".alert-soft").slideUp()',
        'style' => 'position: absolute; top: 8px; right: 14px;'
    ]
);
$tuto .= $this->Html->tag('h4', $this->Fa->i('fa-info-circle', __("Actions")));
$tuto .= $this->Html->tag('ul');
$tuto .= $this->Html->tag(
    'li',
    $this->Fa->i(
        'fa-mouse-pointer',
        __("Clic gauche pour ouvrir un element en édition")
    )
);
$tuto .= $this->Html->tag(
    'li',
    $this->Fa->i(
        'fa-mouse-pointer',
        __("Glisser-déposer - permet de déplacer dans une autre unité d'archives")
    )
);
$tuto .= $this->Html->tag(
    'li',
    $this->Fa->i(
        'fa-plus-square text-success',
        __("Clic gauche sur le \"+\" vert pour ajouter l'élément")
    )
);
$tuto .= $this->Html->tag('/ul');
$tuto .= $this->Html->tag('/div');

$formUnit = $this->Html->tag('div.container-flex.row');
$formUnit .= $this->Form->create(
    $entity,
    [
        'idPrefix' => 'edit-form-units',
        'id' => 'edit-form-units',
    ]
);
$formUnit .= $this->Form->control(
    'tab',
    [
        'type' => 'hidden',
        'value' => 'units',
    ]
);
$formUnit .= $this->Form->end();
$formUnit .= $this->Html->tag(
    'div#' . $jstreeId . '.col-xl-4.edit-form-jstree',
    $loading
)
    . $this->Html->tag(
        'div#' . $dataId . '.col-xl-8.form-div',
        $this->Form->create()
        . $this->Form->control(
            'action',
            ['type' => 'hidden', 'default' => 'cancel']
        )
        . $tuto
        . $this->Form->end()
    );
$formUnit .= $this->Html->tag('/div');

$getTreeUrl = $this->Url->build('/form-units/get-tree/' . $id);
$formUnitsAddUrl = $this->Url->build('/form-units/add/' . $id);
$formUnitsEditUrl = $this->Url->build('/form-units/edit');
$formUnitsMoveUrl = $this->Url->build('/form-units/move');
$formUnitsDeleteUrl = $this->Url->build('/form-units/delete');
$faEye = addcslashes(
    $this->Fa->i('fa-eye', __("Prévisualiser l'arborescence")),
    "'"
);
$formUnit .= <<<JS
<!--suppress JSDeprecatedSymbols -->
<script>
    $('.jstree[role=tree]:not(:visible)').empty();
    var toggleAddableInnerState = true;
    var tree = $('#$jstreeId');
    var modal = tree.closest('.modal');
    var footer = modal.find('.modal-footer');
    var zoneDescription = $('#$dataId');
    var naccept;
    
    function handleFormUnitEditSubmit(e)
    {
        let form = $(this);
        e.preventDefault();
        if (form.get(0).checkValidity()) {
            naccept.disable();
            $.ajax(
                {
                    url: form.attr('action'),
                    method: 'POST',
                    data: form.serialize(),
                    success: function (content, textStatus, jqXHR) {
                        var id = jqXHR.getResponseHeader('X-Inserted-Id');
                        // en cas de succès d'un ajout, on revien en édition (id null si edit)
                        if (id) {
                            tree.one('refresh.jstree', function () {
                                $('#jstree-node-'+id+'_anchor').click();
                            });
                        }
                        rebuildTreeOnAjaxSuccess(content, textStatus, jqXHR);
                        // Mise à jour de l'onglet form_calculators
                        $.ajax(
                            {
                                url:'{$this->Url->build('/FormCalculators/getData')}/' + $entity->id,
                                success: function (content, textStatus, jqXHR) {
                                    if (!content.form_calculators?.length) { // si aucun calculator
                                        return;
                                    }
                                    tableVariables.data = content.form_calculators;
                                    TableGenerator.appendActions(tableVariables.data, tableVariables.actions);
                                    tableVariables.generateAll();
                                },
                            }
                        );
                        // Mise à jour de l'onglet Zones de saisie
                        updateInputsTab();
                    },
                    error: function (e) {
                        endEditMode(e.responseText);
                    },
                    complete: function () {
                        naccept?.enable();
                    }
                }
            );
        } else {
            // ramène sur l'onglet en erreur
            $('div.ui-tabs-panel:has(.has-error, :invalid):not(.disabled)').each(
                function () {
                    var targetId = $(this).attr('id'),
                        tab = $('a.ui-tabs-anchor[href="#'+targetId+'"]').first(),
                        firstErrorId;
                    if (tab.is('.disabled')) {
                        return;
                    }
                    tab.click();
                }
            );
            form.get(0).reportValidity();
        }
    }

    function startEditMode(content = '')
    {
        $('li.ui-tabs-tab > a.ui-tabs-anchor')
            .disable()
            .on('click.editing-au', () => $('[href="#tab-edit-form-units"]').click());
        tree.disable();
        zoneDescription.html(content);
        zoneDescription.find('form').on('submit', handleFormUnitEditSubmit);
        replaceBtn(cancelBtn).on('click', cancelFormUnit);
        naccept = replaceBtn(acceptBtn, __("Enregistrer l'unité d'archives")).on('click', acceptFormUnit);
    }
    function endEditMode(content = '')
    {
        zoneDescription.html(content);
        tree.enable();
        $('li.ui-tabs-tab > a.ui-tabs-anchor').enable().off('.editing-au');
        restoreBtn(cancelBtn);
        restoreBtn(acceptBtn);
        naccept = null;
    }

    function cancelFormUnit()
    {
        endEditMode();
    }
    function acceptFormUnit()
    {
        var form = zoneDescription.find('form');
        form.submit();
    }

    // retire les autres jstree - évite les collisions d'ids des noeuds du jstree
    $('.edit-form-jstree').not(tree).empty();

    var ajaxData = {
        url: '$getTreeUrl/true',
        data: function (node) {
            return {
                id: node.id
            };
        }
    };

    $(document).off('.vakata.custom').off('.jstree.custom');

    function rebuildTreeOnAjaxSuccess(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            endEditMode();
            tree.jstree(true).deselect_all(true);
            tree.jstree(true).settings.core.data = content;
            tree.jstree(true).refresh();
        } else {
            zoneDescription.html(content);
        }
    }

    function actionAddFormUnit(data) {
        AsalaeLoading.ajax(
            {
                url: '$formUnitsAddUrl/'
                    +data.node.original.form_unit_id,
                method: 'GET',
                success: function (content) {
                    startEditMode(content);
                },
                error: function (e) {
                    zoneDescription.html(e.responseText);
                }
            }
        );
    }

    function actionEditFormUnit(data) {
        AsalaeLoading.ajax(
            {
                url: '$formUnitsEditUrl/'
                    +data.node.original.form_unit_id,
                method: 'GET',
                success: function (content) {
                    startEditMode(content);
                },
                error: function (e) {
                    zoneDescription.html(e.responseText);
                }
            }
        );
    }

    function prependToogleAddables() {
        tree.prepend(
            $('<div class="form-group checkbox">')
                .append(
                    $('<input type="checkbox">')
                        .attr('id', '$toggleAddablesId')
                        .prop('checked', toggleAddableInnerState)
                        .on('change', function () {
                            toggleAddableInnerState = !toggleAddableInnerState;
                            reloadTree();
                        })
                )
                .append(
                    $('<label></label>')
                        .attr('for', '$toggleAddablesId')
                        .text(__("Afficher les boutons \"ajouter\""))
                )
        );
    }

    function appendPreviewButton() {
        tree.append(
            $('<div class="">')
                .append(
                    $('<button type="button" class="mt-4 btn btn-primary">')
                        .on('click', function () {
                            previewFormTree($entity->id);
                        })
                        .append('$faEye')
                )
        );
    }

    tree.jstree({
        core: {
            data: ajaxData,
            check_callback: function (op, node, par, pos, more) {
                if (op === 'move_node') {
                    return this.settings.dnd.is_droppable(node);
                }
                return true;
            }
        },
        conditionalselect: function (node, event) {
            return !tree.hasClass('disabled');
        },
        search: {
            case_insensitive: true,
            show_only_matches: false,
            search_callback: function (search, node) {
                search = search.toLowerCase().unaccents();
                if (typeof node.original.searchtext === 'undefined') {
                    return node.original.text.toLowerCase().indexOf(search) >= 0;
                }
                return node.original.searchtext.toLowerCase().indexOf(search) >= 0;
            }
        },
        plugins: [
            "conditionalsel", // permet de désactiver la sélection lorsque le formulaire est apparent
            "search", // Recherche par nom de noeud
            "wholerow", // sélection sur la ligne (clic facilité et affichage + moderne)
            "dnd" // Drag & drop - Permet de déplacer les noeuds
        ],
        dnd: {
            copy: false,
            is_draggable: function (selected) {
                var type = selected[0].original.type;
                return ['tree_search', 'tree_path', 'navigation']
                    .indexOf(type) === -1;
            },
            is_droppable: function (node) {
                var hovered = $('.jstree-wholerow-hovered').closest('li').attr('id');
                hovered = tree.jstree(true).get_node(hovered).original;
                var original = node.original;
                
                // si on est lié a des champs répétables,
                //      on ne peux pas aller en dehors d'une ua répétable
                if (original.has_linked_repeatable_fieldsets && !hovered.repeatable_fieldset_id_parent) {
                    return false;
                }
                
                // si on est répétable ou a un enfant répétable, on ne peut être envoyé dans une ua répétable
                //  que si elle est basée sur la même section répétable
                if (
                    original.has_repeatable_child && hovered.repeatable_fieldset_id_parent
                    && original.repeatable_fieldset_id_parent !== hovered.repeatable_fieldset_id_parent
                ) {
                    return false;
                }
                
                // une répétable ne peut être déplacée sous une répétable
                if (original.type === 'repeatable' && hovered.repeatable_fieldset_id_parent) {
                    return false;
                }

                return ['tree_search', 'tree_path', 'tree_root', 'document', 'document_multiple', 'navigation']
                    .indexOf(hovered.type) === -1;
            }
        }
    })
        .off('ready.jstree set_state.jstree') // affiche les pointillés (supprimés dans cet event par wholerow)
        .on('select_node.jstree', function (event, data) {
            if ($(this).hasClass('disabled')) {
                event.stopPropagation();
                event.preventDefault();
                return;
            }
            if (data.node.original.action === 'add') {
                actionAddFormUnit(data);
            } else if (data.node.original.action === 'edit') {
                actionEditFormUnit(data);
            }
        })
        .on('loaded.jstree', function() {
            prependToogleAddables();
            // prependSearchbar('');
            var treeJstree = tree.jstree(true);
            treeJstree.move_node = function (obj, par, pos, callback, is_loaded) {
                var target = $('.jstree-wholerow-hovered').closest('li');
                moveNode(
                    obj[0].original,
                    tree.jstree(true).get_node(target.attr('id')).original
                );
                return true;
            };
            appendPreviewButton();
        })
        .on('refresh.jstree', function() {
            tree.trigger('loaded.jstree');
        });

    /**
     * Déplace un noeud selon data
     */
    function moveNode(move, target) {
        AsalaeLoading.ajax(
            {
                url: '$formUnitsMoveUrl/'+move.form_unit_id+'/'+target.form_unit_id,
                method: 'POST',
                data: {display_addables: toggleAddableInnerState},
                success: rebuildTreeOnAjaxSuccess
            }
        );
    }

    /**
     * reconstruit l'arbre
     */
    function reloadTree() {
        var toggleAdd = toggleAddableInnerState ? 'true' : 'false';
        AsalaeLoading.ajax(
            {
                url: '$getTreeUrl/'+toggleAdd,
                method: 'POST',
                success: rebuildTreeOnAjaxSuccess
            }
        );
    }

    function deleteFormUnit(id) {
        var msg = __(
            "Cette action supprimera cette unité d'archives et tout ce qu'elle contient."
            +" Tapez le mot 'delete' pour confirmer la suppression"
        );
        if (prompt(msg) === 'delete') {
            $.ajax(
                {
                    url: '$formUnitsDeleteUrl/'+id,
                    method: 'DELETE',
                    success: function (content, textStatus, jqXHR) {
                        rebuildTreeOnAjaxSuccess(content, textStatus, jqXHR);
                        updateInputsTab();
                    }
                }
            );
        }
    }

    function updateInputsTab() {
        $.ajax(
            {
                url:'{$this->Url->build('/Forms/edit')}/' + $entity->id,
                success: function (content, textStatus, jqXHR) {
                    var newContent = $(content).find('#tab-edit-form-fieldsets > form').html();
                    $('#tab-edit-form-fieldsets > form').html(newContent);
                    initEditForm();
                }
            }
        );
    }
</script>
JS;

return $formUnit;
