<?php

/**
 * @var Versae\View\AppView $this
 */

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Formulaires"));
$this->Breadcrumbs->add($h1 = __("Tous les formulaires"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-code', $h1))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

$jsTable = $this->Table->getJsTableObject($tableId);

$buttons = [];
if ($this->Acl->check('/forms/add')) {
    $buttons[] = $this->ModalForm
        ->create('add-form-modal', ['size' => 'large'])
        ->modal(__("Ajouter un formulaire"))
        ->output(
            'button',
            $this->Fa->charte('Ajouter', __("Ajouter un formulaire")),
            '/forms/add'
        )
        ->step('/forms/add', 'actionAdd1Form')
        ->javascriptCallback('afterAddForm(' . $jsTable . ', "Forms")')
        ->generate(
            [
                'class' => 'btn btn-success float-none',
                'type' => 'button',
            ] + ($archivingSystems->count() ? [] : ['disabled' => true])
        );
}
if ($this->Acl->check('/forms/import')) {
    $buttons[] = $this->ModalForm
        ->create('import-form-modal', ['size' => 'large'])
        ->modal(__("Importer un formulaire"))
        ->output(
            'button',
            $this->Fa->charte('Importer', __("Importer un formulaire")),
            '/forms/import'
        )
        ->step('/forms/import', 'actionImportForm')
        ->javascriptCallback('afterAddForm(' . $jsTable . ', "Forms")')
        ->generate(
            [
                'class' => 'btn btn-success float-none',
                'type' => 'button',
            ] + ($archivingSystems->count() ? [] : ['disabled' => true])
        );
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}

$alert = '';
if ($archivingSystems->count() === 0) {
    $alert .= $this->Html->tag('li', __("Système d'Archivage Electronique"));
    $addable = false;
}
if ($alert) {
    echo $this->Html->tag('section.container');
    echo $this->Html->tag('div.alert.alert-warning');
    echo $this->Html->tag('h4', __("Paramétrage manquant"));
    echo $this->Html->tag('ul', $alert);
    echo $this->Html->tag('/div');
    echo $this->Html->tag('/section');
}

require 'index-common.php';
?>
<script>
    function afterAddForm(table, model)
    {
        return function (content, textStatus, jqXHR) {
            var step = jqXHR.getResponseHeader('X-Asalae-Step');
            if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true' && step) {
                if (step && step.match(/^[\w\-.'" (),\[\]]+$/)) {
                    eval(step);
                }
                table.data.unshift({[model]: content});
                TableGenerator.appendActions(table.data, table.actions);
                table.generateAll();
            } else {
                AsalaeGlobal.colorTabsInError();
            }
        }
    }
</script>
