<?php

/**
 * @var Versae\View\AppView $this
 */

use Versae\Model\Table\FormInputsTable;

echo $this->MultiStepForm->template('MultiStepForm/add_form_input')->step(1) . '<hr>';

echo $this->Form->create($form, ['idPrefix' => 'add-input1']);
echo $this->Form->control(
    'is_submit',
    ['type' => 'hidden', 'value' => '1']
);

echo $this->Html->tag('table#table-copy-input.table');
echo $this->Html->tag('tbody');
echo $this->Html->tag('tr');
echo $this->Html->tag('td');
echo $this->Html->tag('label.control-label.field-type');
echo $this->Html->tag(
    'input',
    '',
    [
        'id' => 'add-input1-type-copy',
        'type' => 'radio',
        'name' => 'type',
        'value' => 'copy',
    ]
);
echo __("Copier un champ existant");
echo $this->Html->tag('/label');
echo $this->Html->tag('/td');
// exemple
echo $this->Html->tag('td.exemple', null, ['width' => '65%']);
echo $this->Form->control(
    'copy',
    [
        'label' => __("Choix"),
        'empty' => __("-- Choisir un champ à copier --"),
        'options' => $inputs,
    ]
);
echo $this->Html->tag('/td');
echo $this->Html->tag('/tr');
echo $this->Html->tag('/tbody');
echo $this->Html->tag('/table');

echo $this->Html->tag('table#table-add-input.table');
echo $this->Html->tag('thead');
echo $this->Html->tag('tr');
echo $this->Html->tag('th', __("Type de champ de formulaire"));
echo $this->Html->tag('th', __("Exemple"), ['width' => '65%']);
echo $this->Html->tag('/tr');
echo $this->Html->tag('/thead');
echo $this->Html->tag('tbody');
/**
 * type text
 */
echo $this->Html->tag('tr');
echo $this->Html->tag('td');
echo $this->Html->tag('label.control-label.field-type');
echo $this->Html->tag(
    'input',
    '',
    [
        'type' => 'radio',
        'name' => 'type',
        'value' => FormInputsTable::TYPE_TEXT,
        'checked',
    ]
);
echo __("Texte");
echo $this->Html->tag('/label');
echo $this->Html->tag('/td');
// exemple
echo $this->Html->tag('td.exemple');
echo $this->Form->control(
    'example[text]',
    [
        'label' => __("Nom du champ"),
        'aria-disabled' => 'true',
        'placeholder' => 'exemple',
        'help' => __("texte d'aide"),
        'class' => 'example',
        'tabindex' => '-1',
    ]
);
echo $this->Html->tag('/td');
echo $this->Html->tag('/tr');

/**
 * type number
 */
echo $this->Html->tag('tr');
echo $this->Html->tag('td');
echo $this->Html->tag('label.control-label.field-type');
echo $this->Html->tag(
    'input',
    '',
    [
        'type' => 'radio',
        'name' => 'type',
        'value' => FormInputsTable::TYPE_NUMBER,
    ]
);
echo __("Nombre");
echo $this->Html->tag('/label');
echo $this->Html->tag('/td');
// exemple
echo $this->Html->tag('td.exemple');
echo $this->Form->control(
    'example[number]',
    [
        'label' => __("Nom du champ"),
        'aria-disabled' => 'true',
        'help' => __("texte d'aide"),
        'class' => 'example',
        'placeholder' => '255',
        'type' => 'number',
        'tabindex' => '-1',
    ]
);
echo $this->Html->tag('/td');
echo $this->Html->tag('/tr');

/**
 * type email
 */
echo $this->Html->tag('tr');
echo $this->Html->tag('td');
echo $this->Html->tag('label.control-label.field-type');
echo $this->Html->tag(
    'input',
    '',
    [
        'type' => 'radio',
        'name' => 'type',
        'value' => FormInputsTable::TYPE_EMAIL,
    ]
);
echo __("Email");
echo $this->Html->tag('/label');
echo $this->Html->tag('/td');
// exemple
echo $this->Html->tag('td.exemple');
echo $this->Form->control(
    'example[email]',
    [
        'label' => __("Nom du champ"),
        'aria-disabled' => 'true',
        'help' => __("texte d'aide"),
        'class' => 'example',
        'placeholder' => 'example@test.fr',
        'type' => 'email',
        'append' => $this->Fa->i('fa-envelope-o'),
        'tabindex' => '-1',
    ]
);
echo $this->Html->tag('/td');
echo $this->Html->tag('/tr');

/**
 * type textarea
 */
echo $this->Html->tag('tr');
echo $this->Html->tag('td');
echo $this->Html->tag('label.control-label.field-type');
echo $this->Html->tag(
    'input',
    '',
    [
        'type' => 'radio',
        'name' => 'type',
        'value' => FormInputsTable::TYPE_TEXTAREA,
    ]
);
echo __("Bloc de texte");
echo $this->Html->tag('/label');
echo $this->Html->tag('/td');
// exemple
echo $this->Html->tag('td.exemple');
echo $this->Form->control(
    'example[textarea]',
    [
        'label' => __("Nom du champ"),
        'aria-disabled' => 'true',
        'type' => 'textarea',
        'placeholder' => 'exemple',
        'help' => __("texte d'aide"),
        'class' => 'example',
        'tabindex' => '-1',
    ]
);
echo $this->Html->tag('/td');
echo $this->Html->tag('/tr');

/**
 * type select
 */
echo $this->Html->tag('tr');
echo $this->Html->tag('td');
echo $this->Html->tag('label.control-label.field-type');
echo $this->Html->tag(
    'input',
    '',
    [
        'type' => 'radio',
        'name' => 'type',
        'value' => FormInputsTable::TYPE_SELECT,
    ]
);
echo __("Liste déroulante");
echo $this->Html->tag('/label');
echo $this->Html->tag('/td');
// exemple
echo $this->Html->tag('td.exemple');
echo $this->Form->control(
    'example[select]',
    [
        'label' => __("Nom du champ"),
        'options' => [
            'null' => __("-- Exemple --"),
        ],
        'aria-disabled' => 'true',
        'help' => __("texte d'aide (Valeur multiple possible)"),
        'class' => 'example',
        'tabindex' => '-1',
    ]
);
echo $this->Html->tag('/td');
echo $this->Html->tag('/tr');

/**
 * type date
 */
echo $this->Html->tag('tr');
echo $this->Html->tag('td');
echo $this->Html->tag('label.control-label.field-type');
echo $this->Html->tag(
    'input',
    '',
    [
        'type' => 'radio',
        'name' => 'type',
        'value' => FormInputsTable::TYPE_DATE,
    ]
);
echo __("Date");
echo $this->Html->tag('/label');
echo $this->Html->tag('/td');
// exemple
echo $this->Html->tag('td.exemple');
echo $this->Form->control(
    'example[date]',
    [
        'label' => __("Nom du champ"),
        'append' => $this->Fa->i('fa-calendar'),
        'aria-disabled' => 'true',
        'help' => __("texte d'aide"),
        'class' => 'example',
        'placeholder' => 'jj/mm/yyyy',
        'tabindex' => '-1',
    ]
);
echo $this->Html->tag('/td');
echo $this->Html->tag('/tr');

/**
 * type datetime
 */
echo $this->Html->tag('tr');
echo $this->Html->tag('td');
echo $this->Html->tag('label.control-label.field-type');
echo $this->Html->tag(
    'input',
    '',
    [
        'type' => 'radio',
        'name' => 'type',
        'value' => FormInputsTable::TYPE_DATETIME,
    ]
);
echo __("Date et heure");
echo $this->Html->tag('/label');
echo $this->Html->tag('/td');
// exemple
echo $this->Html->tag('td.exemple');
echo $this->Form->control(
    'example[datetime]',
    [
        'label' => __("Nom du champ"),
        'append' => $this->Fa->i('fa-calendar'),
        'aria-disabled' => 'true',
        'help' => __("texte d'aide"),
        'class' => 'example',
        'placeholder' => 'jj/mm/yyyy hh:mm',
        'tabindex' => '-1',
    ]
);
echo $this->Html->tag('/td');
echo $this->Html->tag('/tr');

/**
 * type checkbox
 */
echo $this->Html->tag('tr');
echo $this->Html->tag('td');
echo $this->Html->tag('label.control-label.field-type');
echo $this->Html->tag(
    'input',
    '',
    [
        'type' => 'radio',
        'name' => 'type',
        'value' => FormInputsTable::TYPE_CHECKBOX,
    ]
);
echo __("Case à cocher");
echo $this->Html->tag('/label');
echo $this->Html->tag('/td');
// exemple
echo $this->Html->tag('td.exemple');
echo $this->Form->control(
    'example[checkbox]',
    [
        'label' => __("Nom du champ"),
        'aria-disabled' => 'true',
        'help' => __("texte d'aide"),
        'class' => 'example',
        'type' => 'checkbox',
        'tabindex' => '-1',
        'templates' => [
            'checkboxContainer' => '<div class="form-group checkbox {{required}}">{{content}}<br>{{append}}</div>',
        ],
    ]
);
echo $this->Html->tag('/td');
echo $this->Html->tag('/tr');

/**
 * type multi_checkbox
 */
echo $this->Html->tag('tr');
echo $this->Html->tag('td');
echo $this->Html->tag('label.control-label.field-type');
echo $this->Html->tag(
    'input',
    '',
    [
        'type' => 'radio',
        'name' => 'type',
        'value' => FormInputsTable::TYPE_MULTI_CHECKBOX,
    ]
);
echo __("Cases à cocher multiples");
echo $this->Html->tag('/label');
echo $this->Html->tag('/td');
// exemple
echo $this->Html->tag('td.exemple');
echo $this->Form->control(
    'example[multi_checkbox]',
    [
        'label' => __("Nom du champ"),
        'aria-disabled' => 'true',
        'help' => __("texte d'aide"),
        'class' => 'example',
        'multiple' => 'checkbox',
        'options' => [
            'val1' => __("option 1"),
            'val2' => __("option 2"),
            'val3' => __("option 3"),
        ],
        'tabindex' => '-1',
    ]
);
echo $this->Html->tag('/td');
echo $this->Html->tag('/tr');

/**
 * type radio
 */
echo $this->Html->tag('tr');
echo $this->Html->tag('td');
echo $this->Html->tag('label.control-label.field-type');
echo $this->Html->tag(
    'input',
    '',
    [
        'type' => 'radio',
        'name' => 'type',
        'value' => FormInputsTable::TYPE_RADIO,
    ]
);
echo __("Bouton radio");
echo $this->Html->tag('/label');
echo $this->Html->tag('/td');
// exemple
echo $this->Html->tag('td.exemple');
echo $this->Form->control(
    'example[radio]',
    [
        'label' => __("Nom du champ"),
        'aria-disabled' => 'true',
        'help' => __("texte d'aide"),
        'class' => 'example',
        'type' => 'radio',
        'options' => [
            'val1' => 'val1',
            'val2' => 'val2',
            'val3' => 'val3',
        ],
        'tabindex' => '-1',
    ]
);
echo $this->Html->tag('/td');
echo $this->Html->tag('/tr');

/**
 * type file
 */
echo $this->Html->tag('tr');
echo $this->Html->tag('td');
echo $this->Html->tag('label.control-label.field-type');
echo $this->Html->tag(
    'input',
    '',
    [
        'type' => 'radio',
        'name' => 'type',
        'value' => FormInputsTable::TYPE_FILE,
    ]
);
echo __("Fichier");
echo $this->Html->tag('/label');
echo $this->Html->tag('/td');
// exemple
echo $this->Html->tag('td.exemple.fake-input');
echo $this->Html->tag(
    'div',
    $this->Fa->i('fa-upload fa-3x text-primary')
    . $this->Html->tag('p', __("Glissez-déposez vos fichiers ici"))
    . $this->Html->tag(
        'div',
        $this->Fa->i('fa-folder-open fa-space')
        . __("Parcourir...")
        . $this->Html->tag('label.sr-only', __("Fichier")),
        ['class' => 'btn btn-success']
    ),
    ['class' => 'dropbox']
);
echo $this->Html->tag('p.help-block', __("texte d'aide"));
echo $this->Html->tag('/td');
echo $this->Html->tag('/tr');

/**
 * type archive
 */
echo $this->Html->tag('tr');
echo $this->Html->tag('td');
echo $this->Html->tag('label.control-label.field-type');
echo $this->Html->tag(
    'input',
    '',
    [
        'type' => 'radio',
        'name' => 'type',
        'value' => FormInputsTable::TYPE_ARCHIVE_FILE,
    ]
);
echo __("Dossier zippé");
echo $this->Html->tag('/label');
echo $this->Html->tag('/td');
// exemple
echo $this->Html->tag('td.exemple.fake-input');
echo $this->Html->tag(
    'div',
    $this->Fa->i('fa-file-archive fa-3x text-warning')
    . $this->Html->tag('p', __("Glissez-déposez votre fichier ici"))
    . $this->Html->tag(
        'div',
        $this->Fa->i('fa-folder-open fa-space')
        . __("Parcourir...")
        . $this->Html->tag('label.sr-only', __("Fichier")),
        ['class' => 'btn btn-success']
    ),
    ['class' => 'dropbox']
);
echo $this->Html->tag('p.help-block', __("texte d'aide"));
echo $this->Html->tag('/td');
echo $this->Html->tag('/tr');

/**
 * type paragraph
 */
echo $this->Html->tag('tr');
echo $this->Html->tag('td');
echo $this->Html->tag('label.control-label.field-type');
echo $this->Html->tag(
    'input',
    '',
    [
        'type' => 'radio',
        'name' => 'type',
        'value' => FormInputsTable::TYPE_PARAGRAPH,
    ]
);
echo __("Paragraphe de texte (lecture seule)");
echo $this->Html->tag('/label');
echo $this->Html->tag('/td');
// exemple
echo $this->Html->tag('td.exemple');
echo $this->Html->tag(
    'p',
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. '
    . 'Fusce a tristique ex. Vivamus scelerisque urna nec convallis porta. '
    . 'Curabitur id ligula egestas, cursus nisi nec, rhoncus enim. '
    . 'Curabitur leo ante, vehicula non purus sit amet, vulputate ultrices tortor. '
    . 'Praesent quis arcu ut lectus facilisis volutpat eu et urna. '
    . 'Praesent condimentum imperdiet erat, vel malesuada ipsum efficitur sit amet. '
    . 'Donec sit amet nisi dignissim, vestibulum mauris tristique, bibendum tortor. '
    . 'Nullam in odio a arcu vehicula sollicitudin id id lacus. '
    . 'Nunc sollicitudin tortor vel tellus finibus, in varius urna cursus. '
    . 'Nam a mi orci. Quisque efficitur arcu venenatis fermentum porta.'
);
echo $this->Html->tag('/td');
echo $this->Html->tag('/tr');

/**
 *
 */
echo $this->Html->tag('/tbody');
echo $this->Html->tag('/table');

echo $this->Form->end();
?>
<script>
    var addInputCopy = $('#add-input1-copy');
    var addInputCopyType = $('#add-input1-type-copy');

    $('#table-add-input').find('tr').on(
        'mousedown click',
        function(e) {
            e.preventDefault();
            $(this).find('input[type="radio"]').first().prop('checked', true);
            addInputCopy.prop('required', false);
            return false;
        }
    ).find('input[type="radio"]').on(
        'keypress',
        function (e) {
            if (e.originalEvent.keyCode === 32) { // espace
                $(this).click();
            }
        }
    );

    addInputCopy.change(() => addInputCopyType.click());
    addInputCopyType.change(() => addInputCopy.prop('required', true));
</script>
