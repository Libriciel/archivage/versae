<?php

/**
 * @var Versae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */

echo $this->Form->create(
    $entity,
    [
        'idPrefix' => 'version-form-form',
        'id' => 'version-form-form',
    ]
);
echo $this->Form->control(
    'version_note',
    [
        'label' => __("Note de version"),
    ]
);
echo $this->Form->end();
