<?php

/**
 * @var Versae\View\AppView $this
 */

$jstreeId = 'jstree-preview-archive-units';

$loading = $this->Html->tag(
    'div',
    $this->Html->tag(
        'i',
        '',
        ['class' => 'fa fa-4x fa-spinner faa-spin animated']
    ),
    ['class' => 'text-center loading-container']
);

echo $this->Html->tag('div#' . $jstreeId, $loading);
?>
<script>
    var previewTree = $('#<?=$jstreeId?>');
    previewTree.jstree(
        {
            core: {
                data: <?=json_encode($jsData)?>,
            }
        }
    );
    previewTree.on('loaded.jstree', function() {
        // au survol, affiche le text entier d'un noeud
        previewTree.find('span.child-value.trimmed:not(.mouseover)')
            .off('.jstree')
            .on('mouseover.jstree', function() {
                var trimmed = $(this).text();
                var name = $(this).attr('title');
                $(this).addClass('mouseover')
                    .attr('data-text', trimmed)
                    .text(name);
            })
            .on('mouseout.jstree', function() {
                var trimmed = $(this).attr('data-text');
                $(this).removeClass('mouseover')
                    .attr('data-text', '')
                    .text(trimmed);
            });
    });
</script>
