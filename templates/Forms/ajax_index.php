<?php

/**
 * @var Versae\View\AppView $this
 */

use Versae\Model\Table\FormsTable;

if (empty($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size test-form-refresh-table'])
    ->fields(
        [
            'Forms.name' => [
                'label' => __("Nom"),
                'order' => 'name',
                'filter' => [
                    'name[0]' => [
                        'id' => 'filter-name',
                        'label' => false,
                        'aria-label' => __("Nom"),
                    ],
                ],
            ],
            'Forms.description' => [
                'label' => __("Description"),
                'display' => false,
            ],
            'Forms.archiving_system.name' => [
                'label' => __("SAE"),
                'filter' => [
                    'archiving_system_id[0]' => [
                        'id' => 'filter-archiving_system',
                        'label' => false,
                        'aria-label' => __("SAE"),
                        'options' => $archivingSystemsOptions
                    ],
                ],
            ],
            'Forms.statetrad' => [
                'label' => __("Etat"),
                'filter' => [
                    'state[0]' => [
                        'id' => 'filter-state',
                        'label' => false,
                        'aria-label' => __("Etat"),
                        'options' => $states
                    ],
                ],
            ],
            'Forms.version_number' => [
                'label' => __("Version"),
                'filter' => [
                    'version_number[0][value]' => [
                        'label' => __("Nombre de fichiers"),
                        'id' => 'filter-version_number-min',
                        'prepend' => $this->Input->operator(
                            'version_number[0][operator]',
                            '>=',
                            ['id' => 'filter-version_number-operator']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                    'version_number[1][value]' => [
                        'label' => false,
                        'aria-label' => __("Nombre 2"),
                        'id' => 'filter-version_number-max',
                        'prepend' => $this->Input->operator(
                            'version_number[1][operator]',
                            '<=',
                            ['id' => 'filter-version_number-max-operator']
                        ),
                        'class' => 'with-select',
                        'type' => 'number',
                        'min' => 0,
                    ],
                ],
            ],
            'Forms.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'order' => 'created',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'created[0]' => [
                        'id' => 'filter-created-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator('dateoperator_created[0]', '>='),
                        'append' => $this->Date->picker('#filter-created-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'created[1]' => [
                        'id' => 'filter-created-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_created[1]', '<='),
                        'append' => $this->Date->picker('#filter-created-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
            'Forms.seda_version' => [
                'label' => __("Version du SEDA"),
                'order' => 'seda_version',
                'display' => false,
            ]
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'Forms.id',
            'favorites' => true,
            'sortable' => true,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "viewForm({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => $title = __("Visualiser {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/forms/view'),
                'params' => ['Forms.id', 'Forms.name'],
            ],
            [
                'onclick' => "actionEditForm({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/forms/edit'),
                'displayEval' => 'data[{index}].Forms.editable',
                'params' => ['Forms.id', 'Forms.name'],
            ],
            [
                'onclick' => "publishForm({0}, {2})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-globe text-success'),
                'title' => $title = __("Publier {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/forms/publish'),
                'displayEval' => 'data[{index}].Forms.publishable',
                'params' => ['Forms.id', 'Forms.name', 'Forms.tested'],
            ],
            [
                'onclick' => "null",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-globe gray disabled'),
                'title' => $title = __("Non publiable : {0}", '{3}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/forms/publish'),
                'displayEval' =>
                    'data[{index}].Forms.state === "' . FormsTable::S_EDITING . '" && !data[{index}].Forms.publishable',
                'params' => ['Forms.id', 'Forms.name', 'Forms.tested', 'Forms.not_publishable_reason'],
            ],
            [
                'onclick' => "unpublishForm({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-globe text-warning'),
                'title' => $title = __("Dépublier {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/forms/unpublish'),
                'displayEval' => 'data[{index}].Forms.unpublishable',
                'params' => ['Forms.id', 'Forms.name'],
            ],
            [
                'onclick' => "actionTestForm({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-calendar-check-o'),
                'title' => $title = __("Tester {0}", '{1}'),
                'aria-label' => $title,
                'data-action' => __("Tester"),
                'display' => $this->Acl->check('/forms/test'),
                'displayEval' => 'data[{index}].Forms.state !== "' . FormsTable::S_EDITING . '"'
                    . ' || data[{index}].Forms.publishable',
                'params' => ['Forms.id', 'Forms.name'],
            ],
            [
                'onclick' => "deactivateForm({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Désactiver'),
                'title' => $title = __("Désactiver {0}", '{1}'),
                'aria-label' => $title,
                'data-action' => __("Désactiver"),
                'display' => $this->Acl->check('/forms/deactivate'),
                'displayEval' => 'data[{index}].Forms.state === "' . FormsTable::S_PUBLISHED . '"',
                'params' => ['Forms.id', 'Forms.name'],
            ],
            [
                'onclick' => "activateForm({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Activer'),
                'title' => $title = __("Activer {0}", '{1}'),
                'aria-label' => $title,
                'data-action' => __("Activer"),
                'display' => $this->Acl->check('/forms/activate'),
                'displayEval' => 'data[{index}].Forms.state === "' . FormsTable::S_DEACTIVATED . '"',
                'params' => ['Forms.id', 'Forms.name'],
            ],
            [
                'onclick' => "actionVersionForm({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-code-branch'),
                'title' => $title = __("Versionner {0}", '{1}'),
                'aria-label' => $title,
                'data-action' => __("Versionner"),
                'display' => $this->Acl->check('/forms/version'),
                'displayEval' => 'data[{index}].Forms.versionable',
                'params' => ['Forms.id', 'Forms.name'],
            ],
            [
                'data-callback' => "duplicateForm({0})",
                'confirm' => __("Êtes-vous sûr de vouloir dupliquer ce formulaire ?"),
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Dupliquer'),
                'title' => $title = __("Dupliquer {0}", '{1}'),
                'aria-label' => $title,
                'data-action' => __("Dupliquer"),
                'display' => $this->Acl->check('/forms/duplicate'),
                'params' => ['Forms.id', 'Forms.name'],
            ],
            [
                'href' => $this->Url->build('/forms/export') . '/{0}/{3}',
                'download' => '{2}',
                'target' => '_blank',
                'type' => 'button',
                'class' => 'btn-link',
                'data-action' => __("Exporter"),
                'label' => $this->Fa->charte('Télécharger'),
                'title' => $title = __("Exporter {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/Forms/export'),
                'params' => ['Forms.id', 'Forms.name', 'Forms.export_filename', 'Forms.version_number']
            ],
            function ($table) {
                /** @var Versae\View\AppView $this */
                $deleteUrl = $this->Url->build('/Forms/delete');
                return [
                    'onclick' => "deleteForm($table->tableObject, '$deleteUrl')({0})",
                    'type' => 'button',
                    'class' => 'btn-link',
                    'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'data-action' => __("Supprimer"),
                    'display' => $this->Acl->check('forms/delete'),
                    'displayEval' => 'data[{index}].Forms.deletable',
                    'params' => ['Forms.id', 'Forms.name']
                ];
            },
        ],
    );

echo $this->element(
    'section_table',
    [
        'id' => $tableId . '-section',
        'title' => __("Liste des formulaires"),
        'table' => $table,
    ]
);
