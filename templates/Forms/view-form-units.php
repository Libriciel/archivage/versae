<?php

/**
 * @var Versae\View\AppView $this
 */

$jstreeId = 'jstree-view-archive-units';
$dataId = 'data-view-archive-units';
$searchbarId = 'searchbar-view-archive-units';
$toggleAddablesId = 'toggle-addables-view-archive-units';

$loading = $this->Html->tag(
    'div',
    $this->Html->tag(
        'i',
        '',
        ['class' => 'fa fa-4x fa-spinner faa-spin animated']
    ),
    ['class' => 'text-center loading-container']
);

/**
 * Aide: Recherche
 */
$tuto = $this->Html->tag(
    'div.alert.alert-info.alert-soft',
    null,
    ['style' => 'position: relative']
);
$tuto .= $this->Html->tag(
    'h4',
    $this->Fa->i('fa-info-circle', __("Barre de recherche"))
);
$tuto .= $this->Html->tag(
    'button.close',
    $this->Html->tag('span', '×'),
    [
        'type' => 'button',
        'aria-label' => __("Fermer cette alerte"),
        'onclick' => '$(this).parent().parent().find(".alert-soft").slideUp()',
        'style' => 'position: absolute; top: 8px; right: 14px;'
    ]
);
$tuto .= $this->Html->tag('ul');
$tuto .= $this->Html->tag(
    'li',
    $this->Fa->i(
        'fa-i-cursor',
        __("Le terme saisi est recherché dans l'arbre courant")
    )
);
$tuto .= $this->Html->tag('/ul');
$tuto .= $this->Html->tag('/div');

/**
 * Aide: Navigation
 */
$tuto .= $this->Html->tag(
    'div.alert.alert-info.alert-soft',
    null,
    ['style' => 'position: relative']
);
$tuto .= $this->Html->tag(
    'h4',
    $this->Fa->i('fa-info-circle', __("Navigation"))
);
$tuto .= $this->Html->tag(
    'button.close',
    $this->Html->tag('span', '×'),
    [
        'type' => 'button',
        'aria-label' => __("Fermer cette alerte"),
        'onclick' => '$(this).parent().parent().find(".alert-soft").slideUp()',
        'style' => 'position: absolute; top: 8px; right: 14px;'
    ]
);
$tuto .= $this->Html->tag('ul');
$tuto .= $this->Html->tag(
    'li',
    $this->Html->tag(
        'i',
        '',
        [
            'style' => 'background-image: url(/css/jstree/32px.png); background-position: -132px -4px;'
                . ' background-repeat: no-repeat; background-color: transparent; '
                . 'height: 17px; width: 30px; display: inline-block'
        ]
    )
    . __("Élément contenant d'autres éléments (ouverts)")
);
$tuto .= $this->Html->tag(
    'li',
    $this->Html->tag(
        'i',
        '',
        [
            'style' => 'background-image: url(/css/jstree/32px.png); background-position: -100px -4px;'
                . ' background-repeat: no-repeat; background-color: transparent; height: 17px;'
                . ' width: 30px; display: inline-block'
        ]
    )
    . __("Élément contenant d'autres éléments (fermés) - Cliquer pour ouvrir")
);
$tuto .= $this->Html->tag(
    'li',
    $this->Fa->i('fa-archive', __("Unité d'archives"))
);
$tuto .= $this->Html->tag(
    'li',
    $this->Fa->i('fa-paperclip', __("Document"))
);
$tuto .= $this->Html->tag(
    'li',
    $this->Fa->i('fa-files-o', __("Lot de documents"))
);
$tuto .= $this->Html->tag(
    'li',
    $this->Fa->i('fa-code-fork', __("Racine d'une arborescence obtenu d'un fichier compressé (ZIP)"))
);
$tuto .= $this->Html->tag(
    'li',
    $this->Fa->i('fa-search', __("Modification des noeuds de l'arborescence correspondant à la recherche"))
);
$tuto .= $this->Html->tag(
    'li',
    $this->Fa->i('fa-code', __("Modification du noeud de l'arborescence correspondant au chemin indiqué"))
);
$tuto .= $this->Html->tag('/ul');
$tuto .= $this->Html->tag('/div');

$formUnit = $this->Html->tag('div.container-flex.row');
$formUnit .= $this->Html->tag(
    'div#' . $jstreeId . '.col-xl-4.view-form-jstree',
    $loading
)
    . $this->Html->tag(
        'div#' . $dataId . '.col-xl-8.form-div',
        $this->Form->create()
        . $this->Form->control(
            'action',
            ['type' => 'hidden', 'default' => 'cancel']
        )
        . $tuto
        . $this->Form->end()
    );
$formUnit .= $this->Html->tag('/div');

$getTreeUrl = $this->Url->build('/form-units/get-tree/' . $id);
$formUnitsViewUrl = $this->Url->build('/form-units/view');
/** @noinspection JSJQueryEfficiency */
$formUnit .= <<<EOT
<!--suppress JSDeprecatedSymbols -->
<script>
    var tree = $('#$jstreeId');
    var zoneDescription = $('#$dataId');

    // retire les autres jstree - évite les collisions d'ids des noeuds du jstree
    $('.view-form-jstree').not(tree).empty();

    var ajaxData = {
        url: '$getTreeUrl/false',
        data: function (node) {
            return {
                id: node.id
            };
        }
    };

    $(document).off('.vakata.custom').off('.jstree.custom');

    function actionViewFormUnit(data) {
        AsalaeLoading.ajax(
            {
                url: '$formUnitsViewUrl/'
                    +data.node.original.form_unit_id,
                method: 'GET',
                success: function (content) {
                    zoneDescription.html(content);
                },
                error: function (e) {
                    zoneDescription.html(e.responseText);
                }
            }
        );
    }

    tree.jstree({
        core: {
            data: ajaxData,
        },
        conditionalselect: function (node, event) {
            return !tree.hasClass('disabled');
        },
        search: {
            case_insensitive: true,
            show_only_matches: false,
            search_callback: function (search, node) {
                search = search.toLowerCase().unaccents();
                if (typeof node.original.searchtext === 'undefined') {
                    return node.original.text.toLowerCase().indexOf(search) >= 0;
                }
                return node.original.searchtext.toLowerCase().indexOf(search) >= 0;
            }
        },
        plugins: [
            "conditionalsel", // permet de désactiver la sélection lorsque le formulaire est apparent
            "search", // Recherche par nom de noeud
            "wholerow", // sélection sur la ligne (clic facilité et affichage + moderne)
        ],
    })
        .off('ready.jstree set_state.jstree') // affiche les pointillés (supprimés dans cet event par wholerow)
        .on('select_node.jstree', function (event, data) {
            if ($(this).hasClass('disabled')) {
                event.stopPropagation();
                event.preventDefault();
                return;
            }
            actionViewFormUnit(data);
        })
        .on('loaded.jstree', function() {
            // prependSearchbar('');
            var treeJstree = tree.jstree(true);
            treeJstree.move_node = function (obj, par, pos, callback, is_loaded) {
                var target = $('.jstree-wholerow-hovered').closest('li');
                moveNode(
                    obj[0].original,
                    tree.jstree(true).get_node(target.attr('id')).original
                );
                return true;
            };
        })
        .on('refresh.jstree', function() {
            tree.trigger('loaded.jstree');
        });

    function prependSearchbar(value) { // FIXME désactivé - on le met ou pas ?
        tree.jstree("search", value);
        tree.prepend(
            $('<form class="form-group text"></form>').append(
                $('<label></label>')
                    .attr('for', '$searchbarId')
                    .text(__("Rechercher"))
            ).append(
                $('<div class="input-group"></div>').append(
                    $('<input class="form-control">')
                        .attr('id', '$searchbarId')
                        .val(value)
                        .on('keyup', function () {
                            tree.jstree("search", $(this).val());
                        })
                ).append(
                    $('<span class="input-group-addon btn btn-primary" role="button" tabindex="0"></span>').append(
                        $('<i class="fa fa-search" aria-hidden="true"></i>')
                    ).append($('<span class="sr-only"></span>').text(__("Rechercher")))
                        .attr('title', __("Rechercher"))
                        .on('click', doSearch)
                )
            ).on('submit', function(event) {
                event.preventDefault();
                $(this).find('.btn[role="button"]').click();
            })
        );
    }

    function doSearch()
    {
        // TODO que faire lors de la recherche ?
        console.log('research '+$(this).parent().find('input').val());
    }
</script>
EOT;

return $formUnit;
