<?php

/**
 * @var Versae\View\AppView $this
 */

$formId = 'form-test-form-' . uuid_create(UUID_TYPE_TIME);
echo $this->Form->create($form, ['id' => $formId, 'idPrefix' => 'form-test']);

require 'generate-form.php';

echo $this->Form->end();
