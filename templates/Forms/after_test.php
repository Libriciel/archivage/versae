<?php

/**
 * @var Versae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */

use Cake\I18n\FrozenDate;
use Cake\ORM\Entity;

$loading = $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);

$infos = $this->Html->tag('article');
$infos .= $this->Html->tag('header.bottom-space');
$infos .= $this->Html->tag('h3', __("Transfert généré"));
$infos .= $this->Html->tag('/header');

$infos .= $this->Html->tag('h4', __("Résumé"));
$infos .= $this->ViewTable->generate(
    $data,
    [
        __("Identifiant du transfert") => 'identifier',
        __("Date indiqué dans le transfert") => fn($v) => (new FrozenDate($v->get('date')))->nice(),
        __("Nombre d'unités d'archives") => 'au_count',
        __("Nombre de fichiers") => 'file_count',
        __("Taille du bordereau") => 'size|toReadableSize'
    ]
);
$infos .= $this->Html->link(
    $this->Fa->charte('Visualiser', __("Visualiser le bordereau")),
    $this->Url->build('/forms/display-xml-test/' . $entity->id),
    ['target' => '_blank', 'escape' => false, 'class' => 'btn btn-default']
);
$infos .= $this->Html->link(
    $this->Fa->charte('Télécharger', __("Télécharger le bordereau")),
    $this->Url->build('/download/file/' . $entity->id),
    ['target' => '_blank', 'download' => $entity->get('name'), 'escape' => false, 'class' => 'btn btn-primary']
);
$infos .= $this->Html->tag('/article');

$tabs = $this->Tabs->create('tabs-test-result', ['class' => 'row no-padding']);
$tabs->add(
    'tab-test-result-infos',
    $this->Fa->i('fa-file-code-o', __("Informations principales")),
    $infos
);

$twigFormated = array_map(fn($v) => is_array($v) ? __("vide") : h((string)$v), $twigVars);
$tableData = array_combine(
    array_keys($twigVars),
    array_map(fn($v) => fn() => $twigFormated[$v], array_keys($twigVars))
);

$twigVarsTab = $this->Html->tag('article');
$twigVarsTab .= $this->Html->tag('header');
$twigVarsTab .= $this->Html->tag('h3', __("Champs de formulaire"));
$twigVarsTab .= $this->Html->tag('/header');
$twigVarsTab .= $this->ViewTable->generate(
    new Entity(),
    $twigVarsCategories['input']
);
$twigVarsTab .= $this->Html->tag('/article');

$twigVarsTab .= $this->Html->tag('article');
$twigVarsTab .= $this->Html->tag('header');
$twigVarsTab .= $this->Html->tag('h3', __("Extracteurs"));
$twigVarsTab .= $this->Html->tag('/header');
$twigVarsTab .= $this->ViewTable->generate(
    new Entity(),
    $twigVarsCategories['extract']
);
$twigVarsTab .= $this->Html->tag('/article');

$twigVarsTab .= $this->Html->tag('article');
$twigVarsTab .= $this->Html->tag('header');
$twigVarsTab .= $this->Html->tag('h3', __("Variables"));
$twigVarsTab .= $this->Html->tag('/header');
$twigVarsTab .= $this->ViewTable->generate(
    new Entity(),
    $twigVarsCategories['var']
);
$twigVarsTab .= $this->Html->tag('/article');

$twigVarsTab .= $this->Html->tag('article');
$twigVarsTab .= $this->Html->tag('header');
$twigVarsTab .= $this->Html->tag('h3', __("Autres"));
$twigVarsTab .= $this->Html->tag('/header');
$twigVarsTab .= $this->ViewTable->generate(
    new Entity(),
    $twigVarsCategories['meta']
);
$twigVarsTab .= $this->Html->tag('/article');

$tabs->add(
    'tab-test-twigvars-infos',
    $this->Fa->i('fa-cube fa-space', __("Variables twig")),
    $twigVarsTab
);

echo $tabs;

?>
<script>
    $(() => $('#test-form-modal-2 button.accept').remove());

    $(() => $('#test-form-modal-2 button.cancel')
        .off('click.filedelete')
        .on(
            'click.filedelete',
            () => {
                $.ajax({
                    url: '<?=$this->Url->build('/fileuploads/delete/' . $entity->get('id'))?>',
                    method: 'DELETE',
                    error: function (e) {
                        if (e.responseText) {
                            alert(e.responseText);
                        } else {
                            alert(e);
                        }
                    }
                });
            }
        ));

    var table = $('.test-form-refresh-table');
    var generator = TableGenerator.instance[table.attr('data-table-uid')];
    var entity = generator.getDataId(<?= $formId ?>);
    if (entity) {
        entity.tested = true;
        entity.Forms.tested = true;
    }
    generator.generateAll();
</script>
