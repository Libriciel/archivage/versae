<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->Form->create(
    $entity,
    [
        'idPrefix' => 'add1-form',
        'id' => 'add1-form-form',
    ]
);
echo $this->Form->control(
    'name',
    [
        'label' => __("Nom du formulaire"),
    ]
);
echo $this->Form->control(
    'description',
    [
        'label' => __("Description"),
    ]
);
echo $this->Form->control(
    'seda_version',
    [
        'label' => __("Version du SEDA"),
        'options' => [
            'seda1.0' => __("SEDA version {0}", '1.0'),
            'seda2.1' => __("SEDA version {0}", '2.1'),
            'seda2.2' => __("SEDA version {0}", '2.2'),
        ],
        'default' => 'seda2.2',
    ]
);

echo $this->Form->control(
    'transferring_agencies._ids',
    [
        'label' => __("Services versants autorisés à utiliser le formulaire pour les versements"),
        'options' => $transferringAgencies,
        'data-placeholder' => __("-- Sélectionner un/plusieurs service(s) versant(s) --"),
        'multiple' => true,
        'required' => true,
    ]
);

echo $this->Form->control(
    'archiving_system_id',
    [
        'label' => __("Système d'Archivage Electronique (SAE) destinataire"),
        'empty' => __("-- Choisir un SAE --"),
        'options' => $archivingSystems,
    ]
);

echo $this->Form->control(
    'user_guide',
    [
        'label' => __("Guide utilisateur"),
        'class' => 'mce mce-small',
    ]
);
echo $this->Form->end();
?>
<script>
    AsalaeGlobal.select2($('#add1-form-transferring-agencies-ids'), __("Services versants disponibles"));
    new AsalaeMce({selector: '#add1-form-user-guide'});

    $('#add1-form-name').on('keyup', function() {
        $('#add1-form-title').val($(this).val());
    });
</script>
