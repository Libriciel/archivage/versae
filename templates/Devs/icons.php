<?php

/**
 * @var Versae\View\AppView $this
 */

/**
 * Ajoute le code hex de l'icone
 * @param string $u
 * @return string
 */
$uniord = function ($u) {
    $k = mb_convert_encoding($u, 'UCS-2LE', 'UTF-8');
    $k1 = ord(substr($k, 0, 1));
    $k2 = ord(substr($k, 1, 1));
    return dechex($k2 * 256 + $k1);
};
foreach ($icons as $css => $ics) {
    echo $this->Html->tag('h2.h3', $css);
    foreach ($ics as $class => $str) {
        $hexValue = $uniord($str);

        echo $this->Html->tag('table.table.table-striped.table-hover.fixed-left-th');
        echo $this->Html->tag('tbody');

        echo $this->Html->tag('tr');
        echo $this->Html->tag('th', $hexValue);
        echo $this->Html->tag('td', sprintf('\\%s', $hexValue));
        echo $this->Html->tag('td', sprintf('&amp;#x%s;', $hexValue));
        echo $this->Html->tag('/tr');

        echo $this->Html->tag('tr');
        echo $this->Html->tag('th', 'fa ' . $class);
        echo $this->Html->tag('td', $this->Html->tag('span.fa.fa-2x.' . $class, ''));
        echo $this->Html->tag(
            'td',
            $this->Html->tag('span.fa.fa-2x', $str),
            ['title' => 'font-family: "FontAwesome";']
        );
        echo $this->Html->tag('/tr');

        echo $this->Html->tag('tr');
        echo $this->Html->tag('th', 'far ' . $class);
        echo $this->Html->tag('td', $this->Html->tag('span.far.fa-2x.' . $class, ''));
        echo $this->Html->tag(
            'td',
            $this->Html->tag('span.far.fa-2x', $str),
            ['title' => 'font-family: "Font Awesome 5 Free";']
        );
        echo $this->Html->tag('/tr');

        echo $this->Html->tag('tr');
        echo $this->Html->tag('th', 'fas ' . $class);
        echo $this->Html->tag('td', $this->Html->tag('span.fas.fa-2x.' . $class, ''));
        echo $this->Html->tag(
            'td',
            $this->Html->tag('span.fas.fa-2x', $str),
            ['title' => 'font-family: "Font Awesome 5 Free"; font-weight: 900;']
        );
        echo $this->Html->tag('/tr');

        echo $this->Html->tag('/tbody');
        echo $this->Html->tag('/table');
    }
}
