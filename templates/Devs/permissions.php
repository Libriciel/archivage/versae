<?php

/**
 * @var Versae\View\AppView $this
 * @var string[] $roles
 */

?>
<script>
    function inputPerm(role) {
        return function (value, context) {
            var button = $('<button type="button">')
                .attr('data-path', context.path)
                .attr('data-role', role);
            button.on('click', function (event) {
                changeperm(this, event);
            });
            var icon = $('<i aria-hidden="true" class="fa fa-2x">');
            var str = $('<span class="sr-only">').text('change perm');
            button.append(icon).append(str);
            if (value === 'mixed') {
                button.attr('onclick', '').disable().addClass('gray');
                icon.addClass('fa-check-square');
            } else if (value === null) {
                button.attr('data-value', 'null');
                icon.addClass('fa-square-o');
            } else if (value) {
                button.attr('data-value', 'true');
                icon.addClass('fa-check-square-o');
            } else {
                button.attr('data-value', 'false');
                icon.addClass('fa-minus-square-o');
            }
            return button;
        }
    }

    function changeperm(button, event) {
        button = $(button);
        button.closest('tr').removeClass('alert-warning');
        var icon = button.find('i');
        var value = button.attr('data-value');
        if (event.shiftKey) {
            value = 'null';
        }
        if (event.ctrlKey) {
            value = 'true';
        }
        if (event.shiftKey && event.ctrlKey) {
            value = 'false';
        }
        icon.removeClass('fa-check-square-o')
            .removeClass('fa-minus-square-o')
            .removeClass('fa-square-o');
        switch (value) {
            case 'true':
                button.attr('data-value', 'false');
                icon.addClass('fa-minus-square-o');
                break;
            case 'false':
                button.attr('data-value', 'null');
                icon.addClass('fa-square-o');
                break;
            case 'null':
            default:
                button.attr('data-value', 'true');
                icon.addClass('fa-check-square-o');
        }
        button.addClass('gray').disable();
        $('html').addClass('ajax-loading');
        $.ajax(
            {
                url: '<?=$this->Url->build('/devs/set-permission')?>',
                method: 'POST',
                data: {
                    path: button.attr('data-path'),
                    role: button.attr('data-role'),
                    value: button.attr('data-value'),
                },
                error: function () {
                    alert(PHP.messages.genericError);
                },
                complete: function () {
                    $('html').removeClass('ajax-loading');
                    button.removeClass('gray').enable();
                }
            }
        )
    }
</script>
<?php
$this->start('script');
echo $this->Html->script('jquery.floatThead.js', ['ext' => '']);
$this->end();

$fields = [
    'controller' => [
        'label' => __("Controlleur"),
    ],
    'action' => [
        'label' => __("Action"),
    ],
    'group' => [
        'label' => __("Groupe"),
    ],
    'traduction' => [
        'label' => __("Traduction"),
    ],
];
foreach ($roles as $role) {
    $fields[$role] = [
        'label' => $role,
        'callback' => 'inputPerm("' . $role . '")',
        'title' => $role,
    ];
}

$table = $this->Table
    ->create($tableId = 'permissions-table', ['class' => 'table table-striped table-hover table-fixed-headers'])
    ->fields($fields)
    ->data($data)
    ->params(
        [
            'identifier' => 'path',
            'classEval' => 'data[{index}].defined === false ? "alert-warning" : ""',
        ]
    );

echo $this->Html->tag('section.bg-white');
echo $this->Html->tag('header');
echo $this->Html->tag('h4', __("Permissions"));
echo $this->Html->tag('/header');

echo $this->Html->tag('div.row'); // row
echo $this->Html->tag('div.col-3');
echo $this->Html->tag(
    'div',
    $this->Fa->i('fa-2x fa-check-square-o', __("Accès autorisé"), ['v5' => false])
);
echo $this->Html->tag(
    'div',
    $this->Fa->i('fa-2x fa-minus-square-o', __("Accès refusé"), ['v5' => false])
);
echo $this->Html->tag(
    'div',
    $this->Fa->i('fa-2x fa-square-o', __("Non défini (accès refusé par héritage)"), ['v5' => false])
);
echo $this->Html->tag(
    'div',
    $this->Fa->i('fa-2x fa-check-square gray disabled', __("Accès partiel (crud) à définir manuellement"))
);
echo $this->Html->tag(
    'button',
    $this->Fa->i('fa-caret-down', __("Scroller vers le prochain droit non défini")),
    ['class' => 'btn btn-default', 'onclick' => 'scrollToUndefined()']
);
echo $this->Html->tag('/div'); // col-3

echo $this->Html->tag('div.col-8');

echo $this->Html->tag('div.keyboard-comb');
echo $this->Fa->i(
    'fa-mouse-pointer',
    __("Rotation des permissions: accès, refusé, hérité"),
    ['style' => 'font-size: 22px; margin-top: -5px']
);
echo $this->Html->tag('/div');

echo $this->Html->tag('div.keyboard-comb');
echo $this->Html->tag('span.keyboard-key', 'shift');
echo $this->Fa->i('fa-plus');
echo $this->Fa->i('fa-mouse-pointer', __("Autorise l'accès"), ['style' => 'font-size: 22px; margin-top: -5px']);
echo $this->Html->tag('/div');

echo $this->Html->tag('div.keyboard-comb');
echo $this->Html->tag('span.keyboard-key', 'ctrl');
echo $this->Fa->i('fa-plus');
echo $this->Fa->i('fa-mouse-pointer', __("Refuse l'accès"), ['style' => 'font-size: 22px; margin-top: -5px']);
echo $this->Html->tag('/div');

echo $this->Html->tag('div.keyboard-comb');
echo $this->Html->tag('span.keyboard-key', 'ctrl');
echo $this->Fa->i('fa-plus');
echo $this->Html->tag('span.keyboard-key', 'shift');
echo $this->Fa->i('fa-plus');
echo $this->Fa->i('fa-mouse-pointer', __("Permission hérité"), ['style' => 'font-size: 22px; margin-top: -5px']);
echo $this->Html->tag('/div');

echo $this->Html->tag('/div'); // col-8

echo $this->Html->tag('/div'); // row

echo $this->Html->tag('/section');
echo $this->Html->tag('div.mt-1', $table);
?>
<script>
    var table = $('#<?=$tableId?>');
    table.floatThead(
        {
            position: 'fixed',
            top: table.closest('section').offset().top
        }
    );

    function scrollToUndefined() {
        $([document.documentElement, document.body]).animate({
            scrollTop: $("tr.alert-warning").offset().top - 170
        }, 'fast');
    }
</script>
