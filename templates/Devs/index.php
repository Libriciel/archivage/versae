<?php

/**
 * @var Versae\View\AppView $this
 */
use Cake\Utility\Hash;

$invisibles = [
    '1' => __("Pas de gestion des droits"),
    '0' => __("Gestion des droits dans l'application"),
];

$commeDroits = [];
foreach ($controllers as $controller => $actions) {
    foreach ($actions as $action) {
        $commeDroits[$controller][$controller . '::' . $action] = $controller . '::' . $action;
    }
}

$inAcos = [];
$aliasAcos = [];
foreach ($acos as $aco) {
    if ($aco->model) {
        $inAcos[] = $aco->model . '::' . $aco->alias;
    }
}
?>
<div class="container">
    <h1>Dev</h1>
</div>

<div class="affix">
    <?php
    foreach (array_keys($controllers) as $controller) {
        $as[] = $this->Html->tag('a', $controller, ['href' => '#' . $controller]);
    }
    echo '<ul class="nav"><li>' . implode('</li><li>', $as) . '</li></ul>';
    ?>
</div>

<section class="container bg-white">
    <header>
        <h2 class="h4">Aco</h2>
    </header>
    <form class="no-padding" method="post">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th><?=__("Action")?></th>
                    <th><?=__("Gestion des droits")?></th>
                    <th><?=__("Accès")?></th>
                    <th><?=__("Comme droits")?></th>
                    <th><?=__("Groupe")?></th>
                </tr>
            </thead>
            <tbody>
        <?php foreach ($controllers as $controller => $actions) :?>
                <tr>
                    <th colspan="6" class="h4" id="<?=$controller?>"><?=$controller?></th>
                </tr>
            <?php foreach ($actions as $action) :?>
                <tr<?=!in_array($controller . '::' . $action, $inAcos) ? ' class="danger"' : ''?>>
                    <td>
                        <input type="hidden" value="<?=$controller?>" class="id-controller">
                        <input type="hidden" name="new[<?=$controller . '::' . $action?>]" value="0" class="new">
                        <input type="hidden" name="update[<?=$controller . '::' . $action?>]" value="0" class="update">
                    <?php
                    echo implode(
                        '</td><td>',
                        [
                            $action,
                            $this->Form->control(
                                $n = $controller . '.' . $action . '.invisible',
                                [
                                    'type' => 'radio', 'class' => 'visible',
                                    'required' => true,
                                    'options' => $invisibles, 'label' => false,
                                    'value' => Hash::get($data, $n)
                                ]
                            ),
                            $this->Form->control(
                                $n = $controller . '.' . $action . '.accesParDefaut',
                                [
                                    'type' => 'checkbox',
                                    'class' => 'accesParDefaut',
                                    'checked' => Hash::get($data, $n)
                                ]
                            ),
                            $this->Form->control(
                                $n = $controller . '.' . $action . '.commeDroits',
                                [
                                    'label' => __("Hérite des droits de"),
                                    'type' => 'select',
                                    'options' => $commeDroits,
                                    'empty' => __("-- Sélectionner un commeDroit --"),
                                    'class' => 'commeDroits',
                                    'value' => Hash::get($data, $n)
                                ]
                            ),
                            $this->Form->control(
                                $n = $controller . '.' . $action . '.group',
                                [
                                    'label' => __("Groupe de permissions"),
                                    'class' => 'group', 'type' => 'select',
                                    'options' => $groups,
                                    'empty' => __("-- Sélectionner un groupe --"),
                                    'value' => Hash::get($data, $n)
                                ]
                            ),
                        ]
                    );
                    ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endforeach; ?>
            </tbody>
        </table>
        <div class="container">
            <?=$this->Form->button(
                $this->Fa->charte(
                    'Enregistrer',
                    __("Enregistrer")
                ),
                ['bootstrap-type' => 'primary', 'class' => 'accept']
            )?>
        </div><br>
    </form>
</section>

<script>
$('tr.danger input.new').each(function() {
    $(this).val(1);
});
$('input.visible').change(function() {
    var tr = $(this).closest('tr');
    if ($('input.visible[name="'+$(this).attr('name')+'"]:checked').val() === '0'
        && tr.find('select.commeDroits').val() === ''
    ) {
        tr.find('select.group').enable();
    } else {
        tr.find('select.group').disable();
    }
}).change();
$('select.commeDroits').change(function() {
    var val = $(this).val();
    var tr = $(this).closest('tr');
    tr.find('input:not(.new):not(.update)').prop('disabled', function () {
        return val;
    });
    if (val) {
        tr.find('input.aucunsDroits').prop('checked', false);
        tr.find('input.accesParDefaut').prop('checked', false);
        tr.find('input.visible').prop('checked', true);
    }
    tr.find('input.visible:checked').change();
}).change();
$('input.aucunsDroits').change(function() {
    var val = $(this).prop('checked'),
        tr = $(this).closest('tr'),
        val2 = tr.find('select.commeDroits').val();
    tr.find('input:not(.aucunsDroits):not(.new):not(.update), select').prop('disabled', function () {
        return val || ($(this).prop('tagName') !== 'SELECT' && val2);
    });
    if (val) {
        tr.find('input.accesParDefaut').prop('checked', true);
        tr.find('input.visible').prop('checked', true);
    }
}).change();

$('input, select').change(function(e) {
    $(this).closest('tr').removeClass('danger').addClass('warning')
        .find('input.update').val(1);
    e.stopPropagation();
});

// Affiche un background rouge sur les controlleur a gauche qui ont besoin de définir des valeurs
$('tr.danger input.id-controller').each(function() {
    $('li > a[href="#'+$(this).val()+'"]').parent().addClass('alert-danger');
});
</script>
