<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Datasource\EntityInterface;
use Cake\I18n\I18n;
use Cake\Utility\Hash;
use Versae\View\AppView;

$this->Breadcrumbs->add(__("Tableau de bord"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag(
        'h1',
        $this->Fa->charte('Tableau de bord', __("Tableau de bord"))
    )
    . $this->Breadcrumbs->render()
);

$buttons = [];
if ($this->Acl->check('/deposits/add1')) {
    $buttons[] = $this->Html->link(
        $this->Fa->charte('Ajouter', __("Créer un versement")),
        '/deposits/index-in-progress?add=deposit',
        ['class' => 'btn btn-success', 'escape' => false]
    );
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}

echo $this->ModalView
    ->create('view-deposit-modal', ['size' => 'modal-xxl'])
    ->modal(__("Visualisation du versement"))
    ->output(
        'function',
        'viewDeposit',
        '/deposits/view'
    )
    ->generate();

/**
 * Graph
 */
echo $this->Html->tag('h2', __("Tous les versements de mon service"));
echo $this->Html->tag('div.container');
echo $this->Html->tag('div.row');
echo $this->Html->tag('div.col-lg-8.col-md-12');
echo $this->Html->tag(
    'canvas#chart-transfers',
    '',
    ['width' => '400', 'height' => '400']
);
echo $this->Html->tag('/div');
echo $this->Html->tag('div.col-lg-4.col-md-12');
echo $this->Html->tag(
    'canvas#chart-ratio-conform',
    '',
    ['width' => '400', 'height' => '400']
);
echo $this->Html->tag('/div');
echo $this->Html->tag('/div');
echo $this->Html->tag('/div');

/**
 * Rendu de la vignette
 * @param AppView         $view
 * @param EntityInterface $values
 */
$buildInnerVignetteValidation = function (
    AppView $view,
    EntityInterface $values
) {
    echo $view->Html->tag('tr');
    echo $view->Html->tag('td');

    // Date | identifier
    echo $view->Html->tag('div.td-group');
    echo $view->Html->tag(
        'span.td-group-fieldvalue.h4.h5',
        Hash::get($values, 'created')
    );
    echo $view->Html->tag(
        'span.td-group-fieldvalue.h4.float-right',
        h(Hash::get($values, 'form.name'))
    );
    echo $view->Html->tag('/div');
    // étape
    echo $view->Html->tag('div.td-group');
    echo $view->Html->tag('span.td-group-fieldname', __("état"));
    echo $view->Html->tag(
        'span.td-group-fieldvalue',
        Hash::get($values, 'statetrad')
    );
    echo $view->Html->tag('/div');

    // versant
    if ($transfer = Hash::get($values, 'transfer.identifier')) {
        echo $view->Html->tag('div.td-group');
        echo $view->Html->tag('span.td-group-fieldname', __("transfert"));
        echo $view->Html->tag('span.td-group-fieldvalue', h($transfer));
        echo $view->Html->tag('/div');
    }

    // erreurs
    if ($error = Hash::get($values, 'error_message')) {
        echo $view->Html->tag('div.td-group');
        echo $view->Html->tag('span.td-group-fieldname', __("erreur"));
        echo $view->Html->tag('span.td-group-fieldvalue', h($error));
        echo $view->Html->tag('/div');
    }

    echo $view->Html->tag('/td');

    // actions
    echo $view->Html->tag('td.action');
    echo $view->Html->tag('div.td-group');
    echo $view->Html->tag(
        'button',
        $view->Fa->charte('Visualiser'),
        [
            'onclick' => 'viewDeposit(' . $values->id . ')',
            'type' => 'button',
            'class' => 'btn-link',
            'title' => $title = __(
                "Visualiser {0}",
                h(Hash::get($values, 'name'))
            ),
            'aria-label' => $title
        ]
    );
    echo $view->Html->tag('/div');
    echo $view->Html->tag('/td');

    echo $view->Html->tag('/tr');
};
echo $this->Html->tag('div.container.container-flex.home');

/**
 * En préparation
 */
echo $this->Html->tag('section.bg-white.vignette');
echo $this->Html->tag(
    'header',
    $this->Html->tag(
        'h2.h4',
        $this->Html->link(
            __("Mes versements en préparation ({0})", $preparing_count),
            '/deposits/index-in-progress?state%5B0%5D%5B%5D=editing'
        )
    )
);
echo $this->Html->tag(
    'table.table.table-striped.table-hover.table-vignette.smart-td-size'
);
echo $this->Html->tag('tbody');
foreach ($preparing as $values) {
    $buildInnerVignetteValidation($this, $values);
}

echo $this->Html->tag('/tbody');
echo $this->Html->tag('/table');

echo $this->Html->tag('/section');

/**
 * En erreur
 */
echo $this->Html->tag('section.bg-white.vignette');
echo $this->Html->tag(
    'header',
    $this->Html->tag(
        'h2.h4',
        $this->Html->link(
            __("Mes versements en erreur ({0})", $errors_count),
            '/deposits/index-in-progress?state%5B0%5D%5B%5D=preparing_error&state%5B0%5D%5B%5D=sending_error'
            . '&state%5B0%5D%5B%5D=acknowledging_error&state%5B0%5D%5B%5D=answering_error'
        )
    )
);
echo $this->Html->tag(
    'table.table.table-striped.table-hover.table-vignette.smart-td-size'
);
echo $this->Html->tag('tbody');
foreach ($errors as $values) {
    $buildInnerVignetteValidation($this, $values);
}
echo $this->Html->tag('/tbody');
echo $this->Html->tag('/table');
echo $this->Html->tag('/section');

/**
 * Terminés
 */
echo $this->Html->tag('section.bg-white.vignette');
echo $this->Html->tag(
    'header',
    $this->Html->tag(
        'h2.h4',
        $this->Html->link(
            __("Mes versements acceptés/refusés ({0})", $finished_count),
            '/deposits/index-all'
        )
    )
);
echo $this->Html->tag(
    'table.table.table-striped.table-hover.table-vignette.smart-td-size'
);
echo $this->Html->tag('tbody');
foreach ($finished as $values) {
    $buildInnerVignetteValidation($this, $values);
}

echo $this->Html->tag('/tbody');
echo $this->Html->tag('/table');

echo $this->Html->tag('/section');


echo $this->Html->tag('/div'); // div.container

[$lang] = explode('_', I18n::getLocale());
?>
<script>
    $(function () {
        moment.locale('<?=$lang?>');

        function getDayChartLabels() {
            var dt = new Date;
            var labels = [];
            dt.setDate(dt.getDate() - 7);
            for (var i = 7; i > 0; i--) {
                dt.setDate(dt.getDate() + 1);
                labels.push(moment(dt).format('dddd'));
            }
            return labels;
        }

        var ctx = document.getElementById('chart-transfers');
        var chartTransfers = new Chart(ctx, {
            type: 'line',
            data: {
                labels: getDayChartLabels(),
                datasets: [{
                    label: '# Acceptés',
                    data: <?=json_encode(
                        array_values($dataChartTransfersAccepted)
                    )?>,
                    backgroundColor: '#90f69f',
                    borderWidth: 1
                }, {
                    label: '# Refusés',
                    data: <?=json_encode(
                        array_values($dataChartTransfersRefused)
                    )?>,
                    backgroundColor: '#f69b90',
                    borderWidth: 1
                }, {
                    label: '# En cours',
                    data: <?=json_encode(array_values($dataChartTransfers))?>,
                    backgroundColor: 'rgba(0, 147, 255, 0.4)',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                responsive: true,
                maintainAspectRatio: false,
            }
        });
        var ctx2 = document.getElementById('chart-ratio-conform');
        var chartRatio = new Chart(ctx2, {
            type: 'pie',
            data: {
                labels: [__("acceptés"), __("refusés"), __("en cours")],
                datasets: [{
                    data: <?=json_encode($dataChartRatio)?>,
                    backgroundColor: [
                        'rgba(0, 255, 39, 0.4)',
                        'rgba(255, 29, 0, 0.4)',
                        'rgba(0, 147, 255, 0.4)'
                    ]
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
            }
        });
    });
</script>
