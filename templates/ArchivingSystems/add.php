<?php

/**
 * @var Versae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */

echo $this->Flash->render();
echo $this->Form->create($entity, ['idPrefix' => 'add-archiving-system']);

require 'addedit-common.php';
echo $this->Form->end();
