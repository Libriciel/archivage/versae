<?php
/**
 * @var Versae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 * @var array $entities
 */

if (empty($org_entity_id)) {
    $locked = $entity->get('removable')
        ? []
        : [
            'readonly' => true,
            'disabled' => true,
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
        ];

    echo $this->Form->control(
        'org_entity_id',
        [
            'label' => __("Entité du Système d'Archivage Electronique"),
            'options' => $sas,
            'empty' => __("-- Sélectionner une entité --"),
            'default' => count($sas) === 1 ? current(array_keys($sas)) : null,
            'help' => __("Optionnel - Vous pourrez sélectionner ce SAE depuis l'interface des services d'archives"),
        ]
        + $locked
    );
}
echo '<label class="hide">autocomplete obliterator'
    . '<input type="password" value="false"><!-- neutralise le préremplissage firefox --></label>';
echo $this->Html->tag(
    'div',
    $this->Form->control(
        'connecteur',
        [
            'label' => __("Connecteur"),
            'options' => [
                'asalaev2' => 'Asalae V2', // option unique pour l'instant
            ],
            'readonly',
        ]
    )
    . $this->Form->control(
        'name',
        [
            'label' => __("Nom du Système d'Archivage Electronique"),
        ]
    )
    . $this->Form->control(
        'description',
        [
            'label' => __("Description"),
        ]
    )
    . $this->Form->control(
        'url',
        [
            'label' => __("Url de l'application ({0})", 'valeur de App.fullBaseUrl'),
            'placeholder' => 'https://asalae.libriciel.fr',
            'type' => 'url',
        ]
    )
    . $this->Form->control(
        'chunk_size',
        [
            'label' => __("Envoi par morceaux: Taille des morceaux en octet"),
            'min' => 1024 * 1024,
            'max' => 1000000000,
            'step' => 1024 * 1024,
            'help' => __(
                "Laissez vide pour une connexion bidirectionnelle - valeur max de php par défaut: {0}",
                '2097152 (2Mo)'
            ),
        ]
    )
    . $this->Form->control(
        'active',
        [
            'label' => __("Actif"),
            'default' => true,
        ]
    )
    . $this->Form->control(
        'use_proxy',
        [
            'label' => __("Utiliser le proxy"),
            'default' => true,
        ]
    )
);
$urlDelete = $this->Url->build('/');
$uploads = $this->Upload
    ->create('upload-archiving-system-cert', ['class' => 'table table-striped table-hover hide'])
    ->fields(
        [
            'info'    => [
                'label'  => __("Fichier"),
                'target' => '',
                'thead'  => [
                    'filename' => [
                        'label'    => __("Nom de fichier"),
                        'callback' => 'TableHelper.wordBreak()',
                    ],
                    'hash' => [
                        'label'    => __("Empreinte du fichier"),
                        'callback' => 'TableHelper.wordBreak("_", 32)',
                    ],
                    'size' => [
                        'label' => __("Taille"),
                        'callback' => 'TableHelper.readableBytes',
                    ],
                ],
            ],
            'message' => [
                'label'    => __("Message"),
                'style'    => 'width: 400px',
                'class'    => 'message',
            ],
        ]
    )
    ->data($uploadData ?? [])
    ->params(
        [
            'identifier' => 'id',
            'classEval' => 'data[{index}].valid !== false ? "" : "danger"',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "$('.archiving-system-cafile').val('{0}')",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-check-square'),
                'title' => $title = __("Sélectionner {0} comme certificat", '{1}'),
                'aria-label' => $title,
                'params' => ['path', 'name']
            ],
            function ($table) {
                $deleteUrl = $this->Url->build('/upload/delete');
                return [
                    'onclick' => "TableGenericAction.deleteAction($table->tableObject, '$deleteUrl')({0})",
                    'type' => 'button',
                    'class' => 'btn-link',
                    'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'params' => ['id', 'name']
                ];
            },
        ]
    );
$jsUpload = $uploads->jsObject;
$jsTableUpload = $this->Upload->getTableId('upload-archiving-system-cert');
echo $this->Form->fieldset(
    $this->Form->control(
        'ssl_verify_peer',
        [
            'label' => __("Activation de la certification SSL"),
        ]
    )
    . $this->Form->control(
        'ssl_verify_peer_name',
        [
            'label' => __("Vérification du nom d’hôte (certification SSL)"),
        ]
    )
    . $this->Form->control(
        'ssl_verify_depth',
        [
            'label' => __("Profondeur de parcours de la chaîne du certificat"),
            'type' => 'number',
            'min' => 1,
            'step' => 1,
            'max' => 100,
        ]
    )
    . $this->Form->control(
        'ssl_verify_host',
        [
            'label' => __("Valide le certificat SSL pour un nom d’hôte"),
            'help' => __("Compare le nom d'hôte du certificat SSL avec celui défini dans l'url"),
        ]
    )
    . $this->Html->tag(
        'div.form-group.fake-input',
        $this->Html->tag('span.fake-label', __("Certificats"))
        . $uploads->generate(
            [
                'singleFile' => false,
                'allowDuplicateUploads' => true,
                'target' => $this->Url->build('/upload/archiving-system-cert')
            ]
        )
    )
    . $this->Form->control(
        'ssl_cafile',
        [
            'label' => __("Chemin vers le fichier du certificat CA"),
            'class' => 'archiving-system-cafile',
        ]
    ),
    ['legend' => __("Paramétrage SSL")]
);
echo $this->Form->fieldset(
    $this->Form->control(
        'username',
        [
            'label' => __("Identifiant de connexion"),
            'placeholder' => 'wslogin',
        ]
    )
    . $this->Form->control(
        'password',
        [
            'label' => __("Mot de passe de l'utilisateur"),
            'type' => 'password',
        ]
    )
    . $this->Html->tag(
        'div',
        $this->Html->tag(
            'button',
            __("Tester la connexion"),
            [
                'type' => 'button',
                'class' => 'btn btn-default',
                'onclick' => 'testConnection(this)'
            ]
        )
    ),
    ['legend' => __("Identification (basic)")]
);

?>
<script>
    function testConnection(button) {
        var form = $(button).closest('form');
        var popup = new AsalaePopup($(button).closest('form'));
        if (form.is(':valid')) {
            $('html').addClass('ajax-loading');
            popup.element()
                .append(AsalaeModal.loading)
                .css({
                    "max-width": 600,
                    "max-height": 400,
                    overflow: "auto"
                });
            popup.show();
            popup.element()
                .css({
                    top: '',
                    bottom: 50,
                    left: 165
                });
            $.ajax({
                url: '<?=$this->Url->build('/ArchivingSystems/testConnection')?>',
                method: 'POST',
                data: form.serialize(),
                success: function(content) {
                    popup.element().append(
                        $('<h4>').text(__("Réponse de {0}", form.find('input[name="url"]').val()))
                    );
                    var table = $('<table class="table-condensed table-striped">');
                    var tbody = $('<tbody>');
                    var tr;
                    table.append(tbody);
                    for (var key in content) {
                        tr = $('<tr>');
                        tr.append($('<th>').text(key));
                        tr.append($('<td>').text(content[key]));
                        tbody.append(tr)
                    }
                    popup.element().append(table);
                },
                error: function (e) {
                    popup.element().append($('<p>').text(e.responseJSON));
                },
                complete: function() {
                    $('html').removeClass('ajax-loading');
                    popup.element().find('.loading-container').remove();
                }
            });
        } else {
            form.get(0).reportValidity();
        }
    }

    $('#upload-archiving-system-cert').on('fileSuccess', function(event, file, data) {
        $('.archiving-system-cafile').val(data.report.path);
    });
</script>
