<?php

/**
 * @var Versae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */

$loading = $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);

$infos = $this->Html->tag('article');
$infos .= $this->Html->tag('header.bottom-space');
$infos .= $this->Html->tag('h3', h($archivingSystem->get('name')));
$infos .= $this->Html->tag('/header');

$infos .= $this->Html->tag('h4', __("Système d'Archivage Electronique"));
$infos .= $this->ViewTable->generate(
    $archivingSystem,
    [
        __("Entité du Système d'Archivage Electronique") => 'org_entity.name',
        __("Connecteur") => function () {
            return 'Asalae V2';
        },
        __("Nom du Système d'Archivage Electronique") => 'name',
        __("Description") => 'description',
        __("Url de l'application (valeur de App.fullBaseUrl)") => 'url',
        __("Actif") => 'active',
        __("Utiliser le proxy") => 'use_proxy',
        __("Activation de la certification SSL") => 'ssl_verify_peer',
        __("Vérification du nom d’hôte (certification SSL)") => 'ssl_verify_peer_name',
        __("Profondeur de parcours de la chaîne du certificat") => 'ssl_verify_depth',
        __("Valide le certificat SSL pour un nom d’hôte") => 'ssl_verify_host',
        __("Chemin vers le fichier du certificat CA") => 'ssl_cafile',
        __("Identifiant de connexion") => 'username',
        __("Password")  => function () {
            return '********';
        },
        __("Date de création") => 'created',
        __("Date de modification") => 'modified',
    ]
);
$infos .= $this->Html->tag('/article');

echo $infos;
