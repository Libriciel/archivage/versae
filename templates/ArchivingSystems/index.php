<?php

/**
 * @var Versae\View\AppView $this
 * @var ArchivingSystem $archivingSystem
 */

use Versae\Model\Entity\ArchivingSystem;

$view = $this;

echo $this->element('modal', ['idTable' => $tableId, 'paginate' => false]);

$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'ArchivingSystems.name' => ['label' => __("Nom")],
            'ArchivingSystems.url' => ['label' => __("Url")],
            'ArchivingSystems.org_entity.name' => ['label' => __("Service d'Archives")],
            'ArchivingSystems.username' => ['label' => __("Identifiant Utilisateur")],
            'ArchivingSystems.active' => ['label' => __("Actif"), 'type' => 'boolean'],
            'ArchivingSystems.use_proxy' => ['label' => __("Actif"), 'type' => 'boolean', 'display' => false],
            'ArchivingSystems.created' => [
                'label' => __("Date de création"),
                'type' => 'date',
                'display' => false,
            ],
            'ArchivingSystems.modified' => [
                'label' => __("Date de Modification"),
                'type' => 'date',
                'display' => false,
            ],
        ]
    )
    ->data($archivingSystems)
    ->params(['identifier' => 'ArchivingSystems.id'])
    ->actions(
        [
            [
                'onclick' => "actionViewArchivingSystem({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => $title = __("Voir {0}", '{1}'),
                'aria-label' => $title,
                'params' => ['ArchivingSystems.id', 'ArchivingSystems.name']
            ],
            [
                'onclick' => "actionTestArchivingSystem({0}, '{2}')",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-calendar-check-o'),
                'title' => $title = __("Tester {0}", '{1}'),
                'aria-label' => $title,
                'params' => ['ArchivingSystems.id', 'ArchivingSystems.name', 'ArchivingSystems.url']
            ],
            [
                'onclick' => "actionEditArchivingSystem({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier {0}", '{1}'),
                'aria-label' => $title,
                'params' => ['ArchivingSystems.id', 'ArchivingSystems.name']
            ],
            function ($table) use ($view) {
                return [
                    'type' => 'button',
                    'class' => 'btn-link',
                    'data-callback' => sprintf(
                        "TableGenericAction.deleteAction(%s, '%s')({0}, false)",
                        $table->tableObject,
                        $view->Url->build('/ArchivingSystems/delete')
                    ),
                    'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => $title = __("Supprimer {0}", '{1}'),
                    'aria-label' => $title,
                    'data-action' => __("Supprimer"),
                    'displayEval' => 'data[{index}].ArchivingSystems.deletable',
                    'confirm' => __("Êtes-vous sûr de vouloir supprimer ce système d'archivage électronique ?"),
                    'params' => ['ArchivingSystems.id', 'ArchivingSystems.name']
                ];
            },
            [
                'type' => 'button',
                'disabled' => true,
                'class' => 'btn-link',
                'href' => '#',
                'label' => '<i class="fas fa-trash" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'title' => $title = __("Pour pouvoir être supprimé, ne doit pas être lié à un Service d'Archives"),
                'aria-label' => $title,
                'data-action' => __("Supprimer"),
                'displayEval' => '!data[{index}].ArchivingSystems.deletable',
                'params' => [],
            ],
        ]
    );

$jsTable = $table->tableObject;

echo $this->Html->tag(
    'section#archiving-system-section.bg-white',
    $this->Html->tag(
        'div.separator',
        $this->ModalForm
            ->create('add-archiving-system-modal')
            ->modal(__("Ajout d'un Système d'Archivage Electronique"))
            ->javascriptCallback('TableGenericAction.afterAdd(' . $jsTable . ', "ArchivingSystems")')
            ->output(
                'button',
                $this->Fa->charte('Ajouter', __("Ajouter un Système d'Archivage Electronique")),
                '/ArchivingSystems/add'
            )
            ->generate(['class' => 'btn btn-success'])
        . $this->ModalView
            ->create('view-archiving-system-modal')
            ->modal(__("Visualiser un Système d'Archivage Electronique"))
            ->output(
                'function',
                'actionViewArchivingSystem',
                '/ArchivingSystems/view'
            )
            ->generate()
        . $this->ModalForm
            ->create('edit-archiving-system-modal')
            ->modal(__("Modification d'un Système d'Archivage Electronique"))
            ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTable . ', "ArchivingSystems")')
            ->output(
                'function',
                'actionEditArchivingSystem',
                '/ArchivingSystems/edit'
            )
            ->generate()
    )
    . $this->Html->tag(
        'header',
        $this->Html->tag('h2.h4', __("Liste des Système d'Archivage Electroniques"))
        . $this->Html->tag('div.r-actions.h4', $table->getConfigureLink())
    )
    . $table->generate()
);
?>
<script>
    function actionTestArchivingSystem(id, url) {
        var popup = new AsalaePopup($('#archiving-system-section'));
        $('html').addClass('ajax-loading');
        popup.element()
            .append(AsalaeModal.loading)
            .css({
                "max-width": 600,
                "max-height": 400,
                overflow: "auto"
            });
        popup.show();
        popup.element()
            .css({
                top: '',
                bottom: 50,
                left: 165
            });
        $.ajax({
            url: '<?=$this->Url->build('/ArchivingSystems/testConnection')?>/'+id,
            method: 'GET',
            success: function(content) {
                popup.element().append(
                    $('<h4>').text(__("Réponse de {0}", url))
                );
                var table = $('<table class="table-condensed table-striped">');
                var tbody = $('<tbody>');
                var tr;
                table.append(tbody);
                for (var key in content) {
                    tr = $('<tr>');
                    tr.append($('<th>').text(key));
                    tr.append($('<td>').text(content[key]));
                    tbody.append(tr)
                }
                popup.element().append(table);
            },
            error: function (e) {
                popup.element().append($('<p>').text(e.responseJSON));
            },
            complete: function() {
                $('html').removeClass('ajax-loading');
                popup.element().find('.loading-container').remove();
            }
        });
    }
</script>
