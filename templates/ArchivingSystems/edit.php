<?php

/**
 * @var Versae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */

echo $this->Flash->render();
echo $this->Form->create($entity, ['idPrefix' => 'edit-archiving-system']);

$passwordRequired = false;
$idSelectEntity = 'edit-archiving-system-org-entity-id';
$idSelectRoles = 'edit-archiving-system-role-id';

require 'addedit-common.php';
echo $this->Form->end();
