<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Core\Configure;

$config = Configure::read(
    'Admins.probe',
    [
        'loadavg' => [
            'low' => 0.7, // vert
            'medium' => 1.2, // bleu
            'high' => 1.6, // jaune
            'max' => 5, // rouge
        ],
        'mem' => [
            'low' => 25, // vert
            'medium' => 50, // bleu
            'high' => 75, // jaune
            'max' => 100, // rouge
        ],
        'swap' => [
            'low' => 25, // vert
            'medium' => 50, // bleu
            'high' => 75, // jaune
            'max' => 100, // rouge
        ],
    ]
);

/**
 * Donne une couleur en fonction de la charge $load
 * @param float $load
 * @param array $config
 * @return string
 */
$getColor = function (float $load, array $config): string {
    $class = 'progress-bar-success';
    if ($load > $config['low']) {
        $class = 'progress-bar-primary';
    }
    if ($load > $config['medium']) {
        $class = 'progress-bar-warning';
    }
    if ($load > $config['high']) {
        $class = 'progress-bar-danger';
    }
    return $class;
};

echo $this->Fa->button(
    'fa-pause',
    __("Stopper/reprendre le rafraîchissement"),
    [
        'id' => 'refresh-probe',
        'class' => 'btn-link pause',
        'onclick' => 'toogleProbeRefresh()',
        'style' => 'position: absolute; right: 0; z-index: 100',
    ]
);

echo $this->Html->tag('div.row');

echo $this->Html->tag('div.col-md-4');
echo $this->Html->tag('span', 'Load average 1 min');
$current = $probe['loadavg'][0] ?? 0;
$tx = $config['loadavg']['max'] ? $current / $config['loadavg']['max'] : 1;
$percentage = min((int)($tx * 100), 100);
echo $this->Html->tag(
    'div.progress.progress-flat',
    $this->Html->tag(
        'div',
        $this->Html->tag(
            'span',
            $current
        ),
        [
            'role' => 'progressbar',
            'aria-valuenow' => min($current, $config['loadavg']['max']),
            'aria-min' => 0,
            'aria-max' => $config['loadavg']['max'],
            'style' => 'width: ' . $percentage . '%',
            'class' => 'progress-bar ' . $getColor($current, $config['loadavg']),
        ]
    )
);
echo $this->Html->tag('/div');

echo $this->Html->tag('div.col-md-4');
echo $this->Html->tag('span', 'Load average 5 min');
$current = $probe['loadavg'][1] ?? 0;
$tx = $config['loadavg']['max'] ? $current / $config['loadavg']['max'] : 1;
$percentage = min((int)($tx * 100), 100);
echo $this->Html->tag(
    'div.progress.progress-flat',
    $this->Html->tag(
        'div',
        $this->Html->tag(
            'span',
            $current
        ),
        [
            'role' => 'progressbar',
            'aria-valuenow' => min($current, $config['loadavg']['max']),
            'aria-min' => 0,
            'aria-max' => $config['loadavg']['max'],
            'style' => 'width: ' . $percentage . '%',
            'class' => 'progress-bar ' . $getColor($current, $config['loadavg']),
        ]
    )
);
echo $this->Html->tag('/div');

echo $this->Html->tag('div.col-md-4');
echo $this->Html->tag('span', 'Load average 15 min');
$current = $probe['loadavg'][2] ?? 0;
$tx = $config['loadavg']['max'] ? $current / $config['loadavg']['max'] : 1;
$percentage = min((int)($tx * 100), 100);
echo $this->Html->tag(
    'div.progress.progress-flat',
    $this->Html->tag(
        'div',
        $this->Html->tag(
            'span',
            $current
        ),
        [
            'role' => 'progressbar',
            'aria-valuenow' => min($current, $config['loadavg']['max']),
            'aria-min' => 0,
            'aria-max' => $config['loadavg']['max'],
            'style' => 'width: ' . $percentage . '%',
            'class' => 'progress-bar ' . $getColor($current, $config['loadavg']),
        ]
    )
);
echo $this->Html->tag('/div');

// row
echo $this->Html->tag('/div');
echo $this->Html->tag('div.row');

// RAM
echo $this->Html->tag('div.col-lg-6');
echo $this->Html->tag('span', 'RAM');
$current = $probe['memory']['total'] - $probe['memory']['available'];
$tx = $probe['memory']['total'] ? $current / $probe['memory']['total'] : 1;
$percentage = min((int)($tx * 100), 100);
echo $this->Html->tag(
    'div.progress.progress-flat',
    $this->Html->tag(
        'div',
        $this->Html->tag(
            'span',
            $this->Number->toReadableSize($current * 1024) . ' / '
            . $this->Number->toReadableSize($probe['memory']['total'] * 1024)
        ),
        [
            'role' => 'progressbar',
            'aria-valuenow' => min($current, $config['mem']['max']),
            'aria-min' => 0,
            'aria-max' => $config['mem']['max'],
            'style' => 'width: ' . $percentage . '%',
            'class' => 'progress-bar ' . $getColor($percentage, $config['mem']),
        ]
    )
);
echo $this->Html->tag('/div');

// Swap
echo $this->Html->tag('div.col-lg-6');
echo $this->Html->tag('span', 'SWAP');
$current = $probe['swap']['total'] - $probe['swap']['available'];
$tx = $probe['swap']['total'] ? $current / $probe['swap']['total'] : 1;
$percentage = min((int)($tx * 100), 100);
echo $this->Html->tag(
    'div.progress.progress-flat',
    $this->Html->tag(
        'div',
        $this->Html->tag(
            'span',
            $this->Number->toReadableSize($current * 1024) . ' / '
            . $this->Number->toReadableSize($probe['swap']['total'] * 1024)
        ),
        [
            'role' => 'progressbar',
            'aria-valuenow' => min($current, $config['swap']['max']),
            'aria-min' => 0,
            'aria-max' => $config['swap']['max'],
            'style' => 'width: ' . $percentage . '%',
            'class' => 'progress-bar ' . $getColor($percentage, $config['swap']),
        ]
    )
);
echo $this->Html->tag('/div');
// row
echo $this->Html->tag('/div');
