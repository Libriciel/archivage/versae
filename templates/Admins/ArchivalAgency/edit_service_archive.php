<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->element('modal', ['idTable' => $tableIdUser, 'paginate' => false]);

$tableUser = $this->Table
    ->create($tableIdUser, ['class' => 'table table-striped table-hover smart-td-size'])
    ->url(
        $url = [
            'controller' => 'admins',
            'action' => 'paginate-users',
            $entity->id,
            '?' => [
                'sort' => 'username',
                'direction' => 'asc',
            ]
        ]
    )
    ->fields(
        [
            'username' => ['label' => __("Identifiant de connexion")],
            'name' => ['label' => __("Nom d'utilisateur")],
            'role.name' => ['label' => __("Rôle")],
            'active' => ['label' => __("Actif"), 'type' => 'boolean'],
            'created' => ['label' => __("Date de création"), 'type' => 'datetime', 'display' => false],
            'modified' => ['label' => __("Date de modification"), 'type' => 'datetime', 'display' => false],
        ]
    )
    ->data($entity->get('users'))
    ->params(
        [
            'identifier' => 'id',
            'checkbox' => false,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionViewWebservice({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => __("Voir {0}", '{1}'),
                'aria-label' => __("Voir {0}", '{1}'),
                'displayEval' => 'data[{index}].agent_type === "software"',
                'params' => ['id', 'name']
            ],
            [
                'onclick' => "actionEditWebservice({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'displayEval' => 'data[{index}].agent_type === "software"',
                'params' => ['id', 'name']
            ],
            [
                'onclick' => "loadEditModalUser({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'displayEval' => 'data[{index}].agent_type === "person"',
                'params' => ['id', 'name']
            ],
            [
                'type' => "button",
                'class' => "btn-link",
                'data-callback' => 'deleteUser({0})',
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'confirm' => __("Êtes-vous sûr de vouloir supprimer cet utilisateur ?"),
                'displayEval' => 'data[{index}].Users.deletable === true',
                'params' => ['Users.id', 'Users.username']
            ]
        ]
    );
$jsTableUser = $tableUser->tableObject;
$paginator = $this->AjaxPaginator->create('pagination-org-entity-users')
    ->url($url)
    ->table($tableUser)
    ->count($countUsers);

$addBtn = '';
if ($rolesUsers) {
    $addBtn .= $this->ModalForm
        ->create('edit-entity-add-user-modal')
        ->modal(__("Ajouter un utilisateur"))
        ->javascriptCallback('afterAddUserSA')
        ->output(
            'button',
            $this->Fa->charte('Ajouter', __("Ajouter un utilisateur")),
            '/Users/add/' . $entity->get('id')
        )
        ->generate(['class' => 'btn btn-success', 'type' => 'button']);
}
if ($rolesWs) {
    $addBtn .= $this->ModalForm
        ->create('add-webservice-modal')
        ->modal(__("Ajout d'un accès de webservice"))
        ->javascriptCallback('TableGenericAction.afterAdd(' . $jsTableUser . ', "Users")')
        ->output(
            'button',
            $this->Fa->charte('Ajouter', __("Ajouter un accès de webservice")),
            '/Webservices/add/' . $entity->get('id')
        )
        ->generate(['class' => 'btn btn-success']);
}

if (count($ldaps)) {
    $addBtn .= $this->ModalForm
        ->create('import-ldap-users')
        ->modal(__("Importer un utilisateur LDAP"))
        ->javascriptCallback('TableGenericAction.afterAdd(' . $jsTableUser . ', "Users")')
        ->output(
            'button',
            $this->Fa->charte('Ajouter', __("Importer un utilisateur LDAP")),
            '/Admins/importLdapUser/' . $entity->get('id')
        )
        ->generate(['class' => 'btn btn-success']);
}

$infosTab = $this->Form->create($entity, ['id' => 'edit-org-entity-sa-form', 'idPrefix' => 'edit-org-entity-sa'])
    . $this->Form->control(
        '_tab',
        [
            'type' => 'hidden',
            'value' => 'main',
        ]
    )
    . $this->Form->control('name', ['label' => __("Nom"), 'required'])
    . $this->Form->control(
        'identifier',
        [
            'label' => __("Identifiant unique"),
            'required',
        ]
    )
    . $this->Form->control(
        'max_disk_usage_conv',
        [
            'label' => __("Limite logicielle de taille du disque"),
            'append' => $this->Input->mult('mult', 'go'),
            'class' => 'with-select',
            'type' => 'number',
            'min' => 1,
            'required' => true,
        ]
    )
    . $this->Form->control(
        'ldaps._ids',
        [
            'label' => __("Ldaps disponibles"),
            'options' => $ldaps,
            'multiple' => true,
            'data-placeholder' => __("-- Sélectionner des Ldaps disponibles --"),
        ]
    )
    . $this->Form->control(
        'archiving_systems._ids',
        [
            'label' => __("Systèmes d'Archivage Electronique pour les transferts sortants"),
            'options' => $archivingSystems,
            'multiple' => true,
            'data-placeholder' => __("-- Sélectionner parmis les SAE disponibles --"),
        ]
    )
    . $this->Html->tag(
        'div',
        $this->Form->button(
            $this->Fa->charte('Enregistrer', __("Enregistrer")),
            ['bootstrap-type' => 'primary']
        ),
        ['class' => 'text-right']
    )
    . $this->Form->end();

$usersTab = $this->Html->tag(
    'section.bg-white',
    $this->Html->tag(
        'div',
        $paginator->scrollBottom(['style' => 'position: absolute; right: 20px; z-index:100'])
        . $addBtn
        . $this->ModalForm
            ->create('user-edit')
            ->modal(__("Edition d'un utilisateur"))
            ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTableUser . ', "Users")')
            ->output('function', 'loadEditModalUser', '/Users/edit-by-admin')
            ->generate()
        . $this->ModalView
            ->create('view-webservice-modal')
            ->modal(__("Visualiser un accès de webservice"))
            ->output(
                'function',
                'actionViewWebservice',
                '/Webservices/view'
            )
            ->generate()
        . $this->ModalForm
            ->create('edit-webservice-modal')
            ->modal(__("Modification d'un accès de webservice"))
            ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTableUser . ', "Users")')
            ->output(
                'function',
                'actionEditWebservice',
                '/Webservices/edit'
            )
            ->generate(),
        ['class' => 'separator']
    )
    . $this->Html->tag(
        'header',
        $this->Html->tag('h2.h4', __("Liste des utilisateurs de l'entité"))
        . $this->Html->tag('div.r-actions.h4', $tableUser->getConfigureLink())
    )
    . $tableUser . $paginator . $paginator->scrollTop()
);

$rgpdTab = $this->Form->create(
    $entity,
    ['id' => 'edit-org-entity-sa-rgpd', 'idPrefix' => 'edit-org-entity-sa-rgpd']
)
    . $this->Html->tag(
        'div.alert.alert-info',
        $this->Html->tag(
            'p',
            __(
                "Les informations ci-dessous seront affichées dans la section \""
                . "Responsable des traitements\" dans la politique de confidentialité."
            )
        )
        . $this->Html->tag(
            'p',
            __("Les sous entités héritent de ces informations.")
        )
    );
$rgpdTab .= $this->Form->fieldset(
    $this->Form->control(
        '_tab',
        [
            'type' => 'hidden',
            'value' => 'rgpd',
        ]
    )
    . $this->Form->control(
        'rgpd_info.org_name',
        [
            'label' => __("Raison sociale"),
        ]
    )
    . $this->Form->control(
        'rgpd_info.org_address',
        [
            'label' => __("Adresse"),
        ]
    )
    . $this->Form->control(
        'rgpd_info.org_siret',
        [
            'label' => __("Numéro SIRET"),
        ]
    )
    . $this->Form->control(
        'rgpd_info.org_ape',
        [
            'label' => __("Code APE"),
        ]
    )
    . $this->Form->control(
        'rgpd_info.org_phone',
        [
            'label' => __("N° de téléphone"),
        ]
    )
    . $this->Form->control(
        'rgpd_info.org_email',
        [
            'label' => __("E-mail"),
        ]
    ),
    ['legend' => __("Entité")]
);

$rgpdTab .= $this->Form->fieldset(
    $this->Form->control(
        'rgpd_info.ceo_name',
        [
            'label' => __("Nom du responsable"),
        ]
    )
    . $this->Form->control(
        'rgpd_info.ceo_function',
        [
            'label' => __("Qualité/fonction du responsable"),
        ]
    ),
    ['legend' => __("Responsable de l'entité/Responsable des traitements")]
);

$rgpdTab .= $this->Form->fieldset(
    $this->Form->control(
        'rgpd_info.dpo_name',
        [
            'label' => __("Nom du délégué"),
        ]
    )
    . $this->Form->control(
        'rgpd_info.dpo_email',
        [
            'label' => __("Adresse e-mail du délégué"),
        ]
    ),
    ['legend' => __("Délégué à la protection des données de l'entité (DPD / DPO)")]
);
$rgpdTab .= $this->Form->end();

$tabs = $this->Tabs->create('tabs-edit-archival-agency', ['class' => 'row no-padding']);
$tabs->add(
    'tab-edit-archival-agency-infos',
    $this->Fa->charte('Modifier', __("Edition du service d'archives")),
    $infosTab
);
$tabs->add(
    'tab-edit-archival-agency-users',
    $this->Fa->charte('Utilisateurs', __("Utilisateurs")),
    $usersTab,
    ['div' => ['class' => 'no-padding']]
);
$tabs->add(
    'tab-edit-archival-agency-rgpd',
    $this->Fa->i('fa-user-secret', __("RGPD")),
    $rgpdTab
);
echo $tabs;
?>
<!--suppress JSJQueryEfficiency -->
<script>
    var entitySaForm = $('#edit-org-entity-sa-form');

    AsalaeGlobal.select2($('#edit-org-entity-sa-ldaps-ids'));
    AsalaeGlobal.select2($('#edit-org-entity-sa-archiving-systems-ids'));

    var initialFormSAValue = '';
    setTimeout(function() {
        initialFormSAValue = entitySaForm.serialize();
    }, 0);
    $('.save-on-change').click(function() {
        var form = $('#edit-org-entity-sa-form');
        var formValue = entitySaForm.serialize();
        if (formValue !== initialFormSAValue
            && confirm(__("Vous avez apporté des modifications au formulaire. Souhaitez-vous les sauvegarder ?"))
        ) {
            entitySaForm.trigger('submit');
        }
        initialFormSAValue = formValue;
    });

    function deleteUser(id) {
        var afterDeleteUser = function (message) {
            var table = <?= $jsTableUser ?>;
            var tr = $(table.table).find('tr[data-id="'+id+'"]');
            if (message && message.report === 'done') {
                table.removeDataId(id);
                tr.fadeOut(
                    400,
                    function () {
                        $(this).remove();
                        $(table.table).trigger('deleted.tablejs', tr);
                        table.decrementPaginator();
                    }
                );
            } else if (message && message.report) {
                if (typeof AsalaeGlobal === 'object') {
                    return AsalaeGlobal.ajaxError(e);
                }
                tr.addClass('danger');
            }
        };
        AsalaeLoading.ajax(
            {
                url: '<?=$this->Url->build('/users/delete')?>/' + id,
                method: 'DELETE',
                headers: {
                    Accept: 'application/json'
                },
                success: function (content, textStatus, jqXHR) {
                    if (
                        jqXHR.getResponseHeader('X-Asalae-Deposits') === 'true'
                        && confirm(
                            __(
                                "Des versements sont en cours pour cet utilisateur. Si vous confirmez la suppression, "
                                + "ses versements en cours seront également supprimés."
                            )
                        )
                    ) {
                        AsalaeLoading.ajax(
                            {
                                url: '<?=$this->Url->build('/users/delete')?>/' + id,
                                method: 'DELETE',
                                headers: {
                                    Accept: 'application/json',
                                    'X-Versae-Force-Delete': true,
                                },
                                success: afterDeleteUser
                            }
                        )
                    } else {
                        afterDeleteUser(content);
                    }
                }
            }
        );
    }
</script>
