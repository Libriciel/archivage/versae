<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->MultiStepForm->template('MultiStepForm/add_org_entity_sa')->step(1) . '<hr>';

echo $this->Form->create($entity, ['idPrefix' => 'add-service-archive'])
    . $this->Form->control('name', ['label' => __("Nom")])
    . $this->Form->control('identifier', ['label' => __("Identifiant")])
    . $this->Form->control(
        'max_disk_usage_conv',
        [
            'label' => __("Limite logicielle de taille du disque"),
            'append' => $this->Input->mult('mult', 'go'),
            'class' => 'with-select',
            'type' => 'number',
            'min' => 1,
            'required' => true,
        ]
    );

    echo $this->Form->control(
        'ldaps._ids',
        [
            'label' => __("Ldaps disponibles"),
            'options' => $ldaps,
            'multiple' => true,
            'data-placeholder' => __("-- Sélectionner des Ldaps disponibles --"),
        ]
    )
    . $this->Form->control(
        'archiving_systems._ids',
        [
            'label' => __("Systèmes d'Archivage Electronique pour les transferts sortants"),
            'options' => $archivingSystems,
            'multiple' => true,
            'data-placeholder' => __("-- Sélectionner parmis les SAE disponibles --"),
        ]
    )
    . $this->Form->end();
    ?>
<script>
    AsalaeGlobal.select2($('#add-service-archive-ldaps-ids'));
    AsalaeGlobal.select2($('#add-service-archive-archiving-systems-ids'));
</script>
