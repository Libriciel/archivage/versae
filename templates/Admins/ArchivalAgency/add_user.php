<?php

/**
 * @var Versae\View\AppView $this
 * @var Versae\Model\Entity\User $user
 */

echo $this->MultiStepForm->template('MultiStepForm/add_org_entity_sa')->step(2) . '<hr>';
echo $this->Form->create($user, ['autocomplete' => 'off', 'idPrefix' => 'add-user']);
$labelPassword = __("Password");
$idJauge = 'add-user-jauge-password';
$displayRole = false;
$displayPassword = false;
require dirname(__DIR__, 2) . '/Users/addedit-common.php';
echo $this->Form->end();
