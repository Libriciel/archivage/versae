<?php

/**
 * @var Versae\View\AppView $this
 * @var Cake\ORM\ResultSet $workers
 */

?>
<script>
    // evite l'appel multiple de reloadThisModal durant la même seconde
    var lastReload = Math.floor(Date.now() /1000); // timestamp (seconds)
    function reloadThisModal() {
        var time = Math.floor(Date.now() /1000);
        if (time > lastReload) {
            lastReload = time;
            AsalaeWebsocket.unsubscribe(
                ['worker_start', 'worker_stop', 'job_state_changed']
            );
            $('#tasks-monitor.modal:visible').find('button.refresh').click();
            if (tailServiceXhr) {
                tailServiceXhr.abort();
            }
        }
    }

    function indexJobsTube(value) {
        return $('<button type="button" class="btn-link"></button>')
            .text(value)
            .attr('title', __("Liste des jobs du tube {0}", value))
            .on('click', function() {
                viewJobsTube(value);
            });
    }
    function indexJobsState(state) {
        return function (value, context) {
            return $('<button type="button" class="btn-link"></button>')
                .text(value)
                .attr('title', __("Liste des jobs {0} du tube {1}", state, context.tube))
                .on('click', function() {
                    viewJobsTube(context.tube + '?job_state[]='+state);
                });
        }
    }
    function extractDataFromJobs(value, context) {
        if (typeof jobsTable === 'undefined') {
            return value;
        }
        var tableData = jobsTable.getDataId(context.tube);
        return tableData.pending;
    }
</script>
<?php
$view = $this;
$actualiser = $this->Fa->charteBtn(
    'Actualiser',
    $title = __("Actualiser"),
    ['icon' => 'cursor-pointer', 'class' => 'btn-link refresh', 'title' => $title]
);
$jsTable = $this->Table->getJsTableObject($tableId = 'jobs-tube');

echo $this->ModalView
    ->create('view-worker-log', ['size' => 'large'])
    ->modal(__("Affichage des logs d'un worker"))
    ->output(
        'function',
        'viewWorkerLog',
        '/admins/worker-log'
    )
    ->generate();

echo $this->ModalView
    ->create('view-jobs-tube', ['size' => 'modal-xxl'])
    ->modal(__("Affichage des jobs d'un tube") . ' ' . $actualiser)
    ->output(
        'function',
        'viewJobsTube',
        '/admins/index-jobs'
    )
    ->generate();

$tabs = $this->Tabs->create('tabs-tasks', ['class' => 'no-padding']);

/**
 * Jobs tab
 */
$tabs->add(
    'tab-tasks-jobs',
    $this->Fa->i('fa-tasks', __("Jobs")),
    require 'jobs_tab.php',
    ['div' => ['class' => 'no-padding']]
);

/**
 * Workers tab
 */
$tabs->add(
    'tab-tasks-workers',
    $this->Fa->i('fa-wrench', __("Workers")),
    require 'workers_tab.php',
    ['div' => ['class' => 'no-padding']]
);
/**
 * Workers tab
 */
$tabs->add(
    'tab-tasks-service',
    $this->Fa->i('fas fa-drafting-compass', __("Service")),
    require 'workers_service.php',
    ['div' => ['class' => 'no-padding']]
);

echo $tabs;
?>
<!--suppress JSUnusedAssignment -->
<script>
/**
 *
 * @param {string} url
 * @param {string} tube
 * @param {TableGenerator} tableGenerator
 */
function ajaxAction(url, tube, tableGenerator) {
    var tr = tableGenerator.table.find('tr[data-id="'+tube+'"]').first();
    $('html').addClass('ajax-loading');
    tr.addClass('warning').removeClass('error').removeClass('success');
    $.ajax({
        url: url,
        headers: {Accept: 'application/json'},
        success: function(content, textStatus, jqXHR) {
            if (typeof content === 'object' && typeof content.success === 'boolean') {
                tr.removeClass('warning');
                if (content.success) {
                    if (typeof content.tube === 'string') {
                        tr = tr.parent().find('tr').filter(function() {
                            return $(this).find('td.tube').text().trim() === content.tube;
                        });
                    }
                    tr.addClass('success');
                } else {
                    tr.addClass('error');
                    alert(content.error);
                }
            }
            switch (jqXHR.getResponseHeader('X-Asalae-Success')) {
                case 'true':
                    reloadThisModal();
                    break;
                case 'false':
                    tr.addClass('danger').removeClass('warning');
                    break;
            }
        },
        error: function() {
            tr.addClass('danger').removeClass('warning');
            alert(PHP.messages.genericError);
        },
        complete: function() {
            $('html').removeClass('ajax-loading');
        }
    });
}

function stopAllWorkers() {
    if (!confirm("<?=__d('tasks', "Arrêter tout les workers ?")?>")) {
        return;
    }

    $.ajax({
        url: '<?=$this->Url->build('/Admins/stopAllWorkers')?>',
        complete: function() {
            reloadThisModal();
        }
    });
}

var chrono = function() {
    return (Date.now ? Date.now() : new Date().getTime());
};
var lastTailTime = chrono();

var tailServiceXhr;
var tailServiceBtn = $('button.tail-service');
function tailService(url, textAreaId, button) {
    var textarea = $('#'+textAreaId),
        btn = tailServiceBtn,
        modal = btn.closest('.modal');
    if (tailServiceXhr) {
        tailServiceXhr.abort();
    }
    textarea.off('.activearea').one('focus.activearea', function() {
        if (tailServiceXhr) {
            tailServiceXhr.abort();
        }
        btn.removeClass('active')
            .attr('title', btn.attr('data-play'))
            .disable()
            .find('i.fas')
            .addClass('fa-play')
            .removeClass('fa-pause');
    }).one('blur.activearea', function(e) {
        setTimeout(function() {
            if (btn.is(':enabled')) {
                return;
            }
            tailService(url, textAreaId);
            btn.addClass('active')
                .attr('title', btn.attr('data-pause'))
                .enable()
                .find('i.fass')
                .removeClass('fa-play')
                .addClass('fa-pause');
        }, 200);// nécéssaire si on click sur le bouton (100=insuffisants)
    });

    btn.blur()
        .attr('onclick', '')
        .off('click.abort')
        .one('click.abort', function(e) {
            $('button.tail:not(.active)').enable()
                .find('i.fas').removeClass('fa-lock').addClass('fa-play');
            $(this).one('click.abort', function() {
                $(this).data('abortAjax', false);
                tailService(url, textAreaId);
            });
            $(this).data('abortAjax', true)
                .removeClass('active')
                .attr('title', btn.attr('data-play'))
                .find('i.fas')
                .addClass('fa-play')
                .removeClass('fa-pause');
            modal.off('hide.bs.modal');
            if (tailServiceXhr) {
                tailServiceXhr.abort();
            }
        })
        .addClass('active')
        .attr('title', btn.attr('data-pause'))
        .find('i.fas')
        .removeClass('fa-play')
        .addClass('fa-pause');
    modal.one('hide.bs.modal', function () {
        if (tailServiceXhr) {
            tailServiceXhr.abort();
        }
    });
    if (typeof btn.data('abortAjax') === 'undefined') {
        btn.data('abortAjax', false);
    }
    // on limite à un tail toutes les 10s
    var delay;
    var timeSinceLastTail = chrono() - lastTailTime;
    if (timeSinceLastTail > 10000) {
        delay = 0;
    } else {
        delay = 10000 - timeSinceLastTail;
    }
    setTimeout(
        function() {
            lastTailTime = chrono();
            tailServiceXhr = $.ajax({
                url: url,
                xhr: function() {
                    var xhr = new XMLHttpRequest();
                    var loaded = false;
                    xhr.addEventListener("readystatechange", function() {
                        if (this.readyState === 3 && !btn.data('abortAjax')) {// === LOADING
                            if (loaded) {
                                var textarea = $('#'+textAreaId);
                                var actualValue = textarea.val();
                                var newValue = this.response.trim();
                                if (textarea.length && actualValue !== newValue) {
                                    textarea.val(newValue);
                                    textarea.scrollTop(textarea.get(0).scrollHeight);
                                }
                            }
                            loaded = true;
                        }
                    }, false);
                    return xhr;
                },
                success: function(content) {
                    var textarea = $('#'+textAreaId);
                    var actualValue = textarea.val();
                    if (textarea.length && actualValue !== content) {
                        textarea.val(content.trim());
                        textarea.scrollTop(textarea.get(0).scrollHeight);
                    }
                    if (!btn.data('abortAjax')) {
                        tailService(url, textAreaId);
                    }
                }
            });
        },
        delay
    );
}

function stopService(btn)
{
    $(btn).disable();
    $.ajax({
        url: '<?=$this->Url->build('/Admins/stopService')?>',
        complete: function() {
            reloadThisModal();
        }
    });
}

tailServiceBtn.click();

var jobsTable;
jobsTable = <?=$this->Table->getJsTableObject('all-tube')?>;
var workersTable;
workersTable = <?=$this->Table->getJsTableObject('workers-table')?>;
AsalaeWebsocket.unsubscribe(
    ['worker_start', 'worker_stop', 'job_state_changed', 'new_job', 'delete_job']
);

function updateWorkerJobs() {
    for (let i = 0; i < workersTable.data.length; i++) {
        workersTable.data[i].jobs = jobsTable.getDataId(workersTable.data[i].tube).pending;
        $('tr[data-id="'+workersTable.data[i].id+'"] > .td-jobs').text(workersTable.data[i].jobs);
    }
}

AsalaeWebsocket.subscribe(
    'worker_start',
    function(topic, data) {
        var tableData = jobsTable.getDataId(data.message.tube);
        tableData.workers++;
        var td = $('tr[data-id="'+data.message.tube+'"] > td.worker');
        td.text(tableData.workers);

        workersTable.data.push(data.message);
        workersTable.generateAll();
    }
);
AsalaeWebsocket.subscribe(
    'worker_stop',
    function(topic, data) {
        var tableData = jobsTable.getDataId(data.message.tube);
        tableData.workers--;
        var td = $('tr[data-id="'+data.message.tube+'"] > td.worker');
        td.text(tableData.workers);

        workersTable.removeDataId(data.message.id);
        workersTable.generateAll();
    }
);
AsalaeWebsocket.subscribe(
    'new_job',
    function(topic, data) {
        var tableData = jobsTable.getDataId(data.message.tube);
        tableData[data.message.state]++;
        var td = $('tr[data-id="'+data.message.tube+'"] > td.'+data.message.state);
        var value = indexJobsState(data.message.state)(tableData[data.message.state], tableData);
        td.html(value);

        updateWorkerJobs();
    }
);
AsalaeWebsocket.subscribe(
    'job_state_changed',
    function(topic, data) {
        var tableData = jobsTable.getDataId(data.message.tube);

        tableData[data.message.prev_state]--;
        var td = $('tr[data-id="'+data.message.tube+'"] > td.'+data.message.prev_state);
        var value = indexJobsState(data.message.prev_state)(tableData[data.message.prev_state], tableData);
        td.html(value);

        tableData[data.message.state]++;
        td = $('tr[data-id="'+data.message.tube+'"] > td.'+data.message.state);
        value = indexJobsState(data.message.state)(tableData[data.message.state], tableData);
        td.html(value);

        updateWorkerJobs();
    }
);
AsalaeWebsocket.subscribe(
    'delete_job',
    function(topic, data) {
        var tableData = jobsTable.getDataId(data.message.tube);
        tableData[data.message.prev_state]--;
        var td = $('tr[data-id="'+data.message.tube+'"] > td.'+data.message.prev_state);
        var value = indexJobsState(data.message.prev_state)(tableData[data.message.prev_state], tableData);
        td.html(value);

        updateWorkerJobs();
    }
);

var modal = $('.modal:visible').last();
modal.one(
    'hidden.bs.modal',
    function() {
        AsalaeWebsocket.unsubscribe(
            ['worker_start', 'worker_stop', 'job_state_changed', 'new_job', 'delete_job']
        );
    }
);
setTimeout(
    function() {
        $('a.ui-tabs-anchor').off('click.scrolldown').on('click.scrolldown', function() {
            var e = $('#beanstalk-service-logs').get(0);
            e.scrollTop = e.scrollHeight - e.getBoundingClientRect().height;
        });
    },
    0
);
</script>
