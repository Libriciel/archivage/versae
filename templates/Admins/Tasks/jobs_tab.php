<?php

/**
 * @var Versae\View\AppView $this
 */

use AsalaeCore\View\Helper\Object\Table;
use Beanstalk\Model\Table\BeanstalkJobsTable;

$jsTable = $this->Table->getJsTableObject('all-tube');

$jobTable = $this->Html->tag('div.modal-header');
$jobTable .= $this->Html->tag('h3.h4', __d('tasks', "Gestion des jobs"));
$jobTable .= $this->ModalForm
    ->create('tasks-add-form')
    ->modal(__("Nouveau Job"))
    ->javascriptCallback('reloadThisModal')
    ->output('button', $this->Fa->charte('Ajouter', __("Ajouter un job")), '/Tasks/add')
    ->generate(['class' => 'btn btn-success', 'type' => 'button']);
$jobTable .= $this->Html->tag('/div');
$table = $this->Table
    ->create('all-tube', ['class' => 'table table-striped table-hover jobs-table', ])
    ->fields(
        [
            'tube' => [
                'label' => __d('tasks', "Tube"),
                'callback' => 'indexJobsTube',
                'class' => 'tube',
            ],
            'workers' => [
                'label' => __d('tasks', "Workers"),
                'class' => 'worker',
            ],
            'delayed' => [
                'label' => __d('tasks', "Prévu"),
                'callback' => 'indexJobsState("delayed")',
                'class' => 'delayed',
            ],
            'pending' => [
                'label' => __d('tasks', "Prêts"),
                'callback' => 'indexJobsState("pending")',
                'class' => 'pending',
            ],
            'paused' => [
                'label' => __d('tasks', "En Pause"),
                'callback' => 'indexJobsState("paused")',
                'class' => 'paused',
            ],
            'working' => [
                'label' => __d('tasks', "En cours"),
                'callback' => 'indexJobsState("working")',
                'class' => 'working',
            ],
            'failed' => [
                'label' => __d('tasks', "Échecs"),
                'callback' => 'indexJobsState("failed")',
                'class' => 'failed',
            ],
        ]
    )
    ->data(array_values($tubes))
    ->params(
        [
            'identifier' => 'tube',
            'checkbox' => false,
            'classEval' => 'data[{index}].failed > 0 ? "has-error" : ""',
        ]
    )
    ->actions(
        [
            [
                'type' => "button",
                'onclick' => "viewWorkerLog('{0}')",
                'label' => $view->Fa->i('fa-file-text'),
                'class' => 'btn-link',
                'title' => $title = __d('tasks', "Afficher les logs"),
                'aria-label' => $title,
                'params' => ['tube']
            ],
            function (Table $table) use ($view) {
                return [
                    'type' => "button",
                    'class' => "btn-link",
                    'data-callback' => sprintf(
                        "ajaxAction('%s/{0}', '{0}', %s)",
                        $view->Url->build('/Admins/resumeAllTube'),
                        $table->tableObject
                    ),
                    'confirm' => __d(
                        'tasks',
                        "Tous les jobs en échecs seront relancés. Voulez-vous continuer ?"
                    ),
                    'label' => $view->Fa->i('fas fa-fast-forward text-success'),
                    'title' => $title = __d('tasks', "Relancer tous les jobs"),
                    'displayEval' => 'data[{index}]["' . BeanstalkJobsTable::S_FAILED . '"] > 0',
                    'aria-label' => $title,
                    'params' => ['tube']
                ];
            },
            function (Table $table) use ($view) {
                return [
                    'type' => "button",
                    'class' => "btn-link",
                    'data-callback' => sprintf(
                        "ajaxAction('%s/{0}', '{0}', %s)",
                        $view->Url->build('/Admins/deleteTube'),
                        $table->tableObject
                    ),
                    'confirm' => __d('tasks', "Vider le tube ?"),
                    'label' => $view->Fa->i('fas fa-vial text-danger'),
                    'title' => $title = __d('tasks', "Vider le tube"),
                    'aria-label' => $title,
                    'params' => ['tube']
                ];
            },
        ]
    );

$jobTable .= $table;
return $jobTable;
