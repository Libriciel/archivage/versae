<?php

/**
 * @var Versae\View\AppView $this
 */

?>
<script>
    function parseArrayData(value, context) {
        if (!value) {
            return;
        }
        var table = $('<table class="parsed-json-data"></table>');
        var tbody = $('<tbody></tbody>');
        table.append(tbody);
        var tr;
        if (typeof value === 'string') {
            tbody.append($('<tr></tr>').append($('<td colspan="2">').append(value)));
        } else {
            for (var key in value) {
                tbody.append(
                    $('<tr></tr>')
                        .append($('<th></th>').append(key+':'))
                        .append($('<td></td>').append(value[key]))
                );
            }
        }
        return table;
    }
    function parseErrors(value) {
        if (!value) {
            return;
        } else if (value === 'pause') {
            return 'pause';
        }
        value = value.replace(/ <?=preg_quote(ROOT, '/')?>/g, ' APP');
        var pos = value.indexOf('Stack trace:');
        if (pos) {
            var trace = value.substr(pos + 13).split("\n");
            var ul = $('<ul></ul>').css('display', 'none');
            var button = $('<button class="btn btn-default" type="button"></button>')
                .text('Stack trace')
                .on('click', function() {
                    ul.toggle();
                });
            for (var i = 0; i < trace.length; i++) {
                var split = trace[i].split(':', 2);
                ul.append(
                    (function (i, split) {
                        return $('<li class="toggle-link"></li>')
                            .text(split[0].substr(split[0].lastIndexOf('/') +1))
                            .css('cursor', 'pointer')
                            .css('color', '#23527c')
                            .on('click', function() {
                                var isOpen = $(this).hasClass('open');
                                ul.find('li.open').trigger('rmtext');
                                if (!isOpen) {
                                    $(this).html(trace[i]).addClass('open');
                                }
                            })
                            .on('rmtext', function() {
                                $(this).text(split[0].substr(split[0].lastIndexOf('/') +1))
                                    .removeClass('open');
                            });
                    })(i, split)
                );
            }
            value = value.substr(0, pos).replace("\n", '<hr>');
        }
        var m = value.match(/^((?:\w+\\)*\w*Exception: )(.*) in APP(.*)/);
        if (m) {
            value = '<h4>'+m[1]+'</h4><p>'+m[2]+'</p><code>APP'+m[3]+'</code>';
        }
        if (pos) {
            value = $('<p></p>').html(value).append(button).append(ul);
        }
        return value;
    }
</script>
<?php
use Beanstalk\Model\Table\BeanstalkJobsTable;
use Cake\Core\Configure;

echo $this->Html->tag('h3.h4', __("Tube: {0}", $tube));

$jsTable = $this->Table->getJsTableObject($tableId = 'jobs-tube');

echo $this->ModalForm
    ->create('admin-edit-task')
    ->modal(__("Modifier un job"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTable . ', "BeanstalkJobs")')
    ->output(
        'function',
        'actionEditJob',
        '/tasks/edit'
    )
    ->generate();

echo $this->ModalView->create('admin-view-transfer-tasks', ['size' => 'large'])
    ->modal(__("Visualisation d'un transfert"))
    ->output('function', 'viewTransfer', '/Transfers/view')
    ->generate();

echo $this->ModalView
    ->create('admin-view-user-tasks')
    ->modal(__("Visualiser un utilisateur"))
    ->output(
        'function',
        'loadViewUser',
        '/users/view'
    )
    ->generate();

echo $this->ModalView->create('admin-view-archive-tasks', ['size' => 'modal-xxl'])
    ->modal(__("Visualisation d'une archive"))
    ->output('function', 'viewArchive', '/Archives/view')
    ->generate();

$stateReserved = BeanstalkJobsTable::S_WORKING;
$stateFailed = BeanstalkJobsTable::S_FAILED;
$stateReady = BeanstalkJobsTable::S_PENDING;
$stateDelayed = BeanstalkJobsTable::S_DELAYED;
$statePaused = BeanstalkJobsTable::S_PAUSED;

$table = $this->Table
    ->create($tableId)
    ->fields(
        [
            'id' => [
                'label' => __d('tasks', "ID"),
                'order' => 'id',
                'filter' => [
                    'id[0]' => [
                        'id' => 'filter-id-0',
                        'label' => false,
                        'aria-label' => __("ID"),
                        'type' => 'number',
                        'min' => 1,
                    ],
                ],
            ],
            'object_data' => ['label' => __("Data"), 'callback' => 'parseArrayData'],
            'job_state' => [
                'label' => __("Statut"),
                'class' => 'state',
                'order' => 'job_state',
                'filter' => [
                    'job_state[0]' => [
                        'id' => 'filter-job_state-0',
                        'label' => false,
                        'aria-label' => __("Statut"),
                        'options' => $states,
                        'empty' => __("-- Sélectionner un ou plusieurs états --"),
                        'multiple' => true,
                    ],
                ],
            ],
            'created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'order' => 'created'
            ],
            'user.username' => [
                'label' => __d('tasks', "Utilisateur"),
                'filter' => [
                    'username[0]' => [
                        'id' => 'filter-username-0',
                        'label' => false,
                        'aria-label' => __("Utilisateur"),
                    ],
                ],
            ],
            'errors' => [
                'label' => __("Erreurs"),
                'class' => 'error',
                'callback' => 'parseErrors',
                'style' => 'min-width: 390px'
            ],
            'priority' => ['display' => false, 'order' => 'age'],
            'age' => ['display' => false, 'order' => 'age'],
            'delay' => ['display' => false, 'order' => 'delay'],
            'ttr' => ['display' => false, 'order' => 'ttr'],
        ]
    )
    ->data($data->toArray())
    ->params(
        [
            'identifier' => 'id',
            'classEval' => "!data[{index}].job_state === '$stateReserved' ? 'warning' : ''",
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionEditJob({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'data-action' => __("Modifier"),
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier le job"),
                'aria-label' => $title,
                'displayEval' => "['$stateFailed', '$statePaused'].indexOf(data[{index}].job_state) >= 0 "
                    . "&& typeof data[{index}].object_data === 'object'",
                'params' => ['id']
            ],
            [
                'onclick' => "viewTransfer({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'data-action' => __("Transfert"),
                'title' => $title = __("Visualiser le transfert"),
                'aria-label' => $title,
                'display' => $connected,
                'displayEval' => 'data[{index}].data.transfer_id',
                'params' => ['data.transfer_id']
            ],
            [
                'onclick' => "loadViewUser({0})",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'data-action' => __("Utilisateur"),
                'title' => $title = __("Visualiser l'utilisateur"),
                'aria-label' => $title,
                'display' => $connected,
                'displayEval' => 'data[{index}].data.user_id',
                'params' => ['data.user_id']
            ],
            [
                'onclick' => "viewArchive({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'data-action' => __("Archive (entrée)"),
                'title' => $title = __("Visualiser {0}", '{1}'),
                'aria-label' => $title,
                'display' => $connected,
                'displayEval' => 'data[{index}].data.archive_id',
                'params' => ['data.archive_id'],
            ],
            [
                'onclick' => "jobPause({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-pause'),
                'title' => $title = 'Pause',
                'aria-label' => $title,
                'displayEval' => "['$stateReady', '$stateDelayed'].indexOf(data[{index}].job_state) >= 0",
                'params' => ['id']
            ],
            [
                'onclick' => "jobResume({0}, '{1}')",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-play'),
                'title' => $title = __("Reprendre le job"),
                'aria-label' => $title,
                'displayEval' => "['$stateFailed', '$statePaused'].indexOf(data[{index}].job_state) >= 0",
                'params' => ['id', 'tube']
            ],
            [
                'data-callback' => "jobDelete({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Annuler le job"),
                'aria-label' => $title,
                'confirm' => __("Annuler le job ?"),
                'displayEval' => "data[{index}].job_state !== '$stateReserved'",
                'params' => ['id']
            ],
            [
                'onclick' => "getJobInfo({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-refresh'),
                'title' => $title = __("Actualiser"),
                'aria-label' => $title,
                'params' => ['id']
            ],
        ]
    );

echo $table;

echo $this->AjaxPaginator->create('pagination-jobs-tube')
    ->url(
        [
            'controller' => 'admins',
            'action' => 'index-jobs',
            $tube
        ]
    )
    ->table($table)
    ->count($count)
    ->generate();
?>
<!--suppress JSUnusedAssignment -->
<script>
    function getJobInfo(id) {
        var table = {};
        table = <?=$jsTable?>;
        var tr = table.table.find('tr[data-id="'+id+'"]');
        tr.addClass('alert')
            .addClass('alert-warning')
            .find('button')
            .remove();

        TableGenerator.appendActions(table.data, table.actions);
        table.generateAll();
        $.ajax({
            url: '<?=$this->Url->build('/admins/job-info')?>/'+id,
            success: function(content) {
                if (content) {
                    table.replaceDataId(id, content);
                } else {
                    table.getDataId(id).job_state = __("Terminé");
                }
                TableGenerator.appendActions(table.data, table.actions);
                table.generateAll();
                setTimeout(function () {
                    table.table.find('tr[data-id="'+id+'"] td')
                        .css('background-color', '#5cb85c')
                        .animate({'background-color': 'transparent'});

                }, 0);
            },
            error: function(e) {
                console.error(e);
                alert(e.statusText);
            }
        });
    }

    function jobDelete(id) {
        var table = {};
        table = <?=$jsTable?>;
        var tr = $(table.table).find('tr[data-id="'+id+'"]');
        $.ajax({
            url: '<?=$this->Url->build('/admins/job-cancel')?>/' + id,
            success: function (message) {
                if (message.report === 'done') {
                    table.removeDataId(id);
                    tr.fadeOut(400, function () {
                        AsalaeGlobal.updatePaginationAjax($(this).closest('section'));
                    });
                } else {
                    throw message.report;
                }
            },
            error: function (e, type, code) {
                var message = "<?=__("Une erreur a eu lieu lors de la suppression")?>";
                if (code === 'Forbidden') {
                    message = "<?=__("Vous n'avez pas la permission d'effectuer cette action")?>";
                }
                tr.addClass('danger')
                    .find('td.job_state')
                    .text(message);
            }
        });
    }

    function jobPause(id) {
        var table;
        table = <?=$jsTable?>;
        var tr = $(table.table).find('tr[data-id="'+id+'"]');
        $.ajax({
            url: '<?=$this->Url->build('/admins/job-pause')?>/' + id,
            success: function (message) {
                if (message.report === 'done') {
                    var table;
                    table = <?=$jsTable?>;
                    table.replaceDataId(id, message.data).generateAll();
                } else {
                    throw message.report;
                }
            },
            error: function (e, type, code) {
                var message = "<?=__("Une erreur a eu lieu lors de la mise en pause")?>";
                if (code === 'Forbidden') {
                    message = "<?=__("Vous n'avez pas la permission d'effectuer cette action")?>";
                }
                $('#table-1-tr-' + id)
                    .addClass('danger')
                    .find('td.job_state')
                    .text(message);
            }
        });
    }

    function jobResume(id, tube) {
        var table;
        table = <?=$jsTable?>;
        var tr = $(table.table).find('tr[data-id="'+id+'"]');
        $.ajax({
            url: '<?=$this->Url->build('/admins/job-resume')?>/' + id,
            success: function (message) {
                if (message.report === 'done') {
                    var table;
                    table = <?=$jsTable?>;
                    table.replaceDataId(id, message.data).generateAll();
                } else {
                    throw message.report;
                }
            },
            error: function (e, type, code) {
                var message = "<?=__("Une erreur a eu lieu lors de la reprise")?>";
                if (code === 'Forbidden') {
                    message = "<?=__("Vous n'avez pas la permission d'effectuer cette action")?>";
                }
                tr.addClass('danger')
                    .find('td.job_state')
                    .text(message);
            }
        });
    }

    if (typeof ratchetJobs === 'undefined' || Object.keys(ratchetJobs._subscriptions).length === 0) {
        var ratchetJobs;
        AsalaeWebsocket.connect(
            '<?=Configure::read('Ratchet.connect')?>',
            function(session) {
                ratchetJobs = session;
                ratchet.push(session);
                session.subscribe('work_begin', function(topic, data) {
                    var table;
                    table = <?=$jsTable?>;
                    var tr = $(table.table).find('tr[data-id="'+data.message.jobid+'"]');
                    if (tr.length) {
                        tr.addClass('alert')
                            .addClass('alert-warning');
                        tr.find('td.state').text(__("En cours"));
                        table.getDataId(data.message.jobid).statetrad = __("En cours");
                    }
                });
                session.subscribe('work_end', function(topic, data) {
                    var table;
                    table = <?=$jsTable?>;
                    var tr = $(table.table).find('tr[data-id="'+data.message.jobid+'"]');
                    if (tr.length) {
                        var notSync = $(table.table)
                            .find(
                                'tr.alert-warning.not-sync.tube-'
                                +data.message.tube+':not([data-id="'
                                +data.message.jobid+'"])'
                            );
                        if (notSync.length) {
                            notSync.removeClass('alert')
                                .removeClass('alert-warning')
                                .addClass('gray')
                                .disable()
                                .find('td.state')
                                .text('???');
                            tr.find('td.state').text(__("En cours"));
                            table.getDataId(data.message.jobid).statetrad = __("En cours");
                        }
                        if (data.message.error_message) {
                            tr.removeClass('alert-warning')
                                .addClass('alert-danger')
                                .find('td.error')
                                .text(data.message.error_message);
                            tr.find('td.state').text(__("En erreur"));
                        } else {
                            tr.removeClass('alert').removeClass('alert-warning').addClass('gray').disable();
                            tr.find('a').disable();
                            tr.find('td.state').text(__("Terminé"));
                            table.getDataId(data.message.jobid).statetrad = __("Terminé");
                        }
                        if ($('#auto-reload-index-tasks').prop('checked') && tr.is(':last-child')) {
                            AsalaeGlobal.updatePaginationAjax('#<?=$tableId?>-section');
                            $('#alert-job-container').hide(400, function() {
                                $(this).remove();
                            });
                        }
                    }
                });
            },
            function() {
            },
            {'skipSubprotocolCheck': true}
        );
    }

    function afterEditJob(table, model = null) {
        if (model === null) {
            model = TableGenericAction.getModelName(table);
        }
        return function (content, textStatus, jqXHR) {
            if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                var data = table.getDataId(content.previd);
                if (!data) {
                    console.warn('Unable to edit: content.id was not found');
                    return;
                }
                table.replaceDataId(content.previd, $.extend(true, {[model]: content}, content));
                TableGenerator.appendActions(table.data, table.actions);
                table.generateAll();
            } else {
                AsalaeGlobal.colorTabsInError();
            }
        }
    }
</script>
