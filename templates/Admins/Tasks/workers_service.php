<?php

/**
 * @var Versae\View\AppView $this
 */

$service = $this->Html->tag(
    'div.modal-header',
    $this->Html->tag('h3.h4', __d('tasks', "Gestion du service beanstalk server"))
    . $this->Html->tag(
        'button',
        $this->Fa->i('fa-stop', __d('tasks', "Arreter le service (redemarrage auto)")),
        ['type' => 'button', 'class' => 'btn btn-danger', 'onclick' => 'stopService(this)']
    )
);
$button = $this->Fa->button(
    'fa-play',
    $title = __("Mise à jour en temp réel"),
    [
        'title' => $title,
        'data-play' => $title,
        'data-pause' => __("Arrêter la mise à jour en temps réel"),
        'class' => 'btn btn-link tail tail-service',
        'onclick' => sprintf(
            "tailService('%s', '%s')",
            $this->Url->build('/no-session-renew/admins/tailService'),
            'beanstalk-service-logs'
        ),
    ]
);
$service .= $this->Html->tag(
    'div',
    $this->Html->tag(
        'label',
        __("Logs du service Beanstalk"),
        ['for' => 'beanstalk-service-logs']
    )
    . ' ' . $button
    . $this->Html->tag(
        'textarea',
        $serviceLogs,
        [
            'class' => 'form-control console',
            'rows' => 20,
            'id' => 'beanstalk-service-logs',
            'readonly' => true,
        ]
    ),
    ['class' => 'log-area form-group']
);
return $service;
