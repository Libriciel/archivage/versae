<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->Form->create($job, ['idPrefix' => 'add-worker']);

echo $this->Form->control(
    'tube',
    [
        'label' => __("Tube"),
        'empty' => __("-- Choisir un worker --"),
        'options' => $availables,
    ]
)
. $this->Form->control(
    'keep-alive',
    [
        'label' => __("Maintenir le worker en vie"),
        'type' => 'checkbox',
    ]
);

echo $this->Form->end();
?>
<script>
    $('#add-worker-name').change(function() {
        $('#add-worker-tube').val($(this).val());
    });
</script>
