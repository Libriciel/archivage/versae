<?php

/**
 * @var Versae\View\AppView $this
 * @var Cake\ORM\ResultSet $workers
 */

use AsalaeCore\View\Helper\Object\Table;

$workerTable = $this->Html->tag(
    'div.modal-header',
    $this->Html->tag('h3.h4', __d('tasks', "Gestion des workers"))
    . $this->Html->tag(
        'button',
        $this->Fa->i('fa-stop', __d('tasks', "Arrêter tous les workers")),
        ['type' => 'button', 'class' => 'btn btn-danger', 'onclick' => 'stopAllWorkers()']
    )
    . $this->ModalForm->create('add-worker')
        ->modal(__("Ajout d'un worker"))
        ->javascriptCallback('reloadThisModal')
        ->output(
            'button',
            $this->Fa->charte('Ajouter', __("Ajouter un worker")),
            '/Admins/addWorker'
        )
        ->generate(['class' => 'btn btn-success', 'type' => 'button'])
);

$workerTable .= $this->Table
    ->create('workers-table')
    ->fields(
        [
            'tube' => ['label' => __d('tasks', "Tube"), 'class' => 'tube'],
            'last_launch' => ['label' => __d('tasks', "Dernier lancement"), 'type' => 'datetime'],
            'hostname' => ['label' => 'Hostname'],
            'pid' => ['label' => 'Pid'],
            'jobs' => [
                'label' => __("Jobs prêts"),
                'class' => 'td-jobs',
                'callback' => 'extractDataFromJobs',
            ],
        ]
    )
    ->data($workers->toArray())
    ->params(
        [
            'identifier' => 'id',
            'classEval' => "!data[{index}].running ? 'danger' : ''",
        ]
    )
    ->actions(
        function (Table $table) use ($view) {
            return [
                [
                    'type' => "button",
                    'data-callback' => sprintf(
                        "ajaxAction('%s/{0}', '{0}', %s)",
                        $view->Url->build('/Admins/stopWorker'),
                        $table->tableObject
                    ),
                    'data-action' => __d('task', "Arrêter"),
                    'confirm' => __d(
                        'tasks',
                        "Cette action va envoyer un signal d’arrêt, le worker "
                        . "s’arrêtera lorsqu'il aura fini le travail en cours. Continuer ?"
                    ),
                    'label' => $view->Fa->i('fa-stop text-danger'),
                    'class' => 'stop btn-link',
                    'title' => __d('tasks', "Stop"),
                    'aria-label' => __d('tasks', "Stop"),
                    'displayEval' => "data[{index}].running",
                    'params' => ['id', 'name']
                ],
                [
                    'type' => "button",
                    'data-callback' => sprintf(
                        "ajaxAction('%s/{0}', '{0}', %s)",
                        $view->Url->build('/Admins/killWorker'),
                        $table->tableObject
                    ),
                    'title' => $title = __d('tasks', "Kill"),
                    'aria-label' => $title,
                    'data-action' => $title,
                    'confirm' => __d(
                        'tasks',
                        "Si vous tuez ce worker, le travail en cours sera perdu. Continuer ?"
                    ),
                    'label' => $view->Fa->i('fa-bolt text-danger'),
                    'class' => 'stop btn-link',
                    'displayEval' => "data[{index}].running",
                    'params' => ['id', 'name']
                ],
                [
                    'type' => "button",
                    'onclick' => "viewWorkerLog('{0}')",
                    'label' => $view->Fa->i('fa-file-text'),
                    'class' => 'btn-link',
                    'title' => $title = __d('tasks', "Afficher les logs"),
                    'aria-label' => $title,
                    'params' => ['tube']
                ],
            ];
        }
    )
    ->generate();
return $workerTable;
