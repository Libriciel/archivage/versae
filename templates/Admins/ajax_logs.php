<?php

/**
 * @var Versae\View\AppView $this
 */

$keys = array_keys($logs);
echo $this->Form->control(
    'selectlogfile',
    [
        'label' => __("Fichiers de log"),
        'options' => array_combine($keys, $keys),
        'default' => 'error.log',
    ]
);
foreach ($logs as $logname => $content) {
    $id = preg_replace('/\W+/', '_', basename($logname));
    $button = $this->Fa->button(
        'fa-play',
        $title = __("Mise à jour en temp réel"),
        [
            'title' => $title,
            'data-play' => $title,
            'data-pause' => __("Arrêter la mise à jour en temps réel"),
            'class' => 'btn btn-link tail',
            'onclick' => sprintf(
                "tailF('%s', '%s, this)",
                $this->Url->build('/no-session-renew/admins/tail/' . $logname),
                $id
            ),
        ]
    );
    echo $this->Html->tag(
        'div',
        $this->Html->tag('label', $logname)
        . ' ' . $button
        . $this->Html->tag(
            'pre',
            $content,
            [
                'id' => $id,
                'class' => 'form-control console log',
                'contenteditable' => true,
            ]
        )
        . $this->Html->tag(
            'div.help-block',
            __("Mise à jour du log dans {0}", $this->Html->tag('span.num', '10'))
        )
        . '<hr>'
        . $this->Html->link(
            $this->Fa->charte('Télécharger', __("Télécharger {0}", $logname)),
            sprintf(
                '/admins/downloadLog/%s',
                $logname
            ),
            [
                'class' => 'btn btn-primary',
                'download' => $logname,
                'escape' => false,
            ]
        ),
        ['class' => 'log-area form-group', 'style' => 'display: none', 'data-logfile' => $logname]
    );
}
?>
<script>
$('textarea').each(function() {
    var e = $(this).get(0);
    e.scrollTop = e.scrollHeight - e.getBoundingClientRect().height;
});

var showRemaining = $('div.help-block > span.num');
var tailXhr;
var updateFrequency = 10; // TODO paramétrable ? utilisé comme 2e param dans l'url, default=10
var remaining = updateFrequency;
var intervalRemaining;
function tailF(url, id, button) {
    var textarea = $('#'+id),
        btn = $(button),
        modal = btn.closest('.modal');
    if (tailXhr) {
        tailXhr.abort();
    }
    remaining = 10;
    showRemaining.text(10);
    if (intervalRemaining) {
        clearInterval(intervalRemaining);
    }
    textarea.off('.activearea').one('focus.activearea', function() {
        if (tailXhr) {
            tailXhr.abort();
        }
        if (intervalRemaining) {
            showRemaining.text('PAUSE');
            clearInterval(intervalRemaining);
        }
        btn.removeClass('active')
            .attr('title', btn.attr('data-play'))
            .disable()
            .find('i.fa, i.fas')
            .addClass('fa-play')
            .removeClass('fa-pause');
    }).one('blur.activearea', function(e) {
        setTimeout(function() {
            if (btn.is(':enabled')) {
                return;
            }
            tailF(url, id, button);
            btn.addClass('active')
                .attr('title', btn.attr('data-pause'))
                .enable()
                .find('i.fa, i.fas')
                .removeClass('fa-play')
                .addClass('fa-pause');
        }, 200);// nécessaire si on click sur le bouton (100=insuffisants)
    });

    btn.blur()
        .attr('onclick', '')
        .off('click.abort')
        .one('click.abort', function(e) {
            $('button.tail:not(.active)').enable()
                .find('i.fa, i.fas').removeClass('fa-lock').addClass('fa-play');
            $(this).one('click.abort', function() {
                $(this).data('abortAjax', false);
                tailF(url, id, this);
            });
            $(this).data('abortAjax', true)
                .removeClass('active')
                .attr('title', btn.attr('data-play'))
                .find('i.fa, i.fas')
                .addClass('fa-play')
                .removeClass('fa-pause');
            modal.off('hide.bs.modal');
            if (tailXhr) {
                tailXhr.abort();
            }
            if (intervalRemaining) {
                showRemaining.text('PAUSE');
                clearInterval(intervalRemaining);
            }
        })
        .addClass('active')
        .attr('title', btn.attr('data-pause'))
        .find('i.fa, i.fas')
        .removeClass('fa-play')
        .addClass('fa-pause');
    modal.one('hide.bs.modal', function () {
        if (tailXhr) {
            tailXhr.abort();
        }
        if (intervalRemaining) {
            showRemaining.text('PAUSE');
            clearInterval(intervalRemaining);
        }
    });
    if (typeof btn.data('abortAjax') === 'undefined') {
        btn.data('abortAjax', false);
    }
    $('button.tail:not(.active)').disable()
        .find('i.fa, i.fas').addClass('fa-lock').removeClass('fa-play')
        .closest('.log-area').hide();
    intervalRemaining = setInterval(
        function () {
            remaining--;
            showRemaining.text(remaining);
            if (remaining === 0) {
                clearInterval(intervalRemaining);
            }
        },
        1000
    );
    tailXhr = $.ajax({
        url: url + '/' + updateFrequency,
        xhr: function() {
            var xhr = new XMLHttpRequest();
            var loaded = false;
            xhr.addEventListener("readystatechange", function() {
                if (this.readyState === 3 && !btn.data('abortAjax')) {// === LOADING
                    if (loaded) {
                        var actualValue = textarea.html();
                        var newValue = this.response.trim();
                        if (actualValue !== newValue) {
                            textarea.html((new Filter).toHtml(newValue));
                            textarea.scrollTop(textarea.get(0).scrollHeight);
                        }
                    }
                    loaded = true;
                }
            }, false);
            return xhr;
        },
        success: function(content) {
            var actualValue = textarea.html();
            if (actualValue !== content) {
                textarea.html((new Filter).toHtml(content.trim()));
                textarea.scrollTop(textarea.get(0).scrollHeight);
            }
            if (!btn.data('abortAjax')) {
                clearInterval(intervalRemaining);
                remaining = 0;
                showRemaining.text(0);
                tailF(url, id, button);
            }
        }
    });
}

var selectlogfile = $('#selectlogfile').change(function() {
    if (tailXhr) {
        $('button.tail.active').trigger('click.abort');
    }
    $('.log-area').hide();
    var div = $('[data-logfile="'+$(this).val()+'"]').show();
    div.find('button.tail').click();
    var textarea = div.find('pre.console');
    textarea.scrollTop(textarea.get(0).scrollHeight);
    var actualValue = textarea.html();
    textarea.html((new Filter).toHtml(actualValue));
}).change();
AsalaeGlobal.select2(selectlogfile);
</script>
