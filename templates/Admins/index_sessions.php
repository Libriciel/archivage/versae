<?php

/**
 * @var Versae\View\AppView $this
 */

$view = $this;

echo $this->ModalForm
    ->create(
        'notify-single',
        [
            'size' => 'large',
            'acceptButton' => $this->Form->button(
                $this->Fa->charte('Envoyer', __("Envoyer")),
                ['bootstrap-type' => 'primary', 'class' => 'accept']
            )
        ]
    )
    ->modal(__("Envoyer une notification à un utilisateur"))
    ->output('function', 'loadSessionNotificationModal', '/admins/notify-user')
    ->javascriptCallback('afterSingleNotification')
    ->generate();

$jsTable = $this->Table->getJsTableObject($tableIdSessions);

echo $this->element('modal', ['idTable' => $tableIdSessions, 'paginate' => false]);
$table = $this->Table
    ->create($tableIdSessions, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'Sessions.user.username' => ['label' => __("Identifiant")],
            'Sessions.user.name' => ['label' => __("Nom")],
            'Sessions.user.org_entity.name' => ['label' => __("Entité")],
            'Sessions.user.org_entity.archival_agency.name' => ['label' => __("Service d'archives")],
            'Sessions.created' => ['label' => __("Début de session"), 'type' => 'datetime'],
            'Sessions.modified' => ['label' => __("Dernière action"), 'type' => 'datetime'],
            'Sessions.id' => ['label' => 'ID', 'display' => false],
        ]
    )
    ->data($sessions)
    ->params(
        [
            'identifier' => 'Sessions.id',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "loadSessionNotificationModal('{0}')",
                'type' => 'button',
                'class' => 'btn-link notify',
                'label' => $this->Fa->i('fa-comment'),
                'title' => $title = __("Envoyer une notification à {0}", '{1}'),
                'aria-label' => $title,
                'params' => ['Sessions.id', 'Sessions.user.username']
            ],
            [
                'data-callback' => sprintf(
                    "TableGenericAction.deleteAction(%s, '%s')('{0}', false)",
                    $jsTable,
                    $this->Url->build('/admins/deleteSession')
                ),
                'type' => 'button',
                'class' => 'btn-link delete',
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'confirm' => __(
                    "Supprimer une session déconnecte l'utilisateur, "
                    . "son travail en cours sera perdu, voulez-vous continuer ?"
                ),
                'params' => ['Sessions.id', 'Sessions.user.username']
            ],
        ]
    );

echo $this->Html->tag(
    'section',
    '
    <header>
        <h2 class="h4">' . __("Liste des sessions actives") . '</h2>
        <div class="r-actions h4">
            ' . $table->getConfigureLink() . '
        </div>
    </header>
    ' . $table->generate(),
    ['id' => 'service-sessions-section', 'class' => "bg-white"]
);
?>
<script>
    function afterSingleNotification(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true' && content.success) {
            alert(__("Message envoyé avec succès"));
        }
    }
</script>
