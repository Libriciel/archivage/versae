<?php

/**
 * @var Versae\View\AppView $this
 */

$hours = [];
for ($h = 0; $h < 24; $h++) {
    for ($m = 0; $m < 60; $m += 5) {
        $hours[$s = sprintf('%02d:%02d:00', $h, $m)] = $s;
    }
}

echo $this->Form->create($form, ['idPrefix' => 'interrupt']);

echo $this->Form->control(
    'enabled',
    [
        'label' => __("Activer immédiatement l'interruption")
    ]
);
echo $this->Form->control(
    'scheduled__begin',
    [
        'label' => __("Programmer la date de début de l'interruption"),
        'type' => 'text',
        'append' => $this->Date->datetimePicker('#interrupt-scheduled-begin'),
        'class' => 'datepicker',
    ]
);
echo $this->Form->control(
    'scheduled__end',
    [
        'label' => __("Programmer la date de fin de l'interruption"),
        'type' => 'text',
        'append' => $this->Date->datetimePicker('#interrupt-scheduled-end'),
        'class' => 'datepicker',
    ]
);
echo $this->Form->control(
    'message',
    [
        'label' => __("Message à afficher aux utilisateurs"),
        'type' => 'textarea',
        'class' => 'mce mce-small',
    ]
);

echo $this->Form->fieldset(
    $this->Form->control(
        'periodic__begin',
        [
            'label' => __("Date de début de l'interruption journalière"),
            'options' => $hours,
            'empty' => __("-- Sélectionner une heure --"),
        ]
    )
    . $this->Form->control(
        'periodic__end',
        [
            'label' => __("Date de fin de l'interruption journalière"),
            'options' => $hours,
            'empty' => __("-- Sélectionner une heure --"),
        ]
    )
    . $this->Form->control(
        'message_periodic',
        [
            'label' => __("Message lors de l'interruption journalière"),
            'type' => 'textarea',
            'class' => 'mce mce-small',
        ]
    ),
    ['legend' => __("Interruption journalière")]
);

echo $this->Form->fieldset(
    $this->Form->control(
        'enable_whitelist',
        [
            'label' => __("Permettre aux webservices de continuer à accéder à l'application")
        ]
    )
    . $this->Form->control(
        'whitelist_headers_inline',
        [
            'label' => __("Entêtes identifiant les webservices autorisés (séparés par une virgule(,))")
        ]
    ),
    ['legend' => __("Autorisation des webservices")]
);

echo $this->Form->submit();
echo $this->Form->end();
?>

<script>
    var interruptModal = $('#service-interruption'),
        interruptForm = interruptModal.find('form');
    interruptForm.submit(function(event) {
        event.preventDefault();
        $.ajax({
            url: interruptForm.attr('action'),
            method: 'POST',
            data: interruptForm.serialize(),
            success: function(content, textStatus, jqXHR) {
                switch (jqXHR.getResponseHeader('X-Asalae-Success')) {
                    case 'true':
                        interruptModal.modal("hide");
                        break;
                    case 'false':
                        interruptModal.find('div.modal-body').html(content);
                        break;
                }
            },
            error: function() {
                alert(PHP.messages.genericError);
            }
        });
    });

    new AsalaeMce({selector: 'textarea.mce-small'});
</script>
