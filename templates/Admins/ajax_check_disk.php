<?php

/**
 * @var Versae\View\AppView $this
 * @var array $disks
 */

$data = [];
$keys = [
    'filesystem',
    'size',
    'used',
    'avail',
    'use',
    'mounted_on'
];

for ($i = 1; $i < count($disks); $i++) {
    $data[] = array_combine($keys, preg_split('/\s+/', $disks[$i], 6));
}

echo $this->Table->create('disk-state')
    ->fields(
        [
            'filesystem' => ['label' => __("Filesystem")],
            'size' => ['label' => __("Taille")],
            'used' => ['label' => __("Utilisé")],
            'avail' => ['label' => __("Disponible")],
            'use' => ['label' => __("Utilisé en %")],
            'mounted_on' => ['label' => __("Monté sur")]
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'mounted_on',
        ]
    )
    ->generate();
