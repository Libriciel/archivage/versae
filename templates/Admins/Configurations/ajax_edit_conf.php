<?php

/**
 * @var Versae\View\AppView $this
 * @var EntityInterface $se
 * @var AsalaeCore\Form\ConfigurationForm $form
 */

?>
<!--suppress RequiredAttributes -->
<script>
    function urlToImg(value) {
        if (value && value.substr(0, 4) !== '<img') {
            return $('<img alt="prev">')
                .attr('src', value)
                .css({
                    "max-width": "160px",
                    "max-height": "160px"
                });
        }
        return value;
    }

    function selectableUploadRadio(value, context) {
        if (!AsalaeGlobal.is_numeric(context.id) && context.id !== 'local') {
            return '';
        }
        var input = $('<input type="radio" name="fileuploads[id]">').val(context.id);
        setTimeout(function () {
            $('#config-upload-logo-table')
                .find('input[name="fileuploads[id]"]')
                .prop('checked', false)
                .first()
                .prop('checked', true);
        }, 0);
        return input;
    }
</script>
<?php
use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;

$configArray = json_decode($config, true);

// Permet d'afficher "valeur custom" si la valeur n'est pas dans la liste
$passwordComplexityDefault = Hash::get($configArray, 'Password.complexity');
if ($passwordComplexityDefault && !in_array($passwordComplexityDefault, array_keys($password_complexity))) {
    $passwordComplexityDefault = -1;
}
$passwordAdminComplexityDefault = Hash::get($configArray, 'Password.admin.complexity');
if ($passwordAdminComplexityDefault && !in_array($passwordAdminComplexityDefault, array_keys($password_complexity))) {
    $passwordAdminComplexityDefault = -1;
}

$defaultEncodeFilename = Hash::get($default, 'DataCompressorUtility.encodeFilename');
if ($defaultEncodeFilename === false) {
    $defaultEncodeFilename = $datacompressor_encodefilename['0'];
}
$defaultUseShred = Hash::get($default, 'FilesystemUtility.useShred');
if ($defaultUseShred !== null) {
    $defaultUseShred = $filesystem_useshred[(int)Hash::get($default, 'FilesystemUtility.useShred')];
}

$view = $this;

$tabs = $this->Tabs->create('tabs-edit-configuration', ['class' => 'row no-padding']);

echo $this->Form->create($form, ['id' => 'config-editor']);

echo '<label class="hide">autocomplete obliterator'
    . '<input type="password" value="false"><!-- neutralise le préremplissage firefox --></label>';

$tabs->add(
    'tab-edit-configuration-utility',
    $this->Fa->i('fa-cogs', __x('tab', "Utilitaires")),
    // -----------------------------------------------------------------------------
    $this->Form->control(
        'app_fullbaseurl',
        [
            'label' => __("Url de base de l'application"),
            'default' => Hash::get($configArray, 'App.fullBaseUrl'),
            'placeholder' => 'https://versae.fr',
            'required'
        ]
    )

    . $this->Form->control(
        'ignore_invalid_fullbaseurl',
        [
            'label' => __("Ignore les erreurs sur l'url de base de l'application"),
            'default' => Hash::get($configArray, 'App.ignore_invalid_fullbaseurl'),
            'type' => 'checkbox',
        ]
    )

    . $this->Form->control(
        'hash_algo',
        [
            'label' => __("algorithme de hashage"),
            'default' => Hash::get($configArray, 'hash_algo'),
            'options' => $hash_algo,
            'empty' => __("-- Utiliser la valeur par défaut ({0}) --", Hash::get($default, 'hash_algo')),
        ]
    )

    . $this->Form->control(
        'password_complexity_select',
        [
            'label' => __("Complexité du mot de passe"),
            'default' => $passwordComplexityDefault,
            'options' => $password_complexity,
            'empty' => __(
                "-- Utiliser la valeur par défaut ({0}) --",
                Hash::get($default, 'Password.complexity')
            ),
        ]
    )

    . $this->Form->control(
        'password_complexity',
        [
            'label' => false,
            'default' => Hash::get($configArray, 'Password.complexity'),
            'type' => 'number',
            'min' => 0,
            'step' => 1
        ]
    )

    . $this->Form->control(
        'password_admin_complexity_select',
        [
            'label' => __("Complexité du mot de passe pour l'administration technique"),
            'default' => $passwordAdminComplexityDefault,
            'options' => $password_complexity,
            'empty' => __(
                "-- Utiliser la valeur par défaut ({0}) --",
                Hash::get($default, 'Password.admin.complexity')
            ),
        ]
    )

    . $this->Form->control(
        'password_admin_complexity',
        [
            'label' => false,
            'default' => Hash::get($configArray, 'Password.admin.complexity'),
            'type' => 'number',
            'min' => 0,
            'step' => 1
        ]
    )

    . $this->Form->control(
        'datacompressor_encodefilename',
        [
            'label' => __("Encodage du nom des fichiers contenus dans les fichiers ZIP"),
            'default' => Hash::get($configArray, 'DataCompressorUtility.encodeFilename'),
            'options' => $datacompressor_encodefilename,
            'empty' => __("-- Utiliser la valeur par défaut ({0}) --", $defaultEncodeFilename),
        ]
    )

    . $this->Form->control(
        'filesystem_useshred',
        [
            'label' => __("Utiliser shred pour la destruction des fichiers"),
            'default' => Hash::get($configArray, 'FilesystemUtility.useShred'),
            'options' => $filesystem_useshred,
            'empty' => __(
                "-- Utiliser la valeur par défaut ({0}) --",
                $defaultUseShred ? $filesystem_useshred[1] : $filesystem_useshred[0]
            ),
        ]
    )

    . $this->Form->control(
        'paginator_limit',
        [
            'label' => __("Nombre de résultats par pages par défaut"),
            'default' => Hash::get($configArray, 'Pagination.limit', 20),
            'type' => 'number',
            'step' => 1,
            'min' => 1,
            'max' => 200,
            'help' => __("Une valeur élevée a un impacte négatif sur les performances")
        ]
    )

    . $this->Form->control(
        'ajaxpaginator_limit',
        [
            'label' => __("Nombre de résultats par pages dans les modales"),
            'default' => Hash::get($configArray, 'AjaxPaginator.limit', 100),
            'type' => 'number',
            'step' => 1,
            'min' => 1,
            'max' => 1000,
        ]
    )

    . $this->Form->control(
        'downloads_asyncfilecreationtimelimit',
        [
            'label' => __(
                "Durée maximale de temps de création de zip avant de "
                . "proposer un téléchargement asynchrone (secondes)"
            ),
            'default' => Hash::get($configArray, 'Downloads.asyncFileCreationTimeLimit'),
            'placeholder' => 30,
            'min' => 1,
            'max' => 240
        ]
    )

    . $this->Form->control(
        'downloads_tempfileconservationtimelimit',
        [
            'label' => __(
                "Temps de conservation des fichiers temporaires disponibles au téléchargement (heures)"
            ),
            'default' => Hash::get($configArray, 'Downloads.tempFileConservationTimeLimit'),
            'placeholder' => 48,
            'min' => 1,
            'max' => 24 * 30
        ]
    )

    . '<hr>'

    . $this->Form->control(
        'proxy_host',
        [
            'label' => __("Host du proxy"),
            'default' => Hash::get($configArray, 'Proxy.host'),
            'placeholder' => 'proxy.exemple.com',
            'min' => 1,
            'max' => 180
        ]
    )
    . $this->Form->control(
        'proxy_port',
        [
            'label' => __("Port du proxy"),
            'default' => Hash::get($configArray, 'Proxy.port'),
            'placeholder' => 8080,
            'min' => 1,
            'max' => 65535
        ]
    )
    . $this->Form->control(
        'proxy_username',
        [
            'label' => __("Utilisateur de connexion du proxy"),
            'default' => Hash::get($configArray, 'Proxy.username'),
        ]
    )
    . $this->Form->control(
        'proxy_password',
        [
            'label' => __("Mot de passe de connexion du proxy"),
            'default' => Hash::get($configArray, 'Proxy.password'),
            'placeholder' => '******************',
            'type' => 'password',
        ]
    )
);

$tabs->add(
    'tab-edit-configuration-style',
    $this->Fa->i('fa-picture-o', __x('tab', "Apparence")),
    // -----------------------------------------------------------------------------
    $this->Html->tag(
        'div.form-group.fake-input',
        $this->Html->tag('span.fake-label', __("Logo sur la page de login"))
        . $this->Upload
            ->create('config-upload-logo', ['class' => 'table table-striped table-hover hide'])
            ->fields(
                [
                    'select' => [
                        'label' => __("Sélectionner"),
                        'class' => 'radio-td',
                        'callback' => 'selectableUploadRadio',
                    ],
                    'prev' => ['label' => __("Prévisualiser"), "callback" => "urlToImg", 'escape' => false],
                    'name' => ['label' => __("Nom de fichier"), 'callback' => 'TableHelper.wordBreak()'],
                    'size' => ['label' => __("Taille"), 'callback' => 'TableHelper.readableBytes'],
                    'message' => ['label' => __("Message"), 'style' => 'min-width: 100px', 'class' => 'message'],
                ]
            )
            ->data($uploadFiles)
            ->params(
                [
                    'identifier' => 'id',
                    'classEval' => 'data[{index}].valid !== false ? "" : "danger"',
                ]
            )
            ->actions(
                [
                    function ($table) use ($view) {
                        $url = $view->Url->build('/Upload/delete');
                        return [
                            'onclick' => "GenericUploader.fileDelete($table->tableObject, '$url', {0})",
                            'type' => 'button',
                            'class' => 'btn-link delete',
                            'displayEval' => 'AsalaeGlobal.is_numeric(data[{index}].id)',
                            'label' => $view->Fa->charte('Supprimer', '', 'text-danger'),
                            'title' => __("Supprimer {0}", '{1}'),
                            'aria-label' => __("Supprimer {0}", '{1}'),
                            'params' => ['id', 'name']
                        ];
                    },
                    function ($table) use ($view, $se) {
                        $url = $view->Url->build('/Admins/deleteLogo');
                        $id = $se ? $se->get('id') : null;
                        return [
                            'onclick' => "deleteLogoClient($table->tableObject, '$url', $id)",
                            'type' => 'button',
                            'class' => 'btn-link delete',
                            'displayEval' => 'data[{index}].webroot',
                            'label' => $view->Fa->charte('Supprimer', '', 'text-danger'),
                            'title' => __("Supprimer {0}", '{1}'),
                            'aria-label' => __("Supprimer {0}", '{1}'),
                            'params' => ['id', 'name']
                        ];
                    }
                ]
            )
            ->generate(
                [
                    'attributes' => ['accept' => 'image/*'],
                    'allowDuplicateUploads' => true,
                    'target' => $this->Url->build('/upload/index/?replace=true')
                ]
            )
    )

    . $this->Form->control(
        'libriciel_login_background',
        [
            'label' => __("Couleur de fond du logo pour la page de login"),
            'default' => ($style = Hash::get($configArray, 'Libriciel.login.logo-client.style'))
                ? substr($style, strpos($style, ': ') + 2)
                : '#337ab7',
            'type' => 'color'
        ]
    )
);

$tabs->add(
    'tab-edit-configuration-email',
    $this->Fa->i('fa-envelope', __x('tab', "E-mails")),
    // -----------------------------------------------------------------------------
    $this->Form->control(
        'emails_from',
        [
            'label' => __("Champ from"),
            'default' => Hash::get($configArray, 'Email.default.from'),
            'placeholder' => 'contact@versae.fr'
        ]
    )

    . '<hr>'

    . $this->Form->control(
        'emails_class',
        [
            'label' => __("Méthode d'envoi"),
            'data-placeholder' => __("-- Choisir une méthode --"),
            'options' => [
                [
                    'text' => 'Mail',
                    'value' => 'Mail',
                    'title' => __("Send using PHP mail function"),
                ],
                [
                    'text' => 'Smtp',
                    'value' => 'Smtp',
                    'title' => __("Send using SMTP"),
                ],
            ],
            'default' => Hash::get($configArray, 'EmailTransport.default.className'),
        ]
    )

    . $this->Form->control(
        'emails_host',
        [
            'label' => __("Hôte"),
            'default' => Hash::get($configArray, 'EmailTransport.default.host'),
            'placeholder' => 'localhost'
        ]
    )

    . $this->Form->control(
        'emails_port',
        [
            'label' => __("Port"),
            'default' => Hash::get($configArray, 'EmailTransport.default.port'),
            'placeholder' => '25',
            'min' => 1,
            'step' => 1,
            'max' => 65535
        ]
    )

    . $this->Form->control(
        'emails_username',
        [
            'label' => __("Nom d'utilisateur"),
            'default' => Hash::get($configArray, 'EmailTransport.default.username'),
            'placeholder' => 'contact'
        ]
    )

    . $this->Form->control(
        'emails_password',
        [
            'label' => __("Password"),
            'default' => Hash::get($configArray, 'EmailTransport.default.password'),
            'placeholder' => '*************',
            'type' => 'password'
        ]
    )

    . $this->Html->tag(
        'button.btn.btn-warning',
        $this->Fa->i('fa-envelope-o', __("Envoyer un mail de test")),
        ['onclick' => 'sendTestMail()', 'type' => 'button']
    )
);

$uploads = $this->Upload
    ->create('upload-seda-generator-cert', ['class' => 'table table-striped table-hover hide'])
    ->fields(
        [
            'info' => [
                'label' => __("Fichier"),
                'target' => '',
                'thead' => [
                    'filename' => [
                        'label' => __("Nom de fichier"),
                        'callback' => 'TableHelper.wordBreak()',
                    ],
                    'hash' => [
                        'label' => __("Empreinte du fichier"),
                        'callback' => 'TableHelper.wordBreak("_", 32)',
                    ],
                    'size' => [
                        'label' => __("Taille"),
                        'callback' => 'TableHelper.readableBytes',
                    ],
                ],
            ],
            'message' => [
                'label' => __("Message"),
                'style' => 'width: 400px',
                'class' => 'message',
            ],
        ]
    )
    ->data($sedaSslUploadData ?? [])
    ->params(
        [
            'identifier' => 'id',
            'classEval' => 'data[{index}].valid !== false ? "" : "danger"',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "$('.seda-generator-cafile').val('{0}')",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-check-square'),
                'title' => $title = __("Sélectionner {0} comme certificat", '{1}'),
                'aria-label' => $title,
                'params' => ['path', 'name']
            ],
            function ($table) {
                $deleteUrl = $this->Url->build('/upload/delete');
                return [
                    'onclick' => "TableGenericAction.deleteAction($table->tableObject, '$deleteUrl')({0})",
                    'type' => 'button',
                    'class' => 'btn-link',
                    'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'params' => ['id', 'name']
                ];
            },
        ]
    );

$tabs->add(
    'tab-edit-configuration-editor',
    $this->Fa->i('fa-file-text', __x('tab', "Editeur de texte")),
    // -----------------------------------------------------------------------------
    $this->Html->tag(
        'p',
        __("Les valeurs du json seront écrasées par les champs saisis dans les autres onglets"),
        ['class' => 'alert alert-warning']
    )

    . $this->Html->tag(
        'p',
        __("Fichier de configuration : {0}", $appLocalConfigFileUri),
        ['class' => 'alert alert-warning']
    )

    . $this->Form->control(
        'config',
        [
            'label' => __("Configuration"),
            'rows' => 50,
            'default' => $config,
            'style' => 'font-family: monospace',
            'spellcheck' => 'false'
        ]
    )
);

echo $tabs . $this->Form->end();
?>
<script>
    /**
     * Colore en rouge le onglets en erreur, et selectionne le premier
     */
    AsalaeGlobal.colorTabsInError();

    $('#password-complexity-select').change(function () {
        var value = $(this).val(),
            passwordComplexity = $('#password-complexity');
        if (value === '-1') {
            value = '';
            passwordComplexity.focus();
        }
        passwordComplexity.val(value);
    });
    $('#password-admin-complexity-select').change(function () {
        var value = $(this).val(),
            passwordComplexity = $('#password-admin-complexity');
        if (value === '-1') {
            value = '';
            passwordComplexity.focus();
        }
        passwordComplexity.val(value);
    });


    /**
     * Suppression d'un background-image local (!= TableGenericAction.deleteAction)
     * @param table
     * @param url
     * @param id
     */
    function deleteLogoClient(table, url, id) {
        var useConfirm = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
        if (useConfirm && !confirm(__("Êtes-vous sûr de vouloir supprimer cet enregistrement ?"))) {
            return;
        }
        $.ajax({
            url: url.replace(/\/$/, '') + '/' + id,
            method: 'DELETE',
            success: function (message) {
                if (message.report === 'done') {
                    table.removeDataId('local');
                    $(table.table)
                        .find('tr[data-id="local"]')
                        .fadeOut(400, function() {
                            $(this).remove();
                        });
                } else {
                    throw message.report;
                }
            },
            error: function () {
                $(table.table).find('tr[data-id="' + id + '"]').addClass('danger');
            }
        });
    }

    function sendTestMail() {
        var email = prompt(
            __("Veuillez entrer le mail de destination")<?=$defaultEmail ? ', "' . $defaultEmail . '"' : ''?>
        );
        if (email) {
            var input = $('<input type="email" name="email">').val(email);

            if (!input.is(':valid')) {
                alert(__("Email invalide"));
                return;
            }

            var form = $('<form>');
            $('#tab-edit-configuration-email').find('select, input').each(
                function() {
                    form.append($(this).clone());
                }
            );

            $.ajax({
                url: '<?=$this->Url->build('/admins/sendTestMail')?>',
                method: 'POST',
                data: form.append(input).serialize(),
                headers: {
                    Accept: 'application/json'
                },
                success: function (content, textStatus, jqXHR) {
                    if (jqXHR.getResponseHeader('X-Email-Debug') === 'true') {
                        window.open(content, '_blank');
                        return;
                    }
                    if (content.success) {
                        alert(__("L'email de test a bien été envoyé"));
                    } else {
                        return this.error();
                    }
                },
                error: function () {
                    alert(PHP.messages.genericError);
                }
            })
        }
    }

    function pingSeda(btn) {
        var form = $(btn).closest('form');
        $(btn).disable();
        $.ajax({
            url: '<?=$this->Url->build('/admins/sedageneratorPing')?>',
            method: 'POST',
            data: form.serialize(),
            success: function (content) {
                if (content.success) {
                    alert('pong!');
                } else {
                    alert(content.error);
                }
            },
            error: function (e) {
                if (e.responseText) {
                    alert(e.responseText);
                } else {
                    alert(e);
                }
            },
            complete: function () {
                $(btn).enable();
            }
        })
    }

    $('#upload-seda-generator-cert').on('fileSuccess', function (event, file, data) {
        $('.seda-generator-cafile').val(data.report.path);
    });
</script>
