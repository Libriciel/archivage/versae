<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Core\Configure;

$debug = Configure::read('debug') ? ' checked="checked"' : '';

echo $this->element('jsFormTemplates');

$loading = $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);
$genericVignette = [
    'class' => 'cursor-pointer vignette bg-white',
    'role' => 'button',
    'tabindex' => '0',
    'onkeydown' => 'handleModalKeypress(event)'
];
$actualiser = $this->Fa->charteBtn(
    'Actualiser',
    $title = __("Actualiser"),
    ['icon' => 'cursor-pointer', 'class' => 'btn-link refresh', 'title' => $title]
);

echo $this->Html->tag(
    'div.container.sr-only',
    $this->Html->tag('h1', __d('admins', "Administration technique"))
);
echo $this->Html->tag('div#probe-div.container', '', ['style' => 'position: relative']);

/**
 * Liste des vignettes
 */
echo $this->Html->tag('div.container.container-flex.check-container');
echo $this->Html->tag('div.row');

/**
 * System check
 */
if ($system) {
    $icon = $this->Html->tag(
        'div.icon',
        $this->Fa->i('fa-spinner faa-spin animated'),
        ['class' => 'text-center loading-container']
    );
    $class = 'loading';
} else {
    $icon = $this->Html->tag('div.icon', $this->Fa->i('fa-times'));
    $class = 'danger';
}
echo $this->ModalView
    ->create('check-app', ['size' => 'large'])
    ->modal(
        'System check' . ' ' . $actualiser,
        $loading,
        ['class' => 'no-padding']
    )
    ->output(
        'div',
        $icon
        . $this->Html->tag('div.text', __d('admins', "System check")),
        '/admins/ajax-check'
    )
    ->generate(
        ['class' => 'cursor-pointer vignette ' . $class]
        + $genericVignette
    );

/**
 * Debug
 */
echo $this->Html->tag(
    'div#debug-section.cursor-pointer.vignette.bg-white',
    $this->Html->tag('div.icon', $this->Fa->i('fa-bug'))
    . $this->Html->tag(
        'div.text.no-reformat',
        __d('admins', "Debug") . '<br>'
        /**
         * Bouton slider
         */
        . $this->Html->tag(
            'label#toggle-debug.switch',
            $this->Html->tag('span.sr-only', __d('admins', "Activer / désactiver le debug"))
            . $this->Html->tag('input', '', ['type' => 'checkbox', 'checked' => Configure::read('debug')])
            . $this->Html->tag('span.slider.round', ''),
            ['tabindex' => 0]
        )
    )
);

/**
 * Service d'exploitation
 */
echo $this->ModalForm
    ->create('service-exploitation', ['size' => 'large'])
    ->modal(
        __("Entité du Service d'exploitation"),
        $loading
    )
    ->output(
        'div',
        $this->Html->tag(
            'div.icon',
            $this->Fa->i('fa-sitemap', '', ['v5' => false])
        )
        . $this->Html->tag('div.text', __d('admins', "Service d'exploitation")),
        '/admins/edit-service-exploitation'
    )
    ->generate($genericVignette);

/**
 * Sessions
 */
echo $this->ModalView
    ->create('sessions', ['size' => 'large'])
    ->modal(
        __d('admins', "Sessions actives") . ' ' . $actualiser,
        $loading,
        ['class' => 'no-padding']
    )
    ->output(
        'div',
        $this->Html->tag('div.icon', $this->Fa->i('fa-link'))
        . $this->Html->tag('div.text', __d('admins', "Sessions actives")),
        '/admins/index-sessions'
    )
    ->generate($genericVignette);

/**
 * Envoyer une notification
 */
echo $this->ModalForm
    ->create(
        'notify-all',
        [
            'size' => 'large',
            'acceptButton' => $this->Form->button(
                $this->Fa->charte('Envoyer', __("Envoyer")),
                ['bootstrap-type' => 'primary', 'class' => 'accept']
            )
        ]
    )
    ->modal(__("Envoyer une notification à tous les utilisateurs"))
    ->output(
        'div',
        $this->Html->tag('div.icon', $this->Fa->i('fa-bell'))
        . $this->Html->tag('div.text', __d('admins', "Envoyer une Notification")),
        '/admins/notify-all'
    )
    ->javascriptCallback('afterNotification')
    ->generate($genericVignette);

/**
 * Etat disques
 */
echo $this->ModalView
    ->create('check-disk', ['size' => 'large'])
    ->modal(
        __("Etat des disques") . ' ' . $actualiser,
        $loading,
        ['class' => 'no-padding']
    )
    ->output(
        'div',
        $this->Html->tag('div.icon', $this->Fa->i('fa-database'))
        . $this->Html->tag('div.text', __d('admins', "Etat disques")),
        '/admins/ajax-check-disk'
    )
    ->generate($genericVignette);

/**
 * Service d'archives
 */
echo $this->ModalView
    ->create('service-archive', ['size' => 'large'])
    ->modal(
        __d('admins', "Services d'Archives") . ' ' . $actualiser,
        $loading,
        ['class' => 'no-padding']
    )
    ->output(
        'div',
        $this->Html->tag('div.icon', $this->Fa->i('fa-sitemap', '', ['v5' => false]))
        . $this->Html->tag('div.text', __d('admins', "Services d'Archives")),
        '/admins/index-services-archives'
    )
    ->generate($genericVignette);

/**
 * Taches planifiés
 */
echo $this->ModalView
    ->create('crons', ['size' => 'large'])
    ->modal(
        __d('admins', "Tâches planifiées"),
        $loading
    )
    ->output(
        'div',
        $this->Html->tag('div.icon', $this->Fa->i('fa-clock-o'))
        . $this->Html->tag('div.text', __d('admins', "Tâches planifiées")),
        '/admins/index-crons'
    )
    ->generate($genericVignette);

/**
 * Interruption de service
 */
$cssClass = $genericVignette['class'];
if (Configure::read('Interruption.active')) {
    $cssClass .= ' warning';
}
echo $this->ModalForm
    ->create('service-interruption', ['size' => 'large'])
    ->modal(
        __("Interruption de service"),
        $loading
    )
    ->output(
        'div',
        $this->Html->tag('div.icon', $this->Fa->i('fa-ban'))
        . $this->Html->tag('div.text', __d('admins', "Interruption de service")),
        '/admins/ajax-interrupt'
    )
    ->javascriptCallback('afterInterruption')
    ->generate(['class' => $cssClass] + $genericVignette);

/**
 * Jobs
 */
echo $this->ModalView
    ->create('tasks-monitor', ['size' => 'large'])
    ->modal(
        __("Jobs et workers") . ' ' . $actualiser,
        $loading,
        ['class' => 'no-padding']
    )
    ->output(
        'div',
        $this->Html->tag('div.icon', $this->Fa->i('fa-tasks'))
        . $this->Html->tag('div.text', __d('admins', "Jobs")),
        '/admins/ajax-tasks'
    )
    ->generate($genericVignette);

/**
 * Logs
 */
echo $this->ModalView
    ->create('view-logs', ['size' => 'large'])
    ->modal('Logs' . ' ' . $actualiser, $loading)
    ->output(
        'div',
        $this->Html->tag('div.icon', $this->Fa->i('fa-file-text'))
        . $this->Html->tag('div.text', __d('admins', "Logs")),
        '/admins/ajax-logs'
    )
    ->generate($genericVignette);

/**
 * Annuaires LDAP
 */
echo $this->ModalView
    ->create('ldaps', ['size' => 'large'])
    ->modal(
        __d('admins', "Annuaires LDAP/AD"),
        $loading,
        ['class' => 'no-padding']
    )
    ->output(
        'div',
        $this->Html->tag('div.icon', $this->Fa->i('fa-address-book-o'))
        . $this->Html->tag('div.text', __d('admins', "Annuaires LDAP/AD")),
        '/admins/index-ldaps'
    )
    ->generate($genericVignette);

/**
 * Systèmes d'Archivage Electronique
 */
echo $this->ModalView
    ->create('archiving-systems', ['size' => 'large'])
    ->modal(
        __d('admins', "Systèmes d'Archivage Electronique"),
        $loading,
        ['class' => 'no-padding']
    )
    ->output(
        'div',
        $this->Html->tag('div.icon', $this->Fa->i('fa-external-link'))
        . $this->Html->tag('div.text', __d('admins', "Systèmes d'Archivage Electronique")),
        '/archiving-systems/index'
    )
    ->generate($genericVignette);

/**
 * Modifier la configuration
 */
echo $this->ModalForm
    ->create('edit-conf', ['size' => 'large'])
    ->modal(
        __d('admins', "Modifier la configuration"),
        $loading
    )
    ->output(
        'div',
        $this->Html->tag('div.icon', $this->Fa->i('fa-cogs'))
        . $this->Html->tag('div.text', __d('admins', "Modifier la configuration")),
        '/admins/ajax-edit-conf'
    )
    ->generate($genericVignette);

echo $this->Html->tag('/div');
echo $this->Html->tag('/div');
?>
<script>
    var asyncChecks = 0;
    var asyncChecksInError = false;

    /**
     * enter = click sur le debug
     */
    $('#toggle-debug').on('click, keydown', function(event) {
        if (event.type === 'keydown' && [13, 32].indexOf(event.originalEvent.keyCode) === -1) {
            return;
        }
        $(this).find('input').click();
    });
    $('#debug-section').find('input').change(function() {
        $.ajax({
            url: '<?=$this->Url->build('/Admins/ajax-toggle-debug')?>',
            type: 'POST',
            data: {state: $('#debug-section').find('input').prop('checked') ? 'true' : 'false'},
            headers: {
                Accepts: 'application/json'
            },
            success: function(content) {
                if (typeof content !== 'object') {
                    this.error();
                } else if (content.success === false) {
                    alert(content.error);
                }
            },
            error: function() {
                alert(PHP.messages.genericError);
            }
        });
    });

    /**
     * les divs se comporte comme un bouton (bouton enter = click)
     */
    function handleModalKeypress(event) {
        if ([13, 32].indexOf(event.keyCode) === -1) {
            return;
        }
        $(event.target).click();
    }

    /**
     * Colore le system-check
     */
    var checkedVerbs = [];
    function checkVerbs() {
        var checkVerbs = ['put', 'delete', 'patch', 'head'];
        for (var i = 0; i < checkVerbs.length; i++) {
            asyncChecks++;
            (function(i) {
                $.ajax({
                    url: '<?=$this->Url->build('/admins/ping')?>',
                    method: checkVerbs[i],
                    success: function() {
                        checkedVerbs.push(checkVerbs[i]);
                    },
                    error: function() {
                        asyncChecksInError = true;
                        console.error('HTTP method ' + checkVerbs[i] + ' failed');
                    },
                    complete: function() {
                        asyncChecks--;
                        checkAsyncCheck();
                    }
                });
            })(i);
        }
    }
    checkVerbs();
    asyncChecks++;
    $.ajax(
        {
            url: '<?=$this->Url->build('/admins/ajax-check-tree-sanity')?>',
            headers: {Accept: 'application/json'},
            success: function(content) {
                if (!content.tree_ok) {
                    this.error();
                }
            },
            error: function() {
                asyncChecksInError = true;
                console.error('tree sanity failed');
            },
            complete: function() {
                asyncChecks--;
                checkAsyncCheck();
            }
        }
    );

    asyncChecks++;
    var uuidSocket = uuid.v4();
    var websocketCheckTimeout = setTimeout(
        function () {
            asyncChecksInError = true;
            asyncChecks--;
            checkAsyncCheck();
            console.error('websocket failed');
        },
        <?=Configure::read('Ratchet.check_timeout', 1500)?>
    );
    AsalaeWebsocket.connect(
        '<?=Configure::read('Ratchet.connect')?>',
        function (session) {
            var unsubscribeTimeout = setTimeout(
                function () {
                    session.unsubscribe('ping_' + uuidSocket);
                },
                <?=Configure::read('Ratchet.check_timeout', 1500)?>
            );
            session.subscribe('ping_' + uuidSocket, function (topic, data) {
                session.unsubscribe('ping_' + uuidSocket);
                clearTimeout(websocketCheckTimeout);
                clearTimeout(unsubscribeTimeout);
                asyncChecks--;
                checkAsyncCheck();
            });
            setTimeout(
                function () {
                    $.ajax(
                        {
                            url: '<?=$this->Url->build('/admins/ajax-check-websocket')?>/' + uuidSocket
                        }
                    );
                },
                10
            );
        },
        function () {
        },
        {'skipSubprotocolCheck': true}
    );

    /**
     * Utilisé dans index_crons.php
     */
    function parseJsonData(value) {
        if (!value) {
            return;
        }
        var parsed = JSON.parse(value);
        var table = $('<table class="parsed-json-data"></table>');
        var tbody = $('<tbody></tbody>');
        table.append(tbody);
        var tr;
        for (var key in parsed) {
            tbody.append(
                $('<tr></tr>')
                    .append($('<th></th>').append(key+':'))
                    .append($('<td></td>').append(parsed[key]))
            );
        }
        return table;
    }

    /**
     * Utilisé dans index_jobs.php
     */
    function parseArrayData(value) {
        if (!value) {
            return;
        }
        var table = $('<table class="parsed-json-data"></table>');
        var tbody = $('<tbody></tbody>');
        table.append(tbody);
        var tr;
        for (var key in value) {
            tbody.append(
                $('<tr></tr>')
                    .append($('<th></th>').append(key+':'))
                    .append($('<td></td>').append(value[key]))
            );
        }
        return table;
    }

    /**
     * Donne le nombre de message envoyés
     */
    function afterNotification(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            if (content.count > 1) {
                alert(__("{0} messages ont été envoyés", content.count));
            } else if (content.count === 1) {
                alert(__("1 message a été envoyé"));
            } else {
                alert(__("aucun message n'a été envoyé"));
            }
        } else {
            alert(PHP.messages.genericError);
        }
    }

    /**
     * Défini la couleur de la vignette d'interruption (callback)
     */
    function afterInterruption(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            $.ajax({
                url: '<?=$this->Url->build('/')?>',
                error: function (jqXHR) {
                    if (jqXHR.getResponseHeader('X-Asalae-Interruption')) {
                        $('#div-service-interruption').addClass('warning');
                    } else {
                        $('#div-service-interruption').removeClass('warning');
                    }
                },
                success: function (content, textStatus, jqXHR) {
                    if (jqXHR.getResponseHeader('X-Asalae-Interruption')) {
                        $('#div-service-interruption').addClass('warning');
                    } else {
                        $('#div-service-interruption').removeClass('warning');
                    }
                }
            });
        }
    }

    /**
     * Calcule la taille que prend un texte (word) dans un contexte (element)
     * @var {string} word
     * @var {DOM|jQuery|string} element
     */
    function wordSizeCalculator(word, element) {
        var div = $('<div></div>').css({
            "position": 'absolute',
            "visibility": 'hidden',
            "height": 'auto',
            "width": 'auto',
            "white-space": 'nowrap'
        }).append(word);
        $(element).append(div);
        var size = {
            width: div.outerWidth(),
            height: div.outerHeight()
        };
        div.remove();
        return size;
    }

    /**
     * Formate les textes des vignettes pour une meilleure apparence
     */
    $('.vignette .text:not(.no-reformat)').each(function() {
        var words = $(this).text().trim().split(' ');
        var max = 0;
        var current = 0;
        var widths = [];
        var currentWidth;
        var small = $('<small></small>');
        var width;
        var br = false;
        var space = false;

        if (words.length === 1) {
            if (wordSizeCalculator(words[0], this).width > $(this).width()) {
                $(this).html(small.append(words[0]));
            }
            return;
        }
        for (var i = 0; i < words.length; i++) {
            currentWidth = wordSizeCalculator(words[i], this).width;
            widths.push(currentWidth);
            max = Math.max(max, currentWidth);
        }
        for (i = 0; i < words.length; i++) {
            if (br) {
                br = false;
                small.append('<br>');
                current = 0;
            } else if (i > 0) {
                small.append(' ');
            }
            width = widths.shift();
            current += width;
            if (current >= max) {
                if (i > 0) {
                    small.append('<br>');
                    current = 0;
                } else {
                    br = true;
                }
            }
            small.append(words[i]);
        }
        if (max > $(this).width()) {
            $(this).html(small);
        } else {
            $(this).html(small.html());
        }
    });

    function probe() {
        if (probePaused) {
            return;
        }
        $.ajax({
            url: '<?=$this->Url->build('/no-session-renew/admins/probe')?>',
            success: function (content) {
                $('#probe-div').html(content);
                refreshProbeIcon = $('#refresh-probe').find('i.fas');
            }
        });
    }
    var probePaused = false;
    probe();
    probePaused = <?=Configure::read('Admins.probePaused') ? 'true' : 'false'?>;
    var probeInterval = setInterval(probe, 5000);
    var refreshProbeIcon = $('#refresh-probe').find('i.fas');
    setTimeout(
        function() {
            refreshProbeIcon
                .toggleClass('fa-pause', !probePaused)
                .toggleClass('fa-play', probePaused)
        },
        100
    );
    function toogleProbeRefresh() {
        refreshProbeIcon
            .toggleClass('fa-pause', probePaused)
            .toggleClass('fa-play', !probePaused);
        probePaused = !probePaused;
    }

    var ctrlCount = 0;
    $(document).on('keydown', function (event) {
        if (event.keyCode === 17) { // Ctrl
            ctrlCount++;
        } else {
            ctrlCount = 0;
            return;
        }
        if (ctrlCount === 3) {
            toogleProbeRefresh();
        }
    });

    var ratchet = [];
    $('.modal').on('hide.bs.modal', function() {
        var keys = [];
        var i, j;
        for (i = 0; i < ratchet.length; i++) {
            keys = Object.keys(ratchet[i]._subscriptions);
            for (j = 0; j < keys.length; j++) {
                try {
                    ratchet[i].unsubscribe(keys[j]);
                } catch (e) {
                }
            }
        }
        ratchet = [];
    });

    if (typeof PHP === 'object') {
        var config = {
            element: '#notifications-container',
            elementUl: '#notifications-ul-container',
            token: 'admin',
            ajax: {
                deleteNotification: PHP.urls.deleteNotification
            }
        };
        var notifications = new AsalaeNotifications(config);
        notifications.connect('admin', PHP.configs.Ratchet.connect).insert(PHP.notifications);
    }

    function notifyMe(btn)
    {
        var inputs = $(btn).closest('form').serializeObject();
        notifications.session.publish(
            'user_admin_admin',
            {
                message: {
                    id: uuid.v4(),
                    user_id: 'admin',
                    text: '<h4>' + __("Message de l'administrateur") + '</h4>' + inputs.msg,
                    css_class: inputs.color,
                    created: Date.now()
                }
            }
        );
    }

    function checkAsyncCheck()
    {
        var checkVignette = $('#div-check-app');
        var checkVignetteIcon = checkVignette.find('.icon .fas');
        if (checkVignette.hasClass('danger')) {
            return;
        }
        if (asyncChecksInError) {
            checkVignette.removeClass('loading');
            checkVignetteIcon.removeClass('fa-spinner faa-spin animated');
            checkVignette.removeClass('success').addClass('danger');
            checkVignetteIcon.removeClass('fa-check').addClass('fa-times');
            return;
        }
        if (asyncChecks === 0) {
            checkVignette.removeClass('loading');
            checkVignetteIcon.removeClass('fa-spinner faa-spin animated');
            if (asyncChecksInError) {
                checkVignette.removeClass('success').addClass('danger');
                checkVignetteIcon.removeClass('fa-check').addClass('fa-times');
            } else {
                checkVignette.removeClass('danger').addClass('success');
                checkVignetteIcon.removeClass('fa-times').addClass('fa-check');
            }
        }
    }

    asyncChecks++;
    $.ajax(
        {
            url: '<?=$this->Url->build('/admins/ajax-check-archiving-system')?>',
            headers: {Accept: 'application/json'},
            success: function (content, textStatus, jqXHR) {
                if (jqXHR.status === 202) {
                    $(content.failed).each((k, v) => console.error("test failed on " + v.name));
                    this.error();
                }
            },
            error: function() {
                asyncChecksInError = true;
                console.error('archiving-system failed');
            },
            complete: function() {
                asyncChecks--;
                checkAsyncCheck();
            }
        }
    );
</script>
