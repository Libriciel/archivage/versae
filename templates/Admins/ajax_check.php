<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Core\Configure;
use Cake\Utility\Hash;
use Versae\Utility\Check;

$ok = $this->Fa->i('fa-check-circle pull-right text-success no-margin');
$warning = $this->Fa->i('fa-times-circle pull-right text-warning no-margin');
$bad = $this->Fa->i('fa-times-circle pull-right text-danger no-margin');

$configErrors = Check::configurationErrors();
$missings = Check::missings();
$missingsExtPhp = Check::missingsExtPhp();
$database = Check::database();
$beanstalkd = Check::beanstalkd();
$ratchet = Check::ratchet();
$clamav = Check::clamav();
$php = Check::php();
$writables = Check::writables();
$sedaGenerator = Check::sedaGeneratorStatus();
$treeSanity = Check::treeSanity();
$systemOk = empty($missings) && empty($missingsExtPhp)
    && $database && $ratchet && $clamav && $beanstalkd && !$configErrors
    && Check::phpOk() && Check::writableOk()
    && $sedaGenerator['success'] && Check::ArchivingSystemsOK()
    && Check::treeSanityOk();

// Serveurs
echo $this->Html->tag(
    'section',
    $this->Html->tag('h3', 'Serveurs', ['class' => 'h4']),
    ['class' => 'modal-header']
);
echo $this->Html->tag('table', null, ['class' => 'table table-striped table-hover no-padding'])
    . $this->Html->tag(
        'thead',
        $this->Html->tag(
            'tr',
            $this->Html->tag('th', 'Serveur')
            . $this->Html->tag('th', 'Etat', ['class' => 'text-right'])
        )
    )
    . $this->Html->tag('tbody');

echo $this->Html->tag(
    'tr',
    $this->Html->tag('td', __("Base de données"))
    . $this->Html->tag('td', $database ? $ok : $bad, ['class' => 'h4']),
    $database ? [] : ['class' => 'danger']
);

echo $this->Html->tag(
    'tr',
    $this->Html->tag('td', __("Serveur de jobs"))
    . $this->Html->tag('td', $beanstalkd ? $ok : $bad, ['class' => 'h4']),
    $beanstalkd ? [] : ['class' => 'danger']
);

echo $this->Html->tag(
    'tr',
    $this->Html->tag('td', __("Serveur de notifications"))
    . $this->Html->tag('td', $ratchet ? $ok : $bad, ['class' => 'h4', 'id' => 'td-websocket']),
    $ratchet ? [] : ['class' => 'danger']
);

echo $this->Html->tag(
    'tr',
    $this->Html->tag('td', __("Antivirus"))
    . $this->Html->tag('td', $clamav ? $ok : $bad, ['class' => 'h4']),
    $database ? [] : ['class' => 'danger']
);

echo $this->Html->tag('/tbody') . $this->Html->tag('/table');

// PHP
echo $this->Html->tag(
    'section',
    $this->Html->tag('h3', __("Paramétrage PHP"), ['class' => 'h4'])
    . $this->Html->link(
        'php_info',
        '/admins/php-info',
        ['target' => '_blank']
    ),
    ['class' => 'modal-header']
);
echo $this->Html->tag('table', null, ['class' => 'table table-striped table-hover no-padding'])
    . $this->Html->tag(
        'thead',
        $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Variable"))
            . $this->Html->tag('th', __("Valeur"))
            . $this->Html->tag('th', __("Etat"), ['class' => 'text-right'])
        )
    )
    . $this->Html->tag('tbody');

foreach ($php as $var => $value) {
    echo $this->Html->tag(
        'tr',
        $this->Html->tag('td', $var)
        . $this->Html->tag('td', ini_get($var))
        . $this->Html->tag('td', $value ? $ok : $bad, ['class' => 'h4']),
        $value ? [] : ['class' => 'danger']
    );
}

echo $this->Html->tag('/tbody') . $this->Html->tag('/table');

// Config
echo $this->Html->tag(
    'section',
    $this->Html->tag('h3', __("Paramétrage de l'application"), ['class' => 'h4']),
    ['class' => 'modal-header']
);

$formatedConfigErrors = '';
if ($configErrors) {
    $formatedConfigErrors .= $this->Html->tag('ul');
    foreach ($configErrors as $field => $errors) {
        foreach (Hash::flatten($errors) as $error) {
            $formatedConfigErrors .= $this->Html->tag('li', $field . ' : ' . $error);
        }
    }
    $formatedConfigErrors .= $this->Html->tag('/ul');
}

echo $this->Html->tag('table', null, ['class' => 'table table-striped table-hover no-padding'])
    . $this->Html->tag(
        'thead',
        $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Validation"))
            . $this->Html->tag('th', __("Erreurs"))
            . $this->Html->tag('th', __("Etat"), ['class' => 'text-right'])
        )
    )
    . $this->Html->tag('tbody')
    . $this->Html->tag(
        'tr',
        $this->Html->tag('td', __("Configuration de l'application"))
        . $this->Html->tag('td', $formatedConfigErrors)
        . $this->Html->tag('td', !$configErrors ? $ok : $bad, ['class' => 'h4']),
        $value ? [] : ['class' => 'danger']
    )
    . $this->Html->tag('/tbody')
    . $this->Html->tag('/table');

// Programmes
echo $this->Html->tag(
    'section',
    $this->Html->tag('h3', __("Installation des modules externes"), ['class' => 'h4']),
    ['class' => 'modal-header']
);
echo $this->Html->tag(
    'table',
    null,
    ['class' => 'table table-striped table-hover no-padding']
)
    . $this->Html->tag(
        'thead',
        $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Programmes"))
            . $this->Html->tag('th', __("Version"))
            . $this->Html->tag('th', 'Etat', ['class' => 'text-right'])
        )
    )
    . $this->Html->tag('tbody');

$prerequis = Configure::read('Check.prerequis', Check::$prerequis);
foreach (array_keys(Hash::normalize($prerequis)) as $function) {
    $in = in_array($function, $missings);
    echo $this->Html->tag('tr', null, $in ? ['class' => 'danger'] : []);
    echo $this->Html->tag('td', $function);
    $version = null;
    if (isset(Check::$prerequisVersion[$function])) {
        $cmd = Check::$prerequisVersion[$function]['command'];
        $output = null;
        $m = null;
        exec($cmd, $output);
        preg_match(Check::$prerequisVersion[$function]['regex'], implode("\n", $output), $m);
        if ($m && isset($m[1])) {
            $version = true;
            echo $this->Html->tag('td', $m[1]);
        }
    }
    echo $this->Html->tag(
        'td',
        $in ? $bad : $ok,
        ['class' => 'h4', 'colspan' => $version ? 1 : 2]
    );
}

echo $this->Html->tag('/tbody') . $this->Html->tag('/table');

// Classes PHP
echo $this->Html->tag(
    'section',
    $this->Html->tag('h3', __("Installation des modules PHP"), ['class' => 'h4']),
    ['class' => 'modal-header']
);
echo $this->Html->tag('table', null, ['class' => 'table table-striped table-hover no-padding'])
    . $this->Html->tag(
        'thead',
        $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Modules PHP"))
            . $this->Html->tag('th', __("Etat"), ['class' => 'text-right'])
        )
    )
    . $this->Html->tag('tbody');

foreach (array_keys(Hash::normalize(Check::extPhp())) as $className) {
    $in = in_array($className, $missingsExtPhp);
    echo $this->Html->tag(
        'tr',
        $this->Html->tag('td', $className)
        . $this->Html->tag(
            'td',
            $in ? $bad : $ok,
            ['class' => 'h4']
        ),
        $in ? ['class' => 'danger'] : []
    );
}

echo $this->Html->tag('/tbody') . $this->Html->tag('/table');


// Verbes HTTP
echo $this->Html->tag(
    'section',
    $this->Html->tag('h3', __("Méthodes de requête HTTP"), ['class' => 'h4']),
    ['class' => 'modal-header']
);
echo $this->Html->tag('table', null, ['class' => 'table table-striped table-hover no-padding'])
    . $this->Html->tag(
        'thead',
        $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Méthodes"))
            . $this->Html->tag('th', __("Etat"), ['class' => 'text-right'])
        )
    )
    . $this->Html->tag('tbody');

foreach (['put', 'delete', 'patch', 'head'] as $verb) {
    echo $this->Html->tag(
        'tr',
        $this->Html->tag('td', $verb)
        . $this->Html->tag(
            'td',
            $this->Html->tag('i', '', ['class' => 'fa fa-spinner faa-spin animated pull-right']),
            ['id' => 'td-method-http-' . $verb, 'class' => 'h4']
        )
    );
}

echo $this->Html->tag('/tbody') . $this->Html->tag('/table');

// accès aux dossiers en écriture
echo $this->Html->tag(
    'section',
    $this->Html->tag('h3', __("Dossiers inscriptibles"), ['class' => 'h4']),
    ['class' => 'modal-header']
);
echo $this->Html->tag('table', null, ['class' => 'table table-striped table-hover no-padding'])
    . $this->Html->tag(
        'thead',
        $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Dossiers"))
            . $this->Html->tag('th', __("Etat"), ['class' => 'text-right'])
        )
    )
    . $this->Html->tag('tbody');

foreach ($writables as $dir => $access) {
    echo $this->Html->tag(
        'tr',
        $this->Html->tag('td', $dir)
        . $this->Html->tag('td', $access ? $ok : $bad, ['class' => 'h4']),
        $access ? [] : ['class' => 'danger']
    );
}

echo $this->Html->tag('/tbody') . $this->Html->tag('/table');

// Seda Generator et Archiving Systems
echo $this->Html->tag(
    'section',
    $this->Html->tag('h3', __("Services Externes"), ['class' => 'h4']),
    ['class' => 'modal-header']
);
echo $this->Html->tag('table', null, ['class' => 'table table-striped table-hover no-padding'])
    . $this->Html->tag(
        'thead',
        $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Seda Generator"))
            . $this->Html->tag('th', __("Message"))
            . $this->Html->tag('th', __("Etat"), ['class' => 'text-right'])
        )
    )
    . $this->Html->tag('tbody');

echo $this->Html->tag(
    'tr',
    $this->Html->tag('td', __("Seda Generator"))
    . $this->Html->tag('td', h($sedaGenerator['info']) ?? '')
    . $this->Html->tag(
        'td',
        $sedaGenerator['success'] ? $ok : ($sedaGenerator['warning'] ? $warning : $bad)
    ),
    $sedaGenerator['success'] ? [] : ['class' => 'danger']
);
echo $this->Html->tag('/tbody') . $this->Html->tag('/table');

echo $this->Html->tag('table', null, ['class' => 'table table-striped table-hover no-padding'])
    . $this->Html->tag(
        'thead',
        $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Systèmes d'Archivage Électronique actifs"))
            . $this->Html->tag('th', __("Etat"), ['class' => 'text-right'])
        )
    )
    . $this->Html->tag('tbody');
foreach (Check::archivingSystems() as $as) {
    $success = $as->get('success');
    echo $this->Html->tag(
        'tr',
        $this->Html->tag('td', h($as->get('name')))
        . $this->Html->tag(
            'td',
            $success ? $ok : $bad
        ),
        $success ? [] : ['class' => 'danger']
    );
}

echo $this->Html->tag('/tbody') . $this->Html->tag('/table');

// état des tables
echo $this->Html->tag(
    'section',
    $this->Html->tag('h3', __("Etat des tables"), ['class' => 'h4']),
    ['class' => 'modal-header']
);
echo $this->Html->tag('table', null, ['class' => 'table table-striped table-hover no-padding'])
    . $this->Html->tag(
        'thead',
        $this->Html->tag(
            'tr',
            $this->Html->tag('th', __("Table"))
            . $this->Html->tag('th', __("Message"))
            . $this->Html->tag('th', __("Etat"), ['class' => 'text-right'])
        )
    )
    . $this->Html->tag('tbody');

foreach ($treeSanity as $name => $data) {
    $access = $data['success'];
    echo $this->Html->tag(
        'tr',
        $this->Html->tag('td', h($name))
        . $this->Html->tag('td', h($data['message']))
        . $this->Html->tag(
            'td',
            $this->Html->tag('td', $access ? $ok : $bad, ['class' => 'h4'])
        ),
        $access ? [] : ['class' => 'danger']
    );
}

echo $this->Html->tag('/tbody') . $this->Html->tag('/table');

?>
<script>
    var vignette = $('#div-check-app');
<?php if ($systemOk) :?>
    vignette
        .addClass('success')
        .removeClass('danger')
        .find('div.icon i.fa')
        .removeClass('fa-times')
        .addClass('fa-check');
<?php else : ?>
    vignette
        .addClass('danger')
        .removeClass('success')
        .find('div.icon i.fa')
        .removeClass('fa-check')
        .addClass('fa-times');
<?php endif; ?>
    function checkVerbsTd() {
        var checkVerbs = ['put', 'delete', 'patch', 'head'];
        for (var i = 0; i < checkVerbs.length; i++) {
            (function(verb) {
                $.ajax({
                    url: '<?=$this->Url->build('/admins/ping')?>',
                    method: verb,
                    success: function() {
                        $('#td-method-http-'+verb).html($('<?=$ok?>'));
                    },
                    error: function() {
                        $('#div-check-app').removeClass('success').addClass('danger')
                            .find('.icon .fa').removeClass('fa-check').addClass('fa-times');
                        $('#td-method-http-'+verb).html($('<?=$bad?>')).closest('tr').addClass('danger');
                    }
                });
            })(checkVerbs[i]);
        }
    }
    checkVerbsTd();
</script>
