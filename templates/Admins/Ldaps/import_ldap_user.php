<?php

/**
 * @var Versae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 * @var array $ldaps
 * @var array $roles
 */

echo $this->Flash->render();
echo $this->Form->create(null, ['id' => 'import-ldap-user']);
echo $this->Form->control(
    'ldap_id',
    [
        'label' => __("Ldap"),
        'required' => true,
        'options' => $ldaps,
        'empty' => __("-- Sélectionner un Ldap --"),
        'default' => count($ldaps) === 1 ? current(array_keys($ldaps)) : null,
    ]
)
    . $this->Form->control(
        'ldap_user',
        [
            'id' => 'ldap-users-searchbar',
            'label' => __("Utilisateur à importer"),
            'type' => 'select',
            'required' => true,
        ]
    )
    . $this->Form->control(
        'role_id',
        [
            'label' => __("Role"),
            'required' => true,
            'options' => $roles,
            'empty' => __("-- Sélectionner un role --"),
        ]
    );
echo $this->Form->end();
?>
<script>
    var form = $('#import-ldap-user');
    var searchbar = form.find('#ldap-users-searchbar');
    var searchbarDiv = searchbar.closest('.form-group');
    var ajaxRunning = false;
    var prevval = '';
    var ldapSelect = $('#ldap-id');

    ldapSelect.change(
        function() {
            searchbar.empty();
            if (!ldapSelect.val()) {
                searchbar.disable();
            } else {
                searchbar.enable();
                searchbar.select2({
                    ajax: {
                        url: '<?=$this->Url->build('/Admins/get-ldap-users/')?>' + ldapSelect.val(),
                        method: 'POST',
                        delay: 250
                    },
                    escapeMarkup: function (v) {
                        return $('<span></span>').html(v).text();
                    },
                    dropdownParent: searchbar.closest('.modal'),
                });
            }
        }
    );

    if (!ldapSelect.val()) {
        searchbar.disable();
    } else {
        ldapSelect.change();
    }
</script>
