<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->element('modal', ['idTable' => $tableId, 'paginate' => false]);
$jsTable = $this->Table->getJsTableObject($tableId);

$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover'])
    ->fields(
        [
            'Ldaps.name' => ['label' => __("Nom du LDAP")],
            'Ldaps.host' => ['label' => __("Hôte")],
            'Ldaps.port' => ['label' => __("Port")],
            'Ldaps.account_prefix' => ['label' => __("Prefix"), 'display' => false],
            'Ldaps.account_suffix' => ['label' => __("Suffix"), 'display' => false],
            'Ldaps.user_query_login' => ['label' => __("Identifiant de connexion"), 'display' => false],
            'Ldaps.ldap_root_search' => ['label' => __("DN de base"), 'display' => false],
            'Ldaps.user_login_attribute' => ['label' => __("Attribut username"), 'display' => false],
            'Ldaps.user_name_attribute' => ['label' => __("Attribut name"), 'display' => false],
            'Ldaps.user_mail_attribute' => ['label' => __("Attribut mail"), 'display' => false],
            'Ldaps.ldap_users_filter' => ['label' => __("Filtre"), 'display' => false],
            'Ldaps.use_proxy' => ['label' => __("Proxy"), 'display' => false, 'type' => 'boolean'],
            'Ldaps.use_ssl' => ['label' => __("SSL"), 'display' => false, 'type' => 'boolean'],
            'Ldaps.use_tls' => ['label' => __("TLS"), 'display' => false, 'type' => 'boolean'],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'Ldaps.id',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionEditLdap({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier {0}", '{1}'),
                'aria-label' => $title,
                'params' => ['Ldaps.id', 'Ldaps.name']
            ],
            [
                'onclick' => "actionListLdapEntries({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-list'),
                'title' => $title = __("Lister les entrées de {0}", '{1}'),
                'aria-label' => $title,
                'params' => ['Ldaps.id', 'Ldaps.name']
            ],
            [
                'type' => "button",
                'class' => "btn-link",
                'data-callback' => sprintf(
                    "TableGenericAction.deleteAction(%s, '%s')({0}, false)",
                    $jsTable,
                    $this->Url->build('/admins/delete-ldap')
                ),
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'confirm' => __("Êtes-vous sûr de vouloir supprimer ce LDAP ?"),
                'displayEval' => "data[{index}].Ldaps.deletable",
                'params' => ['Ldaps.id', 'Ldaps.username']
            ]
        ]
    );

echo $this->Html->tag(
    'section#ldap-section.bg-white',
    $this->Html->tag(
        'div.separator',
        $this->ModalForm
            ->create('edit-ldap')
            ->modal(__("Modifier un LDAP"))
            ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTable . ', "Ldaps")')
            ->output(
                'function',
                'actionEditLdap',
                '/admins/editLdap'
            )
            ->generate()
        . $this->ModalView
            ->create('list-ldap-entries', ['size' => 'large'])
            ->modal(__("Liste des entrées du LDAP"))
            ->output(
                'function',
                'actionListLdapEntries',
                '/ldaps/listLdapEntries'
            )
            ->generate()
        . $this->ModalForm
            ->create('add-ldap')
            ->modal(__("Ajouter un LDAP"))
            ->javascriptCallback('TableGenericAction.afterAdd(' . $jsTable . ', "Ldaps")')
            ->output(
                'button',
                $this->Fa->charte('Ajouter', __("Ajouter un LDAP")),
                '/admins/addLdap'
            )
            ->generate(['class' => 'btn btn-success', 'type' => 'button'])
    )
    . $this->Html->tag(
        'header',
        $this->Html->tag('h2.h4', __("Liste des LDAPs"))
        . $this->Html->tag('div.r-actions.h4', $table->getConfigureLink())
    )
    . $table->generate()
);
