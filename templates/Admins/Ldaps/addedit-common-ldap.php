<?php

/**
 * @var Versae\View\AppView $this
 */

use Versae\Model\Entity\Ldap;

if (!empty($sas)) {
    echo $this->Form->control(
        'org_entity_id',
        [
            'label' => __("Service d'archives"),
            'options' => $sas,
            'empty' => __("-- Sélectionner un service d'archives --"),
            'help' => __(
                "Optionnel - Vous pourrez sélectionner ce Ldap depuis l'interface des services d'archives"
            ),
            'default' => count($sas) === 1 ? current(array_keys($sas)) : null,
        ]
    );
}
echo $this->Form->control('name', ['label' => __("Nom du LDAP")])
    . $this->Form->control('description', ['label' => __("Description")])
    . '<hr>'
    . $this->Form->control(
        'schema',
        [
            'label' => __("Schema"),
            'options' => $schemas,
            'default' => Adldap\Schemas\ActiveDirectory::class,
        ]
    )
    . $this->Form->control(
        'version',
        [
            'label' => __("Version LDAP"),
            'options' => $versions,
            'default' => 3,
        ]
    )
    . $this->Form->control(
        'host',
        [
            'label' => __("Hôte du serveur"),
            'placeholder' => 'localhost',
        ]
    )
    . $this->Form->control(
        'port',
        [
            'label' => __("Port du serveur"),
            'type' => 'number',
            'min' => 0,
            'max' => 65535,
            'placeholder' => 389,
        ]
    )
    . $this->Form->control(
        'use_proxy',
        [
            'label' => __("Utiliser le proxy"),
        ]
    )
    . $this->Form->control(
        'use_ssl',
        [
            'label' => __("SSL"),
        ]
    )
    . $this->Form->control(
        'use_tls',
        [
            'label' => __("TLS"),
        ]
    )
    . $this->Form->control(
        'follow_referrals',
        [
            'label' => __("Suivre les références LDAP"),
        ]
    )
    . $this->Form->control(
        'timeout',
        [
            'label' => __("Timeout"),
            'type' => 'number',
            'default' => 5,
            'step' => 1,
        ]
    )
    . $this->Form->control(
        'append_custom_option',
        [
            'label' => __("Ajouter une option"),
            'empty' => __("-- Sélectionner une option --"),
            'options' => array_combine($custom_options, $custom_options),
            'onchange' => 'appendLdapOption(this)',
            'help' => $this->Html->link(
                $url = 'https://www.php.net/ldap_set_option',
                $url,
                ['target' => '_blank']
            )
        ]
    );

$close = $this->Html->tag(
    'button',
    $this->Html->tag('span', '×'),
    [
        'class' => 'close',
        'type' => 'button',
        'aria-label' => __("Retirer l'option"),
        'onclick' => 'var p = $(this).parent(), f = p.closest("form");'
            . ' p.slideUp(400, function() { p.remove(); f.change(); })'
    ]
);
$closeOnClick = 'var p = $(this).parent(), f = p.closest("form"); '
    . 'p.slideUp(400, function() { p.remove(); f.change(); })';
foreach ($entity->get('meta') as $opt => $value) {
    echo $this->Html->tag(
        'div.filter-container',
        $this->Html->tag(
            'button.close',
            $this->Html->tag('span', '×'),
            [
                'type' => 'button',
                'aria-label' => __("Supprimer l'option {0}", $opt),
                'onclick' => $closeOnClick
            ]
        )
        . $this->Form->control(
            'custom_options.' . $opt,
            [
                'label' => $opt,
                'val' => $value
            ]
        )
    );
}

echo $this->Html->tag(
    'button',
    __("Ping"),
    [
        'type' => 'button',
        'class' => 'btn btn-default',
        'onclick' => 'pingLdap(this)'
    ]
);
echo '<hr>';

echo $this->Form->control(
    'user_query_login',
    [
        'label' => __("Identifiant de connexion pour les recherches"),
        'placeholder' => 'admin@versae.fr',
    ]
)
    . '<label class="hide">autocomplete obliterator'
    . '<input type="password" value="false"><!-- neutralise le préremplissage firefox --></label>';

echo $this->Form->control(
    'user_query_password',
    [
        'label' => __("Mot de passe pour les recherches"),
        'type' => 'password',
    ]
)
    . $this->Form->control(
        'ldap_root_search',
        [
            'label' => __("DN de base"),
            'placeholder' => 'dc=versae,dc=fr',
        ]
    );

echo $this->Html->tag(
    'button',
    __("Récupérer une entrée LDAP"),
    [
        'type' => 'button',
        'class' => 'btn btn-default',
        'onclick' => 'getLdapEntry(this)'
    ]
);
echo '<hr>';

echo $this->Form->control(
    'ldap_users_filter',
    [
        'label' => __("Filtre supplémentaire de recherche"),
        'placeholder' => '(memberOf=versae)',
        'help' => __(
            "Il est conseillé de limiter la recherche aux utilisateurs et"
            . " de placer des parentèses autours de la condition. ex: {0}",
            '(objectClass=inetOrgPerson)'
        )
    ]
)
    . $this->Html->tag(
        'button',
        __("Compter les entrées LDAP (avec filtre)"),
        [
            'type' => 'button',
            'class' => 'btn btn-default',
            'onclick' => 'countLdapEntries(this)'
        ]
    );

echo '<hr>';

$exemple1 = $this->Html->tag('span.tips-prefix', 'uid=', ['title' => 'prefix'])
    . $this->Html->tag('span.disabled.gray', 'admin', ['title' => 'username'])
    . $this->Html->tag('span.tips-suffix', ',ou=users,dc=libriciel,dc=fr', ['title' => 'suffix']);
$exemple2 = $this->Html->tag('span.disabled.gray', 'admin', ['title' => 'username'])
    . $this->Html->tag('span.tips-suffix', '@libriciel.fr', ['title' => 'suffix']);
$tmpid = uniqid('tmp');
echo $this->Html->tag(
    'div.fixtips.alert.alert-info',
    $this->Fa->i('fa-lightbulb-o', __("exemples:") . '<br>' . $exemple1 . '<br>' . $exemple2)
);

echo $this->Html->tag(
    'div.form-group',
    $this->Html->tag(
        'button',
        __("Prefix/suffix auto"),
        [
            'type' => 'button',
            'class' => 'btn btn-default',
            'onclick' => 'prefixSuffixAuto(this)'
        ]
    )
);

echo $this->Form->control(
    'account_prefix',
    [
        'label' => __("Prefix pour le login"),
        'placeholder' => 'prefix-',
        'class' => 'prefix-input',
    ]
) . $this->Form->control(
    'account_suffix',
    [
        'label' => __("Suffix pour le login"),
        'placeholder' => '@versae.fr',
        'class' => 'suffix-input',
    ]
)
    . $this->Html->tag(
        'div.form-group',
        $this->Html->tag(
            'button',
            __("Afficher/cacher le test de connexion"),
            [
                'type' => 'button',
                'class' => 'btn btn-default',
                'onclick' => "$('#$tmpid').slideToggle()"
            ]
        )
    );

echo $this->Html->tag("div#$tmpid", null, ['style' => 'display: none'])
    . $this->Form->control(
        'test.username',
        [
            'label' => __("Identifiant"),
        ]
    )
    . $this->Form->control(
        'test.password',
        [
            'label' => __("Mot de passe"),
            'password' => true,
        ]
    )
    . $this->Html->tag(
        'button',
        __("Envoyer le test"),
        [
            'type' => 'button',
            'class' => 'btn btn-default',
            'onclick' => 'testConnection(this)'
        ]
    )
    . $this->Html->tag('/div');

echo '<hr>';

echo $this->Html->tag(
    'div.form-group',
    $this->Html->tag(
        'button',
        __("Mapping auto"),
        [
            'type' => 'button',
            'class' => 'btn btn-default',
            'onclick' => 'mappingAuto(this)'
        ]
    )
);

echo $this->Form->control(
    'user_login_attribute',
    [
        'label' => __("Nom de l'attribut utilisé pour se logger au LDAP"),
        'placeholder' => 'sAMAccountName',
    ]
)
    . $this->Form->control(
        'user_username_attribute',
        [
            'label' => __("Nom de l'attribut utilisé pour se logger à Versae"),
            'placeholder' => 'uid',
            'required' => true,
        ]
    )
    . $this->Form->control(
        'user_name_attribute',
        [
            'label' => __("Attribut utilisé pour le champ nom"),
            'placeholder' => 'displayname',
            'required' => true,
        ]
    )
    . $this->Form->control(
        'user_mail_attribute',
        [
            'label' => __("Attribut utilisé pour le champ mail"),
            'placeholder' => 'mail',
            'required' => true,
        ]
    )
    . $this->Html->tag(
        'button',
        __("Récupérer une entrée filtré/mappé"),
        [
            'type' => 'button',
            'class' => 'btn btn-default',
            'onclick' => 'getFilteredLdapEntry(this)'
        ]
    );

?>
<script>
    function pingLdap(btn) {
        var form = $(btn).closest('form');
        $(btn).disable();
        $.ajax({
            url: '<?=$this->Url->build('/ldaps/ping')?>',
            method: 'POST',
            data: form.serialize(),
            success: function() {
                alert('pong!');
            },
            error: function(e) {
                if (e.responseText) {
                    alert(e.responseText);
                } else {
                    alert(e);
                }
            },
            complete: function() {
                $(btn).enable();
            }
        })
    }
    function getLdapEntry(btn) {
        var form = $(btn).closest('form');
        $(btn).disable();
        $.ajax({
            url: '<?=$this->Url->build('/ldaps/get-random-entry')?>',
            method: 'POST',
            data: form.serialize(),
            success: function(content) {
                alert(content);
            },
            error: function(e) {
                if (e.responseText) {
                    alert(e.responseText);
                } else {
                    alert(e);
                }
            },
            complete: function() {
                $(btn).enable();
            }
        })
    }
    function countLdapEntries(btn) {
        var form = $(btn).closest('form');
        $(btn).disable();
        $.ajax({
            url: '<?=$this->Url->build('/ldaps/get-count')?>',
            method: 'POST',
            data: form.serialize(),
            success: function(content) {
                alert(content);
            },
            error: function(e) {
                if (e.responseText) {
                    alert(e.responseText);
                } else {
                    alert(e);
                }
            },
            complete: function() {
                $(btn).enable();
            }
        });
    }
    function testConnection(btn) {
        var form = $(btn).closest('form');
        $(btn).disable();
        $.ajax({
            url: '<?=$this->Url->build('/ldaps/test-connection')?>',
            method: 'POST',
            data: form.serialize(),
            success: function(content) {
                alert(content);
            },
            error: function(e) {
                if (e.responseText) {
                    alert(e.responseText);
                } else {
                    alert(e);
                }
            },
            complete: function() {
                $(btn).enable();
            }
        });
    }
    function getFilteredLdapEntry(btn) {
        var form = $(btn).closest('form');
        $(btn).disable();
        $.ajax({
            url: '<?=$this->Url->build('/ldaps/get-filtered')?>',
            method: 'POST',
            data: form.serialize(),
            success: function(content) {
                alert(content);
            },
            error: function(e) {
                if (e.responseText) {
                    alert(e.responseText);
                } else {
                    alert(e);
                }
            },
            complete: function() {
                $(btn).enable();
            }
        });
    }

    function appendLdapOption(select) {
        var element = $(select);
        var value = element.val();
        if (!value) {
            return;
        }
        var div = $('<div class="filter-container"></div>').append(
            $('<button type="button" class="close"><span>×</span></button>')
                .attr('aria-label', __("Supprimer l'option {0}", value))
                .attr('onclick', '<?=$closeOnClick?>')
            ).append(Form.input(value, {name: 'custom_options['+value+']'}));
        element.closest('.form-group').after(div);
        element.val('');
        div.find('input').focus();
        optTextToInt();
    }

    $('.prefix-input').on('mouseover focus', function() {
        $('.fixtips .tips-prefix').addClass('text-danger').css('text-decoration', 'underline');
    }).on('mouseout blur', function() {
        if ($(this).is(':focus')) {
            return;
        }
        $('.fixtips .tips-prefix').removeClass('text-danger').css('text-decoration', '');
    });
    $('.suffix-input').on('mouseover focus', function() {
        $('.fixtips .tips-suffix').addClass('text-danger').css('text-decoration', 'underline');
    }).on('mouseout blur', function() {
        if ($(this).is(':focus')) {
            return;
        }
        $('.fixtips .tips-suffix').removeClass('text-danger').css('text-decoration', '');
    });

    var opts_int = <?=json_encode(Ldap::LDAP_OPTS_INT)?>;
    function optTextToInt() {
        var input;
        for (var i = 0; i < opts_int.length; i++) {
            input = $('[name="custom_options['+opts_int[i]+']"]').attr('type', 'number');
        }
    }

    function mappingAuto(btn) {
        var form = $(btn).closest('form');
        $(btn).disable();
        $.ajax({
            url: '<?=$this->Url->build('/ldaps/list-available-attributes')?>',
            method: 'POST',
            data: form.serialize(),
            success: function(content) {
                var lowercase = content.map((v) => v.toLowerCase());
                var ldap_login_fields = ['samaccountname', 'uid', 'cn'];
                var ldap_username_fields = ['samaccountname', 'uid', 'cn'];
                var ldap_name_fields = ['cn', 'commonname', 'displayname', 'name', 'givenname', 'sn'];
                var ldap_mail_fields = ['mail', 'rfc822mailbox', 'mailrfc822mailbox'];
                var login = $('[name="account_prefix"]:visible').val().replace(/(.*,)?=$/g, '');
                var user_query_login = $('[name="user_query_login"]:visible').val();
                var index = 0;
                if (!login) {
                    user_query_login = user_query_login
                        .split(',')
                        .filter((v) => v.substr(0, 2) !== 'dc' && v.substr(0, 2) !== 'ou')[0];
                    login = user_query_login.substr(0, user_query_login.indexOf('='));
                }
                ldap_login_fields.unshift(login);
                for (var i = 0; i < ldap_login_fields.length; i++) {
                    index = lowercase.indexOf(ldap_login_fields[i]);
                    if (index > 0) {
                        $('[name="user_login_attribute"]:visible').val(content[index]);
                        break;
                    }
                }
                for (i = 0; i < ldap_username_fields.length; i++) {
                    index = lowercase.indexOf(ldap_username_fields[i]);
                    if (index > 0) {
                        $('[name="user_username_attribute"]:visible').val(content[index]);
                        break;
                    }
                }
                for (i = 0; i < ldap_name_fields.length; i++) {
                    index = lowercase.indexOf(ldap_name_fields[i]);
                    if (index > 0) {
                        $('[name="user_name_attribute"]:visible').val(content[index]);
                        break;
                    }
                }
                for (i = 0; i < ldap_mail_fields.length; i++) {
                    index = lowercase.indexOf(ldap_mail_fields[i]);
                    if (index > 0) {
                        $('[name="user_mail_attribute"]:visible').val(content[index]);
                        break;
                    }
                }
            },
            error: function(e) {
                if (e.responseText) {
                    alert(e.responseText);
                } else {
                    alert(e);
                }
            },
            complete: function() {
                $(btn).enable();
            }
        });
    }

    function prefixSuffixAuto()
    {
        var queryLogin = $('[name="user_query_login"]:visible').val();
        if (!queryLogin) {
            return;
        }
        var schema = $('[name="schema"]:visible').val();
        if (schema === 'Adldap\\Schemas\\ActiveDirectory') {
            var domain = queryLogin
                .split(',')
                .filter((v) => v.substr(0, 2) === 'dc')
                .map((v) => v.substr(3))
                .join('.');
            $('[name="account_prefix"]:visible').val('');
            $('[name="account_suffix"]:visible').val('@'+domain);
        } else {
            var adminusername = queryLogin
                .split(',')
                .filter((v) => v.substr(0, 2) !== 'dc' && v.substr(0, 2) !== 'ou')[0];
            adminusername = adminusername.substr(adminusername.indexOf('=') +1);
            var fixes = queryLogin.split(adminusername);
            // l'admin est souvent dans un autre groupe, on récupère donc le DN de base
            if (fixes[1][0] === ',') {
                fixes[1] = ','+$('[name="ldap_root_search"]:visible').val();
            }
            $('[name="account_prefix"]:visible').val(fixes[0]); // ex: uid=
            $('[name="account_suffix"]:visible').val(fixes[1]); // ex: ,ou=test,dc=libriciel,dc=fr
        }
    }
</script>
