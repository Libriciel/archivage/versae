<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->Form->create($entity, ['idPrefix' => 'add-ldap']);
require 'addedit-common-ldap.php';
echo $this->Form->end();
