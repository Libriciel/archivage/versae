<?php

/**
 * @var Versae\View\AppView $this
 * @var Versae\Model\Entity\Cron $entity
 */

$units = $this->Form->control(
    'frequency_build_unit',
    [
        'options' => $entity->get('units'),
        'default' => 'PT_H',
        'value' => $entity->get('frequency_build_unit'),
        'templates' => [
            'inputContainer' => '{{content}}',
            'formGroup' => '{{input}}',
            'select' => '<select name="{{name}}">{{content}}</select>',
        ],
    ]
);

echo $this->Html->tag('h3', h($entity->get('name')));
echo $this->Html->tag('p', $entity->get('description'));

echo $this->Form->create($entity, ['idPrefix' => 'add-cron'])
    . $this->Form->control(
        'next',
        [
            'label' => __("Date et heure de la prochaine exécution"),
            'required' => true,
            'type' => 'text',
            'append' => $this->Date->datetimePicker('#add-cron-next'),
            'class' => 'datepicker',
        ]
    );

if ($entity->get('frequency') === 'one_shot') {
    echo $this->Form->control(
        'frequency',
        [
            'label' => __("Délai entre deux exécutions"),
            'readonly' => true,
        ]
    );
} else {
    echo $this->Form->control(
        'frequency_build',
        [
            'label' => __("Délai entre deux exécutions"),
            'required' => true,
            'class' => 'with-select',
            'type' => 'number',
            'min' => 1,
            'append' => $units,
        ]
    );
}
echo $this->Form->control(
    'active',
    [
        'label' => __("Actif"),
    ]
)
    . $this->Form->control(
        'hidden_locked',
        [
            'type' => 'hidden',
        ]
    )
    . $this->Form->control(
        'locked',
        [
            'label' => __("Verrouillée"),
            'onclick' => 'return alertUnlockCron()'
        ]
    );

echo '<hr>';

/** @var string|AsalaeCore\Cron\CronInterface $className */
$className = $entity->get('classname');
foreach ($className::getVirtualFields() as $field => $options) {
    $type = $options['type'] ?? 'text';
    $default = $type !== 'checkbox';
    $options['required'] = $options['required'] ?? $default;
    echo $this->Form->control($field, $options);
}

echo $this->Form->end();
?>
<script>
    function alertUnlockCron() {
        if (confirm(
            __(
                "Le verrouillage est géré automatiquement, et ne doit être modifié qu'en cas de plantage. "
                +"Voulez-vous vraiment modifier cette valeur ?"
            )
        )
        ) {
            $('#add-cron-hidden-locked').val(1);
            return true;
        }
        return false;
    }
</script>
