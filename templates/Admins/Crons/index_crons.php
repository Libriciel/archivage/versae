<?php

/**
 * @var Versae\View\AppView $this
 */

?>
<script>
    function formatClassname(value) {
        return value.substr(value.lastIndexOf('\\') +1);
    }
</script>
<?php
echo $this->element('modal', ['idTable' => $tableId, 'paginate' => false]);

$view = $this;

$jsTable = $this->Table->getJsTableObject($tableId);
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover'])
    ->fields(
        [
            'Crons.id' => [
                'label' => __("ID"),
                'display' => false,
            ],
            'Crons.name' => ['label' => __("Nom")],
            'Crons.frequencytrad' => ['label' => __("Fréquence")],
            'Crons.classname' => [
                'label' => __("Nom de classe"),
                'escape' => false,
                'callback' => 'formatClassname',
                'display' => false,
            ],
            'Crons.app_meta' => [
                'label' => __("Champs"),
                'escape' => false,
                'callback' => 'parseJsonData',
                'display' => false,
            ],
            'Crons.lastexec.state' => [
                'label' => __("Statut dernière exécution"),
            ],
            'Crons.description' => [
                'label' => __("Description"),
                'display' => false
            ],
            'Crons.active' => [
                'label' => __("Actif"),
                'type' => 'boolean',
            ],
            'Crons.locked' => [
                'label' => __("Verrouillée"),
                'type' => 'boolean',
            ],
            'Crons.next' => [
                'label' => __("Date de la prochaine execution"),
                'type' => 'datetime',
            ],
        ]
    )
    ->data($crons)
    ->params(
        [
            'identifier' => 'Crons.id',
            'classEval' => 'var a = data[{index}].Crons.lastexec.state;
            if (a === "error") a = "danger";
            if (a === "deleted") a = "gray disabled";
            a',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionViewCron({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => __("Visualiser {0}", '{1}'),
                'aria-label' => __("Visualiser {0}", '{1}'),
                'displayEval' => '!data[{index}].Crons.lastexec || data[{index}].Crons.lastexec.state !== "deleted"',
                'params' => ['Crons.id', 'Crons.name']
            ],
            [
                'onclick' => "actionEditCron({0})",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'displayEval' => '!data[{index}].Crons.lastexec || (data[{index}].Crons.lastexec.state !== "deleted"
                && data[{index}].Crons.lastexec.state !== "running")',
                'params' => ['Crons.id', 'Crons.name']
            ],
            [
                'onclick' => "actionRunCron({0})",
                'type' => "button",
                'class' => 'btn-link run-cron',
                'label' => $this->Fa->i('fa-cogs'),
                'title' => __("Lancer {0}", '{1}'),
                'aria-label' => __("Lancer {0}", '{1}'),
                'displayEval' => '!data[{index}].Crons.lastexec || (data[{index}].Crons.lastexec.state !== "deleted"
                && data[{index}].Crons.lastexec.state !== "running")',
                'params' => ['Crons.id', 'Crons.name']
            ],
            [
                'type' => "button",
                'class' => "btn-link",
                'data-callback' => sprintf(
                    "TableGenericAction.deleteAction(%s, '%s')({0}, false)",
                    $jsTable,
                    $this->Url->build('/admins/delete-cron')
                ),
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'displayEval' => 'data[{index}].Crons.deletable',
                'confirm' => __("Êtes-vous sûr de vouloir supprimer cette tâche planifiée ?"),
                'params' => ['Crons.id', 'Crons.name']
            ],
        ]
    );
$jsTable = $table->tableObject;

echo $this->Html->tag('div.row.no-padding')
        . $this->Html->tag('section.bg-white')
        . $this->Html->tag(
            'header',
            $this->Html->tag('h2.h4', __("Liste des tâches planifiées"))
            . $this->Html->tag('div.r-actions.h4', $table->getConfigureLink())
        )
        . $this->Html->tag('/section')
        . $table->generate();


echo $this->ModalView->create('view-cron', ['size' => 'large'])
    ->modal(__("Visualisation d'une tâche planifiée"))
    ->output('function', 'actionViewCron', '/Admins/viewCron')
    ->generate();

echo $this->ModalForm->create('edit-cron')
    ->modal(__("Modification d'une tâche planifiée"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $table->tableObject . ', "Crons")')
    ->output('function', 'actionEditCron', '/Admins/editCron')
    ->generate();

echo $this->Html->tag('/section');

use Cake\Core\Configure; ?>
<script>
    var cronRunning = [];
    var cronTimeout = [];
    function animateRunningCron(tr) {
        tr.removeClass('success')
            .removeClass('danger')
            .removeClass('warning')
            .animate({
                backgroundColor: '#fcd8c7'
            }, 500);
        setTimeout(function() {
            tr.removeClass('active')
                .animate({
                    backgroundColor: '#ffffff'
                }, 500);
        }, 0);
        return setInterval(function() {
            tr.animate({
                backgroundColor: '#fcd8c7'
            }, 500).animate({
                backgroundColor: '#ffffff'
            }, 500);
        }, 1000);
    }

    function actionRunCron(id) {
        if (!confirm(__("Cette action va forcer le lancement de la tâche planifiée, voulez-vous continuer ?"))) {
            return;
        }
        var tr = $('#index-crons-table').find('tr[data-id='+id+']');
        var btn = tr.find('.run-cron').disable();
        cronRunning[id] = animateRunningCron(tr);

        cronTimeout[id] = setTimeout(function() {
            refreshLine(id);
        }, 5000);
        AsalaeLoading.start();
        $.ajax({
            url: '<?=$this->Url->build('/admins/runCron')?>/'+id,
            error: function(error) {
                alert(PHP.messages.genericError);
            },
            complete: function() {
                AsalaeLoading.stop();
                btn.enable();
            }
        });
    }

    function refreshLine(id) {
        $.ajax({
            url: '<?=$this->Url->build('/admins/getCronState')?>/'+id,
            headers: {Accept: 'application/json'},
            success: function(content) {
                var tr = $('#index-crons-table').find('tr[data-id='+id+']');
                if (content.lastexec.state === 'running') {
                    cronTimeout[id] = setTimeout(function() {
                        refreshLine(id);
                    }, 5000);
                    return;
                }
                clearInterval(cronRunning[id]);
                var table;
                table = <?=$table->tableObject?>;
                var data = table.getDataId(content.id)
                if (!data) {
                    console.warn('Unable to edit: content.id was not found');
                    return;
                }
                data.Crons = content;
                TableGenerator.appendActions(table.data, table.actions);
                table.generateAll();
            },
            error: function() {
                clearInterval(cronRunning[id]);
                alert(PHP.messages.genericError);
            },
            complete: function() {
                AsalaeLoading.stop();
            }
        });
    }

    AsalaeWebsocket.connect(
        '<?=Configure::read('Ratchet.connect')?>',
        function(session) {
            ratchet.push(session);
            session.subscribe('crons', function(topic, data) {
                var table;
                table = <?=$jsTable?>;
                if (typeof data !== 'object' || typeof data.message !== 'object') {
                    return;
                }
                var id = data.message.cron_id;
                var tr = $(table.table).find('tr[data-id="'+id+'"]');
                if (!tr.length) {
                    return;
                }
                if (typeof cronRunning[id] !== 'undefined') {
                    clearInterval(cronRunning[id]);
                }
                if (typeof cronTimeout[id] !== 'undefined') {
                    clearTimeout(cronTimeout[id]);
                }
                var dataTable = table.getDataId(id);
                if (data.message.state) {
                    if (!dataTable.Crons.lastexec) {
                        dataTable.Crons.lastexec = {};
                    }
                    dataTable.Crons.lastexec.state = data.message.state;
                    if (data.message.state === 'running') {
                        setTimeout(function() {
                            var tr = $(table.table).find('tr[data-id="'+id+'"]');
                            if (table.getDataId(id).Crons.lastexec.state === 'running') {
                                animateRunningCron(tr);
                                tr.find('.run-cron').disable();
                            }
                        }, 100);
                        dataTable.Crons.locked = true;
                    }
                    table.generateAll();
                } else if (data.message.deleted) {
                    dataTable.Crons.lastexec.state = 'deleted';
                    table.generateAll();
                } else {
                    setTimeout(function() {
                        refreshLine(id);
                    }, 100); // Le temps que le cron se termine complètement
                }
            });
        },
        function() {
        },
        {'skipSubprotocolCheck': true}
    );
</script>
