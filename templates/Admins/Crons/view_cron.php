<?php

/**
 * @var Versae\View\AppView $this
 */

use AsalaeCore\Model\Table\CronExecutionsTable;
use Cake\ORM\Entity;

$fields = '';
if ($f = $entity->get('app_meta')) {
    $fields = [];
    foreach (json_decode($f, true) as $field => $value) {
        if (is_array($value)) {
            $value = implode(', ', $value);
        }
        $fields[] = $field . ': ' . h($value);
    }
    $fields = $this->Html->tag(
        'ul',
        $this->Html->tag('li', implode('</li><li>', $fields))
    );
}

$lastExec = $entity->get('lastexec') ?: new Entity();
$begin = $lastExec->get('date_begin');
$end = $lastExec->get('date_end');
$report = $lastExec->get('state') === CronExecutionsTable::S_RUNNING
    ? $this->Html->tag('pre', $entity->get('log'))
    : nl2br(preg_replace('/<script.*<\/script[^>]*>/', '', $lastExec->get('report')));

echo $this->Html->tag(
    'div',
    $this->Html->tag(
        'div',
        $this->Html->tag('h3.h2', h($entity->get('name')))
        . $this->Html->tag('p', $entity->get('description'))
    )
    . $this->Html->tag(
        'table.table.table-striped.table-hover.fixed-left-th',
        $this->Html->tag(
            'thead',
            $this->Html->tag(
                'tr',
                $this->Html->tag('th', __("Informations principales"), ['colspan' => 2])
            )
        )
        . $this->Html->tag(
            'tbody',
            $this->Html->tag(
                'tr',
                $this->Html->tag('th', __("Identifiant en BDD"))
                . $this->Html->tag('td', $entity->get('id'))
            )
            . $this->Html->tag(
                'tr',
                $this->Html->tag('th', __("Nom"))
                . $this->Html->tag('td', h($entity->get('name')))
            )
            . $this->Html->tag(
                'tr',
                $this->Html->tag('th', __("Champs additionnels"))
                . $this->Html->tag('td', $fields)
            )
            . $this->Html->tag(
                'tr',
                $this->Html->tag('th', __("Actif"))
                . $this->Html->tag('td', $entity->get('active') ? __("Oui") : __("Non"))
            )
            . $this->Html->tag(
                'tr',
                $this->Html->tag('th', __("En cours d'execution"))
                . $this->Html->tag('td', $entity->get('locked') ? __("Oui") : __("Non"))
            )
        )
    )
    . $this->Html->tag(
        'table.table.table-striped.table-hover.fixed-left-th',
        $this->Html->tag(
            'thead',
            $this->Html->tag(
                'tr',
                $this->Html->tag('th', __("Prochaine exécution"), ['colspan' => 2])
            )
        )
        . $this->Html->tag(
            'tbody',
            $this->Html->tag(
                'tr',
                $this->Html->tag('th', __("Date prévue"))
                . $this->Html->tag('td', Cake\I18n\FrozenTime::parseDateTime((string)$entity->get('next')))
            )
        )
        . $this->Html->tag(
            'tbody',
            $this->Html->tag(
                'tr',
                $this->Html->tag('th', __("Délai entre 2 exécutions"))
                . $this->Html->tag('td', h($entity->get('frequencytrad')))
            )
        )
    )
    . $this->Html->tag(
        'table.table.table-striped.table-hover.fixed-left-th',
        $this->Html->tag(
            'thead',
            $this->Html->tag(
                'tr',
                $this->Html->tag('th', __("Dernière exécution"), ['colspan' => 2])
            )
        )
        . $this->Html->tag(
            'tbody',
            $this->Html->tag(
                'tr',
                $this->Html->tag('th', __("Statut"))
                . $this->Html->tag('td', h($lastExec->get('state')))
            )
        )
        . $this->Html->tag(
            'tbody',
            $this->Html->tag(
                'tr',
                $this->Html->tag('th', __("PID"))
                . $this->Html->tag('td', h($lastExec->get('pid')))
            )
        )
        . $this->Html->tag(
            'tbody',
            $this->Html->tag(
                'tr',
                $this->Html->tag('th', __("Début exécution"))
                . $this->Html->tag('td', Cake\I18n\FrozenTime::parseDateTime((string)$begin))
            )
        )
        . $this->Html->tag(
            'tbody',
            $this->Html->tag(
                'tr',
                $this->Html->tag('th', __("Dernière mise à jour"))
                . $this->Html->tag('td', Cake\I18n\FrozenTime::parseDateTime((string)$lastExec->get('last_update')))
            )
        )
        . $this->Html->tag(
            'tbody',
            $this->Html->tag(
                'tr',
                $this->Html->tag('th', __("Fin exécution"))
                . $this->Html->tag('td', Cake\I18n\FrozenTime::parseDateTime((string)$end))
            )
        )
        . $this->Html->tag(
            'tbody',
            $this->Html->tag(
                'tr',
                $this->Html->tag(
                    'td',
                    '<b>' . __("Rapport") . '</b><hr>'
                        . $report,
                    ['colspan' => 2]
                )
            )
        )
    )
);
