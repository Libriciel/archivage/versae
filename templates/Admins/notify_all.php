<?php

/**
 * @var Versae\View\AppView $this
 */

$options = [
    'alert-info' => __("bleu"),
    'alert-success' => __("vert"),
    'alert-danger' => __("rouge"),
    'alert-warning' => __("jaune"),
];
echo $this->Form->create(null, ['idPrefix' => 'notify-all']);
echo $this->Form->control(
    'color',
    [
        'options' => $options,
        'label' => __("Couleur de notification")
    ]
);
echo $this->Form->control(
    'activeusers',
    [
        'type' => 'checkbox',
        'label' => __("Utilisateur connecté uniquement ?"),
        'checked'
    ]
);
echo $this->Form->control(
    'msg',
    [
        'type' => 'textarea',
        'label' => __("Message"),
        'class' => 'mce mce-small',
    ]
);
echo $this->Html->tag(
    'button',
    __("Tester"),
    [
        'type' => 'button',
        'class' => 'btn btn-default',
        'onclick' => 'notifyMe(this)'
    ]
);
echo $this->Form->end();
?>
<script>
    setTimeout(function() {
        tiny = tinymce.init({
            selector: 'textarea.mce-small',
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste help wordcount'
            ],
            // @see https://www.tiny.cloud/docs/advanced/editor-control-identifiers
            toolbar: 'undo redo | bold italic underline forecolor backcolor | fontsizeselect | bullist numlist | code',
            contextmenu: 'link | cut copy paste | formats removeformat | align | hr',
            branding: false,
            language: tinymceDefaultLanguage,
            setup: function(editor) {
                editor.on('keyup', function(e) {
                    editor.save();
                });
                editor.on('change', function(e) {
                    editor.save();
                    $(editor.getElement()).trigger('change');
                });
            }
        }).then(function(result) {
            var tiny = result[0];
            $('#notify-all-color').change(function() {
                var css = applyStyle($(this).val());
                tiny.getBody().setAttribute('style', css);
            }).change();
        });
        var modal = $('.modal:visible').last();
        modal.one('hidden.bs.modal', function() {
            tinymce.remove('textarea.mce-small');
        });
        $(document).off('.tinymodal').on('focusin.tinymodal', function(e) {
            var dialog = $(e.target).closest(".tox-dialog");
            if (dialog.length && modal.find(dialog).length === 0) {
                var wrapper = $('.tox-tinymce-aux');
                modal.append(wrapper);
            }
        });
    }, 0);
    function applyStyle(option) {
        var css = {
            "color": "#333"
        };
        switch (option) {
            case 'alert-info':
                css["background-color"] = '#c8e5f3';
                break;
            case 'alert-warning':
                css["background-color"] = '#faf5d7';
                break;
            case 'alert-danger':
                css["background-color"] = '#eed5d5';
                break;
            case 'alert-success':
                css["background-color"] = '#d4eacb';
                break;
        }
        var output = [];
        for (var key in css) {
            output.push(key+': '+css[key]);
        }
        return output.join('; ');
    }
</script>
