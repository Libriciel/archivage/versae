<?php

/**
 * @var Versae\View\AppView $this
 */

?>
<form method="post" class="form-horizontal">
    <!-- Text input-->
    <div class="form-group row">
        <label class="col-md-4 control-label login-label" for="textinput"><?=__("Identifiant")?></label>
        <div class="col-md-8">
            <input id="textinput"
                   name="username"
                   placeholder="<?=__("Identifiant de connexion")?>"
                   class="form-control input-md">
        </div>
    </div>

    <!-- Password input-->
    <div class="form-group row">
        <label class="col-md-4 control-label login-label" for="passwordinput"><?=__("Mot de passe")?></label>
        <div class="col-md-8">
            <input id="passwordinput"
                   name="password"
                   placeholder="********"
                   class="form-control input-md"
                   type="password">
        </div>
    </div>

    <?=$this->Html->link(
        '<i aria-hidden="true" class="fa fa-reply"></i> '
        . __("Quitter l'espace d'administration"),
        '/',
        ['escape' => false, 'data-mode' => 'no-ajax']
    )?>

    <!-- Button -->
    <div class="form-group text-right login-btn-group row">
        <div class="col-md-12">
            <button type="submit"
                    id="singlebutton"
                    class="btn btn-primary"><?=$this->Fa->charte(
                        'Se connecter',
                        '',
                        'fa-fw'
                    )?> <?=__("Se connecter")?></button>
        </div>
    </div>
</form>
