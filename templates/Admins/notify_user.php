<?php

/**
 * @var Versae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */

use Cake\Utility\Hash;

echo $this->Html->tag(
    'h3',
    __("Envoyer une notification à {0}", h(Hash::get($entity, 'user.username')))
);

$options = [
    'alert-info' => __("bleu"),
    'alert-success' => __("vert"),
    'alert-danger' => __("rouge"),
    'alert-warning' => __("jaune"),
];
echo $this->Form->create(null, ['idPrefix' => 'notify-user']);
echo $this->Form->control(
    'color',
    [
        'options' => $options,
        'label' => __("Couleur de notification")
    ]
);
echo $this->Form->control(
    'msg',
    [
        'type' => 'textarea',
        'label' => __("Message"),
        'class' => 'mce mce-small',
    ]
);
echo $this->Form->end();
?>
<script>
    new AsalaeMce({selector: '#notify-user-msg'}, $('#notify-user-color'));
</script>
