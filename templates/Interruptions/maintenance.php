<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Core\Configure;
use Cake\I18n\FrozenTime;

$libricielLogo = defined('LIBRICIEL_LOGIN_LOGO')
    ? LIBRICIEL_LOGIN_LOGO
    : $this->Html->image(
        'versae-color.svg',
        ['class' => 'row center-block main-logo', 'alt' => 'Logo Versae']
    );

$message = Configure::read('Interruption.is_periodic')
    ? Configure::read('Interruption.message_periodic')
    : Configure::read('Interruption.message');

if ($this->getRequest()->is('ajax')) {
    $classes = 'center-block login-block';
} else {
    $classes = 'col-xs-10 col-sm-7 col-md-6 col-lg-5 center-block login-block';
}

$now = new DateTime();
$dateStart = $timeStart = null;
if ($start = Configure::read('Interruption.scheduled.begin')) {
    $dateStart = new DateTime($start);
    $timeStart = $dateStart->format('H:i:s');
}
$dateEnd = $timeEnd = null;
if ($end = Configure::read('Interruption.scheduled.end')) {
    $dateEnd = new FrozenTime($end);
    $timeEnd = $dateEnd->format('Y-m-d') === $now->format('Y-m-d')
        ? __("Le service est interrompu jusqu'à {0}", $dateEnd->format('H:i:s'))
        : __("Le service est interrompu jusqu'au {0}", $dateEnd->nice());
}

$small = '';
if (Configure::read('Interruption.enabled')) {
    if ($dateEnd > $now) {
        $small = $timeEnd;
    } else {
        $small = __("Le service est interrompu pour une période indéterminée");
    }
} elseif ($start && $dateStart < $now) {
    if ($dateEnd > $now) {
        $small = $timeEnd;
    } else {
        $small = __("Le service est interrompu pour une période indéterminée");
    }
} elseif (
    ($s = Configure::read('Interruption.periodic.begin'))
    && $t = Configure::read('Interruption.periodic.end')
) {
    $small = __("Le service est interrompu de {0} à {1}", h($s), h($t));
}

$this->loadHelper('Fa', ['className' => 'AsalaeCore.Fa']);
echo $this->Html->tag(
    'div',
    $this->Html->tag(
        'div',
        $libricielLogo . PHP_EOL
        . '<hr>'
        . $this->Html->tag('h2', __("Mode maintenance"), ['class' => 'text-center'])
        . ($small ? $this->Html->tag('p', $small, ['class' => 'text-center']) : '')
        . '<hr>'
        . $this->Html->tag(
            'div',
            $this->Fa->i('fa-4x fa-cogs', '', ['style' => 'vertical-align: middle']),
            ['style' => 'display: inline-block; width: 20%;']
        )
        . $this->Html->tag('div', $message, ['style' => 'display: inline-block; width: 79%;']),
        ['class' => 'form-block']
    ),
    ['class' => $classes, 'id' => 'maintenance-section']
);
