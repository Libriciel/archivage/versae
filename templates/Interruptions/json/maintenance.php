<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Core\Configure;
use Cake\I18n\FrozenTime as Time;

$message = Configure::read('Interruption.is_periodic')
    ? Configure::read('Interruption.message_periodic')
    : Configure::read('Interruption.message');

$now = new DateTime();
$dateStart = $timeStart = null;
if ($start = Configure::read('Interruption.scheduled.begin')) {
    $dateStart = new DateTime($start);
    $timeStart = $dateStart->format('H:i:s');
}
$dateEnd = $timeEnd = null;
if ($end = Configure::read('Interruption.scheduled.end')) {
    $dateEnd = new Time($end);
    $timeEnd = $dateEnd->format('Y-m-d') === $now->format('Y-m-d')
        ? __("Le service est interrompu jusqu'à {0}", $dateEnd->format('H:i:s'))
        : __("Le service est interrompu jusqu'au {0}", $dateEnd->nice());
}

$small = '';
if (Configure::read('Interruption.enabled')) {
    if ($dateEnd > $now) {
        $small = $timeEnd;
    } else {
        $small = __("Le service est interrompu pour une période indéterminée");
    }
} elseif ($start && $dateStart < $now) {
    if ($dateEnd > $now) {
        $small = $timeEnd;
    } else {
        $small = __("Le service est interrompu pour une période indéterminée");
    }
} elseif (
    ($s = Configure::read('Interruption.periodic.begin'))
    && $t = Configure::read('Interruption.periodic.end')
) {
    $small = __("Le service est interrompu de {0} à {1}", h($s), h($t));
}
$report = [
    'error' => __("Mode maintenance"),
    'code' => 503,
    'message' => $message
];
if ($small) {
    $report['description'] = $small;
}

echo json_encode($report, JSON_PRETTY_PRINT);
