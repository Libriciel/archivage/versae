<?php

/**
 * @var Versae\View\AppView $this
 */
use Cake\Core\Configure;

$advanced = Configure::read('Tasks.admin.advanced', false);

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Administration"));
$this->Breadcrumbs->add(__("Jobs en cours"));

// Titre de la page
echo $this->Html->tag(
    'div.container',
    $this->Html->tag('h1', $this->Fa->i('fa-tasks', __("Jobs en cours")))
    . $this->Breadcrumbs->render()
);

$jsTable = $this->Table->getJsTableObject($tableId);

echo $this->element('modal', ['idTable' => $tableId]);

echo $this->ModalForm
    ->create('edit-task')
    ->modal(__("Modifier un job"))
    ->output(
        'function',
        'actionEditJob',
        '/tasks/edit'
    )
    ->generate();

echo $this->ModalView->create('view-transfer-tasks', ['size' => 'large'])
    ->modal(__("Visualisation d'un transfert"))
    ->output('function', 'viewTransfer', '/Transfers/view')
    ->generate();

echo $this->ModalView
    ->create('view-user-tasks')
    ->modal(__("Visualiser un utilisateur"))
    ->output(
        'function',
        'loadViewUser',
        '/users/view'
    )
    ->generate();

echo $this->ModalView->create('view-archive-tasks', ['size' => 'modal-xxl'])
    ->modal(__("Visualisation d'une archive"))
    ->output('function', 'viewArchive', '/Archives/view')
    ->generate();

$buttons = [];
if ($advanced && $this->Acl->check('/Tasks/add')) {
    $buttons[] = $this->ModalForm
        ->create('task-add-form')
        ->modal(__("Nouveau Job"))
        ->javascriptCallback('TableGenericAction.afterAdd(' . $jsTable . ', "BeanstalkJobs")')
        ->output('button', $this->Fa->charte('Ajouter', __("Ajouter un job")), '/Tasks/add')
        ->generate(['class' => 'btn btn-success', 'type' => 'button']);
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}

echo $this->Filter->create('admin-jobs-filter')
    ->saves($savedFilters)
    ->filter(
        'tube',
        [
            'label' => __("Tubes"),
            'type' => 'select',
            'multiple' => true,
            'options' => $tubes,
        ]
    )
    ->filter(
        'job_state',
        [
            'label' => __("Etat"),
            'type' => 'select',
            'multiple' => true,
            'options' => $job_states
        ]
    )
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date'
        ]
    )
    ->filter(
        'user_id',
        [
            'label' => __("Utilisateur"),
            'options' => $users
        ]
    )
    ->generateSection();
?>
<script>
    $(function() {
        AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');
    });

    AsalaeWebsocket.connect(
        '<?=Configure::read('Ratchet.connect')?>',
        function(session) {
            ratchet = session;
            session.subscribe('work_begin', function(topic, data) {
                var table;
                table = <?=$jsTable?>;
                var tr = $(table.table).find('tr[data-id="'+data.message.jobid+'"]');
                if (tr.length) {
                    tr.addClass('alert')
                        .addClass('alert-warning');
                    tr.find('td.state').text(__("En cours"));
                    table.getDataId(data.message.jobid).statetrad = __("En cours");
                }
            });
            session.subscribe('work_end', function(topic, data) {
                var table = {};
                table = <?=$jsTable?>;
                var tr = $(table.table).find('tr[data-id="'+data.message.jobid+'"]');
                if (tr.length) {
                    var notSync = $(table.table)
                        .find(
                            'tr.alert-warning.not-sync.tube-'
                            +data.message.tube+':not([data-id="'+data.message.jobid+'"])'
                        );
                    if (notSync.length) {
                        notSync.removeClass('alert')
                            .removeClass('alert-warning')
                            .addClass('gray')
                            .disable()
                            .find('td.state')
                            .text('???');
                        tr.find('td.state').text(__("En cours"));
                    }
                    if (data.message.error_message) {
                        tr.removeClass('alert-warning')
                            .addClass('alert-danger')
                            .find('td.error')
                            .text(data.message.error_message);
                        tr.find('td.state').text(__("En erreur"));
                    } else {
                        tr.removeClass('alert').removeClass('alert-warning').addClass('gray').disable();
                        tr.find('a').disable();
                        tr.find('td.state').text(__("Terminé"));
                        table.getDataId(data.message.jobid).statetrad = __("Terminé");
                    }
                    if ($('#auto-reload-index-tasks').prop('checked')) {
                        if (tr.is(':last-child')) {
                            AsalaeGlobal.updatePaginationAjax('#<?=$tableId?>-section');
                        } else {
                            var ready = [];
                            tr.parent().find('> tr').each(function() {
                                table._getDataById($(this).attr('data_id'))
                            });
                        }
                    }
                }
            });
        },
        function() {
        },
        {'skipSubprotocolCheck': true}
    );

    function updatePaginationAjaxCallback() {
        $('#btn-paginate-all-jobs').click(function () {
            var table = {};
            table = <?=$jsTable?>;
            var visibleCheckedCheckboxes = table.table.find('tbody td.td-checkbox input[type=checkbox]:checked');
            if (!visibleCheckedCheckboxes.length) {
                return;
            }
            $(this).disable();
            switch ($('#select-paginate-all-jobs').val()) {
                case '3':
                    if (confirm("<?=__("Êtes-vous sûr de vouloir supprimer les jobs sélectionnés ?")?>")) {
                        visibleCheckedCheckboxes.each(function () {
                            var data = table.getDataId($(this).val());
                            jobDelete(data.id, data.jobid);
                        });
                    }
                    break;
                case '2':
                    visibleCheckedCheckboxes.each(function () {
                        var d = table.getDataId($(this).val());
                        if (d && d.state === 'failed') {
                            var data = table.getDataId($(this).val());
                            jobResume(data.id, data.jobid);
                        }
                    });
                    break;
                case '1':
                    if (confirm("<?=__("Êtes-vous sûr de vouloir mettre les jobs sélectionnés en pause ?")?>")) {
                        visibleCheckedCheckboxes.each(function () {
                            var data = table.getDataId($(this).val());
                            if (data && data.state === 'ready') {
                                jobPause(data.id, data.jobid);
                            }
                        });
                    }
                    break;
            }
            $(this).removeAttr('disabled');
            table.table.find('> thead th input.checkall').prop('checked', false);
        });
    }
    $(window).off('history.change.pagination').on('history.change.pagination', function() {
        updatePaginationAjaxCallback();
    });

    function jobDelete(id, jobid) {
        var table = {};
        table = <?=$jsTable?>;
        var tr = $(table.table).find('tr[data-id="'+jobid+'"]');
        $.ajax({
            url: '<?=$this->Url->build('/tasks/ajax_cancel')?>/' + id,
            success: function (message) {
                if (message.report === 'done') {
                    table.removeDataId(jobid);
                    tr.fadeOut(400, function () {
                        AsalaeGlobal.updatePaginationAjax($(this).closest('section'));
                    });
                } else {
                    throw message.report;
                }
            },
            error: function (e, type, code) {
                var message = "<?=__("Une erreur a eu lieu lors de la suppression")?>";
                if (code === 'Forbidden') {
                    message = "<?=__("Vous n'avez pas la permission d'effectuer cette action")?>";
                }
                tr.addClass('danger')
                    .find('td.statetrad')
                    .text(message);
            }
        });
    }

    function jobPause(id, jobid) {
        var table;
        table = <?=$jsTable?>;
        var tr = $(table.table).find('tr[data-id="'+jobid+'"]');
        $.ajax({
            url: '<?=$this->Url->build('/tasks/ajax_pause')?>/' + id,
            success: function (message) {
                if (message.report === 'done') {
                    var table;
                    table = <?=$jsTable?>;
                    var oldData = table.getDataId(jobid),
                        newData = JSON.parse(message.data);
                    newData = TableGenerator.appendActions([newData], table.actions)[0];
                    oldData.actions = newData.actions;
                    oldData.state = newData.state;
                    oldData.statetrad = newData.statetrad;
                    table.replaceDataId(id, oldData)
                        .generateTbody();
                } else {
                    throw message.report;
                }
            },
            error: function (e, type, code) {
                var message = "<?=__("Une erreur a eu lieu lors de la mise en pause")?>";
                if (code === 'Forbidden') {
                    message = "<?=__("Vous n'avez pas la permission d'effectuer cette action")?>";
                }
                $('#table-1-tr-' + id)
                    .addClass('danger')
                    .find('td.statetrad')
                    .text(message);
            }
        });
    }

    function jobResume(id, jobid) {
        var table = {};
        table = <?=$jsTable?>;
        var tr = $(table.table).find('tr[data-id="'+jobid+'"]');
        $.ajax({
            url: '<?=$this->Url->build('/tasks/ajax_resume')?>/' + id,
            success: function (message) {
                if (message.report === 'done') {
                    var oldData = table.getDataId(jobid),
                        newData = JSON.parse(message.data);
                    newData = TableGenerator.appendActions([newData], table.actions)[0];
                    oldData.actions = newData.actions;
                    oldData.state = newData.state;
                    oldData.statetrad = newData.statetrad;
                    table.replaceDataId(id, oldData)
                        .generateTbody();
                } else {
                    throw message.report;
                }
            },
            error: function (e, type, code) {
                var message = "<?=__("Une erreur a eu lieu lors de la reprise")?>";
                if (code === 'Forbidden') {
                    message = "<?=__("Vous n'avez pas la permission d'effectuer cette action")?>";
                }
                tr.addClass('danger')
                    .find('td.statetrad')
                    .text(message);
            }
        });
    }

    function parseArrayData(value) {
        if (!value) {
            return;
        }
        var table = $('<table class="parsed-json-data"></table>');
        var tbody = $('<tbody></tbody>');
        table.append(tbody);
        var tr;
        if (typeof value === 'string') {
            tbody.append($('<tr></tr>').append($('<td colspan="2">').append(value)));
        } else {
            for (var key in value) {
                tbody.append(
                    $('<tr></tr>')
                        .append($('<th></th>').append(key+':'))
                        .append($('<td></td>').append(value[key]))
                );
            }
        }
        return table;
    }

    function getJobInfo(id, jobid) {
        var table = {};
        table = <?=$jsTable?>;
        var tr = table.table.find('tr[data-id="'+jobid+'"]');
        tr.addClass('alert')
            .addClass('alert-warning')
            .find('button')
            .remove();

        TableGenerator.appendActions(table.data, table.actions);
        table.generateAll();
        $.ajax({
            url: '<?=$this->Url->build('/tasks/admin-job-info')?>/'+id,
            success: function(content) {
                if (content) {
                    table.replaceDataId(jobid, content);
                } else {
                    table.getDataId(jobid).statetrad = __("Terminé");
                }
                TableGenerator.appendActions(table.data, table.actions);
                table.generateAll();
                setTimeout(function () {
                    table.table.find('tr[data-id="'+jobid+'"] td')
                        .css('background-color', '#5cb85c')
                        .animate({'background-color': 'transparent'});

                }, 0);
            },
            error: function(e) {
                console.error(e);
                alert(e.statusText);
            }
        });
    }
    function parseErrors(value) {
        if (!value) {
            return;
        }
        value = value.replace(/ <?=preg_quote(ROOT, '/')?>/g, ' APP');
        var pos = value.indexOf('Stack trace:');
        if (pos) {
            var trace = value.substr(pos + 13).split("\n");
            var ul = $('<ul></ul>').css('display', 'none');
            var button = $('<button class="btn btn-default" type="button"></button>')
                .text('Stack trace')
                .on('click', function() {
                    ul.toggle();
                });
            for (var i = 0; i < trace.length; i++) {
                var split = trace[i].split(':', 2);
                ul.append(
                    (function (i, split) {
                        return $('<li></li>')
                            .text(split[0].substr(split[0].lastIndexOf('/') +1))
                            .on('mouseover', function() {
                                $(this).html(trace[i]);
                            })
                            .on('mouseout', function() {
                                $(this).text(split[0].substr(split[0].lastIndexOf('/') +1));
                            });
                    })(i, split)
                );
            }
            value = value.substr(0, pos).replace("\n", '<hr>');
        }
        var m = value.match(/^((?:\w+\\)*\w*Exception: )(.*) in APP(.*)/);
        if (m) {
            value = '<h4>'+m[1]+'</h4><p>'+m[2]+'</p><code>APP'+m[3]+'</code>';
        }
        if (pos) {
            value = $('<p></p>').html(value).append(button).append(ul);
        }
        return value;
    }
</script>
<?php
require 'ajax_admin.php';
