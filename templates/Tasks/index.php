<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Core\Configure;

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Menu utilisateur"));
$this->Breadcrumbs->add(__("Mes jobs en attente"));

// Titre de la page
echo $this->Html->tag(
    'div.container',
    $this->Html->tag('h1', $this->Fa->i('fa-tasks', __("Mes jobs en attente")))
    . $this->Breadcrumbs->render()
);

echo $this->element('modal', ['idTable' => $tableId]);

$jsTable = $this->Table->getJsTableObject($tableId);

echo $this->Html->tag('div.container');

echo $this->Form->control(
    'auto-reload-index-tasks',
    [
        'label' => __("Rechargement auto de la page"),
        'type' => 'checkbox',
    ]
);

echo $this->Html->tag('/div');

echo $this->Filter->create('index-jobs-filter')
    ->saves($savedFilters)
    ->filter(
        'tube',
        [
            'label' => __("Tubes"),
            'type' => 'select',
            'multiple' => true,
            'options' => $tubes,
        ]
    )
    ->filter(
        'job_state',
        [
            'label' => __("Etat"),
            'type' => 'select',
            'multiple' => true,
            'options' => $states
        ]
    )
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date'
        ]
    )
    ->generateSection();
?>
<script>
    $(function() {
        AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');
    });

    AsalaeWebsocket.connect(
        '<?=Configure::read('Ratchet.connect')?>',
        function(session) {
            ratchet = session;
            session.subscribe('work_begin', function(topic, data) {
                var table;
                table = <?=$jsTable?>;
                var tr = $(table.table).find('tr[data-id="'+data.message.jobid+'"]');
                if (tr.length) {
                    tr.addClass('alert')
                        .addClass('alert-warning');
                    tr.find('td.state').text(__("En cours"));
                    table.getDataId(data.message.jobid).statetrad = __("En cours");
                }
            });
            session.subscribe('work_end', function(topic, data) {
                var table;
                table = <?=$jsTable?>;
                var tr = $(table.table).find('tr[data-id="'+data.message.jobid+'"]');
                if (tr.length) {
                    var notSync = $(table.table)
                        .find(
                            'tr.alert-warning.not-sync.tube-'+data.message.tube
                            +':not([data-id="'+data.message.jobid+'"])'
                        );
                    if (notSync.length) {
                        notSync.removeClass('alert')
                            .removeClass('alert-warning')
                            .addClass('gray')
                            .disable()
                            .find('td.state')
                            .text('???');
                        tr.find('td.state').text(__("En cours"));
                        table.getDataId(data.message.jobid).statetrad = __("En cours");
                    }
                    if (data.message.error_message) {
                        tr.removeClass('alert-warning')
                            .addClass('alert-danger')
                            .find('td.error')
                            .text(data.message.error_message);
                        tr.find('td.state').text(__("En erreur"));
                    } else {
                        tr.removeClass('alert').removeClass('alert-warning').addClass('gray').disable();
                        tr.find('a').disable();
                        tr.find('td.state').text(__("Terminé"));
                        table.getDataId(data.message.jobid).statetrad = __("Terminé");
                    }
                    if ($('#auto-reload-index-tasks').prop('checked') && tr.is(':last-child')) {
                        AsalaeGlobal.updatePaginationAjax('#<?=$tableId?>-section');
                        $('#alert-job-container').hide(400, function() {
                            $(this).remove();
                        });
                    }
                }
            });
        },
        function() {
        },
        {'skipSubprotocolCheck': true}
    );

    function parseArrayData(value) {
        if (!value) {
            return;
        }
        var table = $('<table class="parsed-json-data"></table>');
        var tbody = $('<tbody></tbody>');
        table.append(tbody);
        var tr;
        if (typeof value === 'string') {
            tbody.append($('<tr></tr>').append($('<td colspan="2">').append(value)));
        } else {
            for (var key in value) {
                tbody.append(
                    $('<tr></tr>')
                        .append($('<th></th>').append(key+':'))
                        .append($('<td></td>').append(value[key]))
                );
            }
        }
        return table;
    }

    function getJobInfo(id, jobid) {
        var table = {};
        table = <?=$jsTable?>;
        var tr = table.table.find('tr[data-id="'+jobid+'"]');
        tr.addClass('alert')
            .addClass('alert-warning')
            .find('button')
            .remove();

        TableGenerator.appendActions(table.data, table.actions);
        table.generateAll();
        $.ajax({
            url: '<?=$this->Url->build('/tasks/my-job-info')?>/'+id,
            success: function(content) {
                if (content) {
                    table.replaceDataId(jobid, content);
                } else {
                    table.getDataId(jobid).statetrad = __("Terminé");
                }
                TableGenerator.appendActions(table.data, table.actions);
                table.generateAll();
                setTimeout(function () {
                    table.table.find('tr[data-id="'+jobid+'"] td')
                        .css('background-color', '#5cb85c')
                        .animate({'background-color': 'transparent'});
                }, 0);
            },
            error: function(e) {
                console.error(e);
                alert(e.statusText);
            }
        });
    }
</script>
<?php
require 'ajax_index.php';
