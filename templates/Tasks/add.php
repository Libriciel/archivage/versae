<?php

/**
 * @var Versae\View\AppView $this
 */

$idPrefix = 'add-job-beanstalk';
echo $this->Form->create($entity, ['idPrefix' => $idPrefix]);

$tmplInputContainer = 'add-tmpl-key-value-pair';

require 'addedit-common.php';
echo $this->Form->end();
