<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Core\Configure;

$advanced = Configure::read('Tasks.admin.advanced', false);

$view = $this;
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'tube' => [
                'label' => __("Tube"),
                'order' => 'tube',
                'filter' => [
                    'tube[0]' => [
                        'id' => 'filter-tube-0',
                        'label' => false,
                        'aria-label' => __("Tube"),
                        'options' => $tubes,
                    ],
                ],
            ],
            'data' => [
                'label'    => __("Data"),
                'callback' => 'parseArrayData',
            ],
            'user.username' => [
                'label'    => __("Utilisateur"),
                'filter' => [
                    'user_id[0]' => [
                        'id' => 'filter-user_id-0',
                        'label' => false,
                        'aria-label' => __("Utilisateur"),
                        'options' => $users,
                    ],
                ],
            ],
            'statetrad' => [
                'label' => __("Statut"),
                'class' => 'state',
                'order' => 'job_state',
                'titleEval' => 'state',
                'filter' => [
                    'job_state[0]' => [
                        'id' => 'filter-job_state-0',
                        'label' => false,
                        'aria-label' => __("Statut"),
                        'options' => $job_states
                    ],
                ],
            ],
            'created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'order' => 'created',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'created[0]' => [
                        'id' => 'filter-created-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator('dateoperator_created[0]', '>='),
                        'append' => $this->Date->picker('#filter-created-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'created[1]' => [
                        'id' => 'filter-created-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_created[1]', '<='),
                        'append' => $this->Date->picker('#filter-created-1'),
                        'class' => 'datepicker with-select',
                    ],
                ]
            ],
            'errors' => [
                'label' => __("Erreurs"),
                'class' => 'error',
                'callback' => 'parseErrors'
            ],
            'age' => ['display' => false, 'order' => 'age'],
            'delay' => ['display' => false, 'order' => 'delay'],
            'ttr' => ['display' => false, 'order' => 'ttr'],
            'jobid' => ['display' => false, 'order' => 'jobid'],
            'priority' => ['display' => false, 'order' => 'priority'],
        ]
    )
    ->data($all)
    ->params(
        [
            'identifier' => 'jobid',
            'classEval' => "(data[{index}].state === 'reserved' "
                . "? 'alert alert-warning not-sync tube-'+data[{index}].tube "
                . ": '') + (data[{index}].statetrad === __('Terminé') ? ' gray disabled' : '')",
            'checkbox' => true
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionEditJob({0}, {1})",
                'type' => 'button',
                'class' => 'btn-link',
                'data-action' => __("Modifier"),
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier le job"),
                'aria-label' => $title,
                'display' => $advanced && $view->Acl->check('/tasks/edit'),
                'displayEval' => "data[{index}].state === 'failed'",
                'params' => ['id', 'jobid']
            ],
            [
                'onclick' => "viewTransfer({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'data-action' => __("Transfert"),
                'title' => $title = __("Visualiser le transfert"),
                'aria-label' => $title,
                'display' => $this->Acl->check('/transfers/view'),
                'displayEval' => 'data[{index}].data.transfer_id',
                'params' => ['data.transfer_id']
            ],
            [
                'onclick' => "loadViewUser({0})",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'data-action' => __("Utilisateur"),
                'title' => $title = __("Visualiser l'utilisateur"),
                'aria-label' => $title,
                'display' => $this->Acl->check('/users/view'),
                'displayEval' => 'data[{index}].data.user_id',
                'params' => ['data.user_id']
            ],
            [
                'onclick' => "viewArchive({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'data-action' => __("Archive (entrée)"),
                'title' => $title = __("Visualiser {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/archives/view'),
                'displayEval' => 'data[{index}].data.archive_id',
                'params' => ['data.archive_id'],
            ],
            [
                'data-callback' => "jobPause({0}, {1})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-pause'),
                'title' => $title = 'Pause',
                'aria-label' => $title,
                'confirm' => __("Mettre en pause le job ?"),
                'display' => $advanced && $view->Acl->check('/tasks/ajax-pause'),
                'displayEval' => "data[{index}].state === 'ready'",
                'params' => ['id', 'jobid']
            ],
            [
                'data-callback' => "jobResume({0}, {1})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-play'),
                'title' => $title = __("Reprendre le job"),
                'aria-label' => $title,
                'confirm' => __("Reprendre le job ?"),
                'display' => $advanced && $view->Acl->check('/tasks/ajax-resume'),
                'displayEval' => "data[{index}].state === 'failed'",
                'params' => ['id', 'jobid']
            ],
            [
                'data-callback' => "jobDelete({0}, {1})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Annuler le job"),
                'aria-label' => $title,
                'confirm' => __("Annuler le job ?"),
                'display' => $advanced && $view->Acl->check('/tasks/ajax-cancel'),
                'displayEval' => "['ready', 'failed', 'delayed'].indexOf(data[{index}].state) >= 0",
                'params' => ['id', 'jobid']
            ],
            [
                'onclick' => "getJobInfo({0}, {1})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-refresh'),
                'title' => $title = __("Actualiser"),
                'aria-label' => $title,
                'display' => $view->Acl->check('/tasks/adminJobInfo'),
                'params' => ['id', 'jobid']
            ],
        ]
    );

echo $this->element(
    'section_table',
    [
        'id' => $tableId . '-section',
        'title' => __("Liste des jobs en cours"),
        'table' => $table,
        'pagination' => [
            'base-id' => 'paginate-' . $tableId,
            'options' => [
                'model' => 'BeanstalkJobs',
                'actions' => [
                    '0' => __("-- action groupée --"),
                    '1' => __("Mettre en pause"),
                    '2' => __("Reprendre le job"),
                    '3' => __("Supprimer"),
                ],
            ],
            'callback' => 'updatePaginationAjaxCallback'
        ]
    ]
);
