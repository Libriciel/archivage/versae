<?php

/**
 * @var Versae\View\AppView $this
 */

$idPrefix = 'edit-job-beanstalk';
echo $this->Form->create($entity, ['idPrefix' => $idPrefix]);

$tmplInputContainer = 'edit-tmpl-key-value-pair';

require 'addedit-common.php';
echo $this->Form->end();
