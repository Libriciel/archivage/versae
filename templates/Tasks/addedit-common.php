<?php
/**
 * @var Versae\View\AppView $this
 */
try {
    $customInput = $entity->get('data');
} catch (Exception $e) {
    echo $this->Html->tag('div.alert.alert-danger', __("Le job n'existe plus"));
    return;
}
unset($customInput['user_id'], $customInput['silent']);
if ($customInput) {
    $i = 0;
    $fields = '';
    foreach ($customInput as $key => $value) {
        $idContainer = $i === 0 ? ' id="' . $tmplInputContainer . '"' : '';
        $inputKey = $this->Form->control(
            'mix.' . $i . '.key',
            [
                'class' => 'key-value-pair',
                'placeholder' => 'key',
                'label' => [
                    'text' => 'key',
                    'class' => 'sr-only'
                ],
                'templates' => [
                    'inputContainer' => '{{content}}',
                    'formGroup' => '{{label}}{{input}}',
                ],
                'val' => $key,
            ]
        );
        $fields .= $this->Form->control(
            'mix.' . $i . '.value',
            [
                'label' => __("Champ additionnel"),
                'class' => 'key-value-pair',
                'placeholder' => 'value',
                'templates' => [
                    'inputContainer' => '<div' . $idContainer
                        . ' class="form-group mix-key-value {{type}}{{required}}">{{content}}</div>',
                    'formGroup' => '{{label}}<div>{{prepend}}{{input}}{{append}}</div>',
                    'inputGroupAddons' => '{{content}}',
                    'inputGroupStart' => '{{prepend}}',
                    'inputGroupEnd' => '{{append}}',
                ],
                'prepend' => $inputKey,
                'val' => $value,
            ]
        );
        $i++;
    }
} else {
    $key = $this->Form->control(
        'mix.0.key',
        [
            'class' => 'key-value-pair',
            'placeholder' => 'key',
            'label' => [
                'text' => 'key',
                'class' => 'sr-only'
            ],
            'templates' => [
                'inputContainer' => '{{content}}',
                'formGroup' => '{{label}}{{input}}',
            ]
        ]
    );
    $fields = $this->Form->control(
        'mix.0.value',
        [
            'label' => __("Champ additionnel"),
            'class' => 'key-value-pair',
            'placeholder' => 'value',
            'templates' => [
                'inputContainer' => '<div id="' . $tmplInputContainer
                    . '" class="form-group mix-key-value {{type}}{{required}}">{{content}}</div>',
                'formGroup' => '{{label}}<div>{{prepend}}{{input}}{{append}}</div>',
                'inputGroupAddons' => '{{content}}',
                'inputGroupStart' => '{{prepend}}',
                'inputGroupEnd' => '{{append}}',
            ],
            'prepend' => $key
        ]
    );
}

echo $this->Form->control(
    'tube',
    [
        'type' => 'select',
        'empty' => __("-- Veuillez choisir un tube --"),
        'options' => $tubes,
        'required' => true,
        'default' => false,
    ]
)
    . $this->Form->control(
        'priority',
        [
            'type' => 'number',
            'min' => 1,
            'step' => 1,
            'required'
        ]
    )
    . $this->Form->control(
        'delay',
        [
            'label' => __("Delay (s)"),
            'type' => 'number',
            'min' => 0,
            'step' => 1,
            'required'
        ]
    )
    . $this->Form->control(
        'ttr',
        [
            'label' => __("Time to run (s)"),
            'type' => 'number',
            'min' => 1,
            'step' => 1,
            'required'
        ]
    )
    . $this->Form->control(
        'data.silent',
        [
            'label' => __("Notification désactivée ?"),
            'type' => 'checkbox',
        ]
    )
    . $this->Form->control(
        'data.user_id',
        [
            'label' => __("Utilisateur"),
            'empty' => __("-- Veuillez sélectionner un utilisateur --"),
            'options' => $users,
            'required' => true,
        ]
    )
    . $fields;
?>
<script>
    var tmplKeyValuePair = $('#<?=$tmplInputContainer?>');

    function addKeyValuePair() {
        var fieldsdivs = $(this).closest('form').find('.mix-key-value');
        var emptyfieldsdivs = fieldsdivs.filter(function() {
            return $(this).find('.key-value-pair').filter(function() {
                return !$(this).val();
            }).length >= 2;
        }).toArray();
        while (emptyfieldsdivs.length > 1) {
            emptyfieldsdivs.pop().remove();
        }
        if (emptyfieldsdivs.length === 0) {
            var nDiv = tmplKeyValuePair.clone();
            var index = fieldsdivs.length;
            nDiv.attr('id', '');
            nDiv.find('label').attr('for', '<?=$idPrefix?>-mix-'+index+'-value');
            nDiv.find('input[name="mix[0][key]"]')
                .attr('name', 'mix['+index+'][key]')
                .attr('id', 'add-job-beanstalk-mix-'+index+'-key')
                .val('');
            nDiv.find('label.sr-only').attr('for', '<?=$idPrefix?>-mix-'+index+'-key');
            nDiv.find('input[name="mix[0][value]"]')
                .attr('name', 'mix['+index+'][value]')
                .attr('id', '<?=$idPrefix?>-mix-'+index+'-value')
                .val('');
            nDiv.find('input').change(addKeyValuePair);
            fieldsdivs.last().after(nDiv);
        }
    }
    tmplKeyValuePair.find('input').change(addKeyValuePair).trigger('change');
</script>
