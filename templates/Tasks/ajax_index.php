<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->Html->tag('div#alert-job-container.container');
echo $this->Html->tag(
    'p.alert.alert-info',
    __(
        "Il y a actuellement un total de {0} jobs en attente et {1} jobs en erreur.",
        $countReady,
        $countFailed
    )
);
echo $this->Html->tag('/div');

$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'tube' => [
                'label' => __("Tube"),
                'order' => 'tube',
                'filter' => [
                    'tube[0]' => [
                        'id' => 'filter-tube-0',
                        'label' => false,
                        'aria-label' => __("Tube"),
                        'options' => $tubes,
                    ],
                ],
            ],
            'data' => [
                'label'    => __("Data"),
                'callback' => 'parseArrayData',
            ],
            'job_statetrad' => [
                'label' => __("Statut"),
                'class' => 'state',
                'order' => 'job_state',
                'titleEval' => 'state',
                'filter' => [
                    'job_state[0]' => [
                        'id' => 'filter-job_state-0',
                        'label' => false,
                        'aria-label' => __("Statut"),
                        'options' => $states
                    ],
                ],
            ],
            'created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'order' => 'created',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'filter' => [
                    'created[0]' => [
                        'id' => 'filter-created-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator('dateoperator_created[0]', '>='),
                        'append' => $this->Date->picker('#filter-created-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'created[1]' => [
                        'id' => 'filter-created-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_created[1]', '<='),
                        'append' => $this->Date->picker('#filter-created-1'),
                        'class' => 'datepicker with-select',
                    ],
                ]
            ],
            'errors' => [
                'label' => __("Erreurs"),
                'class' => 'error',
                'callback' => '(function(value) { return value ? (""+value).replace(/\n/g, "<hr>") : null; })'
            ],
            'age' => ['display' => false, 'order' => 'age'],
            'delay' => ['display' => false, 'order' => 'delay'],
            'ttr' => ['display' => false, 'order' => 'ttr'],
            'jobid' => ['display' => false, 'order' => 'jobid'],
            'priority' => ['display' => false, 'order' => 'priority'],
        ]
    )
    ->data($all)
    ->params(
        [
            'identifier' => 'jobid',
            'classEval' => "(data[{index}].state === 'reserved' "
                . "? 'alert alert-warning not-sync tube-'+data[{index}].tube "
                . ": '') + (data[{index}].statetrad === __('Terminé') ? ' gray disabled' : '')",
            'checkbox' => true
        ]
    )
    ->actions(
        [
            [
                'onclick' => "getJobInfo({0}, {1})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-refresh'),
                'title' => $title = __("Actualiser"),
                'aria-label' => $title,
                'display' => $this->Acl->check('/tasks/myJobInfo'),
                'params' => ['id', 'jobid']
            ],
        ]
    );

echo $this->element(
    'section_table',
    [
        'id' => $tableId . '-section',
        'title' => __("Liste de mes jobs en attente"),
        'table' => $table,
        'pagination' => [
            'options' => [
                'model' => 'BeanstalkJobs',
            ]
        ]
    ]
);
