<?php

/**
 * @var Versae\View\AppView $this
 */

$filters = $this->Filter->create($helperConfig['id']);
$filters->saveList = $helperConfig['saves'];
$filters->filters = $helperConfig['filters'];

echo $filters->loadFilters($savedFilters)->generateFilters();
