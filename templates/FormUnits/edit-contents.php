<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Datasource\EntityInterface;
use Versae\Model\Table\FormUnitsTable;

if ($entity->get('type') === FormUnitsTable::TYPE_DOCUMENT_MULTIPLE) {
    return $this->Html->tag('div.alert.alert-info', __("Pas de contenu sur Lot de documents"));
}

$jsTableVariables = $this->Table->getJsTableObject($jsTableContentId = 'edit-form-unit-content-table');

foreach ($dataContents as $key => $content) {
    if ($content instanceof EntityInterface) {
        $content = $content->toArray();
    }
    $dataContents[$key] = $content + ['type' => 'content'];
}
foreach ($dataHeaders as $key => $header) {
    if ($header instanceof EntityInterface) {
        $header = $header->toArray();
    }
    $dataHeaders[$key] = $header + ['type' => 'header'];
}

// Filtre et tri les données
$tableContentsData = [];
foreach (array_merge($dataHeaders, $dataContents) as $key => $content) {
    if ($content['additionnal'] && !$content['id']) {
        continue;
    }
    if (!preg_match('/^(\w+)_(\d+)$/', $content['name'], $m)) {
        if (str_ends_with($content['name'], '_{n}')) {
            $m = [null, strstr($content['name'], '_{n}', true), '0'];
            $content['name'] = $m[1];
        } else {
            $m = [null, $content['name'], '0'];
        }
    }
    [, $name, $index] = $m;
    $tableContentsData[$name][(int)$index] = $content;
}

$data = [];
foreach ($tableContentsData as $elements) {
    foreach ($elements as $params) {
        $data[] = $params;
    }
}

$tableContents = $this->Table
    ->create($jsTableContentId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'label' => [
                'label' => __("Nom"),
                'callback' => 'requiredCallback',
                'titleEval' => 'name',
            ],
            'form_variable.twig' => [
                'label' => __("Twig"),
                'callback' => 'twigForm',
                'class' => 'twig',
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'name',
            'checkbox' => false,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionAddEditContentUnit('$id/{0}')",
                'type' => 'button',
                'class' => 'btn-link text-success',
                'label' => $this->Fa->charte('Ajouter'),
                'title' => $title = __("Ajouter {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/form-calculators/add-edit-content-unit'),
                'displayEval' => 'data[{index}].id === null && data[{index}].type === "content"',
                'params' => ['name', 'label']
            ],
            [
                'onclick' => "actionAddEditContentUnit('$id/{0}')",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/form-calculators/add-edit-content-unit'),
                'displayEval' => 'data[{index}].id !== null && data[{index}].type === "content"',
                'params' => ['id', 'label']
            ],
            [
                'data-callback' => sprintf(
                    "actionDeleteContentUnit(%s, '%s')('{0}', '{2}')",
                    $jsTableVariables,
                    $this->Url->build('/form-calculators/delete-unit-content')
                ),
                'type' => 'button',
                'class' => 'btn-link delete',
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'confirm' => __("Supprimer cette variable ?"),
                'display' => $this->Acl->check('/form-calculators/delete-unit-content'),
                'displayEval' => 'data[{index}].id !== null && data[{index}].type === "content"',
                'params' => ['id', 'label', 'name']
            ],
            [
                'onclick' => "actionAddEditVariableUnit('$id/{0}')",
                'type' => 'button',
                'class' => 'btn-link text-success',
                'label' => $this->Fa->charte('Ajouter'),
                'title' => $title = __("Ajouter {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/form-calculators/add-edit-header-unit'),
                'displayEval' => 'data[{index}].id === null && data[{index}].type === "header"',
                'params' => ['name', 'label']
            ],
            [
                'onclick' => "actionAddEditVariableUnit('$id/{0}')",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/form-calculators/add-edit-header-unit'),
                'displayEval' => 'data[{index}].id !== null && data[{index}].type === "header"',
                'params' => ['id', 'label']
            ],
            [
                'data-callback' => sprintf(
                    "actionDeleteHeaderUnit(%s, '%s')('{0}', '{2}')",
                    $jsTableVariables,
                    $this->Url->build('/form-calculators/delete-unit-header')
                ),
                'type' => 'button',
                'class' => 'btn-link delete',
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'confirm' => __("Supprimer cette variable ?"),
                'display' => $this->Acl->check('/form-calculators/delete-unit-header'),
                'displayEval' => 'data[{index}].id !== null && data[{index}].type === "header"',
                'params' => ['id', 'label', 'name']
            ],
        ]
    );

$contents = $this->Html->tag(
    'header',
    $this->Html->tag('h2.h4', __("Liste du contenu"))
);

$contents .= $tableContents->generate();

$encodedData = json_encode($dataContents ?? []);

$contents .= $this->Html->tag('div#add_other_content_div');

$contents .= $this->Form->control(
    'add_other_content',
    [
        'label' => __("Ajouter une métadonnée"),
        'options' => [], // ajouté par JS
        'empty' => __("-- Sélectionner une métadonnée à ajouter --"),
        'onchange' => "addAdditionnalContentData($jsTableVariables, $(this))",
    ]
);

$contents .= $this->Html->tag('/div');

$entityId = $entity->id;
/** @noinspection JSUnusedAssignment faux positif */
$contents .= <<<HTML
<script>
    var dataTableContent = $encodedData;
    var optionsAddData = {};
    var addContentToTable = $('select#add-other-content');
    var links = {};

    // extrait les champs additionnels des données du tableau 
    for (let i in dataTableContent) {
       // multiple
        if (
            (dataTableContent[i].additionnal && dataTableContent[i].multiple)
            || (dataTableContent[i].multiple && dataTableContent[i].name.match(/_(\d+)/)) 
            || (dataTableContent[i].multiple && dataTableContent[i].name.match(/_\{n\}/)) 
        ) {
            let currentIndex = 0;
            let genericName = dataTableContent[i].name;
            let match = genericName.match(/_(\d+)/);
            if (match) {
                currentIndex = parseInt(match[1]);
                genericName = genericName.replace(/_\d+/, '_{n}');
            }
            
            // avec classe
            if (dataTableContent[i].links) {
                let className = genericName.match(/(.*)_{n}(.*)/)[1];
                optionsAddData[className] = optionsAddData[className] ?? {};

                if (typeof optionsAddData[className][genericName] === 'undefined') {
                    let text = dataTableContent[i].genericLabel !== genericName
                        ? genericName + ' - ' + dataTableContent[i].genericLabel
                        : genericName;

                    optionsAddData[className][genericName] = {
                        value: genericName,
                        label: dataTableContent[i].genericLabel,
                        text: text,
                        multiple: true,
                        indexes: dataTableContent[i].id ? [currentIndex] : [],
                    };

                    // ajout des links dans un tableau de référence
                    if (!links[className]) {
                        links[className] = dataTableContent[i].links ? dataTableContent[i].links.split(',') : [];
                    } 
                } else {
                    optionsAddData[className][genericName].indexes.push(currentIndex);
                }
            // sans classe
            } else {
                if (typeof optionsAddData[genericName] === 'undefined') {
                    let text = dataTableContent[i].genericLabel !== genericName
                        ? genericName + ' - ' + dataTableContent[i].genericLabel
                        : genericName;

                    optionsAddData[genericName] = {
                        value: genericName,
                        label: dataTableContent[i].genericLabel,
                        text: text,
                        multiple: true,
                        index: currentIndex,
                    };
                } else {
                    optionsAddData[genericName].index = Math.max(optionsAddData[genericName].index + 1, currentIndex);
                }
            }

        // pas multiple
        } else if (dataTableContent[i].additionnal && !dataTableContent[i].id) {
            let text = dataTableContent[i].genericLabel !== dataTableContent[i].name
                ? dataTableContent[i].name + ' - ' + dataTableContent[i].genericLabel
                : dataTableContent[i].name;
            optionsAddData[dataTableContent[i].name] = {
                value: dataTableContent[i].name,
                label: dataTableContent[i].genericLabel,
                text: dataTableContent[i].name + ' - ' + dataTableContent[i].genericLabel,
                multiple: false
            };
        }
    }

    // replace _{n}_ par l'index + 1 
    for (let [i, field] of Object.entries(optionsAddData)) {
        if (!field.value) {
            let indexes = [...new Set(Object.values(field).map(x => x.indexes).flat())];
            if (!indexes.length) {
                let optGroup = $('<optgroup>')
                    .attr('label', i + ' N°' + 1);
                for (let sub in field) {
                    let value = field[sub].value;
                    let text = field[sub].text;
                    value = value.replace(/_\{n\}/g, '_1');
                    optGroup.append(
                        $('<option>')
                            .attr('value', value)
                            .attr('title', value)
                            .attr('data-field', sub)
                            .text(text.replace(/_\{n\}/g, '_1'))
                    );
                }
                addContentToTable.append(optGroup);
            } else {
                for (var index of indexes) {
                    let optGroup = $('<optgroup>')
                        .attr('label', i + ' N°' + index);
                    // append dans le optgroup
                    for (let [sub, f] of Object.entries(field).sort()) {
                        // vérifier dans indexes
                        if (f.indexes.includes(index)) {
                            continue;
                        };

                        let value = f.value;
                        let text = f.text;
                        value = value.replace(/_\{n\}/g, '_' + index);
                        text = text.replace(/_\{n\}/g, '_' + index);
                        optGroup.append(
                            $('<option>')
                                .attr('value', value)
                                .attr('title', value)
                                .attr('data-field', sub)
                                .text(text)
                        );
                    }
                    addContentToTable.append(optGroup);
                }
                let newIndex = index + 1;
                let optGroup = $('<optgroup>')
                    .attr('label', i + ' N°' + newIndex); 

                // append dans le optgroup
                for (let sub in field) {
                    let value = field[sub].value;
                    let text = field[sub].text;
                    value = value.replace(/_\{n\}/g, '_' + newIndex);
                    text = text.replace(/_\{n\}/g, '_' + newIndex);
                    optGroup.append(
                        $('<option>')
                            .attr('value', value)
                            .attr('title', value)
                            .attr('data-field', sub)
                            .text(text)
                    );
                }
                addContentToTable.append(optGroup);
            }
        } else {
            let value = field.value;
            let text = field.text;
            if (field.multiple) {
                let newIndex = field.index + 1;
                value = value.replace(/_\{n\}/g, '_' + newIndex);
                text = text.replace(/_\{n\}/g, '_' + newIndex);
            }
            addContentToTable.append(
                $('<option>')
                    .attr('value', value)
                    .attr('title', value)
                    .attr('data-field', i)
                    .text(text)
            );
        }
    }

    if (!Object.keys(optionsAddData).length) {
        $('#add_other_content_div').remove();
    }

    function addAdditionnalContentData(table, element) {
        let value = element.val();
        if (!value) {
            return;
        }

        let option = element.find('option[value="'+ value +'"]');
        let genericName = option.attr('data-field');

        let split = genericName.split('_{n}_');
        let entry = split.length > 1 ? optionsAddData[split[0]][genericName] : optionsAddData[genericName];
        if (entry.multiple) {
            let newIndex = parseInt(value.match(/_(\d+)/)[1]) + 1;

            // si liés, on ajoute tout un nouveau group à index + 1
            let groupExists = $('optgroup[label="' + split[0] + ' N°' + newIndex + '"]').length > 0;;
            if (links[split[0]] && !groupExists) {
                let optGroup = $('<optgroup>')
                    .attr('label', split[0] + ' N°' + newIndex);
                for (let link of links[split[0]].sort()) {
                    optGroup.append(
                        $('<option></option>')
                            .val(optionsAddData[split[0]][link].value.replace(/_\{n\}/g, '_' + newIndex))
                            .attr('data-field', genericName)
                            .attr('title', optionsAddData[split[0]][link].value.replace(/_\{n\}/g, '_' + newIndex))
                            .text(optionsAddData[split[0]][link].text.replace(/_\{n\}/g, '_' + newIndex))
                    );

                }
                addContentToTable.append(optGroup);
                option.remove();
            } else if (!links[split[0]]) {
                let val = entry.value.replace(/{n}/g, newIndex); 
                option.attr('value', val);
                option.attr('title', val);
                option.text(entry.text.replace(/{n}/g, newIndex));

                setTimeout( // sinon bug
                   () => AsalaeGlobal.select2(addContentToTable),
                   500 
                );
            } else {
                option.remove();
            }
        } else {
            option.remove();
        }
        table.data.push({id: null, name: value, label: value, type: 'content'});
        table.generateAll();
        element.val('').change();

        actionAddEditContentUnit('$entityId/'+value);
    }

    AsalaeGlobal.select2(addContentToTable);
</script>
HTML;

return $contents;
