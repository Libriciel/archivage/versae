<?php

/**
 * @var Versae\View\AppView $this
 */

use Versae\Model\Table\FormUnitsTable;

if ($entity->get('type') === FormUnitsTable::TYPE_DOCUMENT_MULTIPLE) {
    return $this->Html->tag('div.alert.alert-info', __("Pas de mot clés sur Lot de documents"));
} elseif ($entity->get('type') === FormUnitsTable::TYPE_DOCUMENT && $form->get('seda_version') === 'seda1.0') {
    return $this->Html->tag('div.alert.alert-info', __("Pas de mot clés sur Document en SEDA 1.0"));
}

$jsTableVariables = $this->Table->getJsTableObject($jsTableKeywordId = 'edit-form-unit-keyword-table');

$tableKeywords = $this->Table
    ->create($jsTableKeywordId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'name' => [
                'label' => __("Nom"),
            ],
            'form_unit_keyword_details' => [
                'label' => __("Variables"),
                'callback' => 'keywordVars',
            ],
            'multiple_with' => [
                'label' => __("Champ multiple"),
            ],
        ]
    )
    ->data($keywords)
    ->params(
        [
            'identifier' => 'id',
            'checkbox' => false,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionEditFormKeyword('{0}')",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/form-unit-keywords/edit'),
                'params' => ['id', 'label']
            ],
            [
                'data-callback' => sprintf(
                    "actionDeleteKeyword(%s, '%s')('{0}')",
                    $jsTableVariables,
                    $this->Url->build('/form-unit-keywords/delete')
                ),
                'type' => 'button',
                'class' => 'btn-link delete',
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'confirm' => __("Supprimer cette variable ?"),
                'display' => $this->Acl->check('/form-unit-keywords/delete'),
                'displayEval' => 'data[{index}].id !== null',
                'params' => ['id', 'label']
            ],
        ]
    );

$addBtn = $this->ModalForm
    ->create('keyword-modal-' . uuid_create(), ['size' => 'large'])
    ->modal(__("Ajout d'un mot clé"))
    ->javascriptCallback('afterAddKeyword')
    ->output(
        'button',
        $this->Fa->charte('Ajouter', __("Ajouter un mot clé")),
        '/form-unit-keywords/add/' . $id
    )
    ->generate(['class' => 'btn btn-success', 'type' => 'button']);

$keywords = $this->Html->tag(
    'div',
    $addBtn,
    ['class' => 'separator padding10']
)

    . $this->Html->tag(
        'header',
        $this->Html->tag('h2.h4', __("Liste des mots clés"))
    );

$keywords .= $tableKeywords->generate();

return $keywords;
