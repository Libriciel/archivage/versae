<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Utility\Hash;
use Versae\Model\Table\FormUnitsTable;

?>
<script>
    function twigForm(value, context) {
        value = value && value.replace(/\n/g, '<span class="text-info">\\n</span><wbr/>');
        if (context.required) {
            return $('<div>').append(
                $('<input required>')
                    .css(
                        {
                            height: 0,
                            width: 0,
                            padding: 0,
                            border: 0,
                            opacity: 0
                        }
                    )
                    .attr('aria-hidden', 'true')
                    .val(value)
            ).append(value);
        }
        return value;
    }
    function requiredCallback(value, context) {
        if (context.required) {
            return $('<div class="form-group required">').append(value);
        }
        return value;
    }
    function keywordVars(value) {
        if (!value) {
            return '';
        }
        var dl = $('<dl>');
        // structure dl > div > dt + dd
        for (var i = 0; i < value.length; i++) {
            let div = $('<div>')
                .append($('<dt>').text(value[i].name+':'))
                .append($('<dd>').text(value[i].form_variable.twig));
            dl.append(div);
        }
        return dl;
    }
</script>
<?php
echo $this->ModalForm->create('edit-header-unit', ['size' => 'large'])
    ->modal(__("Modification d'une variable d'entête d'unité d'archives"))
    ->javascriptCallback('afterEditHeaderUnit')
    ->output('function', 'actionAddEditVariableUnit', '/form-calculators/add-edit-header-unit')
    ->generate();

echo $this->ModalForm->create('edit-management-unit', ['size' => 'large'])
    ->modal(__("Modification d'une variable de règle de gestion d'unité d'archives"))
    ->javascriptCallback('afterEditManagementUnit')
    ->output('function', 'actionAddEditManagementUnit', '/form-calculators/add-edit-management-unit')
    ->generate();

echo $this->ModalForm->create('edit-content-unit', ['size' => 'large'])
    ->modal(__("Modification d'une variable de contenu d'unité d'archives"))
    ->javascriptCallback('afterEditContentUnit')
    ->output('function', 'actionAddEditContentUnit', '/form-calculators/add-edit-content-unit')
    ->generate();

echo $this->ModalForm
    ->create('form-edit-keyword-modal', ['size' => 'large'])
    ->modal(__("Modifier un mot clé"))
    ->javascriptCallback('afterEditFormKeyword')
    ->output('function', 'actionEditFormKeyword', '/form-unit-keywords/edit')
    ->generate();

echo $this->ModalForm
    ->create('form-edit-keyword-detail-modal', ['size' => 'large'])
    ->modal(__("Modifier le contenu d'un mot clé"))
    ->javascriptCallback('afterEditFormKeywordDetail')
    ->output('function', 'actionAddEditKeywordDetail', '/form-calculators/add-edit-keyword-detail')
    ->generate();

$deleteBtn = $this->Html->tag(
    'button.p-2.btn-link.text-danger',
    $this->Fa->charte(
        'Supprimer',
        '',
        ['title' => $title = __("Supprimer cette unité d'archives")]
    )
    . $this->Html->tag(
        'span',
        $title,
        ['class' => 'sr-only']
    ),
    ['type' => 'button', 'onclick' => 'deleteFormUnit(' . $id . ')']
);

echo $this->Html->tag('h4', __("Modification d'un élément de l'arborescence") . $deleteBtn);
echo '<hr>';

$htmlForm = $this->Form->create(
    $entity,
    ['id' => 'form-edit-form-unit', 'idPrefix' => 'form-edit-form-unit']
);

$htmlForm .= $this->Form->control(
    'type',
    ['type' => 'hidden', 'id' => false]
);
$htmlForm .= $this->Form->control(
    'type',
    [
        'label' => __("Type d'élément"),
        'options' => $types,
        'empty' => __("-- Veuillez sélectionner un type --"),
        'disabled' => true,
        'style' => 'font-family: FontAwesome',
    ]
);
$htmlForm .= $this->Form->control(
    'name',
    [
        'label' => __("Nom de l'élément"),
    ]
);

if ($parent && Hash::get($parent, 'type') !== FormUnitsTable::TYPE_TREE_ROOT) {
    $htmlForm .= $this->Form->control(
        'parent_id',
        [
            'label' => __("Rattachée à"),
            'options' => $parent_ids,
            'default' => $parent_id,
        ]
        + ($isChildOfRepetatable ?? false
            ? []
            : ['empty' => __("<racine>")]
        )
    );
}
$htmlForm .= $this->Form->control(
    'order',
    [
        'label' => __("Position dans l'arborescence"),
        'empty' => __("Au tout début du noeud parent"),
        'options' => $orders,
        'default' => $orders ? array_keys($orders)[count($orders) - 1] : null,
    ]
);

$htmlForm .= $this->Html->tag('div#form-edit-form-unit-hiddable-form-input-id');
$htmlForm .= $this->Form->control(
    'form_input_id',
    [
        'label' => __("Champ de formulaire"),
        'options' => $formInputs,
        'empty' => __("-- Sélectionner un champ de formulaire --"),
    ]
);
$htmlForm .= $this->Html->tag('/div');

$htmlForm .= $this->Html->tag('div#form-edit-form-unit-hiddable-presence-condition-id');
$htmlForm .= $this->Form->control(
    'presence_condition_id',
    [
        'label' => __("Condition de présence"),
        'help' => __("Optionnel - Cet élément ne sera pas utilisé si la valeur est vide, nulle ou égale à 0"),
        'options' => $presence_conditions,
        'empty' => __("Toujours présent"),
    ]
);
$htmlForm .= $this->Html->tag('/div');

$htmlForm .= $this->Html->tag('div#form-edit-form-unit-hiddable-search-expression');
$htmlForm .= $this->Form->control(
    'search_expression',
    [
        'label' => __("Expression de recherche"),
        'help' => __("L'asterisque * remplace zéro à plusieurs caractères")
            . '<br>' . __(
                "Le séparateur pour les dossiers est le '' {0} '', "
                . "le 1er {0} est ignoré (ex: {0}Chemin{0}vers{0}*{0}fichier.txt)",
                DS
            )
    ]
);
$htmlForm .= $this->Form->control(
    'presence_required',
    [
        'label' => __("Présence obligatoire"),
    ]
);
$htmlForm .= $this->Html->tag('/div');

$htmlForm .= $this->Html->tag('div#form-edit-form-unit-hiddable-storage-path');
$htmlForm .= $this->Form->control(
    'storage_calculation',
    [
        'label' => __("Calcul du rangement des fichiers"),
        'options' => $storage_calculations,
    ]
);
$htmlForm .= $this->Html->tag('/div');

$htmlForm .= $this->Html->tag('div#form-edit-form-unit-repeatable-fieldset');
$htmlForm .=  $this->Form->control(
    'repeatable_fieldset_id',
    [
        'label' => __("Section répétable"),
        'options' => $repeatableFieldsets,
        'empty' => __("-- Sélectionner une section répétable --"),
        'required' => true,
    ]
);
$htmlForm .= $this->Html->tag('/div');

$htmlForm .= $this->Form->end();

$tabs = $this->Tabs->create('tabs-edit-form-unit', ['class' => 'row no-padding']);
$tabs->add(
    'tab-edit-form-unit-infos',
    $this->Fa->i('fa-file-code-o', __("Élément de l'arborescence")),
    $htmlForm
);
if (
    $entity->get('type') !== FormUnitsTable::TYPE_DOCUMENT_MULTIPLE
    && ($entity->get('type') !== FormUnitsTable::TYPE_DOCUMENT
    || $form->get('seda_version') === 'seda1.0')
) {
    $tabs->add(
        'tab-edit-form-unit-headers',
        $this->Fa->i('fa-newspaper-o', __("Descriptions")),
        include 'edit-contents.php'
    );
}
if (
    $entity->get('type') !== FormUnitsTable::TYPE_DOCUMENT_MULTIPLE
    && $entity->get('type') !== FormUnitsTable::TYPE_DOCUMENT
) {
    $tabs->add(
        'tab-edit-form-unit-managements',
        $this->Fa->i('fa-gavel', __("Règles de gestion")),
        include 'edit-managements.php'
    );
    $tabs->add(
        'tab-edit-form-unit-keywords',
        $this->Fa->i('fa-key', __("Mots clés")),
        include 'edit-keywords.php'
    );
}
echo $tabs;
?>
<script>
    /* global initialArchiveUnitForm */
    var disableRepeatable = <?= $disableRepeatable ? 'true' : 'false' ?>;
    var repeatableFieldsetId = <?=$repeatableFieldsetId ?: 'null'?>;
    archiveUnitForm = $('#form-edit-form-unit');
    var divInput = $('#form-edit-form-unit-hiddable-form-input-id');
    var divPresence = $('#form-edit-form-unit-hiddable-presence-condition-id');
    var divSearch = $('#form-edit-form-unit-hiddable-search-expression');
    var divStoragePath = $('#form-edit-form-unit-hiddable-storage-path');
    var repeatableFieldsetDiv = $('#form-edit-form-unit-repeatable-fieldset');
    var selectType = $('#form-edit-form-unit-type');
    var selectFormInput = $('#form-edit-form-unit-form-input-id');
    var selectFormSearchExp = $('#form-edit-form-unit-search-expression');
    var tableAddUnitHeaders;
    var tableAddUnitManagements;
    var tableAddUnitContents;
    var tableAddUnitKeywords;
    try {
        tableAddUnitHeaders = <?=$this->Table->getJsTableObject('edit-form-unit-content-table')?>;
    } catch (e) {
    }
    try {
        tableAddUnitContents = <?=$this->Table->getJsTableObject('edit-form-unit-content-table')?>;
    } catch (e) {
    }
    try {
        tableAddUnitKeywords = <?=$this->Table->getJsTableObject('edit-form-unit-keyword-table')?>;
    } catch (e) {
    }
    setTimeout(() => initialArchiveUnitForm = $(archiveUnitForm).serialize(), 0);

    function setIsRequired(element, is_required) {
        $(element)
            .prop('required', is_required)
            .closest('.form-group')
            .toggleClass('required', is_required);
    }

    function disableOption(option) {
        var select = option.closest('select');
        var value = select.val();
        if (value === option.attr('value')) {
            select.val('');
        }
        option.disable();
    }

    selectType.change(
        function () {
            var value = $(this).val();
            let currentStorage = divStoragePath.find('select');
            let currentStorageValue = currentStorage.val();
            if (!currentStorageValue) {
                currentStorage.find('[value=""]').remove();
            }
            let currentStorageInitialValue = currentStorage.attr('data-initial-value');
            if (typeof currentStorageInitialValue === 'undefined' || currentStorageInitialValue === false) {
                currentStorage.attr('data-initial-value', currentStorageValue);
            }

            divStoragePath.hide();
            divInput.find('option').enable();
            selectFormInput.find('option').prop('disabled',false);
            if (value === 'document_multiple') {
                disableOption(divInput.find('option.file:not([data-multiple=1])'));
            } else {
                disableOption(divInput.find('option[data-multiple=1]'));
            }
            if (disableRepeatable) {
                disableOption(divInput.find('option[data-repeatable=1]'));
            } else {
                let selector = 'option[data-repeatable=1][data-fieldsets!=%d]'.format(repeatableFieldsetId);
                disableOption(divInput.find(selector));
            }
            if (value === 'document' || value === 'document_multiple' || value === 'tree_root') {
                divInput.show();
                setIsRequired(selectFormInput, true);
                setIsRequired(selectFormSearchExp, false);
            } else {
                divInput.hide();
            }
            if (value === 'tree_root') {
                disableOption(divInput.find('option.file'));
                divStoragePath.show();
                divStoragePath.find('[value="tree_element_name_index"]').hide();
                if (!currentStorage.attr('data-initial-value') || currentStorageValue === 'tree_element_name_index') {
                    divStoragePath.find('select').val('tree_element_name');
                }
            } else {
                disableOption(divInput.find('option.archive_file'));
            }
            if (value === 'simple') {
                divPresence.show();
                divStoragePath.show();
                divStoragePath.find('[value="tree_element_name_index"]').hide();
                if (!currentStorage.attr('data-initial-value') || currentStorageValue === 'tree_element_name_index') {
                    divStoragePath.find('select').val('tree_element_name');
                }
                setIsRequired(selectFormInput, false);
                setIsRequired(selectFormSearchExp, false);
            } else {
                divPresence.hide();
            }
            if (value === 'tree_search' || value === 'tree_path') {
                divSearch.show();
                setIsRequired(selectFormInput, false);
                setIsRequired(selectFormSearchExp, true);
            } else {
                divSearch.hide();
            }
            if (value === 'repeatable') {
                divPresence.show();
                divStoragePath.show();
                divStoragePath.find('[value="tree_element_name_index"]').show();
                if (!currentStorage.attr('data-initial-value')) {
                    divStoragePath.find('select').val('tree_element_name_index');
                }
                repeatableFieldsetDiv.show();
                repeatableFieldsetDiv.find('select').enable();
            } else {
                repeatableFieldsetDiv.hide();
                repeatableFieldsetDiv.find('select').disable();
            }
            AsalaeGlobal.select2(selectFormInput, {minimumResultsForSearch: 10});
        }
    ).change();

    $('#form-edit-form-unit-parent-id').change(
        function () {
            var value = $(this).val();
            AsalaeLoading.ajax(
                {
                    url: '<?=$this->Url->build('/form-units/get-order-options/' . $form_id)?>/'+value,
                    success: function (content) {
                        var select = $('#form-edit-form-unit-order');
                        var newvalue = 0;
                        select.find('option[value!=""]').remove();
                        for (var elem of content) {
                            select.append($('<option>').attr('value', elem.value).text(elem.text));
                            newvalue++;
                        }
                        select.prop('selectedIndex', newvalue);
                    }
                }
            );
        }
    );

    archiveUnitForm
        .append(
            $('<input type="hidden" name="append_addables">')
                .val(toggleAddableInnerState ? 'true' : 'false')
        );

    var simpleOpt = selectType.find('option[value="simple"]');
    simpleOpt.html('&#xf187; '+simpleOpt.html());
    var documentOpt = selectType.find('option[value="document"]');
    documentOpt.html('&#xf0c6; '+documentOpt.html());
    var documentMultipleOpt = selectType.find('option[value="document_multiple"]');
    documentMultipleOpt.html('&#xf0c5; '+documentMultipleOpt.html());
    var treeRootOpt = selectType.find('option[value="tree_root"]');
    treeRootOpt.html('&#xf126; '+treeRootOpt.html());
    var treePathOpt = selectType.find('option[value="tree_path"]');
    treePathOpt.html('&#xf121; '+treePathOpt.html());
    var treeSearchOpt = selectType.find('option[value="tree_search"]');
    treeSearchOpt.html('&#xf002; '+treeSearchOpt.html());

    // selectType.css({"font-family": "FontAwesome"}).select2({minimumResultsForSearch: 10});

    function afterEditHeaderUnit(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            var data = tableAddUnitHeaders.getDataId(content.form_unit_header.name);
            data.id = content.form_unit_header.id;
            data.form_variable = content;
            data.FormTransferHeaders = content.form_unit_header;
            TableGenerator.appendActions(tableAddUnitHeaders.data, tableAddUnitHeaders.actions);
            tableAddUnitHeaders.generateAll();
        }
    }

    function actionDeleteHeaderUnit(table, url) {
        return function (id, name) {
            var data = tableAddUnitHeaders.getDataId(name);
            data.id = null;
            data.form_variable = null;
            $.ajax(
                {
                    url: url+'/'+id,
                    method: 'DELETE'
                }
            );
            TableGenerator.appendActions(tableAddUnitHeaders.data, tableAddUnitHeaders.actions);
            tableAddUnitHeaders.generateAll();
        }
    }

    function afterEditManagementUnit(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            var data = tableAddUnitManagements.getDataId(content.form_unit_management.name);
            data.id = content.form_unit_management.id;
            data.form_variable = content;
            data.FormTransferManagements = content.form_unit_management;
            TableGenerator.appendActions(tableAddUnitManagements.data, tableAddUnitManagements.actions);
            tableAddUnitManagements.generateAll();
        }
    }

    function actionDeleteManagementUnit(table, url) {
        return function (id, name) {
            var data = table.getDataId(name);
            data.id = null;
            data.form_variable = null;
            $.ajax(
                {
                    url: url+'/'+id,
                    method: 'DELETE'
                }
            );
            TableGenerator.appendActions(table.data, table.actions);
            table.generateAll();
        }
    }

    function afterEditContentUnit(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            var data = tableAddUnitContents.getDataId(content.form_unit_content.name);
            data.id = content.form_unit_content.id;
            data.form_variable = content;
            data.FormTransferContents = content.form_unit_content;
            TableGenerator.appendActions(tableAddUnitContents.data, tableAddUnitContents.actions);
            tableAddUnitContents.generateAll();
        }
    }

    function actionDeleteContentUnit(table, url) {
        return function (id, name) {
            var data = tableAddUnitContents.getDataId(name);
            data.id = null;
            data.form_variable = null;
            $.ajax(
                {
                    url: url+'/'+id,
                    method: 'DELETE'
                }
            );
            TableGenerator.appendActions(tableAddUnitContents.data, tableAddUnitContents.actions);
            tableAddUnitContents.generateAll();
        }
    }

    function afterEditFormKeyword(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            var data = tableAddUnitKeywords.getDataId(content.id);
            data.name = content.name;
            data.form_unit_keyword_details = content.form_unit_keyword_details;
            data.FormTransferKeywords = content;
            TableGenerator.appendActions(tableAddUnitKeywords.data, tableAddUnitKeywords.actions);
            tableAddUnitKeywords.generateAll();
        }
    }

    function actionDeleteKeyword(table, url) {
        return function (id) {
            var data = tableAddUnitKeywords.removeDataId(id);
            tableAddUnitKeywords.generateAll();
            $.ajax(
                {
                    url: url+'/'+id,
                    method: 'DELETE'
                }
            );
        }
    }

    function afterEditFormKeywordDetail(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            var tableAddUnitKeywordDetails;
            tableAddUnitKeywordDetails = <?=$this->Table->getJsTableObject('edit-form-unit-keyword-details-table')?>;
            var data = tableAddUnitKeywordDetails.getDataId(content.form_unit_keyword_detail.name);
            data.id = content.form_unit_keyword_detail.id;
            data.form_variable = content;
            data.FormTransferKeywordDetails = content.form_unit_keyword_detail;
            TableGenerator.appendActions(tableAddUnitKeywordDetails.data, tableAddUnitKeywordDetails.actions);
            tableAddUnitKeywordDetails.generateAll();
        }
    }

    function actionDeleteKeywordDetail(table, url) {
        return function (id, name) {
            var tableAddUnitKeywordDetails;
            tableAddUnitKeywordDetails = <?=$this->Table->getJsTableObject('edit-form-unit-keyword-details-table')?>;
            var data = tableAddUnitKeywordDetails.getDataId(name);
            data.id = null;
            data.form_variable = null;
            data.FormTransferKeywordDetails = null;
            $.ajax(
                {
                    url: url+'/'+id,
                    method: 'DELETE'
                }
            );
            TableGenerator.appendActions(tableAddUnitKeywordDetails.data, tableAddUnitKeywordDetails.actions);
            tableAddUnitKeywordDetails.generateAll();
        }
    }

    function wrapActionAddEditManagementUnit(table) {
        tableAddUnitManagements = table;
        return actionAddEditManagementUnit;
    }

    function addAdditionnalOtherRuleData(table, element) {
        var fields = element.val().split(',');
        for (let i in fields) {
            let name = 'Management_' + fields[i];
            if (!table.data.some(obj => obj.name === name)) {
                table.data.push({id: null, name: name, label: fields[i]});
            }
        }
        table.generateAll();
        element.find('option[value="'+element.val()+'"]').remove();
        element.val('');
    }

    AsalaeGlobal.select2(selectFormInput, {minimumResultsForSearch: 10});
</script>
