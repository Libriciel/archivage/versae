<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Utility\Hash;
use Versae\Model\Table\FormUnitsTable;

// duplication impossible si parent est de type tree_root
$duplicateBtn = Hash::get($parent, 'type') !== FormUnitsTable::TYPE_TREE_ROOT
    ? $this->Html->tag(
        'button.p-2.btn-link.text-primary',
        $this->Fa->charte(
            'Dupliquer',
            '',
            ['title' => $title = __("Dupliquer une unité d'archives")]
        )
        . $this->Html->tag(
            'span',
            $title,
            ['class' => 'sr-only']
        ),
        ['type' => 'button', 'onclick' => 'duplicateFormUnit()']
    )
    : '';

echo $this->Html->tag('h4', __("Ajout d'un élément de l'arborescence") . $duplicateBtn);
echo '<hr>';

echo $this->Form->create(
    $entity,
    ['id' => 'form-add-form-unit', 'idPrefix' => 'form-add-form-unit']
);

echo $this->Html->tag('fieldset#add-form-unit-div');
echo $this->Form->control(
    'type',
    [
        'label' => __("Type d'élément"),
        'options' => $types,
        'empty' => __("-- Veuillez sélectionner un type --"),
    ]
);
echo $this->Form->control(
    'name',
    [
        'label' => __("Nom de l'élément"),
    ]
);

if (Hash::get($parent, 'type') !== FormUnitsTable::TYPE_TREE_ROOT) {
    echo $this->Form->control(
        'parent_id',
        [
            'label' => __("Rattachée à"),
            'empty' => __("<racine>"),
            'options' => $parent_ids,
            'default' => $parent_id,
        ]
    );
}
echo $this->Form->control(
    'order',
    [
        'label' => __("Position dans l'arborescence"),
        'empty' => __("Au tout début du noeud parent"),
        'options' => $orders,
        'default' => $orders ? array_keys($orders)[count($orders) - 1] : null,
    ]
);

echo $this->Html->tag('div#form-add-form-unit-hiddable-form-input-id');
echo $this->Form->control(
    'form_input_id',
    [
        'label' => __("Champ de formulaire"),
        'options' => $formInputs,
        'empty' => __("-- Sélectionner un champ de formulaire --"),
    ]
);
echo $this->Html->tag('/div');

echo $this->Html->tag('div#form-add-form-unit-hiddable-presence-condition-id');
echo $this->Form->control(
    'presence_condition_id',
    [
        'label' => __("Condition de présence"),
        'help' => __("Optionnel - Cet élément ne sera pas utilisé si la valeur est vide, nulle ou égale à 0"),
        'options' => $presence_conditions,
        'empty' => __("Toujours présent"),
    ]
);
echo $this->Html->tag('/div');

echo $this->Html->tag('div#form-add-form-unit-hiddable-search-expression');
echo $this->Form->control(
    'search_expression',
    [
        'label' => __("Expression de recherche"),
        'help' => __("L'asterisque * remplace zéro à plusieurs caractères")
            . '<br>' . __(
                "Le séparateur pour les dossiers est le '' {0} '', "
                . "le 1er {0} est ignoré (ex: {0}Chemin{0}vers{0}*{0}fichier.txt)",
                DS
            )
    ]
);
echo $this->Form->control(
    'presence_required',
    [
        'label' => __("Présence obligatoire"),
    ]
);
echo $this->Html->tag('/div');

echo $this->Html->tag('div#form-add-form-unit-hiddable-storage-path');
echo $this->Form->control(
    'storage_calculation',
    [
        'label' => __("Calcul du rangement des fichiers"),
        'options' => $storage_calculations,
        'empty' => true, // sera supprimé par js
    ]
);
echo $this->Html->tag('/div');

echo $this->Html->tag('div#form-add-form-unit-repeatable-fieldset');
echo $this->Form->control(
    'repeatable_fieldset_id',
    [
        'label' => __("Section répétable"),
        'options' => $repeatableFieldsets,
        'empty' => __("-- Sélectionner une section répétable --"),
        'required' => true,
    ]
);
echo $this->Html->tag('/div');
echo $this->Html->tag('/fieldset'); // #add-form-unit-div

echo $this->Html->tag('fieldset#duplicate-form-unit-div', null, ['disabled' => 'disabled']);
echo $this->Form->control(
    'duplicate',
    [
        'label' => __("Unité d'archives à dupliquer"),
        'options' => $duplicates,
        'empty' => __("-- Veuillez sélectionner une unité d'archives --"),
    ]
);
echo $this->Html->tag('/fieldset'); // #duplicate-form-unit-div

echo $this->Form->end();
?>
<script>
    /* global archiveUnitForm, initialArchiveUnitForm, toggleAddableInnerState */
    var disableRepeatable = <?= $disableRepeatable ? 'true' : 'false' ?>;
    var repeatableFieldsetId = <?=$repeatableFieldsetId ?: 'null'?>;
    archiveUnitForm = $('#form-add-form-unit');
    var divInput = $('#form-add-form-unit-hiddable-form-input-id');
    var divPresence = $('#form-add-form-unit-hiddable-presence-condition-id');
    var divSearch = $('#form-add-form-unit-hiddable-search-expression');
    var divStoragePath = $('#form-add-form-unit-hiddable-storage-path');
    var repeatableFieldsetDiv = $('#form-add-form-unit-repeatable-fieldset');
    var selectType = $('#form-add-form-unit-type');
    var selectFormInput = $('#form-add-form-unit-form-input-id');
    var selectFormSearchExp = $('#form-add-form-unit-search-expression');
    setTimeout(() => initialArchiveUnitForm = $(archiveUnitForm).serialize(), 0);

    function setIsRequired(element, is_required) {
        $(element)
            .prop('required', is_required)
            .closest('.form-group')
            .toggleClass('required', is_required);
    }

    function disableOption(option) {
        var select = option.closest('select');
        var value = select.val();
        if (value === option.attr('value')) {
            select.val('');
        }
        option.disable();
    }

    disableOption(selectType.find('option[data-disabled="1"]'));

    selectType.change(
        function () {
            let value = $(this).val();
            let currentStorage = divStoragePath.find('select');
            let currentStorageValue = currentStorage.val();
            if (!currentStorageValue) {
                currentStorage.find('[value=""]').remove();
            }
            let currentStorageInitialValue = currentStorage.attr('data-initial-value');
            if (typeof currentStorageInitialValue === 'undefined' || currentStorageInitialValue === false) {
                currentStorage.attr('data-initial-value', currentStorageValue);
            }

            divStoragePath.hide();
            divInput.find('option').enable();
            selectFormInput.find('option').prop('disabled',false);
            if (value === 'document_multiple') {
                disableOption(divInput.find('option.file:not([data-multiple=1])'));
            } else {
                disableOption(divInput.find('option[data-multiple=1]'));
            }
            if (disableRepeatable) {
                disableOption(divInput.find('option[data-repeatable=1]'));
            } else {
                let selector = 'option[data-repeatable=1][data-fieldsets!=%d]'.format(repeatableFieldsetId);
                disableOption(divInput.find(selector));
            }
            if (value === 'document' || value === 'document_multiple' || value === 'tree_root') {
                divInput.show();
                setIsRequired(selectFormInput, true);
                setIsRequired(selectFormSearchExp, false);
            } else {
                divInput.hide();
            }
            if (value === 'tree_root') {
                disableOption(divInput.find('option.file'));
                divStoragePath.show();
                divStoragePath.find('[value="tree_element_name_index"]').hide();
                if (!currentStorage.attr('data-initial-value') || currentStorageValue === 'tree_element_name_index') {
                    divStoragePath.find('select').val('tree_element_name');
                }
            } else {
                disableOption(divInput.find('option.archive_file'));
            }
            if (value === 'simple') {
                divPresence.show();
                divStoragePath.show();
                divStoragePath.find('[value="tree_element_name_index"]').hide();
                if (!currentStorage.attr('data-initial-value') || currentStorageValue === 'tree_element_name_index') {
                    divStoragePath.find('select').val('tree_element_name');
                }
                setIsRequired(selectFormInput, false);
                setIsRequired(selectFormSearchExp, false);
            } else {
                divPresence.hide();
            }
            if (value === 'tree_search' || value === 'tree_path') {
                divSearch.show();
                setIsRequired(selectFormInput, false);
                setIsRequired(selectFormSearchExp, true);
            } else {
                divSearch.hide();
            }
            if (value === 'repeatable') {
                divPresence.show();
                divStoragePath.show();
                divStoragePath.find('[value="tree_element_name_index"]').show();
                if (!currentStorage.attr('data-initial-value')) {
                    divStoragePath.find('select').val('tree_element_name_index');
                }
                repeatableFieldsetDiv.show();
                repeatableFieldsetDiv.find('select').enable();
            } else {
                repeatableFieldsetDiv.hide();
                repeatableFieldsetDiv.find('select').disable();
            }
            AsalaeGlobal.select2(selectFormInput, {minimumResultsForSearch: 10});
        }
    ).change();

    $('#form-add-form-unit-parent-id').change(
        function () {
            var value = $(this).val();
            AsalaeLoading.ajax(
                {
                    url: '<?=$this->Url->build('/form-units/get-order-options/' . $form_id)?>/' + value,
                    success: function (content) {
                        var select = $('#form-add-form-unit-order');
                        var newvalue = 0;
                        select.find('option[value!=""]').remove();
                        for (var elem of content) {
                            select.append($('<option>').attr('value', elem.value).text(elem.text));
                            newvalue++;
                        }
                        select.prop('selectedIndex', newvalue);
                    }
                }
            );
        }
    );

    archiveUnitForm
        .append(
            $('<input type="hidden" name="append_addables">')
                .val(toggleAddableInnerState ? 'true' : 'false')
        );

    var simpleOpt = selectType.find('option[value="simple"]');
    simpleOpt.html('&#xf187; ' + simpleOpt.html());
    var documentOpt = selectType.find('option[value="document"]');
    documentOpt.html('&#xf0c6; ' + documentOpt.html());
    var documentMultipleOpt = selectType.find('option[value="document_multiple"]');
    documentMultipleOpt.html('&#xf0c5; ' + documentMultipleOpt.html());
    var treeRootOpt = selectType.find('option[value="tree_root"]');
    treeRootOpt.html('&#xf126; ' + treeRootOpt.html());
    var treePathOpt = selectType.find('option[value="tree_path"]');
    treePathOpt.html('&#xf121; ' + treePathOpt.html());
    var treeSearchOpt = selectType.find('option[value="tree_search"]');
    treeSearchOpt.html('&#xf002; ' + treeSearchOpt.html());
    var repeatableOpt = selectType.find('option[value="repeatable"]');
    repeatableOpt.html('&#xf1b3;' + repeatableOpt.html());

    selectType.css({"font-family": "FontAwesome"}).select2({minimumResultsForSearch: 10});
    AsalaeGlobal.select2(selectFormInput, {minimumResultsForSearch: 10});

    var duplicateFormUnitDiv = $('#duplicate-form-unit-div').prop('disabled', true).hide();

    function duplicateFormUnit()
    {
        $('#add-form-unit-div').prop('disabled', true).hide();
        duplicateFormUnitDiv.prop('disabled', false).show();
    }
</script>
