<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;
use Versae\Model\Table\FormUnitsTable;

?>
<script>
    function twigForm(value, context) {
        value = value && value.replace(/\n/g, '<span class="text-info">\\n</span><wbr/>');
        if (context.required) {
            return $('<div>').append(
                $('<input required>')
                    .css(
                        {
                            height: 0,
                            width: 0,
                            padding: 0,
                            border: 0,
                            opacity: 0
                        }
                    )
                    .attr('aria-hidden', 'true')
                    .val(value)
            ).append(value);
        }
        return value;
    }
    function keywordVars(value) {
        if (!value) {
            return '';
        }
        var dl = $('<dl>');
        // structure dl > div > dt + dd
        for (var i = 0; i < value.length; i++) {
            let div = $('<div>')
                .append($('<dt>').text(value[i].name+':'))
                .append($('<dd>').text(value[i].form_variable.twig));
            dl.append(div);
        }
        return dl;
    }
</script>
<?php
echo $this->Html->tag('h4', __("Visualisation d'une unité d'archives"));
echo '<hr>';


$tabs = $this->Tabs->create('tabs-view-form-unit', ['class' => 'row no-padding']);

/**
 * Élément de l'arborescence
 */
$htmlInfos = $this->ViewTable->generate(
    $entity,
    [
        __("Type d'élément") => 'type',
        __("Nom de l'élément") => 'name',
    ]
    + (
    $parent && Hash::get($parent, 'type') !== FormUnitsTable::TYPE_TREE_ROOT
        ? [__("Rattachée à") => 'parent_id']
        : []
    )
    + [
        __("Position dans l'arborescence") => 'order',
        __("Champ de formulaire") => 'form_input_id',
        __("Condition de présence") => 'presence_condition_id',
        __("Expression de recherche") => 'search_expression',
        __("Présence obligatoire") => 'presence_required',
        __("Calcul du rangement des fichiers") => function (EntityInterface $e) {
            $auType = [FormUnitsTable::TYPE_SIMPLE, FormUnitsTable::TYPE_TREE_ROOT, FormUnitsTable::TYPE_REPEATABLE];
            if (in_array($e->get('type'), $auType)) {
                if (empty($e->get('storage_calculation'))) {
                    $e->set('storage_calculation', FormUnitsTable::STORAGE_CALCULATION_TREE_ELEMENT_NAME);
                }
                return $e->get('storage_calculationtrad');
            }
            return '';
        },
    ]
);

$tabs->add(
    'tab-view-form-unit-infos',
    $this->Fa->i('fa-file-code-o', __("Élément de l'arborescence")),
    $htmlInfos
);
if ($entity->get('type') !== FormUnitsTable::TYPE_DOCUMENT_MULTIPLE) {
    $filteredData = [];
    foreach (array_merge($dataHeaders, $dataContents) as $data) {
        if (!$data['additionnal'] || $data['id']) {
            $filteredData[] = $data;
        }
    }
    /**
     * Données
     */
    $htmlHeaders = $this->Html->tag(
        'header',
        $this->Html->tag('h2.h4', __("Liste des données"))
    )
        . $this->Table
            ->create('view-form-unit-header-table', ['class' => 'table table-striped table-hover smart-td-size'])
            ->fields(
                [
                    'label' => [
                        'label' => __("Nom"),
                    ],
                    'form_variable.twig' => [
                        'label' => __("Twig"),
                        'callback' => 'twigForm',
                        'class' => 'twig',
                    ],
                ]
            )
            ->data($filteredData)
            ->params(
                [
                    'identifier' => 'name',
                    'checkbox' => false,
                ]
            )
            ->generate();
    $tabs->add(
        'tab-view-form-unit-headers',
        $this->Fa->i('fa-newspaper-o', __("Descriptions")),
        $htmlHeaders
    );

    /**
     * Règles de gestion
     */
    if ($entity->get('type') !== FormUnitsTable::TYPE_DOCUMENT || $form->get('seda_version') !== 'seda1.0') {
        $filteredData = [];
        foreach ($dataManagements as $data) {
            if (!$data['additionnal'] || $data['id']) {
                $filteredData[] = $data;
            }
        }
        $htmlManagements = $this->Table
            ->create('view-form-unit-management-table', ['class' => 'table table-striped table-hover smart-td-size'])
            ->fields(
                [
                    'label' => [
                        'label' => __("Nom"),
                    ],
                    'form_variable.twig' => [
                        'label' => __("Twig"),
                        'callback' => 'twigForm',
                        'class' => 'twig',
                    ],
                ]
            )
            ->data($filteredData)
            ->params(
                [
                    'identifier' => 'name',
                    'checkbox' => false,
                ]
            )
            ->generate();
        $tabs->add(
            'tab-view-form-unit-managements',
            $this->Fa->i('fa-gavel', __("Règles de gestion")),
            $htmlManagements
        );
    }

    /**
     * Mots clés
     */
    if ($entity->get('type') !== FormUnitsTable::TYPE_DOCUMENT || $form->get('seda_version') !== 'seda1.0') {
        $htmlKeywords = $this->Table
            ->create('view-form-unit-keyword-table', ['class' => 'table table-striped table-hover smart-td-size'])
            ->fields(
                [
                    'name' => [
                        'label' => __("Nom"),
                    ],
                    'form_unit_keyword_details' => [
                        'label' => __("Variables"),
                        'callback' => 'keywordVars',
                    ],
                ]
            )
            ->data($keywords)
            ->params(
                [
                    'identifier' => 'id',
                    'checkbox' => false,
                ]
            )
            ->generate();
        $tabs->add(
            'tab-view-form-unit-keywords',
            $this->Fa->i('fa-key', __("Mots clés")),
            $htmlKeywords
        );
    }
}

echo $tabs;
