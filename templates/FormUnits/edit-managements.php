<?php

/**
 * @var Versae\View\AppView $this
 */

use Versae\Model\Table\FormUnitsTable;

if ($entity->get('type') === FormUnitsTable::TYPE_DOCUMENT_MULTIPLE) {
    return $this->Html->tag('div.alert.alert-info', __("Pas de règles de gestion sur Lot de documents"));
} elseif ($entity->get('type') === FormUnitsTable::TYPE_DOCUMENT && $form->get('seda_version') === 'seda1.0') {
    return $this->Html->tag('div.alert.alert-info', __("Pas de règles de gestion sur Document en SEDA 1.0"));
}

$jsTableManagementId = 'edit-form-unit-management-table-access-rule';
$jsTableVariables = $this->Table->getJsTableObject($jsTableManagementId);
$tableManagements = $this->Table
    ->create($jsTableManagementId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'label' => [
                'label' => __("Nom"),
                'callback' => 'requiredCallback',
                'titleEval' => 'name',
            ],
            'form_variable.twig' => [
                'label' => __("Twig"),
                'callback' => 'twigForm',
                'class' => 'twig',
            ],
        ]
    )
    ->data(
        array_values(
            array_filter($dataManagements, fn($v) => $v['section'] === 'access_rule')
        )
    )
    ->params(
        [
            'identifier' => 'name',
            'checkbox' => false,
        ]
    )
    ->actions(
        [
            [
                'onclick' => sprintf(
                    "wrapActionAddEditManagementUnit(%s)('%s/{0}')",
                    $jsTableVariables,
                    $id
                ),
                'type' => 'button',
                'class' => 'btn-link text-success',
                'label' => $this->Fa->charte('Ajouter'),
                'title' => $title = __("Ajouter {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/form-calculators/add-edit-management-unit'),
                'displayEval' => 'data[{index}].id === null',
                'params' => ['name', 'label']
            ],
            [
                'onclick' => sprintf(
                    "wrapActionAddEditManagementUnit(%s)('%s/{0}')",
                    $jsTableVariables,
                    $id
                ),
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/form-calculators/add-edit-management-unit'),
                'displayEval' => 'data[{index}].id !== null',
                'params' => ['id', 'label']
            ],
            [
                'data-callback' => sprintf(
                    "actionDeleteManagementUnit(%s, '%s')('{0}', '{2}')",
                    $jsTableVariables,
                    $this->Url->build('/form-calculators/delete-unit-management')
                ),
                'type' => 'button',
                'class' => 'btn-link delete',
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'confirm' => __("Supprimer cette variable ?"),
                'display' => $this->Acl->check('/form-calculators/delete-unit-management'),
                'displayEval' => 'data[{index}].id !== null',
                'params' => ['id', 'label', 'name']
            ],
        ]
    );

$managements = $this->Html->tag(
    'header',
    $this->Html->tag('h2.h4', __("Règles d'accessibilités"))
);

$managements .= $tableManagements->generate();

$jsTableManagementId = 'edit-form-unit-management-table-appraisal-rule';
$jsTableVariables = $this->Table->getJsTableObject($jsTableManagementId);
$tableManagements = $this->Table
    ->create($jsTableManagementId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'label' => [
                'label' => __("Nom"),
                'callback' => 'requiredCallback',
                'titleEval' => 'name',
            ],
            'form_variable.twig' => [
                'label' => __("Twig"),
                'callback' => 'twigForm',
                'class' => 'twig',
            ],
        ]
    )
    ->data(
        array_values(
            array_filter($dataManagements, fn($v) => $v['section'] === 'appraisal_rule')
        )
    )
    ->params(
        [
            'identifier' => 'name',
            'checkbox' => false,
        ]
    )
    ->actions(
        [
            [
                'onclick' => sprintf(
                    "wrapActionAddEditManagementUnit(%s)('%s/{0}')",
                    $jsTableVariables,
                    $id
                ),
                'type' => 'button',
                'class' => 'btn-link text-success',
                'label' => $this->Fa->charte('Ajouter'),
                'title' => $title = __("Ajouter {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/form-calculators/add-edit-management-unit'),
                'displayEval' => 'data[{index}].id === null',
                'params' => ['name', 'label']
            ],
            [
                'onclick' => sprintf(
                    "wrapActionAddEditManagementUnit(%s)('%s/{0}')",
                    $jsTableVariables,
                    $id
                ),
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/form-calculators/add-edit-management-unit'),
                'displayEval' => 'data[{index}].id !== null',
                'params' => ['id', 'label']
            ],
            [
                'data-callback' => sprintf(
                    "actionDeleteManagementUnit(%s, '%s')('{0}', '{2}')",
                    $jsTableVariables,
                    $this->Url->build('/form-calculators/delete-unit-management')
                ),
                'type' => 'button',
                'class' => 'btn-link delete',
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'confirm' => __("Supprimer cette variable ?"),
                'display' => $this->Acl->check('/form-calculators/delete-unit-management'),
                'displayEval' => 'data[{index}].id !== null',
                'params' => ['id', 'label', 'name']
            ],
        ]
    );

$managements .= $this->Html->tag(
    'header',
    $this->Html->tag('h2.h4', __("Règles de durée d'utilité administrative"))
);

$managements .= $tableManagements->generate();

if ($entity->get('seda_version') !== 'seda1.0') {
    $jsTableManagementId = 'edit-form-unit-management-table-other-rule';
    $jsTableVariables = $this->Table->getJsTableObject($jsTableManagementId);

    $dataOtherRules = array_values(
        array_filter(
            $dataManagements,
            function ($v) {
                return in_array(
                    $v['section'],
                    ['other_rule', 'other_rule_options']
                ) && $v['id'];
            }
        )
    );
    $parents = [];
    foreach ($dataOtherRules as $key => $value) {
        if ($value['parent']) {
            $parents['Management_' . $value['parent']][$value['label']] = true;
        }
    }
    // ajoute les champs manquants. ex: StorageRule_Rule sans StorageRule_StartDate
    foreach ($dataManagements as $value) {
        if (
            isset($parents[$value['parent']])
            && !isset($parents[$value['parent']][$value['label']])
        ) {
            $dataOtherRules[] = $value;
        }
    }
    usort($dataOtherRules, fn($a, $b) => strnatcmp($a['name'], $b['name']));

    $tableManagements = $this->Table
        ->create($jsTableManagementId, ['class' => 'table table-striped table-hover smart-td-size'])
        ->fields(
            [
                'label' => [
                    'label' => __("Nom"),
                    'callback' => 'requiredCallback',
                    'titleEval' => 'name',
                ],
                'form_variable.twig' => [
                    'label' => __("Twig"),
                    'callback' => 'twigForm',
                    'class' => 'twig',
                ],
            ]
        )
        ->data($dataOtherRules)
        ->params(
            [
                'identifier' => 'name',
                'checkbox' => false,
            ]
        )
        ->actions(
            [
                [
                    'onclick' => sprintf(
                        "wrapActionAddEditManagementUnit(%s)('%s/{0}')",
                        $jsTableVariables,
                        $id
                    ),
                    'type' => 'button',
                    'class' => 'btn-link text-success',
                    'label' => $this->Fa->charte('Ajouter'),
                    'title' => $title = __("Ajouter {0}", '{1}'),
                    'aria-label' => $title,
                    'display' => $this->Acl->check('/form-calculators/add-edit-management-unit'),
                    'displayEval' => 'data[{index}].id === null',
                    'params' => ['name', 'label']
                ],
                [
                    'onclick' => sprintf(
                        "wrapActionAddEditManagementUnit(%s)('%s/{0}')",
                        $jsTableVariables,
                        $id
                    ),
                    'type' => 'button',
                    'class' => 'btn-link',
                    'label' => $this->Fa->charte('Modifier'),
                    'title' => $title = __("Modifier {0}", '{1}'),
                    'aria-label' => $title,
                    'display' => $this->Acl->check('/form-calculators/add-edit-management-unit'),
                    'displayEval' => 'data[{index}].id !== null',
                    'params' => ['id', 'label']
                ],
                [
                    'data-callback' => sprintf(
                        "actionDeleteManagementUnit(%s, '%s')('{0}', '{2}')",
                        $jsTableVariables,
                        $this->Url->build('/form-calculators/delete-unit-management')
                    ),
                    'type' => 'button',
                    'class' => 'btn-link delete',
                    'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => $title = __("Supprimer {0}", '{1}'),
                    'aria-label' => $title,
                    'confirm' => __("Supprimer cette variable ?"),
                    'display' => $this->Acl->check('/form-calculators/delete-unit-management'),
                    'displayEval' => 'data[{index}].id !== null',
                    'params' => ['id', 'label', 'name']
                ],
            ]
        );

    $otherMetadata = $this->Html->tag(
        'header',
        $this->Html->tag('h2.h4', __("Autres règles de gestion"))
    );
    $otherMetadata .= $tableManagements->generate();

    $missingAdditionnals = [];
    foreach ($dataManagements as $value) {
        if (
            $value['additionnal']
            && $value['section'] === 'other_rule_options'
            && !$value['id']
        ) {
            $missingAdditionnals[$value['subfields']] = $value['label'];
        }
    }

    if ($missingAdditionnals) {
        $otherMetadata .= $this->Form->control(
            'add_other_rule',
            [
                'label' => __("Ajouter une métadonnée"),
                'options' => $missingAdditionnals,
                'empty' => __("-- Sélectionner une métadonnée à ajouter --"),
                'onchange' => "addAdditionnalOtherRuleData($jsTableVariables, $(this))",
            ]
        );
        $otherMetadata .= $this->Html->tag('script', "AsalaeGlobal.select2($('select#add-other-rule'));");
    }
    $managements .= $otherMetadata;
}

return $managements;
