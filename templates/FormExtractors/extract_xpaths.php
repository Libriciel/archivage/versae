<?php

/**
 * @var Versae\View\AppView $this
 * @var DOMDocument $dom
 */

$namespaces = [];

/**
 * @param DOMElement $node
 * @param array      $paths
 * @return array
 */
$extractXpath = function (DOMElement $node, array $paths = []) use (&$namespaces, &$extractXpath): array {
    // donne une liste d'éléments et de leurs quantités
    $elements = [];
    foreach ($node->childNodes as $subnode) {
        if ($subnode instanceof DOMElement) {
            $elements[$subnode->tagName] = ($elements[$subnode->tagName] ?? 0) + 1;
        }
    }

    $indexes = [];
    $nodeList = [];
    foreach ($node->childNodes as $subnode) {
        if ($subnode instanceof DOMElement) {
            $tagName = $subnode->tagName;
            $ns = '';
            if (strpos($tagName, ':')) {
                [$ns, $tagName] = explode(':', $tagName);
            }
            $namespace = $subnode->namespaceURI;
            if ($namespace && !isset($namespaces[$namespace])) {
                if (!$ns && count($namespaces) === 0) {
                    $namespaces[$namespace] = 'ns';
                } elseif (!$ns) {
                    $namespaces[$namespace] = 'ns' . (count($namespaces) + 1);
                } else {
                    $namespaces[$namespace] = $ns;
                }
            }
            $ns = $namespace ? $namespaces[$namespace] . ':' : '';

            $curName = $ns . $tagName;
            if ($elements[$subnode->tagName] > 1) {
                $indexes[$subnode->tagName] = ($indexes[$subnode->tagName] ?? 0) + 1;
                $curName .= '[' . $indexes[$subnode->tagName] . ']';
            }

            $value = '';
            foreach ($subnode->childNodes as $childNode) {
                if ($childNode instanceof DOMText) {
                    $value .= $childNode->nodeValue;
                }
            }
            $value = trim($value);

            $curPaths = $paths;
            $curPaths[] = $curName;
            if ($value) {
                $nodeList[] = [
                    'path' => '/' . implode('/', $curPaths),
                    'value' => $value,
                ];
            }
            /** @var DOMAttr $attr */
            foreach ($subnode->attributes as $attr) {
                $nodeList[] = [
                    'path' => '/' . implode('/', $curPaths) . '/@' . $attr->nodeName,
                    'value' => trim($attr->nodeValue),
                ];
            }
            $nodeList = array_merge($nodeList, $extractXpath($subnode, $curPaths));
        }
    }
    return $nodeList;
};

echo $this->Html->tag('table.table.table-striped.table-hover');
echo $this->Html->tag('thead');
echo $this->Html->tag('tr');
echo $this->Html->tag('th', __("Chemin XPath"));
echo $this->Html->tag('th', __("Valeur"));
echo $this->Html->tag('th', __("Actions"));
echo $this->Html->tag('/tr');
echo $this->Html->tag('/thead');
echo $this->Html->tag('tbody');

$tagName = $dom->documentElement->tagName;
$ns = '';
if (strpos($tagName, ':')) {
    [$ns, $tagName] = explode(':', $tagName);
}
$namespace = $dom->documentElement->namespaceURI;
if ($namespace && !isset($namespaces[$namespace])) {
    if (!$ns && count($namespaces) === 0) {
        $namespaces[$namespace] = 'ns';
    } elseif (!$ns) {
        $namespaces[$namespace] = 'ns' . (count($namespaces) + 1);
    } else {
        $namespaces[$namespace] = $ns;
    }
}
$ns = $namespace ? $namespaces[$namespace] . ':' : '';
$curName = $ns . $tagName;
$paths = [$ns . $tagName];

foreach ($extractXpath($dom->documentElement, $paths) as $node) {
    echo $this->Html->tag('tr');
    echo $this->Html->tag('td', h($node['path']));
    echo $this->Html->tag(
        'td',
        wordwrap(
            h($node['value']),
            40,
            '<wbr/>',
            true
        )
    );
    echo $this->Html->tag(
        'td.action',
        $this->Html->tag(
            'button.btn.btn-link',
            $this->Fa->i('fa-location-arrow'),
            ['type' => 'button', 'onclick' => 'useXpath.call(this)']
        )
    );
    echo $this->Html->tag('/tr');
}
echo $this->Html->tag('/tbody');
echo $this->Html->tag('/table');
?>
<script>
    namespaces = <?=json_encode($namespaces)?>;
</script>
