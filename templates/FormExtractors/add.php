<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->Form->create(
    $entity,
    [
        'idPrefix' => $idPrefix = 'add-extractor',
        'id' => $idForm = 'add-extractor-form',
    ]
);

require 'addedit-common.php';
