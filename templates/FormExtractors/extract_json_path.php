<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Utility\Hash;

echo $this->Html->tag('table.table.table-striped.table-hover');
echo $this->Html->tag('thead');
echo $this->Html->tag('tr');
echo $this->Html->tag('th', __("Chemin JsonPath"));
echo $this->Html->tag('th', __("Valeur"));
echo $this->Html->tag('th', __("Actions"));
echo $this->Html->tag('/tr');
echo $this->Html->tag('/thead');
echo $this->Html->tag('tbody');

foreach (Hash::flatten($json) as $node => $value) {
    if (is_array($value)) {
        continue;
    }
    echo $this->Html->tag('tr');
    echo $this->Html->tag(
        'td',
        str_replace('.', '<wbr/>.', h($node))
    );
    echo $this->Html->tag(
        'td',
        wordwrap(
            h($value),
            40,
            '<wbr/>',
            true
        )
    );
    echo $this->Html->tag(
        'td.action',
        $this->Html->tag(
            'button.btn.btn-link',
            $this->Fa->i('fa-location-arrow'),
            ['type' => 'button', 'onclick' => 'useXpath.call(this)']
        )
    );
    echo $this->Html->tag('/tr');
}
echo $this->Html->tag('/tbody');
echo $this->Html->tag('/table');
?>
<script>
    namespaces = [];
</script>
