<?php

/**
 * @var Versae\View\AppView $this
 */

use Versae\Model\Table\FormExtractorsTable;

echo $this->Form->control(
    'name',
    [
        'label' => __("Nom (identifiant twig)"),
        'pattern' => '[a-zA-Z][a-zA-Z0-9_]*',
        'help' => __(
            "Autorisés: lettres, chiffres et underscores `_` (obligatoirement une lettre en premier caractère)"
        ),
    ]
);
echo $this->Form->control(
    'description',
    [
        'label' => __("Description")
    ]
);
echo $this->Form->control(
    'type',
    [
        'label' => __("Type"),
        'empty' => __("-- Sélectionnez un type d'extracteur --"),
        'options' => $types,
        'class' => 'extract-type',
    ]
);

echo '<hr>';

echo $this->Html->tag('div.type_input');
echo $this->Form->control(
    'form_input_id',
    [
        'label' => __("Champ de formulaire"),
        'options' => $formInputs,
        'empty' => __("-- Sélectionnez un champ de formulaire --"),
        'class' => 'extract-input',
    ]
);
echo $this->Form->control(
    'multiple',
    ['type' => 'hidden']
);
echo $this->Html->tag(
    'p.alert.alert-warning.hide.alert-multiple',
    __("Le champ de formulaire est de type multiple, cet extracteur sera donc lui aussi de type multiple.")
);
echo $this->Form->control(
    'file_selector',
    [
        'label' => __("Selecteur de fichier"),
        'placeholder' => 'path/to/file*.xml',
        'help' => __(
            "Chemin vers le fichier ciblé, prendra le 1er fichier qui "
            . "correspond au chemin. Vous pouvez utiliser le wildcard `*`"
        ),
        'class' => 'extract-file-selector',
    ]
);
echo $this->Html->tag('/div');

echo $this->Html->tag('div.type_webservice');
echo '<label class="hide">autocomplete obliterator'
    . '<input type="password" value="false"><!-- neutralise le préremplissage firefox --></label>';
echo $this->Form->control(
    'url',
    [
        'label' => __("URL du webservice"),
        'pattern' => '^http(s)://.*',
        'placeholder' => 'https://my.webservice.com/myservice.php',
        'help' => __(
            "URL de test: {0}",
            $this->Url->build('/api/forms/test-extractor/test.json', ['fullBase' => true])
        ),
    ]
);
echo $this->Form->control(
    'username',
    [
        'label' => __("Nom d'utilisateur (basic auth)"),
    ]
);
echo $this->Form->control(
    'password',
    [
        'label' => __("Mot de passe (basic auth)"),
        'type' => 'password',
    ]
);
echo $this->Form->control(
    'use_proxy',
    [
        'label' => __("Utiliser le proxy"),
        'type' => 'checkbox',
        'default' => '1',
    ]
);
echo $this->Form->control(
    'ssl_verify_peer',
    [
        'label' => __("Activation de la certification SSL"),
        'type' => 'checkbox',
        'default' => '1',
    ]
);
echo $this->Form->control(
    'ssl_verify_peer_name',
    [
        'label' => __("Vérification du nom d’hôte (certification SSL)"),
        'type' => 'checkbox',
        'default' => '1',
    ]
);
echo $this->Form->control(
    'ssl_verify_host',
    [
        'label' => __("Valide le certificat SSL pour un nom d’hôte"),
        'type' => 'checkbox',
        'help' => __("Compare le nom d'hôte du certificat SSL avec celui défini dans l'url"),
        'default' => '1',
    ]
);
$uploadCerts = $this->Upload
    ->create('upload-form-cert', ['class' => 'table table-striped table-hover hide'])
    ->fields(
        [
            'info' => [
                'label' => __("Fichier"),
                'target' => '',
                'thead' => [
                    'filename' => [
                        'label' => __("Nom de fichier"),
                        'callback' => 'TableHelper.wordBreak()',
                    ],
                    'hash' => [
                        'label' => __("Empreinte du fichier"),
                        'callback' => 'TableHelper.wordBreak("_", 32)',
                    ],
                    'size' => [
                        'label' => __("Taille"),
                        'callback' => 'TableHelper.readableBytes',
                    ],
                ],
            ],
            'message' => [
                'label' => __("Message"),
                'style' => 'width: 400px',
                'class' => 'message',
            ],
        ]
    )
    ->data($uploadDataCert ?? [])
    ->params(
        [
            'identifier' => 'id',
            'classEval' => 'data[{index}].valid !== false ? "" : "danger"',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "$('.form-cafile:visible').val('{0}')",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-check-square'),
                'title' => $title = __("Sélectionner {0} comme certificat", '{1}'),
                'aria-label' => $title,
                'params' => ['path', 'name']
            ],
            function ($table) {
                $deleteUrl = $this->Url->build('/upload/delete');
                return [
                    'onclick' => "TableGenericAction.deleteAction($table->tableObject, '$deleteUrl')({0})",
                    'type' => 'button',
                    'class' => 'btn-link',
                    'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'params' => ['id', 'name']
                ];
            },
        ]
    );
echo $this->Html->tag(
    'div.form-group.fake-input',
    $this->Html->tag('span.fake-label', __("Certificats"))
    . $uploadCerts->generate(
        [
            'singleFile' => false,
            'allowDuplicateUploads' => true,
            'target' => $this->Url->build('/upload/form-cert'),
        ]
    )
);
echo $this->Form->control(
    'ssl_cafile',
    [
        'label' => __("Chemin vers le fichier du certificat CA"),
        'class' => 'form-cafile',
    ]
);
echo $this->Html->tag('/div');

echo '<hr>';

echo $this->Form->control(
    'data_format',
    [
        'label' => __("Format attendu du fichier"),
        'empty' => __("-- Sélectionnez un format de fichier --"),
        'options' => $data_formats,
    ]
);

echo $this->Form->control(
    'virtual_field_multiple',
    [
        'label' => __("Données multiples attendues"),
        'options' => [
            '0' => __("Non"),
            '1' => __("Oui"),
        ],
    ]
);

echo $this->Form->control(
    'data_path',
    [
        'label' => __("Chemin vers la donnée"),
        'placeholder' => 'ns:RootNode/ns:Node[2]',
        'help' => __("Vous pouvez choisir un fichier d'exemple ci-dessous")
    ]
);
echo $this->Form->control(
    'namespace',
    [
        'type' => 'hidden',
    ]
);

echo '<hr>';

$uploads = $this->Upload
    ->create('upload-form-extractor-test-file', ['class' => 'table table-striped table-hover hide'])
    ->fields(
        [
            'filename' => [
                'label' => __("Fichier"),
            ],
            'message' => [
                'label' => __("Message"),
                'style' => 'width: 400px',
                'class' => 'message',
            ],
        ]
    )
    ->data([])
    ->params(
        [
            'identifier' => 'id',
            'classEval' => 'data[{index}].valid !== false ? "" : "danger"',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "testFilePath('{0}')",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-calendar-check-o'),
                'title' => $title = __("Extraire la donnée à partir du chemin indiqué", '{1}'),
                'aria-label' => $title,
                'params' => ['id']
            ],
            [
                'onclick' => "actionExtractXpath('{0}')",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-code'),
                'title' => $title = __("Extraire un chemin vers la donnée", '{1}'),
                'aria-label' => $title,
                'displayEval' => 'data[{index}].filename.substr(-4).toLowerCase() === ".xml"',
                'params' => ['id']
            ],
            [
                'onclick' => "actionExtractJsonPath('{0}')",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-code'),
                'title' => $title = __("Extraire un chemin vers la donnée", '{1}'),
                'aria-label' => $title,
                'displayEval' => 'data[{index}].filename.substr(-5).toLowerCase() === ".json"',
                'params' => ['id']
            ],
            [
                'onclick' => "actionExtractCsvPath('{0}')",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->i('fa-code'),
                'title' => $title = __("Extraire un chemin vers la donnée", '{1}'),
                'aria-label' => $title,
                'displayEval' => 'data[{index}].filename.substr(-4).toLowerCase() === ".csv"',
                'params' => ['id']
            ],
            function ($table) {
                $deleteUrl = $this->Url->build('/upload/delete');
                return [
                    'data-callback' => sprintf(
                        "TableGenericAction.deleteAction(%s, '%s')({0}, false)",
                        $table->tableObject,
                        $deleteUrl
                    ),
                    'confirm' => __("Êtes-vous sûr de vouloir supprimer ce fichier temporaire ?"),
                    'type' => 'button',
                    'class' => 'btn-link delete',
                    'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'params' => ['id', 'name']
                ];
            },
        ]
    );
$jsUpload = $uploads->jsObject;
$jsTableUpload = $this->Upload->getTableId('upload-form-extractor-test-file');

echo $this->Html->tag(
    'div.form-group.fake-input',
    $this->Html->tag(
        'span.fake-label',
        __("Définir un chemin vers la donnée à l'aide d'un fichier (ne sera pas conservé)")
    )
    . $uploads->generate(
        [
            'attributes' => ['accept' => '.xml, application/xml'],
        ]
    )
    . $this->Html->tag('p.help-block', __("Vous permet d'extraire un chemin ou des namespaces"))
    . $this->Html->tag(
        'button#test-download-by-webservice.btn.btn-info',
        $this->Fa->i('fa-upload', __("Récupérer le fichier du webservice")),
        [
            'type' => 'button',
            'style' => 'display: none'
        ]
    )
);

echo '<hr>';

$options = $this->Html->tag('div.row.border-bottom');
$options .= $this->Html->tag(
    'label.col-5.font-weight-bold',
    __("Préfixe du namespace"),
    ['for' => 'prefix-namespace-add-option']
);
$options .= $this->Html->tag(
    'label.col-5.font-weight-bold',
    __("URL du namespace"),
    ['for' => 'url-namespace-add-option']
);
$options .= $this->Html->tag('div.col-2', '');
$options .= $this->Html->tag('/div');
$options .= $this->Html->tag(
    'div.display_options.div-striped',
    '',
    ['style' => 'max-height: 400px; overflow-x: hidden;']
);
$options .= $this->Html->tag(
    'div.display_options_tmpl.row.pt-1.pb-1.hide',
    $this->Html->tag('div.col-lg-5', $this->Html->tag('span.value', 'exemple'))
    . $this->Html->tag('div.col-lg-5', $this->Html->tag('span.text', 'exemple'))
    . $this->Html->tag(
        'div.col-lg-2.d-flex.align-items-center',
        $this->Html->tag(
            'span.action',
            $this->Html->tag(
                'button.btn.btn-link.delete',
                $this->Fa->charte('Supprimer')
                . $this->Html->tag('span.sr-only', __("Supprimer")),
                ['type' => 'button', 'title' => __("Supprimer")]
            )
        )
    )
);
$options .= $this->Html->tag(
    'div.row.mt-4.add-namespace',
    $this->Html->tag(
        'div.col-lg-5',
        $this->Html->tag('input', '', ['id' => 'prefix-namespace-add-option', 'class' => 'text'])
    )
    . $this->Html->tag(
        'div.col-lg-5',
        $this->Html->tag('input', '', ['id' => 'url-namespace-add-option', 'class' => 'value'])
    )
    . $this->Html->tag(
        'div.col-lg-2.d-flex.align-items-center',
        $this->Html->tag(
            'span.action',
            $this->Html->tag(
                'button.btn.btn-link.add',
                $this->Fa->i('fa-plus text-success')
                . $this->Html->tag('span.sr-only', __("Ajouter")),
                ['type' => 'button']
            )
        )
    )
);
echo $this->Html->tag('div.namespaces', $options);

echo $this->Form->end();
?>
<script>
    try {
        var namespaces = JSON.parse($('#<?=$idPrefix?>-namespace').val());
        var newNamespaces = {};
        for (let key in namespaces) {
            if (namespaces[key].prefix) {
                newNamespaces[namespaces[key].namespace] = namespaces[key].prefix;
            }
        }
        if (Object.keys(newNamespaces).length > 0) {
            namespaces = newNamespaces;
        }
        setTimeout(function () {
            for (let url in namespaces) {
                insertOption(namespaces[url], url);
                selectOptions.push(namespaces[url]);
            }
        });
    } catch (e) {
    }
    var pathField = $('#<?=$idPrefix?>-data-path');
    var multipleField = $('#<?=$idPrefix?>-multiple');
    var virtualMultipleField = $('#<?=$idPrefix?>-virtual-field-multiple');
    var form = pathField.closest('form');
    var addNamspaceDiv = form.find('.add-namespace');
    var optionTmplDiv = form.find('.display_options_tmpl');
    var displayOptionsDiv = form.find('.display_options');
    var type = form.find('.extract-type');
    var input = form.find('.extract-input');
    var alertMultiple = form.find('p.alert-multiple').hide().removeClass('hide');
    var fileSelector = form.find('.extract-file-selector');
    var selectOptions = [];
    var optionsInput = $('#<?=$idPrefix?>-namespace');
    var dlByWsButton = $('#test-download-by-webservice');

    function useXpath() {
        var button = $(this);
        pathField.val(
            button.closest('tr').find('td').first().text()
        );

        button.closest('.modal')
            .one(
                'hidden.bs.modal',
                () => setTimeout(
                    () => pathField.parent()
                        .parent()
                        .css('background-color', '#5cb85c')
                        .animate({'background-color': 'transparent'}),
                    200 // retarde l'animation
                )
            )
            .modal('hide');

        displayOptionsDiv.empty();
        selectOptions = [];
        for (var namespaceUrl in namespaces) {
            insertOption(namespaces[namespaceUrl], namespaceUrl);
            selectOptions.push({
                prefix: namespaces[namespaceUrl],
                namespace: namespaceUrl
            });
        }
        optionsInput.val(JSON.stringify(selectOptions));
        pathField.focus();
    }

    function insertOption(value, text) {
        let div = optionTmplDiv.clone();
        div.find('span.value').html(TableHelper.wordBreak()($('<div>').html(value).text()));
        div.find('span.text').html(TableHelper.wordBreak()($('<div>').html(text).text()));
        div.removeClass('hide');
        div.removeClass('display_options_tmpl');
        div.find('button.delete').on(
            'click',
            function () {
                div.slideUp(
                    200,
                    function () {
                        var value = $(this).find('span.value').text();
                        for (let i = 0; i < selectOptions.length; i++) {
                            if (selectOptions[i].prefix === value) {
                                selectOptions.splice(i, 1);
                                break;
                            }
                        }
                        optionsInput.val(JSON.stringify(selectOptions));
                        $(this).remove();
                    }
                );
            }
        );
        displayOptionsDiv.append(div);
    }

    $('.add-namespace button.add').on(
        'click',
        function () {
            var value = $('#prefix-namespace-add-option').prop('required', true);
            var text = $('#url-namespace-add-option').prop('required', true);

            if (value.is(':invalid')) {
                value.get(0).reportValidity();
                return;
            }
            if (text.is(':invalid')) {
                text.get(0).reportValidity();
                return;
            }
            value.prop('required', false);
            text.prop('required', false);
            insertOption(value.val(), text.val());
            selectOptions.push({prefix: value.val(), namespace: text.val()});
            optionsInput.val(JSON.stringify(selectOptions));
            value.val('');
            text.val('');
            displayOptionsDiv.stop().animate({
                scrollTop: displayOptionsDiv[0].scrollHeight
            }, 200);
            value.focus();
        }
    );
    var nsPrefix = $('#prefix-namespace-add-option');
    var nsUrl = $('#url-namespace-add-option');
    $('.add-namespace *').blur(
        function () {
            if (nsPrefix.prop('required') && !nsPrefix.val() && !nsUrl.val()) {
                nsPrefix.data('req', true);
                nsPrefix.prop('required', false);
                nsUrl.prop('required', false);
            }
        }
    ).focus(
        function () {
            if (nsPrefix.data('req')) {
                nsPrefix.data('req', false);
                nsPrefix.prop('required', true);
                nsUrl.prop('required', true);
            }
        }
    );
    nsPrefix.add(nsUrl).on(
        'keypress',
        function (event) {
            if (event.originalEvent.keyCode === 13) { // Enter
                $('.add-namespace button.add').click();
                event.preventDefault();
            }
        }
    );

    $('#<?=$idPrefix?>-type').change(
        function () {
            var value = $(this).val();
            form.find('div.type_webservice')
                .hide()
                .find('input, select, textarea')
                .disable();
            form.find('div.type_input')
                .hide()
                .find('input, select, textarea')
                .disable();
            dlByWsButton
                .hide()
                .find('input, select, textarea')
                .disable();

            if (value === '<?=FormExtractorsTable::TYPE_FILE?>'
                || value === '<?=FormExtractorsTable::TYPE_ARCHIVE_FILE?>'
            ) {
                form.find('div.type_input')
                    .show()
                    .find('input, select, textarea')
                    .enable();
            } else if (value === '<?=FormExtractorsTable::TYPE_WEBSERVICE?>') {
                form.find('div.type_webservice')
                    .show()
                    .find('input, select, textarea')
                    .enable();
                dlByWsButton
                    .show()
                    .find('input, select, textarea')
                    .enable();
            }
        }
    ).change();

    $('#<?=$idPrefix?>-data-format').change(
        function () {
            var accept = '';
            var namespacesDiv = form.find('div.namespaces');
            namespacesDiv.hide();
            switch ($(this).val()) {
                case '<?=FormExtractorsTable::DATA_FORMAT_CSV?>':
                    accept = '.csv, text/csv';
                    break;
                case '<?=FormExtractorsTable::DATA_FORMAT_XML?>':
                    accept = '.xml, application/xml';
                    namespacesDiv.show();
                    break;
                case '<?=FormExtractorsTable::DATA_FORMAT_JSON?>':
                    accept = '.json, application/json';
                    break;
            }
            $('#upload-form-extractor-test-file').find('input[accept]')
                .attr('accept', accept);
        }
    ).change();

    function testFilePath(fileupload_id) {
        AsalaeLoading.ajax(
            {
                url: '<?=$this->Url->build('/form-extractors/test-file-path')?>/' + fileupload_id,
                method: 'GET',
                data: {
                    namespace: namespaces,
                    path: $('#<?=$idPrefix?>-data-path').val()
                },
                success: function (content) {
                    alert(content);
                },
                error: () => alert(__("Coordonnées invalides")),
            }
        );
    }

    dlByWsButton.on('click', function () {
        var table = $('#upload-form-extractor-test-file-table');
        generator = TableGenerator.instance[table.attr('data-table-uid')];
        table.find('tr button.delete').each(
            function () {
                eval($(this).attr('data-callback'));
            }
        );
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/form-extractors/test-webservice-file')?>',
            method: 'POST',
            data: $('#<?=$idForm?>').serialize(),
            success: function (content) {
                if (!content || !content.id) {
                    return this.error(content);
                }
                table.removeClass('hide');
                generator.data.push(content);
                TableGenerator.appendActions(generator.data, generator.actions);
                generator.generateAll();
            },
            error: function (e) {
                if (e.responseText) {
                    alert(e.responseText);
                } else {
                    alert(e);
                }
            }
        });
    });

    type.change(
        function () {
            var opt = input.find('option[value="' + input.val() + '"]');
            if ($(this).val() === 'archivefile') {
                fileSelector.closest('.form-group').show();
                input.find('option.file').disable();
                if (opt.hasClass('file')) {
                    input.val('').change();
                }
            } else {
                fileSelector.closest('.form-group').hide();
                input.find('option.file').enable();
                if (opt.hasClass('archivefile')) {
                    input.val('').change();
                }
            }
        }
    ).change();

    input.change(
        function () {
            var opt = input.find('option[value="' + input.val() + '"]');
            if (opt.attr('data-multiple') === '1') {
                alertMultiple.show();
                multipleField.val('1');
                virtualMultipleField.val('1').disable().parent();
            } else {
                alertMultiple.hide();
                multipleField.val('0');
                virtualMultipleField.enable().parent();
            }
        }
    ).change();

    virtualMultipleField.change(
        function () {
            multipleField.val($(this).val());
        }
    );

    AsalaeGlobal.select2($('#<?=$idPrefix?>-form-input-id'));
</script>
