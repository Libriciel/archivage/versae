<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->Form->create(
    $entity,
    [
        'idPrefix' => $idPrefix = 'edit-extractor',
        'id' => $idForm = 'edit-extractor-form',
    ]
);

require 'addedit-common.php';
