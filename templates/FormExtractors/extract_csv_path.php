<?php

/**
 * @var Versae\View\AppView $this
 */

use Versae\Utility\Csv;

echo $this->Html->tag('table.table.table-striped.table-hover');
echo $this->Html->tag('thead');
echo $this->Html->tag('tr');
echo $this->Html->tag('th', __("Chemin CsvPath"));
echo $this->Html->tag('th', __("Valeur"));
echo $this->Html->tag('th', __("Actions"));
echo $this->Html->tag('/tr');
echo $this->Html->tag('/thead');
echo $this->Html->tag('tbody');

$csv = new Csv($file);
$cells = [];

foreach ($csv->getData() as $col => $rows) {
    foreach ($rows as $row => $value) {
        $cells[Csv::colRowToCoords($col, $row)] = $value;
    }
}

foreach ($cells as $coords => $value) {
    echo $this->Html->tag('tr');
    echo $this->Html->tag('td', $coords);
    echo $this->Html->tag(
        'td',
        wordwrap(
            h($value),
            40,
            '<wbr/>',
            true
        )
    );
    echo $this->Html->tag(
        'td.action',
        $this->Html->tag(
            'button.btn.btn-link',
            $this->Fa->i('fa-location-arrow'),
            ['type' => 'button', 'onclick' => 'useXpath.call(this)']
        )
    );
    echo $this->Html->tag('/tr');
}
echo $this->Html->tag('/tbody');
echo $this->Html->tag('/table');
?>
<script>
    namespaces = [];
</script>
