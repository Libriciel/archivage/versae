<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->Form->control(
    'form_calculator.name',
    [
        'label' => __("Nom (identifiant twig)"),
        'pattern' => '[a-zA-Z][a-zA-Z0-9_]*',
        'help' => __(
            "Autorisés: lettres, chiffres et underscores `_` (obligatoirement une lettre en premier caractère)"
        ),
    ]
);
echo $this->Form->control(
    'form_calculator.description',
    [
        'label' => __("Description")
    ]
);
echo $this->Form->control(
    'form_calculator.multiple',
    [
        'type' => 'hidden',
    ]
);
echo $this->Form->control(
    'multiple_with',
    [
        'label' => __("Ce calculateur sera à valeurs multiples"),
        'empty' => __("Non"),
        'help' => __(
            "Créera une valeur de variable pour chaque valeur du champ "
            . "sélectionné.<br>Cette valeur est accessible dans le twig : {0}",
            '{{multiple.value}}'
        ),
        'options' => $fields_multiples,
    ]
);
echo $this->Form->control(
    'type',
    [
        'label' => __("Type"),
        'empty' => __("-- Sélectionnez un type de variable --"),
        'options' => $types,
    ]
);

$repeatable = true; // autorise tous les elements répétables
require 'addedit-vars-common.php';
