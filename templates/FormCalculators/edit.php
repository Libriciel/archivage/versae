<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->Form->create(
    $entity,
    [
        'idPrefix' => $idPrefix = 'edit-variable',
        'id' => $idForm = 'edit-variable-form',
    ]
);

require 'addedit-common.php';
