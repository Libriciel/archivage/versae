<?php

/**
 * @var Versae\View\AppView $this
 */

/**
 * Sinon
 */
$else = $this->Html->tag('fieldset', null, ['class' => 'condition-if', 'data-group' => 'else']);

$elseValue = $this->Html->tag('div.part-field', null, ['data-part' => 'else']);
$elseValue .= $this->Form->control(
    'conditions_else_value.field',
    [
        'aria-label' => __("Champ"),
        'empty' => __("-- Sélectionnez un champ --"),
        'options' => $fields,
        'data-part' => 'else',
        'class' => 'custom-value field field-input with-select',
        'templates' => $partTemplates,
    ]
);
$elseValue .= $this->Html->tag('/div'); // .part-field
$elseValue .= $this->Html->tag('div.part-meta', null, ['data-part' => 'else']);
$elseValue .= $this->Form->control(
    'conditions_else_value.meta',
    [
        'aria-label' => __("Metadonnée de fichier"),
        'empty' => __("-- Sélectionnez une métadonnée de fichier --"),
        'options' => $metas,
        'data-part' => 'else',
        'class' => 'custom-value meta with-select',
        'templates' => $partTemplates,
    ]
);
$elseValue .= $this->Html->tag('/div'); // .part-meta
$elseValue .= $this->Html->tag('div.part-value', null, ['data-part' => 'else']);
$elseValue .= $this->Form->control(
    'conditions_else_value.value',
    [
        'aria-label' => __("Valeur"),
        'data-part' => 'else',
        'class' => 'custom-value value with-select',
        'templates' => $partTemplates,
    ]
);
$elseValue .= $this->Html->tag('/div'); // .part-value
$else .= $this->Form->control(
    'conditions_else_value',
    [
        'label' => __("Valeur du Sinon"),
        'help' => __(
            "Valeur de la variable si les conditions des blocs SI et Sinon SI sont fausses."
        ),
        'templates' => [
            'input' => $this->Html->tag(
                'div',
                $elseValue,
                ['class' => 'fake-input with-prepend-addons']
            ),
        ],
        'prepend' => $this->Form->control(
            'conditions_else_value.type',
            [
                'options' => [
                    'field' => __("Champ"),
                    'meta' => __("Métadonnée de fichier"),
                    'value' => __("Text"),
                ],
                'default' => 'value',
                'required' => false,
                'class' => 'datatype-selector',
                'data-part' => 'else',
                'templates' => $partTemplates,
            ]
        ),
    ]
);
$else .= $this->Html->tag('/fieldset'); // Conditions Sinon

return $else;
