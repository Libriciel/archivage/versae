<?php

/**
 * @var Versae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $formUnit
 */

use Cake\Utility\Hash;
use AsalaeCore\Form\MessageSchema\Seda10Schema;

$name = Hash::get($entity, 'form_unit_keyword_detail.name');
$label = $name;
if (preg_match('/(.*)_(\d+)(.*)/', $name, $m)) {
    $label = $m[1] . $m[3] . ' n°' . ((int)$m[2] + 1);
}
echo $this->Html->tag('h4.h3.mb-5', h($label));

if ($name === 'KeywordType') {
    $values = array_map(
        fn($v) => '<option value="' . $v['value'] . '">' . $v['text'] . '</option>',
        Seda10Schema::getXsdOptions('keywordtype')
    );
    sort($values);
} elseif ($name === 'AccessRestrictionRule_Code' || preg_match('/AccessRule(_\d+)?_Rule/', $name)) {
    $values = array_map(
        fn($v) => '<option value="' . $v['value'] . '">' . $v['text'] . '</option>',
        Seda10Schema::getXsdOptions('access_code')
    );
    sort($values);
} elseif (str_contains($name, 'Date')) {
    $autorizedValues = [
        __(
            "Date au format ISO aaaa-mm-jj ex: {0}",
            (new DateTime())->format('Y-m-d')
        )
    ];
}
if (isset($values)) {
    $autorizedValues = $autorizedValues ?? [];
    $autorizedValues[] = '<select aria-label="values">'
        . '<option>' . __("-- Liste des valeurs autorisées --") . '</option>'
        . implode('', $values)
        . '</select>';
}
if (isset($autorizedValues)) {
    echo $this->Html->tag(
        'p.alert.alert-info',
        __("Le schema impose certaines valeurs pour ce noeud :<br>{0}", implode(', ', $autorizedValues))
    );
}

echo $this->Form->create(
    $entity,
    [
        'idPrefix' => $idPrefix = 'edit-keyword-detail',
        'id' => $idForm = 'edit-keyword-detail-form',
    ]
);

echo $this->Form->control(
    'type',
    [
        'label' => __("Type"),
        'options' => $types,
    ]
);
echo '<hr>';

$repeatable = $formUnit->get('repeatable_fieldset_id_parent') ?: false;
require 'addedit-vars-common.php';
