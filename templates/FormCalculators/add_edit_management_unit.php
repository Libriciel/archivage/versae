<?php

/**
 * @var Versae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $formUnit
 */

use AsalaeCore\Form\MessageSchema\Seda10Schema;
use Cake\Utility\Hash;

$name = Hash::get($entity, 'form_unit_management.name');
$label = $name;
if (preg_match('/(.*)_(\d+)(.*)/', $name, $m)) {
    $label = $m[1] . $m[3] . ' n°' . ((int)$m[2] + 1);
}
echo $this->Html->tag('h4.h3.mb-5', h($label));

if ($name === 'AccessRestrictionRule_Code' || $name === 'AccessRule_Rule') {
    $values = array_map(
        fn($v) => '<option value="' . $v['value'] . '">' . $v['text'] . '</option>',
        Seda10Schema::getXsdOptions('access_code')
    );
    sort($values);
} elseif (in_array($name, ['AccessRestrictionRule_StartDate', 'AppraisalRule_StartDate', 'AccessRule_StartDate'])) {
    $autorizedValues = [__("Date au format ISO aaaa-mm-jj ex: {0}", (new DateTime())->format('Y-m-d'))];
} elseif ($name === 'AppraisalRule_Code') {
    $values = [
        '<option value="conserver">conserver - ' . __("Conserver") . '</option>',
        '<option value="detruire">detruire - ' . __("Détruire") . '</option>',
    ];
} elseif ($name === 'AppraisalRule_FinalAction') {
    $values = [
        '<option value="Keep">Keep - ' . __("Conserver") . '</option>',
        '<option value="Destroy">Destroy - ' . __("Détruire") . '</option>',
    ];
} elseif ($name === 'AppraisalRule_Duration') {
    $values = [];
    for ($i = 0; $i <= 100; $i++) {
        $values[] = '<option value="P' . $i . 'Y">P' . $i . 'Y - ' . __n("{0} an", "{0} ans", $i, $i) . '</option>';
    }
} elseif ($name === 'AppraisalRule_Rule') {
    $values = [];
    for ($i = 0; $i <= 100; $i++) {
        $values[] = '<option value="APP' . $i . 'Y">APP' . $i . 'Y - ' . __n("{0} an", "{0} ans", $i, $i) . '</option>';
    }
}
if (isset($values)) {
    $autorizedValues = $autorizedValues ?? [];
    $autorizedValues[] = '<select aria-label="values">'
        . '<option>' . __("-- Liste des valeurs autorisées --") . '</option>'
        . implode('', $values)
        . '</select>';
}
if (isset($autorizedValues)) {
    echo $this->Html->tag(
        'p.alert.alert-info',
        __("Le schema impose certaines valeurs pour ce noeud :<br>{0}", implode(', ', $autorizedValues))
    );
}

echo $this->Form->create(
    $entity,
    [
        'idPrefix' => $idPrefix = 'edit-management-unit',
        'id' => $idForm = 'edit-management-unit-form',
    ]
);

echo $this->Form->control(
    'type',
    [
        'label' => __("Type"),
        'options' => $types,
    ]
);
echo '<hr>';

$repeatable = $formUnit->get('repeatable_fieldset_id_parent') ?: false;
require 'addedit-vars-common.php';
