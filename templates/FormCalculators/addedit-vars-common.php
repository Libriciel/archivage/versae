<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Utility\Hash;
use Versae\Model\Table\FormVariablesTable;
use Versae\Utility\Twig;

echo '<hr>';

/**
 * Concat
 */
echo $this->Html->tag('div#' . $idPrefix . '-type-concat.type-form');
echo $this->Form->control(
    'twig',
    [
        'id' => $idPrefix . '-twig-concat',
        'label' => __("Représentation twig"),
        'help' => __("Tapez le texte souhaité et/ou insérez les champs à partir de la liste ci-dessous"),
        'class' => 'font-monospace',
    ]
);
echo $this->Form->control(
    'field',
    [
        'id' => $idPrefix . '-field-concat',
        'name' => false,
        'label' => __("Ajouter un champ"),
        'empty' => __("-- Ajouter un champ --"),
        'options' => $fields,
        'class' => 'field-input',
    ]
);
echo $this->Form->control(
    'meta',
    [
        'id' => $idPrefix . '-meta-concat',
        'name' => false,
        'label' => __("Ajouter une métadonnée de fichier"),
        'empty' => __("-- Ajouter une métadonnée de fichier --"),
        'options' => $metas,
    ]
);
echo $this->Form->control(
    'date_format',
    [
        'id' => $idPrefix . '-date_format-concat',
        'label' => __("Format de la date"),
        'options' => $dateFormats,
    ]
);
echo $this->Html->tag('/div');

/**
 * Concat multiple
 */
echo $this->Html->tag('div#' . $idPrefix . '-type-concat_multiple.type-form');
echo $this->Form->control(
    'twig',
    [
        'id' => $idPrefix . '-twig-concat_multiple',
        'label' => __("Représentation twig"),
        'readonly' => true,
        'class' => 'font-monospace',
    ]
);
echo $this->Form->control(
    'join_field',
    [
        'id' => $idPrefix . '-field-concat_multiple',
        'label' => __("Sélectionner un champ multiple"),
        'empty' => __("-- Sélectionner un champ multiple --"),
        'options' => $fields_multiples,
        'class' => 'field-input-multiple',
    ]
);
echo $this->Form->control(
    'join_with',
    [
        'id' => $idPrefix . '-join_with-concat_multiple',
        'label' => __("Séparateur"),
        'placeholder' => ', ',
        'help' => __(
            "Sera inséré entre chaque valeur du champs multiple."
            . " Exemple: virgule puis espace (', ') donnera 'valeur1, valeur2, valeur3...'"
        ),
        'style' => 'font-variant-alternates: visible-controls;',
    ]
);
echo $this->Html->tag('/div');

/**
 * Conditions
 */
echo $this->Html->tag('div#' . $idPrefix . '-type-conditions.type-form');

echo $this->Form->control(
    'twig',
    [
        'id' => $idPrefix . '-twig-conditions',
        'label' => __("Représentation twig"),
        'readonly' => true,
        'class' => 'font-monospace',
        'rows' => 8,
    ]
);

echo $this->Html->tag('span.fake-label', __("Représentation textuelle"));
echo $this->Html->tag(
    'pre.cond-text',
    '',
    ['style' => 'display: none; white-space: break-spaces; overflow: initial;']
);

$partTemplates = [
    'inputContainer' => '{{content}}',
    'formGroup' => '{{input}}',
];

$tabs = $this->Tabs->create($idPrefix . 'tabs', ['style' => 'margin: 0 -15px; padding: 0']);
$tabs->add(
    $idPrefix . '-tab-if',
    $this->Fa->i('fa-code', __("SI")),
    require 'if-tab.php'
);
$tabs->add(
    $idPrefix . 'tab-add-elseif',
    $this->Fa->charte('Ajouter', __("Sinon SI")),
    '',
    [
        'onclick' => 'addElseIf.call(this)',
        'class' => 'add-elseif btn btn-success',
        'li' => ['class' => 'add-elseif-tab'],
    ]
);
$tabs->add(
    $idPrefix . 'tab-else',
    $this->Fa->i('fa-exclamation', __("Sinon")),
    require 'else-tab.php',
    ['li' => ['class' => 'btn-else']]
);
echo $tabs;

echo $this->Html->tag('/div'); // Conditions

/**
 * Switch
 */
echo $this->Html->tag('div#' . $idPrefix . '-type-switch.type-form');

echo $this->Form->control(
    'keyword_list_id_hidden',
    [
        'type' => 'hidden',
        'name' => 'keyword_list_id',
    ]
);
echo $this->Form->control(
    'twig',
    [
        'id' => $idPrefix . '-twig-switch',
        'label' => __("Représentation twig"),
        'readonly' => true,
        'class' => 'font-monospace',
    ]
);

echo $this->Form->control(
    'switch_field',
    [
        'id' => $idPrefix . '-twig-switch_field',
        'label' => __("Champ"),
        'empty' => __("-- Sélectionnez un champ --"),
        'options' => $fields,
        'class' => 'field-input',
    ]
);

echo $this->Form->control(
    'load_keywords',
    [
        'id' => $idPrefix . '-twig-switch_load_keywords',
        'label' => __("Charger une liste de mot clés (code => nom)"),
        'empty' => __("-- Sélectionner une liste de mots clés --"),
        'options' => $keywordLists,
    ]
);

$options = $this->Html->tag('div.row.border-bottom');
$options .= $this->Html->tag(
    'label.col-5.font-weight-bold',
    __("SI valeur du champ"),
    ['for' => 'then-value-add-option-switch']
);
$options .= $this->Html->tag(
    'label.col-5.font-weight-bold',
    __("Valeur de la variable"),
    ['for' => 'if-value-add-option-switch']
);
$options .= $this->Html->tag('div.col-2', '');
$options .= $this->Html->tag('/div');
$options .= $this->Html->tag(
    'div.display_options.div-striped',
    '',
    ['style' => 'max-height: 400px; overflow-x: hidden;']
);
$options .= $this->Html->tag(
    'div.display_options_tmpl.row.pt-1.pb-1.hide',
    $this->Html->tag('div.col-lg-5', $this->Html->tag('input.value', '', ['value' => 'exemple']))
    . $this->Html->tag('div.col-lg-5', $this->Html->tag('input.text', '', ['value' => 'exemple']))
    . $this->Html->tag(
        'div.col-lg-2.d-flex.align-items-center',
        $this->Html->tag(
            'span.action',
            $this->Html->tag(
                'button.btn.btn-link.delete',
                $this->Fa->charte('Supprimer')
                . $this->Html->tag('span.sr-only', __("Supprimer")),
                ['type' => 'button', 'title' => __("Supprimer")]
            )
        )
    )
);
$options .= $this->Html->tag(
    'div.row.mt-4.add-option-inputs',
    $this->Html->tag(
        'div.col-lg-5',
        $this->Html->tag('input', '', ['id' => 'if-value-add-option-switch', 'class' => 'value'])
    )
    . $this->Html->tag(
        'div.col-lg-5',
        $this->Html->tag('input', '', ['id' => 'then-value-add-option-switch', 'class' => 'text'])
    )
    . $this->Html->tag(
        'div.col-lg-2.d-flex.align-items-center',
        $this->Html->tag(
            'span.action',
            $this->Html->tag(
                'button.btn.btn-link.add',
                $this->Fa->i('fa-plus text-success')
                . $this->Html->tag('span.sr-only', __("Ajouter")),
                ['type' => 'button']
            )
        )
    )
);
echo $options;

echo '<br>';
echo $this->Form->control(
    'switch_default_value',
    [
        'label' => __("Valeur par défaut"),
        'required' => true,
    ]
);
echo $this->Form->control(
    'switch_options',
    [
        'type' => 'hidden',
    ]
);
echo $this->Html->tag('/div'); // switch

/**
 * Twig
 */
echo $this->Html->tag('div#' . $idPrefix . '-type-twig.type-form');
echo $this->Form->control(
    'twig',
    [
        'label' => __("Code twig"),
        'help' => __(
            "Les caractères d'acolades ({}) sont réservés pour le code twig"
            . " et seront considérés comme des erreurs s'ils sont utilisés en dehors"
            . " des balises twig ({{ var }} et {% expression %})"
        )
        . '<br>' . __(
            "Tags twig autorisés: {0}",
            '<b>' . implode(', ', Twig::SANDBOX_TAGS) . '</b>'
        )
        . '<br>' . __(
            "Filtres twig autorisés: {0}",
            '<b>' . implode(
                ', ',
                array_map(
                    fn($filter) => '<span title="' . Twig::getFiltersHelp($filter) . '">'
                    . $filter . '</span>',
                    Twig::SANDBOX_FILTERS
                )
            ) . '</b>'
        ) .  __(" (les filtres numériques appliqués à des valeurs incompatibles donneront la valeur \"0\")")
    ]
);
echo $this->Form->control(
    'field',
    [
        'id' => $idPrefix . '-field-twig',
        'name' => false,
        'label' => __("Ajouter un champ"),
        'empty' => __("-- Ajouter un champ --"),
        'options' => $fields,
        'class' => 'field-input',
    ]
);
echo $this->Form->control(
    'meta',
    [
        'id' => $idPrefix . '-meta-twig',
        'name' => false,
        'label' => __("Ajouter une métadonnée de fichier"),
        'empty' => __("-- Ajouter une métadonnée de fichier --"),
        'options' => $metas,
        'class' => 'field-input',
    ]
);
echo $this->Html->tag('/div'); // twig

echo $this->Form->end();

$jsonFields = [];
foreach ($fields as $group => $list) {
    foreach ($list as $field) {
        $text = $group . ' / ';
        if (str_starts_with($field['value'], 'input.')) {
            $sep = strpos($field['text'], '-');
            $text .= substr($field['text'], 0, $sep - 1);
        } else {
            $text .= $field['text'];
        }
        $jsonFields[$field['value']] = $text;
    }
}
$jsonMetas = [];
foreach ($metas as $list) {
    $jsonMetas = array_merge($jsonMetas, $list);
}

$inputFieldsets = [];
foreach (Hash::extract($fields, '{s}.{*}') as $opt) {
    $inputFieldsets[$opt['value']] = $opt;
}

?>
<!--suppress JSUnresolvedVariable, JSUnusedAssignment -->
<script>
    var form = $('#<?=$idForm?>');
    var linkedFields = {};
    var dateFields = <?=json_encode($dates)?>;
    var dateFormat = $('#<?=$idPrefix?>-date_format-concat').closest('.form-group').hide();
    var typeCondition = $('#<?=$idPrefix?>-type-conditions');
    var sectionTmpl = typeCondition.find('.visible-fieldset').first().clone();
    sectionTmpl.find('.custom-value').val('');
    var valueIfType = $('#<?=$idPrefix?>-conditions-if-value-type');
    var valueIfField = $('#<?=$idPrefix?>-conditions-if-value-field');
    var valueIfMeta = $('#<?=$idPrefix?>-conditions-if-value-meta');
    var valueIfValue = $('#<?=$idPrefix?>-conditions-if-value-value');
    var valueElseType = $('#<?=$idPrefix?>-conditions-else-value-type');
    var valueElseField = $('#<?=$idPrefix?>-conditions-else-value-field');
    var valueElseMeta = $('#<?=$idPrefix?>-conditions-else-value-meta');
    var valueElseValue = $('#<?=$idPrefix?>-conditions-else-value-value');
    var reprTwigConcat = $('#<?=$idPrefix?>-twig-concat');
    var reprTwig = $('#<?=$idPrefix?>-twig');
    var reprTwigCondition = $('#<?=$idPrefix?>-twig-conditions');
    var reprTwigSwitch = $('#<?=$idPrefix?>-twig-switch');
    var twigSwitchField = $('#<?=$idPrefix?>-twig-switch_field');
    var twigSwitchDefault = $('#<?=$idPrefix?>-switch-default-value');
    var twigSwitchOptions = $('#<?=$idPrefix?>-switch-options');
    var activeModal = reprTwigSwitch.closest('.modal');
    var optionTmplDiv = activeModal.find('.display_options_tmpl');
    var displayOptionsDiv = activeModal.find('.display_options');
    var selectOptions = [];
    var reloadedConditions = <?=json_encode(Hash::get($entity, 'conditions'))?>;
    var reloadedElseifValues = <?=json_encode(Hash::get($entity, 'conditions_elseif_value'))?>;
    var reloadedSwitchOptions = <?=Hash::get($entity, 'switch_options') ?: '[]'?>;
    var ifValueAddOptionSwitch = $('#if-value-add-option-switch');
    var multipleField = $('#<?=$idPrefix?>-multiple-with');
    var isMultiple = $('#<?=$idPrefix?>-form-calculator-multiple');
    var fieldConcatMultiple = $('#<?=$idPrefix?>-field-concat_multiple');
    var joinWithConcatMultiple = $('#<?=$idPrefix?>-join_with-concat_multiple');
    var twigConcatMultiple = $('#<?=$idPrefix?>-twig-concat_multiple');
    var baseValue = reprTwigConcat.val();
    var twigConcatUndoHistory = [reprTwigConcat.val()];
    var twigConcatRedoHistory = [];
    var undoing = false; // switch
    var operatorOptions = {
        "==": __("égal au"),
        "!=": __("différent du"),
        ">": __("supérieur au"),
        ">=": __("supérieur ou égal au"),
        "<": __("inférieur au"),
        "<=": __("inférieur ou égal au"),
        "in": __("à l'intérieur du"),
        "not in": __("en dehors du"),
    };
    var fieldsOptions = <?=json_encode($jsonFields)?>;
    var metasOptions = <?=json_encode($jsonMetas)?>;
    var inputFieldsets = <?=json_encode($inputFieldsets)?>;
    var requireRepeatableFieldset = <?=json_encode($repeatable)?>;

    $('#<?=$idPrefix?>-type').change(
        function() {
            var value = $(this).val();
            form.find('> div.type-form').hide().find('input, select, textarea').disable();
            if (value) {
                $('#<?=$idPrefix?>-type-' + value).show().find('input, select, textarea').enable();
            }
        }
    ).change();

    /**
     * Insertion des variables dans Représentation twig
     */
    var addFieldSelect = $('#<?=$idPrefix?>-field-concat').add('#<?=$idPrefix?>-meta-concat').change(
        function() {
            var value = $(this).val();
            $(this).val('');
            if (value) {
                var twig = reprTwigConcat.val();
                var caret = reprTwigConcat.getCaretPosition();
                var part1 = twig.substr(0, caret);
                var part2 = twig.substr(caret);
                var insert = value;
                if (dateFields.indexOf(value) >= 0) {
                    var popup = new AsalaePopup($(this).closest('div'));
                    var formatGroup = dateFormat.clone().show();
                    var inputFormat = formatGroup.find('select');
                    inputFormat.attr('id', inputFormat.attr('id') + '-popup');
                    formatGroup.find('label').attr('for', inputFormat.attr('id'));
                    popup.element().append(formatGroup);

                    var submit = $('<button class="btn btn-success btn-small" type="button">')
                        .append('<i class="fa fa-plus-circle" aria-hidden="true"></i>')
                        .append($('<span>').text(__("Ajouter la date")))
                        .on('click', function() {
                            popup.hide();
                            switch (inputFormat.val()) {
                                case 'iso':
                                    insert += '|date("Y-m-d")';
                                    break
                                case 'fr':
                                    insert += '|format_datetime("short", "none", locale="fr")';
                                    break
                                case 'fr_long':
                                    insert += '|format_datetime("full", "none", locale="fr")';
                                    break
                            }
                            reprTwigConcat.val(part1 + '{{' + insert + '}}' + part2);
                            linkedFields[value] = linkedFields[value] ? linkedFields[value] + 1 : 1;
                        });
                    popup.element().append(submit);
                    popup.show();
                    return;
                }
                reprTwigConcat.val(part1 + '{{' + insert + '}}' + part2);
                linkedFields[value] = linkedFields[value] ? linkedFields[value] + 1 : 1;
                updateAvailableOptions($(this));
            }
        }
    );

    /**
     * Donne la liste des variables entre {{<var>}} et leurs positions dans le twig
     * @param element
     * @returns {*[]}
     */
    function extractVarsPositions(element) {
        var twig = element.val();
        var pos = 0;
        var npos = -1;
        var vars = [];
        var curvar;
        do {
            pos = twig.indexOf('{{', pos) + 2;
            npos = twig.indexOf('}}', pos);
            if (pos <= 1 || npos <= pos) { // indexOf() retourne -1 si faux; -1 + 2 = 1
                break;
            }
            curvar = twig.substr(pos, npos - pos);
            vars.push(
                {
                    name: curvar.trim(),
                    begin: pos - 2,
                    end: npos + 2
                }
            );
            pos = npos;
        } while (true);
        return vars;
    }

    /**
     * Permet de savoir si le curseur dans l'input est avant une variable et/ou après ou à l'interieur
     */
    function getPositionType(element) {
        var vars = extractVarsPositions(element);
        var caret = element.getCaretPosition();
        var position = {};
        for (var i = 0; i < vars.length; i++) {
            if (vars[i].begin === caret) {
                position.after = vars[i];
            } else if (vars[i].end === caret) {
                position.before = vars[i];
            } else if (vars[i].begin < caret && vars[i].end > caret) {
                position.inside = vars[i];
            }
        }
        return position;
    }

    /**
     * Affichage/cacher champ ou text (dans "comparer" et "avec")
     */
    function dataPartHideHandler() {
        var value = $(this).val();
        var parent = $(this).closest('.form-group');
        parent.find('.custom-value')
            .hide()
            .disable();
        parent.find('.custom-value.' + value).show().enable();
    }

    typeCondition.find('.visible-fieldset select.datatype-selector').on(
        'change',
        dataPartHideHandler
    ).change();
    typeCondition.find('.visible-fieldset select.datatype-selector').on(
        'change',
        dataPartHideHandler
    ).change();
    typeCondition.find('select.datatype-selector[data-part="if"], select.datatype-selector[data-part="else"]').on(
        'change',
        dataPartHideHandler
    ).change();

    function partToCondition(part) {
        var cond;
        switch (part.type) {
            case 'field':
                cond = part.field;
                break;
            case 'meta':
                cond = part.meta;
                break;
            case 'value':
                cond = JSON.stringify(part.value);
                break;
            default:
                cond = part.type;
        }
        if (part.field_tolower === '1') {
            cond += '|lower';
        }
        return cond;
    }

    function partToConditionText(part) {
        var cond;
        var value;
        switch (part.type) {
            case 'field':
                cond = __("champ {0}", '<mark>'+fieldsOptions[part.field]+'</mark>');
                break;
            case 'meta':
                cond = __("métadonnée de fichier {0}", '<mark>'+metasOptions[part.meta]+'</mark>');
                break;
            case 'value':
                value = part.value;
                if (!value) {
                    cond = '<mark>'+__("valeur vide")+'</mark>';
                } else if (value === ' ') {
                    cond = '<mark>'+__("un espace")+'</mark>';
                } else if (isNaN(value) === false) {
                    cond = __("nombre {0}", '<mark>'+value+'</mark>');
                } else {
                    cond = __("texte {0}", '<mark>'+$('<span>').text(value).html()+'</mark>');
                }
                break;
            case undefined:
                cond = '<mark>'+__("non défini")+'</mark>';
                break;
            default:
                cond = __("valeur {0}", '<mark>'+part.type+'</mark>');
        }
        if (part.field_tolower === '1') {
            cond += " " + __("en minuscule");
        }
        return cond;
    }

    /**
     * Transforme les conditions en valeur twig
     */
    function twigConditionMaker() {
        var condition = "{%\nif ";
        var ors = [];
        var formSerialized = form.serializeObject();
        var data = formSerialized.conditions?.if || [];
        for (let condIndex = 0; condIndex < data.length; condIndex++) {
            let ands = [];
            for (let part = 0; part < data[condIndex].part.length; part = part + 2) {
                let cond = partToCondition(data[condIndex].part[part]);
                cond += ' ' + data[condIndex].part[part].operator + ' ';
                cond += partToCondition(data[condIndex].part[part + 1]);
                ands.push(cond);
            }
            ors.push('(' + ands.join(' and ') + ')');
        }

        condition += ors.join(' or ');
        condition += '\n    %}' + getIfValue();
        condition += '{%\n';

        elseIfData = formSerialized.conditions?.elseif || [];
        for (let elseifIndex = 0; elseifIndex < elseIfData.length; elseifIndex++) {
            let data = elseIfData[elseifIndex]
            condition += 'elseif ';
            ors = [];
            for (let condIndex = 0; condIndex < data.length; condIndex++) {
                let ands = [];
                for (let part = 0; part < data[condIndex].part.length; part = part + 2) {
                    let cond = partToCondition(data[condIndex].part[part]);
                    cond += ' ' + data[condIndex].part[part].operator + ' ';
                    cond += partToCondition(data[condIndex].part[part + 1]);
                    ands.push(cond);
                }
                ors.push('(' + ands.join(' and ') + ')');
            }
            condition += ors.join(' or ');
            condition += '\n    %}' + getElseIfValue(elseifIndex);
            condition += '{%\n';
        }

        condition += 'else\n    %}';
        condition += getElseValue();
        condition += '{%\nendif\n%}';
        reprTwigCondition.val(condition);
        writeConditionText();
    }

    typeCondition.find('.visible-fieldset').find('input, select, textarea').on(
        'keyup change',
        twigConditionMaker
    );
    form.find('select[data-part="if"], input[data-part="if"]').on(
        'keyup change',
        twigConditionMaker
    );
    form.find('select[data-part="else"], input[data-part="else"]').on(
        'keyup change',
        twigConditionMaker
    );

    function addCondition() {
        var button = $(this);
        var condTypeFieldset = button.closest('fieldset[data-group]');
        var condType = condTypeFieldset.attr('data-group');
        var typeIndex = condTypeFieldset.attr('data-group-index') || 0;
        var sectionIndex = parseInt(button.closest('[data-section]').attr('data-section'), 10);
        var parts = [];
        button.closest('.filters').find('[data-part]').each(
            function() {
                if (parts.indexOf($(this).attr('data-part')) === -1) {
                    parts.push($(this).attr('data-part'))
                }
            }
        )
        var partShift = Math.max(...parts, 0) + 1;
        var div = $('<div class="additionnal-conditions">');
        div.append('<hr>');
        div.append($('<span>').text(__("ET")));
        div.append(sectionTmpl.find('button.close').clone().attr('onclick', 'deleteSubSection.call(this)'));
        sectionTmpl.find('.filters').clone().find('> *').each(
            function() {
                $(this).find('[data-part]').each(
                    function() {
                        var part = parseInt($(this).attr('data-part'), 10);
                        var npart = part + partShift;
                        var label = $(this).closest('.form-group').find('label');
                        var id = $(this).attr('id');
                        $(this).attr('data-part', npart);
                        var name = $(this).attr('name');
                        if (name) {
                            name = name.replace(
                                '[if][0][part][' + part + ']',
                                '[' + (condType === 'elseif' ? condType + '][' + typeIndex : condType)
                                + '][' + sectionIndex + '][part][' + npart + ']'
                            );
                            $(this).attr('name', name);
                        }
                        if (id) {
                            id = id.replace(
                                'if-0-part-' + part,
                                (condType === 'elseif' ? condType + '-' + typeIndex : condType)
                                + '-' + sectionIndex + '-part-' + npart
                            );
                            $(this).attr('id', id);
                            label.attr('for', id);
                        }
                    }
                );
                if ($(this).attr('data-part')) {
                    let part = parseInt($(this).attr('data-part'), 10);
                    $(this).attr('data-part', part + partShift);
                }
                div.append($(this));
            }
        );
        div.find('button.btn').remove();
        div.insertBefore(button);

        div.find('select.datatype-selector').on(
            'change',
            dataPartHideHandler
        ).change();
        div.find('input, select, textarea').on(
            'keyup change',
            twigConditionMaker
        );
        updateAvailableOptions(div.find('select.field-input'));
        $(document).trigger('browserfix.browserfix');
    }

    // supprime la possibilité de supprimer le 1er bloc
    typeCondition.find('.condition-if .visible-fieldset[data-section="0"] button.close').remove();

    function deleteSection() {
        var msg = __("Supprimer la section supprimera également tout ce qu'elle contient. Voulez-vous continuer ?");
        if (confirm(msg)) {
            $(this).closest('.additionnal-section').remove();
        }
    }

    function deleteSubSection() {
        var msg = __("Supprimer la condition supprimera également tout ce qu'elle contient. Voulez-vous continuer ?");
        if (confirm(msg)) {
            var firstField = $(this).closest('.filters').find('input, select, textarea');
            $(this).closest('.additionnal-conditions').remove();
            firstField.trigger('change');
        }
    }

    function deleteElseif() {
        var msg = __("Supprimer le bloc Sinon SI supprimera également tout ce qu'il contient. Voulez-vous continuer ?");
        if (confirm(msg)) {
            var div = $(this).closest('div');
            var tab = $('#' + div.attr('aria-labelledby')).parent();
            var ifTab = tab.parent().find('.ui-tab').first().find('a');
            div.remove();
            tab.remove();
            valueIfType.change();
            ifTab.click();
        }
    }

    function addSection(sectionShift = null) {
        var button = $(this);
        var condTypeFieldset = button.closest('fieldset[data-group]');
        var condType = condTypeFieldset.attr('data-group');
        var typeIndex = condTypeFieldset.attr('data-group-index') || 0;
        var fieldset = button.closest('fieldset');
        var sections = [];
        fieldset.find('[data-section]').each(
            function() {
                if (sections.indexOf($(this).attr('data-section')) === -1) {
                    sections.push($(this).attr('data-section'))
                }
            }
        )
        if (sectionShift === null) {
            sectionShift = Math.max(...sections, 0) + 1;
        }
        var div = $('<div class="additionnal-section">');
        var section = $('<fieldset class="visible-fieldset">')
            .attr('data-section', sectionShift);
        div.append($('<span>').text(__("OU")));
        sectionTmpl.clone().find('> *').each(
            function() {
                $(this).find('[data-part][id]').each(
                    function() {
                        var label = $(this).closest('.form-group').find('label');
                        var id = $(this).attr('id').replace(
                            'conditions-if-0',
                            'conditions-' + (
                                condType === 'elseif' ? condType + '-' + typeIndex : condType
                            ) + '-' + sectionShift
                        );
                        $(this).attr(
                            'name',
                            $(this).attr('name').replace(
                                'conditions[if][0]',
                                'conditions['
                                + (condType === 'elseif' ? condType + '][' + typeIndex : condType)
                                + '][' + sectionShift + ']'
                            )
                        )
                            .attr('id', id);
                        label.attr('for', id);
                    }
                );
                if ($(this).attr('data-section')) {
                    let part = parseInt($(this).attr('data-section'), 10);
                    $(this).attr('data-section', sectionShift);
                }
                section.append($(this));
            }
        );
        section.find('h3 > span').text(sectionShift + 1);
        div.append(section);
        div.insertBefore(button);

        div.find('select.datatype-selector').on(
            'change',
            dataPartHideHandler
        ).change();
        div.find('input, select, textarea').on(
            'keyup change',
            twigConditionMaker
        );
        updateAvailableOptions(div.find('select.field-input'));
        $(document).trigger('browserfix.browserfix');
    }

    function addElseIf() {
        var button = $(this);
        var elseIfDiv = $(button.attr('href'));
        var ifDiv = $('#<?=$idPrefix . '-tab-if'?>');
        var groups = [-1];

        // on duplique l'onglet
        var tabsDiv = $('#<?=$idPrefix . 'tabs'?>');
        tabsDiv.tabs('destroy');
        var tab = button.closest('li');
        var newTab = tab.clone(true);
        var newLink = newTab.find('a');

        newTab.removeClass('add-elseif-tab');

        newLink.attr('onclick', '');
        newLink.removeClass('btn');
        newLink.removeClass('btn-success');
        newLink.find('.fa-plus-circle').removeClass('fa-plus-circle').addClass('fa-divide');

        // on calcule l'index du elseif
        var groupIndex = form.find('a.elseif').length;

        // on créer une nouvelle div qui s'ouvrira avec l'onglet
        var newDiv = elseIfDiv.clone();
        var newDivId = elseIfDiv.attr('id') + '-' + groupIndex;
        newDiv.attr('id', newDivId);
        newLink.attr('href', '#'+newDivId);
        newLink.removeClass('add-elseif');
        newLink.addClass('elseif');

        // input = champ 'Valeur du Sinon SI'
        var input = ifDiv.find('[data-part="if"]').closest('.form-group').closest('.form-group').first().clone();
        input.find('input').val('');
        input.find('select').val('value');
        var fieldset = $('<fieldset>')
            .attr('data-group', 'elseif')
            .attr('data-group-index', groupIndex);
        var deleteBtn = $('<button type="button" class="p-2 btn-link text-danger">')
            .attr('onclick', 'deleteElseif.call(this)')
            .append('<?=$this->Fa->charte('Supprimer')?>');
        var deleteSpan = $('<span class="h4 delete">')
            .text(__("Supprimer ce bloc Sinon SI"))
            .append(deleteBtn);

        var addButton = $('<button type="button" onclick="addSection.call(this)" class="btn btn-success add-section">')
            .append('<i class="fa fa-plus-circle fa-space" aria-hidden="true"></i>' + __("Ajouter une condition (OU)"));
        input.find('[data-part="if"]').attr('data-part', 'elseif-' + groupIndex);
        input.find('*[id]').each(function() {
            $(this).attr('id', $(this).attr('id').replace('-if-', '-elseif-') + '-' + groupIndex);
        });
        input.find('*[for]').each(function() {
            $(this).attr('for', $(this).attr('for').replace('-if-', '-elseif-') + '-' + groupIndex)
                .text(__("Valeur du Sinon SI"));
        });
        input.find('*[name]').each(function() {
            $(this).attr('name', $(this).attr('name').replace('_if_', '_elseif_') + '[' + groupIndex + ']')
                .on('keyup change', twigConditionMaker);
        });
        input.find('select.datatype-selector').on(
            'change',
            dataPartHideHandler
        ).change();
        input.find('.help-block').first().text(
            __("Valeur de la variable si la condition de ce bloc Sinon SI est VRAI.")
        );
        fieldset.append(addButton);
        addSection.call(addButton, 0);
        fieldset.find('.additionnal-section > span').first().remove(); // Remove span "OU"
        fieldset.find('.additionnal-section button.close').first().remove();
        var masterFieldset = $('<fieldset>');
        masterFieldset.append(deleteSpan);
        masterFieldset.append(fieldset);
        masterFieldset.append('<hr>');
        masterFieldset.append(input);
        newDiv.append(masterFieldset);

        // on ajoute le tout à la vue
        newTab.insertBefore(form.find('li.btn-else'));
        newDiv.insertBefore(elseIfDiv);
        tabsDiv.tabs();
        setTimeout(() => newLink.click(), 10);
        writeConditionText();
        $(document).trigger('browserfix.browserfix');
    }

    function insertOption(ifValue, thenValue) {
        let div = optionTmplDiv.clone();
        div.find('input.value')
            .val(ifValue)
            .on('change keyup', twigSwitchMaker);
        div.find('input.text')
            .val(thenValue)
            .on('change keyup', twigSwitchMaker);
        div.removeClass('hide');
        div.removeClass('display_options_tmpl');
        div.find('button.delete').on(
            'click',
            function() {
                div.slideUp(
                    200,
                    function() {
                        var value = $(this).find('span.value').text();
                        for (let i = 0; i < selectOptions.length; i++) {
                            if (selectOptions[i].if === value) {
                                selectOptions.splice(i, 1);
                                break;
                            }
                        }
                        $(this).remove();
                        twigSwitchMaker()
                    }
                );
            }
        );
        displayOptionsDiv.append(div);
    }

    $('.add-option-inputs button.add').off('click').on(
        'click',
        function() {
            var ifValue = $('#if-value-add-option-switch');
            var thenValue = $('#then-value-add-option-switch');
            insertOption(ifValue.val(), thenValue.val());
            selectOptions.push({if: ifValue.val(), then: thenValue.val()});
            ifValue.val('');
            thenValue.val('');
            twigSwitchMaker();
            displayOptionsDiv.stop().animate({
                scrollTop: displayOptionsDiv[0].scrollHeight
            }, 200);
            ifValue.focus();
        }
    );
    ifValueAddOptionSwitch.add('#then-value-add-option-switch').on(
        'keypress',
        function (event) {
            if (event.originalEvent.keyCode === 13) { // Enter
                form.find('.add-option-inputs button.add').click();
                event.preventDefault();
            }
        }
    );

    function twigSwitchMaker() {
        selectOptions = [];
        $('.display_options .row').each(function() {
            let ifValue = $(this).find('.value');
            let thenValue = $(this).find('.text');
            selectOptions.push({if: ifValue.val(), then: thenValue.val()});
        });

        var condition = "{%\nswitch "
            + twigSwitchField.val().replace(/[{}]+/g, '')
            + ' %}{%\n';

        for (var i = 0; i < selectOptions.length; i++) {
            let ifValue = '"' + selectOptions[i].if.replace(/\\([\s\S])|(")/g, "\\$1$2") + '"';
            let thenValue = selectOptions[i].then.replace(/[{}]+/g, '')
            condition += '    case ' + ifValue + '\n';
            condition += '        %}' + thenValue + '{%\n';

        }
        let thenValue = twigSwitchDefault.val().replace(/[{}]+/g, '')
        condition += '    default\n';
        condition += '        %}' + thenValue + '{%\n';
        condition += 'endswitch\n%}';
        reprTwigSwitch.val(condition);

        twigSwitchOptions.val(JSON.stringify(selectOptions));
    }

    twigSwitchField.on('change', twigSwitchMaker);
    twigSwitchDefault.on('keyup change', twigSwitchMaker);

    /**
     * Insertion des variables dans Représentation twig
     */
    $('#<?=$idPrefix?>-field-twig').add('#<?=$idPrefix?>-meta-twig').change(
        function() {
            var value = $(this).val();
            $(this).val('');
            if (value) {
                var twig = reprTwig.val();
                var caret = reprTwig.getCaretPosition();
                var part1 = twig.substr(0, caret);
                var part2 = twig.substr(caret);
                reprTwig.val(part1 + value + part2);
            }
        }
    );

    /**
     * Supprime le code twig des autres sections (évite la répétition dans l'édition)
     */
    if (reprTwigCondition.is(':visible') === false) {
        reprTwigCondition.val('');
    }
    if (reprTwigSwitch.is(':visible') === false) {
        reprTwigSwitch.val('');
    }
    if (reprTwig.is(':visible') === false) {
        reprTwig.val('');
    }
    if (reprTwigConcat.is(':visible') === false) {
        reprTwigConcat.val('');
    }

    /**
     * Reconstruit le formulaire de "conditions"
     */
    function reloadedConditionsAndBlock(section, part, i) {
        if (i > 0 && i % 2 === 0) {
            section.find('button.btn-success').click();
        }
        section.find('.datatype-selector[data-part="' + i + '"]')
            .val(part.type)
            .change();
        if (part.operator) {
            section.find('.select-operator[data-part="' + i + '"]')
                .val(part.operator)
                .change();
        }
        if (part.type) {
            section.find('.custom-value.field[data-part="' + i + '"]')
                .val(part[part.type])
                .change();
            if (part.value) {
                section.find('.custom-value.value[data-part="' + i + '"]')
                    .val(part.value)
                    .change();
            } else if (part.meta) {
                section.find('.custom-value.meta[data-part="' + i + '"]')
                    .val(part.meta)
                    .change();
            }
            if (part.field_tolower) {
                $('.custom-filter.filter-lower').prop('checked', true);
            }
        }
    }

    /**
     * 1er bloc du if (ET)
     */
    var groupIf = form.find('[data-group="if"]');
    if (reloadedConditions?.if && reloadedConditions.if[0]?.part?.length > 2) {
        let section = groupIf.find('.visible-fieldset[data-section="0"]');
        for (let i = 2; i < reloadedConditions.if[0].part.length; i++) {
            reloadedConditionsAndBlock(section, reloadedConditions.if[0].part[i], i);
        }
    }
    /**
     * autres blocs du if (OU)
     */
    if (reloadedConditions?.if && reloadedConditions.if?.length > 1) {
        for (let i = 1; i < reloadedConditions.if.length; i++) {
            groupIf.find('button.add-section').click();
            let section = groupIf.find('.visible-fieldset[data-section="' + i + '"]');
            for (let j = 0; j < reloadedConditions.if[i].part.length; j++) {
                reloadedConditionsAndBlock(section, reloadedConditions.if[i].part[j], j);
            }
        }
    }
    /**
     * elseif
     */
    if (reloadedConditions?.elseif) {
        var addElseIfTab = form.find('a.add-elseif')
        for (let i = 0; i < reloadedConditions.elseif.length; i++) {
            addElseIfTab.click();
            let groupElseif = form.find('[data-group="elseif"][data-group-index="' + i + '"]').parent();
            let type = reloadedElseifValues.type[i];
            let value = reloadedElseifValues[type][i];
            groupElseif
                .find('*[name="conditions_elseif_value[type][%d]"]'.format(i))
                .val(type)
                .change();
            groupElseif
                .find('*[name="conditions_elseif_value[%s][%d]"]'.format(type, i))
                .val(value);
            for (let j = 0; j < reloadedConditions.elseif[i].length; j++) {
                let section = groupElseif.find('.visible-fieldset[data-section="' + j + '"]');
                for (let k = 0; k < reloadedConditions.elseif[i][j].part.length; k++) {
                    reloadedConditionsAndBlock(section, reloadedConditions.elseif[i][j].part[k], k);
                }
            }
        }

        setTimeout(() => $('[href="#<?=$idPrefix . '-tab-if'?>"]').click(), 100);
    }

    /**
     * Reconstruit le formulaire switch
     */
    if (reloadedSwitchOptions.length) {
        let ifField = ifValueAddOptionSwitch;
        let thenField = $('#then-value-add-option-switch');
        let btn = form.find('button.add');
        for (let i = 0; i < reloadedSwitchOptions.length; i++) {
            ifField.val(reloadedSwitchOptions[i].if);
            thenField.val(reloadedSwitchOptions[i].then);
            btn.click();
        }
    }

    var infoSelect = activeModal.find('p.alert-info > select');
    AsalaeGlobal.select2(infoSelect, {allowClear: false});
    infoSelect.change(function() {
        var type = $('#<?=$idPrefix?>-type');
        var twig_content = $('#<?=$idPrefix?>-twig-concat');
        if (type.val() === 'concat') {
            twig_content.val($(this).val());
        }
    });

    multipleField.change(
        function() {
            let multipleSelected = $(this).val() !== '';
            let concatOpt = $('#<?=$idPrefix?>-type option[value="<?= FormVariablesTable::TYPE_CONCAT_MULTIPLE ?>"]');
            form.find('select.field-input option.opt-multiple')
                .parent()
                .toggle($(this).val() !== '');
            isMultiple.val($(this).val() ? '1' : '0');

            concatOpt.prop('disabled', multipleSelected);
            if (multipleSelected && concatOpt.is(':selected')) {
                $('#<?=$idPrefix?>-type option[value="<?= FormVariablesTable::TYPE_CONCAT ?>"]').prop('selected', true);
            }

            // désactiver multiple.value si la variable n'est pas à "valeur multiple",
            // et en retirer les occurrences dans le twig
            let selectFieldConcat = $('#<?=$idPrefix?>-field-concat');
            if (multipleSelected) {
                selectFieldConcat.find('option[value="multiple.value"]')
                    .enable()
                    .attr('title', '');
            } else {
                selectFieldConcat.find('option[value="multiple.value"]')
                    .disable()
                    .attr('title', __("On ne peut pas utiliser \"Valeur multiple\" pour une variable non multiple"));

                let twig = reprTwigConcat.val();
                let m = twig.match(/^(.*?)\{\{[-~]? *multiple.value *[-~]?}}(.*)$/);
                while (m) {
                    twig = m[1] + m[2];
                    m = twig.match(/^(.*?)\{\{[-~]? *multiple.value *[-~]?}}(.*)$/) ;
                }
                reprTwigConcat.val(twig);
            }

            selectFieldConcat.each(function() {
                if ($(this).data('select2')) {
                    setTimeout(() => AsalaeGlobal.select2($(this).select2('destroy')), 10);
                }
            });
        }
    ).change();

    function calcConcatMultipleTwig()
    {
        var joinWith = joinWithConcatMultiple.val().replace('\\', '\\\\').replace('"', '\\"');
        twigConcatMultiple.val('{{' + fieldConcatMultiple.val() + '|join("' + joinWith + '")}}');
    }

    fieldConcatMultiple.change(calcConcatMultipleTwig);
    joinWithConcatMultiple.keyup(calcConcatMultipleTwig);

    function getIfValue()
    {
        switch (valueIfType.val()) {
            case 'field':
                return '{{' + valueIfField.val() + '}}';
            case 'meta':
                return '{{' + valueIfMeta.val() + '}}';
            case 'value':
                return valueIfValue.val().replace(/[{}]+/g, '');
            default:
                return '{{' + valueIfType.val() + '}}';
        }
    }

    function getIfValueText()
    {
        var value;
        switch (valueIfType.val()) {
            case 'field':
                value = valueIfField.val();
                return value
                    ? __("la valeur du champ {0}", '<mark>'+fieldsOptions[value]+'</mark>')
                    : __("non défini");
            case 'meta':
                value = valueIfMeta.val();
                return value
                    ? __("la valeur de la métadonnée de fichier {0}", '<mark>'+metasOptions[value]+'</mark>')
                    : __("non défini");
            case 'value':
                value = valueIfValue.val();
                if (!value) {
                    return '<mark>'+__("la valeur vide")+'</mark>';
                } else if (value === ' ') {
                    return '<mark>'+__("un espace")+'</mark>';
                } else if (isNaN(value) === false) {
                    return __("le nombre {0}", '<mark>'+value+'</mark>');
                }
                return __(
                    "le texte {0}",
                    '<mark>'+$('<span>').text(valueIfValue.val().replace(/[{}]+/g, '')).html()+'</mark>'
                );
            default:
                return '{{' + valueIfType.val() + '}}';
        }
    }

    function getElseIfValue(elseifIndex)
    {
        var type = form.find('[name="conditions_elseif_value[type][' + elseifIndex + ']"]').val();
        var value;
        switch (type) {
            case 'field':
                value = form.find('[name="conditions_elseif_value[field][' + elseifIndex + ']"]').val();
                return value ? '{{' + value + '}}' : '';
            case 'meta':
                value = form.find('[name="conditions_elseif_value[meta][' + elseifIndex + ']"]').val();
                return value ? '{{' + value + '}}' : '';
            case 'value':
                return form.find('[name="conditions_elseif_value[value][' + elseifIndex + ']"]').val()
                    .replace(/[{}]+/g, '');
            default:
                return '{{' + type + '}}';
        }
    }

    function getElseIfValueText(elseifIndex)
    {
        var type = form.find('[name="conditions_elseif_value[type][' + elseifIndex + ']"]').val();
        var value;
        switch (type) {
            case 'field':
                value = form.find('[name="conditions_elseif_value[field][' + elseifIndex + ']"]').val();
                return value
                    ? __("la valeur du champ {0}", '<mark>'+fieldsOptions[value]+'</mark>')
                    : __("non défini");
            case 'meta':
                value = form.find('[name="conditions_elseif_value[meta][' + elseifIndex + ']"]').val();
                return value
                    ? __("la valeur de la métadonnée de fichier {0}", '<mark>'+metasOptions[value]+'</mark>')
                    : __("non défini");
            case 'value':
                value = form.find('[name="conditions_elseif_value[value][' + elseifIndex + ']"]').val()
                    .replace(/[{}]+/g, '');
                if (!value) {
                    return '<mark>'+__("la valeur vide")+'</mark>';
                } else if (value === ' ') {
                    return '<mark>'+__("un espace")+'</mark>';
                } else if (isNaN(value) === false) {
                    return __("le nombre {0}", '<mark>'+value+'</mark>');
                }
                return __("le texte {0}", '<mark>'+$('<span>').text(value).html()+'</mark>');
            default:
                return '{{' + type + '}}';
        }
    }

    function getElseValue()
    {
        switch (valueElseType.val()) {
            case 'field':
                return '{{' + valueElseField.val() + '}}';
            case 'meta':
                return '{{' + valueElseMeta.val() + '}}';
            case 'value':
                return valueElseValue.val()
                    .replace(/[{}]+/g, '');
            default:
                return '{{' + valueElseType.val() + '}}';
        }
    }

    function getElseValueText()
    {
        var value;
        switch (valueElseType.val()) {
            case 'field':
                value = valueElseField.val();
                return value
                    ? __("la valeur du champ {0}", '<mark>'+fieldsOptions[value]+'</mark>')
                    : __("non défini");
            case 'meta':
                value = valueElseMeta.val();
                return value
                    ? __("la valeur de la métadonnée de fichier {0}", '<mark>'+metasOptions[value]+'</mark>')
                    : __("non défini");
            case 'value':
                value = valueElseValue.val()
                    .replace(/[{}]+/g, '');
                if (!value) {
                    return '<mark>'+__("la valeur vide")+'</mark>';
                } else if (value === ' ') {
                    return '<mark>'+__("un espace")+'</mark>';
                } else if (isNaN(value) === false) {
                    return __("le nombre {0}", '<mark>'+value+'</mark>');
                }
                return __("le texte {0}", '<mark>'+$('<span>').text(value).html()+'</mark>');
            default:
                return '{{' + valueElseType.val() + '}}';
        }
    }

    function conditionToTextIf()
    {
        var condition = __("Si") + " ";
        var ors = [];
        var formSerialized = form.serializeObject();
        var data = formSerialized.conditions?.if || [];
        for (let condIndex = 0; condIndex < data.length; condIndex++) {
            let ands = [];
            for (let part = 0; part < data[condIndex].part.length; part = part + 2) {
                let cond = partToConditionText(data[condIndex].part[part]);
                let operator = operatorOptions[data[condIndex].part[part].operator];
                cond += " " + __("est");
                if (operator) {
                    cond += ' <b>' + operator + '</b> ';
                } else {
                    cond += ' <mark>' + __("non défini") + '</mark> ';
                }
                cond += partToConditionText(data[condIndex].part[part + 1]);
                ands.push(cond);
            }
            ors.push('<br>' + ands.join('<br><b>'+__("ET")+'</b> ') + '<br>');
        }

        condition += ors.join('<br>'+__("OU"));
        condition += "<br>" + __("alors la valeur sera {0}", getIfValueText());
        return condition;
    }

    function conditionToTextElseIf(elseifIndex)
    {
        let data = elseIfData[elseifIndex];
        if (!data) {
            data = [];
        }
        var condition = __("Sinon SI") + " ";
        ors = [];
        for (let condIndex = 0; condIndex < data.length; condIndex++) {
            let ands = [];
            for (let part = 0; part < data[condIndex].part.length; part = part + 2) {
                let cond = partToConditionText(data[condIndex].part[part]);
                let operator = operatorOptions[data[condIndex].part[part].operator];
                cond += " " + __("est");
                if (operator) {
                    cond += ' <b>' + operator + '</b> ';
                } else {
                    cond += ' <mark>' + __("non défini") + '</mark> ';
                }
                cond += partToConditionText(data[condIndex].part[part + 1]);
                ands.push(cond);
            }
            ors.push('<br>' + ands.join('<br><b>'+__("ET")+'</b> ') + '<br>');
        }
        condition += ors.join('<br>'+__("OU"));
        condition += "<br>" + __("alors la valeur sera {0}", getElseIfValueText(elseifIndex));
        return condition;
    }

    function conditionToTextElse()
    {
        return __("Sinon") + " " + __("la valeur sera {0}", getElseValueText());
    }

    function writeConditionText()
    {
        var conditionToText = conditionToTextIf() + '<br><hr>';
        var elseIfData = form.serializeObject().conditions?.elseif || [];
        for (let elseifIndex = 0; elseifIndex < elseIfData.length; elseifIndex++) {
            conditionToText += conditionToTextElseIf(elseifIndex) + '<br><hr>';
        }
        conditionToText += conditionToTextElse();
        form.find('pre.cond-text').html(conditionToText).show();
    }
    writeConditionText();

    previousKeywordListId = $('#<?=$idPrefix?>-keyword-list-id-hidden');
    $('#<?=$idPrefix?>-twig-switch_load_keywords').on(
        'change',
        function () {
            if (!$(this).val()) {
                if (previousKeywordListId.val()) {
                    if (confirm(
                        __("Retirer la liste de mots-clés supprimera les mots-clés existants. Voulez-vous continuer ?")
                    )
                    ) {
                        insertOption({});
                        displayOptionsDiv.empty();
                    } else {
                        $(this).val(previousKeywordListId.val());
                    }
                    return;
                }
            } else if (!confirm(
                __("Importer cette liste supprimera les mots-clés existants. Voulez-vous continuer ?")
            )
            ) {
                $(this).val(previousKeywordListId.val());
                return;
            }
            previousKeywordListId.val($(this).val());
            var loading = $('<div class="text-center loading-div">')
                .append('<i aria-hidden="true" class="fa fa-4x fa-spinner faa-spin animated">');
            displayOptionsDiv.empty().append(loading);
            $.ajax(
                {
                    url: '<?=$this->Url->build('/forms/keyword-list-content')?>/'+$(this).val(),
                    headers: {Accept: 'application/json'},
                    success: function (content) {
                        for (var i in content) {
                            insertOption(content[i].value, content[i].text);
                            selectOptions.push({if: content[i].value, then: content[i].text});
                        }
                        twigSwitchMaker();
                    },
                    error: function () {
                        alert(PHP.messages.genericError);
                    },
                    complete: function () {
                        loading.remove();
                    }
                }
            );
        }
    );

    function updateAvailableOptions(select) {
        var fieldsets = [];

        // interdit tous les élements répétables
        if (requireRepeatableFieldset === false) {
            select.find('option[data-repeatable="1"]')
                .disable()
                .attr(
                    'title',
                    __(
                        "On ne peut pas utiliser un élément de section répétable"
                        + " en dehors d'une unité d'archives répétables"
                    )
                );
            return;
        // n'autorise qu'une seule section répétable
        } else if (requireRepeatableFieldset !== true) { // fieldset id
            fieldsets.push('' + requireRepeatableFieldset);
            let optgroup = select.find('[data-fieldsets="'+requireRepeatableFieldset+'"]:first').parent();
            if (optgroup.find('[value="section.index"]').length === 0) {
                optgroup.prepend(
                    $('<option class="text" value="section.index" data-repeatable="1">')
                        .attr('data-fieldsets', requireRepeatableFieldset)
                        .text(__("Index numérique de la section"))
                );
            }
        }
        for (let field in linkedFields) {
            if (inputFieldsets[field] && inputFieldsets[field]['data-repeatable']) {
                let fieldsetIds = inputFieldsets[field]['data-fieldsets'] + '';
                fieldsets = fieldsets.concat(fieldsetIds.split(','));
            }
        }
        select.find('option[data-repeatable="1"]').each(function() {
            let field = $(this).attr('value');
            let optFieldsets = $(this).attr('data-fieldsets').split(',');
            // intersection des deux array
            let enabled = fieldsets.filter((v) => optFieldsets.includes(v)).length > 0
                || fieldsets.length === 0;
            if (enabled) {
                $(this).enable().attr('title', '');
            } else {
                $(this).disable().attr('title', __("Ce champ fait partie d'une autre section répétable"));
            }
        });
        select.each(function() {
            if ($(this).data('select2')) {
                setTimeout(() => AsalaeGlobal.select2($(this).select2('destroy')), 10);
            }
        });
    }

    updateAvailableOptions(AsalaeGlobal.select2($('#<?=$idPrefix?>-field-concat')));
    updateAvailableOptions(AsalaeGlobal.select2($('#<?=$idPrefix?>-field-concat_multiple')));
    updateAvailableOptions(AsalaeGlobal.select2($('#<?=$idPrefix?>-twig-switch_field')));
    updateAvailableOptions(AsalaeGlobal.select2($('#<?=$idPrefix?>-field-twig')));
    updateAvailableOptions(form.find('select.field-input')); // selects du bloc Valeur conditionnelle

    /**
     * Empêche de mettre un code twig erronné
     */
    reprTwigConcat.on(
        'change',
        function (event) {
            let error = false;
            let value = $(this).val();
            let availables = Object.values(
                addFieldSelect.find('option').map(
                    function () {
                        return $(this).attr('value');
                    }
                )
            ).filter((v) => v);
            // cette boucle vérifi que les variables sous forme {{input.name}} existent bien dans la liste
            do {
                // (debut jusqu'a la)(1ère var twig)(puis tout le reste)
                let m = value.match(/^(.*?)\{\{[-~]? *(.*?) *[-~]?}}(.*)$/);
                if (m && m.length === 4) {
                    if (availables.indexOf(m[2]) === -1) {
                        // détection des dates formattés (iso, short fr ou full fr)
                        let m2 = m[2].match(
                            /(input\..*?)\|(date\("Y-m-d"\)|format_datetime\("(?:short|full)", "none", locale="fr"\))/
                        );
                        if (!m2 || availables.indexOf(m2[1]) === -1) {
                            let initalValue = $(this).val();
                            let pos = initalValue.indexOf(value);
                            $(this).get(0).setCustomValidity(
                                __("La variable {0} ne fait pas partie des variables disponibles", m[2])
                            );
                            $(this).get(0).reportValidity();
                            return;
                        }
                    }
                    value = m[3];
                } else {
                    value = null;
                }
            } while (value);

            // vérifi que les {{ sont toujours suivi d'un }}
            value = $(this).val();
            let openingCount = (value.match(/\{\{/g) || []).length;
            let fullCount = (value.match(/\{\{.*?}}/g) || []).length;
            if (openingCount !== fullCount) {
                $(this).get(0).setCustomValidity(
                    __("Le nombre d'ouverture de variables Twig {{ ne correspond pas au nombre de fermetures }}.")
                );
                $(this).get(0).reportValidity();
                return;
            }

            // interdit les {% et {#
            if (/\{[#%]/.test(value)) {
                $(this).get(0).setCustomValidity(
                    __("Seules les variables sont autorisées.")
                );
                $(this).get(0).reportValidity();
                return;
            }

            $(this).get(0).setCustomValidity('');
        }
    );
</script>
