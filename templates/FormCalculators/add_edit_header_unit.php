<?php

/**
 * @var Versae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $formUnit
 */

use AsalaeCore\Form\MessageSchema\Seda10Schema;
use Cake\Utility\Hash;

$name = Hash::get($entity, 'form_unit_header.name');
$label = $name;
if (preg_match('/(.*)_(\d+)(.*)/', $name, $m)) {
    $label = $m[1] . $m[3] . ' n°' . ((int)$m[2] + 1);
}
echo $this->Html->tag('h4.h3.mb-5', h($label));

if ($name === 'DescriptionLanguage' || $name === 'Language') {
    $values = array_map(
        fn($v) => '<option value="' . $v['value'] . '">' . $v['text'] . '</option>',
        Seda10Schema::getXsdOptions('lang')
    );
    sort($values);
    $autorizedValues = [__("Code du pays en 3 lettres ex: '<b>fra</b>' pour la France")];
    $autorizedValues[] = '<select aria-label="values"><option>' . __("-- Liste des valeurs autorisées --") . '</option>'
        . implode('', $values) . '</select>';
}
if (isset($autorizedValues)) {
    echo $this->Html->tag(
        'p.alert.alert-info',
        __("Le schema impose certaines valeurs pour ce noeud :<br>{0}", implode(', ', $autorizedValues))
    );
}

echo $this->Form->create(
    $entity,
    [
        'idPrefix' => $idPrefix = 'edit-header-unit',
        'id' => $idForm = 'edit-header-unit-form',
    ]
);

echo $this->Form->control(
    'type',
    [
        'label' => __("Type"),
        'options' => $types,
    ]
);

$repeatable = $formUnit->get('repeatable_fieldset_id_parent') ?: false;
require 'addedit-vars-common.php';
