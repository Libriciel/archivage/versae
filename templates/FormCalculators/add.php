<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->Form->create(
    $entity,
    [
        'idPrefix' => $idPrefix = 'add-variable',
        'id' => $idForm = 'add-variable-form',
    ]
);

require 'addedit-common.php';
