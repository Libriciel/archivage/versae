<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->Form->create(
    $entity,
    [
        'idPrefix' => $idPrefix = 'edit-header-variable',
        'id' => $idForm = 'edit-header-variable-form',
    ]
);

echo $this->Form->control(
    'type',
    [
        'label' => __("Type"),
        'options' => $types,
    ]
);
echo '<hr>';

$repeatable = false;
require 'addedit-vars-common.php';
