<?php

/**
 * @var Versae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $formUnit
 */

use AsalaeCore\Form\MessageSchema\Seda10Schema;
use Cake\Utility\Hash;

$name = Hash::get($entity, 'form_unit_content.name');
$label = $name;
if (preg_match('/(.*)_(\d+)(.*)/', $name, $m)) {
    $label = $m[1] . $m[3] . ' n°' . ((int)$m[2]);
}
if (preg_match('/(.*)_\{n}$/', $name, $m)) {
    $label = $m[1];
}
echo $this->Html->tag('h4.h3.mb-5', h($label));

if ($name === 'DescriptionLevel' && $form->get('seda_version') === 'seda1.0') {
    $values = array_map(
        fn($v) => '<option value="' . $v['value'] . '">' . $v['text'] . '</option>',
        Seda10Schema::getXsdOptions('level')
    );
    sort($values);
} elseif (
    $name === 'CustodialHistory_CustodialHistoryItem@when'
    || str_contains($name, 'Date')
) {
    $autorizedValues = [
        __(
            "Date au format ISO aaaa-mm-jj ex: {0}",
            (new DateTime())->format('Y-m-d')
        )
    ];
} elseif ($name === 'DescriptionLevel') {
    $mapping = [
        'fonds' => 'Fonds',
        'subfonds' => 'Subfonds',
        'class' => 'Class',
        'collection' => 'Collection',
        'series' => 'Series',
        'subseries' => 'Subseries',
        'recordgrp' => 'RecordGrp',
        'subgrp' => 'SubGrp',
        'file' => 'File',
        'item' => 'Item',
        'OtherLevel' => 'OtherLevel',
    ];
    $values = array_map(
        fn($v) => '<option value="' . $mapping[$v['value']] . '">'
        . str_replace($v['value'], $mapping[$v['value']], $v['text'])
        . '</option>',
        Seda10Schema::getXsdOptions('level')
    );
} elseif ($name === 'DescriptionLanguage' || $name === 'Language') {
    $values = array_map(
        fn($v) => '<option value="' . $v['value'] . '">' . $v['text'] . '</option>',
        Seda10Schema::getXsdOptions('lang')
    );
    sort($values);
    $autorizedValues = [__("Code du pays en 3 lettres ex: '<b>fra</b>' pour la France")];
} elseif ($name === 'AccessRestrictionRule_Code' || $name === 'AccessRule_Rule') {
    $values = array_map(
        fn($v) => '<option value="' . $v['value'] . '">' . $v['text'] . '</option>',
        Seda10Schema::getXsdOptions('access_code')
    );
    sort($values);
}
if (isset($values)) {
    sort($values);
    $autorizedValues = $autorizedValues ?? [];
    $autorizedValues[] = '<select aria-label="values"><option>' . __("-- Liste des valeurs autorisées --") . '</option>'
        . implode('', $values) . '</select>';
}
if (isset($autorizedValues)) {
    echo $this->Html->tag(
        'p.alert.alert-info',
        __("Le schema impose certaines valeurs pour ce noeud :<br>{0}", implode(', ', $autorizedValues))
    );
}

echo $this->Form->create(
    $entity,
    [
        'idPrefix' => $idPrefix = 'edit-content-unit',
        'id' => $idForm = 'edit-content-unit-form',
    ]
);

echo $this->Form->control(
    'type',
    [
        'label' => __("Type"),
        'options' => $types,
    ]
);
echo '<hr>';

$repeatable = $formUnit->get('repeatable_fieldset_id_parent') ?: false;
require 'addedit-vars-common.php';
