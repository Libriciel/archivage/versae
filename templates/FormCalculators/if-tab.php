<?php

/**
 * @var Versae\View\AppView $this
 */

/**
 * Conditions SI
 */
$if = $this->Html->tag('fieldset', null, ['class' => 'condition-if', 'data-group' => 'if']);

$if .= $this->Html->tag('fieldset.visible-fieldset', null, ['data-section' => '0']); // Bloc 1
$if .= $this->Html->tag('div')
    . $this->Html->tag('h3', __("Bloc de conditions") . ' ' . $this->Html->tag('span', '1'))
    . $this->Html->tag(
        'button',
        $this->Fa->i('fa-angle-double-up')
        . $this->Html->tag('span.sr-only', __("Replier")),
        [
            'type' => 'button',
            'class' => 'btn btn-link absolute-center',
            'onclick' => 'AsalaeFilter.toggleFilters(this)',
        ]
    )
    . $this->Html->tag(
        'button.close',
        $this->Html->tag('span', '×'),
        [
            'type' => 'button',
            'aria-label' => __("Supprimer la section"),
            'onclick' => 'deleteSection.call(this)',
        ]
    )
    . $this->Html->tag('/div');
$if .= $this->Html->tag('div.filters');

$if .= '<hr>';

/**
 * part 1
 */
$condValue1 = $this->Html->tag('div.part-field', null, ['data-part' => '0']);
$condValue1 .= $this->Form->control(
    'conditions.if.0.part.0.field',
    [
        'aria-label' => __("Champ"),
        'empty' => __("-- Sélectionnez un champ --"),
        'options' => $fields,
        'data-part' => '0',
        'class' => 'custom-value field field-input with-select',
        'templates' => $partTemplates,
    ]
);
$condValue1 .= $this->Html->tag('/div'); // .part-field
$condValue1 .= $this->Html->tag('div.part-meta', null, ['data-part' => '0']);
$condValue1 .= $this->Form->control(
    'conditions.if.0.part.0.meta',
    [
        'aria-label' => __("Metadonnée de fichier"),
        'empty' => __("-- Sélectionnez une métadonnée de fichier --"),
        'options' => $metas,
        'data-part' => '0',
        'class' => 'custom-value meta with-select',
        'templates' => $partTemplates,
    ]
);
$condValue1 .= $this->Html->tag('/div'); // .part-meta
$condValue1 .= $this->Html->tag('div.part-value', null, ['data-part' => '0']);
$condValue1 .= $this->Form->control(
    'conditions.if.0.part.0.value',
    [
        'aria-label' => __("Valeur"),
        'data-part' => '0',
        'class' => 'custom-value value with-select',
        'templates' => $partTemplates,
    ]
);
$condValue1 .= $this->Html->tag('/div'); // .part-value
$if .= $this->Form->control(
    'conditions_if_value',
    [
        'label' => __("Comparer"),
        'templates' => [
            'input' => $this->Html->tag(
                'div',
                $condValue1,
                ['class' => 'fake-input with-prepend-addons']
            ),
        ],
        'prepend' => $this->Form->control(
            'conditions.if.0.part.0.type',
            [
                'label' => __("Comparer"),
                'options' => [
                    'field' => __("Champ"),
                    'meta' => __("Métadonnée de fichier"),
                    'value' => __("Text"),
                    'null' => __("Valeur NULL"),
                    'false' => __("Valeur FALSE"),
                    'true' => __("Valeur TRUE"),
                ],
                'default' => 'field',
                'required' => false,
                'class' => 'datatype-selector',
                'data-part' => '0',
                'templates' => $partTemplates,
            ]
        ),
    ]
);
$if .= $this->Form->control(
    'conditions.if.0.part.0.field_tolower',
    [
        'label' => __("En minuscule"),
        'type' => 'checkbox',
        'data-part' => '0',
        'class' => 'custom-filter filter-lower',
    ]
);

/**
 * Opérateur
 */
$if .= $this->Form->control(
    'conditions.if.0.part.0.operator',
    [
        'label' => __("Opérateur de comparaison"),
        'empty' => __("-- Choisir un opérateur --"),
        'options' => [
            '==' => __("== Egal"),
            '!=' => __("!= Différent"),
            '>' => __("> Supérieur"),
            '>=' => __(">= Supérieur ou égal"),
            '<' => __("< Inférieur"),
            '<=' => __("<= Inférieur ou égal"),
            'in' => __("in Dans"),
            'not in' => __("not in En dehors"),
        ],
        'data-part' => '0',
        'class' => 'select-operator',
    ]
);

/**
 * part 2
 */
$condValue2 = $this->Html->tag('div.part-field', null, ['data-part' => '1']);
$condValue2 .= $this->Form->control(
    'conditions.if.0.part.1.field',
    [
        'aria-label' => __("Champ"),
        'empty' => __("-- Sélectionnez un champ --"),
        'options' => $fields,
        'data-part' => '1',
        'class' => 'custom-value field field-input with-select',
        'templates' => $partTemplates,
    ]
);
$condValue2 .= $this->Html->tag('/div'); // .part-field
$condValue2 .= $this->Html->tag('div.part-meta', null, ['data-part' => '1']);
$condValue2 .= $this->Form->control(
    'conditions.if.0.part.1.meta',
    [
        'aria-label' => __("Metadonnée de fichier"),
        'empty' => __("-- Sélectionnez une métadonnée de fichier --"),
        'options' => $metas,
        'data-part' => '1',
        'class' => 'custom-value meta with-select',
        'templates' => $partTemplates,
    ]
);
$condValue2 .= $this->Html->tag('/div'); // .part-meta
$condValue2 .= $this->Html->tag('div.part-value', null, ['data-part' => '1']);
$condValue2 .= $this->Form->control(
    'conditions.if.0.part.1.value',
    [
        'aria-label' => __("Valeur"),
        'data-part' => '1',
        'class' => 'custom-value value with-select',
        'templates' => $partTemplates,
    ]
);
$condValue2 .= $this->Html->tag('/div'); // .part-value
$if .= $this->Form->control(
    'conditions_if_value',
    [
        'label' => __("Avec"),
        'templates' => [
            'input' => $this->Html->tag(
                'div',
                $condValue2,
                ['class' => 'fake-input with-prepend-addons']
            ),
        ],
        'prepend' => $this->Form->control(
            'conditions.if.0.part.1.type',
            [
                'label' => __("Comparer"),
                'options' => [
                    'field' => __("Champ"),
                    'meta' => __("Métadonnée de fichier"),
                    'value' => __("Text"),
                    'null' => __("Valeur NULL"),
                    'false' => __("Valeur FALSE"),
                    'true' => __("Valeur TRUE"),
                ],
                'default' => 'value',
                'required' => false,
                'class' => 'datatype-selector',
                'data-part' => '1',
                'templates' => $partTemplates,
            ]
        ),
    ]
);
$if .= $this->Form->control(
    'conditions.if.0.part.1.field_tolower',
    [
        'label' => __("En minuscule"),
        'type' => 'checkbox',
        'data-part' => '1',
        'class' => 'custom-filter filter-lower',
    ]
);
$if .= $this->Html->tag(
    'button',
    $this->Fa->charte('Ajouter', __("Ajouter une condition (ET)")),
    ['type' => 'button', 'onclick' => 'addCondition.call(this)', 'class' => 'btn btn-success add-condition']
);

$if .= $this->Html->tag('/div'); // .filters (partie repliable)
$if .= $this->Html->tag('/fieldset'); // Bloc 1

$if .= $this->Html->tag(
    'button',
    $this->Fa->charte('Ajouter', __("Ajouter une condition (OU)")),
    ['type' => 'button', 'onclick' => 'addSection.call(this)', 'class' => 'btn btn-success add-section']
);

$if .= $this->Html->tag('/fieldset'); // Conditions SI

/**
 * Valeur SI = vrai
 */
$ifValue = $this->Html->tag('div.part-field', null, ['data-part' => 'if']);
$ifValue .= $this->Form->control(
    'conditions_if_value.field',
    [
        'aria-label' => __("Champ"),
        'empty' => __("-- Sélectionnez un champ --"),
        'options' => $fields,
        'data-part' => 'if',
        'class' => 'custom-value field field-input with-select',
        'templates' => $partTemplates,
    ]
);
$ifValue .= $this->Html->tag('/div'); // .part-field
$ifValue .= $this->Html->tag('div.part-meta', null, ['data-part' => 'if']);
$ifValue .= $this->Form->control(
    'conditions_if_value.meta',
    [
        'aria-label' => __("Metadonnée de fichier"),
        'empty' => __("-- Sélectionnez une métadonnée de fichier --"),
        'options' => $metas,
        'data-part' => 'if',
        'class' => 'custom-value meta with-select',
        'templates' => $partTemplates,
    ]
);
$ifValue .= $this->Html->tag('/div'); // .part-meta
$ifValue .= $this->Html->tag('div.part-value', null, ['data-part' => 'if']);
$ifValue .= $this->Form->control(
    'conditions_if_value.value',
    [
        'aria-label' => __("Valeur"),
        'data-part' => 'if',
        'class' => 'custom-value value with-select',
        'templates' => $partTemplates,
    ]
);
$ifValue .= $this->Html->tag('/div'); // .part-value

$if .= '<hr>';
$if .= $this->Form->control(
    'conditions_if_value',
    [
        'label' => __("Valeur du SI"),
        'help' => __(
            "Valeur de la variable si la condition du bloc SI est VRAI."
            . " Si elle est fausse, voir les valeurs dans SINON SI et SINON"
        ),
        'templates' => [
            'input' => $this->Html->tag(
                'div',
                $ifValue,
                ['class' => 'fake-input with-prepend-addons']
            ),
        ],
        'prepend' => $this->Form->control(
            'conditions_if_value.type',
            [
                'options' => [
                    'field' => __("Champ"),
                    'meta' => __("Métadonnée de fichier"),
                    'value' => __("Text"),
                ],
                'default' => 'value',
                'required' => false,
                'class' => 'datatype-selector',
                'data-part' => 'if',
                'templates' => $partTemplates,
            ]
        ),
    ]
);
/**
 * endif value
 */

return $if;
