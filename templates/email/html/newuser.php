<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Core\Configure;

?>
<div style="color: #4c575f">
    <h4 style="margin: 0; padding-bottom: 5px; border-bottom: 1px solid #e5e5e5">
        <?=__("Un accès à versae a été ouvert pour vous.")?>
    </h4>
    <p><?=__(
        "Un administrateur vous a créé un compte utilisateur sur versae avec les attributs suivants:"
    )?></p>

    <ul>
        <li><b><?=__("Identifiant de connexion")?>:</b> <?=$user->get('username')?></li>
        <li><b><?=__("Rôle")?>:</b> <?=$user->get('role')->get('name')?></li>
        <li><b><?=__("Membre de l'entité")?>:</b> <?=$user->get('org_entity')->get('name')?></li>
        <?php
        if ($name = $user->get('name')) {
            echo '<li><b>' . __("Nom d'utilisateur") . ':</b> ' . $user->get('name') . '</li>';
        }
        ?>
        <li><b><?=__("Email")?>:</b> <?=$user->get('email')?></li>
        <li><b><?=__("Contraste élevé")?>:</b> <?=$user->get('high_contrast')
                ? __("Oui")
                : __("Non")?></li>
    </ul>
    <p><?=__(
        "Afin de vous connecter à l'application, il vous faudra cliquer "
        . "sur le lien ci-dessous et choisir un mot de passe."
    )?></p>

    <p><?=$this->Html->link(
        __("Lien d'activation de votre compte"),
        trim(Configure::read('App.fullBaseUrl'), '/ ') . '/auth-urls/activate/' . $code
    )?></p>

    <p><?=__(
        "Vous aurez la possibilité de modifier vos informations personnelles "
        . "sur votre interface en cliquant sur \"Modifier mon compte utilisateur\"."
    )?></p>

    <p><?=$this->Html->link(__("Accéder à la plateforme"), Configure::read('App.fullBaseUrl'))?></p>
</div>
