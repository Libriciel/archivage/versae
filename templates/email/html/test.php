<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Core\Configure;

?>
<div style="color: #4c575f">
    <h4 style="margin: 0; padding-bottom: 5px; border-bottom: 1px solid #e5e5e5">
        <?=__("Ceci est un email de test")?>
    </h4>
    <p><?=__("Email de test")?></p>

    <ul>
        <li><b><?=__("Test 1")?>:</b> Foo</li>
        <li><b><?=__("Test 2")?>:</b> bar</li>
        <li><b><?=__("Test 3")?>:</b> Baz</li>
        <li><b><?=__("Test 4")?>:</b> 15268</li>
    </ul>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id sodales nisi.
    Cras tempus tristique leo, id eleifend metus varius dapibus. Aenean volutpat augue eros,
        id venenatis sem suscipit ac. Nunc ultrices urna ut ex suscipit, ullamcorper dapibus orci sollicitudin.
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut laoreet blandit mi vel venenatis.
        Nam aliquam, nisl et sagittis interdum, augue felis lobortis risus, ac feugiat nisi justo quis diam.
        Nulla ac vestibulum nibh. Phasellus blandit dolor dolor, eget fringilla risus rhoncus ac.</p>

    <p><?=$this->Html->link(__("Accéder à la plateforme"), Configure::read('App.fullBaseUrl'))?></p>
</div>
