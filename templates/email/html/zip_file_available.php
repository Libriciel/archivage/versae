<?php
/**
 * @var Versae\View\AppView $this
 * @var bool $success
 * @var string $link
 * @var string $expiration
 */

if ($success) {
    $title = __("Les fichiers {0} sont prêts à être téléchargés", $titleArg);
    $p = __(
        "Vous avez émis une demande de téléchargement de fichiers {0}, en voici le lien de téléchargement :",
        $pArg
    );
    $link = $this->Html->link(__("Télécharger le zip"), $link);
    $after = $this->Html->tag('p', __("Ce lien sera disponible jusqu'au : {0}", $expiration));
} else {
    $title = __("Une erreur est survenue durant la création du fichier zip.");
    $p = __(
        "Une erreur est survenue lors de la création du fichier zip, "
        . "veuillez ré-essayer ou contacter un administrateur."
    );
    $link = $this->Html->link(__("Recommencer"), $link);
    $after = '';
}
?>
    <div style="color: #4c575f">
        <h4 style="margin: 0; padding-bottom: 5px; border-bottom: 1px solid #e5e5e5"><?=$title?></h4>
        <p><?=$p?></p>

        <p><?=$link?></p>
        <?=$after?>
    </div>
