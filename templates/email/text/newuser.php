<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Core\Configure;

?>
<?=__("Un accès à versae a été ouvert pour vous.")?>
<?=__("Un administrateur vous a créé un compte utilisateur sur versae avec les attributs suivants:")?>

<?=__("Identifiant de connexion")?>: <?=$user->get('username')?>
<?=__("Rôle")?>: <?=$user->get('role')->get('name')?>

<?=__("Membre de l'entité")?>: <?=$user->get('org_entity')->get('name')?>
<?php
if ($name = $user->get('name')) {
    echo __("Nom d'utilisateur") . ': ' . $name;
}
?>
<?=__("Email")?>: <?=$user->get('email')?>
<?=__("Contraste élevé")?>: <?=$user->get('high_contrast')
    ? __("Oui")
    : __("Non")?>

<?=__(
    "Afin de vous connecter à l'application, "
    . "il vous faudra cliquer sur le lien ci-dessous et choisir un mot de passe."
)?>

<?=trim(Configure::read('App.fullBaseUrl'), '/ ') . '/auth-urls/activate/' . $code?>

<?=__(
    "Vous aurez la possibilité de modifier vos informations personnelles"
    . " sur votre interface en cliquant sur \"Modifier mon compte utilisateur\"."
)?>

<?=Configure::read('App.fullBaseUrl')?>
