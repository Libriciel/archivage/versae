<?php
/**
 * @var Versae\View\AppView $this
 * @var bool $success
 * @var string $link
 */

if ($success) {
    $title = __("Les fichiers {0} sont prêts à être téléchargés", $titleArg);
    $p = __(
        "Vous avez émis une demande de téléchargement de fichiers {0}, en voici le lien de téléchargement :",
        $pArg
    );
    $link = $this->Html->link(__("Télécharger le zip"), $link);
    $after = $this->Html->tag('p', __("Ce lien sera disponible jusqu'au : {0}", $expiration));
} else {
    $title = __("Une erreur est survenue durant la création du fichier zip.");
    $p = __(
        "Une erreur est survenue lors de la création du fichier zip,"
        . " veuillez ré-essayer ou contacter un administrateur."
    );
    $link = __("Recommencer") . ' ' . $link;
    $after = '';
}
?>
    <?=$title?>
    <?=$p?>
    <?=$link?>
    <?=$after;
