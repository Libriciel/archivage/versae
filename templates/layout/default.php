<?php

/**
 * @var Versae\View\AppView $this
 */

$this->extend('/layout/html');
$this->assign(
    'logo',
    $this->Html->image(
        'versae-color-white.svg',
        [
            'alt' => 'Logo versae',
            'title' => 'versae',
            'style' => 'height: 45px; padding-top: 7px; padding-bottom: 5px;'
        ]
    )
);
$this->assign(
    'main-header',
    $this->element('header')
);
$this->assign(
    'main-side',
    $this->element('side')
);


$this->start('main-content');
?>
<!-- Contenu de la page -->
<div class="main-container">
    <div class="main-content" tabindex="-1">
        <div id="zone-debug"></div>
        <?php
        try {
            echo $this->Flash->render();
        } catch (\Cake\Database\Exception $e) {
        }
        ?>

        <?= $this->fetch('content') ?>
    </div>
</div>
<?php
$this->end();
