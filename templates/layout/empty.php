<?php

/**
 * Structure HTML
 * @var Versae\View\AppView $this
 */

use Cake\I18n\I18n;

?>
<!DOCTYPE html>
<html lang="<?=str_replace('_', '-', I18n::getLocale())?>">
<head>
    <title>versae</title>
</head>
<body>
    <?=$this->fetch('content')?>
</body>
</html>
