<?php

/**
 * @var Versae\View\AppView $this
 */

$this->assign('title-appname', 'versae: ' . $this->fetch('title'));
?>
<!--suppress HtmlUnknownTag -->
<div id="head-content">
    <title><?=$this->fetch('title-appname')?></title>
</div>
<ul id="layout-content">
    <li class="dropdown navbar-item" title="Filtrer" id="menu-li-filter" style="display: none">
        <button data-toggle="dropdown" type="button" class="btn-link">
            <?=$this->Fa->charte('Filtrer')?>
            <span class="resp resp-hide-large" aria-hidden="true"> <?=__("Filtres")?> </span>
            <span class="sr-only"><?=__("Filtres de recherche")?></span>
        </button>
        <ul class="dropdown-menu flex-column filtersSelectWindow">
            <li>
                <button data-toggle="modal" data-target="#modal-filters" class="navbar-link btn-link" type="button">
                    <?=$this->Fa->charte('Modifier', __("Ajouter / Modifier les filtres"))?>
                </button>
            </li>
            <?php if ($this->getRequest()->getParam('?.SaveFilterSelect') !== false) : ?>
            <li class="savedFilter">
                <button class="navbar-link btn-link" onclick="AsalaeGlobal.confirmRemoveFilters()" type="button">
                    <?=$this->Fa->charte('Réinitialiser', __("Réinitialiser les filtres"))?>
                </button>
            </li>
            <?php endif; ?>
            <li role="separator" class="divider"></li>
            <?php
            if (!empty($savedFilters)) {
                echo $this->Filter->selectSaved($savedFilters);
            }
            ?>
        </ul>
    </li>
</ul>
<div id="template-content">
    <div id="zone-debug"></div>
    <?= $this->Flash->render() ?>

    <?= $this->fetch('content') ?>
</div>
