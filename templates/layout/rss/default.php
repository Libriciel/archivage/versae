<?php
/**
 * @var Versae\View\AppView $this
 */
if (!isset($channel)) {
    $channel = [];
}
/** @noinspection PhpArrayIsAlwaysEmptyInspection */
if (!isset($channel['title'])) {
    $channel['title'] = $this->fetch('title');
}

echo $this->Rss->document(
    $this->Rss->channel(
        [],
        $channel,
        $this->fetch('content')
    )
);
