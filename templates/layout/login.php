<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @var Versae\View\AppView $this
 */

use Cake\I18n\I18n;

$cakeDescription = 'versae';
$logo = $this->Html->image(
    'versae-color.svg',
    [
        'alt' => 'Logo',
        'title' => 'versae',
        'class' => 'main-logo',
    ]
);

use Cake\Core\Configure;
?>
<!DOCTYPE html>
<html lang="<?=str_replace('_', '-', I18n::getLocale())?>">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') . "\n" ?>

    <?=$this->element('head')?>
</head>
<body class="bg-login">
<div id="body-content" style="background-image: none">
    <section class="container clearfix main-content container-small bg-white login-container">
        <div class="text-center login-logo-container">
            <?=$logo?>
            <h1 class="logo-title"><?=__("Administration Technique")?></h1>
        </div>
        <div id="zone-debug"></div>
        <div class="row">
            <?= $this->Flash->render() ?>
        </div>
        <?= $this->fetch('content') ?>
    </section>
</div>
<!-- Footer -->
<footer>
    <?=Configure::read('App.name', 'versae')?>
    <?=Configure::read('App.version', VERSAE_VERSION_LONG)?> - © 2010-<?=date('Y')?> Libriciel SCOP
</footer>
<?= $this->Html->script('asalae.global.js') . "\n" ?>
</body>
</html>
