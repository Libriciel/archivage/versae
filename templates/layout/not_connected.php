<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Core\Configure;

$this->extend('/layout/html');
$this->assign(
    'logo',
    $this->Html->image(
        'versae-color-white.svg',
        [
            'alt' => 'LOGO',
            'title' => Configure::read('App.name'),
            'style' => 'height: 45px; padding-top: 7px; padding-bottom: 5px;'
        ]
    )
);
$this->assign(
    'main-header',
    $this->element('header')
);

$this->start('main-content');
?>
    <!-- Contenu de la page -->

    <div class="clearfix main-content">
        <div id="zone-debug"></div>
        <?= $this->Flash->render() ?>

        <?= $this->fetch('content') ?>
    </div>
<?php
$this->end();
