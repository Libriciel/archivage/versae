<?php

/**
 * @var Versae\View\AppView $this
 * @var Cake\Core\Exception\Exception $error
 */
use Cake\Core\Configure;
use Cake\I18n\I18n;

$this->assign('title-appname', 'versae: ' . $this->fetch('title'));
$this->assign(
    'main-head',
    $this->element('global_javascript')
    . $this->element('head')
);
$this->assign('footer', $this->element('footer'));

/**
 * Structure HTML
 */
?>
<!DOCTYPE html>
<html lang="<?=str_replace('_', '-', I18n::getLocale())?>">
<head>
<?php
if ($this->response->getStatusCode() >= 400 && Configure::read('debug') && !empty($error) && !empty($message)) {
    echo "<!-- Facilite la lecture d'une erreur en ajax\n\n";
    echo preg_replace('/([,.])\s+/', "$1\n", $message) . PHP_EOL . PHP_EOL;
    echo str_replace(ROOT, '', (string)$error) . PHP_EOL;
    echo '-->';
}
?>
    <?=$this->Html->charset()?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$this->fetch('title-appname')?></title>
    <?=$this->Html->meta('icon')?>

    <?= $this->Html->meta('favicon.ico', '/favicon.ico', ['type' => 'icon'])?>

    <?=$this->fetch('main-head')?>
</head>
<body class="<?=$this->fetch('body-class')?>">
<div id="body-content">
    <div id="notifications-container"></div>
    <?=$this->fetch('main-header')?>
    <div id="body-row">
        <?=$this->fetch('main-side')?>
        <?=$this->fetch('main-content')?>
    </div>
    <?=$this->fetch('footer')?>
</div>

<!-- scripts -->
<?=$this->element('loading')?>
<?=!empty($user_id) || !empty($admin) ? $this->element('session_timeout') : ''?>
<?=$this->Html->script('asalae.global.js')?>

</body>
</html>
