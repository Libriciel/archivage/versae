<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @var Versae\View\AppView $this
 */

use Cake\I18n\I18n;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html lang="<?=str_replace('_', '-', I18n::getLocale())?>">
<head>
    <title><?=$title ?? $this->fetch('title')?></title>
    <style type='text/css'>
        * {
            font-family: Roboto, Helvetica Neue, sans-serif;
        }
    </style>
</head>
<body style="padding:0; margin:0; background:#f1f1f1;">
<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody>
        <tr>
            <td colspan="3">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td width="20" valign="top" align="center" bgcolor="#222">&nbsp;</td>
                            <td height="50" align="left" bgcolor="#222" width="660">
                                <img alt="Versae"
                                     style="height: 40px;"
                                     src="https://ressources.libriciel.fr/public/versae/img/versae-color-white.svg"
                                     border="0">
                            </td>
                            <td width="20" valign="top" align="center" bgcolor="#222">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="font-size:0;" bgcolor="#f1f1f1">&nbsp;</td>
            <td width="660" bgcolor="#ffffff" align="center">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                    <tr>
                        <td width="600" valign="top" align="center">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                <tr>
                                    <td style="padding-top:10px;" bgcolor="#f1f1f1"></td>
                                </tr>
                                <?php if (!empty($title)) : ?>
                                <tr>
                                    <td style="padding-top:10px;
                                        padding-bottom: 10px;
                                        border-bottom: 1px solid #d9d9d9;
                                        font-family: arial,
                                        sans-serif;
                                        color: #4c575f;
                                        font-size: 32px;
                                        font-weight: 500" bgcolor="#f1f1f1">
                                        <?=$title?>
                                    </td>
                                </tr>
                                <?php endif; ?>
                                <tr>
                                    <td style="padding-top:20px;" bgcolor="#f1f1f1"></td>
                                </tr>
                                <tr>
                                    <td valign="top"
                                        bgcolor="#ffffff"
                                        align="center"
                                        style="color: #4c575f; box-shadow: 0 0 1px rgb(210, 210, 210);">
                                        <table style="padding-bottom:10px; padding-top:10px;margin-bottom: 20px;"
                                               width="100%"
                                               cellspacing="0"
                                               cellpadding="0"
                                               border="0">
                                            <tbody>
                                            <tr valign="bottom">
                                                <td width="20" valign="top" align="center">&nbsp;</td>
                                                <td style="font-family:Calibri, Trebuchet, Arial, sans serif;
                                                    font-size:15px;
                                                    line-height:22px;
                                                     color:#333333;" valign="top">
                                                    <?= $this->fetch('content') ?>
                                                </td>
                                                <td width="20" valign="top" align="center">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <td class="mobMargin" style="font-size:0;" bgcolor="#f1f1f1">&nbsp;</td>
        </tr>
        <tr>
            <td height="20" bgcolor="#f1f1f1" colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td style="font-size:0;" bgcolor="#f1f1f1">&nbsp;</td>
            <td width="660">
<?=$this->getRequest()->getSession()->read('ConfigArchivalAgency.mail-html-signature')?>
            </td>
            <td style="font-size:0;" bgcolor="#f1f1f1">&nbsp;</td>
        </tr>
    </tbody>
</table>
</body>
</html>
