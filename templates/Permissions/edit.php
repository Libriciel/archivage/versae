<?php

/**
 * @var Versae\View\AppView $this
 * @var Versae\Form\PermissionsForm $form
 * @var Cake\Datasource\EntityInterface $entity
 * @var array $controllers
 * @var array $accesses
 * @var array $defaults
 * @var string $aro_id
 */

use Cake\Core\Configure;
use Cake\Utility\Hash;
use Cake\Utility\Text;

$technical = Configure::read('Permissions.display_technical_name');
$prefix = 'perm-edit-cat-';

// tri par groupe
$groups = [];
$optionsGroups = [];
$emptyCategory = __("-- sans catégorie --");
foreach ($controllers as $controller => $actions) {
    foreach ($actions as $action => $params) {
        $params += ['group' => ''];
        if ($params['group'] === '') {
            $params['group'] = $emptyCategory;
        }
        if (!isset($groups[$params['group']])) {
            $groups[$params['group']] = [];
        }
        if (!isset($groups[$params['group']][$controller])) {
            $groups[$params['group']][$controller] = [];
        }
        $groups[$params['group']][$controller][$action] = $params;

        $groupOptionKey = $params['group'];
        if (!isset($optionsGroups[$groupOptionKey])) {
            $optionsGroups[$groupOptionKey] = [];
        }
        $ctrlOptionKey = $prefix . mb_strtolower(
            Text::slug(($groupOptionKey ? $groupOptionKey . '-' : '') . $controller, '-')
        );
        if (!isset($optionsGroups[$groupOptionKey][$ctrlOptionKey])) {
            $trad = $this->Translate->permission($controller);
            if ($technical) {
                $optionsGroups[$groupOptionKey][$ctrlOptionKey]
                    = $controller . ($trad !== $controller ? ' (' . $trad . ')' : '');
            } else {
                $optionsGroups[$groupOptionKey][$ctrlOptionKey] = $trad;
            }
        }
    }
}
$sorter = function ($a, $b) use ($emptyCategory) {
    if (str_starts_with($a, $emptyCategory)) {
        return 1;
    } elseif (str_starts_with($b, $emptyCategory)) {
        return -1;
    }
    return $a > $b ? 1 : -1;
};
uksort($optionsGroups, $sorter);
uksort($groups, $sorter);

if ($entity->get('parent')) {
    if (Hash::get($entity, 'parent.model') === 'Roles') {
        $parent = __("du rôle {0}", Hash::get($entity, 'parent.alias'));
    } else {
        $parent = __("de l'utilisateur {0}", Hash::get($entity, 'parent.alias'));
    }
} else {
    $parent = __("par défaut");
}
$type = $entity->get('model') === 'Roles' ? __("Ce rôle") : __("Cet utilisateur");

echo $this->Html->tag('article');
echo $this->Html->tag('header.bottom-space');
echo $this->Html->tag('h4', h($entity->get('alias')));
echo $this->Html->tag('p', __("{0} hérite des permissions {1}", $type, $parent));
echo $this->Html->tag('/header');
echo $this->Form->control(
    'select',
    [
        'id' => 'edit-perm-group-select',
        'label' => __("Sections"),
        'options' => $optionsGroups,
        'empty' => __("-- Sélectionner une section --"),
        'onchange' => 'scrollToPermission(this)'
    ]
);
echo $this->Form->create($form, ['class' => 'no-padding', 'id' => 'edit-perms-form']);

$options = [
    '0' => __("Hérite"),
    '1' => __("Oui"),
    '-1' => __("Non"),
];

foreach ($groups as $group => $ctrlrs) {
    echo '<hr>';
    echo $this->Html->tag('h4', h($group), ['id' => $prefix . mb_strtolower(Text::slug($group, '-'))]);
    echo $this->Html->tag('table.table.table-striped.table-hover');
    echo $this->Html->tag('tbody');

    $trads = [];
    foreach (array_keys($ctrlrs) as $controller) {
        $trads[$controller] = $this->Translate->permission($controller);
    }
    asort($trads);

    foreach ($trads as $controller => $trad) {
        $actions = $ctrlrs[$controller];
        $buttons = $this->Html->tag(
            'button.btn.btn-default.inherit-all',
            __("Hérite"),
            ['type' => 'button', 'data-controller' => $controller]
        )
            . $this->Html->tag(
                'button.btn.btn-default.yes-all',
                __("Oui"),
                ['type' => 'button', 'data-controller' => $controller]
            )
            . $this->Html->tag(
                'button.btn.btn-default.no-all',
                __("Non"),
                ['type' => 'button', 'data-controller' => $controller]
            );

        if ($technical) {
            echo $this->Html->tag(
                'tr',
                $this->Html->tag('th', $controller)
                . $this->Html->tag('th', $trad !== $controller ? $trad : '')
                . $this->Html->tag('td', $buttons, ['colspan' => 2]),
                ['id' => $prefix . mb_strtolower(Text::slug($group . '-' . $controller, '-'))]
            );
        } else {
            echo $this->Html->tag(
                'tr',
                $this->Html->tag('th', $trad)
                . $this->Html->tag('td', $buttons, ['colspan' => 2]),
                ['id' => $prefix . mb_strtolower(Text::slug($group . '-' . $controller, '-'))]
            );
        }
        foreach ($actions as $action => $params) {
            echo $this->Html->tag('tr');

            $trad = $this->Translate->permission($controller, $action);
            if ($technical) {
                echo $this->Html->tag('td', $action, ['title' => $controller]);
                echo $this->Html->tag('td', $trad !== $action ? $trad : '');
            } else {
                echo $this->Html->tag('td', $trad);
            }
            $path = "controllers.$controller.action.$action";
            $access = Hash::get($accesses, $path);
            echo $this->Html->tag(
                'td',
                $this->Form->control(
                    $path,
                    [
                        'label' => false,
                        'options' => $options,
                        'default' => $default = Hash::get($defaults, $path),
                        'data-initial' => $default,
                        'data-access' => (int)$access,
                        'data-controller' => $controller,
                        'data-action' => $action,
                        'type' => 'radio',
                        'inline' => true,
                        'onchange' => 'colorPermDot(this)',
                        'required' => false,
                    ]
                )
            );
            echo $this->Html->tag('/td');
            echo $this->Html->tag(
                'td',
                $access
                    ? $this->Fa->i('fa-check-circle pull-right text-success no-margin')
                    : $this->Fa->i('fa-times-circle pull-right text-danger no-margin'),
                ['class' => 'h4', 'title' => $access ? __("Autorisé") : __("Non autorisé")]
            );

            echo $this->Html->tag('/tr');
        }
    }
    echo $this->Html->tag('/tbody');
    echo $this->Html->tag('/table');
}

echo $this->Form->end();
echo $this->Html->tag('/article');
?>
<script>
    var authorized = __("Autorisé");
    var forbidden = __("Non autorisé");
    function colorPermDot(element) {
        var input = $(element);
        var initial = input.attr('data-initial');
        var access = input.attr('data-access');
        var inherited = input.attr('data-inherited');
        var dot = input.closest('tr').find('.fa-check-circle, .fa-times-circle')
            .removeClass('fa-times-circle text-danger')
            .removeClass('fa-check-circle text-success');
        switch (input.val()) {
            case '1':
                return dot.addClass('fa-check-circle text-success')
                    .parent().attr('title', authorized);
            case '-1':
                return dot.addClass('fa-times-circle text-danger')
                    .parent().attr('title', forbidden);
            case '0':
                if (initial === '0') {
                    return dot.addClass(access === '1' ? 'fa-check-circle text-success' : 'fa-times-circle text-danger')
                        .parent().attr('title', access === '1' ? authorized : forbidden);
                } else if (inherited) {
                    return dot
                        .addClass(inherited === '1' ? 'fa-check-circle text-success' : 'fa-times-circle text-danger')
                        .parent().attr('title', access === '1' ? authorized : forbidden);
                }
        }
        var controller = input.attr('data-controller');
        var action = input.attr('data-action');
        $.ajax({
            url: '<?=$this->Url->build('/Permissions/ajaxGetPermission/' . $aro_id)?>/'+controller+'/'+action,
            success: function(data) {
                if (data.accessParent) {
                    dot.addClass('fa-check-circle text-success')
                        .parent().attr('title', authorized);
                    input.closest('td').find('input').attr('data-inherited', '1');
                } else {
                    dot.addClass('fa-times-circle text-danger')
                        .parent().attr('title', forbidden);
                    input.closest('td').find('input').attr('data-inherited', '-1');
                }
            }
        });
    }

    $('button.inherit-all').click(function() {
        var controller = $(this).attr('data-controller');
        $(this).closest('table').find('input[data-controller="'+controller+'"][value=0]').click();
    });
    $('button.yes-all').click(function() {
        var controller = $(this).attr('data-controller');
        $(this).closest('table').find('input[data-controller="'+controller+'"][value=1]').click();
    });
    $('button.no-all').click(function() {
        var controller = $(this).attr('data-controller');
        $(this).closest('table').find('input[data-controller="'+controller+'"][value="-1"]').click();
    });

    function scrollToPermission(element) {
        var group = $(element).val();
        $(element).closest('.modal-body').animate({
            scrollTop: $("#"+group).offset().top - 200
        }, 600, function() {
            $("#"+group).css('background-color', '#5cb85c')
                .animate({'background-color': 'transparent'});
        });
    }

    $(function() {
        var form = $('#edit-perms-form');
        var fns = form.data('events').slice(0);
        form.off('submit').on('submit', function(event) {
            var formClone = $(this).clone();
            formClone.find('tbody tr').each(function() {
                var input = $(this).find('input[type=radio]:checked');
                if (input.attr('data-initial') === input.val()) {
                    $(this).remove();
                }
            });
            fns[0].fn.call(formClone, event);
        });
    });
</script>
