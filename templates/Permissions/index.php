<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->element('modal', ['idTable' => $tableId]);

echo $this->Html->tag(
    'div.container',
    $this->Html->tag('h1', __("Permissions"))
    . $this->Breadcrumbs->render()
);

echo $this->Filter->create('permissions-filter')
    ->saves($savedFilters)
    ->filter(
        'model',
        [
            'label' => __("Type"),
            'type' => 'select',
            'multiple' => true,
            'options' => ['Users' => 'Users']
        ]
    )
    ->filter('name', ['label' => 'Nom'])
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date'
        ]
    )
    ->generateSection();

$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover'])
    ->fields(
        [
            'model' => ['label' => __("Type")],
            'name' => ['label' => __("Nom"), 'target' => 'user.name'],
            'created' => ['label' => __("Date de création"), 'target' => 'user.created', 'type' => 'date'],
        ]
    )
    ->data($aros)
    ->params(
        [
            'identifier' => 'id',
        ]
    )
    ->actions(
        [
            [
                'href' => "/permissions/edit/{0}",
                'label' => $this->Fa->charte('Modifier'),
                'title' => __("Modifier {0}", '{1}'),
                'aria-label' => __("Modifier {0}", '{1}'),
                'params' => ['id', 'user.name']
            ],
        ]
    );

echo $this->element(
    'section_table',
    [
        'id' => 'permissions-filter-section',
        'title' => __("Liste des permissions"),
        'table' => $table,
        'pagination' => [
            'options' => [
                'model' => 'Aros'
            ]
        ]
    ]
);
