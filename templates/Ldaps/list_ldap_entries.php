<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->Html->tag('h3.h4', __("LDAP: {0}", $entity->get('name')));

echo $this->ViewTable->generate(
    $entity,
    [
        __("Nom de l'attribut utilisé pour se logger au LDAP") => 'user_login_attribute',
        __("Nom de l'attribut utilisé pour se logger à Versae") => 'user_username_attribute',
        __("Attribut utilisé pour le champ nom") => 'user_name_attribute',
        __("Attribut utilisé pour le champ mail") => 'user_mail_attribute',
    ]
);

echo $this->ModalView
    ->create('view-ldap-entry', ['size' => 'large'])
    ->modal(__("Entrées LDAP"))
    ->output(
        'function',
        'actionViewLdapEntry',
        '/ldaps/viewEntry/' . $entity->id
    )
    ->generate();

$jsTable = $this->Table->getJsTableObject($tableId);

$fields = [
    'user_login_attribute',
    'user_username_attribute',
    'user_name_attribute',
    'user_mail_attribute',
];
$tableFields = [];
foreach ($fields as $field) {
    $ldapAttr = $entity->get($field);
    $tableFields[$ldapAttr] = [
        'label' => $ldapAttr,
        'filter' => [
            $ldapAttr . '[0]' => [
                'id' => "filter-$ldapAttr-0",
                'label' => false,
                'aria-label' => $ldapAttr,
            ],
        ],
    ];
}
$table = $this->Table
    ->create($tableId)
    ->fields($tableFields)
    ->data($data)
    ->params(
        [
            'identifier' => $entity->get('user_username_attribute'),
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionViewLdapEntry('{0}')",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => $title = __("Visualiser {0}", '{0}'),
                'aria-label' => $title,
                'params' => [
                    $entity->get('user_username_attribute'),
                ]
            ],
            [
                'onclick' => "actionLogLdapEntry('{0}', '{1}')",
                'type' => "button",
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Se connecter'),
                'title' => $title = __("Tester la connexion de {0}", '{1}'),
                'aria-label' => $title,
                'params' => [
                    $entity->get('user_login_attribute'),
                    $entity->get('user_username_attribute'),
                ]
            ],
        ]
    );

echo $table;

echo $this->AjaxPaginator->create('pagination-ldap')
    ->url(
        [
            'controller' => 'ldaps',
            'action' => 'list-ldap-entries',
            $id
        ]
    )
    ->table($table)
    ->count($countResults)
    ->generate();
?>
<script>
    function actionLogLdapEntry(login, username)
    {
        var tr = $('tr[data-id="'+username+'"]');
        var table = tr.closest('table');
        var popup = new AsalaePopup(table.parent());
        var input = $('<input type="password" class="form-control" aria-label="password">');
        popup.element()
            .append('<label class="hide">autocomplete obliterator<input type="password" value="false"></label>')
            .append($('<div class="form-group text">').append(input))
            .append('<button class="btn btn-primary btn-small">'+__("Tester")+'</button>')
            .css({
                top: tr.offset().top - table.parent().offset().top,
                "max-width": 600,
                "max-height": 400,
                overflow: "auto"
            })
            .on('submit', function (e) {
                e.preventDefault();
                var password = input.val();
                if (password) {
                    AsalaeLoading.ajax(
                        {
                            url: '<?=$this->Url->build('/ldaps/test-login/' . $entity->id)?>',
                            method: 'POST',
                            headers: {
                                Accept: 'application/json'
                            },
                            data: {
                                username: login,
                                password: password
                            },
                            success: function(content) {
                                if (content) {
                                    alert(__("Connexion réussie!"));
                                } else {
                                    alert(__("Échec de connexion"));
                                }
                            }
                        }
                    );
                }
                popup.hide();
            });
        popup.show();
        input.focus();
    }
</script>
