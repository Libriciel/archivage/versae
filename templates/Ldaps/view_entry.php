<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\ORM\Entity;

echo $this->Html->tag('h3.h4', __("LDAP: {0}", $ldap->get('name')));
echo $this->Html->tag(
    'h4',
    __("Entrée: {0}", $data[$ldap->get('user_username_attribute')] ?? 'null')
);
echo $this->Html->tag(
    'p',
    $ldap->get('account_prefix')
    . ($data[$ldap->get('user_login_attribute')][0] ?? 'null')
    . $ldap->get('account_suffix')
);

$fields = [];
foreach ($data as $attr => $values) {
    foreach ($values as $key => $value) {
        if (strlen($value) > 1024) {
            $data[$attr][$key] = 'binary_data';
        }
    }
    $fields[$attr] = $attr;
}
if (isset($data['mail'][0])) {
    $fields['mail'] = '{do_not_parse}' . $data['mail'][0];
}
echo $this->ViewTable->generate(new Entity($data), $fields, ['id' => 'table-view-ldap-entry']);
?>
<script>
    // parse html
    $('#table-view-ldap-entry').find('.li-row').each(function() {
        var html = $(this).html();
        if (html === 'binary_data') {
            $(this).html('<i>** binary_data **</i>');
        } else {
            $(this).html($('<div></div>').html(html).text());
        }
    });
</script>
