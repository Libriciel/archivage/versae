<?php

/**
 * @var Versae\View\AppView $this
 * @deprecated use admins/addLdap instead
 */

echo $this->Form->control('name', ['label' => __("Nom du LDAP/AD")])
    . $this->Form->control('description', ['label' => __("Description")])
    . '<hr>'
    . $this->Form->control(
        'host',
        [
            'label' => __("Hôte du serveur"),
            'placeholder' => 'localhost',
        ]
    )
    . $this->Form->control(
        'port',
        [
            'label' => __("Port du serveur"),
            'type' => 'number',
            'min' => 0,
            'max' => 65535,
            'placeholder' => 389,
        ]
    )
    . $this->Form->control(
        'use_proxy',
        [
            'label' => __("Utiliser le proxy"),
        ]
    )
    . $this->Form->control(
        'use_ssl',
        [
            'label' => __("SSL"),
        ]
    )
    . $this->Form->control(
        'use_tls',
        [
            'label' => __("TLS"),
        ]
    );

echo $this->Html->tag(
    'button',
    __("Ping"),
    [
        'type' => 'button',
        'class' => 'btn btn-default',
        'onclick' => 'pingLdap(this)'
    ]
);
echo '<hr>';

echo $this->Form->control(
    'account_prefix',
    [
        'label' => __("Prefix pour le login"),
        'placeholder' => 'prefix-',
    ]
) . $this->Form->control(
    'account_suffix',
    [
        'label' => __("Suffix pour le login"),
        'placeholder' => '@versae.fr',
    ]
);

echo $this->Form->control(
    'user_query_login',
    [
        'label' => __("Identifiant de connexion pour les recherches"),
        'placeholder' => 'admin@versae.fr',
    ]
)
    . '<label class="hide">autocomplete obliterator'
    . '<input type="password" value="false"><!-- neutralise le préremplissage firefox --></label>';

echo $this->Form->control(
    'user_query_password',
    [
        'label' => __("Mot de passe de l'utilisateur"),
        'type' => 'password',
    ]
)
    . $this->Form->control(
        'ldap_root_search',
        [
            'label' => __("DN de base"),
            'placeholder' => 'dc=versae,dc=fr',
        ]
    );

echo $this->Html->tag(
    'button',
    __("Récupérer une entrée LDAP"),
    [
        'type' => 'button',
        'class' => 'btn btn-default',
        'onclick' => 'getLdapEntry(this)'
    ]
);
echo '<hr>';

echo $this->Form->control(
    'ldap_users_filter',
    [
        'label' => __("Filtre supplémentaire de recherche"),
        'placeholder' => '(memberOf=versae)',
        'help' => __(
            "Il est conseillé de limiter la recherche aux utilisateurs et"
            . " de placer des parentèses autours de la condition. ex: {0}",
            '(objectClass=inetOrgPerson)'
        )
    ]
)
    . $this->Html->tag(
        'button',
        __("Compter les entrées LDAP (avec filtre)"),
        [
            'type' => 'button',
            'class' => 'btn btn-default',
            'onclick' => 'countLdapEntries(this)'
        ]
    );

echo '<hr>';

echo $this->Form->control(
    'user_login_attribute',
    [
        'label' => __("Nom de l'attribut utilisé pour se logger au LDAP"),
        'placeholder' => 'sAMAccountName',
    ]
)
    . $this->Form->control(
        'user_username_attribute',
        [
            'label' => __("Nom de l'attribut utilisé pour se logger à Versae"),
            'placeholder' => 'uid',
        ]
    )
    . $this->Form->control(
        'user_name_attribute',
        [
            'label' => __("Attribut utilisé pour le champ nom"),
            'placeholder' => 'displayname',
        ]
    )
    . $this->Form->control(
        'user_mail_attribute',
        [
            'label' => __("Attribut utilisé pour le champ mail"),
            'placeholder' => 'mail',
        ]
    )
    . $this->Html->tag(
        'button',
        __("Récupérer une entrée filtrée/mappée"),
        [
            'type' => 'button',
            'class' => 'btn btn-default',
            'onclick' => 'getFilteredLdapEntry(this)'
        ]
    );
?>
<script>
    function pingLdap(btn) {
        var form = $(btn).closest('form');
        $(btn).disable();
        $.ajax({
            url: '<?=$this->Url->build('/ldaps/ping')?>',
            method: 'POST',
            data: form.serialize(),
            success: function() {
                alert('pong!');
            },
            error: function(e) {
                if (e.responseText) {
                    alert(e.responseText);
                } else {
                    alert(e);
                }
            },
            complete: function() {
                $(btn).enable();
            }
        })
    }
    function getLdapEntry(btn) {
        var form = $(btn).closest('form');
        $(btn).disable();
        $.ajax({
            url: '<?=$this->Url->build('/ldaps/get-random-entry')?>',
            method: 'POST',
            data: form.serialize(),
            success: function(content) {
                alert(content);
            },
            error: function(e) {
                if (e.responseText) {
                    alert(e.responseText);
                } else {
                    alert(e);
                }
            },
            complete: function() {
                $(btn).enable();
            }
        })
    }
    function countLdapEntries(btn) {
        var form = $(btn).closest('form');
        $(btn).disable();
        $.ajax({
            url: '<?=$this->Url->build('/ldaps/get-count')?>',
            method: 'POST',
            data: form.serialize(),
            success: function(content) {
                alert(content);
            },
            error: function(e) {
                if (e.responseText) {
                    alert(e.responseText);
                } else {
                    alert(e);
                }
            },
            complete: function() {
                $(btn).enable();
            }
        });
    }
    function getFilteredLdapEntry(btn) {
        var form = $(btn).closest('form');
        $(btn).disable();
        $.ajax({
            url: '<?=$this->Url->build('/ldaps/get-filtered')?>',
            method: 'POST',
            data: form.serialize(),
            success: function(content) {
                alert(content);
            },
            error: function(e) {
                if (e.responseText) {
                    alert(e.responseText);
                } else {
                    alert(e);
                }
            },
            complete: function() {
                $(btn).enable();
            }
        });
    }
</script>
