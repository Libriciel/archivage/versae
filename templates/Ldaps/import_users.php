<?php

/**
 * @var Versae\View\AppView $this
 */

?>
<script>
    function toLabelFor(value, context) {
        var uid = $('#ldaps-table').attr('data-table-uid');
        var forid = 'table-'+uid+'-checkbox'+context.hash;
        setTimeout(function() {
            $('[for="'+forid+'"].sr-only').remove();
            if ($('#'+forid).length === 0) {
                var label = $('[for="'+forid+'"]');
                label.parent().append(label.text());
                label.remove();
            }
        }, 0);
        return $('<label></label>').attr('for', forid).text(value);
    }
</script>
<?php
$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Administration"));
$this->Breadcrumbs->add(__("Utilisateurs"), '/users');
$this->Breadcrumbs->add(h($entity->get('name')));
echo $this->Html->tag(
    'div.container',
    $this->Html->tag(
        'h1',
        $this->Fa->i('fa-address-book-o', __("Annuaires LDAP/AD ({0})", h($entity->get('name'))))
    )
    . $this->Breadcrumbs->render()
);

$jsTable = $this->Table->getJsTableObject($tableId);

echo $this->element('modal', ['idTable' => $tableId, 'paginate' => false]);

echo $this->ModalForm
    ->create('import-user-user-edit')
    ->modal(__("Edition d'un utilisateur"))
    ->javascriptCallback('afterEditUser')
    ->output('function', 'actionEditUser', '/Users/edit-by-admin')
    ->generate();

$attrs = [
    'cn', 'sAMAccountName', 'dn', 'displayname', 'givenname',
    'mail', 'name', 'sn', 'surname', 'uid'
];
$selectedAttrs = [
    $entity->get('user_login_attribute'),
    $entity->get('user_username_attribute'),
    $entity->get('user_name_attribute'),
    $entity->get('user_mail_attribute'),
];
$attrs = array_unique(array_merge($attrs, $selectedAttrs));
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'conf-login' => [
                'label' => 'log LDAP (' . h($entity->get('user_login_attribute')) . ')',
                'callback' => 'toLabelFor',
                'filter' => [
                    h($entity->get('user_login_attribute')) . '[0]' => [
                        'id' => 'filter-conf-login-0',
                        'label' => false,
                        'aria-label' => __("Identifiant LDAP"),
                    ]
                ]
            ],
            'conf-username' => [
                'label' => 'log Versae (' . h($entity->get('user_username_attribute')) . ')',
                'filter' => [
                    h($entity->get('user_username_attribute')) . '[0]' => [
                        'id' => 'filter-conf-username-0',
                        'label' => false,
                        'aria-label' => __("Identifiant de connexion"),
                    ]
                ]
            ],
            'conf-name' => [
                'label' => __("Nom ({0})", h($entity->get('user_name_attribute'))),
                'filter' => [
                    h($entity->get('user_name_attribute')) . '[0]' => [
                        'id' => 'filter-conf-name-0',
                        'label' => false,
                        'aria-label' => __("Nom"),
                    ]
                ]
            ],
            'conf-email' => [
                'label' => __("Email ({0})", h($entity->get('user_mail_attribute'))),
                'filter' => [
                    h($entity->get('user_mail_attribute')) . '[0]' => [
                        'id' => 'filter-conf-email-0',
                        'label' => false,
                        'aria-label' => __("Email"),
                    ]
                ]
            ],
            'attrs' => [
                'target' => '',
                'thead' => $attrs
            ]
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'hash',
            'checkbox' => '!data[{index}].user',
        ]
    )
    ->actions(
        [
            [
                'onclick' => "actionEditUser({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier {0}", '{1}'),
                'aria-label' => $title,
                'displayEval' => 'data[{index}].user !== null',
                'params' => ['user.id', 'user.name']
            ],
            [
                'type' => "button",
                'class' => "btn-link",
                'data-callback' => sprintf(
                    "deleteAction(%s, '%s', {0}, '{2}')",
                    $jsTable,
                    $this->Url->build('/users/delete')
                ),
                'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                'title' => $title = __("Supprimer {0}", '{1}'),
                'aria-label' => $title,
                'confirm' => __("Êtes-vous sûr de vouloir supprimer cet utilisateur ?"),
                'displayEval' => 'data[{index}].user !== null',
                'params' => ['user.id', 'user.username', 'hash']
            ]
        ]
    );

$this->AjaxPaginator->setConfig('limit', $paginateLimit);

echo $this->Html->tag('section.container.bg-white');
echo $this->Html->tag('header');
echo $this->Html->tag('h2.h4', __("Importer les utilisateurs sélectionnés"));
echo $this->Html->tag('/header');

echo $this->Form->create($user, ['id' => 'form-import-ldap-users', 'idPrefix' => 'import-ldap-user']);
echo $this->Form->control(
    'org_entity_id',
    [
        'label' => __("Entité"),
        'options' => $entities,
        'empty' => __("-- Sélectionner une entité sur laquelle rattacher les utilisateurs --")
    ]
);
echo $this->Form->control(
    'role_id',
    [
        'label' => __("Rôle"),
        'options' => [],
        'empty' => __("-- Sélectionner le rôle des utilisateurs --"),
        'disabled' => true,
    ]
);
echo $this->Html->tag(
    'button.btn.btn-primary',
    $this->Fa->i(
        'fa-arrow-up',
        __("Importer {0} utilisateur(s) du LDAP sur versae", $this->Html->tag('span.count', 0))
    )
);
echo $this->Form->end();

echo $this->Html->tag('/section');

$filters = $this->Filter->create('ldap-filter')->saves($savedFilters);
foreach ($attrs as $attr) {
    $filters->filter(
        h($attr),
        [
            'label' => h($attr),
        ]
    );
}
echo $filters->generateSection();
?>
<section class="container bg-white" id="<?=$id?>">
    <header>
        <h2 class="h4"><?=__("Entrées LDAP")?></h2>
        <div class="l-actions h4">
            <?=$this->Fa->charteBtn(
                'Filtrer',
                __("Réinitialiser les filtres de recherche"),
                [
                    'class' => 'btn-link display-active-filter',
                    'text' => $this->Fa->i('fa-times fa-small-append fa-hidden-append text-danger')
                ]
            )
                                        . $this->Fa->button(
                                            'fa-sort-amount-asc',
                                            __("Retirer le tri"),
                                            [
                                                'class' => 'btn-link display-active-sort',
                                                'text' => $this->Fa->i(
                                                    'fa-times fa-small-append fa-hidden-append text-danger'
                                                )
                                            ]
                                        )?>
        </div>
        <div class="r-actions h4">
            <?=$table->getConfigureLink()?>
        </div>
    </header>
    <?=$table->generate()?>
    <div class="row">
        <?=$this->AjaxPaginator->create('pagination-ldap-import-users')
            ->url(
                [
                    'controller' => 'ldaps',
                    'action' => 'import-users',
                    $id
                ]
            )
            ->table($table)
            ->count($countResults)
            ->generate()
?>
    </div>
</section>

<script>
    var form = $('#form-import-ldap-users');
    var submitBtn = form.find('button.btn.btn-primary').disable();
    $('#import-ldap-user-role-id').change(function() {
        if ($(this).val() && table.table.find('.td-checkbox input:checked').length) {
            submitBtn.enable();
        } else {
            submitBtn.disable();
        }
    });

    $('#import-ldap-user-org-entity-id').on('change', function() {
        var that = $(this);
        var value = that.val();
        var roles = $('#import-ldap-user-role-id');
        var rolesVal = roles.val();
        roles.disable().find('option:not([value=""])').remove();
        submitBtn.disable();
        if (!value) {
            return;
        }
        that.disable();
        $.ajax({
            url: '<?=$this->Url->build('/org-entities/get-availables-roles')?>/'+value+'?agent_type=person',
            headers: {
                Accept: 'application/json'
            },
            success: function(content) {
                var option;
                roles.enable();
                for (var key in content) {
                    option = $('<option></option>').attr('value', key).text(content[key]);
                    if (key === rolesVal) {
                        option.prop('selected', true);
                    }
                    roles.append(option);
                }
                roles.change();
            },
            error: function(e) {
                if (e.responseText) {
                    alert(e.responseText);
                } else {
                    alert(e);
                }
            },
            complete: function() {
                that.enable();
            }
        })
    });

    var table = {};
    table = <?=$jsTable?>;
    function checkboxesChangeHandler() {
        table.table.find('.td-checkbox input, input.checkall').change(function() {
            form.find('button.btn.btn-primary span.count').text(
                table.table.find('.td-checkbox input:checked').length
            );
            if (table.table.find('.td-checkbox input:checked').length) {
                $('#import-ldap-user-role-id').change();
            }
        }).change();
    }
    checkboxesChangeHandler();

    form.submit(function(e) {
        var data = [];
        e.preventDefault();
        table.table.find('.td-checkbox input:checked').each(function() {
            data.push(table.getDataId($(this).val()));
        });
        table.table.find('input').disable();
        submitBtn.disable();
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/ldaps/import-users/' . $id)?>',
            method: 'POST',
            headers: {
                "Accept": 'application/json',
                "Content-Type": 'application/json'
            },
            data: JSON.stringify({
                form: {
                    entity: $('#import-ldap-user-org-entity-id').val(),
                    role: $('#import-ldap-user-role-id').val(),
                },
                selected: data,
                bind_users_to_ldap: form.data('bind_users_to_ldap')
            }),
            success: function(content) {
                if (typeof content === 'object' && typeof content.error_type === 'string') {
                    if (content.error_type === 'USER_CONFLICT_SAME_ARCHIVAL_AGENCY') {
                        if (confirm(content.error)) {
                            form.data('bind_users_to_ldap', true).submit();
                        }
                    } else {
                        alert(content.error);
                    }
                    return;
                }

                table.table.find('.td-checkbox input:checked').each(function() {
                    var d = table.getDataId($(this).val());
                    d.user = {username: d.username};
                }).remove();
                form.find('input.checkall').change();

                for (var i = 0; i < content.length; i++) {
                    var d = table.getDataId(content[i].hash);
                    for (var key in content[i]) {
                        d[key] = content[i][key];
                    }
                }

                table.generateAll();
                checkboxesChangeHandler();
            },
            complete: function() {
                table.table.find('input').enable();
                submitBtn.enable();
            }
        });
    });

    function deleteAction(table, url, user_id, trId) {
        var tr = $(table.table).find('tr[data-id="'+trId+'"]');
        tr.find('button').addClass('deleting').addClass('gray').addClass('disabled').disable();
        $.ajax({
            url: url.replace(/\/$/, '') + '/' + user_id,
            method: 'DELETE',
            success: function (message) {
                if (message.report === 'done') {
                    var data = table.getDataId(trId);
                    data.user_id = null;
                    data.user = null;
                    table.generateAll();
                    checkboxesChangeHandler();
                } else {
                    alert(message.report);
                    this.error();
                }
            },
            error: function () {
                tr.addClass('danger');
            }
        });
    }

    /**
     * adapté de TableHelper, surcharge la gestion du AjaxPaginator
     */
    $('#pagination-ldap-import-users').on('initialized.paginator', function() {
        $('#ldaps-import-users-table').off('created.form').on('created.form', function (e, data) {
            var container = $(data.form).on('submit', function (e) {
                e.preventDefault();
                e.stopPropagation();
                var url = $('#LdapFilter-form').attr('action');
                url += (url.indexOf('?') !== -1 ? '&' : '?');
                var formdata = AsalaeFilter.getFiltersData(this);
                AsalaeGlobal.interceptedLinkToAjax(url + formdata);
                $(document).off('click.outside.form');
                container.toggle('drop', function () {
                    $(this).remove();
                });
            });
            setTimeout(function () {
                $(document).off('click.outside.form').on('click.outside.form keydown.outside.form', function (e) {
                    if (e.keyCode === 27
                        || ((!container.is(e.target) && container.has(e.target).length === 0)
                            && $(document).has(e.target).length > 0)
                    ) {
                        $(document).off('click.outside.form');
                        container.toggle('drop', function () {
                            $(this).remove();
                        });
                    }
                });
                container.find('input').on('keydown', function (e) {
                    if (e.keyCode === 13) {
                        e.preventDefault();
                        e.stopPropagation();
                        $(this).closest('form').submit();
                    }
                });
            }, 0);
        });
    });
</script>
