<?php

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Administration"));
$this->Breadcrumbs->add(__("Annuaires LDAP/AD"));
echo $this->Html->tag(
    'div.container',
    $this->Html->tag('h1', $this->Fa->i('fa-address-book-o', __("Annuaires LDAP/AD")))
    . $this->Breadcrumbs->render()
);

$jsTable = $this->Table->getJsTableObject($tableId);
$buttons = [];
if ($this->Acl->check('/ldaps/add')) {
    $buttons[] = $this->ModalForm
        ->create('add-ldap')
        ->modal(__("Ajouter un LDAP/AD"))
        ->javascriptCallback('TableGenericAction.afterAdd(' . $jsTable . ', "Ldaps")')
        ->output(
            'button',
            $this->Fa->charte('Ajouter', __("Ajouter un LDAP/AD")),
            '/ldaps/add'
        )
        ->generate(['class' => 'btn btn-success', 'type' => 'button']);
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}

echo $this->element('modal', ['idTable' => $tableId, 'paginate' => false]);
echo $this->ModalForm
    ->create('edit-ldap')
    ->modal(__("Modifier un LDAP"))
    ->javascriptCallback('TableGenericAction.afterEdit(' . $jsTable . ', "Ldaps")')
    ->output(
        'function',
        'actionEditLdap',
        '/Ldaps/edit'
    )
    ->generate();

require 'ajax_index.php';
