<?php

/**
 * @var Versae\View\AppView $this
 */

echo $this->Form->create($entity, ['idPrefix' => 'edit-ldap']);
require 'addedit-common.php';
echo $this->Form->end();
