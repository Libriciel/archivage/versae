<?php

/**
 * @var Versae\View\AppView $this
 * @var Versae\Form\InstallForm $form
 */

use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;

echo $this->Html->tag('div.container');
echo $this->Html->tag('h1', $this->Fa->i('fa-cogs', __("Installation de {0}", "versae")));

echo $this->Form->create($form, ['id' => 'install-form']);

echo $this->Html->tag('section.bg-white');

if (is_readable(Configure::read('App.paths.path_to_local_config'))) {
    $pathToLocal = include Configure::read('App.paths.path_to_local_config');
    $config = is_readable($pathToLocal)
        ? (array)json_decode(file_get_contents($pathToLocal), true)
        : [];
} else {
    $config = [];
}

$admins = is_readable(Configure::read('App.paths.administrators_json'))
    ? json_decode(file_get_contents(Configure::read('App.paths.administrators_json')), true)
    : [];

$connected = function () {
    try {
        ConnectionManager::get('default')->newQuery()->select('1')->execute()->fetch();
        return true;
    } catch (Exception) {
        return false;
    }
};

$databaseIsInitialized = function () use (&$connected) {
    if (!$connected()) {
        return false;
    }
    try {
        $count = ConnectionManager::get('default')->newQuery()
            ->select('count(*)')
            ->from('phinxlog')
            ->execute()
            ->fetch();
        $count = $count[0] ?? 0;
        $c = 0;
        foreach (glob(CONFIG . 'Migrations' . DS . '*.php') as $uri) {
            $filename = basename($uri, '.php');
            if (preg_match('/^\d+_\w+$/', $filename)) {
                $c++;
            }
        }
        return $count >= $c;
    } catch (Exception) {
        return false;
    }
};

if (empty($config['App'])) {
    echo $this->Html->tag('header');
    echo $this->Html->tag('h2.h4', __("Création du fichier de configuration"));
    echo $this->Html->tag('/header');

    echo $this->Form->control(
        'local_config_file',
        [
            'label' => __("Emplacement du fichier app_local.json"),
            'placeholder' => '/path/to/app_local.json',
            'default' => CONFIG . 'app_local.json',
            'help' => __(
                "Contient la configuration de l'application avec des données "
                . "sensibles telles que le mot de passe de la base de données."
                . "\nDoit être ''writable'' pour l'installation et ''readable'' pour la production."
            ),
        ]
    );
    echo $this->Form->control(
        'Config__App__paths__data',
        [
            'label' => __("Emplacement du dossier data"),
            'placeholder' => '/path/to/data',
            'default' => ROOT . DS . 'data',
            'help' => __("Contient les fichiers (ex: pièces jointes de transfers)"),
        ]
    );
    echo $this->Form->control(
        'Config__App__paths__administrators_json',
        [
            'label' => __("Emplacement du fichier administrators.json"),
            'placeholder' => '/path/to/data/administrators.json',
            'default' => ROOT . DS . 'data' . DS . 'administrators.json',
            'help' => __("Emplacement de la liste des administrateurs technique"),
        ]
    );
} elseif (count($config['App']) < 4) {
    /**=======================================================================
     * Application
     * =======================================================================*/
    echo $this->Html->tag('header');
    echo $this->Html->tag('h2.h4', __("Application"));
    echo $this->Html->tag('/header');

    if (empty($config['App']['defaultLocale'])) {
        echo $this->Form->control(
            'Config__App__defaultLocale',
            [
                'label' => __("Locale par défaut"),
                'placeholder' => 'en_US',
                'help' => __(
                    "The default locale for translation, formatting currencies and numbers, date and time."
                ),
                'pattern' => '^[a-z]{2,3}(_[A-Z]{2})?',
                'default' => Configure::read('App.defaultLocale')
            ]
        );
    }
    if (empty($config['App']['timezone'])) {
        echo $this->Form->control(
            'Config__App__timezone',
            [
                'label' => __("Timezone par défaut"),
                'placeholder' => 'Europe/Paris',
                'default' => Configure::read('App.timezone')
            ]
        );
    }
    if (empty($config['App']['fullBaseUrl'])) {
        $host = $this->getRequest()->getEnv('HTTP_HOST', '');
        $actualLink = preg_replace(
            '/install([\/?].*$|$)/i',
            '',
            ($this->getRequest()->getEnv('HTTPS', 'off') === 'on' ? "https" : "http") . "://" . $host
        );
        echo $this->Form->control(
            'Config__App__fullBaseUrl',
            [
                'label' => __("URL complète de l'application"),
                'placeholder' => 'https://versae.macollectivite.fr',
                'help' => __("A base URL to use for absolute links."),
                'default' => Configure::read('App.fullBaseUrl')
            ]
        );
        echo $this->Form->control(
            'ignore_invalid_fullbaseurl',
            [
                'label' => __("Ignore les erreurs sur l'url de base de l'application"),
                'type' => 'checkbox',
            ]
        );
    }
} elseif (empty($config['Ratchet']['connect'])) {
    $host = $this->getRequest()->getEnv('HTTP_HOST', '');
    $actualLink = preg_replace(
        '/install([\/?].*$|$)/i',
        '',
        ($this->getRequest()->getEnv('HTTPS', 'off') === 'on' ? "https" : "http") . "://" . $host
    );
    $info = parse_url($actualLink);
    if (($info['scheme'] ?? 'http') === 'https') {
        $default = 'wss://' . ($info['host'] ?? 'localhost') . '/wss';
    } else {
        $default = 'ws://' . ($info['host'] ?? 'localhost') . '/ws';
    }
    echo $this->Form->control(
        'Config__Ratchet__connect',
        [
            'label' => __("URL du socket pour le client"),
            'placeholder' => 'wss://versae.macollectivite.fr/wss',
            'default' => $default,
            'help' => __(
                "Voir {0}",
                $this->Html->link(
                    'mod_proxy_wstunnel',
                    'https://httpd.apache.org/docs/2.4/mod/mod_proxy_wstunnel.html',
                    ['target' => '_blank']
                )
            )
        ]
    );
} elseif (empty($config['Datasources']['default'])) {
    /**=======================================================================
     * Base de données
     * =======================================================================*/
    echo $this->Html->tag('header');
    echo $this->Html->tag('h2.h4', __("Base de données"));
    echo $this->Html->tag('/header');

    echo $this->Form->control(
        'Config__Datasources__default__host',
        [
            'label' => __("Host"),
            'placeholder' => 'localhost',
        ]
    );
    echo $this->Form->control(
        'Config__Datasources__default__username',
        [
            'label' => __("Identifiant de connexion"),
            'placeholder' => 'versae',
        ]
    );
    echo '<label class="hide">autocomplete obliterator'
        . '<input type="password" value="false"><!-- neutralise le préremplissage firefox --></label>';
    echo $this->Form->control(
        'Config__Datasources__default__password',
        [
            'label' => __("Mot de passe"),
            'placeholder' => '*********',
            'type' => 'password',
        ]
    );
    echo $this->Form->control(
        'confirm_database_password',
        [
            'label' => __("Confirmer le mot de passe"),
            'placeholder' => '*********',
            'type' => 'password',
        ]
    );
    echo $this->Form->control(
        'Config__Datasources__default__database',
        [
            'label' => __("Nom de la base de donnée"),
            'placeholder' => 'versae',
        ]
    );
} elseif (empty($config['Security']['salt'])) {
    /**=======================================================================
     * Clé de sécurité
     * =======================================================================*/
    echo $this->Html->tag('header');
    echo $this->Html->tag('h2.h4', __("Clé de sécurité"));
    echo $this->Html->tag('/header');

    echo $this->Form->control(
        'security_salt_method',
        [
            'label' => __("Clé de sécurité"),
            'empty' => true,
            'data-placeholder' => __("-- Choisir la méthode de génération --"),
            'options' => [
                'apply' => __("Saisir une valeur"),
                'generate' => __("Générer une clé aléatoire"),
                'hash' => __("Créer une clé à partir d'une chaîne de caractères"),
            ],
        ]
    );
    echo $this->Html->tag(
        'div.hiddenFields',
        $this->Form->control(
            'security_salt_method_apply',
            [
                'label' => __("Saisir la clé de sécurité"),
                'placeholder' => hash('sha256', 'libriciel'),
            ]
        ),
        ['style' => 'display: none']
    );
    echo $this->Form->control(
        'security_salt_method_generate',
        [
            'default' => hash('sha256', uniqid('libriciel')),
            'type' => 'hidden'
        ]
    );
    echo $this->Html->tag(
        'div.hiddenFields',
        $this->Form->control(
            'security_salt_method_hash',
            [
                'label' => __("Saisir une chaîne de caractères"),
            ]
        ),
        ['style' => 'display: none']
    );
    ?><script>
    var element = $('#security-salt-method').change(function() {
        $('.hiddenFields').hide();
        switch ($(this).val()) {
            case 'apply':
                return $('#security-salt-method-apply').closest('div.hiddenFields').show();
            case 'hash':
                return $('#security-salt-method-hash').closest('div.hiddenFields').show();
        }
    });
    element.chosen({width: '', allow_single_deselect: true});
</script><?php
} elseif (empty($config['Email']) || empty($config['EmailTransport'])) {
    /**=======================================================================
     * E-mail
     * =======================================================================*/
    echo $this->Html->tag('header');
    echo $this->Html->tag('h2.h4', __("E-mails"));
    echo $this->Html->tag('/header');

    if (empty($config['Email'])) {
        echo $this->Form->control(
            'Config__Email__default__from',
            [
                'label' => __("Saisir l'adresse e-mail qui apparaîtra dans le champ from"),
                'placeholder' => 'no-reply@versae.fr',
            ]
        ) . '<hr>';
    }
    if (empty($config['EmailTransport'])) {
        echo $this->Form->control(
            'Config__EmailTransport__default__className',
            [
                'label' => __("Méthode d'envoi"),
                'data-placeholder' => __("-- Choisir une méthode --"),
                'empty' => true,
                'options' => [
                    [
                        'text' => 'Mail',
                        'value' => 'Mail',
                        'title' => __("Send using PHP mail function"),
                    ],
                    [
                        'text' => 'Smtp',
                        'value' => 'Smtp',
                        'title' => __("Send using SMTP"),
                    ],
                ]
            ]
        );
        echo $this->Form->control(
            'Config__EmailTransport__default__host',
            [
                'label' => 'host',
                'default' => 'localhost',
            ]
        );
        echo $this->Form->control(
            'Config__EmailTransport__default__port',
            [
                'label' => 'port',
                'default' => '25',
            ]
        );
        echo $this->Form->control(
            'Config__EmailTransport__default__username',
            [
                'label' => 'username',
            ]
        );
        echo '<label class="hide">autocomplete obliterator'
            . '<input type="password" value="false"><!-- neutralise le préremplissage firefox --></label>';
        echo $this->Form->control(
            'Config__EmailTransport__default__password',
            [
                'label' => 'password',
                'type' => 'password'
            ]
        );
        echo $this->Html->tag(
            'div.btn-group',
            $this->Html->tag(
                'button.btn.btn-warning',
                $this->Fa->i('fa-envelope-o', __("Envoyer un mail de test")),
                ['onclick' => 'sendTestMail()', 'type' => 'button']
            )
        );
        ?>
<script>
    $("#config-emailtransport-default-classname").chosen({width: '', allow_single_deselect: true});

    function sendTestMail()
    {
        var form = $('#install-form');
        if (form.get(0).checkValidity()) {
            var email = prompt(__("Veuillez entrer le mail de destination"));
            if (email) {
                var input = $('<input type="email" name="email">').val(email);

                if (!input.is(':valid')) {
                    alert(__("Email invalide"));
                    return;
                }
                $.ajax({
                    url: '<?=$this->Url->build('/install/sendTestMail')?>',
                    method: 'POST',
                    data: form.clone().append(input).serialize(),
                    headers: {
                        Accept: 'application/json'
                    },
                    success: function (content) {
                        if (content.success) {
                            alert(__("L'email de test a bien été envoyé"));
                        } else {
                            return this.error();
                        }
                    },
                    error: function () {
                        alert(PHP.messages.genericError);
                    }
                })
            }
        } else {
            form.get(0).reportValidity();
        }
    }
</script>
        <?php
    }
} elseif (!$databaseIsInitialized()) {
    /**=======================================================================
     * Migrations
     * =======================================================================*/
    echo $this->Html->tag('header');
    echo $this->Html->tag('h2.h4', __("Initialisation du schema de base de données"));
    echo $this->Html->tag('/header');

    echo $this->Html->tag('div.btn-group');
    echo $this->Html->tag(
        'button',
        __("Initialiser la base de données"),
        [
            'type' => 'button',
            'onclick' => 'initializeDatabase()',
            'class' => 'btn btn-default',
            'id' => 'btn-initialize-database',
        ]
    );
    echo $this->Html->tag('/div');
    ?>
    <script>
        function initializeDatabase() {
            $('button').disable();
            $('[name=_method]').val('PATCH');
            $('#install-form').append('<input name="initializeDatabase" value="1" type="hidden">').submit();
        }
    </script>
    <?php
} elseif (!$admins) {
    /**=======================================================================
     * Administrateur technique
     * =======================================================================*/
    echo $this->Html->tag('header');
    echo $this->Html->tag('h2.h4', __("Création d'un administrateur technique"));
    echo $this->Html->tag('/header');

    echo $this->Form->control(
        'Admins__username',
        [
            'label' => __("Identifiant de connexion"),
        ]
    );
    echo $this->Form->control(
        'Admins__email',
        [
            'label' => __("E-mail"),
            'type' => 'email',
        ]
    );
    echo '<label class="hide">autocomplete obliterator'
        . '<input type="password" value="false"><!-- neutralise le préremplissage firefox --></label>';
    echo $this->Form->control(
        'Admins__password',
        [
            'label' => __("Mot de passe"),
            'type' => 'password',
        ]
    );
    echo $this->Form->control(
        'Admins__confirm-password',
        [
            'label' => __("Confirmez le mot de passe"),
            'type' => 'password',
        ]
    );
    ?>
<script>
    var minScore = <?=Configure::read('Password.admin.complexity', PASSWORD_WEAK)?>;
    $('#admins-password').change(function() {
        var value = AsalaeGlobal.passwordScore($(this).val());
        if (value < minScore) {
            alert(
                __(
                    "La force de votre mot de passe ({0}) est inférieure à la force minimum configurée ({1})."
                    +"\nL'ANSSI recommande un mot de passe de plus de 12 caractères "
                    +"(chiffres, lettres, majuscules et spéciaux)",
                    Math.floor(value),
                    minScore
                )
            );
        }
    });
</script>
    <?php
} else {
    /**=======================================================================
     * Service d'exploitation
     * =======================================================================*/
    echo $this->Html->tag('header');
    echo $this->Html->tag('h2.h4', __("Service d'exploitation"));
    echo $this->Html->tag('/header');

    echo $this->Form->control(
        'ServiceExploitation__name',
        [
            'label' => __("Nom"),
        ]
    );
    echo $this->Form->control(
        'ServiceExploitation__identifier',
        [
            'label' => __("identifiant"),
        ]
    );
}
echo $this->Html->tag('/section');
echo $this->Html->tag('br');

echo $this->Form->button(
    $this->Fa->charte('Actualiser', __("Actualiser")),
    [
        'bootstrap-type' => 'default',
        'class' => 'refresh',
        'type' => 'button',
        'onclick' => "window.location.href=window.location.href.replace(/#.*$/, '')"
    ]
);
echo $this->Form->button(
    $this->Fa->charte('Enregistrer', __("Enregistrer")),
    ['bootstrap-type' => 'primary', 'class' => 'accept']
);

echo $this->Form->end();
echo $this->Html->tag('/div');
