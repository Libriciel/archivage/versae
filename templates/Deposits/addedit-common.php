<?php

/**
 * @var Versae\View\AppView $this
 * @var Versae\Form\FormMakerForm $form
 */

use Cake\Core\Configure;
use Cake\Error\Debugger;
use Cake\Utility\Hash;

$allowedTags = ['p', 'a', 'img', 'div', 'b', 'ol', 'ul', 'li', 'span', 'strong', 'small'];
$guide = $formEntity->get('user_guide')
    ? strip_tags($formEntity->get('user_guide'), $allowedTags)
    : __("Le guide utilisateur est vide");

echo $this->Modal->create(['id' => $guideModalId])
    . $this->Modal->header(__("Guide utilisateur"))
    . $this->Modal->body($guide, ['style' => 'max-width: 100%;'])
    . $this->Modal->end()
    . $this->ModalForm->modalScript('info-file');

if ($sedaGeneratorIsDown) {
    echo $this->Html->tag(
        'p.alert.alert-danger',
        __("Generateur SEDA KO, veuillez contacter un administrateur")
    );
}

echo $this->Form->create($entity, ['id' => $formId, 'idPrefix' => $idPrefix]);

if ($form->isValidForm() === false) {
    $d = new Debugger();

    if ($form->getErrors()) {
        $output = '<ul>';
        foreach ($form->getErrors() as $field => $errors) {
            $output .= '<li>';
            $output .= $field . PHP_EOL;
            $output .= '<ul>';
            foreach (Hash::flatten($errors) as $key => $error) {
                $msg = $error;
                if (str_contains($key, '.')) {
                    $msg .= ' (' . substr($key, 0, strrpos($key, '.')) . ')';
                }
                $output .= '<li>' . $msg . '</li>' . PHP_EOL;
            }
            $output .= '</ul></li>' . PHP_EOL;
        }
        $output .= '</ul>';
    } else {
        $output = __(
            "Il est possible que certains champs de formulaire non marqués"
            . " comme obligatoires, soient nécessaires pour la génération du bordereau"
        );
    }

    echo $this->Html->tag(
        'div.alert.alert-danger',
        __("Le formulaire n'est pas valide")
        . '<br>'
        . $output
    );
}

echo $this->Form->control(
    'bypass_validation',
    [
        'type' => 'hidden',
    ]
);

echo $this->Html->tag(
    'div.form-group-wrapper',
    $this->Html->tag('span', h($entity->get('name')), ['id' => $idPrefix . '-display-deposit-name']),
);
echo $this->Flash->render();

echo $this->Html->tag('div.btn-group mt-3');
echo $this->Html->tag(
    'button',
    $this->Fa->charte('Modifier', __("Modifier Nom et Service Versant")),
    [
        'id' => $idPrefix . '-edit-name-and-transferring',
        'type' => 'button',
        'class' => 'btn btn-primary',
    ]
);
if ($formEntity->get('user_guide')) {
    echo $this->Html->tag(
        'button',
        $this->Fa->i('fa-book', __("Afficher le guide utilisateur")),
        [
            'type' => 'button',
            'class' => 'btn btn-default',
            'data-toggle' => 'modal',
            'data-target' => '#' . $guideModalId,
        ]
    );
}
echo $this->Html->tag('/div');

echo $this->Html->tag(
    'div',
    $this->Html->tag('br')
    . $this->Form->control(
        'name',
        [
            'label' => __("Nom du versement"),
        ]
    )
    . $this->Form->control(
        'transferring_agency_id',
        [
            'label' => __("Service versant"),
            'required' => true,
            'options' => $transferringAgencies,
            'empty' => __("-- Sélectionner un service versant --"),
            'data-placeholder' => __("-- Sélectionner parmis les Services versants disponibles --"),
        ]
    )
    . $this->Form->control(
        'originating_agency_id',
        [
            'label' => __("Service producteur"),
            'required' => true,
            'options' => $originatingAgencies[$entity->get('transferring_agency_id')],
        ]
    ),
    ['id' => $idPrefix . '-name-and-transfering-display', 'style' => 'display: none']
);

echo '<hr class="mt-5 mb-5">';

require dirname(__DIR__) . '/Forms/generate-form.php';

echo $this->Form->end();

echo $this->Html->tag("span#$idPrefix-$ws_channel", '');
?>
<script>
    var testFormForm = $('#<?=$formId?>');
    var testModal = testFormForm.closest('.modal');
    var disableExps = <?=json_encode($disableExps)?>;
    var timepickers = <?=json_encode($datepickers)?>;
    var inputValues = <?=json_encode($inputData)?>;
    var fileuploadList = <?=json_encode($fileuploadList)?>;
    var originatingAgencies = <?=json_encode($originatingAgencies)?>;
    var selectTransferringAgencies = $('#<?=$idPrefix?>-transferring-agency-id');
    var initialOptsTransferringAgencies = selectTransferringAgencies.html();
    var selectOriginatingAgencies = $('#<?=$idPrefix?>-originating-agency-id');
    var initialOriginatingAgency = selectOriginatingAgencies.val();

    var modal = testFormForm.closest('.modal');
    var footer = modal.find('.modal-footer');
    var accept = footer.find('button.accept').first();

    $('#<?=$idPrefix?>-save-and-send').remove();
    var saveAndSend = accept.clone();
    saveAndSend.find('i').attr('class', 'fa fa-paper-plane')
    saveAndSend.contents()
        .filter(function () {
            return this.nodeType === 3; //Node.TEXT_NODE
        })
        .first()
        .get(0)
        .nodeValue = ' ' + __("Enregistrer et envoyer");

    /**
     * Action d'enregistrer avec possibilité de bypass la validation
     * @param event
     */
    var saveWithBypassValidation = function (event) {
        $(this).disable();
        AsalaeLoading.start();

        var form = modal.find('form:visible').first();
        form.find('input[name=send]').remove();

        if (form.get(0).checkValidity()) {
            form.trigger('beforeSubmit.modalForm', event);

            setTimeout(
                function() {
                    form.trigger('submit')
                        .trigger('afterSubmit.modalForm', event);
                },
                0
            );
        } else {
            if (confirm(__("Le versement contient des erreurs, sauvegarder le brouillon ?"))) {
                var submit = form.find('button[type=submit]').disable();
                $('#<?= $idPrefix ?>-bypass-validation').val(true);
                form.trigger('beforeSubmit.modalForm', event);
                $.ajax({
                    url: form.attr('action'),
                    method: 'POST',
                    data: form.serialize(),
                    headers: {"No-Layout": true},
                    success: function (content, textStatus, jqXHR) {
                        setTimeout(function() {
                            testModal
                                .trigger('ajax.success', jqXHR)
                                .find('div.modal-body')
                                .removeClass('alert')
                                .removeClass('alert-danger')
                            if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                                var btns = [];
                                testModal.find('button:disabled').each(function () {
                                    $(this).data('do-not-enable-after-submit', true);
                                    btns.push(this);
                                });
                                testModal.one('hidden.bs.modal', function () {
                                    for (var i = 0; i < btns.length; i++) {
                                        $(btns[i]).enable();
                                    }
                                }).modal("hide");
                            } else {
                                if (typeof content.errors === 'object') {
                                    var message = "Une ou plusieurs erreurs ont été détectés\n\n";
                                    for (var field in content.errors) {
                                        message += '\t' + field + ':\n'
                                        for (var name in content.errors[field]) {
                                            message += '\t\t• ' + content.errors[field][name] + '\n';
                                        }
                                    }
                                    alert(message);
                                } else {
                                    this.error(jqXHR);
                                }
                            }
                            form.trigger('afterSubmit.modalForm', event);
                            afterEditDeposit(content, textStatus, jqXHR);
                            submit.enable()
                        }, 400);
                    },
                    error: function () {
                        alert(PHP.messages.genericError);
                    },
                    complete: function () {
                        submit.enable();
                    }
                });
            } else {
                $('#<?= $idPrefix ?>-bypass-validation').val(0);
                form.get(0).reportValidity();
            }
        }
        AsalaeLoading.stop();
        $(this).enable();
    }

    saveAndSend.show();
    saveAndSend.attr('id', '<?=$idPrefix?>-save-and-send');
    saveAndSend.on(
        'click',
        function (event) {
            $(this).disable();
            AsalaeLoading.start();
            var form = modal.find('form:visible').first();
            if (form.get(0).checkValidity()) {
                form.trigger('beforeSubmit.modalForm', event);
                form.find('input[name=send]').remove();
                form.append('<input type="hidden" name="send" value="1">');
                setTimeout(
                    function() {
                        form.trigger('submit')
                            .trigger('afterSubmit.modalForm', event);
                    },
                    0
                );
            } else {
                form.get(0).reportValidity();
            }
            AsalaeLoading.stop();
            saveAndSend.enable();
        }
    );
    saveAndSend.insertAfter(accept);

    // bouton accept qui permet de bypasser la validation html
    $('#<?=$idPrefix?>-new-accept').remove();
    var newAccept = accept.clone();
    newAccept.show();
    newAccept.attr('id', '<?=$idPrefix?>-new-accept');
    newAccept.on(
        'click',
        saveWithBypassValidation
    );
    newAccept.insertAfter(accept);
    accept.hide();

    $('#<?=$idPrefix?>-edit-name-and-transferring').on(
        'click',
        function () {
            $('#<?=$idPrefix?>-name-and-transfering-display').toggle();
        }
    );

    $('#edit-deposit-name').on('keyup change', function () {
        $('#<?=$idPrefix?>-display-deposit-name').html($('#edit-deposit-name').val());
    });

    setTimeout(
        () => testFormForm.on('afterSubmit.modalForm', function() {
            if ($(this).get(0).checkValidity()) {
                $(this).hide();
            }
        }),
        0
    );

    try {
        var wsChannelDeposits = '<?=isset($idPrefix) ? "$idPrefix-$ws_channel" : ''?>';
        AsalaeWebsocket.connect(
            '<?=Configure::read('Ratchet.connect')?>',
            function (session) {
                session.subscribe(
                    wsChannelDeposits,
                    function (topic, data) {
                        $('#'+wsChannelDeposits).text(data.message);
                    }
                );
                $('.modal:visible').one(
                    'hidden.bs.modal',
                    () => session.unsubscribe(wsChannelDeposits)
                );
            },
            function () {
            },
            {'skipSubprotocolCheck': true}
        );
    } catch (e) {
        console.error(e);
    }

    // restaure les sections avec leurs valeurs
    // les valeurs se présentes ainsi: section-1.0.inputname.value(s)
    for (let key in inputValues) {
        if (!/^section-\d+$/.test(key)) {
            continue;
        }
        let fieldset = testModal.find('.repeatable-fieldset[data-name="%s"]'.format(key));
        for (let i in inputValues[key]) {
            let fieldsetItem = fieldset.find('> fieldset[data-index=%d]'.format(i));
            // si la section répétable n'existe pas, on click sur ajouter une section
            if (fieldsetItem.length === 0) {
                fieldset.find('button.add-fieldset').click();
                fieldsetItem = fieldset.find('> fieldset[data-index=%d]'.format(i));
            }
            for (let name in inputValues[key][i]) {
                let inputName = 'inputs[%s][%d][%s]'.format(key, i, name);

                let input = fieldset.find('[data-type][name="%s[]"]'.format(inputName));
                if (input.length === 0) {
                    input = fieldset
                        .find('[data-type][name="%s"], [data-input-name="%s"]'.format(inputName, inputName));
                }
                // champ simple
                if (input.length === 1) {
                    let value = inputValues[key][i][name];
                    if (!Array.isArray(value)) {
                        value = [value];
                    }
                    for (let j in value) {
                        setInputValue(input, value[j]);
                    }
                    continue;
                } else if (input.length > 1 && input.attr('data-type') === 'checkbox') {
                    input = input.filter('[type="checkbox"]');
                    input.prop('checked', true);
                    continue;
                } else if (input.length > 1 && input.attr('data-type') === 'multi_checkbox') {
                    // champ avec multiples valeurs
                    for (let j = 0; j < inputValues[key][i][name].length; j++) {
                        input.filter('[value="%s"]'.format(inputValues[key][i][name][j]))
                            .prop('checked', true);
                    }
                    continue;
                }
                // champ texte multiple avec 1 seule valeur
                if (!Array.isArray(inputValues[key][i][name])) {
                    inputName = 'inputs[%s][%d][%s][0]'.format(key, i, name);
                    input = fieldset.find('[name="%s"], [data-input-name="%s"]'.format(inputName, inputName));
                    setInputValue(input, inputValues[key][i][name]);
                    continue;
                }
                // champ texte multiple avec plusieurs valeurs
                let addBtn;
                for (let j = 0; j < inputValues[key][i][name].length; j++) {
                    inputName = 'inputs[%s][%d][%s][%d]'.format(key, i, name, j);
                    input = fieldset.find('[name="%s"], [data-input-name="%s"]'.format(inputName, inputName));
                    setInputValue(input, inputValues[key][i][name][j]);
                    if (typeof inputValues[key][i][name][j + 1] !== 'undefined') {
                        addBtn = input.parent().find('.input-group-addon .fa-plus').parent();
                        addBtn.click();
                    }
                }
            }
        }
    }

    function setInputValue(input, value)
    {
        if (input.hasClass('fake-input')) {
            AsalaeUploader.addUploadDataToTable(input, value, fileuploadList[value]);
        } else if (input.attr('type') === 'checkbox') {
            input.prop('checked', input.attr('value') === value);
        } else {
            input.val(value).change();
        }
    }

    selectTransferringAgencies.on(
        'change',
        function () {
            var transferringAgencyId = selectTransferringAgencies.val();
            selectOriginatingAgencies.val('').empty();
            if (!transferringAgencyId) {
                return;
            }
            for (var id in originatingAgencies[transferringAgencyId]) {
                if (!id) {
                    continue;
                }
                selectOriginatingAgencies.append(
                    $('<option>').attr('value', id)
                        .text(originatingAgencies[transferringAgencyId][id])
                );
            }
            if (initialOriginatingAgency) {
                selectOriginatingAgencies.val(initialOriginatingAgency);
                initialOriginatingAgency = null;
            } else {
                selectOriginatingAgencies.val(transferringAgencyId);
            }
        }
    ).change();
</script>
