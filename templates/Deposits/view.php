<?php

/**
 * @var Versae\View\AppView $this
 * @var Cake\Datasource\EntityInterface $entity
 */


use Cake\Datasource\EntityInterface;
use Versae\Model\Table\DepositsTable;

$view = $this;
$tabs = $this->Tabs->create('view-deposit', ['class' => 'row no-padding']);

$stateFormatter = function (EntityInterface $entity): string {
    $date = $entity->get('last_state_update') ?? $entity->get('created');
    return sprintf(
        '%s, depuis le %s',
        $entity->get('statetrad'),
        $date->format('d/m/Y H:i')
    );
};

/**
 * Informations principales
 */
$tables = [
    __("Informations principales") => [
        [
            __("Nom") => 'name',
            __("Formulaire") => '{form.name}, version {form.version_number}',
            __("Service Versant") => '{transferring_agency.name}, {transferring_agency.identifier}',
            __("SAE destinataire")
            => '<a href="{form.archiving_system.url}" target="_blank">{form.archiving_system.name}</a>',
            __("Etat") => $stateFormatter,
            __("Date de création") => 'created',
            __("Créateur") => 'created_user.username',
        ],
    ],
];

$states = array_map(
    function ($state) use ($stateOptions) {
        return [
            'date' => $state->date,
            'state' => $stateOptions[$state->state] ?? ''
        ];
    },
    json_decode($entity->get('states_history')) ?? []
);

$stateHistory = $this->Html->tag('h4', __("Historique des états"))
    . $this->Table->create('states_history', ['class' => 'table table-striped table-hover smart-td-size'])
        ->fields(
            [
                'date' => [
                    'label' => __("Date"),
                    'type' => 'datetime',
                ],
                'state' => [
                    'label' => __("État"),
                ],
            ]
        )
        ->data($states)
        ->params(['identifier' => 'date']);

$tabs->add(
    'tab-view-deposit-main',
    $this->Fa->i('fa-file-code-o', __("Versement")),
    $this->Html->tag('h3.h2', h($entity->get('name')))
    . $this->ViewTable->multiple($entity, $tables)
    . '<hr>'
    . $stateHistory
);

/**
 * Transfert
 */
if ($entity->get('transfer')) {
    $transferTables = [
        __("Informations principales") => [
            [
                __("Date") => 'created',
                __("Commentaire") => 'comment',
                __("Identifiant") => 'identifier',
                __("Message d'erreur") => 'error_message',
                __("Nombre de pièces jointes") => function (EntityInterface $transfer) use ($view) {
                    return $transfer->get('deposit')->get('state') === DepositsTable::S_READY_TO_PREPARE
                        ? $view->Html->tag('span.disabled.gray', $transfer->get('statetrad'))
                        : $transfer->get('data_count');
                },
                __("Taille des pièces jointes") => function (EntityInterface $transfer) use ($view) {
                    return $transfer->get('deposit')->get('state') === DepositsTable::S_READY_TO_PREPARE
                        ? ''
                        : $view->Number->toReadableSize($transfer->get('data_size'));
                },
                __("Fichiers supprimés") => function (EntityInterface $transfer) use ($view) {
                    return $transfer->get('deposit')->get('state') === DepositsTable::S_READY_TO_PREPARE
                        ? ''
                        : $transfer->get('files_deleted');
                },
                __("Bordereau de transfert") => function (EntityInterface $transfer) use ($view) {
                    return $transfer->get('deposit')->get('state') === DepositsTable::S_READY_TO_PREPARE
                        ? ''
                        : $view->Html->link(
                            $f = $transfer->get('identifier') . '.xml',
                            $this->Url->build('/deposits/get-xml/' . $transfer->get('deposit_id')),
                            ['download' => $f]
                        );
                },
            ],
        ],
        __("Dernière communication avec le SAE") => [
            [
                __("Date") => 'last_comm_date',
                __("Code réponse") => 'last_comm_code',
                __("Message") => function (EntityInterface $transfer) use ($view) {
                    return '{do_not_parse}' . $view->Html->tag(
                        'pre',
                        h($transfer->get('last_comm_message')),
                        ['style' => 'max-width: 980px']
                    );
                }
            ],
        ],
    ];

    $transfer = $entity->get('transfer');
    $transfer->set('deposit', $entity);
    $tabs->add(
        'tab-view-deposit-transfer',
        $this->Fa->i('fa-exchange', __("Transfert")),
        $this->Html->tag('h3', h($transfer->get('identifier')))
        . $this->ViewTable->multiple($transfer, $transferTables)
    );
}

echo $tabs;
