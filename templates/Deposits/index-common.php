<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Core\Configure;

echo $this->element('modal', ['idTable' => $tableId]);

if (empty($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}

echo $this->ModalView
    ->create('view-deposit-modal', ['size' => 'modal-xxl'])
    ->modal(__("Visualisation du versement"))
    ->output(
        'function',
        'viewDeposit',
        '/deposits/view'
    )
    ->generate();

echo $this->ModalForm
    ->create(
        'summarize-deposit-modal',
        [
            'size' => 'modal-xxl',
            'acceptButton' => $this->Form->button(
                $this->Fa->charte('Envoyer', __("Envoyer")),
                ['bootstrap-type' => 'primary', 'class' => 'accept']
            )

        ]
    )
    ->modal(__("Récapitulatif du versement"))
    ->output(
        'function',
        'summarizeDeposit',
        '/deposits/summarize'
    )
    ->generate();

echo $this->ModalForm
    ->create('add-deposit-modal', ['size' => 'large'])
    ->modal(__("Créer un versement"))
    ->step('/deposits/add1', 'addDepositStep1', 'AsalaeModal.stepCallback')
    ->step('/deposits/add2', 'addDepositStep2', 'afterAddDepositStep2')
    ->step('/deposits/generating', 'addGeneratingStep', 'afterAddDeposit')
    ->output('function')
    ->generate();

echo $this->ModalForm
    ->create('edit-deposit-modal', ['size' => 'large'])
    ->modal(__("Modification du versement"))
    ->step('/deposits/edit', 'actionEditDeposit', 'afterEditDeposit')
    ->step(
        '/deposits/generating',
        'editGeneratingStep',
        'TableGenericAction.afterEdit(' . $jsTable . ', "Deposits")'
    )
    ->output('function')
    ->generate();

$filters = $this->Filter->create('deposits-filter')
    ->saves($savedFilters)
    ->filter(
        'name',
        [
            'label' => __("Nom"),
            'wildcard',
        ]
    )
    ->filter(
        'created',
        [
            'label' => __("Date de création"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'transfer_created',
        [
            'label' => __("Date d'envoi"),
            'placeholder' => __("jj/mm/aaaa"),
            'type' => 'date',
        ]
    )
    ->filter(
        'state',
        [
            'label' => __("Etats"),
            'options' => $states,
            'multiple' => true
        ]
    )
    ->filter(
        'form',
        [
            'label' => __("Formulaire"),
            'options' => $formsOptions,
            'empty' => __("-- Sélectionner un Formulaire --"),
        ]
    )
    ->filter(
        'archiving_system_id',
        [
            'label' => __("SAE"),
            'options' => $archivingSystemsOptions,
            'empty' => __("-- Sélectionner un SAE --"),
        ]
    )
    ->filter(
        'created_user_id',
        [
            'label' => __("Auteur"),
            'options' => $createdUsers,
            'empty' => __("-- Sélectionner un Utilisateur --"),
        ]
    )
    ->filter(
        'favoris',
        [
            'label' => [
                'text' => __("Favoris seulement"),
                'class' => 'as-star-o',
            ],
            'class' => 'with-icon',
            'type' => 'checkbox',
            'onclick' => 'return false',
            'style' => 'cursor: not-allowed',
            'hiddenField' => false,
            'checked',
        ]
    );

if ($tableId === 'forms-index-all-table') {
    $filters->filter(
        'last_version',
        [
            'label' => __("Uniquement la dernière version"),
            'type' => 'checkbox',
            'checked',
        ]
    );
}

$popupNewVersion = $this->Html->tag('h3', __("Une version plus récente est disponible"))
    . $this->Html->tag('div.btn-group-vertical')
    . $this->Html->tag(
        'button',
        $this->Fa->i('fa-code-branch', __("Éditer avec la nouvelle version")),
        [
            'id' => 'popup-edit-new-version',
            'class' => 'btn btn-primary btn-validity',
            'type' => 'button',
        ]
    )
    . $this->Html->tag(
        'button',
        $this->Fa->i('fa-refresh', __("Éditer en conservant la version")),
        [
            'id' => 'popup-edit-keep-version',
            'class' => 'btn btn-default btn-validity',
            'type' => 'button',
        ]
    )
    . $this->Html->tag(
        'button',
        $this->Fa->charte('Annuler', __("Annuler")),
        [
            'id' => 'popup-edit-cancel',
            'class' => 'btn btn-default btn-validity',
            'type' => 'button',
        ]
    )
    . $this->Html->tag('/div');

$popupReeditContent = $this->Html->tag('h3', __("Une version plus récente est disponible"))
    . $this->Html->tag('div.btn-group-vertical')
    . $this->Html->tag(
        'button',
        $this->Fa->i('fa-code-branch', __("Rééditer avec la nouvelle version")),
        [
            'id' => 'popup-reedit-new-version',
            'class' => 'btn btn-primary btn-validity',
            'type' => 'button',
        ]
    )
    . $this->Html->tag(
        'button',
        $this->Fa->i('fa-refresh', __("Rééditer en conservant la version")),
        [
            'id' => 'popup-reedit-keep-version',
            'class' => 'btn btn-default btn-validity',
            'type' => 'button',
        ]
    )
    . $this->Html->tag(
        'button',
        $this->Fa->charte('Annuler', __("Annuler")),
        [
            'id' => 'popup-reedit-cancel',
            'class' => 'btn btn-default btn-validity',
            'type' => 'button',
        ]
    )
    . $this->Html->tag('/div');

echo $filters->generateSection();

require 'ajax_index.php';
?>
<script>
    var currentTable;
    currentTable = <?=$jsTable?>;

    $(function() {
        AsalaeGlobal.paginationAjax('#<?=$tableId?>-section');
    });

    /**
     * Callback de l'action reedit
     */
    function actionReeditDeposit(id) {
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/deposits/reedit')?>/' + id,
            method: 'POST',
            headers: {
                Accept: 'application/json'
            },
            success: function (content, textStatus, jqXHR) {
                if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                    AsalaeGlobal.interceptedLinkToAjax(
                        '<?=$this->Url->build('/deposits/index-in-progress')?>',
                        true,
                        () => actionEditDeposit(id)
                    );
                } else if (content?.error) {
                    alert(content?.error);
                } else {
                    $('#edit-deposit-modal').modal('show').find('div.modal-body').empty().append(content);
                }
            }
        });
    }

    function cancelDeposit(id) {
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/deposits/cancel')?>/' + id,
            method: 'POST',
            headers: {
                Accept: 'application/json'
            },
            success: TableGenericAction.afterEdit(<?= $jsTable ?>, 'Deposits')
        });
    }

    /**
     * Callback de l'action add deposit
     */
    function afterAddDeposit(content, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            var table;
            table = <?=$jsTable?>;
            table.data.unshift({Deposits: content});
            TableGenerator.appendActions(table.data, table.actions);
            table.generateAll();
        }
    }

    function sendDeposit(id) {
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/deposits/send')?>/' + id,
            method: 'POST',
            headers: {
                Accept: 'application/json'
            },
            success: function (message) {
                if (message.report === 'done') {
                    var table = currentTable;
                    var tableData = table.getDataId(id);
                    if (tableData) {
                        table.replaceDataId(id, {Deposits: message});
                    } else {
                        table.data.push({Deposits: message});
                    }
                    table.generateAll();
                    var tr = $(table.table).find('tr[data-id="'+id+'"]');
                    tr.removeClass('alert')
                        .removeClass('alert-warning');
                } else {
                    throw message.report;
                }
            },
            error: function () {
                $(table.table).find('tr[data-id="'+id+'"]').addClass('danger');
            }
        });
    }

    $('#add-deposit-modal-1').on('hidden.bs.modal', function () {
        var url = location.href;
        var match = url.match(/([&?])(add=[^&]+)(&|$)/);
        if (match) {
            var newUrl;
            if (match[3] === "") {
                newUrl = url.replace(match[0], '');
            } else {
                newUrl = url.replace(match[2]+match[3], '');
            }
            window.history.pushState({}, '', newUrl);
        }
    });

    AsalaeWebsocket.connect(
        '<?=Configure::read('Ratchet.connect')?>',
        function(session) {
            session.subscribe('deposits', function(topic, data) {
                var table;
                table = currentTable;
                if (typeof data !== 'object' || typeof data.message !== 'object') {
                    return;
                }
                var tr = $(table.table).find('tr[data-id="'+data.message.deposit_id+'"]');
                if (tr.length) {
                    var tableData = table.getDataId(data.message.deposit_id);
                    setTimeout(function () {
                        tr
                            .css('background-color', '#5cb85c')
                            .animate({'background-color': 'transparent'});
                    }, 0);
                    tr.find('td.state').text(data.message.statetrad);
                    tableData.statetrad = data.message.statetrad;
                    tableData.Deposits.statetrad = data.message.statetrad;
                    tableData.state = data.message.state;
                    tableData.Deposits.state = data.message.state;
                    tr.find('td.transfer_sent').text(TableHelper.date('LLL')(data.message.transfer_created));
                    tableData.Deposits.transfer = {created: data.message.transfer_created};
                }
            });
        },
        function() {
        },
        {'skipSubprotocolCheck': true}
    );

    function checkNewFormVersion(id, callback)
    {
        var modal = $('#edit-deposit-modal');
        AsalaeLoading.ajax({
            url: '<?=$this->Url->build('/deposits/check-new-form-version')?>/' + id,
            headers: {
                Accept: 'application/json'
            },
            success: function(content, textStatus, jqXHR) {
                if (!jqXHR.getResponseHeader('X-Asalae-Success')) {
                    modal.modal('show').find('div.modal-body').empty().append(content);
                    return false;
                }
                if (content?.new_form) {
                    var popup = new AsalaePopup($('tr[data-id=' + id + '] td.action'));
                    popup.element()
                        .append('<?= addcslashes($popupNewVersion, "'") ?>')
                        .addClass('popup-new-form-version');
                    popup.show();
                    popup.element()
                        .css({
                            top: '',
                            bottom: 50,
                            left: -165,
                            'text-align': 'left'
                        });

                    $('#popup-edit-new-version').on('click', function (e) {
                        popup.hide();
                        AsalaeLoading.ajax({
                            url: '<?=$this->Url->build('/deposits/upgrade-form-version')?>/' + id,
                            headers: {
                                Accept: 'application/json'
                            },
                            success: function (content, textStatus, jqXHR) {
                                if (jqXHR.getResponseHeader('X-Asalae-Success')) {
                                    callback(id);
                                } else {
                                    modal.modal('show').find('div.modal-body').empty().append(content);
                                }
                            }
                        });
                    });

                    $('#popup-edit-keep-version').on('click', function (e) {
                        popup.hide();
                        callback(id);
                    });

                    $('#popup-edit-cancel').on('click', function (e) {
                        popup.hide();
                    });
                } else {
                    callback(id);
                }
            },
            error: function (error) {
                AsalaeModal.error(error, $('#edit-deposit-modal').modal('show'));
            }
        });
    }

    function editDeposit(id)
    {
        checkNewFormVersion(id, actionEditDeposit);
    }

    function reeditDeposit(id)
    {
        checkNewFormVersion(id, actionReeditDeposit);
    }

    function afterAddDepositStep2(content, textStatus, jqXHR)
    {
        AsalaeModal.stepCallback(content, textStatus, jqXHR);
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            var table;
            table = <?=$jsTable?>;
            table.data.unshift({Deposits: content});
            TableGenerator.appendActions(table.data, table.actions);
            table.generateAll();
        }
    }

    function afterEditDeposit(content, textStatus, jqXHR)
    {
        if (jqXHR.getResponseHeader('X-Asalae-Bypass-Validation') === 'true') {
            var table = <?=$jsTable?>;
            if (table.getDataId(content.id)) {
                table.getDataId(content.id).Deposits = content;
            } else {
                table.data.unshift({Deposits: content});
            }
            TableGenerator.appendActions(table.data, table.actions);
            table.generateAll();
        } else {
            AsalaeModal.stepCallback(content, textStatus, jqXHR);
        }
    }
</script>
