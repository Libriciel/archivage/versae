<?php

/**
 * @var Versae\View\AppView $this
 */

$guideModalId = 'edit-deposit-user-guide';
$idPrefix = 'edit-deposit';
$formId = 'form-edit-form';

require 'addedit-common.php';
