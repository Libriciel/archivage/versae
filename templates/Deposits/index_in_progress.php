<?php

/**
 * @var Versae\View\AppView $this
 */

$this->Breadcrumbs->add(__("Tableau de bord"), '/');
$this->Breadcrumbs->add(__("Versements"));
$this->Breadcrumbs->add($h1 = __("Mes versements en cours"));

// Titre de la page
echo $this->Html->tag(
    'div',
    $this->Html->tag('h1', $this->Fa->i('fa-code', $h1))
    . $this->Breadcrumbs->render(),
    ['class' => 'container']
);

$jsTable = $this->Table->getJsTableObject($tableId);

$buttons = [];
if ($this->Acl->check('/deposits/add1')) {
    $buttons[] = $this->Html->tag(
        'button',
        $this->Fa->charte('Créer', __("Créer un versement")),
        [
            'type' => 'button',
            'class' => 'btn btn-success float-none',
            'onclick' => 'addDepositStep1()'
        ]
    );
}
if ($buttons) {
    echo $this->Html->tag(
        'div.container.btn-separator',
        implode(PHP_EOL, $buttons)
    );
}

require 'index-common.php';
?>
<script>
    /* global addDepositStep1 */
    $(function() {
        setTimeout(function() {
            var url = location.href;
            var match = url.match(/[&?]add=([^&]+)(?:&|$)/);
            if (match && match[1] === 'deposit') {
                addDepositStep1();
            }
        }, 400);
    });
</script>
