<?php

/**
 * @var Versae\View\AppView $this
 */

use Versae\Model\Table\DepositsTable;

if (empty($jsTable)) {
    $jsTable = $this->Table->getJsTableObject($tableId);
}
$table = $this->Table
    ->create($tableId, ['class' => 'table table-striped table-hover smart-td-size'])
    ->fields(
        [
            'Deposits.name' => [
                'label' => __("Nom"),
                'order' => 'name',
                'filter' => [
                    'name[0]' => [
                        'id' => 'filter-name',
                        'label' => false,
                        'aria-label' => __("Nom"),
                    ],
                ],
            ],
            'Deposits.form.name' => [
                'label' => __("Formulaire"),
                'order' => 'form_name',
                'filter' => [
                    'form[0]' => [
                        'id' => 'filter-form-name',
                        'label' => false,
                        'aria-label' => __("Formulaire"),
                        'options' => $formsOptions,
                    ],
                ],
            ],
            'Deposits.statetrad' => [
                'label' => __("Etat"),
                'class' => 'state',
                'filter' => [
                    'state[0]' => [
                        'id' => 'filter-state',
                        'label' => false,
                        'aria-label' => __("Etat"),
                        'options' => $states
                    ],
                ],
            ],
            'Deposits.transfer.identifier' => [
                'label' => __("Identifiant"),
                'display' => false,
                'order' => 'identifier',
            ],
            'Deposits.transfer.created' => [
                'label' => __("Date d'envoi"),
                'class' => 'transfer_sent',
                'type' => 'datetime',
                'order' => 'transfer_created',
                'filter' => [
                    'transfer_created[0]' => [
                        'id' => 'filter-transfer-created-0',
                        'label' => __("Date d'envoi {0}", 1),
                        'prepend' => $this->Input->operator('dateoperator_transfer_created[0]', '>='),
                        'append' => $this->Date->picker('#filter-transfer-created-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'transfer_created[1]' => [
                        'id' => 'filter-transfer-created-1',
                        'label' => false,
                        'aria-label' => __("Date d'envoi {0}", 2),
                        'prepend' => $this->Input->operator('dateoperator_transfer_created[1]', '<='),
                        'append' => $this->Date->picker('#filter-transfer-created-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
            'Deposits.created' => [
                'label' => __("Date de création"),
                'type' => 'datetime',
                'order' => 'created',
                'filterCallback' => 'AsalaeFilter.datepickerCallback',
                'display' => false,
                'filter' => [
                    'created[0]' => [
                        'id' => 'filter-created-0',
                        'label' => __("Date de création"),
                        'prepend' => $this->Input->operator('dateoperator_created[0]', '>='),
                        'append' => $this->Date->picker('#filter-created-0'),
                        'class' => 'datepicker with-select',
                    ],
                    'created[1]' => [
                        'id' => 'filter-created-1',
                        'label' => false,
                        'aria-label' => __("Date 2"),
                        'prepend' => $this->Input->operator('dateoperator_created[1]', '<='),
                        'append' => $this->Date->picker('#filter-created-1'),
                        'class' => 'datepicker with-select',
                    ],
                ],
            ],
            'Deposits.form.archiving_system.name' => [
                'label' => __("SAE"),
                'display' => false,
                'filter' => [
                    'archiving_system_id[0]' => [
                        'id' => 'filter-archiving_system',
                        'label' => false,
                        'aria-label' => __("SAE"),
                        'options' => $archivingSystemsOptions
                    ],
                ],
            ],
            'Deposits.created_user.username' => [
                'label' => __("Auteur"),
                'display' => false,
                'order' => 'created_user_id',
                'filter' => [
                    'created_user_id[0]' => [
                        'id' => 'filter-username',
                        'label' => false,
                        'aria-label' => __("Auteur"),
                        'options' => $createdUsers
                    ],
                ],
            ],
        ]
    )
    ->data($data)
    ->params(
        [
            'identifier' => 'Deposits.id',
            'favorites' => true,
            'sortable' => true,
        ]
    )
    ->actions(
        [
            [
                'onclick' => "viewDeposit({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Visualiser'),
                'title' => $title = __("Visualiser {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/deposits/view'),
                'params' => ['Deposits.id', 'Deposits.name'],
            ],
            [
                'onclick' => "editDeposit({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier'),
                'title' => $title = __("Modifier {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/deposits/edit'),
                'displayEval' => 'data[{index}].Deposits.editable',
                'params' => ['Deposits.id', 'Deposits.name'],
            ],
            function ($table) {
                /** @var Versae\View\AppView $this */
                $deleteUrl = $this->Url->build('/Deposits/delete');
                return [
                    'onclick' => "TableGenericAction.deleteAction($table->tableObject, '$deleteUrl')({0})",
                    'type' => 'button',
                    'class' => 'btn-link',
                    'label' => $this->Fa->charte('Supprimer', '', 'text-danger'),
                    'title' => __("Supprimer {0}", '{1}'),
                    'aria-label' => __("Supprimer {0}", '{1}'),
                    'data-action' => __("Supprimer"),
                    'display' => $this->Acl->check('deposits/delete'),
                    'displayEval' => 'data[{index}].Deposits.deletable',
                    'params' => ['Deposits.id', 'Deposits.name']
                ];
            },
            [
                'onclick' => "summarizeDeposit({0})",
                'type' => 'button',
                'class' => 'btn-link send',
                'display' => $this->Acl->check('/deposits/summarize'),
                'displayEval' => 'data[{index}].Deposits.sendable '
                    . '&& data[{index}].Deposits.created_user_id === PHP.user_id',
                'label' => $this->Fa->charte('Envoyer', '', 'text-success'),
                'title' => $title = __("Envoyer {0}", '{1}'),
                'aria-label' => $title,
                'params' => ['Deposits.id', 'Deposits.name']
            ],
            [
                'onclick' => "summarizeDeposit({0})",
                'type' => 'button',
                'class' => 'btn-link send',
                'display' => $this->Acl->check('/deposits/summarize'),
                'displayEval' => 'data[{index}].Deposits.sendable '
                    . '&& data[{index}].Deposits.created_user_id === PHP.user_id'
                    . '&& data[{index}].Deposits.state === "' . DepositsTable::S_SENDING_ERROR . '"',
                'label' => $this->Fa->charte('Envoyer', '', 'text-warning'),
                'title' => $title = __("Renvoyer {0}", '{1}'),
                'aria-label' => $title,
                'params' => ['Deposits.id', 'Deposits.name']
            ],
            [
                'data-callback' => "reeditDeposit({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Modifier', '', 'text-success'),
                'title' => $title = __("Rééditer {0}", '{1}'),
                'aria-label' => $title,
                'display' => $this->Acl->check('/deposits/reedit'),
                'displayEval' => 'data[{index}].Deposits.reeditable'
                    . '&& data[{index}].Deposits.created_user_id === PHP.user_id',
                'confirm' => __("Souhaitez-vous rééditer votre versement ?"),
                'params' => ['Deposits.id', 'Deposits.name'],
            ],
            [
                'onclick' => "cancelDeposit({0})",
                'type' => 'button',
                'class' => 'btn-link',
                'label' => $this->Fa->charte('Annuler', 1),
                'title' => $title = __("Annuler {0}", '{1}'),
                'aria-label' => $title,
                'data-action' => __("Annuler"),
                'display' => $this->Acl->check('/deposits/cancel'),
                'displayEval' => 'data[{index}].Deposits.cancelable',
                'params' => ['Deposits.id', 'Deposits.name'],
            ],
        ],
    );

$jsTable = $table->tableObject;

echo $this->element(
    'section_table',
    [
        'id' => $tableId . '-section',
        'title' => __("Liste des Versements"),
        'table' => $table,
    ]
);
