<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\Datasource\EntityInterface;
use Versae\Model\Table\FormInputsTable;

/*
 * Formatage d'une valeur d'input pour l'affichage dans le tableau
 * @param mixed           $value
 * @param EntityInterface $input
 * @param int             $line
 * @return string
 */
$formatValue = function ($value, EntityInterface $input) use ($fileuploads): string {
    /** @var Versae\View\AppView $view */
    $view = $this;
    switch ($input->get('type')) {
        case FormInputsTable::TYPE_FILE:
        case FormInputsTable::TYPE_ARCHIVE_FILE:
            if ($input->get('multiple')) {
                $st = $view->Html->tag('ul', null, ['class' => 'p-0']);
                $value = is_array($value) ? $value : [$value];
                foreach ($value as $v) {
                    $file = array_filter(
                        $fileuploads[$input->get('name')],
                        fn($e) => $e->get('id') === (int)$v
                    );
                    $file = current($file);
                    $st .= $view->Html->tag(
                        'li',
                        $view->Html->link(
                            $file->get('filename'),
                            $view->Url->build('/download/file/' . $file->get('id')),
                            ['target' => '_blank', 'download' => $file->get('name')]
                        )
                    );
                }

                return $st;
            } else {
                $file = null;
                /** @var EntityInterface[] $namedFileuploads */
                $namedFileuploads = $fileuploads[$input->get('name')];
                foreach ($namedFileuploads as $fileupload) {
                    if ($fileupload->id === (int)$value) {
                        $file = $fileupload;
                        break;
                    }
                }
                if (!$file) {
                    throw new Exception('File not found');
                }

                return $view->Html->link(
                    $file->get('filename'),
                    $view->Url->build('/download/file/' . $file->get('id')),
                    ['target' => '_blank', 'download' => $file->get('name')]
                );
            }
        case FormInputsTable::TYPE_CHECKBOX:
            return $view->Html->tag(
                'input',
                '',
                ['type' => 'checkbox', 'checked' => (bool)$value, 'disabled' => true]
            );
        case FormInputsTable::TYPE_RADIO:
        case FormInputsTable::TYPE_SELECT:
            $options = json_decode($input->get('options'), true);
            if ($input->get('multiple')) {
                $str = $view->Html->tag('ul', null, ['class' => 'p-0']);
                foreach (is_array($value) ? $value : [$value] as $v) {
                    $text = current(
                        array_filter(
                            $options,
                            fn($e) => $e['value'] === $v
                        )
                    )['text'];
                    $str .= $view->Html->tag('li', $text);
                }
                $str .= $view->Html->tag('/ul');

                return $str;
            }
            return current(
                array_filter(
                    $options,
                    fn($e) => $e['value'] === $value
                )
            )['text'];
        case FormInputsTable::TYPE_MULTI_CHECKBOX:
            $str = $view->Html->tag('ul', null, ['class' => 'p-0']);
            $options = json_decode($input->get('options'), true);
            foreach (is_array($value) ? $value : [$value] as $v) {
                $text = current(
                    array_filter(
                        $options,
                        fn($e) => $e['value'] === $v
                    )
                )['text'];
                $str .= $view->Html->tag('li', $text);
            }
            $str .= $view->Html->tag('/ul');

            return $str;
        default:
            return $input->get('multiple')
                ? $view->Html->tag(
                    'ul',
                    $view->Html->tag('li', implode('</li><li>', (array)$value)),
                    ['class' => 'p-0']
                )
                : $value;
    }
};

echo $this->Html->tag('h2', h($entity->get('name')));

/** @var EntityInterface $fieldset */
foreach ($formEntity->get('form_fieldsets') ?: [] as $fieldset) {
    if ($fieldset->get('repeatable')) {
        $fieldSetData = $inputData['section-' . $fieldset->get('ord')] ?? [];
        $count = count($fieldSetData);
        foreach ($fieldSetData as $fieldsetIndex => $values) { // for each itération du répétable
            echo $this->Html->tag(
                'h3',
                sprintf('%s (%d/%d)', $fieldset->get('legend'), $fieldsetIndex + 1, $count)
            );

            echo $this->Html->tag(
                'table',
                null,
                ['class' => 'table table-striped table-hover no-padding']
            )
                . $this->Html->tag('tbody');

            foreach ($values as $field => $value) {
                $input = null;
                foreach ($fieldset->get('form_inputs') as $input) {
                    if ($input->get('name') === $field) {
                        echo $this->Html->tag('tr')
                            . $this->Html->tag('th', h($input->get('label')), ['style' => 'width: 50%'])
                            . $this->Html->tag('td', $formatValue($value, $input))
                            . $this->Html->tag('/tr');
                        break;
                    }
                }
            }
            echo $this->Html->tag('/tbody') . $this->Html->tag('/table');
        }
    } else {
        $display = '';
        foreach ($fieldset->get('form_inputs') as $input) {
            $fieldname = $input['name'] ?? 'paragraph';
            $value = $inputData[$fieldname] ?? null;
            if ($value !== null) {
                $display .= $this->Html->tag('tr')
                    . $this->Html->tag('th', h($input['label']), ['style' => 'width: 50%'])
                    . $this->Html->tag('td', $formatValue($value, $input))
                    . $this->Html->tag('/tr');
            }
        }

        if (!$display) {
            continue;
        }

        $legend = $fieldset->get('legend')
            ?: __("Section sans titre #{0}", $fieldset->get('ord') + 1);
        echo $this->Html->tag('h3', $legend);
        echo $this->Html->tag(
            'table',
            null,
            ['class' => 'table table-striped table-hover no-padding', 'style' => 'table-layout: fixed']
        )
            . $this->Html->tag('tbody');

        echo $display;
        echo $this->Html->tag('/tbody') . $this->Html->tag('/table');
    }
}

if (!$inputData) {
    echo $this->Html->tag(
        'p.alert.alert-info',
        __("Aucune donnée pour ce versement")
    );
}
?>

<script>
    var modal = $('#summarize-deposit-modal');
    var footer = modal.find('.modal-footer');
    var accept = footer.find('button.accept').first();
    accept
        .off('click')
        .on(
            'click',
            function (event) {
                event.preventDefault();
                sendDeposit(<?=$entity->get('id')?>);
                modal.modal("hide");
            }
        );
</script>