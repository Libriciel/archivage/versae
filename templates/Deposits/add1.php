<?php

/**
 * @var Versae\View\AppView $this
 */

use Cake\I18n\FrozenDate;

echo $this->MultiStepForm->template('MultiStepForm/add_deposit')->step(1) . '<hr>';

if ($sedaGeneratorIsDown) {
    echo $this->Html->tag(
        'p.alert.alert-danger',
        __("Generateur SEDA KO, veuillez contacter un administrateur")
    );
}

echo $this->Form->create(null, ['idPrefix' => 'add1-deposit']);
echo $this->Form->control(
    'form_id',
    [
        'label' => __("Formulaire"),
        'required' => true,
        'options' => $forms ?? [],
        'empty' => __("-- Sélectionner un formulaire --"),
        'data-placeholder' => __("-- Sélectionner parmis les formulaires disponibles --"),
    ]
);
echo $this->Form->control(
    'name',
    [
        'label' => __("Nom du versement"),
        'required' => true,
    ]
);
echo $this->Form->control(
    'transferring_agency_id',
    [
        'label' => __("Service versant"),
        'required' => true,
        'options' => [],
        'empty' => $empty = __("-- Sélectionner un service versant --"),
        'data-placeholder' => __("-- Sélectionner parmis les Services versants disponibles --"),
    ]
);
echo $this->Form->control(
    'originating_agency_id',
    [
        'label' => __("Service producteur"),
        'required' => true,
        'options' => [],
    ]
);
echo $this->Form->end();
?>
<script>
    var selectForm = $('#add1-deposit-form-id');
    var nameInput = $('#add1-deposit-name');
    var selectTransferringAgencies = $('#add1-deposit-transferring-agency-id');
    var initialOptsTransferringAgencies = selectTransferringAgencies.html();
    var selectOriginatingAgencies = $('#add1-deposit-originating-agency-id');
    var initialOriginatingAgency = selectOriginatingAgencies.val();
    var transferringsAgencies = <?=json_encode($transferringsAgencies)?>;
    var originatingAgencies = <?=json_encode($originatingAgencies)?>;
    var currentDate = '<?= (new FrozenDate())->i18nFormat([IntlDateFormatter::SHORT, IntlDateFormatter::NONE]) ?>';

    selectForm.on(
        'change',
        function(e) {
            nameInput.val(
                selectForm.val()
                    ? $('#add1-deposit-form-id option:selected').text() + ' - ' + currentDate
                    : ''
            );
            selectTransferringAgencies
                .empty()
                .append(initialOptsTransferringAgencies);
            let selectedForm = transferringsAgencies[selectForm.val()];
            for (const id in selectedForm) {
                if (!id) {
                    continue;
                }
                selectTransferringAgencies.append(
                    $('<option>').attr('value', id).text(selectedForm[id])
                );
            }
            if (selectedForm && Object.keys(selectedForm).length === 1) {
                selectTransferringAgencies.find('option').attr('selected', true);
                selectTransferringAgencies.change();
            }
        }
    );

    selectTransferringAgencies.on(
        'change',
        function () {
            var transferringAgencyId = selectTransferringAgencies.val();
            selectOriginatingAgencies.val('').empty();
            if (!transferringAgencyId) {
                return;
            }
            for (var id in originatingAgencies[transferringAgencyId]) {
                if (!id) {
                    continue;
                }
                selectOriginatingAgencies.append(
                    $('<option>').attr('value', id)
                        .text(originatingAgencies[transferringAgencyId][id])
                );
            }
            if (initialOriginatingAgency) {
                selectOriginatingAgencies.val(initialOriginatingAgency);
                initialOriginatingAgency = null;
            } else {
                selectOriginatingAgencies.val(transferringAgencyId);
            }
        }
    ).change();

    AsalaeGlobal.select2(selectForm);
</script>
