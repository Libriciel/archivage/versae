<?php

/**
 * @var Versae\View\AppView $this
 * @var Versae\Form\FormMakerForm $form
 */

//echo $this->MultiStepForm->template('MultiStepForm/add_deposit')->step(2) . '<hr>';

$guideModalId = 'add2-deposit-user-guide';
$idPrefix = 'add2-deposit';
$formId = 'form-test-add2';

require 'addedit-common.php';

?>
<script>
    // suppression du versement si on appuis sur annuler
    $('#add-deposit-modal-2').find('.modal-footer button.cancel')
        .off('.delete-add')
        .on('click.delete-add', function () {
            $.ajax({
                url: '<?=$this->Url->build('/deposits/delete/' . $entity->get('id'))?>',
                method: 'DELETE'
            });
        });
</script>

