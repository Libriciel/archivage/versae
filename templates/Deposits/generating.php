<?php

/**
 * @var Cake\Datasource\EntityInterface $deposit
 * @var Versae\View\AppView $this
 */

use Cake\Core\Configure;
use Versae\Model\Table\DepositsTable;

echo $this->Html->tag(
    'div.p',
    __(
        "Les fichiers du versement sont en cours de préparation sur le serveur."
            . " Vous pouvez fermer cette fenêtre ou patienter jusqu'à sa fermeture automatique."
    )
);

echo $this->Html->tag(
    'div',
    $this->Html->tag('i', '', ['class' => 'fa fa-4x fa-spinner faa-spin animated']),
    ['class' => 'text-center loading-container']
);
$deposit->set('state', DepositsTable::S_EDITING);
?>
<script>
    var modalGenerating = $('.modal:visible').last();
    modalGenerating.find('.modal-title')
        .text(__("Préparation du versement"));
    var footer = modalGenerating.find('.modal-footer');
    var accept = footer.find('button.accept').first();
    accept.remove();
    var cancel = footer.find('button.cancel').first();
    cancel.contents()
        .filter(function () {
        return this.nodeType === 3; //Node.TEXT_NODE
    })
        .first()
        .get(0)
        .nodeValue = ' ' + __("Fermer");

    var checkInterval = setInterval(checkGenerationByAjax, 5000);
    var currentDeposit = <?=json_encode($deposit->toArray())?>;
    AsalaeWebsocket.connect(
        '<?=Configure::read('Ratchet.connect')?>',
        function(session) {
            session.subscribe(
                'generate_deposit_success',
                function(topic, data) {
                    clearInterval(checkInterval);
                    checkInterval = null;
                    if (data.message.deposit_id === <?=$deposit->get('id')?>) {
                        $('.modal:visible div.p').text("done");
                        $('.modal:visible').modal('hide');
                        let tableDom = $('.main-content > section > table[data-table-uid]:visible');
                        let tableObject = TableGenerator.instance[tableDom.attr('data-table-uid')];
                        currentDeposit.sendable = data.message.sendable;
                        tableObject.replaceDataId(currentDeposit.id, {Deposits: currentDeposit});
                        tableObject.generateAll();
                        if (data.message.send) {
                            summarizeDeposit(data.message.deposit_id);
                        }
                    }
                }
            );
            session.subscribe(
                'generate_deposit_failed',
                function(topic, data) {
                    clearInterval(checkInterval);
                    checkInterval = null;
                    if (data.message.deposit_id === <?=$deposit->get('id')?>) {
                        $('.loading-container').remove();
                        let err = $('<div>').html(data.message.error).text();
                        $('.modal:visible div.p').html(
                            __(
                                "Une erreur a eu lieu avec le message suivant: {0}",
                                (new Filter).toHtml(err).replace("\n", '<br>')
                            )
                        );
                        let tableDom = $('.main-content > section > table[data-table-uid]:visible');
                        let tableObject = TableGenerator.instance[tableDom.attr('data-table-uid')];
                        currentDeposit.deletable = !!data.message.error;
                        tableObject.generateAll();
                    }
                }
            );
        }
    );
    modalGenerating.one(
        'hidden.bs.modal',
        () => AsalaeWebsocket.unsubscribe(['generate_deposit_success', 'generate_deposit_failed'])
    );

    function checkGenerationByAjax() {
        if (!checkInterval) {
            return;
        }
        $.ajax(
            {
                url: '<?=$this->Url->build('/deposits/check-generating/' . $deposit->get('id'))?>',
                headers: {
                    Accept: 'application/json'
                },
                success: function (content) {
                    if (!checkInterval) {
                        return;
                    }
                    if (!modalGenerating.is(':visible')) {
                        clearInterval(checkInterval);
                        checkInterval = null;
                    } else {
                        var p = $('.modal:visible div.p');
                        p.find('ul').remove();
                        var ul = $('<ul>');
                        p.append(ul);
                        if (!content.beanstalk_worker_id) {
                            ul.append($('<li>').text(__("Non lié à un worker")));
                        } else {
                            ul.append($('<li>').text(__("Lié à un worker")));
                        }
                        if (content.job_state) {
                            ul.append($('<li>').text(__("Etat du job : {0}", content.job_state)));
                        } else {
                            return this.error();
                        }
                        if (content.errors) {
                            ul.append($('<li>').text(content.errors));
                        }
                        if (content.working_workers) {
                            ul.append($('<li>').text(__("Un worker travaille actuellement")));
                        } else {
                            ul.append($('<li>').text(__("Aucun workers ne travaille actuellement")));
                        }
                    }
                },
                error: function () {
                    if (!checkInterval) {
                        return;
                    }
                    $('.modal:visible div.p').html(PHP.messages.genericError);
                    clearInterval(checkInterval);
                    checkInterval = null;
                }
            }
        );
    }
</script>
