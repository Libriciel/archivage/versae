# Versae
[![License](https://img.shields.io/badge/license-AGPL-blue)](https://www.gnu.org/licenses/agpl-3.0.txt)
[![build status](https://gitlab.libriciel.fr/libriciel/pole-archivage/versae/versae/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/libriciel/pole-archivage/versae/versae/pipelines/latest)
[![coverage report](https://gitlab.libriciel.fr/libriciel/pole-archivage/versae/versae/badges/master/coverage.svg)](https://versae-master.dev.libriciel.eu/coverage/versae/index.html)

Dépendances:

| Projet | Build | Coverage |
| ------------ | --------------------- | ---------------------- |
| [asalae-assets](https://gitlab.libriciel.fr/Archivage/asalae/asalae-assets) | [![build status](https://gitlab.libriciel.fr/Archivage/asalae/asalae-assets/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/Archivage/asalae/asalae-assets/pipelines/latest) | [![coverage report](https://gitlab.libriciel.fr/Archivage/asalae/asalae-assets/badges/master/coverage.svg)](https://asalae-master.dev.libriciel.eu/coverage/asalae-assets/index.html) |
| [asalae-core](https://gitlab.libriciel.fr/Archivage/asalae/asalae-core) | [![build status](https://gitlab.libriciel.fr/Archivage/asalae/asalae-core/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/Archivage/asalae/asalae-core/pipelines/latest) | [![coverage report](https://gitlab.libriciel.fr/Archivage/asalae/asalae-core/badges/master/coverage.svg)](https://asalae-master.dev.libriciel.eu/coverage/asalae-core/index.html) |
| [cakephp-beanstalk](https://gitlab.libriciel.fr/CakePHP/cakephp-beanstalk) | [![build status](https://gitlab.libriciel.fr/CakePHP/cakephp-beanstalk/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/CakePHP/cakephp-beanstalk/pipelines/latest) | [![coverage report](https://gitlab.libriciel.fr/CakePHP/cakephp-beanstalk/badges/master/coverage.svg)](https://asalae-master.dev.libriciel.eu/coverage/cakephp-beanstalk/index.html) |
| [cakephp-datacompressor](https://gitlab.libriciel.fr/CakePHP/cakephp-datacompressor) | [![build status](https://gitlab.libriciel.fr/CakePHP/cakephp-datacompressor/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/CakePHP/cakephp-datacompressor/pipelines/latest) | [![coverage report](https://gitlab.libriciel.fr/CakePHP/cakephp-datacompressor/badges/master/coverage.svg)](https://asalae-master.dev.libriciel.eu/coverage/cakephp-datacompressor/index.html) |
| [cakephp-filesystem](https://gitlab.libriciel.fr/CakePHP/cakephp-filesystem) | [![build status](https://gitlab.libriciel.fr/CakePHP/cakephp-filesystem/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/CakePHP/cakephp-filesystem/pipelines/latest) | [![coverage report](https://gitlab.libriciel.fr/CakePHP/cakephp-filesystem/badges/master/coverage.svg)](https://asalae-master.dev.libriciel.eu/coverage/cakephp-filesystem/index.html) |
| [cakephp-login-page](https://gitlab.libriciel.fr/CakePHP/cakephp-login-page) | [![build status](https://gitlab.libriciel.fr/CakePHP/cakephp-login-page/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/CakePHP/cakephp-login-page/pipelines/latest) | [![coverage report](https://gitlab.libriciel.fr/CakePHP/cakephp-login-page/badges/master/coverage.svg)](https://asalae-master.dev.libriciel.eu/coverage/cakephp-login-page/index.html) |
| [file-converters](https://gitlab.libriciel.fr/file-converters/file-converters) | [![build status](https://gitlab.libriciel.fr/file-converters/file-converters/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/file-converters/file-converters/pipelines/latest) | [![coverage report](https://gitlab.libriciel.fr/file-converters/file-converters/badges/master/coverage.svg)](https://asalae-master.dev.libriciel.eu/coverage/file-converters/index.html) |
| [file-validator](https://gitlab.libriciel.fr/file-validator/file-validator) | [![build status](https://gitlab.libriciel.fr/file-validator/file-validator/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/file-validator/file-validator/pipelines/latest) | [![coverage report](https://gitlab.libriciel.fr/file-validator/file-validator/badges/master/coverage.svg)](https://asalae-master.dev.libriciel.eu/coverage/file-validator/index.html) |
| [seda2pdf](https://gitlab.libriciel.fr/libriciel/pole-archivage/seda2pdf) | [![pipeline status](https://gitlab.libriciel.fr/libriciel/pole-archivage/seda2pdf/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/libriciel/pole-archivage/seda2pdf/pipelines/latest) | [![coverage report](https://gitlab.libriciel.fr/libriciel/pole-archivage/seda2pdf/badges/master/coverage.svg)](https://gitlab.libriciel.fr/libriciel/pole-archivage/seda2pdf/-/commits/master) |

## Installation

prérequis d'exploitation
```
sudo su
apt update && apt upgrade -y && apt install -y \
    zip \
    unzip
```

docker + docker compose
```
sudo su
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

installation/update de versae
```
sudo su
cd /opt \
    && mkdir -p /opt/versae \
    && cd versae \
    && wget https://nexus.libriciel.fr/repository/ls-raw/public/archivage/versae-dev.tgz \
    && tar -xvf versae-dev.tgz \
    && rm versae-dev.tgz \
    && if [ ! -f .env ]; then cp .env.dist .env; fi
```

- Modifiez .env selon les besoins (vérifier .env.dist en cas de mise à jour)  

Lancez toute la stack
```
sudo docker compose pull && sudo docker compose up -d
```

Plus qu'à accéder à l'application (par défaut: [http://localhost](http://localhost))
