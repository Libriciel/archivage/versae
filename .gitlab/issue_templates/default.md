### Informations

- Version du logiciel : 1.1.x
- Constaté sur l'instance :
    - [ ] recette
    - [ ] formation
    - [ ] dev
    - [ ] client
- Url : https://...
- Tickets :
  - {url}


### Description du bug

Brève explication du bug, fournir si possible des screens, fichiers et logs.


### Ce que vous auriez souhaité

Expliquez, si nécessaire, le comportement attendu.


### Comment reproduire le bug

Décrire si possible, le déroulement d'opérations qui mènent au bug.



/label ~"Cible:Patch" ~"Type:Bug" 
