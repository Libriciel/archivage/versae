FROM ubuntu:22.04 AS baseimage

ARG timezone=Europe/Paris
ENV DEBIAN_FRONTEND=noninteractive

# upgrade
RUN ln -snf /usr/share/zoneinfo/$timezone /etc/localtime && echo $timezone > /etc/timezone
COPY ./bin/bash_cake_completion /etc/bash_completion.d/cake
RUN apt-get update \
    && apt dist-upgrade -y \
    && apt install -y \
    sudo \
    vim \
    curl \
    wget \
    zip \
    unzip \
    rar \
    && apt install -y --allow-unauthenticated \
    apache2 \
    libapache2-mod-php \
    php \
    php-curl \
    php-gd \
    php-http \
    php-intl \
    php-ldap \
    php-mbstring \
    php-pcov  \
    php-pdo  \
    php-pgsql \
    php-simplexml \
    php-soap \
    php-sqlite3 \
    php-xdebug \
    php-xsl \
    php-zip \
    php-zmq \
    php-raphf \
    && apt install -y bash-completion \
    && apt install -y msmtp \
    && apt install -y imagemagick

# mails
RUN sed -i 's#;sendmail_path =#sendmail_path = "/usr/bin/msmtp -t"#' /etc/php/8.1/apache2/php.ini \
    && sed -i 's#;sendmail_path =#sendmail_path = "/usr/bin/msmtp -t"#' /etc/php/8.1/cli/php.ini

# siegfried
RUN wget https://ressources.libriciel.fr/public/asalae/sf \
    && chmod +x sf \
    && mv sf /usr/bin/ \
    && sf -update

COPY ./apache2-foreground /usr/local/bin/apache2-foreground
RUN chmod +x /usr/local/bin/apache2-foreground

RUN mkdir -p /data /data/config /data/data-versae && chown www-data /var/www && chown www-data /data -R

RUN apt update && apt install -y locales && \
  sed -i -e 's/# fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/' /etc/locale.gen && \
  sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
  dpkg-reconfigure --frontend=noninteractive locales

RUN rm /usr/bin/sf \
    && wget https://go.dev/dl/go1.20.3.linux-amd64.tar.gz \
    && tar -C /usr/local -xzf go1.20.3.linux-amd64.tar.gz \
    && update-alternatives --install "/usr/bin/go" "go" "/usr/local/go/bin/go" 0 \
    && update-alternatives --set go /usr/local/go/bin/go \
    && GOBIN=/usr/local/bin/ go install github.com/richardlehane/siegfried/cmd/sf@latest \
    && sf -update \
    && rm go1.20.3.linux-amd64.tar.gz \
    && rm -rf /usr/local/go /root/go

RUN apt update && apt install -y gnupg2 wget \
    && echo "deb [arch=amd64 signed-by=/usr/share/keyrings/google-linux-signing-key.gpg] https://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list \
    && wget -O- https://dl-ssl.google.com/linux/linux_signing_key.pub | gpg --dearmor -o /usr/share/keyrings/google-linux-signing-key.gpg \
    && apt-get update \
    && apt-get install -y google-chrome-stable

ARG APACHE_UID=1000

ENV TIMEZONE=Europe/Paris
ENV APACHE_RUN_USER=versae
ENV APACHE_RUN_GROUP=versae
ENV DEBIAN_FRONTEND=noninteractive
ENV INITIALIZATION_DIR=/data-entrypoint
ENV APACHE_LOG_DIR=/data-logs
ENV APACHE_PID_FILE=/data-run/apache2.pid
ENV APACHE_RUN_DIR=/data-run

COPY ./bin/bash_cake_completion /etc/bash_completion.d/cake
COPY ./build/apache-security.conf /etc/apache2/conf-available/
COPY build/versae.conf /etc/apache2/sites-available/

RUN useradd --create-home --uid "$APACHE_UID" --user-group "$APACHE_RUN_USER" || true \
    && sed -i "s|APACHE_RUN_USER=.*|APACHE_RUN_USER=$APACHE_RUN_USER|g" /etc/apache2/envvars \
    && sed -i "s|APACHE_RUN_GROUP=.*|APACHE_RUN_GROUP=$APACHE_RUN_GROUP|g" /etc/apache2/envvars \
    && echo 'ServerName versae' >> /etc/apache2/apache2.conf \
    && mkdir -p /etc/apache2/ssl \
    && openssl req -new -x509 -sha256 -newkey rsa:2048 -nodes -keyout /etc/apache2/ssl/server.key -out /etc/apache2/ssl/server.crt -days 365 -subj "/C=FR/ST=FR/L=Herault/O=AS/OU=PRA/CN=localhost" \
    && a2enmod headers proxy proxy_http rewrite ssl \
    && a2enconf apache-security.conf \
    && a2dissite 000-default.conf && a2ensite versae.conf \
    && mkdir -p /var/www/versae && chown $APACHE_RUN_USER /var/www/versae -R \
    && mkdir -p "$INITIALIZATION_DIR" "$APACHE_LOG_DIR" "$APACHE_RUN_DIR" \
    && cp /etc/apache2/sites-available/versae.conf "$INITIALIZATION_DIR"/ \
    && cp /etc/apache2/ssl/* "$INITIALIZATION_DIR"/ \
    && sed -i "s|APACHE_PID_FILE=.*|APACHE_PID_FILE=$APACHE_PID_FILE|g" /etc/apache2/envvars \
    && sed -i "s|APACHE_RUN_DIR=.*|APACHE_RUN_DIR=$APACHE_RUN_DIR|g" /etc/apache2/envvars \
    && sed -i "s|APACHE_LOG_DIR=.*|APACHE_LOG_DIR=$APACHE_LOG_DIR|g" /etc/apache2/envvars \
    && chown -R $APACHE_RUN_USER /etc/apache2/ "$INITIALIZATION_DIR" "$APACHE_LOG_DIR" "$APACHE_RUN_DIR" \
    && mkdir -p /data /data/config /data/data-versae && chown www-data /var/www && chown www-data /data -R

RUN apt install -y gnupg2 wget  \
    && echo "deb [arch=amd64] https://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list  \
    && wget -q -O- https://dl-ssl.google.com/linux/linux_signing_key.pub | tee /etc/apt/trusted.gpg.d/google-linux-signing-key.gpg > /dev/null \
    && apt-get update  \
    && apt-get install -y google-chrome-stable

RUN wget https://ressources.libriciel.fr/public/asalae/sonar-scanner-cli-4.5.0.2216-linux.zip \
    && unzip sonar-scanner-cli-4.5.0.2216-linux.zip \
    && mv sonar-scanner-4.5.0.2216-linux/ /opt/sonar-scanner/ \
    && rm sonar-scanner-cli-4.5.0.2216-linux.zip

RUN sudo -u $APACHE_RUN_USER sf -update

COPY ./build/initialization.sh "$INITIALIZATION_DIR"/

RUN echo "export HOME=/home/versae" >> /etc/apache2/envvars

RUN apt install -y postgresql

CMD ["apache2-foreground"]

#-------------------------------------------------------------------------------

FROM baseimage AS composer-install

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer
RUN apt update && apt install -y git

COPY composer.json /var/www/versae/composer.json
COPY composer.lock /var/www/versae/composer.lock

WORKDIR /var/www/versae

ARG COMPOSER_COMMAND="composer install --optimize-autoloader"
ENV COMPOSER_ALLOW_SUPERUSER=true
RUN $COMPOSER_COMMAND

#-------------------------------------------------------------------------------
FROM quay.io/keycloak/keycloak:22.0.0 AS keycloak_builder

ENV KC_DB=postgres
ENV KC_FEATURES="token-exchange,scripts,preview"

WORKDIR /opt/keycloak

RUN /opt/keycloak/bin/kc.sh build --cache=ispn --health-enabled=true --metrics-enabled=true

#-------------------------------------------------------------------------------
FROM quay.io/keycloak/keycloak:22.0.0 AS keycloak

LABEL image.version=22.0.0

COPY --from=keycloak_builder /opt/keycloak/ /opt/keycloak/

# https://github.com/keycloak/keycloak/issues/19185#issuecomment-1480763024
USER root
RUN sed -i '/disabledAlgorithms/ s/ SHA1,//' /etc/crypto-policies/back-ends/java.config
USER keycloak

RUN /opt/keycloak/bin/kc.sh show-config

ENTRYPOINT ["/opt/keycloak/bin/kc.sh"]

#-------------------------------------------------------------------------------

FROM composer-install AS final

ARG APACHE_UID=1000
ENV APACHE_RUN_USER=versae

RUN useradd --create-home --uid "$APACHE_UID" "$APACHE_RUN_USER" || true

ENV URL=versae.local
ENV DEBUG=false
ENV ADMIN_USER=admin
ENV ADMIN_PASSWORD=password
ENV ADMIN_MAIL=local@localhost.local
ENV DATA_DIR=/data
ENV MAIL_FROM=local@localhost.local
ENV SV_NAME=sv
ENV SA_NAME=sa
ENV POSTGRES_USER=versae
ENV POSTGRES_PASSWORD=versae
ENV POSTGRES_DB=versae
ENV POSTGRES_DBPORT=5432
ENV POSTFIX_PORT=25
ENV TZ=Europe/Paris
ENV APACHE_LOG_FILES=true

COPY --chown=$APACHE_RUN_USER . /var/www/versae/

# empêche de mettre en cache les commandes suivantes
ADD "https://www.random.org/cgi-bin/randbyte?nbytes=10&format=h" skipcache
RUN date > /var/www/versae/.image-dev
WORKDIR /var/www/versae
RUN /var/www/versae/bin/changelog_builder.php > CHANGELOG.md \
    && /var/www/versae/bin/cake hash-dir \
    bin/cake.php \
    src/ \
    webroot/ \
    resources/ \
    templates/ \
    vendor/ \
    -r \
    --filter webroot/org-entity-data/\* \
    --filter resources/hashes.txt \
    --filter resources/hashes.txt.md5 \
    --filter webroot/libersign/\* \
    --filter webroot/libersign \
    --filter webroot/coverage/\* \
    --filter webroot/coverage \
    --filter '*.pot' \
    -o resources/hashes.txt

CMD ["/usr/sbin/apachectl", "-D FOREGROUND"]
