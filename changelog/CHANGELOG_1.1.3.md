## \[1.1.3\] - 13/05/2025

### Évolutions

- Changement des réglages de limite de connexion par défaut à 5 essais toutes les 5 minutes #747
- Paramétrage possible du php.ini #753

### Corrections

- Mise à jour du seda_generator #724
- Sauvegarde des conditions de masquage sur les inputs #740
- Affichage des métadonnées répétables (Language en double) #744
- Édition des transferts avec champs texte répétables #741
- Versioning des formulaires avec mots-clés à champ multiple #742
- Correction erreur 500 sur le récapitulatif du transfert #738
- Configuration de l'upload dans les sections répétables #752
- Optimisation du temps de chargement lors de l'édition d'un versement #755
- Ajout d'une icône de chargement sur le curseur #755
- Les uploads sont initialisés avec required: false si des données existent #755
- Correction de la réapparition des fichiers après suppression #754
