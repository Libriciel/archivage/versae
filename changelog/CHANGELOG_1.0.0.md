## \[1.0.0\] - 19/01/2022

### Nouveautés

- Versements : création, envoi, suivi, historique
- Formulaires : création, publication, gestion des versions
- Administration fonctionnelle : entités, utilisateurs, rôles
- Administration technique : tenants (Services d'Archives), LDAPs, SAEs, workers, crons
