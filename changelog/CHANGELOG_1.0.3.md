## \[1.0.3\] - 09/12/2022

### Nouveautés

- Ajout d'un bouton de test de notification
- Possibilité de choisir plusieurs formats de fichier pour un champ Fichier #420
- Possibilité de rendre obligatoire les cases à cocher multiples #454
- Saisie possible de dates dans les bornes min et max des champs Dates #456
- Nouveau filtre Twig pour le retrait des accents #479

### Évolutions

- Mise à jour du socle technique au niveau d'Asalae 2.1.9
- Les Systèmes d'Archivage Électroniques inactifs ne sont plus vérifiés sur le System Check #488

### Corrections

- SEDA 2.1 à corriger - balises de gestion apparaissent aux deux endroits #398
- Meilleure validation sur les champs de type fichier #429
- Utilisation des formulaires par les entités mères #447
- Bug de suppression logo dans la config administrateur technique #449
- Limitation du filesystem linux - taille des noms sur mkdir() #452
- Zones de saisie : bug d'affichage après une suppression d'une option liste déroulante #455
- Conditions sinon si dans twig : informations perdues lors de la modification #458
- Un utilisateur connecté n'est plus supprimable #471
- Unicité des noms des Systèmes d'archivage électronique au sein d'un même Service d'archives #478
- Suppression de mot-clef ou règle de gestion vide #459
- Profil d'archives et niveau de service en seda 2.1 #460
- L'entité d'appartenance n'apparait pas dans la liste juste après la création d'un utilisateur #472
- Liste déroulante multiple avec moteur de recherche #473
- Mots de passe avec caractères spéciaux pour postgres et admins #477
- Fresh install répétable en cas d'échec #453
