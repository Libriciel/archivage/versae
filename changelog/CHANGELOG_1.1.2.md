## \[1.1.2\] - 04/12/2024

### Évolutions

- Définition du paramétrage proxy dans les variables d'environnement #720
- Champs multiple possible pour certains éléments du contenu de la description #687
- Check des SAE en asynchrone #489

### Corrections

- Optimisation du chargement de l'édition d'un input avec plusieurs milliers d'options #721
- Retrait possible de certains champs spéciaux (comme min, max, formats etc...) dans les champs de formulaire #711
- Retrait de l'onglet du SEDA generator dans l'éditeur de configuration #723
- Champ avec condition de masquage sans icône dans l'édition du formulaire  #729
- Rechargement des droits suite à l'édition d'un utilisateur #569 
