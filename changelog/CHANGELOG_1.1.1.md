## \[1.1.1\] - 03/10/2024

### Nouveautés

- Ajout d'un créateur de job #673
- Ajout de la métadonnée `count` sur les sections répétables #659

### Évolutions

- Input date : date du jour par défaut #667
- Tableau de correspondance (SWITCH) : possibilité de modifier des valeurs importées depuis une liste de mots-clés #674
- Condition de présence sur les unités d'archives répétables #688

### Corrections

- Optimisation du Sedagenerator #686
- Versement bloqué en ready_to_prepare #683
- Input date : date min et date max non enregistrées #664
- Condition SI/SINON : décalage d'affichage dans le select Comparer #661
- Amélioration de l'UI/UX pour la suppression des connecteurs vers les SAE #691
- Pré-remplissage du champ "Envoi par morceaux : Taille des morceaux en octets" #699
- Test d'un formulaire avec sections répétables : perte des informations saisies en cas d'erreur SEDA #670
- Affichage erroné dans la balise dédiée à la communicabilité dans "Prévisualiser l'arborescence" en SEDA 1.0 #685
- Affichage du menu utilisateur avec OpenID #701
- Règles de gestion d'une UA en SEDA v2.1 : problème lorsque toutes les règles sont ajoutées #695
- Extracteurs dans les sections répétables #671
- Correction de la validation PHP des patterns #675
- Métadonnées manquantes/en trop dans l'arborescence #694 #698 #700
- Métadonnées additionnelles partiellement remplies = impossible de reprendre la saisie #703
- Paramétrages admin tech limités à la configuration Docker #668
- Case à cocher cochée par défaut #704
- Possibilité de désassigner un SAE d'un SA #702
- Champ caché dans les sections répétables : bug après enregistrement et réouverture #705
- Initialisation de l'antivirus Clamav #709
- Rendre impossible la suppression de son propre utilisateur #713
