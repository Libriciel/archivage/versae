## \[1.1.0\] - 11/09/2023

### Nouveautés

- Import / Export des formulaires #386
- Choix du rangement des fichiers d'un versement #508
- Champ de texte multiple #417
- Affichage des liens entre éléments de formulaire, extracteurs et calculateurs
- Seda 2.2 #503
- Duplication de champs de formulaire #414
- Duplication des éléments de formulaire #484
- Prévisualisation de l'arborescence d'un formulaire #494
- Changement d'entités d'un utilisateur #466
- Variables sur l'utilisateur connecté, son entité et le service versant sélectionné #416 #457
- Limitation du nombre de tentatives de login #467
- Coche "Utiliser le nom à la place du code" pour le chargement d'options #480
- Tri possible sur les options par valeur ou par texte affiché #531
- Mapping "switch" à partir d'une liste de mots-clés #480
- Ajout libre de métadonnées dans ManagementMetadata #526
- Possibilité d'enregistrer un versement incomplet #524

### Évolutions

- Nouvelle gestion des fichiers #502
- Modifications des listes de formulaires #505
- Augmentation de la force minimum des mots de passe #501
- Amélioration de l'interface de saisie des conditions #418
- Meilleure interface de conditions de masquage de champs
- Possibilité de choisir plusieurs valeurs pour le masquage de champs #284
- Préparation des versements en asynchrone #355
- Gestion de la page de politique de confidentialité (RGPD) #127
- Conversion automatique des CSV pour l'extracteur #425
- Utilisateurs supprimables #127
- Amélioration de la saisie et édition possible des options de listes déroulantes #511
- Affichage des métadonnées "Autre" suite à un test de formulaire #416
- Ajout du service producteur pour les versements et les formulaires #520
- Affichage du type de champ dans le titre de la modale
- Ajout de ManagementMetadata et CodeListVersions #526
- Ajout de métadonnées de gestion et de contenu #530
- Unicité des noms de formulaires #353
- Format de la date par popup #496
- Alerte en cas d'envoi d'un fichier en double #561
- Suppression automatique des métadonnées incomplètes #595
- Saisie possible de plage de coordonnées pour les extracteurs CSV #390

### Corrections

- Affichage des dates au format court FR #496
- Timeout sur versements lourds #500
- Recherche dans une arborescence créant des archives d'unités de documents.
- Affichage des métadonnées liées aux documents #475
- Problème d'import d'un logo #522
- Problème à la suppression d'un fichier dans un versement sans sauvegarde de ce dernier #528
- Nouveaux filtres Twig #523
- Correction de la visualisation d'un formulaire sans droits ajouter #529
- Changement d'onglet automatique en cas d'erreur du formulaire #535
- Correction sécurité #612
- Ordre des unités d'archives dans une arborescence issue d'un fichier compressé #639
- MemoryLimit et Timelimit pour les très grands transferts #642
