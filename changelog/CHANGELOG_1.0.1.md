## \[1.0.1\] - 22/03/2022

### Nouveautés

- Limitation du nombre de tentatives de connexion sur un même login #380
- Affichage de l'interruption de service en json pour les webservices
- Suppression d'un utilisateur possible même s'il possède des versements en cours #256

### Corrections

- DescriptionLevel en seda2.1 #397
- L'ajout d'un mot-clé sur une unité d'archives supprime le contenu défini #397
- Blocage/déblocage des champs utilisés suite à une modification d'une unité d'archives #393
- Édition d'un versement en cours erreur en cas de requête trop lente #387
- Correction de la disparition des logos (ajout d'un volume persistant) #381 
- Données sur la DUA absentes dans la visualisation d'un test de formulaire #399
- Retrait des infobulles en bas de page #403
- Ajout de validation sur les noms de fichiers #396
