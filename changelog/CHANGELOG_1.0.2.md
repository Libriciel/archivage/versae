## \[1.0.2\] - 18/05/2022

### Nouveautés

- feedback lors de la génération du transfert #356

### Corrections

- Affichage conditionnel du guide utilisateur
- On ne propose plus les formulaires des SAE désactivés #359
- Erreur de gestion de droit preedit sur versement #405
- Validations des champs cachés #409
- Suppression des fichiers de test de formulaire #411
- Formulaires : Ajout d'un message d'aide pour tous les types de champs #413
- Versements supprimables depuis l'état "erreur d'envoi" #415
- Mise à jour auto de l'attribut 'name' du champ (identifiant) quand on modifie le nom du champ #421
- Correction sur les conditions de masquage des select et radio #406 #426
- Envoi des transferts en .tar.gz pour contourner le problème sur les noms d'archives finissant par un point #427
- Problème de TransferringAgency vide dans un versement après son édition #428
- Correction sur les champs d'un versement qui pouvaient changer d'ordre d'affichage #430
- Retour en edition sur champs à fichiers multiples #431
- Extracteurs sur champs multiples #433
- Les champs d'une section de formulaire ne sont plus sélectionnables pour ses propres conditions de masquage #445
- La valeur de "Message lorsque le champ select est vide" perdue lors de la duplication #450
- Caractères spéciaux dans les noms de fichier du transfert
