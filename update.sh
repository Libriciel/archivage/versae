#!/bin/bash

/usr/bin/php /var/www/html/bin/cake.php migrations migrate || exit 1
/usr/bin/php /var/www/html/bin/cake.php migrations seed || exit 1
rm /var/www/html/tmp/cache -r
/usr/bin/php /var/www/html/bin/cake.php update || exit 1
/usr/bin/php /var/www/html/bin/cake.php roles_perms import --keep /var/www/html/resources/export_roles.json || exit 1

