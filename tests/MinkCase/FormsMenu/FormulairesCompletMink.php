<?php

namespace Versae\Test\MinkCase\FormsMenu;

use DateTime;
use Exception;
use Versae\MinkSuite\MinkCase;
use Versae\Utility\Twig;

class FormulairesCompletMink extends MinkCase
{
    public function minkAddForm()
    {
        $mink = $this;
        $mink->databaseDeleteIfExists('Forms', ['name' => 'mink_form_complet']);

        $mink->doLogin();
        $mink->doClickMenu(__("Formulaires"), __("Tous les formulaires"));
        $mink->pausable();
        $mink->assertPageTitleIs(__("Tous les formulaires"));

        $mink->doClickButton(__("Ajouter un formulaire"));
        $mink->waitModalOpen();

        // Informations principales
        $mink->assertModalIsOpenned();
        $mink->pausable();
        $mink->setFormFieldValue(__("Nom du formulaire"), 'mink_form_complet');
        $mink->setFormFieldValue(__("Version du SEDA"), 'SEDA version 2.2');
        $mink->setFormFieldValues(
            __("Services versants autorisés à utiliser le formulaire pour les versements"),
            [2]
        );
        $mink->setFormFieldValue(__("Système d'Archivage Electronique (SAE) destinataire"), 1);
        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->pausable();

        // Zones de saisie
        $mink->doClickTab(__("Zones de saisie"));
        $mink->doClickButton(__("Ajouter une section de formulaire"));

        $mink->waitModalOpen();
        $mink->setFormFieldValue(__("Titre de la section"), 'section 1');
        $mink->submitModal();
        $mink->assertResponseSuccess();

        // Champ texte simple
        $mink->pausable();
        $mink->doClickButton(__("Ajouter un champ de formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'text_simple');
        $mink->setFormFieldValue(__("Valeur par défaut"), 'valeur par défaut');
        $mink->setFormFieldValue(__("Attribut `placeholder`"), 'valeur du placeholder');
        $mink->setFormFieldValue(__("Message d'aide sous le champ"), "valeur de l'aide");
        $mink->setFormFieldValue(__("Champ obligatoire"), '1');
        $mink->setFormFieldValue(__("Attribut `pattern`"), '[^0-9]*');
        $mink->setFormFieldValue(__("text_simple"), 'test');
        $mink->doClickButton(__("Tester la validation du champ"));
        $mink->assertElementHasClass('.btn-validity', 'btn-success');
        $mink->setFormFieldValue(__("text_simple"), 'test123');
        $mink->doClickButton(__("Tester la validation du champ"));
        $mink->assertElementHasClass('.btn-validity', 'btn-danger');
        $mink->submitModal();
        $mink->pausable();

        // Champ texte multiple
        $mink->pausable();
        $mink->doClickButton(__("Ajouter un champ de formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'text_multiple');
        $mink->setFormFieldValue(__("Champ à valeur multiple"), '1');
        $mink->submitModal();
        $mink->pausable();

        // Champ nombre
        $mink->pausable();
        $mink->doClickButton(__("Ajouter un champ de formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="number"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'number');
        $mink->setFormFieldValue(__("Valeur minimum"), '2');
        $mink->setFormFieldValue(__("Valeur maximum"), '100');
        $mink->setFormFieldValue(__("Pas"), '2');
        $mink->submitModal();
        $mink->pausable();

        // Champ email
        $mink->pausable();
        $mink->doClickButton(__("Ajouter un champ de formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="email"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'email');
        $mink->setFormFieldValue(__("Attribut `placeholder`"), 'test@test.fr');
        $mink->submitModal();
        $mink->pausable();

        // Champ textarea
        $mink->pausable();
        $mink->doClickButton(__("Ajouter un champ de formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="textarea"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'textarea');
        $mink->submitModal();
        $mink->pausable();

        // Champ select simple
        $mink->pausable();
        $mink->doClickButton(__("Ajouter un champ de formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="select"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'select_simple');
        $mink->doClickTab('Options');
        // setFormFieldValue provoque une exception à cause du confirm()
        $mink->findVisibleFormElement(__("Utiliser une liste de mot clés existante"))->setValue('2');
        $mink->doAcceptAlert();
        $mink->findFirst($mink->cssToXpath('#value-add-option'))->setValue('testunit3');
        $mink->findFirst($mink->cssToXpath('#text-add-option'))->setValue('testunit 3');
        $mink->doClickButton(__("Ajouter une option"));
        $mink->submitModal();
        $mink->pausable();

        // Champ select multiple
        $mink->pausable();
        $mink->doClickButton(__("Ajouter un champ de formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="select"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'select_multiple');
        $mink->setFormFieldValue(__("Champ à valeur multiple"), '1');
        $mink->doClickTab('Options');
        // setFormFieldValue provoque une exception à cause du confirm()
        $mink->findVisibleFormElement(__("Utiliser une liste de mot clés existante"))->setValue('2');
        $mink->doAcceptAlert();
        $mink->findVisibleFormElement(__("valeur = nom"))->setValue('1');
        $mink->doAcceptAlert();
        $mink->submitModal();
        $mink->pausable();

        // Champ date
        $mink->pausable();
        $mink->doClickButton(__("Ajouter un champ de formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="date"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'date');
        $mink->submitModal();
        $mink->pausable();

        // Champ datetime
        $mink->pausable();
        $mink->doClickButton(__("Ajouter un champ de formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="datetime"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'datetime');
        $mink->submitModal();
        $mink->pausable();

        // Champ checkbox_simple
        $mink->pausable();
        $mink->doClickButton(__("Ajouter un champ de formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="checkbox"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'checkbox_simple');
        $mink->submitModal();
        $mink->pausable();

        // Champ checkbox_multiple
        $mink->pausable();
        $mink->doClickButton(__("Ajouter un champ de formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="multi_checkbox"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'checkbox_multiple');
        $mink->doClickTab('Options');
        // setFormFieldValue provoque une exception à cause du confirm()
        $mink->findVisibleFormElement(__("Utiliser une liste de mot clés existante"))->setValue('2');
        $mink->doAcceptAlert();
        $mink->wait(500); // ne charge pas les options sinon
        $mink->submitModal();
        $mink->pausable();

        // Champ radio
        $mink->pausable();
        $mink->doClickButton(__("Ajouter un champ de formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="radio"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'radio');
        $mink->doClickTab('Options');
        // setFormFieldValue provoque une exception à cause du confirm()
        $mink->findVisibleFormElement(__("Utiliser une liste de mot clés existante"))->setValue('2');
        $mink->doAcceptAlert();
        $mink->wait(500); // ne charge pas les options sinon
        $mink->submitModal();
        $mink->pausable();

        // Champ file_simple
        $mink->pausable();
        $mink->doClickButton(__("Ajouter un champ de formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="file"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'file_simple');
        $mink->setFormFieldValue(__("Champ obligatoire"), '1');
        $mink->submitModal();
        $mink->pausable();

        // Champ file_multiple
        $mink->pausable();
        $mink->doClickButton(__("Ajouter un champ de formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="file"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'file_multiple');
        $mink->setFormFieldValue(__("Champ à valeur multiple"), '1');
        $mink->submitModal();
        $mink->pausable();

        // Champ zip
        $mink->pausable();
        $mink->doClickButton(__("Ajouter un champ de formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="archive_file"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'zip');
        $mink->submitModal();
        $mink->pausable();

        // Champ paragraph
        $mink->pausable();
        $mink->doClickButton(__("Ajouter un champ de formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="paragraph"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Couleur de fond"), 'alert-info');
        $mink->doTinyMceFill('add-input2-p', "Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        $mink->submitModal();
        $mink->pausable();
    }

    /**
     * section repetable
     * @return void
     * @throws Exception
     */
    public function minkAddSection()
    {
        $mink = $this;

        $mink->assertModalIsOpenned();
        $mink->doClickButton(__("Ajouter une section de formulaire"));

        $mink->waitModalOpen();
        $mink->setFormFieldValue(__("Titre de la section"), 'section 2');
        $mink->setFormFieldValue(__("Section répétable"), '1');
        $mink->setFormFieldValue(__("Nom du bouton ajouter"), 'Ajouter une section répétable');
        $mink->submitModal();
        $mink->assertResponseSuccess();

        // Champ texte simple
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'text_simple_repeatable');
        $mink->setFormFieldValue(__("Valeur par défaut"), 'valeur par défaut');
        $mink->setFormFieldValue(__("Attribut `placeholder`"), 'valeur du placeholder');
        $mink->setFormFieldValue(__("Message d'aide sous le champ"), "valeur de l'aide");
        $mink->setFormFieldValue(__("Champ obligatoire"), '1');
        $mink->setFormFieldValue(__("Attribut `pattern`"), '[^0-9]*');
        $mink->setFormFieldValue(__("text_simple_repeatable"), 'test');
        $mink->doClickButton(__("Tester la validation du champ"));
        $mink->assertElementHasClass('.btn-validity', 'btn-success');
        $mink->setFormFieldValue(__("text_simple_repeatable"), 'test123');
        $mink->doClickButton(__("Tester la validation du champ"));
        $mink->assertElementHasClass('.btn-validity', 'btn-danger');
        $mink->submitModal();
        $mink->pausable();

        // Champ texte multiple
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'text_multiple_repeatable');
        $mink->setFormFieldValue(__("Champ à valeur multiple"), '1');
        $mink->submitModal();
        $mink->pausable();

        // Champ nombre
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="number"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'number_repeatable');
        $mink->setFormFieldValue(__("Valeur minimum"), '2');
        $mink->setFormFieldValue(__("Valeur maximum"), '100');
        $mink->setFormFieldValue(__("Pas"), '2');
        $mink->submitModal();
        $mink->pausable();

        // Champ email
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="email"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'email_repeatable');
        $mink->setFormFieldValue(__("Attribut `placeholder`"), 'test@test.fr');
        $mink->submitModal();
        $mink->pausable();

        // Champ textarea
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="textarea"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'textarea_repeatable');
        $mink->submitModal();
        $mink->pausable();

        // Champ select simple
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="select"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'select_simple_repeatable');
        $mink->doClickTab('Options');
        // setFormFieldValue provoque une exception à cause du confirm()
        $mink->findVisibleFormElement(__("Utiliser une liste de mot clés existante"))->setValue('2');
        $mink->doAcceptAlert();
        $mink->findFirst($mink->cssToXpath('#value-add-option'))->setValue('testunit3');
        $mink->findFirst($mink->cssToXpath('#text-add-option'))->setValue('testunit 3');
        $mink->doClickButton(__("Ajouter une option"));
        $mink->submitModal();
        $mink->pausable();

        // Champ select multiple
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="select"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'select_multiple_repeatable');
        $mink->setFormFieldValue(__("Champ à valeur multiple"), '1');
        $mink->doClickTab('Options');
        // setFormFieldValue provoque une exception à cause du confirm()
        $mink->findVisibleFormElement(__("Utiliser une liste de mot clés existante"))->setValue('2');
        $mink->doAcceptAlert();
        $mink->findVisibleFormElement(__("valeur = nom"))->setValue('1');
        $mink->doAcceptAlert();
        $mink->submitModal();
        $mink->pausable();

        // Champ date
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="date"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'date_repeatable');
        $mink->submitModal();
        $mink->pausable();

        // Champ datetime
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="datetime"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'datetime_repeatable');
        $mink->submitModal();
        $mink->pausable();

        // Champ checkbox_simple
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="checkbox"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'checkbox_simple_repeatable');
        $mink->submitModal();
        $mink->pausable();

        // Champ checkbox_multiple
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="multi_checkbox"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'checkbox_multiple_repeatable');
        $mink->doClickTab('Options');
        // setFormFieldValue provoque une exception à cause du confirm()
        $mink->findVisibleFormElement(__("Utiliser une liste de mot clés existante"))->setValue('2');
        $mink->doAcceptAlert();
        $mink->wait(500); // ne charge pas les options sinon
        $mink->submitModal();
        $mink->pausable();

        // Champ radio
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="radio"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'radio_repeatable');
        $mink->doClickTab('Options');
        // setFormFieldValue provoque une exception à cause du confirm()
        $mink->findVisibleFormElement(__("Utiliser une liste de mot clés existante"))->setValue('2');
        $mink->doAcceptAlert();
        $mink->wait(500); // ne charge pas les options sinon
        $mink->submitModal();
        $mink->pausable();

        // Champ file_simple
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="file"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'file_simple_repeatable');
        $mink->setFormFieldValue(__("Champ obligatoire"), '1');
        $mink->submitModal();
        $mink->pausable();

        // Champ file_multiple
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="file"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'file_multiple_repeatable');
        $mink->setFormFieldValue(__("Champ à valeur multiple"), '1');
        $mink->submitModal();
        $mink->pausable();

        // Champ zip
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="archive_file"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'zip_repeatable');
        $mink->submitModal();
        $mink->pausable();

        // Champ paragraph
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="paragraph"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Couleur de fond"), 'alert-info');
        $mink->doTinyMceFill('add-input2-p', "Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        $mink->submitModal();
        $mink->pausable();

        $mink->submitModal();
    }

    public function minkAddTree()
    {
        $mink = $this;

        $mink->assertPageTitleIs(__("Tous les formulaires"));
        $mink->doClickAction(__("Modifier {0}", 'mink_form_complet'));
        $mink->waitModalOpen();

        // Informations principales
        $mink->assertModalIsOpenned();
        $mink->pausable();

        $mink->doClickTab(__("Transfert"));
        $mink->doClickAction(__("Ajouter {0}", __("Commentaire (vide = Nom du versement)")));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Représentation twig"), 'Commentaire: ');
        $mink->findFirst($mink->cssToXpath('#edit-header-variable-field-concat'))->setValue('input.text_simple');
        $mink->submitModal();

        $mink->doClickTab(__("Arborescence"));

        // noeud archive
        $mink->doClick('#jstree-btn-add-root_anchor');
        $mink->waitRequestChange();
        $mink->pausable();
        $mink->setFormFieldValue(__("Type d'élément"), 'simple');
        $mink->setFormFieldValue(__("Nom de l'élément"), 'ua');
        $mink->prepareRequestChange();
        $mink->submitModal(); // on enregistre, ça redirige sur l'édition
        $mink->waitRequestChange();

        $mink->doClickTab(__("Descriptions"));
        $mink->doClickAction(__("Ajouter {0}", __("Description détaillée")));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Représentation twig"), 'Description: ');
        $mink->findFirst($mink->cssToXpath('#edit-content-unit-field-concat'))->setValue('input.select_simple');
        $mink->submitModal();

        $mink->doClickTab(__("Mots clés"));
        $mink->doClickButton(__("Ajouter un mot clé"));
        $mink->waitModalOpen();
        $mink->setFormFieldValue(__("Nom du mot clé"), 'mot clé de test');
        $mink->setFormFieldValue(__("Ce mot-clé sera à valeurs multiples"), 'input.select_multiple');
        $mink->submitModal();
        $mink->prepareRequestChange();
        $mink->submitModal();
        $mink->waitRequestChange();
        $mink->prepareRequestChange();
        $mink->submitModal();
        $mink->waitRequestChange();
        $mink->wait(); // chargement jstree

        $mink->pausable();
        $mink->prepareRequestChange();
        $mink->doClick('.jstree-container-ul > li:first-child > ul > li.add-element > a');
        $mink->waitRequestChange();
        $mink->setFormFieldValue(__("Type d'élément"), 'simple');
        $mink->setFormFieldValue(__("Nom de l'élément"), 'concat_all');
        $mink->prepareRequestChange();
        $mink->submitModal(); // on enregistre, ça redirige sur l'édition
        $mink->waitRequestChange();

        $mink->doClickTab(__("Descriptions"));
        $mink->doClickAction(__("Ajouter {0}", __("Description détaillée")));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Représentation twig"), 'Description: ');
        // NOTE: findFirst nécéssaire car le champ n'a pas de name
        $select = $mink->findFirst($mink->cssToXpath('#edit-content-unit-field-concat'));
        $select->setValue('input.text_simple');
        $mink->appendToFieldValue(__("Représentation twig"), ', ');
        $select->setValue('input.number');
        $mink->appendToFieldValue(__("Représentation twig"), ', ');
        $select->setValue('input.email');
        $mink->appendToFieldValue(__("Représentation twig"), ', ');
        $select->setValue('input.textarea');
        $mink->appendToFieldValue(__("Représentation twig"), ', ');
        $select->setValue('input.select_simple');
        $mink->appendToFieldValue(__("Représentation twig"), ', ');
        $select->setValue('input.date');
        $mink->findFirst($mink->cssToXpath('#edit-content-unit-date_format-concat-popup'))->setValue('fr');
        $mink->doClickButton(__("Ajouter la date"));
        $mink->wait(); // animation
        $mink->appendToFieldValue(__("Représentation twig"), ', ');
        $select->setValue('input.datetime');
        $mink->findFirst($mink->cssToXpath('#edit-content-unit-date_format-concat-popup'))->setValue('fr_long');
        $mink->doClickButton(__("Ajouter la date"));
        $mink->wait(); // animation
        $mink->appendToFieldValue(__("Représentation twig"), ', ');
        $select->setValue('input.checkbox_simple');
        $mink->appendToFieldValue(__("Représentation twig"), ', ');
        $select->setValue('input.radio');

        $mink->submitModal();
        $mink->doClickTab(__("Mots clés"));

        $mink->doClickButton(__("Ajouter un mot clé"));
        $mink->waitModalOpen();
        $mink->setFormFieldValue(__("Nom du mot clé"), 'text_multiple');
        $mink->setFormFieldValue(__("Référence du mot clé"), 'concat_multiple');
        $mink->setFormFieldValue(__("Type du mot clé"), 'subject');
        $mink->submitModal();
        $mink->doClickAction(__("Modifier {0}", __("Valeur du mot clé (defaut : Nom du mot clé)")));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Type"), 'concat_multiple');
        $mink->setFormFieldValue(__("Sélectionner un champ multiple"), 'input.text_multiple');
        $mink->setFormFieldValue(__("Séparateur"), ', ');
        $mink->submitModal();
        $mink->submitModal();

        $mink->doClickButton(__("Ajouter un mot clé"));
        $mink->waitModalOpen();
        $mink->setFormFieldValue(__("Nom du mot clé"), 'select_multiple');
        $mink->setFormFieldValue(__("Référence du mot clé"), 'concat_multiple');
        $mink->setFormFieldValue(__("Type du mot clé"), 'subject');
        $mink->submitModal();
        $mink->doClickAction(__("Modifier {0}", __("Valeur du mot clé (defaut : Nom du mot clé)")));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Type"), 'concat_multiple');
        $mink->setFormFieldValue(__("Sélectionner un champ multiple"), 'input.select_multiple');
        $mink->setFormFieldValue(__("Séparateur"), ', ');
        $mink->submitModal();
        $mink->submitModal();

        $mink->doClickButton(__("Ajouter un mot clé"));
        $mink->waitModalOpen();
        $mink->setFormFieldValue(__("Nom du mot clé"), 'checkbox_multiple');
        $mink->setFormFieldValue(__("Référence du mot clé"), 'concat_multiple');
        $mink->setFormFieldValue(__("Type du mot clé"), 'subject');
        $mink->submitModal();
        $mink->doClickAction(__("Modifier {0}", __("Valeur du mot clé (defaut : Nom du mot clé)")));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Type"), 'concat_multiple');
        $mink->setFormFieldValue(__("Sélectionner un champ multiple"), 'input.checkbox_multiple');
        $mink->setFormFieldValue(__("Séparateur"), ', ');
        $mink->submitModal();
        $mink->submitModal();

        // noeud document simple
        $mink->prepareRequestChange();
        $mink->submitModal();
        $mink->waitRequestChange();
        $mink->wait(); // chargement jstree

        $form = $mink->databaseFindFirstOrFail('Forms', ['name' => 'mink_form_complet']);

        $mink->pausable();
        $mink->prepareRequestChange();
        $mink->doClick('.jstree-container-ul > li:first-child > ul > li.add-element > a');
        $mink->waitRequestChange();
        $mink->setFormFieldValue(__("Type d'élément"), 'document');
        $mink->setFormFieldValue(__("Nom de l'élément"), 'document');
        $input = $mink->databaseFindFirstOrFail('FormInputs', ['name' => 'file_simple', 'form_id' => $form->id]);
        $mink->setFormFieldValue(__("Champ de formulaire"), $input->id);
        $mink->prepareRequestChange();
        $mink->submitModal(); // on enregistre, ça redirige sur l'édition
        $mink->waitRequestChange();
        $mink->prepareRequestChange();
        $mink->submitModal();
        $mink->waitRequestChange();

        $mink->pausable();
        $mink->prepareRequestChange();
        $mink->doClick('.jstree-container-ul > li:first-child > ul > li.add-element > a');
        $mink->waitRequestChange();

        $mink->setFormFieldValue(__("Type d'élément"), 'document_multiple');
        $mink->setFormFieldValue(__("Nom de l'élément"), 'document_multiple');
        $input = $mink->databaseFindFirstOrFail('FormInputs', ['name' => 'file_multiple', 'form_id' => $form->id]);
        $mink->setFormFieldValue(__("Champ de formulaire"), $input->id);
        $mink->prepareRequestChange();
        $mink->submitModal(); // on enregistre, ça redirige sur l'édition
        $mink->waitRequestChange();
        $mink->prepareRequestChange();
        $mink->submitModal();
        $mink->waitRequestChange();

        $mink->pausable();
        $mink->prepareRequestChange();
        $mink->doClick('.jstree-container-ul > li:first-child > ul > li.add-element > a');
        $mink->waitRequestChange();

        $mink->setFormFieldValue(__("Type d'élément"), 'tree_root');
        $mink->setFormFieldValue(__("Nom de l'élément"), 'tree_root');
        $input = $mink->databaseFindFirstOrFail('FormInputs', ['name' => 'zip', 'form_id' => $form->id]);
        $mink->setFormFieldValue(__("Champ de formulaire"), $input->id);
        $mink->prepareRequestChange();
        $mink->submitModal(); // on enregistre, ça redirige sur l'édition
        $mink->waitRequestChange();
        $mink->prepareRequestChange();
        $mink->submitModal();
        $mink->waitRequestChange();

        $mink->prepareRequestChange();
        $mink->submitModal();
        $mink->waitRequestChange();
    }

    public function minkAddRepeatable()
    {
        $mink = $this;

        $mink->assertPageTitleIs(__("Tous les formulaires"));
        $mink->doClickAction(__("Modifier {0}", 'mink_form_complet'));
        $mink->waitModalOpen();

        // Informations principales
        $mink->assertModalIsOpenned();
        $mink->pausable();

        $mink->doClickTab(__("Arborescence"));
        $form = $mink->databaseFindFirstOrFail('Forms', ['name' => 'mink_form_complet']);
        $fieldset = $mink->databaseFindFirstOrFail('FormFieldsets', ['legend' => 'section 2', 'form_id' => $form->id]);
        $formUnitRootId = $mink->databaseFindFirstOrFail('FormUnits', ['name' => 'ua', 'form_id' => $form->id])->id;

        // noeud archive
        $mink->doClick('.jstree-container-ul > li:first-child > ul > li.add-element > a');
        $mink->waitRequestChange();
        $mink->pausable();
        $mink->setFormFieldValue(__("Type d'élément"), 'repeatable');
        $mink->setFormFieldValue(__("Nom de l'élément"), 'repeatable');
        $mink->setFormFieldValue(__("Section répétable"), $fieldset->id);
        $mink->prepareRequestChange();
        $mink->submitModal(); // on enregistre, ça redirige sur l'édition
        $mink->waitRequestChange();

        $mink->doClickTab(__("Descriptions"));
        $mink->doClickAction(__("Ajouter {0}", __("Description détaillée")));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Représentation twig"), 'Description: ');
        $select = $mink->findFirst($mink->cssToXpath('#edit-content-unit-field-concat'));
        $select->setValue('input.select_simple_repeatable');
        $mink->submitModal();

        $mink->doClickTab(__("Mots clés"));
        $mink->doClickButton(__("Ajouter un mot clé"));
        $mink->waitModalOpen();
        $mink->setFormFieldValue(__("Nom du mot clé"), 'mot clé de test');
        $mink->setFormFieldValue(__("Ce mot-clé sera à valeurs multiples"), 'input.select_multiple_repeatable');
        $mink->submitModal();
        $mink->prepareRequestChange();
        $mink->submitModal();
        $mink->waitRequestChange();
        $mink->prepareRequestChange();
        $mink->submitModal();
        $mink->waitRequestChange();
        $mink->wait(); // chargement jstree

        $conditions = ['name' => 'repeatable', 'form_id' => $form->id];
        $formUnitRepeatableId = $mink->databaseFindFirstOrFail('FormUnits', $conditions)->id;
        $jstreeAddId = sprintf('#jstree-btn-add-%d-%d_anchor', $formUnitRootId, $formUnitRepeatableId);

        $mink->pausable();
        $mink->prepareRequestChange();
        $mink->doClick($jstreeAddId);
        $mink->waitRequestChange();
        $mink->setFormFieldValue(__("Type d'élément"), 'simple');
        $mink->setFormFieldValue(__("Nom de l'élément"), 'concat_all_repeatable');
        $mink->prepareRequestChange();
        $mink->submitModal(); // on enregistre, ça redirige sur l'édition
        $mink->waitRequestChange();

        $mink->doClickTab(__("Descriptions"));
        $mink->doClickAction(__("Ajouter {0}", __("Description détaillée")));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Représentation twig"), 'Description: ');
        $select = $mink->findFirst($mink->cssToXpath('#edit-content-unit-field-concat'));
        $select->setValue('input.text_simple_repeatable');
        $mink->appendToFieldValue(__("Représentation twig"), ', ');
        $select->setValue('input.number_repeatable');
        $mink->appendToFieldValue(__("Représentation twig"), ', ');
        $select->setValue('input.email_repeatable');
        $mink->appendToFieldValue(__("Représentation twig"), ', ');
        $select->setValue('input.textarea_repeatable');
        $mink->appendToFieldValue(__("Représentation twig"), ', ');
        $select->setValue('input.select_simple_repeatable');
        $mink->appendToFieldValue(__("Représentation twig"), ', ');
        $select->setValue('input.date_repeatable');
        $mink->findFirst($mink->cssToXpath('#edit-content-unit-date_format-concat-popup'))->setValue('fr');
        $mink->doClickButton(__("Ajouter la date"));
        $mink->wait(); // animation
        $mink->appendToFieldValue(__("Représentation twig"), ', ');
        $select->setValue('input.datetime_repeatable');
        $mink->findFirst($mink->cssToXpath('#edit-content-unit-date_format-concat-popup'))->setValue('fr_long');
        $mink->doClickButton(__("Ajouter la date"));
        $mink->wait(); // animation
        $mink->appendToFieldValue(__("Représentation twig"), ', ');
        $select->setValue('input.checkbox_simple_repeatable');
        $mink->appendToFieldValue(__("Représentation twig"), ', ');
        $select->setValue('input.radio_repeatable');

        $mink->submitModal();
        $mink->doClickTab(__("Mots clés"));

        $mink->doClickButton(__("Ajouter un mot clé"));
        $mink->waitModalOpen();
        $mink->setFormFieldValue(__("Nom du mot clé"), 'text_multiple');
        $mink->setFormFieldValue(__("Référence du mot clé"), 'concat_multiple');
        $mink->setFormFieldValue(__("Type du mot clé"), 'subject');
        $mink->submitModal();
        $mink->doClickAction(__("Modifier {0}", __("Valeur du mot clé (defaut : Nom du mot clé)")));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Type"), 'concat_multiple');
        $mink->setFormFieldValue(__("Sélectionner un champ multiple"), 'input.text_multiple');
        $mink->setFormFieldValue(__("Séparateur"), ', ');
        $mink->submitModal();
        $mink->submitModal();

        $mink->doClickButton(__("Ajouter un mot clé"));
        $mink->waitModalOpen();
        $mink->setFormFieldValue(__("Nom du mot clé"), 'select_multiple');
        $mink->setFormFieldValue(__("Référence du mot clé"), 'concat_multiple');
        $mink->setFormFieldValue(__("Type du mot clé"), 'subject');
        $mink->submitModal();
        $mink->doClickAction(__("Modifier {0}", __("Valeur du mot clé (defaut : Nom du mot clé)")));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Type"), 'concat_multiple');
        $mink->setFormFieldValue(__("Sélectionner un champ multiple"), 'input.select_multiple');
        $mink->setFormFieldValue(__("Séparateur"), ', ');
        $mink->submitModal();
        $mink->submitModal();

        $mink->doClickButton(__("Ajouter un mot clé"));
        $mink->waitModalOpen();
        $mink->setFormFieldValue(__("Nom du mot clé"), 'checkbox_multiple');
        $mink->setFormFieldValue(__("Référence du mot clé"), 'concat_multiple');
        $mink->setFormFieldValue(__("Type du mot clé"), 'subject');
        $mink->submitModal();
        $mink->doClickAction(__("Modifier {0}", __("Valeur du mot clé (defaut : Nom du mot clé)")));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Type"), 'concat_multiple');
        $mink->setFormFieldValue(__("Sélectionner un champ multiple"), 'input.checkbox_multiple');
        $mink->setFormFieldValue(__("Séparateur"), ', ');
        $mink->submitModal();
        $mink->submitModal();

        // noeud document simple
        $mink->prepareRequestChange();
        $mink->submitModal();
        $mink->waitRequestChange();
        $mink->wait(); // chargement jstree

        $form = $mink->databaseFindFirstOrFail('Forms', ['name' => 'mink_form_complet']);

        $mink->pausable();
        $mink->prepareRequestChange();
        $mink->doClick($jstreeAddId);
        $mink->waitRequestChange();
        $mink->setFormFieldValue(__("Type d'élément"), 'document');
        $mink->setFormFieldValue(__("Nom de l'élément"), 'document_repeatable');
        $conditions = ['name' => 'file_simple_repeatable', 'form_id' => $form->id];
        $input = $mink->databaseFindFirstOrFail('FormInputs', $conditions);
        $mink->setFormFieldValue(__("Champ de formulaire"), $input->id);
        $mink->prepareRequestChange();
        $mink->submitModal(); // on enregistre, ça redirige sur l'édition
        $mink->waitRequestChange();
        $mink->prepareRequestChange();
        $mink->submitModal();
        $mink->waitRequestChange();

        $mink->pausable();
        $mink->prepareRequestChange();
        $mink->doClick($jstreeAddId);
        $mink->waitRequestChange();

        $mink->setFormFieldValue(__("Type d'élément"), 'document_multiple');
        $mink->setFormFieldValue(__("Nom de l'élément"), 'document_multiple_repeatable');
        $conditions = ['name' => 'file_multiple_repeatable', 'form_id' => $form->id];
        $input = $mink->databaseFindFirstOrFail('FormInputs', $conditions);
        $mink->setFormFieldValue(__("Champ de formulaire"), $input->id);
        $mink->prepareRequestChange();
        $mink->submitModal(); // on enregistre, ça redirige sur l'édition
        $mink->waitRequestChange();
        $mink->prepareRequestChange();
        $mink->submitModal();
        $mink->waitRequestChange();

        $mink->pausable();
        $mink->prepareRequestChange();
        $mink->doClick($jstreeAddId);
        $mink->waitRequestChange();

        $mink->setFormFieldValue(__("Type d'élément"), 'tree_root');
        $mink->setFormFieldValue(__("Nom de l'élément"), 'tree_root_repeatable');
        $conditions = ['name' => 'zip_repeatable', 'form_id' => $form->id];
        $input = $mink->databaseFindFirstOrFail('FormInputs', $conditions);
        $mink->setFormFieldValue(__("Champ de formulaire"), $input->id);
        $mink->prepareRequestChange();
        $mink->submitModal(); // on enregistre, ça redirige sur l'édition
        $mink->waitRequestChange();
        $mink->prepareRequestChange();
        $mink->submitModal();
        $mink->waitRequestChange();

        $mink->prepareRequestChange();
        $mink->submitModal();
        $mink->waitRequestChange();
    }

    public function minkTestForm()
    {
        $mink = $this;

        $mink->assertPageTitleIs(__("Tous les formulaires"));
        $mink->doClickAction(__("Tester {0}", 'mink_form_complet'));
        $mink->waitModalOpen();

        // Informations principales
        $mink->assertModalIsOpenned();
        $mink->pausable();

        $mink->setFormFieldValue('text_simple', 'text_simple');
        $mink->setFormFieldValue('text_multiple', 'text_multiple_1');
        $mink->doClickButton(__("Ajouter une valeur ''{0}''", 'text_multiple'));
        $mink->setFormFieldValue('text_multiple', 'text_multiple_2');
        $mink->setFormFieldValue('number', '4');
        $mink->setFormFieldValue('email', 'test@test.fr');
        $mink->setFormFieldValue('textarea', 'textarea_1');
        $mink->setFormFieldValue('select_simple', 'testunit1');
        $mink->setFormFieldValues('select_multiple', ['testunit1', 'testunit2']);
        $mink->doClick('#test-inputs-date');
        $mink->doClick('a.ui-state-highlight');
        $now = new DateTime();
        $mink->doClick('#test-inputs-datetime');
        $mink->doClick('button.ui-datepicker-current');
        $mink->doClick('button.ui-datepicker-close');
        $mink->setFormFieldValue('checkbox_simple', '1');
        $mink->setFormFieldValues('checkbox_multiple', ['testunit1', 'testunit2']);
        $mink->setFormFieldValue('radio', 'testunit1');
        $file1 = $mink->fileCreate('test1.txt', 'test');
        $mink->setFormFieldValue('file_simple', $file1);
        $file2 = $mink->fileCreate('test2.txt', 'test');
        $file3 = $mink->fileCreate('test3.txt', 'test');
        $mink->setFormFieldValue('file_multiple', $file2);
        $mink->setFormFieldValue('file_multiple', $file3);
        $file4 = $mink->fileCreate('dir1/test4.txt', 'test');
        $file5 = $mink->fileCreate('dir1/test5.txt', 'test');
        $file6 = $mink->fileCreate('dir2/test6.txt', 'test');
        $zip1 = $mink->fileCreateZip('test7.zip', [$file4, $file5, $file6]);
        $mink->setFormFieldValue('zip', $zip1);
        $mink->waitUploadComplete();
        $mink->wait();

        // section répétable 1
        $mink->doClickButton('Ajouter une section répétable');
        $mink->setFormFieldValue('text_simple_repeatable 1', 'text_simple');
        $mink->setFormFieldValue('text_multiple_repeatable 1', 'text_multiple_section1_1');
        $mink->doClickButton(__("Ajouter une valeur ''{0}''", 'text_multiple_repeatable'));
        $mink->setFormFieldValue('text_multiple_repeatable 1', 'text_multiple_section1_2');
        $mink->setFormFieldValue('number_repeatable 1', '6');
        $mink->setFormFieldValue('email_repeatable 1', 'test@testsection1.fr');
        $mink->setFormFieldValue('textarea_repeatable 1', 'textarea_section1_1');
        $mink->setFormFieldValue('select_simple_repeatable 1', 'testunit1');
        $mink->setFormFieldValues('select_multiple_repeatable 1', ['testunit1', 'testunit2']);
        $mink->doClick('#test-inputs-date-repeatable-0');
        $mink->doClick('a.ui-state-highlight');
        $mink->doClick('#test-inputs-datetime-repeatable-0');
        $mink->doClick('button.ui-datepicker-current');
        $mink->doClick('button.ui-datepicker-close');
        $mink->setFormFieldValue('checkbox_simple_repeatable 1', '1');
        $mink->setFormFieldValues('checkbox_multiple_repeatable 1', ['testunit1', 'testunit2']);
        $mink->setFormFieldValue('radio_repeatable 1', 'testunit2');
        $file1 = $mink->fileCreate('test11.txt', 'test');
        $mink->setFormFieldValue('file_simple_repeatable 1', $file1);
        $file2 = $mink->fileCreate('test12.txt', 'test');
        $file3 = $mink->fileCreate('test13.txt', 'test');
        $mink->setFormFieldValue('file_multiple_repeatable 1', $file2);
        $mink->setFormFieldValue('file_multiple_repeatable 1', $file3);
        $file4 = $mink->fileCreate('dir1/test14.txt', 'test');
        $file5 = $mink->fileCreate('dir1/test15.txt', 'test');
        $file6 = $mink->fileCreate('dir2/test16.txt', 'test');
        $zip1 = $mink->fileCreateZip('test17.zip', [$file4, $file5, $file6]);
        $mink->setFormFieldValue('zip_repeatable 1', $zip1);
        $mink->waitUploadComplete();
        $mink->wait();

        // section répétable 2
        $mink->doClickButton('Ajouter une section répétable');
        $mink->setFormFieldValue('text_simple_repeatable 2', 'text_simple');
        $mink->setFormFieldValue('text_multiple_repeatable 2', 'text_multiple_section2_1');
        $btnXpath = $mink->cssToXpath('#test-inputs-text-multiple-repeatable-1-0');
        $mink->findFirst($btnXpath)->getParent()->find('css', 'button')->click();
        $mink->setFormFieldValue('text_multiple_repeatable 2', 'text_multiple_section2_2');
        $mink->setFormFieldValue('number_repeatable 2', '8');
        $mink->setFormFieldValue('email_repeatable 2', 'test@testsection2.fr');
        $mink->setFormFieldValue('textarea_repeatable 2', 'textarea_section2_1');
        $mink->setFormFieldValue('select_simple_repeatable 2', 'testunit1');
        $mink->setFormFieldValues('select_multiple_repeatable 2', ['testunit2']);
        $mink->doClick('#test-inputs-date-repeatable-1');
        $mink->doClick('a.ui-state-highlight');
        $mink->doClick('#test-inputs-datetime-repeatable-1');
        $mink->doClick('button.ui-datepicker-current');
        $mink->doClick('button.ui-datepicker-close');
        $mink->setFormFieldValue('checkbox_simple_repeatable 2', '0');
        $mink->setFormFieldValues('checkbox_multiple_repeatable 2', ['testunit2']);
        $mink->setFormFieldValue('radio_repeatable 2', 'testunit2');
        $file1 = $mink->fileCreate('test21.txt', 'test');
        $mink->setFormFieldValue('file_simple_repeatable 2', $file1);
        $file2 = $mink->fileCreate('test22.txt', 'test');
        $file3 = $mink->fileCreate('test23.txt', 'test');
        $mink->setFormFieldValue('file_multiple_repeatable 2', $file2);
        $mink->setFormFieldValue('file_multiple_repeatable 2', $file3);
        $file4 = $mink->fileCreate('dir1/test24.txt', 'test');
        $file5 = $mink->fileCreate('dir1/test25.txt', 'test');
        $file6 = $mink->fileCreate('dir2/test26.txt', 'test');
        $zip1 = $mink->fileCreateZip('test27.zip', [$file4, $file5, $file6]);
        $mink->setFormFieldValue('zip_repeatable 2', $zip1);
        $mink->waitUploadComplete();
        $mink->wait();

        $mink->prepareRequestChange();
        $mink->submitModal();
        $mink->waitRequestChange();

        // assertions
        $xmlPath = $mink->doDownloadFromLink(__("Télécharger le bordereau"));
        $mink->domLoad($xmlPath);
        $concatAll = $mink->domGetValue('//ns:ArchiveObject//ns:Description');
        $expectedValues = ['Description: text_simple'];
        $expectedValues[] = '4';
        $expectedValues[] = 'test@test.fr';
        $expectedValues[] = 'textarea_1';
        $expectedValues[] = 'testunit1';
        $formatShort = '{{datevalue|format_datetime("short", "none", locale="fr")}}';
        $expectedValues[] = Twig::render($formatShort, ['datevalue' => $now]);
        $formatFull = '{{datevalue|format_datetime("full", "none", locale="fr")}}';
        $expectedValues[] = Twig::render($formatFull, ['datevalue' => $now]);
        $expectedValues[] = '1';
        $expectedValues[] = 'testunit1';
        $mink->assertEquals(implode(', ', $expectedValues), $concatAll);

        $archiveKeywords = $mink->domGetValues('ns:Archive/ns:ContentDescription//ns:KeywordContent');
        $mink->assertEquals(['testunit1', 'testunit2'], $archiveKeywords);

        $archiveObjectKeywords = $mink->domGetValues('ns:Archive/ns:ArchiveObject[1]//ns:KeywordContent');
        $expectedValues = ['testunit1, testunit2'];
        $expectedValues[] = 'testunit1, testunit2';
        $expectedValues[] = 'text_multiple_1, text_multiple_2';
        $mink->assertEquals($expectedValues, $archiveObjectKeywords);

        // documents du noeud archive
        $filenames = $mink->domGetValues('ns:Archive/ns:Document//ns:Attachment/@filename');
        sort($filenames);
        $mink->assertEquals(['ua/test1.txt', 'ua/test2.txt', 'ua/test3.txt'], $filenames);

        // documents du noeud tree_root
        $xpath = 'ns:Archive/ns:ArchiveObject[2]/ns:ArchiveObject/ns:Document//ns:Attachment/@filename';
        $filenames = $mink->domGetValues($xpath);
        sort($filenames);
        $expectedFilenames = ['ua/tree_root/dir1/test4.txt'];
        $expectedFilenames[] = 'ua/tree_root/dir1/test5.txt';
        $expectedFilenames[] = 'ua/tree_root/dir2/test6.txt';
        $mink->assertEquals($expectedFilenames, $filenames);

        // valeurs de la section répétable 1
        $concatAll = $mink->domGetValue('ns:Archive/ns:ArchiveObject[3]/ns:ArchiveObject//ns:Description');
        $expectedValues = ['Description: text_simple'];
        $expectedValues[] = '6';
        $expectedValues[] = 'test@testsection1.fr';
        $expectedValues[] = 'textarea_section1_1';
        $expectedValues[] = 'testunit1';
        $formatShort = '{{datevalue|format_datetime("short", "none", locale="fr")}}';
        $expectedValues[] = Twig::render($formatShort, ['datevalue' => $now]);
        $formatFull = '{{datevalue|format_datetime("full", "none", locale="fr")}}';
        $expectedValues[] = Twig::render($formatFull, ['datevalue' => $now]);
        $expectedValues[] = '1';
        $expectedValues[] = 'testunit2';
        $mink->assertEquals(implode(', ', $expectedValues), $concatAll);

        $archiveKeywords = $mink->domGetValues('ns:Archive/ns:ContentDescription//ns:KeywordContent');
        $mink->assertEquals(['testunit1', 'testunit2'], $archiveKeywords);

        $archiveObjectKeywords = $mink->domGetValues('ns:Archive/ns:ArchiveObject[1]//ns:KeywordContent');
        $expectedValues = ['testunit1, testunit2'];
        $expectedValues[] = 'testunit1, testunit2';
        $expectedValues[] = 'text_multiple_1, text_multiple_2';
        $mink->assertEquals($expectedValues, $archiveObjectKeywords);

        // documents de la racine de section répétable 1
        $xpath = 'ns:Archive/ns:ArchiveObject[3]/ns:Document//ns:Attachment/@filename';
        $filenames = $mink->domGetValues($xpath);
        sort($filenames);
        $expectedFilenames = ['ua/repeatable1/test11.txt'];
        $expectedFilenames[] = 'ua/repeatable1/test12.txt';
        $expectedFilenames[] = 'ua/repeatable1/test13.txt';
        $mink->assertEquals($expectedFilenames, $filenames);

        // documents du tree_root de section répétable 1
        $xpath = 'ns:Archive/ns:ArchiveObject[3]/ns:ArchiveObject//ns:Attachment/@filename';
        $filenames = $mink->domGetValues($xpath);
        sort($filenames);
        $expectedFilenames = ['ua/repeatable1/tree_root_repeatable/dir1/test14.txt'];
        $expectedFilenames[] = 'ua/repeatable1/tree_root_repeatable/dir1/test15.txt';
        $expectedFilenames[] = 'ua/repeatable1/tree_root_repeatable/dir2/test16.txt';
        $mink->assertEquals($expectedFilenames, $filenames);

        // documents de la racine de section répétable 2
        $xpath = 'ns:Archive/ns:ArchiveObject[4]/ns:Document//ns:Attachment/@filename';
        $filenames = $mink->domGetValues($xpath);
        sort($filenames);
        $expectedFilenames = ['ua/repeatable2/test21.txt'];
        $expectedFilenames[] = 'ua/repeatable2/test22.txt';
        $expectedFilenames[] = 'ua/repeatable2/test23.txt';
        $mink->assertEquals($expectedFilenames, $filenames);

        // documents du tree_root de section répétable 2
        $xpath = 'ns:Archive/ns:ArchiveObject[4]/ns:ArchiveObject//ns:Attachment/@filename';
        $filenames = $mink->domGetValues($xpath);
        sort($filenames);
        $expectedFilenames = ['ua/repeatable2/tree_root_repeatable/dir1/test24.txt'];
        $expectedFilenames[] = 'ua/repeatable2/tree_root_repeatable/dir1/test25.txt';
        $expectedFilenames[] = 'ua/repeatable2/tree_root_repeatable/dir2/test26.txt';
        $mink->assertEquals($expectedFilenames, $filenames);
    }
}
