<?php

namespace Versae\Test\MinkCase\FormsMenu;

use Cake\ORM\TableRegistry;
use Versae\MinkSuite\MinkCase;

class FormulairesEnPreparationMink extends MinkCase
{
    public function minkAddForm()
    {
        $mink = $this;
        $Forms = TableRegistry::getTableLocator()->get('Forms');
        $form = $Forms->find()->where(['name' => 'mink_form'])->first();
        $form && $Forms->delete($form);
        $form = $Forms->find()->where(['name' => 'mink_form2'])->first();
        $form && $Forms->delete($form);

        $mink->doLogin();
        $mink->doClickMenu(__("Formulaires"), __("Tous les formulaires"));
        $mink->pausable();
        $mink->assertPageTitleIs(__("Tous les formulaires"));

        $mink->doClickButton(__("Ajouter un formulaire"));
        $mink->waitModalOpen();

        // Informations principales
        $mink->assertModalIsOpenned();
        $mink->pausable();
        $mink->setFormFieldValue(__("Nom du formulaire"), 'mink_form');
        $mink->setFormFieldValue(__("Version du SEDA"), 'SEDA version 1.0');
        $mink->setFormFieldValues(
            __("Services versants autorisés à utiliser le formulaire pour les versements"),
            [2]
        );
        $mink->setFormFieldValue(__("Système d'Archivage Electronique (SAE) destinataire"), 1);
        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->pausable();

        // Zones de saisie
        $mink->doClickTab(__("Zones de saisie"));
        $mink->doClickButton(__("Ajouter une section de formulaire"));

        $mink->waitModalOpen();
        $mink->setFormFieldValue(__("Titre de la section"), 'section 1');
        $mink->submitModal();
        $mink->assertResponseSuccess();

        $mink->pausable();
        $mink->doClickButton(__("Ajouter un champ de formulaire"));

        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="file"]');
        $mink->submitModal();

        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), $input = 'input_file_1');
        $mink->submitModal();
        $mink->pausable();

        // Unité d'archives
        $mink->doClickTab(__("Arborescence"));
        $mink->doAcceptAlert();
        $mink->doClickTab(__("Arborescence"));
        $mink->assertResponseSuccess();
        $mink->pausable();
        $mink->prepareRequestChange();
        $mink->doClick('#jstree-btn-add-root_anchor');
        $mink->waitRequestChange();
        $mink->pausable();
        $mink->setFormFieldValue(__("Type d'élément"), 'simple');
        $mink->setFormFieldValue(__("Nom de l'élément"), 'ua');
        $mink->prepareRequestChange();
        $mink->submitModal(); // on enregistre, ça redirige sur l'édition
        $mink->waitRequestChange();
        $mink->prepareRequestChange();
        $mink->submitModal();
        $mink->waitRequestChange();
        $mink->wait(); // chargement jstree

        $mink->pausable();
        $mink->prepareRequestChange();
        $mink->doClick('.jstree-container-ul > li:first-child > ul > li.add-element > a');
        $mink->waitRequestChange();

        $mink->pausable();

        $mink->setFormFieldValue(__("Type d'élément"), 'document');
        $mink->setFormFieldValue(__("Nom de l'élément"), 'document');
        $mink->setFormSelectValueByText(__("Champ de formulaire"), $input . ' - ' . $input);
        $mink->prepareRequestChange();
        $mink->submitModal();
        $mink->waitRequestChange();
        $mink->prepareRequestChange();
        $mink->submitModal();
        $mink->waitRequestChange();

        $mink->pausable();
        $mink->submitModal();
        $mink->assertResponseSuccess();

        $mink->assertTableContains('#forms-index-editing-table', 'mink_form');
    }

    public function minkDuplicateForm()
    {
        $mink = $this;

        $mink->assertPageTitleIs(__("Tous les formulaires"));
        $mink->doClickAction(__("Dupliquer {0}", 'mink_form'));
        $mink->pausable();
        $mink->doAcceptAlert();
        $mink->waitModalOpen();
        $mink->assertResponseSuccess();

        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Nom du formulaire"), 'mink_form2');
        $mink->submitModal();
        $mink->assertResponseSuccess();

        $mink->assertTableContains('#forms-index-editing-table', 'mink_form2');
    }

    public function minkTestForm()
    {
        $mink = $this;

        $mink->assertPageTitleIs(__("Tous les formulaires"));
        $mink->doClickAction(__("Tester {0}", 'mink_form'));
        $mink->pausable();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        file_put_contents($file = MinkCase::getTmpDir() . DS . 'mink.txt', 'test');
        $mink->setFormFieldValue("input_file_1", $file);
        $mink->waitUploadComplete();
        $mink->wait();
        $mink->submitModal();
        $this->waitModalOpen(400);

        $mink->assertElementContains('h3', __("Transfert généré"));
        $mink->doClickButton(__("Annuler"));
        $mink->wait(); // animation de fermeture
    }

    public function minkDeleteForm()
    {
        $mink = $this;

        $mink->assertPageTitleIs(__("Tous les formulaires"));
        $mink->doClickAction(__("Supprimer {0}", 'mink_form'));
        $mink->pausable();
        $mink->doAcceptAlert();
        $mink->waitAjaxComplete();
        $mink->pausable();

        $Forms = TableRegistry::getTableLocator()->get('Forms');
        $count = $Forms->find()->where(['name' => 'mink_form'])->count();
        $this->assertEquals(0, $count);
    }
}
