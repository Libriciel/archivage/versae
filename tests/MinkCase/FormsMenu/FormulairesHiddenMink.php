<?php

namespace Versae\Test\MinkCase\FormsMenu;

use Versae\MinkSuite\MinkCase;

class FormulairesHiddenMink extends MinkCase
{
    public function minkAddForm()
    {
        $mink = $this;
        $mink->databaseDeleteIfExists('Forms', ['name' => 'mink_form_hidden']);

        $mink->doLogin();
        $mink->doClickMenu(__("Formulaires"), __("Tous les formulaires"));
        $mink->pausable();
        $mink->assertPageTitleIs(__("Tous les formulaires"));

        $mink->doClickButton(__("Ajouter un formulaire"));
        $mink->waitModalOpen();

        // Informations principales
        $mink->assertModalIsOpenned();
        $mink->pausable();
        $mink->setFormFieldValue(__("Nom du formulaire"), 'mink_form_hidden');
        $mink->setFormFieldValue(__("Version du SEDA"), 'SEDA version 2.2');
        $mink->setFormFieldValues(
            __("Services versants autorisés à utiliser le formulaire pour les versements"),
            [2]
        );
        $mink->setFormFieldValue(__("Système d'Archivage Electronique (SAE) destinataire"), 1);
        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->pausable();

        // Zones de saisie
        $mink->doClickTab(__("Zones de saisie"));
        $mink->doClickButton(__("Ajouter une section de formulaire"));

        $mink->waitModalOpen();
        $mink->setFormFieldValue(__("Titre de la section"), 'section 1');
        $mink->submitModal();
        $mink->assertResponseSuccess();

        // Sujet des conditions en texte simple
        $mink->pausable();
        $mink->doClickButton(__("Ajouter un champ de formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'text_simple');
        $mink->submitModal();
        $mink->pausable();

        $mink->doClickButton(__("Ajouter une section de formulaire"));
        $mink->waitModalOpen();
        $mink->setFormFieldValue(__("Titre de la section"), 'section 2');
        $mink->submitModal();
        $mink->assertResponseSuccess();

        $mink->submitModal();
    }

    public function minkDisplayOnTextSimple()
    {
        $mink = $this;

        $mink->assertPageTitleIs(__("Tous les formulaires"));
        $mink->doClickAction(__("Modifier {0}", 'mink_form_hidden'));
        $mink->waitModalOpen();

        // Informations principales
        $mink->assertModalIsOpenned();
        $mink->pausable();

        $mink->doClickTab(__("Zones de saisie"));

        // Champs à affichage conditionnels

        // Champ texte simple non vide
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_text_simple_not_empty');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'text_simple');
        $mink->submitModal();
        $mink->pausable();

        // Champ texte simple vide
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_text_simple_empty');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'text_simple');
        $mink->setFormFieldValue(__("Le champ"), 'standard_empty');
        $mink->submitModal();
        $mink->pausable();

        // Champ texte simple value
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_text_simple_value');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'text_simple');
        // setFormFieldValue ne déclanche pas l'event change sur un radio
        $mink->doClick('#add-input2-cond-display-field-standard_value');
        $mink->setFormFieldValue(__("La valeur suivante"), 'test');
        $mink->submitModal();
        $mink->pausable();

        // Champ texte simple non vide inversé
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_text_simple_not_empty_inverted');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'text_simple');
        $mink->setFormFieldValue(__("Sens"), 'hide_if');
        $mink->submitModal();
        $mink->pausable();

        // Champ texte simple vide inversé
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_text_simple_empty_inverted');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'text_simple');
        $mink->setFormFieldValue(__("Sens"), 'hide_if');
        $mink->setFormFieldValue(__("Le champ"), 'standard_empty');
        $mink->submitModal();
        $mink->pausable();

        // Champ texte simple value inversé
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_text_simple_value_inverted');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'text_simple');
        $mink->setFormFieldValue(__("Sens"), 'hide_if');
        // setFormFieldValue ne déclanche pas l'event change sur un radio
        $mink->doClick('#add-input2-cond-display-field-standard_value');
        $mink->setFormFieldValue(__("La valeur suivante"), 'test');
        $mink->submitModal();
        $mink->pausable();

        $mink->doClickButton(__("Prévisualiser le formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();

        $form = $mink->driver->find($mink->cssToXpath('#add-form-preview-form'))[0];
        $targetField = $form->find('css', '[name="inputs[text_simple]"]');
        $inputEmpty = $form->find('css', '[name="inputs[display_if_text_simple_empty]"]');
        $inputNotEmpty = $form->find('css', '[name="inputs[display_if_text_simple_not_empty]"]');
        $inputValue = $form->find('css', '[name="inputs[display_if_text_simple_value]"]');
        $inputEmptyInverted = $form->find('css', '[name="inputs[display_if_text_simple_empty_inverted]"]');
        $inputNotEmptyInverted = $form->find('css', '[name="inputs[display_if_text_simple_not_empty_inverted]"]');
        $inputValueInverted = $form->find('css', '[name="inputs[display_if_text_simple_value_inverted]"]');

        // text simple est vide, les champs suivants doivent donc être visible:
        $mink->assertTrue($inputEmpty->isVisible());
        $mink->assertTrue($inputNotEmptyInverted->isVisible());
        $mink->assertTrue($inputValueInverted->isVisible());
        // ceux-ci doivent ne pas être visible
        $mink->assertTrue(!$inputNotEmpty->isVisible());
        $mink->assertTrue(!$inputValue->isVisible());
        $mink->assertTrue(!$inputEmptyInverted->isVisible());

        // test avec text simple non vide
        $targetField->setValue('mink');
        $mink->wait(50);
        $mink->assertTrue($inputNotEmpty->isVisible());
        $mink->assertTrue($inputEmptyInverted->isVisible());
        $mink->assertTrue($inputValueInverted->isVisible());
        // ceux-ci doivent ne pas être visible
        $mink->assertTrue(!$inputEmpty->isVisible());
        $mink->assertTrue(!$inputNotEmptyInverted->isVisible());
        $mink->assertTrue(!$inputValue->isVisible());

        // test avec text simple avec valeur cible
        $targetField->setValue('test');
        $mink->wait(50);
        $mink->assertTrue($inputNotEmpty->isVisible());
        $mink->assertTrue($inputEmptyInverted->isVisible());
        $mink->assertTrue($inputValue->isVisible());
        // ceux-ci doivent ne pas être visible
        $mink->assertTrue(!$inputEmpty->isVisible());
        $mink->assertTrue(!$inputNotEmptyInverted->isVisible());
        $mink->assertTrue(!$inputValueInverted->isVisible());

        $mink->closeModal();
        $mink->closeModal();
    }

    public function minkDisplayOnTextMultiple()
    {
        $mink = $this;

        $mink->assertPageTitleIs(__("Tous les formulaires"));
        $mink->doClickAction(__("Modifier {0}", 'mink_form_hidden'));
        $mink->waitModalOpen();

        // Informations principales
        $mink->assertModalIsOpenned();
        $mink->pausable();

        $mink->doClickTab(__("Zones de saisie"));

        // on transforme le text_simple en text_multiple
        $mink->doClick('[title="' . __("Modifier {0}", __("le champ de formulaire")) . '"]');
        $mink->waitModalOpen();
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'text_multiple');
        $mink->setFormFieldValue(__("Champ à valeur multiple"), '1');
        $mink->submitModal();
        $mink->pausable();

        // Champs à affichage conditionnels

        // Champ texte simple non vide
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_text_multiple_not_empty');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'text_multiple');
        $mink->submitModal();
        $mink->pausable();

        // Champ texte simple vide
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_text_multiple_empty');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'text_multiple');
        $mink->setFormFieldValue(__("Le champ"), 'standard_empty');
        $mink->submitModal();
        $mink->pausable();

        // Champ texte simple value
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_text_multiple_value');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'text_multiple');
        // setFormFieldValue ne déclanche pas l'event change sur un radio
        $mink->doClick('#add-input2-cond-display-field-standard_value');
        $mink->setFormFieldValue(__("La valeur suivante"), 'test');
        $mink->submitModal();
        $mink->pausable();

        // Champ texte simple non vide inversé
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_text_multiple_not_empty_inverted');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'text_multiple');
        $mink->setFormFieldValue(__("Sens"), 'hide_if');
        $mink->submitModal();
        $mink->pausable();

        // Champ texte simple vide inversé
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_text_multiple_empty_inverted');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'text_multiple');
        $mink->setFormFieldValue(__("Sens"), 'hide_if');
        $mink->setFormFieldValue(__("Le champ"), 'standard_empty');
        $mink->submitModal();
        $mink->pausable();

        // Champ texte simple value inversé
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_text_multiple_value_inverted');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'text_multiple');
        $mink->setFormFieldValue(__("Sens"), 'hide_if');
        // setFormFieldValue ne déclanche pas l'event change sur un radio
        $mink->doClick('#add-input2-cond-display-field-standard_value');
        $mink->setFormFieldValue(__("La valeur suivante"), 'test');
        $mink->submitModal();
        $mink->pausable();

        $mink->doClickButton(__("Prévisualiser le formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();

        $form = $mink->driver->find($mink->cssToXpath('#add-form-preview-form'))[0];
        $targetField = $form->find('css', '[name="inputs[text_multiple][0]"]');
        $inputEmpty = $form->find('css', '[name="inputs[display_if_text_multiple_empty]"]');
        $inputNotEmpty = $form->find('css', '[name="inputs[display_if_text_multiple_not_empty]"]');
        $inputValue = $form->find('css', '[name="inputs[display_if_text_multiple_value]"]');
        $inputEmptyInverted = $form->find('css', '[name="inputs[display_if_text_multiple_empty_inverted]"]');
        $inputNotEmptyInverted = $form->find('css', '[name="inputs[display_if_text_multiple_not_empty_inverted]"]');
        $inputValueInverted = $form->find('css', '[name="inputs[display_if_text_multiple_value_inverted]"]');

        // text multiple est vide, les champs suivants doivent donc être visible:
        $mink->assertTrue($inputEmpty->isVisible());
        $mink->assertTrue($inputNotEmptyInverted->isVisible());
        $mink->assertTrue($inputValueInverted->isVisible());
        // ceux-ci doivent ne pas être visible
        $mink->assertTrue(!$inputNotEmpty->isVisible());
        $mink->assertTrue(!$inputValue->isVisible());
        $mink->assertTrue(!$inputEmptyInverted->isVisible());

        // test avec text multiple non vide
        $targetField->setValue('mink');
        $mink->wait(50);
        $mink->assertTrue($inputNotEmpty->isVisible());
        $mink->assertTrue($inputEmptyInverted->isVisible());
        $mink->assertTrue($inputValueInverted->isVisible());
        // ceux-ci doivent ne pas être visible
        $mink->assertTrue(!$inputEmpty->isVisible());
        $mink->assertTrue(!$inputNotEmptyInverted->isVisible());
        $mink->assertTrue(!$inputValue->isVisible());

        // test avec text multiple avec valeur cible
        $mink->doClickButton(__("Ajouter une valeur ''{0}''", 'text_multiple'));
        $targetField2 = $form->find('css', '[name="inputs[text_multiple][1]"]');
        $targetField2->setValue('test');
        $mink->assertTrue($inputNotEmpty->isVisible());
        $mink->assertTrue($inputEmptyInverted->isVisible());
        $mink->assertTrue($inputValue->isVisible());
        // ceux-ci doivent ne pas être visible
        $mink->assertTrue(!$inputEmpty->isVisible());
        $mink->assertTrue(!$inputNotEmptyInverted->isVisible());
        $mink->assertTrue(!$inputValueInverted->isVisible());

        $mink->closeModal();
        $mink->closeModal();
    }

    public function minkDisplayOnSelect()
    {
        $mink = $this;

        $mink->assertPageTitleIs(__("Tous les formulaires"));
        $mink->doClickAction(__("Modifier {0}", 'mink_form_hidden'));
        $mink->waitModalOpen();

        // Informations principales
        $mink->assertModalIsOpenned();
        $mink->pausable();

        $mink->doClickTab(__("Zones de saisie"));

        // on transforme le select_simple en select
        $mink->doClick('[title="' . __("Supprimer {0}", __("le champ de formulaire")) . '"]');
        $mink->doAcceptAlert();
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="0"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="select"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'select_simple');
        $mink->doClickTab('Options');
        // setFormFieldValue provoque une exception à cause du confirm()
        $mink->findVisibleFormElement(__("Utiliser une liste de mot clés existante"))->setValue('2');
        $mink->doAcceptAlert();
        $mink->findFirst($mink->cssToXpath('#value-add-option'))->setValue('testunit3');
        $mink->findFirst($mink->cssToXpath('#text-add-option'))->setValue('testunit 3');
        $mink->doClickButton(__("Ajouter une option"));
        $mink->submitModal();
        $mink->pausable();

        // Champs à affichage conditionnels

        // Champ texte simple non vide
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_select_simple_not_empty');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'select_simple');
        $mink->submitModal();
        $mink->pausable();

        // Champ texte simple vide
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_select_simple_empty');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'select_simple');
        $mink->setFormFieldValue(__("Le champ"), 'select_empty');
        $mink->submitModal();
        $mink->pausable();

        // Champ texte simple value
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_select_simple_value');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'select_simple');
        // setFormFieldValue ne déclanche pas l'event change sur un radio
        $mink->doClick('#add-input2-cond-display-field-select_value');
        $mink->setFormFieldValues(__("Au moins l'une des valeurs suivantes"), ['testunit1', 'testunit2']);
        $mink->submitModal();
        $mink->pausable();

        // Champ texte simple non vide inversé
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_select_simple_not_empty_inverted');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'select_simple');
        $mink->setFormFieldValue(__("Sens"), 'hide_if');
        $mink->submitModal();
        $mink->pausable();

        // Champ texte simple vide inversé
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_select_simple_empty_inverted');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'select_simple');
        $mink->setFormFieldValue(__("Sens"), 'hide_if');
        $mink->setFormFieldValue(__("Le champ"), 'select_empty');
        $mink->submitModal();
        $mink->pausable();

        // Champ texte simple value inversé
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_select_simple_value_inverted');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'select_simple');
        $mink->setFormFieldValue(__("Sens"), 'hide_if');
        // setFormFieldValue ne déclanche pas l'event change sur un radio
        $mink->doClick('#add-input2-cond-display-field-select_value');
        $mink->setFormFieldValues(__("Au moins l'une des valeurs suivantes"), ['testunit1', 'testunit2']);
        $mink->submitModal();
        $mink->pausable();

        $mink->doClickButton(__("Prévisualiser le formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();

        $form = $mink->driver->find($mink->cssToXpath('#add-form-preview-form'))[0];
        $targetField = $form->find('css', '[name="inputs[select_simple]"]');
        $inputEmpty = $form->find('css', '[name="inputs[display_if_select_simple_empty]"]');
        $inputNotEmpty = $form->find('css', '[name="inputs[display_if_select_simple_not_empty]"]');
        $inputValue = $form->find('css', '[name="inputs[display_if_select_simple_value]"]');
        $inputEmptyInverted = $form->find('css', '[name="inputs[display_if_select_simple_empty_inverted]"]');
        $inputNotEmptyInverted = $form->find('css', '[name="inputs[display_if_select_simple_not_empty_inverted]"]');
        $inputValueInverted = $form->find('css', '[name="inputs[display_if_select_simple_value_inverted]"]');

        // select simple est vide, les champs suivants doivent donc être visible:
        $mink->assertTrue($inputEmpty->isVisible());
        $mink->assertTrue($inputNotEmptyInverted->isVisible());
        $mink->assertTrue($inputValueInverted->isVisible());
        // ceux-ci doivent ne pas être visible
        $mink->assertTrue(!$inputNotEmpty->isVisible());
        $mink->assertTrue(!$inputValue->isVisible());
        $mink->assertTrue(!$inputEmptyInverted->isVisible());

        // test avec select simple non vide
        $targetField->setValue('testunit3');
        $mink->wait(50);
        $mink->assertTrue($inputNotEmpty->isVisible());
        $mink->assertTrue($inputEmptyInverted->isVisible());
        $mink->assertTrue($inputValueInverted->isVisible());
        // ceux-ci doivent ne pas être visible
        $mink->assertTrue(!$inputEmpty->isVisible());
        $mink->assertTrue(!$inputNotEmptyInverted->isVisible());
        $mink->assertTrue(!$inputValue->isVisible());

        // test avec select simple avec valeur cible
        $targetField->setValue('testunit1');
        $mink->wait(50);
        $mink->assertTrue($inputNotEmpty->isVisible());
        $mink->assertTrue($inputEmptyInverted->isVisible());
        $mink->assertTrue($inputValue->isVisible());
        // ceux-ci doivent ne pas être visible
        $mink->assertTrue(!$inputEmpty->isVisible());
        $mink->assertTrue(!$inputNotEmptyInverted->isVisible());
        $mink->assertTrue(!$inputValueInverted->isVisible());

        $mink->closeModal();
        $mink->closeModal();
    }

    public function minkDisplayOnSelectMultiple()
    {
        $mink = $this;

        $mink->assertPageTitleIs(__("Tous les formulaires"));
        $mink->doClickAction(__("Modifier {0}", 'mink_form_hidden'));
        $mink->waitModalOpen();

        // Informations principales
        $mink->assertModalIsOpenned();
        $mink->pausable();

        $mink->doClickTab(__("Zones de saisie"));

        // on transforme le select_simple en select multiple
        $mink->doClick('[title="' . __("Supprimer {0}", __("le champ de formulaire")) . '"]');
        $mink->doAcceptAlert();
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="0"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="select"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'select_multiple');
        $mink->setFormFieldValue(__("Champ à valeur multiple"), '1');
        $mink->doClickTab('Options');
        // setFormFieldValue provoque une exception à cause du confirm()
        $mink->findVisibleFormElement(__("Utiliser une liste de mot clés existante"))->setValue('2');
        $mink->doAcceptAlert();
        $mink->findFirst($mink->cssToXpath('#value-add-option'))->setValue('testunit3');
        $mink->findFirst($mink->cssToXpath('#text-add-option'))->setValue('testunit 3');
        $mink->doClickButton(__("Ajouter une option"));
        $mink->submitModal();
        $mink->pausable();

        // Champs à affichage conditionnels

        // Champ texte simple non vide
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_select_multiple_not_empty');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'select_multiple');
        $mink->submitModal();
        $mink->pausable();

        // Champ texte simple vide
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_select_multiple_empty');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'select_multiple');
        $mink->setFormFieldValue(__("Le champ"), 'select_multiple_empty');
        $mink->submitModal();
        $mink->pausable();

        // Champ texte simple value
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_select_multiple_value');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'select_multiple');
        // setFormFieldValue ne déclanche pas l'event change sur un radio
        $mink->doClick('#add-input2-cond-display-field-select_multiple_value');
        $mink->setFormFieldValues(__("Au moins l'une des valeurs suivantes"), ['testunit1', 'testunit2']);
        $mink->submitModal();
        $mink->pausable();

        // Champ texte simple non vide inversé
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_select_multiple_not_empty_inverted');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'select_multiple');
        $mink->setFormFieldValue(__("Sens"), 'hide_if');
        $mink->submitModal();
        $mink->pausable();

        // Champ texte simple vide inversé
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_select_multiple_empty_inverted');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'select_multiple');
        $mink->setFormFieldValue(__("Sens"), 'hide_if');
        $mink->setFormFieldValue(__("Le champ"), 'select_multiple_empty');
        $mink->submitModal();
        $mink->pausable();

        // Champ texte simple value inversé
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_select_multiple_value_inverted');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'select_multiple');
        $mink->setFormFieldValue(__("Sens"), 'hide_if');
        // setFormFieldValue ne déclanche pas l'event change sur un radio
        $mink->doClick('#add-input2-cond-display-field-select_multiple_value');
        $mink->setFormFieldValues(__("Au moins l'une des valeurs suivantes"), ['testunit1', 'testunit2']);
        $mink->submitModal();
        $mink->pausable();

        $mink->doClickButton(__("Prévisualiser le formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();

        $form = $mink->driver->find($mink->cssToXpath('#add-form-preview-form'))[0];
        $targetField = $form->find('css', '[name="inputs[select_multiple][]"]');
        $inputEmpty = $form->find('css', '[name="inputs[display_if_select_multiple_empty]"]');
        $inputNotEmpty = $form->find('css', '[name="inputs[display_if_select_multiple_not_empty]"]');
        $inputValue = $form->find('css', '[name="inputs[display_if_select_multiple_value]"]');
        $inputEmptyInverted = $form->find('css', '[name="inputs[display_if_select_multiple_empty_inverted]"]');
        $inputNotEmptyInverted = $form->find('css', '[name="inputs[display_if_select_multiple_not_empty_inverted]"]');
        $inputValueInverted = $form->find('css', '[name="inputs[display_if_select_multiple_value_inverted]"]');

        // select multiple est vide, les champs suivants doivent donc être visible:
        $mink->assertTrue($inputEmpty->isVisible());
        $mink->assertTrue($inputNotEmptyInverted->isVisible());
        $mink->assertTrue($inputValueInverted->isVisible());
        // ceux-ci doivent ne pas être visible
        $mink->assertTrue(!$inputNotEmpty->isVisible());
        $mink->assertTrue(!$inputValue->isVisible());
        $mink->assertTrue(!$inputEmptyInverted->isVisible());

        // test avec select multiple non vide
        $targetField->setValue(['testunit3']);
        $mink->wait(50);
        $mink->assertTrue($inputNotEmpty->isVisible());
        $mink->assertTrue($inputEmptyInverted->isVisible());
        $mink->assertTrue($inputValueInverted->isVisible());
        // ceux-ci doivent ne pas être visible
        $mink->assertTrue(!$inputEmpty->isVisible());
        $mink->assertTrue(!$inputNotEmptyInverted->isVisible());
        $mink->assertTrue(!$inputValue->isVisible());

        // test avec select multiple avec valeur cible
        $targetField->setValue(['testunit1']);
        $mink->wait(50);
        $mink->assertTrue($inputNotEmpty->isVisible());
        $mink->assertTrue($inputEmptyInverted->isVisible());
        $mink->assertTrue($inputValue->isVisible());
        // ceux-ci doivent ne pas être visible
        $mink->assertTrue(!$inputEmpty->isVisible());
        $mink->assertTrue(!$inputNotEmptyInverted->isVisible());
        $mink->assertTrue(!$inputValueInverted->isVisible());

        $mink->closeModal();
        $mink->closeModal();
    }

    public function minkDisplayOnCheckbox()
    {
        $mink = $this;

        $mink->assertPageTitleIs(__("Tous les formulaires"));
        $mink->doClickAction(__("Modifier {0}", 'mink_form_hidden'));
        $mink->waitModalOpen();

        // Informations principales
        $mink->assertModalIsOpenned();
        $mink->pausable();

        $mink->doClickTab(__("Zones de saisie"));

        // on transforme le select_simple en checkbox
        $mink->doClick('[title="' . __("Supprimer {0}", __("le champ de formulaire")) . '"]');
        $mink->doAcceptAlert();
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="0"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="checkbox"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'checkbox');
        $mink->submitModal();
        $mink->pausable();

        // Champs à affichage conditionnels

        // Champ checkbox non vide
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_checkbox_not_empty');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'checkbox');
        $mink->submitModal();
        $mink->pausable();

        // Champ checkbox vide
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_checkbox_empty');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'checkbox');
        $mink->setFormFieldValue(__("Le champ"), 'checkbox_empty');
        $mink->submitModal();
        $mink->pausable();

        // Champ checkbox non vide inversé
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_checkbox_not_empty_inverted');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'checkbox');
        $mink->setFormFieldValue(__("Sens"), 'hide_if');
        $mink->submitModal();
        $mink->pausable();

        // Champ checkbox vide inversé
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_checkbox_empty_inverted');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'checkbox');
        $mink->setFormFieldValue(__("Sens"), 'hide_if');
        $mink->setFormFieldValue(__("Le champ"), 'checkbox_empty');
        $mink->submitModal();
        $mink->pausable();

        $mink->doClickButton(__("Prévisualiser le formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();

        $form = $mink->driver->find($mink->cssToXpath('#add-form-preview-form'))[0];
        $targetField = $form->find('css', '[type="checkbox"][name="inputs[checkbox]"]');
        $inputEmpty = $form->find('css', '[name="inputs[display_if_checkbox_empty]"]');
        $inputNotEmpty = $form->find('css', '[name="inputs[display_if_checkbox_not_empty]"]');
        $inputEmptyInverted = $form->find('css', '[name="inputs[display_if_checkbox_empty_inverted]"]');
        $inputNotEmptyInverted = $form->find('css', '[name="inputs[display_if_checkbox_not_empty_inverted]"]');

        // checkbox est vide, les champs suivants doivent donc être visible:
        $mink->assertTrue($inputEmpty->isVisible());
        $mink->assertTrue($inputNotEmptyInverted->isVisible());
        // ceux-ci doivent ne pas être visible
        $mink->assertTrue(!$inputNotEmpty->isVisible());
        $mink->assertTrue(!$inputEmptyInverted->isVisible());

        // test avec checkbox non vide
        $targetField->check();
        $mink->assertTrue($inputNotEmpty->isVisible());
        $mink->assertTrue($inputEmptyInverted->isVisible());
        // ceux-ci doivent ne pas être visible
        $mink->assertTrue(!$inputEmpty->isVisible());
        $mink->assertTrue(!$inputNotEmptyInverted->isVisible());

        $mink->closeModal();
        $mink->closeModal();
    }

    public function minkDisplayOnMiscellaneous()
    {
        $mink = $this;

        $mink->assertPageTitleIs(__("Tous les formulaires"));
        $mink->doClickAction(__("Modifier {0}", 'mink_form_hidden'));
        $mink->waitModalOpen();

        // Informations principales
        $mink->assertModalIsOpenned();
        $mink->pausable();

        $mink->doClickTab(__("Zones de saisie"));

        // on transforme le select_simple en radio
        $mink->doClick('[title="' . __("Supprimer {0}", __("le champ de formulaire")) . '"]');
        $mink->doAcceptAlert();
        $mink->wait(); // besoin de temps pour mettre à jours les variables js
        $mink->findFirst($mink->cssToXpath('[data-index="0"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="radio"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'radio');
        $mink->doClickTab('Options');
        $mink->findFirst($mink->cssToXpath('#value-add-option'))->setValue('section2');
        $mink->findFirst($mink->cssToXpath('#text-add-option'))->setValue('Section 2');
        $mink->doClickButton(__("Ajouter une option"));
        $mink->findFirst($mink->cssToXpath('#value-add-option'))->setValue('section3');
        $mink->findFirst($mink->cssToXpath('#text-add-option'))->setValue('Section répétable');
        $mink->doClickButton(__("Ajouter une option"));
        $mink->submitModal();
        $mink->pausable();

        // Champs à affichage conditionnels
        // edition section 2
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.edit'))->click();
        $mink->waitModalOpen();
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'radio');
        $mink->doClick('#form-edit-fieldset-cond-display-field-select_value');
        $mink->setFormFieldValues(__("Au moins l'une des valeurs suivantes"), ['section2']);
        $mink->submitModal();
        $mink->pausable();

        // assertion sur icone oeil sur section
        $fieldset = $mink->findFirst($mink->cssToXpath('#edit-form-fieldsets fieldset[data-index="1"]'));
        $mink->assertTrue(str_contains($fieldset->getAttribute('class'), 'hiddable-fieldset'));

        // Champ checkbox "afficher champs à l'interieur d'une section caché"
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="0"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="checkbox"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), "afficher champs à l'interieur des sections cachés");
        $mink->setFormFieldValue(__("Attribut `name` du champ (identifiant twig)"), 'hide');
        $mink->submitModal();
        $mink->pausable();

        // Champ checkbox multiple
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="0"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="multi_checkbox"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), "checkbox multiple");
        $mink->doClickTab('Options');
        $mink->findFirst($mink->cssToXpath('#value-add-option'))->setValue('foo');
        $mink->doClickButton(__("Ajouter une option"));
        $mink->findFirst($mink->cssToXpath('#value-add-option'))->setValue('bar');
        $mink->doClickButton(__("Ajouter une option"));
        $mink->submitModal();
        $mink->pausable();

        // champ caché foo
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="0"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->setFormFieldValue(__("Nom du champ affiché"), "foo");
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'checkbox_multiple');
        $mink->doClick('#add-input2-cond-display-field-select_multiple_value');
        $mink->setFormFieldValues(__("Au moins l'une des valeurs suivantes"), ['foo']);
        $mink->submitModal();
        $mink->pausable();

        // champ caché bar
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="0"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->setFormFieldValue(__("Nom du champ affiché"), "bar");
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'checkbox_multiple');
        $mink->doClick('#add-input2-cond-display-field-select_multiple_value');
        $mink->setFormFieldValues(__("Au moins l'une des valeurs suivantes"), ['bar']);
        $mink->submitModal();
        $mink->pausable();

        // champ caché à l'interieur d'une section caché
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'hidden_inside_hidden_section');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'hide');
        $mink->submitModal();
        $mink->pausable();

        // ajout d'une section répétable
        $mink->doClickButton(__("Ajouter une section de formulaire"));

        $mink->waitModalOpen();
        $mink->setFormFieldValue(__("Titre de la section"), 'section répétable');
        $mink->setFormFieldValue(__("Section répétable"), '1');
        $mink->setFormFieldValue(__("Au moins une fois"), '1');
        $mink->setFormFieldValue(__("Jusqu'à X fois"), '3');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'radio');
        $mink->doClick('#form-add-fieldset-cond-display-field-select_value');
        $mink->setFormFieldValues(__("Au moins l'une des valeurs suivantes"), ['section3']);
        $mink->submitModal();
        $mink->pausable();

        // Champ texte simple dans section répétable (juste pour garnir la section)
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="2"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'text_simple');
        $mink->submitModal();
        $mink->pausable();

        $mink->doClickButton(__("Prévisualiser le formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();

        $form = $mink->driver->find($mink->cssToXpath('#add-form-preview-form'))[0];
        $inputRadioSection2 = $form->find('css', '#preview-form-inputs-radio-section2');
        $inputRadioSection3 = $form->find('css', '#preview-form-inputs-radio-section3');
        $inputHide = $form->find('css', '#preview-form-inputs-hide');
        $inputCheckboxFoo = $form->find('css', '#preview-form-inputs-checkbox-multiple-foo');
        $inputCheckboxBar = $form->find('css', '#preview-form-inputs-checkbox-multiple-bar');
        $inputFoo = $form->find('css', '[name="inputs[foo]"]');
        $inputBar = $form->find('css', '[name="inputs[bar]"]');
        $section2 = $form->find('css', '#preview-form-fielset1');
        $inputInsideHidden = $form->find('css', '#preview-form-inputs-hidden-inside-hidden-section');
        $section3 = $form->find('css', '#preview-form-fielset2-0');

        // etat initial
        $mink->assertTrue($inputFoo->isVisible() === false);
        $mink->assertTrue($inputBar->isVisible() === false);
        $mink->assertTrue($section2->isVisible() === false);
        $mink->assertTrue($section3->isVisible() === false);

        // affichage de section2
        $inputRadioSection2->click();
        $mink->assertTrue($section2->isVisible());
        $mink->assertTrue($inputInsideHidden->isVisible() === false);

        // affichage du champ caché dans section caché
        $inputHide->check();
        $mink->assertTrue($inputInsideHidden->isVisible());

        // affichage section répétable
        $inputRadioSection3->click();
        $mink->assertTrue($section3->isVisible());

        // coche foo
        $inputCheckboxFoo->check();
        $mink->assertTrue($inputFoo->isVisible());
        // coche bar
        $inputCheckboxBar->check();
        $mink->assertTrue($inputBar->isVisible());

        $mink->closeModal();
        $mink->closeModal();
    }

    public function minkDisplayOnUploads()
    {
        $mink = $this;

        $mink->assertPageTitleIs(__("Tous les formulaires"));
        $mink->doClickAction(__("Modifier {0}", 'mink_form_hidden'));
        $mink->waitModalOpen();

        // Informations principales
        $mink->assertModalIsOpenned();
        $mink->pausable();

        $mink->doClickTab(__("Zones de saisie"));

        // on transforme le select_simple en radio
        $mink->doClick('[title="' . __("Supprimer {0}", __("le champ de formulaire")) . '"]');
        $mink->doAcceptAlert();
        $mink->wait(); // besoin de temps pour mettre à jours les variables js
        $mink->findFirst($mink->cssToXpath('[data-index="0"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="file"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'file');
        $mink->submitModal();
        $mink->pausable();

        // Champs à affichage conditionnels

        // Champ fichier non vide
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_file_selected');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'file');
        $mink->submitModal();
        $mink->pausable();

        // Champ fichier vide
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_file_not_selected');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'file');
        $mink->setFormFieldValue(__("Un ou des fichier(s)"), 'is_not_selected');
        $mink->submitModal();
        $mink->pausable();

        // Champ fichier non vide inversé
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_file_selected_inverted');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'file');
        $mink->setFormFieldValue(__("Sens"), 'hide_if');
        $mink->submitModal();
        $mink->pausable();

        // Champ fichier vide inversé
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_file_not_selected_inverted');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'file');
        $mink->setFormFieldValue(__("Sens"), 'hide_if');
        $mink->setFormFieldValue(__("Un ou des fichier(s)"), 'is_not_selected');
        $mink->submitModal();
        $mink->pausable();

        $mink->doClickButton(__("Prévisualiser le formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();

        $form = $mink->driver->find($mink->cssToXpath('#add-form-preview-form'))[0];
        $targetField = $form->find('css', '[type="file"]');
        $inputFileSelected = $form->find('css', '[name="inputs[display_if_file_selected]"]');
        $inputFileNotSelected = $form->find('css', '[name="inputs[display_if_file_not_selected]"]');
        $inputFileSelectedInverted = $form->find('css', '[name="inputs[display_if_file_selected_inverted]"]');
        $inputFileNotSelectedInverted = $form->find('css', '[name="inputs[display_if_file_not_selected_inverted]"]');

        // file est vide
        $mink->assertTrue($inputFileSelected->isVisible() === false);
        $mink->assertTrue($inputFileNotSelected->isVisible());
        $mink->assertTrue($inputFileSelectedInverted->isVisible());
        $mink->assertTrue($inputFileNotSelectedInverted->isVisible() === false);

        // test avec file non vide
        $mink->requiredIdName('input[type="file"]');
        $targetField->attachFile($mink->fileCreate());
        $mink->wait();
        $mink->removeRequiredIdName();
        $mink->assertTrue($inputFileSelected->isVisible());
        $mink->assertTrue($inputFileNotSelected->isVisible() === false);
        $mink->assertTrue($inputFileSelectedInverted->isVisible() === false);
        $mink->assertTrue($inputFileNotSelectedInverted->isVisible());

        // delete file
        $buttonDelete = $form->find('css', 'button.delete');
        $buttonDelete->click();
        $mink->doAcceptAlert();
        $mink->wait();
        $mink->assertTrue($inputFileSelected->isVisible() === false);
        $mink->assertTrue($inputFileNotSelected->isVisible());
        $mink->assertTrue($inputFileSelectedInverted->isVisible());
        $mink->assertTrue($inputFileNotSelectedInverted->isVisible() === false);

        $mink->closeModal();
        $mink->closeModal();
    }

    public function minkDisplayOnUploadsMultiple()
    {
        $mink = $this;

        $mink->assertPageTitleIs(__("Tous les formulaires"));
        $mink->doClickAction(__("Modifier {0}", 'mink_form_hidden'));
        $mink->waitModalOpen();

        // Informations principales
        $mink->assertModalIsOpenned();
        $mink->pausable();

        $mink->doClickTab(__("Zones de saisie"));

        // on transforme le select_simple en radio
        $mink->doClick('[title="' . __("Supprimer {0}", __("le champ de formulaire")) . '"]');
        $mink->doAcceptAlert();
        $mink->wait(); // besoin de temps pour mettre à jours les variables js
        $mink->findFirst($mink->cssToXpath('[data-index="0"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="file"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'file');
        $mink->setFormFieldValue(__("Champ à valeur multiple"), '1');
        $mink->submitModal();
        $mink->pausable();

        // Champs à affichage conditionnels

        // Champ fichier non vide
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_file_selected');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'file');
        $mink->submitModal();
        $mink->pausable();

        // Champ fichier vide
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_file_not_selected');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'file');
        $mink->setFormFieldValue(__("Un ou des fichier(s)"), 'is_not_selected');
        $mink->submitModal();
        $mink->pausable();

        // Champ fichier non vide inversé
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_file_selected_inverted');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'file');
        $mink->setFormFieldValue(__("Sens"), 'hide_if');
        $mink->submitModal();
        $mink->pausable();

        // Champ fichier vide inversé
        $mink->pausable();
        $mink->findFirst($mink->cssToXpath('[data-index="1"] button.add-input'))->click();
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->doClick('#table-add-input > tbody > tr > td > label > input[value="text"]');
        $mink->submitModal();
        $mink->waitModalOpen();
        $mink->assertModalStep(2);
        $mink->setFormFieldValue(__("Nom du champ affiché"), 'display_if_file_not_selected_inverted');
        $mink->doClickTab(__("Conditions de masquage"));
        $mink->setFormFieldValue(__("L'affichage est conditionné par le champ suivant"), 'file');
        $mink->setFormFieldValue(__("Sens"), 'hide_if');
        $mink->setFormFieldValue(__("Un ou des fichier(s)"), 'is_not_selected');
        $mink->submitModal();
        $mink->pausable();

        $mink->doClickButton(__("Prévisualiser le formulaire"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();

        $form = $mink->driver->find($mink->cssToXpath('#add-form-preview-form'))[0];
        $targetField = $form->find('css', '[type="file"]');
        $inputFileSelected = $form->find('css', '[name="inputs[display_if_file_selected]"]');
        $inputFileNotSelected = $form->find('css', '[name="inputs[display_if_file_not_selected]"]');
        $inputFileSelectedInverted = $form->find('css', '[name="inputs[display_if_file_selected_inverted]"]');
        $inputFileNotSelectedInverted = $form->find('css', '[name="inputs[display_if_file_not_selected_inverted]"]');

        // file est vide
        $mink->assertTrue($inputFileSelected->isVisible() === false);
        $mink->assertTrue($inputFileNotSelected->isVisible());
        $mink->assertTrue($inputFileSelectedInverted->isVisible());
        $mink->assertTrue($inputFileNotSelectedInverted->isVisible() === false);

        // test avec file non vide
        $mink->requiredIdName('input[type="file"]');
        $targetField->attachFile($mink->fileCreate());
        $targetField->attachFile($mink->fileCreate());
        $mink->wait();
        $mink->removeRequiredIdName();
        $mink->assertTrue($inputFileSelected->isVisible());
        $mink->assertTrue($inputFileNotSelected->isVisible() === false);
        $mink->assertTrue($inputFileSelectedInverted->isVisible() === false);
        $mink->assertTrue($inputFileNotSelectedInverted->isVisible());

        // on décoche les fichiers
        $files = $form->findAll('css', '[name="inputs[file][]"]');
        $files[0]->uncheck();
        $files[1]->uncheck();
        $mink->wait();
        $mink->assertTrue($inputFileSelected->isVisible() === false);
        $mink->assertTrue($inputFileNotSelected->isVisible());
        $mink->assertTrue($inputFileSelectedInverted->isVisible());
        $mink->assertTrue($inputFileNotSelectedInverted->isVisible() === false);

        // on recoche
        $files[0]->check();
        $files[1]->check();
        $mink->wait();
        $mink->assertTrue($inputFileSelected->isVisible());
        $mink->assertTrue($inputFileNotSelected->isVisible() === false);
        $mink->assertTrue($inputFileSelectedInverted->isVisible() === false);
        $mink->assertTrue($inputFileNotSelectedInverted->isVisible());

        // on supprime
        $buttonDeletes = $form->findAll('css', 'button.delete');
        $buttonDeletes[0]->click();
        $mink->doAcceptAlert();
        $buttonDeletes[1]->click();
        $mink->doAcceptAlert();
        $mink->wait();
        $mink->assertTrue($inputFileSelected->isVisible() === false);
        $mink->assertTrue($inputFileNotSelected->isVisible());
        $mink->assertTrue($inputFileSelectedInverted->isVisible());
        $mink->assertTrue($inputFileNotSelectedInverted->isVisible() === false);

        $mink->closeModal();
        $mink->closeModal();
    }
}
