<?php

namespace Versae\Test\MinkCase\Versements;

use Versae\MinkSuite\MinkCase;

class CreerUnVersementMink extends MinkCase
{
    public function minkAddDeposit()
    {
        $mink = $this;
        $mink->databaseGetModel('Deposits')->updateAll(['state' => 'editing'], ['name' => 'mink_deposit']);
        $mink->databaseDeleteIfExists('Deposits', ['name' => 'mink_deposit']);
        $mink->databaseDeleteAll('BeanstalkJobs');

        $mink->doLogin();
        $mink->doClickMenu(__("Versements"), __("Mes versements en cours"));
        $mink->pausable();
        $mink->assertPageTitleIs(__("Mes versements en cours"));

        $mink->doClickButton(__("Créer un versement"));
        $mink->waitModalOpen();

        // Informations principales
        $mink->assertModalIsOpenned();
        $mink->assertModalStep(1);
        $mink->pausable();
        $mink->setFormFieldValue(__("Formulaire"), '9'); // testunit_published v2
        $mink->setFormFieldValue(__("Nom du versement"), 'mink_deposit');
        $mink->prepareRequestChange();
        $mink->submitModal();
        $mink->waitRequestChange();
        $mink->pausable();

        $deposit = $mink->databaseFindFirstOrFail('Deposits', ['name' => 'mink_deposit']);
        $mink->setFormFieldValue('simple_text', 'mink_deposit');
        $mink->setFormFieldValue('simple_file', $mink->fileCreate('test1.txt'));
        $mink->waitUploadComplete();
        // FIXME problème de timing avec beanstalk ?
//        $mink->prepareRequestChange();
//        $mink->submitModal();
//        $mink->waitRequestChange();
//
//        $mink->prepareRequestChange();
//        $mink->waitJobFinish('generate-deposit', 'Deposits', $deposit->id);
//        $mink->waitRequestChange();
//        $mink->assertElementContains('h2', 'mink_deposit');
//        $mink->prepareRequestChange();
//        $mink->submitModal();
//        $mink->waitRequestChange();

        // TODO utiliser la stack asalae dans la CI pour faire la suite
//        $mink->waitState($mink->databaseGetModel('Deposits'), $deposit->id, 'sent');
    }
}
