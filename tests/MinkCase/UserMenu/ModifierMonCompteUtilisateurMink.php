<?php

namespace Versae\Test\MinkCase\UserMenu;

use Versae\MinkSuite\MinkCase;
use Cake\ORM\TableRegistry;

class ModifierMonCompteUtilisateurMink extends MinkCase
{
    public function minkEditUser()
    {
        $mink = $this;
        $Users = TableRegistry::getTableLocator()->get('Users');
        $Users->updateAll(['name' => 'M. Martin Dupont'], ['id' => 1]);

        $mink->doLogin();
        $mink->doClickUserMenu(__("Modifier mon compte utilisateur"));
        $mink->waitModalOpen();
        $mink->setFormFieldValue(
            __("Nom d'utilisateur"),
            'Mink Man'
        );
        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->waitModalClose();

        $mink->assertEquals(
            'Mink Man',
            $Users->get(1)->get('name')
        );

        $mink->doClickUserMenu(__("Modifier mon compte utilisateur"));
        $mink->waitModalOpen();
        $mink->assertFormFieldContains(
            __("Nom d'utilisateur"),
            'Mink Man'
        );
    }
}
