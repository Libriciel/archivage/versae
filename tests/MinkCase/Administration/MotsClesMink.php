<?php

namespace Versae\Test\MinkCase\Administration;

use Versae\MinkSuite\MinkCase;

class MotsClesMink extends MinkCase
{
    /**
     * Ajout d'une liste de mots clés
     * @return void
     * @throws \AsalaeCore\MinkSuite\MinkAssertionFailedException
     * @throws \Behat\Mink\Exception\DriverException
     * @throws \Behat\Mink\Exception\UnsupportedDriverActionException
     */
    public function minkAddKeywordlist()
    {
        $mink = $this;
        $mink->databaseDeleteIfExists('KeywordLists', ['identifier' => 'mink_keywordlist']);
        $mink->databaseDeleteIfExists('KeywordLists', ['identifier' => 'mink_keywordlist2']);

        $mink->doLogin();
        $mink->doClickMenu(__("Administration"), __("Mots clés"));
        $mink->pausable();
        $mink->assertPageTitleIs(__("Mots-clés"));

        $mink->doClickButton(__("Ajouter une liste de mots clés"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->pausable();
        $mink->setFormFieldValue(__("Identifiant"), 'mink_keywordlist');
        $mink->setFormFieldValue(__("Nom"), 'Liste de mots clés mink');
        $mink->setFormFieldValue(__("Description"), 'Ceci est une liste de mots clés mink');
        $mink->submitModal();
        $mink->assertResponseSuccess();

        $mink->assertTableContains('#keyword-lists-index-table', 'Liste de mots clés mink');
        $mink->pausable();
    }

    /**
     * Visualisation d'une liste de mots clés
     * @return void
     * @throws \AsalaeCore\MinkSuite\MinkAssertionFailedException
     * @throws \Behat\Mink\Exception\DriverException
     * @throws \Behat\Mink\Exception\UnsupportedDriverActionException
     */
    public function minkViewKeywordlist()
    {
        $mink = $this;

        $mink->assertPageTitleIs(__("Mots-clés"));
        $mink->doClickAction(__("Visualiser {0}", 'Liste de mots clés mink'));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->pausable();
        $mink->assertViewTableValueContains(__("Identifiant"), "mink_keywordlist");
        $mink->closeModal();
    }

    /**
     * Ajout de mots clés
     * @return void
     * @throws \AsalaeCore\MinkSuite\MinkAssertionFailedException
     * @throws \Behat\Mink\Exception\DriverException
     * @throws \Behat\Mink\Exception\UnsupportedDriverActionException
     */
    public function minkAddKeywords()
    {
        $mink = $this;

        $mink->doClickMenu(__("Administration"), __("Mots clés"));
        $mink->assertPageTitleIs(__("Mots-clés"));
        $mink->doClickAction(__("Gérer les mots-clés de la liste {0}", 'Liste de mots clés mink'));
        $mink->wait();
        $mink->pausable();
        $mink->assertPageTitleIs(__("Mots-clés de la liste: {0}", 'Liste de mots clés mink'));

        $mink->doClickButton(__("Ajouter un mot clé"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->pausable();
        $mink->setFormFieldValue(__("Code"), 'code_mot_cle_mink_1');
        $mink->setFormFieldValue(__("Nom"), 'Nom mot clé mink 1');
        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->wait();
        $mink->assertTableContains('#keywords-index-table', 'Nom mot clé mink 1');
        $mink->pausable();

        $mink->doClickButton(__("Ajouter un ensemble de mots-clés"));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->pausable();
        $mink->driver->find($mink->cssToXpath("#add-multiple-keyword-0-code"))[0]->setValue("code_mot_cle_mink_2");
        $mink->driver->find($mink->cssToXpath("#add-multiple-keyword-0-name"))[0]->setValue("Nom mot clé mink 2");
        $mink->driver->find($mink->cssToXpath("#add-multiple-keyword-1-code"))[0]->setValue("code_mot_cle_mink_3");
        $mink->driver->find($mink->cssToXpath("#add-multiple-keyword-1-name"))[0]->setValue("Nom mot clé mink 3");
        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->wait();
        $mink->assertTableContains('#keywords-index-table', 'Nom mot clé mink 2');
        $mink->assertTableContains('#keywords-index-table', 'Nom mot clé mink 3');
        $mink->pausable();
    }

    /**
     * Visualisation d'un mot clé
     * @return void
     * @throws \AsalaeCore\MinkSuite\MinkAssertionFailedException
     * @throws \Behat\Mink\Exception\DriverException
     * @throws \Behat\Mink\Exception\UnsupportedDriverActionException
     */
    public function minkViewKeyword()
    {
        $mink = $this;

        $mink->assertPageTitleIs(__("Mots-clés de la liste: {0}", 'Liste de mots clés mink'));
        $mink->doClickAction(__("Visualiser {0}", 'Nom mot clé mink 3'));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->pausable();
        $mink->assertViewTableValueContains(__("Code"), "code_mot_cle_mink_3");
        $mink->closeModal();
    }

    /**
     * Modification d'un mot clé
     * @return void
     * @throws \AsalaeCore\MinkSuite\MinkAssertionFailedException
     * @throws \Behat\Mink\Exception\DriverException
     * @throws \Behat\Mink\Exception\UnsupportedDriverActionException
     */
    public function minkEditKeyword()
    {
        $mink = $this;

        $mink->assertPageTitleIs(__("Mots-clés de la liste: {0}", 'Liste de mots clés mink'));
        $mink->doClickAction(__("Modifier {0}", 'Nom mot clé mink 3'));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->pausable();
        $mink->setFormFieldValue(__("Code"), 'code_mot_cle_mink_4');
        $mink->setFormFieldValue(__("Nom"), 'Nom mot clé mink 4');
        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->waitModalClose();
        $mink->assertEquals(0, $mink->databaseFind('Keywords', ['code' => 'code_mot_cle_mink_3'])->count());
        $mink->assertEquals(1, $mink->databaseFind('Keywords', ['code' => 'code_mot_cle_mink_4'])->count());
        $mink->assertTableContains('#keywords-index-table', 'Nom mot clé mink 4');
        $mink->pausable();
    }

    /**
     * Suppression d'un mot clé
     * @return void
     * @throws \AsalaeCore\MinkSuite\MinkAssertionFailedException
     * @throws \Behat\Mink\Exception\DriverException
     * @throws \Behat\Mink\Exception\UnsupportedDriverActionException
     */
    public function minkDeleteKeyword()
    {
        $mink = $this;

        $mink->assertPageTitleIs(__("Mots-clés de la liste: {0}", 'Liste de mots clés mink'));
        $mink->doClickAction(__("Supprimer {0}", 'Nom mot clé mink 4'));
        $mink->pausable();
        $mink->doAcceptAlert();
        $mink->waitAjaxComplete();
        $mink->pausable();
        $mink->assertEquals(0, $mink->databaseFind('Keywords', ['code' => 'code_mot_cle_mink_4'])->count());
        $mink->assertTableDoesNotContains('#keywords-index-table', 'Nom mot clé mink 4');
    }

    /**
     * Publication d'une liste de mots clés
     * @return void
     * @throws \AsalaeCore\MinkSuite\MinkAssertionFailedException
     * @throws \Behat\Mink\Exception\DriverException
     * @throws \Behat\Mink\Exception\UnsupportedDriverActionException
     */
    public function minkPublishKeywordlist()
    {
        $mink = $this;

        $mink->assertPageTitleIs(__("Mots-clés de la liste: {0}", 'Liste de mots clés mink'));
        $mink->doClickButton(__("Publier la version de travail"));
        $mink->wait();
        $mink->pausable();
        $mink->assertPageTitleIs(__("Mots-clés"));
        $mink->assertEquals(
            1,
            $mink->databaseFindFirstOrFail('KeywordLists', ['identifier' => 'mink_keywordlist'])->get('version')
        );
    }

    /**
     * Edition d'une liste de mots clés
     * @return void
     * @throws \AsalaeCore\MinkSuite\MinkAssertionFailedException
     * @throws \Behat\Mink\Exception\DriverException
     * @throws \Behat\Mink\Exception\UnsupportedDriverActionException
     */
    public function minkEditKeywordlist()
    {
        $mink = $this;

        $mink->assertPageTitleIs(__("Mots-clés"));
        $mink->doClickAction(__("Modifier {0}", 'Liste de mots clés mink'));
        $mink->waitModalOpen();
        $mink->assertModalIsOpenned();
        $mink->pausable();
        $mink->setFormFieldValue(__("Identifiant"), 'mink_keywordlist2');
        $mink->setFormFieldValue(__("Nom"), 'Liste de mots clés mink 2');
        $mink->setFormFieldValue(__("Description"), 'Description modifiée');
        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->waitModalClose();
        $mink->assertEquals(0, $mink->databaseFind('KeywordLists', ['identifier' => 'mink_keywordlist'])->count());
        $mink->assertEquals(1, $mink->databaseFind('KeywordLists', ['identifier' => 'mink_keywordlist2'])->count());
        $mink->assertTableContains('#keyword-lists-index-table', 'Liste de mots clés mink 2');
        $mink->pausable();
    }

    /**
     * Suppression d'une liste de mots clés
     * @return void
     * @throws \AsalaeCore\MinkSuite\MinkAssertionFailedException
     * @throws \Behat\Mink\Exception\DriverException
     * @throws \Behat\Mink\Exception\UnsupportedDriverActionException
     */
    public function minkDeleteKeywordlist()
    {
        $mink = $this;

        $mink->assertPageTitleIs(__("Mots-clés"));
        $mink->doClickAction(__("Supprimer {0}", 'Liste de mots clés mink 2'));
        $mink->pausable();
        $mink->doAcceptAlert();
        $mink->waitAjaxComplete();
        $mink->pausable();
        $mink->assertEquals(0, $mink->databaseFind('KeywordLists', ['identifier' => 'mink_keywordlist2'])->count());
        $mink->assertTableDoesNotContains('#keyword-lists-index-table', 'Liste de mots clés mink 2');
    }
}
