<?php

namespace Versae\Test\MinkCase\Administration;

use Versae\MinkSuite\MinkCase;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;

class RolesUtilisateurMink extends MinkCase
{
    public function minkAddRole()
    {
        $mink = $this;
        $Roles = TableRegistry::getTableLocator()->get('Roles');
        $Roles->deleteAll(['name' => 'mink_role']);
        $Roles->deleteAll(['name' => 'mink_role2']);

        $mink->doLogin();
        $mink->doClickMenu(__("Administration"), __("Rôles utilisateur"));
        $mink->pausable();
        $mink->assertPageTitleIs(__("Rôles utilisateur"));
        $mink->doClickButton(__("Ajouter un rôle spécifique"));
        $mink->waitModalOpen();
        $mink->pausable();
        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Nom du rôle"), 'mink_role');
        $mink->setFormFieldValue(__("Description"), 'Ceci est un rôle spécifique');

        $mink->setFormFieldValues(__("Types d'entités"), [2, 3]);

        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->waitModalClose(20000);

        $mink->assertEquals(1, $Roles->find()->where(['name' => 'mink_role'])->count());
        $RolesTypeEntities = TableRegistry::getTableLocator()->get('RolesTypeEntities');
        $mink->assertEquals(
            [2, 3],
            $RolesTypeEntities->find()
                ->innerJoinWith('Roles')
                ->where(['Roles.name' => 'mink_role'])
                ->all()
                ->map(fn (EntityInterface $e) => $e->get('type_entity_id'))
                ->toArray()
        );
        $mink->assertTableContains('#roles-index-table', 'mink_role');
        $mink->pausable();
    }

    public function minkViewRole()
    {
        $mink = $this;
        $mink->assertPageTitleIs(__("Rôles utilisateur"));

        $mink->doClickAction(__("Visualiser {0}", 'mink_role'));
        $mink->waitModalOpen(20000);
        $mink->pausable();
        $mink->assertModalIsOpenned();
        $mink->assertTableContains('#roles-index-table', 'mink_role');

        $this->closeModal();
    }

    public function minkEditRole()
    {
        $mink = $this;
        $Roles = TableRegistry::getTableLocator()->get('Roles');

        $mink->assertPageTitleIs(__("Rôles utilisateur"));
        $mink->doClickAction(__("Modifier {0}", 'mink_role'));
        $mink->waitModalOpen();
        $mink->pausable();
        $mink->assertModalIsOpenned();
        $mink->setFormFieldValue(__("Nom du rôle"), 'mink_role2');
        $mink->doClick('input#edit-role-hierarchical-view');

        $mink->submitModal();
        $mink->assertResponseSuccess();
        $mink->waitModalClose();
        $mink->assertEquals(0, $Roles->find()->where(['name' => 'mink_role'])->count());
        $mink->assertEquals(1, $Roles->find()->where(['name' => 'mink_role2'])->count());
        $mink->assertTrue($Roles->find()->where(['name' => 'mink_role2'])->first()->get('hierarchical_view'));
        $mink->assertTableContains('#roles-index-table', 'mink_role2');
        $mink->pausable();
    }

    public function minkEditRoleDroits()
    {
        $mink = $this;

        $mink->assertPageTitleIs(__("Rôles utilisateur"));
        $mink->doClickAction(__("Gérer les droits de {0}", 'mink_role2'));
        $mink->waitModalOpen(20000);
        $mink->waitAjaxComplete();
        $mink->pausable();
        $mink->assertModalIsOpenned();

        $mink->setFormFieldValueById(__('controllers-orgentities-action-edit-1'), 1);
        $mink->submitModal(20000);
        $mink->assertResponseSuccess();
        $mink->waitModalClose();
    }

    public function minkDeleteRole()
    {
        $mink = $this;
        $Roles = TableRegistry::getTableLocator()->get('Roles');

        $mink->assertPageTitleIs(__("Rôles utilisateur"));
        $mink->doClickAction(__("Supprimer {0}", 'mink_role2'));
        $mink->pausable();
        $mink->doAcceptAlert();
        $mink->waitAjaxComplete();
        $mink->pausable();
        $mink->assertEquals(0, $Roles->find()->where(['name' => 'mink_role'])->count());
        $mink->assertEquals(0, $Roles->find()->where(['name' => 'mink_role2'])->count());
        $mink->assertTableDoesNotContains('#roles-index-table', 'mink_role');
        $mink->assertTableDoesNotContains('#roles-index-table', 'mink_role2');
    }
}
