<?php

/**
 * Test runner bootstrap.
 *
 * Add additional configuration/setup your application needs when running
 * unit tests in this file.
 *
 * @noinspection PhpCSValidationInspection
 */

use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Libriciel\Filesystem\Utility\Filesystem;
use Migrations\TestSuite\Migrator;
use Versae\Model\Entity\OrgEntity;
use Versae\Model\Entity\Role;
use Versae\Model\Entity\TypeEntity;
use Versae\Model\Entity\User;
use Versae\Test\Fixture\OrgEntitiesFixture;
use Versae\Test\Fixture\RolesFixture;
use Versae\Test\Fixture\TypeEntitiesFixture;
use Versae\Test\Fixture\UsersFixture;

require dirname(__DIR__) . '/vendor/autoload.php';

require dirname(__DIR__) . '/config/bootstrap.php';

$_SERVER['PHP_SELF'] = '/';

if (!defined('SAMPLES_CONVERTERS')) {
    define('SAMPLES_CONVERTERS', dirname(__DIR__) . DS . 'vendor' . DS . 'libriciel' . DS . 'file-converters-tests' . DS . 'tests');
}
if (!defined('SAMPLES_VALIDATOR')) {
    define('SAMPLES_VALIDATOR', dirname(__DIR__) . DS . 'vendor' . DS . 'libriciel' . DS . 'file-validator-tests' . DS . 'tests');
}

$users = UsersFixture::defaultValues();
$user = new User($users[0], ['validate' => false]); // user testunit
$user['id'] = 1;
$entities = OrgEntitiesFixture::defaultValues();
$entity = new OrgEntity($entities[1], ['validate' => false]); // service d'archives
$entity['id'] = 2;
$typeEntities = TypeEntitiesFixture::defaultValues();
$typeEntity = new TypeEntity($typeEntities[1], ['validate' => false]);
$entity['type_entity'] = $typeEntity;
$archivalAgency = new OrgEntity($entities[1], ['validate' => false]);
$archivalAgency['id'] = 2;
$entity['archival_agency'] = $archivalAgency;
$user['org_entity'] = $entity;
$roles = RolesFixture::defaultValues();
$role = new Role($roles[0], ['validate' => false]);
$role['id'] = 1;
$user['role'] = $role; // archiviste
putenv('PHPUNIT=true');
define('GENERIC_SESSION_DATA', serialize(['Auth' => $user, 'Session' => ['token' => 'testunit']]));

define(
    'FIXTURES_AUTH',
    [
        'app.Configurations',
        'app.Ldaps',
        'app.OrgEntities',
        'app.Roles',
        'app.Sessions',
        'app.TypeEntities',
        'app.Users',
        'app.Acos',
        'app.Aros',
        'app.ArosAcos',
    ]
);
define(
    'FIXTURES_APP_BEFORE_FILTER',
    [
        'app.Filters',
        'app.Notifications',
        'app.SavedFilters',
    ]
);
define(
    'FIXTURES_FORMS',
    [
        'app.FormCalculators',
        'app.FormCalculatorsFormVariables',
        'app.FormExtractors',
        'app.FormExtractorsFormVariables',
        'app.FormFieldsets',
        'app.FormInputs',
        'app.FormInputsFormVariables',
        'app.FormTransferHeaders',
        'app.FormUnitContents',
        'app.FormUnitHeaders',
        'app.FormUnitKeywordDetails',
        'app.FormUnitKeywords',
        'app.FormUnitManagements',
        'app.FormUnits',
        'app.FormVariables',
        'app.Forms',
        'app.FormsTransferringAgencies',
    ]
);

if (getenv('TEST_TOKEN')) {
    putenv('NO_COLOR=true'); // désactive les couleurs de debug pour un bon affichage avec paratest
}
$tokenSuffix = getenv('TEST_TOKEN') ? '_' . getenv('TEST_TOKEN') : '';
if (!defined('TMP_TESTDIR')) {
    define('TMP_TESTDIR', sys_get_temp_dir() . DS . 'testunit' . $tokenSuffix);
}

Configure::write('Keycloak.enabled', false);
Configure::write('OpenIDConnect.enabled', false);

Configure::write('FilesystemUtility.tmpDir', sys_get_temp_dir());
Filesystem::clear();

// create schema
ConnectionManager::alias('test', 'default');
try {
    $migrator = new Migrator();
    $migrator->run();
} catch (Exception $e) {
    debug((string)$e);
    die;
}
TableRegistry::getTableLocator()->clear();
