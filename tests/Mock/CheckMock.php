<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\Mock;

use AsalaeCore\Utility\Check;

abstract class CheckMock
{
    public static $outputs = [];

    public static function __callStatic($name, $arguments)
    {
        if (!isset(self::$outputs[$name])) {
            return forward_static_call_array([Check::class, $name], $arguments);
        } elseif (is_callable(self::$outputs[$name])) {
            return call_user_func_array(self::$outputs[$name], $arguments);
        } else {
            return self::$outputs[$name];
        }
    }
}
