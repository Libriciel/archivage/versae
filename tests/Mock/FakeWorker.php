<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\Mock;

use AsalaeCore\Worker\TestableWorkerInterface;
use DateTime;

class FakeWorker extends FakeEmitter implements TestableWorkerInterface
{
    public $Pheanstalk;

    public function getWorker()
    {
        $this->Pheanstalk = $this;
        return $this;
    }

    public $next = false;

    public function getNext()
    {
        $this->next = !$this->next;
        return $this->next;
    }

    public function getData()
    {
        return [
            'date' => (new DateTime('now'))->format('Y-m-d H:i:s'),
            'test' => 'test',
        ];
    }

    public function emitTest(array $params = [])
    {
    }

    public function getTestResults(): array
    {
        return [];
    }
}
