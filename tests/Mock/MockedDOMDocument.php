<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\Mock;

use DOMDocument;

class MockedDOMDocument extends DOMDocument
{
    public function schemaValidate($filename, $options = null)
    {
        return true;
    }
}
