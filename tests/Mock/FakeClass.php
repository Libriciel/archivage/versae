<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\Mock;

use Cake\ORM\TableRegistry;
use DateTime;

class FakeClass
{
    public function __call($func, $args)
    {
        return ['test' => true];
    }

    public function send(
        int $user_id, /** @noinspection PhpUnusedParameterInspection */
        array $token = [],
        string $message = '',
        string $class = 'alert-info'
    ): array {
        $data = [
            'user_id' => $user_id,
            'text' => $message,
            'css_class' => $class,
            'created' => new DateTime('now')
        ];

        $Notifications = TableRegistry::getTableLocator()->get('Notifications');
        $notif = $Notifications->newEntity($data);
        $Notifications->save($notif);
        $data['id'] = $notif->get('id');

        return $data;
    }
}
