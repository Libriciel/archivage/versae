<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\Mock;

class FakeEmitter
{
    public function reset()
    {
        $this->index = 5;
    }
    public function getPheanstalk()
    {
        return $this;
    }
    public function peek()
    {
        return $this;
    }
    public function setTube()
    {
        return $this;
    }
    private $index = 5;
    public function reserveFromTube()
    {
        $this->index--;
        return $this;
    }
    public function getId()
    {
        return (string)$this->index;
    }
    public function getData()
    {
        return json_encode(
            [
                'data' => ['name' => 'test', 'id' => 1, 'message' => 'test'],
            ]
        );
    }
    public function statsJob()
    {
        return (object)[
            'state' => 'ready',
            'tube' => 'test',
        ];
    }
    public function emitNoSave()
    {
        return ++$this->index;
    }
    public function __call($func, $args)
    {
        return true;
    }
}
