<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\Mock;

class FakeCheck
{
    public function __call($func, $args)
    {
        return ['test' => true];
    }
}
