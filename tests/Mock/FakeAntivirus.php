<?php

namespace Versae\Test\Mock;

use AsalaeCore\Utility\Antivirus\AntivirusInterface;

class FakeAntivirus implements AntivirusInterface
{
    public static $scanResult = [];
    public static $pingResult = true;

    /**
     * Effectue un scan sur un fichier/dossier et renvoi la liste des fichiers
     * infectés (clé) et le nom du virus (valeur)
     * @param string $directory
     * @return array ['/path/to/virus/file.txt' => 'Eicar-Test-Signature']
     */
    public static function scan(string $directory): array
    {
        return self::$scanResult;
    }

    /**
     * Permet de vérifier que le service est Disponible
     * @return bool
     */
    public static function ping(): bool
    {
        return self::$pingResult;
    }
}
