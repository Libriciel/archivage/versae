<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\Mock;

use AsalaeCore\Driver\Timestamping\TimestampingInterface;
use Exception;

class GenericCaller implements TimestampingInterface
{
    public static $methods = [];
    public $contructArgs;

    public function __construct(...$args)
    {
        $this->contructArgs = $args;
    }

    public function __call($func, $args)
    {
        if (isset(self::$methods[$func])) {
            if (is_callable(self::$methods[$func])) {
                return call_user_func_array(self::$methods[$func], $args);
            }
            return self::$methods[$func];
        }
        return $this;
    }

    /**
     * Vérifie que le service est accessible (ne consomme pas de jeton)
     * @return bool
     */
    public function ping(): bool
    {
        return true;
    }

    /**
     * Test la connexion au service (peut consommer des jetons)
     * @return bool
     */
    public function check(): bool
    {
        return true;
    }

    /**
     * Genère un token à partir d'un fichier
     * @param resource|string $file
     * @return string
     * @throws Exception
     */
    public function generateToken($file): string
    {
        return 'test';
    }
}
