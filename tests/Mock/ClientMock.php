<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\Mock;

use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class ClientMock implements ClientInterface
{

    public static $outputs = [];

    public static $mock;

    public function __call($name, $arguments)
    {
        if (!isset(self::$outputs[$name])) {
            return call_user_func_array([self::$mock, $name], $arguments);
        } elseif (is_callable(self::$outputs[$name])) {
            return call_user_func_array(self::$outputs[$name], $arguments);
        } else {
            return self::$outputs[$name];
        }
    }

    public function sendRequest(RequestInterface $request): ResponseInterface
    {
        return $this->__call('sendRequest', [$request]);
    }
}
