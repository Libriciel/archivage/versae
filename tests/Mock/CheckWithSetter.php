<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\Mock;

use Versae\Utility\Check;

abstract class CheckWithSetter extends Check
{
    public static function setArchivingSystems(array $archivingSystems)
    {
        static::$archivingSystems = $archivingSystems;
    }
}
