<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\Mock;

use Cake\Datasource\ConnectionManager as CakeConnectionManager;

class ConnectionManager
{
    public static $outputs = [];

    public static function __callStatic($name, $arguments)
    {
        if (!isset(self::$outputs[$name])) {
            return forward_static_call_array([CakeConnectionManager::class, $name], $arguments);
        } elseif (is_callable(self::$outputs[$name])) {
            return call_user_func_array(self::$outputs[$name], $arguments);
        } else {
            return self::$outputs[$name];
        }
    }
}
