<?php

namespace Versae\Test\Fixture;

use AsalaeCore\Test\Fixture\TypeEntitiesFixture as CoreTypeEntitiesFixture;

/**
 * TypeEntitiesFixture
 */
class TypeEntitiesFixture extends CoreTypeEntitiesFixture
{
    /**
     * Valeurs par défaut
     * @return array[]
     */
    public static function defaultValues()
    {
        return [
            [// id=1
                'name' => 'Service d\'Exploitation',
                'active' => true,
                'created' => '2018-06-27T16:06:31',
                'modified' => '2018-06-27T16:06:31',
                'code' => 'SE',
            ],
            [// id=2
                'name' => 'Service d\'Archives',
                'active' => true,
                'created' => '2018-06-27T16:06:31',
                'modified' => '2018-06-27T16:06:31',
                'code' => 'SA',
            ],
            [// id=3
                'name' => 'Service Versant',
                'active' => true,
                'created' => '2018-06-27T16:06:31',
                'modified' => '2018-06-27T16:06:31',
                'code' => 'SV',
            ],
            [// id=4
                'name' => 'Organisationnel',
                'active' => true,
                'created' => '2018-06-27T16:06:31',
                'modified' => '2018-06-27T16:06:31',
                'code' => 'SO',
            ],
        ];
    }
}
