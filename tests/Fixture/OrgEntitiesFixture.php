<?php

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * OrgEntitiesFixture
 */
class OrgEntitiesFixture extends TestFixture
{
    /**
     * Valeurs par défaut
     * @return array[]
     */
    public static function defaultValues()
    {
        return [
            [// id=1
                'name' => 'DSI',
                'identifier' => 'dsi',
                'parent_id' => null,
                'lft' => 1,
                // 2 sa 7
                //   3 sv1 4
                //   5 sv2 6
                // 8 sa2 9
                // 10 sa3 11
                'rght' => 12,
                'created' => '2018-06-27T16:06:31',
                'modified' => '2018-06-27T16:06:31',
                'type_entity_id' => 1,
                'active' => true,
                'archival_agency_id' => null,
                'is_main_archival_agency' => null,
                'description' => 'Lorem ipsum',
                'max_disk_usage' => 0,
                'disk_usage' => 0,
                'short_name' => null,
            ],
            [// id=2
                'name' => 'Service d\'archives',
                'identifier' => 'sa',
                'parent_id' => 1,
                'lft' => 2,
                'rght' => 7,
                'created' => '2018-06-27T16:06:31',
                'modified' => '2018-06-27T16:06:31',
                'type_entity_id' => 2,
                'active' => true,
                'archival_agency_id' => null,
                'is_main_archival_agency' => true,
                'description' => 'Lorem ipsum',
                'max_disk_usage' => 0,
                'disk_usage' => 0,
                'short_name' => null,
            ],
            [// id=3
                'name' => 'Service Versant 1',
                'identifier' => 'sv1',
                'parent_id' => 2,
                'lft' => 3,
                'rght' => 4,
                'created' => '2018-06-27T16:06:31',
                'modified' => '2018-06-27T16:06:31',
                'type_entity_id' => 3,
                'active' => true,
                'archival_agency_id' => null,
                'is_main_archival_agency' => null,
                'description' => 'Lorem ipsum',
                'max_disk_usage' => 0,
                'disk_usage' => 0,
                'short_name' => null,
            ],
            [// id=4
                'name' => 'Service Versant 2',
                'identifier' => 'sv2',
                'parent_id' => 2,
                'lft' => 5,
                'rght' => 6,
                'created' => '2018-06-27T16:06:31',
                'modified' => '2018-06-27T16:06:31',
                'type_entity_id' => 3,
                'active' => true,
                'archival_agency_id' => null,
                'is_main_archival_agency' => null,
                'description' => 'Lorem ipsum',
                'max_disk_usage' => 0,
                'disk_usage' => 0,
                'short_name' => null,
            ],
            [// id=5
                'name' => 'Service d\'archives 2',
                'identifier' => 'sa2',
                'parent_id' => 1,
                'lft' => 8,
                'rght' => 9,
                'created' => '2018-06-27T16:06:31',
                'modified' => '2018-06-27T16:06:31',
                'type_entity_id' => 2,
                'active' => true,
                'archival_agency_id' => null,
                'is_main_archival_agency' => false,
                'description' => 'Lorem ipsum',
                'max_disk_usage' => 0,
                'disk_usage' => 0,
                'short_name' => null,
            ],
            [// id=6
                'name' => 'Service d\'archives 3',
                'identifier' => 'sa3',
                'parent_id' => 1,
                'lft' => 10,
                'rght' => 11,
                'created' => '2018-06-27T16:06:31',
                'modified' => '2018-06-27T16:06:31',
                'type_entity_id' => 2,
                'active' => true,
                'archival_agency_id' => null,
                'is_main_archival_agency' => false,
                'description' => 'Lorem ipsum',
                'max_disk_usage' => 0,
                'disk_usage' => 0,
                'short_name' => null,
            ],
        ];
    }

    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = static::defaultValues();
        parent::init();
    }
}
