<?php

namespace Versae\Test\Fixture;

use Cake\Auth\DefaultPasswordHasher;
use Cake\TestSuite\Fixture\TestFixture;

/**
 * UsersFixture
 */
class UsersFixture extends TestFixture
{
    public static function defaultValues()
    {
        $hasher = new DefaultPasswordHasher();
        return [
            [
                'username' => 'admin',
                'created' => '2018-04-25T09:34:17',
                'modified' => '2023-09-05T11:02:51',
                'password' => $hasher->hash('admin'),
                'cookies' => null,
                'menu' => null,
                'high_contrast' => false,
                'active' => true,
                'role_id' => 1,
                'org_entity_id' => 2,
                'email' => 'test@test.fr',
                'name' => 'test test',
                'ldap_id' => null,
                'agent_type' => 'person',
                'ldap_login' => null,
            ],
            [// id=2
                'username' => 'ldap_user',
                'created' => '2018-04-25T09:34:17',
                'modified' => '2018-04-25T09:34:17',
                'password' => null,
                'cookies' => null,
                'menu' => null,
                'high_contrast' => false,
                'active' => true,
                'role_id' => 1,
                'org_entity_id' => 2,
                'email' => 'test@test.fr',
                'name' => 'test ldap',
                'ldap_id' => 1,
                'agent_type' => 'person',
                'ldap_login' => 'ldap_user',
            ],
            [// id=3
                'username' => 'archiviste',
                'created' => '2018-04-25T09:34:17',
                'modified' => '2018-04-25T09:34:17',
                'password' => null,
                'cookies' => null,
                'menu' => null,
                'high_contrast' => false,
                'active' => true,
                'role_id' => 1,
                'org_entity_id' => 2,
                'email' => 'admin@test.fr',
                'name' => 'admin test',
                'ldap_id' => 1,
                'agent_type' => 'person',
                'ldap_login' => 'archiviste',
            ],
            [// id=4
                'username' => 'user_sa2',
                'created' => '2018-04-25T09:34:17',
                'modified' => '2018-04-25T09:34:17',
                'password' => null,
                'cookies' => null,
                'menu' => null,
                'high_contrast' => false,
                'active' => true,
                'role_id' => 3,
                'org_entity_id' => 5,
                'email' => 'admin@test.fr',
                'name' => 'admin du service d\'archives 2',
                'ldap_id' => 1,
                'agent_type' => 'person',
                'ldap_login' => null,
            ],
        ];
    }

    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = static::defaultValues();
        parent::init();
    }
}
