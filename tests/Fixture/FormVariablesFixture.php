<?php

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FormVariablesFixture
 */
class FormVariablesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'form_id' => 1,
                'type' => 'concat',
                'twig' => 'Lorem ipsum dolor sit amet, aliquet feugiat.',
                'app_meta' => '[]',
            ],
            [// id=2
                'form_id' => 1,
                'type' => 'concat',
                'twig' => 'Lorem ipsum dolor sit amet, aliquet feugiat.',
                'app_meta' => '[]',
            ],
            [// id=3
                'form_id' => 1,
                'type' => 'concat',
                'twig' => 'Lorem ipsum dolor sit amet, aliquet feugiat.',
                'app_meta' => '[]',
            ],
            [// id=4
                'form_id' => 7,
                'type' => 'concat',
                'twig' => '{{input.Titre}}',
                'app_meta' => '[]',
            ],
            [// id=5
                'form_id' => 7,
                'type' => 'concat',
                'twig' => '{{input.Commentaire}}',
                'app_meta' => '[]',
            ],
            [// id=6
                'form_id' => 7,
                'type' => 'concat',
                'twig' => 'Lorem ipsum dolor sit amet, aliquet feugiat.',
                'app_meta' => '[]',
            ],
            [// id=7
                'form_id' => 9,
                'type' => 'concat',
                'twig' => '{{input.simple_text}}',
                'app_meta' => '[]',
            ],
        ];
        parent::init();
    }
}
