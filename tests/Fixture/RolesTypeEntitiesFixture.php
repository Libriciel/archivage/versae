<?php

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RolesTypeEntitiesFixture
 */
class RolesTypeEntitiesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'role_id' => 1,
                'type_entity_id' => 2,
            ],
            [// id=2
                'role_id' => 2,
                'type_entity_id' => 3,
            ],
            [// id=3
                'role_id' => 3,
                'type_entity_id' => 3,
            ],
            [// id=4
                'role_id' => 2,
                'type_entity_id' => 4,
            ],
            [// id=5
                'role_id' => 3,
                'type_entity_id' => 4,
            ],
        ];
        parent::init();
    }
}
