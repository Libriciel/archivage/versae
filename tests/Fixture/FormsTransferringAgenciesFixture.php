<?php

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FormsTransferringAgenciesFixture
 */
class FormsTransferringAgenciesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'form_id' => 1,
                'org_entity_id' => 3,
            ],
            [// id=2
                'form_id' => 2,
                'org_entity_id' => 3,
            ],
            [// id=3
                'form_id' => 3,
                'org_entity_id' => 3,
            ],
            [// id=4
                'form_id' => 4,
                'org_entity_id' => 3,
            ],
            [// id=5
                'form_id' => 5,
                'org_entity_id' => 3,
            ],
            [// id=6
                'form_id' => 6,
                'org_entity_id' => 3,
            ],
            [// id=7
                'form_id' => 7,
                'org_entity_id' => 2,
            ],
            [// id=8
                'form_id' => 9,
                'org_entity_id' => 3,
            ],
        ];
        parent::init();
    }
}
