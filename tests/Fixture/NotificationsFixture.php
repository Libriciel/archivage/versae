<?php

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * NotificationsFixture
 */
class NotificationsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'user_id' => 1,
                'text' => 'Ceci n\'est pas un test',
                'created' => '2018-07-26T10:47:22',
                'css_class' => 'alert alert-info',
            ],
        ];
        parent::init();
    }
}
