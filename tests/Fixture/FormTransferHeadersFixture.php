<?php

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FormTransferHeadersFixture
 */
class FormTransferHeadersFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'form_id' => 1,
                'name' => 'Comment',
                'form_variable_id' => 2,
            ],
            [// id=2
                'form_id' => 7,
                'name' => 'Comment',
                'form_variable_id' => 4,
            ],
            [// id=3
                'form_id' => 9,
                'name' => 'Comment',
                'form_variable_id' => 7,
            ],
        ];
        parent::init();
    }
}
