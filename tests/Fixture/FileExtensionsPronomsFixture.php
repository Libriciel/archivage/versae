<?php

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FileExtensionsPronomsFixture
 */
class FileExtensionsPronomsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'file_extension_id' => 1,
                'pronom_id' => 1,
            ],
        ];
        parent::init();
    }
}
