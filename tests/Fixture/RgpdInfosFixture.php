<?php

declare(strict_types=1);

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RgpdInfosFixture
 */
class RgpdInfosFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'org_entity_id' => 1,
                'org_name' => 'Lorem ipsum',
                'org_address' => 'Lorem ipsum',
                'org_siret' => 'Lorem ipsum dolor sit amet',
                'org_ape' => 'Lorem ipsum dolor sit amet',
                'org_phone' => 'Lorem ipsum dolor sit amet',
                'org_email' => 'Lorem ipsum dolor sit amet',
                'ceo_name' => 'Lorem ipsum dolor sit amet',
                'ceo_function' => 'Lorem ipsum dolor sit amet',
                'dpo_name' => 'Lorem ipsum dolor sit amet',
                'dpo_email' => 'Lorem ipsum dolor sit amet',
                'comment' => 'Lorem ipsum',
            ],
        ];
        parent::init();
    }
}
