<?php

declare(strict_types=1);

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;
use DateTime;

/**
 * MailsFixture
 */
class MailsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'serialized' => serialize(new DateTime()),
                'created' => 1618388337,
            ],
        ];
        parent::init();
    }
}
