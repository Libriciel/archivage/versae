<?php

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FormInputsFormVariablesFixture
 */
class FormInputsFormVariablesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'form_input_id' => 1,
                'form_variable_id' => 1,
            ],
            [// id=2
                'form_input_id' => 5,
                'form_variable_id' => 4,
            ],
            [// id=3
                'form_input_id' => 6,
                'form_variable_id' => 5,
            ],
            [// id=4
                'form_input_id' => 10,
                'form_variable_id' => 7,
            ],
        ];
        parent::init();
    }
}
