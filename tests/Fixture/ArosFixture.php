<?php
namespace Versae\Test\Fixture;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArosFixture
 */
class ArosFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'parent_id' => null,
                'model' => 'Roles',
                'foreign_key' => 1,
                'alias' => 'Archiviste',
                'lft' => 1,
                'rght' => 6,
            ],
            [// id=2
                'parent_id' => 1,
                'model' => 'Users',
                'foreign_key' => 1,
                'alias' => 'admin',
                'lft' => 2,
                'rght' => 3,
            ],
            [// id=3
                'parent_id' => 1,
                'model' => 'Users',
                'foreign_key' => 2,
                'alias' => 'ldap_user',
                'lft' => 4,
                'rght' => 5,
            ],
            [// id=4
                'parent_id' => null,
                'model' => 'Roles',
                'foreign_key' => 2,
                'alias' => 'Référent Archives',
                'lft' => 7,
                'rght' => 8,
            ],
            [// id=5
                'parent_id' => null,
                'model' => 'Roles',
                'foreign_key' => 3,
                'alias' => 'Versant',
                'lft' => 9,
                'rght' => 14,
            ],
            [// id=6
                'parent_id' => 5,
                'model' => 'Users',
                'foreign_key' => 3,
                'alias' => 'archiviste',
                'lft' => 10,
                'rght' => 11,
            ],
            [// id=7
                'parent_id' => 5,
                'model' => 'Users',
                'foreign_key' => 4,
                'alias' => 'user_sa2',
                'lft' => 12,
                'rght' => 13,
            ],
        ];
        TableRegistry::getTableLocator()->clear();
        parent::init();
    }
}
