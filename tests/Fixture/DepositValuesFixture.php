<?php

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DepositValuesFixture
 */
class DepositValuesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'deposit_id' => 7,
                'form_input_id' => 2,
                'input_value' => 'Lorem ipsum dolor sit amet',
                'hidden' => false,
                'fieldset_index' => null,
            ],
        ];
        parent::init();
    }
}
