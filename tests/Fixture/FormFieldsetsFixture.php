<?php

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FormFieldsetsFixture
 */
class FormFieldsetsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'form_id' => 1,
                'legend' => 'Lorem ipsum dolor sit amet',
                'ord' => 1,
                'disable_expression' => '',
                'repeatable' => false,
                'required' => null,
                'cardinality' => null,
                'button_name' => null,
            ],
            [// id=2
                'form_id' => 5,
                'legend' => 'Lorem ipsum dolor sit amet',
                'ord' => 1,
                'disable_expression' => '',
                'repeatable' => false,
                'required' => null,
                'cardinality' => null,
                'button_name' => null,
            ],
            [// id=3
                'form_id' => 6,
                'legend' => 'Lorem ipsum dolor sit amet',
                'ord' => 1,
                'disable_expression' => '',
                'repeatable' => false,
                'required' => null,
                'cardinality' => null,
                'button_name' => null,
            ],
            [// id=4
                'form_id' => 7,
                'legend' => '',
                'ord' => 0,
                'disable_expression' => '[]',
                'repeatable' => false,
                'required' => null,
                'cardinality' => null,
                'button_name' => null,
            ],
            [// id=5
                'form_id' => 7,
                'legend' => 'section multiple',
                'ord' => 1,
                'disable_expression' => '[]',
                'repeatable' => true,
                'required' => false,
                'cardinality' => 'n',
                'button_name' => 'Ajouter une section',
            ],
            [// id=6
                'form_id' => 9,
                'legend' => '',
                'ord' => 0,
                'disable_expression' => '[]',
                'repeatable' => false,
                'required' => null,
                'cardinality' => null,
                'button_name' => null,
            ],
        ];
        parent::init();
    }
}
