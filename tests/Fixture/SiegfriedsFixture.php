<?php

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SiegfriedsFixture
 */
class SiegfriedsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'fileupload_id' => 1,
                'pronom' => 'Lorem ipsum dolor sit amet',
                'format' => 'Lorem ipsum dolor sit amet',
                'version' => 'Lorem ipsum dolor sit amet',
                'mime' => 'Lorem ipsum dolor sit amet',
                'basis' => 'Lorem ipsum dolor sit amet',
                'warning' => 'Lorem ipsum dolor sit amet',
            ],
        ];
        parent::init();
    }
}
