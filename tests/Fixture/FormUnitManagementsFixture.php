<?php

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FormUnitManagementsFixture
 */
class FormUnitManagementsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'form_unit_id' => 1,
                'name' => 'Lorem ipsum dolor sit amet',
                'form_variable_id' => 2,
            ],
        ];
        parent::init();
    }
}
