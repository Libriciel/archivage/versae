<?php

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RolesFixture
 */
class RolesFixture extends TestFixture
{
    /**
     * Valeurs par défaut
     * @return array[]
     */
    public static function defaultValues()
    {
        return [
            [// id=1
                'name' => 'Archiviste',
                'active' => true,
                'created' => '2018-06-27T16:06:31',
                'modified' => '2018-06-27T16:06:31',
                'org_entity_id' => null,
                'code' => 'AA',
                'parent_id' => null,
                'lft' => 1,
                'rght' => 2,
                'hierarchical_view' => false,
                'description' => 'Lorem ipsum dolor sit amet',
                'agent_type' => 'person',
            ],
            [// id=2
                'name' => 'Référent Archives',
                'active' => true,
                'created' => '2018-06-27T16:06:31',
                'modified' => '2018-06-27T16:06:31',
                'org_entity_id' => null,
                'code' => 'ARA',
                'parent_id' => null,
                'lft' => 3,
                'rght' => 4,
                'hierarchical_view' => false,
                'description' => 'Lorem ipsum dolor sit amet',
                'agent_type' => 'person',
            ],
            [// id=3
                'name' => 'Versant',
                'active' => true,
                'created' => '2018-06-27T16:06:31',
                'modified' => '2018-06-27T16:06:31',
                'org_entity_id' => null,
                'code' => 'AV',
                'parent_id' => null,
                'lft' => 5,
                'rght' => 6,
                'hierarchical_view' => true,
                'description' => 'Lorem ipsum dolor sit amet',
                'agent_type' => 'person',
            ],
        ];
    }

    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = static::defaultValues();
        parent::init();
    }
}
