<?php

declare(strict_types=1);

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TransfersFixture
 */
class TransfersFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'deposit_id' => 5,
                'identifier' => 'Lorem ipsum dolor sit amet',
                'comment' => 'Lorem ipsum dolor sit amet',
                'path_token' => '12345678',
                'data_count' => 1,
                'data_size' => 1,
                'files_deleted' => true,
                'error_message' => 'Lorem ipsum dolor sit amet',
                'last_comm_date' => '2021-09-29T11:12:26',
                'last_comm_code' => 'Lorem ipsum dolor sit amet',
                'last_comm_message' => 'Lorem ipsum dolor sit amet',
                'created' => '2021-09-29T11:12:26',
                'created_user_id' => 1,
            ],
            [// id=2
                'deposit_id' => 6,
                'identifier' => 'Lorem ipsum dolor sit amet',
                'comment' => 'Lorem ipsum dolor sit amet',
                'path_token' => '12345678',
                'data_count' => 1,
                'data_size' => 1,
                'files_deleted' => true,
                'error_message' => 'Lorem ipsum dolor sit amet',
                'last_comm_date' => '2021-09-29T11:12:26',
                'last_comm_code' => 'Lorem ipsum dolor sit amet',
                'last_comm_message' => 'Lorem ipsum dolor sit amet',
                'created' => '2021-09-29T11:12:26',
                'created_user_id' => 1,
            ],
        ];
        parent::init();
    }
}
