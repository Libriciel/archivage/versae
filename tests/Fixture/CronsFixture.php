<?php

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CronsFixture
 */
class CronsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'name' => 'Lorem ipsum dolor sit amet',
                'description' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'classname' => 'AsalaeCore\Cron\LiberSign',
                'active' => true,
                'locked' => true,
                'frequency' => 'P1D',
                'next' => '2019-03-22T17:12:25',
                'app_meta' => '{}',
                'parallelisable' => true,
                'max_duration' => 'PT1H',
                'last_email' => '2020-03-19T15:45:04',
            ],
            [// id=2
                'name' => 'Lorem ipsum dolor sit amet 2',
                'description' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'classname' => 'AsalaeCore\Cron\Unlocker',
                'active' => true,
                'locked' => true,
                'frequency' => 'P1D',
                'next' => '2019-03-22T17:12:25',
                'app_meta' => '{}',
                'parallelisable' => true,
                'max_duration' => 'PT1H',
                'last_email' => '2020-03-19T15:45:04',
            ],
            [// id=3
                'name' => 'Suppression des données expirées',
                'description' => 'Lorem ipsum dolor sit amet',
                'classname' => 'Versae\Cron\DeleteExpiredData',
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'next' => '2019-03-22T17:12:25',
                'app_meta' => '{}',
                'parallelisable' => true,
                'max_duration' => 'PT1H',
                'last_email' => '2020-03-19T15:45:04',
            ],
            [// id=4
                'name' => 'Créateur de jobs',
                'description' => 'Lorem ipsum dolor sit amet',
                'classname' => 'Versae\Cron\JobMaker',
                'active' => true,
                'locked' => false,
                'frequency' => 'P1D',
                'next' => '2019-03-22T17:12:25',
                'app_meta' => '{}',
                'parallelisable' => true,
                'max_duration' => 'PT1H',
                'last_email' => '2020-03-19T15:45:04',
            ],
        ];
        parent::init();
    }
}
