<?php
namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FileuploadsFixture
 */
class FileuploadsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'name' => 'testfile.txt',
                'path' => TMP_TESTDIR . '/testfile.txt',
                'size' => 1,
                'hash' => 'ABD1472D2DDB6BC98565B56CE59A5FB4C66D108EB9643E5E503C35C243EB69C9',
                'hash_algo' => 'sha256',
                'created' => '2019-01-09T09:15:11',
                'modified' => '2019-01-09T09:15:11',
                'user_id' => 1,
                'valid' => true,
                'state' => 'Lorem ipsum dolor sit amet',
                'mime' => 'plain/text',
                'locked' => true,
            ],
            [// id=2
                'name' => 'testfile.pdf',
                'path' => TMP_TESTDIR . '/testfile.pdf',
                'size' => 1,
                'hash' => 'ABD1472D2DDB6BC98565B56CE59A5FB4C66D108EB9643E5E503C35C243EB69C9',
                'hash_algo' => 'sha256',
                'created' => '2019-01-09T09:15:11',
                'modified' => '2019-01-09T09:15:11',
                'user_id' => 1,
                'valid' => true,
                'state' => 'Lorem ipsum dolor sit amet',
                'mime' => 'application/pdf',
                'locked' => true,
            ],
            [// id=3
                'name' => 'testfileinput.txt',
                'path' => TMP_TESTDIR . '/testfileinput.txt',
                'size' => 1,
                'hash' => 'ABD1472D2DDB6BC98565B56CE59A5FB4C66D108EB9643E5E503C35C243EB69C9',
                'hash_algo' => 'sha256',
                'created' => '2019-01-09T09:15:11',
                'modified' => '2019-01-09T09:15:11',
                'user_id' => 1,
                'valid' => true,
                'state' => 'Lorem ipsum dolor sit amet',
                'mime' => 'plain/text',
                'locked' => false,
            ],
        ];
        parent::init();
    }
}
