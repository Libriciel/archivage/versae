<?php

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * BeanstalkJobsFixture
 */
class BeanstalkJobsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'tube' => 'test',
                'priority' => 1024,
                'created' => '2021-12-10T10:33:31',
                'user_id' => 1,
                'delay' => 0,
                'ttr' => 60,
                'errors' => null,
                'data' => null,
                'beanstalk_worker_id' => null,
                'object_model' => null,
                'object_foreign_key' => null,
                'job_state' => 'pending',
                'last_state_update' => null,
                'states_history' => null,
            ],
        ];
        parent::init();
    }
}
