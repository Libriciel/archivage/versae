<?php
namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArosAcosFixture
 */
class ArosAcosFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'aro_id' => 1, // Archiviste
                'aco_id' => 19, // root/controllers/Admins/ajaxLogin
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=2
                'aro_id' => 1, // Archiviste
                'aco_id' => 92, // root/controllers/Assets/configuredBackground
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=3
                'aro_id' => 1, // Archiviste
                'aco_id' => 93, // root/controllers/Assets/configuredLogoClient
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=4
                'aro_id' => 1, // Archiviste
                'aco_id' => 94, // root/controllers/Assets/css
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=5
                'aro_id' => 1, // Archiviste
                'aco_id' => 96, // root/controllers/AuthUrls/activate
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=6
                'aro_id' => 1, // Archiviste
                'aco_id' => 126, // root/controllers/Download/file
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=7
                'aro_id' => 1, // Archiviste
                'aco_id' => 127, // root/controllers/Download/open
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=8
                'aro_id' => 1, // Archiviste
                'aco_id' => 128, // root/controllers/Download/thumbnailImage
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=9
                'aro_id' => 1, // Archiviste
                'aco_id' => 133, // root/controllers/Filters/ajaxDeleteSave
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=10
                'aro_id' => 1, // Archiviste
                'aco_id' => 134, // root/controllers/Filters/ajaxGetFilters
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=11
                'aro_id' => 1, // Archiviste
                'aco_id' => 135, // root/controllers/Filters/ajaxNewSave
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=12
                'aro_id' => 1, // Archiviste
                'aco_id' => 136, // root/controllers/Filters/ajaxOverwrite
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=13
                'aro_id' => 1, // Archiviste
                'aco_id' => 137, // root/controllers/Filters/ajaxRedirectUrl
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=14
                'aro_id' => 1, // Archiviste
                'aco_id' => 4, // root/controllers/Home/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=15
                'aro_id' => 1, // Archiviste
                'aco_id' => 238, // root/controllers/Notifications/delete
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=16
                'aro_id' => 1, // Archiviste
                'aco_id' => 239, // root/controllers/Notifications/test
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=17
                'aro_id' => 1, // Archiviste
                'aco_id' => 273, // root/controllers/Tasks/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=18
                'aro_id' => 1, // Archiviste
                'aco_id' => 274, // root/controllers/Tasks/index/myJobInfo
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=19
                'aro_id' => 1, // Archiviste
                'aco_id' => 286, // root/controllers/Upload/delete
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=20
                'aro_id' => 1, // Archiviste
                'aco_id' => 288, // root/controllers/Upload/formFile
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=21
                'aro_id' => 1, // Archiviste
                'aco_id' => 289, // root/controllers/Upload/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=22
                'aro_id' => 1, // Archiviste
                'aco_id' => 294, // root/controllers/Users/ajaxLogin
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=23
                'aro_id' => 1, // Archiviste
                'aco_id' => 295, // root/controllers/Users/ajaxSaveMenu
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=24
                'aro_id' => 1, // Archiviste
                'aco_id' => 297, // root/controllers/Users/edit
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=25
                'aro_id' => 1, // Archiviste
                'aco_id' => 299, // root/controllers/Users/forgottenPassword
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=26
                'aro_id' => 1, // Archiviste
                'aco_id' => 303, // root/controllers/Users/initializePassword
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=27
                'aro_id' => 1, // Archiviste
                'aco_id' => 304, // root/controllers/Users/login
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=28
                'aro_id' => 1, // Archiviste
                'aco_id' => 305, // root/controllers/Users/logout
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=29
                'aro_id' => 1, // Archiviste
                'aco_id' => 306, // root/controllers/Users/resetSession
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=30
                'aro_id' => 1, // Archiviste
                'aco_id' => 309, // root/api/Forms/testExtractor
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=31
                'aro_id' => 4, // Référent Archives
                'aco_id' => 19, // root/controllers/Admins/ajaxLogin
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=32
                'aro_id' => 4, // Référent Archives
                'aco_id' => 92, // root/controllers/Assets/configuredBackground
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=33
                'aro_id' => 4, // Référent Archives
                'aco_id' => 93, // root/controllers/Assets/configuredLogoClient
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=34
                'aro_id' => 4, // Référent Archives
                'aco_id' => 94, // root/controllers/Assets/css
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=35
                'aro_id' => 4, // Référent Archives
                'aco_id' => 96, // root/controllers/AuthUrls/activate
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=36
                'aro_id' => 4, // Référent Archives
                'aco_id' => 126, // root/controllers/Download/file
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=37
                'aro_id' => 4, // Référent Archives
                'aco_id' => 127, // root/controllers/Download/open
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=38
                'aro_id' => 4, // Référent Archives
                'aco_id' => 128, // root/controllers/Download/thumbnailImage
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=39
                'aro_id' => 4, // Référent Archives
                'aco_id' => 133, // root/controllers/Filters/ajaxDeleteSave
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=40
                'aro_id' => 4, // Référent Archives
                'aco_id' => 134, // root/controllers/Filters/ajaxGetFilters
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=41
                'aro_id' => 4, // Référent Archives
                'aco_id' => 135, // root/controllers/Filters/ajaxNewSave
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=42
                'aro_id' => 4, // Référent Archives
                'aco_id' => 136, // root/controllers/Filters/ajaxOverwrite
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=43
                'aro_id' => 4, // Référent Archives
                'aco_id' => 137, // root/controllers/Filters/ajaxRedirectUrl
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=44
                'aro_id' => 4, // Référent Archives
                'aco_id' => 4, // root/controllers/Home/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=45
                'aro_id' => 4, // Référent Archives
                'aco_id' => 238, // root/controllers/Notifications/delete
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=46
                'aro_id' => 4, // Référent Archives
                'aco_id' => 239, // root/controllers/Notifications/test
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=47
                'aro_id' => 4, // Référent Archives
                'aco_id' => 273, // root/controllers/Tasks/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=48
                'aro_id' => 4, // Référent Archives
                'aco_id' => 274, // root/controllers/Tasks/index/myJobInfo
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=49
                'aro_id' => 4, // Référent Archives
                'aco_id' => 286, // root/controllers/Upload/delete
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=50
                'aro_id' => 4, // Référent Archives
                'aco_id' => 288, // root/controllers/Upload/formFile
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=51
                'aro_id' => 4, // Référent Archives
                'aco_id' => 289, // root/controllers/Upload/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=52
                'aro_id' => 4, // Référent Archives
                'aco_id' => 294, // root/controllers/Users/ajaxLogin
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=53
                'aro_id' => 4, // Référent Archives
                'aco_id' => 295, // root/controllers/Users/ajaxSaveMenu
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=54
                'aro_id' => 4, // Référent Archives
                'aco_id' => 297, // root/controllers/Users/edit
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=55
                'aro_id' => 4, // Référent Archives
                'aco_id' => 299, // root/controllers/Users/forgottenPassword
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=56
                'aro_id' => 4, // Référent Archives
                'aco_id' => 303, // root/controllers/Users/initializePassword
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=57
                'aro_id' => 4, // Référent Archives
                'aco_id' => 304, // root/controllers/Users/login
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=58
                'aro_id' => 4, // Référent Archives
                'aco_id' => 305, // root/controllers/Users/logout
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=59
                'aro_id' => 4, // Référent Archives
                'aco_id' => 306, // root/controllers/Users/resetSession
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=60
                'aro_id' => 4, // Référent Archives
                'aco_id' => 309, // root/api/Forms/testExtractor
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=61
                'aro_id' => 5, // Versant
                'aco_id' => 19, // root/controllers/Admins/ajaxLogin
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=62
                'aro_id' => 5, // Versant
                'aco_id' => 92, // root/controllers/Assets/configuredBackground
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=63
                'aro_id' => 5, // Versant
                'aco_id' => 93, // root/controllers/Assets/configuredLogoClient
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=64
                'aro_id' => 5, // Versant
                'aco_id' => 94, // root/controllers/Assets/css
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=65
                'aro_id' => 5, // Versant
                'aco_id' => 96, // root/controllers/AuthUrls/activate
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=66
                'aro_id' => 5, // Versant
                'aco_id' => 126, // root/controllers/Download/file
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=67
                'aro_id' => 5, // Versant
                'aco_id' => 127, // root/controllers/Download/open
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=68
                'aro_id' => 5, // Versant
                'aco_id' => 128, // root/controllers/Download/thumbnailImage
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=69
                'aro_id' => 5, // Versant
                'aco_id' => 133, // root/controllers/Filters/ajaxDeleteSave
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=70
                'aro_id' => 5, // Versant
                'aco_id' => 134, // root/controllers/Filters/ajaxGetFilters
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=71
                'aro_id' => 5, // Versant
                'aco_id' => 135, // root/controllers/Filters/ajaxNewSave
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=72
                'aro_id' => 5, // Versant
                'aco_id' => 136, // root/controllers/Filters/ajaxOverwrite
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=73
                'aro_id' => 5, // Versant
                'aco_id' => 137, // root/controllers/Filters/ajaxRedirectUrl
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=74
                'aro_id' => 5, // Versant
                'aco_id' => 4, // root/controllers/Home/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=75
                'aro_id' => 5, // Versant
                'aco_id' => 238, // root/controllers/Notifications/delete
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=76
                'aro_id' => 5, // Versant
                'aco_id' => 239, // root/controllers/Notifications/test
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=77
                'aro_id' => 5, // Versant
                'aco_id' => 273, // root/controllers/Tasks/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=78
                'aro_id' => 5, // Versant
                'aco_id' => 274, // root/controllers/Tasks/index/myJobInfo
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=79
                'aro_id' => 5, // Versant
                'aco_id' => 286, // root/controllers/Upload/delete
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=80
                'aro_id' => 5, // Versant
                'aco_id' => 288, // root/controllers/Upload/formFile
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=81
                'aro_id' => 5, // Versant
                'aco_id' => 289, // root/controllers/Upload/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=82
                'aro_id' => 5, // Versant
                'aco_id' => 294, // root/controllers/Users/ajaxLogin
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=83
                'aro_id' => 5, // Versant
                'aco_id' => 295, // root/controllers/Users/ajaxSaveMenu
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=84
                'aro_id' => 5, // Versant
                'aco_id' => 297, // root/controllers/Users/edit
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=85
                'aro_id' => 5, // Versant
                'aco_id' => 299, // root/controllers/Users/forgottenPassword
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=86
                'aro_id' => 5, // Versant
                'aco_id' => 303, // root/controllers/Users/initializePassword
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=87
                'aro_id' => 5, // Versant
                'aco_id' => 304, // root/controllers/Users/login
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=88
                'aro_id' => 5, // Versant
                'aco_id' => 305, // root/controllers/Users/logout
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=89
                'aro_id' => 5, // Versant
                'aco_id' => 306, // root/controllers/Users/resetSession
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=90
                'aro_id' => 5, // Versant
                'aco_id' => 309, // root/api/Forms/testExtractor
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=91
                'aro_id' => 2, // admin
                'aco_id' => 19, // root/controllers/Admins/ajaxLogin
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=92
                'aro_id' => 2, // admin
                'aco_id' => 92, // root/controllers/Assets/configuredBackground
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=93
                'aro_id' => 2, // admin
                'aco_id' => 93, // root/controllers/Assets/configuredLogoClient
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=94
                'aro_id' => 2, // admin
                'aco_id' => 94, // root/controllers/Assets/css
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=95
                'aro_id' => 2, // admin
                'aco_id' => 96, // root/controllers/AuthUrls/activate
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=96
                'aro_id' => 2, // admin
                'aco_id' => 126, // root/controllers/Download/file
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=97
                'aro_id' => 2, // admin
                'aco_id' => 127, // root/controllers/Download/open
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=98
                'aro_id' => 2, // admin
                'aco_id' => 128, // root/controllers/Download/thumbnailImage
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=99
                'aro_id' => 2, // admin
                'aco_id' => 133, // root/controllers/Filters/ajaxDeleteSave
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=100
                'aro_id' => 2, // admin
                'aco_id' => 134, // root/controllers/Filters/ajaxGetFilters
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=101
                'aro_id' => 2, // admin
                'aco_id' => 135, // root/controllers/Filters/ajaxNewSave
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=102
                'aro_id' => 2, // admin
                'aco_id' => 136, // root/controllers/Filters/ajaxOverwrite
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=103
                'aro_id' => 2, // admin
                'aco_id' => 137, // root/controllers/Filters/ajaxRedirectUrl
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=104
                'aro_id' => 2, // admin
                'aco_id' => 4, // root/controllers/Home/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=105
                'aro_id' => 2, // admin
                'aco_id' => 238, // root/controllers/Notifications/delete
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=106
                'aro_id' => 2, // admin
                'aco_id' => 239, // root/controllers/Notifications/test
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=107
                'aro_id' => 2, // admin
                'aco_id' => 273, // root/controllers/Tasks/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=108
                'aro_id' => 2, // admin
                'aco_id' => 274, // root/controllers/Tasks/index/myJobInfo
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=109
                'aro_id' => 2, // admin
                'aco_id' => 286, // root/controllers/Upload/delete
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=110
                'aro_id' => 2, // admin
                'aco_id' => 288, // root/controllers/Upload/formFile
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=111
                'aro_id' => 2, // admin
                'aco_id' => 289, // root/controllers/Upload/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=112
                'aro_id' => 2, // admin
                'aco_id' => 294, // root/controllers/Users/ajaxLogin
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=113
                'aro_id' => 2, // admin
                'aco_id' => 295, // root/controllers/Users/ajaxSaveMenu
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=114
                'aro_id' => 2, // admin
                'aco_id' => 297, // root/controllers/Users/edit
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=115
                'aro_id' => 2, // admin
                'aco_id' => 299, // root/controllers/Users/forgottenPassword
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=116
                'aro_id' => 2, // admin
                'aco_id' => 303, // root/controllers/Users/initializePassword
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=117
                'aro_id' => 2, // admin
                'aco_id' => 304, // root/controllers/Users/login
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=118
                'aro_id' => 2, // admin
                'aco_id' => 305, // root/controllers/Users/logout
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=119
                'aro_id' => 2, // admin
                'aco_id' => 306, // root/controllers/Users/resetSession
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=120
                'aro_id' => 2, // admin
                'aco_id' => 309, // root/api/Forms/testExtractor
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=121
                'aro_id' => 3, // ldap_user
                'aco_id' => 19, // root/controllers/Admins/ajaxLogin
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=122
                'aro_id' => 3, // ldap_user
                'aco_id' => 92, // root/controllers/Assets/configuredBackground
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=123
                'aro_id' => 3, // ldap_user
                'aco_id' => 93, // root/controllers/Assets/configuredLogoClient
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=124
                'aro_id' => 3, // ldap_user
                'aco_id' => 94, // root/controllers/Assets/css
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=125
                'aro_id' => 3, // ldap_user
                'aco_id' => 96, // root/controllers/AuthUrls/activate
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=126
                'aro_id' => 3, // ldap_user
                'aco_id' => 126, // root/controllers/Download/file
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=127
                'aro_id' => 3, // ldap_user
                'aco_id' => 127, // root/controllers/Download/open
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=128
                'aro_id' => 3, // ldap_user
                'aco_id' => 128, // root/controllers/Download/thumbnailImage
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=129
                'aro_id' => 3, // ldap_user
                'aco_id' => 133, // root/controllers/Filters/ajaxDeleteSave
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=130
                'aro_id' => 3, // ldap_user
                'aco_id' => 134, // root/controllers/Filters/ajaxGetFilters
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=131
                'aro_id' => 3, // ldap_user
                'aco_id' => 135, // root/controllers/Filters/ajaxNewSave
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=132
                'aro_id' => 3, // ldap_user
                'aco_id' => 136, // root/controllers/Filters/ajaxOverwrite
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=133
                'aro_id' => 3, // ldap_user
                'aco_id' => 137, // root/controllers/Filters/ajaxRedirectUrl
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=134
                'aro_id' => 3, // ldap_user
                'aco_id' => 4, // root/controllers/Home/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=135
                'aro_id' => 3, // ldap_user
                'aco_id' => 238, // root/controllers/Notifications/delete
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=136
                'aro_id' => 3, // ldap_user
                'aco_id' => 239, // root/controllers/Notifications/test
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=137
                'aro_id' => 3, // ldap_user
                'aco_id' => 273, // root/controllers/Tasks/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=138
                'aro_id' => 3, // ldap_user
                'aco_id' => 274, // root/controllers/Tasks/index/myJobInfo
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=139
                'aro_id' => 3, // ldap_user
                'aco_id' => 286, // root/controllers/Upload/delete
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=140
                'aro_id' => 3, // ldap_user
                'aco_id' => 288, // root/controllers/Upload/formFile
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=141
                'aro_id' => 3, // ldap_user
                'aco_id' => 289, // root/controllers/Upload/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=142
                'aro_id' => 3, // ldap_user
                'aco_id' => 294, // root/controllers/Users/ajaxLogin
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=143
                'aro_id' => 3, // ldap_user
                'aco_id' => 295, // root/controllers/Users/ajaxSaveMenu
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=144
                'aro_id' => 3, // ldap_user
                'aco_id' => 297, // root/controllers/Users/edit
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=145
                'aro_id' => 3, // ldap_user
                'aco_id' => 299, // root/controllers/Users/forgottenPassword
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=146
                'aro_id' => 3, // ldap_user
                'aco_id' => 303, // root/controllers/Users/initializePassword
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=147
                'aro_id' => 3, // ldap_user
                'aco_id' => 304, // root/controllers/Users/login
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=148
                'aro_id' => 3, // ldap_user
                'aco_id' => 305, // root/controllers/Users/logout
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=149
                'aro_id' => 3, // ldap_user
                'aco_id' => 306, // root/controllers/Users/resetSession
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=150
                'aro_id' => 3, // ldap_user
                'aco_id' => 309, // root/api/Forms/testExtractor
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=151
                'aro_id' => 6, // archiviste
                'aco_id' => 19, // root/controllers/Admins/ajaxLogin
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=152
                'aro_id' => 6, // archiviste
                'aco_id' => 92, // root/controllers/Assets/configuredBackground
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=153
                'aro_id' => 6, // archiviste
                'aco_id' => 93, // root/controllers/Assets/configuredLogoClient
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=154
                'aro_id' => 6, // archiviste
                'aco_id' => 94, // root/controllers/Assets/css
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=155
                'aro_id' => 6, // archiviste
                'aco_id' => 96, // root/controllers/AuthUrls/activate
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=156
                'aro_id' => 6, // archiviste
                'aco_id' => 126, // root/controllers/Download/file
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=157
                'aro_id' => 6, // archiviste
                'aco_id' => 127, // root/controllers/Download/open
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=158
                'aro_id' => 6, // archiviste
                'aco_id' => 128, // root/controllers/Download/thumbnailImage
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=159
                'aro_id' => 6, // archiviste
                'aco_id' => 133, // root/controllers/Filters/ajaxDeleteSave
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=160
                'aro_id' => 6, // archiviste
                'aco_id' => 134, // root/controllers/Filters/ajaxGetFilters
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=161
                'aro_id' => 6, // archiviste
                'aco_id' => 135, // root/controllers/Filters/ajaxNewSave
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=162
                'aro_id' => 6, // archiviste
                'aco_id' => 136, // root/controllers/Filters/ajaxOverwrite
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=163
                'aro_id' => 6, // archiviste
                'aco_id' => 137, // root/controllers/Filters/ajaxRedirectUrl
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=164
                'aro_id' => 6, // archiviste
                'aco_id' => 4, // root/controllers/Home/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=165
                'aro_id' => 6, // archiviste
                'aco_id' => 238, // root/controllers/Notifications/delete
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=166
                'aro_id' => 6, // archiviste
                'aco_id' => 239, // root/controllers/Notifications/test
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=167
                'aro_id' => 6, // archiviste
                'aco_id' => 273, // root/controllers/Tasks/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=168
                'aro_id' => 6, // archiviste
                'aco_id' => 274, // root/controllers/Tasks/index/myJobInfo
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=169
                'aro_id' => 6, // archiviste
                'aco_id' => 286, // root/controllers/Upload/delete
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=170
                'aro_id' => 6, // archiviste
                'aco_id' => 288, // root/controllers/Upload/formFile
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=171
                'aro_id' => 6, // archiviste
                'aco_id' => 289, // root/controllers/Upload/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=172
                'aro_id' => 6, // archiviste
                'aco_id' => 294, // root/controllers/Users/ajaxLogin
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=173
                'aro_id' => 6, // archiviste
                'aco_id' => 295, // root/controllers/Users/ajaxSaveMenu
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=174
                'aro_id' => 6, // archiviste
                'aco_id' => 297, // root/controllers/Users/edit
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=175
                'aro_id' => 6, // archiviste
                'aco_id' => 299, // root/controllers/Users/forgottenPassword
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=176
                'aro_id' => 6, // archiviste
                'aco_id' => 303, // root/controllers/Users/initializePassword
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=177
                'aro_id' => 6, // archiviste
                'aco_id' => 304, // root/controllers/Users/login
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=178
                'aro_id' => 6, // archiviste
                'aco_id' => 305, // root/controllers/Users/logout
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=179
                'aro_id' => 6, // archiviste
                'aco_id' => 306, // root/controllers/Users/resetSession
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=180
                'aro_id' => 6, // archiviste
                'aco_id' => 309, // root/api/Forms/testExtractor
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=181
                'aro_id' => 7, // user_sa2
                'aco_id' => 19, // root/controllers/Admins/ajaxLogin
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=182
                'aro_id' => 7, // user_sa2
                'aco_id' => 92, // root/controllers/Assets/configuredBackground
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=183
                'aro_id' => 7, // user_sa2
                'aco_id' => 93, // root/controllers/Assets/configuredLogoClient
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=184
                'aro_id' => 7, // user_sa2
                'aco_id' => 94, // root/controllers/Assets/css
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=185
                'aro_id' => 7, // user_sa2
                'aco_id' => 96, // root/controllers/AuthUrls/activate
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=186
                'aro_id' => 7, // user_sa2
                'aco_id' => 126, // root/controllers/Download/file
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=187
                'aro_id' => 7, // user_sa2
                'aco_id' => 127, // root/controllers/Download/open
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=188
                'aro_id' => 7, // user_sa2
                'aco_id' => 128, // root/controllers/Download/thumbnailImage
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=189
                'aro_id' => 7, // user_sa2
                'aco_id' => 133, // root/controllers/Filters/ajaxDeleteSave
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=190
                'aro_id' => 7, // user_sa2
                'aco_id' => 134, // root/controllers/Filters/ajaxGetFilters
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=191
                'aro_id' => 7, // user_sa2
                'aco_id' => 135, // root/controllers/Filters/ajaxNewSave
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=192
                'aro_id' => 7, // user_sa2
                'aco_id' => 136, // root/controllers/Filters/ajaxOverwrite
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=193
                'aro_id' => 7, // user_sa2
                'aco_id' => 137, // root/controllers/Filters/ajaxRedirectUrl
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=194
                'aro_id' => 7, // user_sa2
                'aco_id' => 4, // root/controllers/Home/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=195
                'aro_id' => 7, // user_sa2
                'aco_id' => 238, // root/controllers/Notifications/delete
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=196
                'aro_id' => 7, // user_sa2
                'aco_id' => 239, // root/controllers/Notifications/test
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=197
                'aro_id' => 7, // user_sa2
                'aco_id' => 273, // root/controllers/Tasks/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=198
                'aro_id' => 7, // user_sa2
                'aco_id' => 274, // root/controllers/Tasks/index/myJobInfo
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=199
                'aro_id' => 7, // user_sa2
                'aco_id' => 286, // root/controllers/Upload/delete
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=200
                'aro_id' => 7, // user_sa2
                'aco_id' => 288, // root/controllers/Upload/formFile
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=201
                'aro_id' => 7, // user_sa2
                'aco_id' => 289, // root/controllers/Upload/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=202
                'aro_id' => 7, // user_sa2
                'aco_id' => 294, // root/controllers/Users/ajaxLogin
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=203
                'aro_id' => 7, // user_sa2
                'aco_id' => 295, // root/controllers/Users/ajaxSaveMenu
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=204
                'aro_id' => 7, // user_sa2
                'aco_id' => 297, // root/controllers/Users/edit
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=205
                'aro_id' => 7, // user_sa2
                'aco_id' => 299, // root/controllers/Users/forgottenPassword
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=206
                'aro_id' => 7, // user_sa2
                'aco_id' => 303, // root/controllers/Users/initializePassword
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=207
                'aro_id' => 7, // user_sa2
                'aco_id' => 304, // root/controllers/Users/login
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=208
                'aro_id' => 7, // user_sa2
                'aco_id' => 305, // root/controllers/Users/logout
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=209
                'aro_id' => 7, // user_sa2
                'aco_id' => 306, // root/controllers/Users/resetSession
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=210
                'aro_id' => 7, // user_sa2
                'aco_id' => 309, // root/api/Forms/testExtractor
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=211
                'aro_id' => 1, // Archiviste
                'aco_id' => 98, // root/controllers/Counters/edit
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=212
                'aro_id' => 1, // Archiviste
                'aco_id' => 99, // root/controllers/Counters/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=213
                'aro_id' => 1, // Archiviste
                'aco_id' => 101, // root/controllers/Counters/view
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=214
                'aro_id' => 1, // Archiviste
                'aco_id' => 103, // root/controllers/Deposits/add1
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=215
                'aro_id' => 1, // Archiviste
                'aco_id' => 106, // root/controllers/Deposits/cancel
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=216
                'aro_id' => 1, // Archiviste
                'aco_id' => 108, // root/controllers/Deposits/delete
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=217
                'aro_id' => 1, // Archiviste
                'aco_id' => 109, // root/controllers/Deposits/edit
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=218
                'aro_id' => 1, // Archiviste
                'aco_id' => 112, // root/controllers/Deposits/indexAll
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=219
                'aro_id' => 1, // Archiviste
                'aco_id' => 113, // root/controllers/Deposits/indexInProgress
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=220
                'aro_id' => 1, // Archiviste
                'aco_id' => 114, // root/controllers/Deposits/indexMyEntity
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=221
                'aro_id' => 1, // Archiviste
                'aco_id' => 116, // root/controllers/Deposits/resend
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=222
                'aro_id' => 1, // Archiviste
                'aco_id' => 117, // root/controllers/Deposits/send
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=223
                'aro_id' => 1, // Archiviste
                'aco_id' => 119, // root/controllers/Deposits/view
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=224
                'aro_id' => 1, // Archiviste
                'aco_id' => 175, // root/controllers/Forms/activate
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=225
                'aro_id' => 1, // Archiviste
                'aco_id' => 176, // root/controllers/Forms/add
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=226
                'aro_id' => 1, // Archiviste
                'aco_id' => 181, // root/controllers/Forms/deactivate
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=227
                'aro_id' => 1, // Archiviste
                'aco_id' => 182, // root/controllers/Forms/delete
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=228
                'aro_id' => 1, // Archiviste
                'aco_id' => 184, // root/controllers/Forms/duplicate
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=229
                'aro_id' => 1, // Archiviste
                'aco_id' => 185, // root/controllers/Forms/edit
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=230
                'aro_id' => 1, // Archiviste
                'aco_id' => 189, // root/controllers/Forms/indexAll
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=231
                'aro_id' => 1, // Archiviste
                'aco_id' => 190, // root/controllers/Forms/indexEditing
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=232
                'aro_id' => 1, // Archiviste
                'aco_id' => 191, // root/controllers/Forms/indexPublished
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=233
                'aro_id' => 1, // Archiviste
                'aco_id' => 194, // root/controllers/Forms/publish
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=234
                'aro_id' => 1, // Archiviste
                'aco_id' => 195, // root/controllers/Forms/test
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=235
                'aro_id' => 1, // Archiviste
                'aco_id' => 196, // root/controllers/Forms/unpublish
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=236
                'aro_id' => 1, // Archiviste
                'aco_id' => 197, // root/controllers/Forms/version
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=237
                'aro_id' => 1, // Archiviste
                'aco_id' => 198, // root/controllers/Forms/view
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=238
                'aro_id' => 1, // Archiviste
                'aco_id' => 203, // root/controllers/KeywordLists/add
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=239
                'aro_id' => 1, // Archiviste
                'aco_id' => 204, // root/controllers/KeywordLists/delete
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=240
                'aro_id' => 1, // Archiviste
                'aco_id' => 205, // root/controllers/KeywordLists/edit
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=241
                'aro_id' => 1, // Archiviste
                'aco_id' => 206, // root/controllers/KeywordLists/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=242
                'aro_id' => 1, // Archiviste
                'aco_id' => 207, // root/controllers/KeywordLists/newVersion
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=243
                'aro_id' => 1, // Archiviste
                'aco_id' => 210, // root/controllers/KeywordLists/view
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=244
                'aro_id' => 1, // Archiviste
                'aco_id' => 229, // root/controllers/Ldaps/importUsers
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=245
                'aro_id' => 1, // Archiviste
                'aco_id' => 241, // root/controllers/OrgEntities/add
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=246
                'aro_id' => 1, // Archiviste
                'aco_id' => 242, // root/controllers/OrgEntities/delete
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=247
                'aro_id' => 1, // Archiviste
                'aco_id' => 244, // root/controllers/OrgEntities/edit
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=248
                'aro_id' => 1, // Archiviste
                'aco_id' => 249, // root/controllers/OrgEntities/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=249
                'aro_id' => 1, // Archiviste
                'aco_id' => 256, // root/controllers/Roles/add
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=250
                'aro_id' => 1, // Archiviste
                'aco_id' => 257, // root/controllers/Roles/delete
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=251
                'aro_id' => 1, // Archiviste
                'aco_id' => 258, // root/controllers/Roles/edit
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=252
                'aro_id' => 1, // Archiviste
                'aco_id' => 259, // root/controllers/Roles/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=253
                'aro_id' => 1, // Archiviste
                'aco_id' => 260, // root/controllers/Roles/view
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=254
                'aro_id' => 1, // Archiviste
                'aco_id' => 264, // root/controllers/Tasks/admin
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=255
                'aro_id' => 1, // Archiviste
                'aco_id' => 265, // root/controllers/Tasks/admin/adminJobInfo
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=256
                'aro_id' => 1, // Archiviste
                'aco_id' => 266, // root/controllers/Tasks/ajaxCancel
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=257
                'aro_id' => 1, // Archiviste
                'aco_id' => 267, // root/controllers/Tasks/ajaxPause
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=258
                'aro_id' => 1, // Archiviste
                'aco_id' => 268, // root/controllers/Tasks/ajaxResume
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=259
                'aro_id' => 1, // Archiviste
                'aco_id' => 292, // root/controllers/Users/add
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=260
                'aro_id' => 1, // Archiviste
                'aco_id' => 296, // root/controllers/Users/delete
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=261
                'aro_id' => 1, // Archiviste
                'aco_id' => 298, // root/controllers/Users/editByAdmin
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=262
                'aro_id' => 1, // Archiviste
                'aco_id' => 302, // root/controllers/Users/index
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=263
                'aro_id' => 1, // Archiviste
                'aco_id' => 307, // root/controllers/Users/view
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=264
                'aro_id' => 4, // Référent Archives
                'aco_id' => 98, // root/controllers/Counters/edit
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=265
                'aro_id' => 4, // Référent Archives
                'aco_id' => 99, // root/controllers/Counters/index
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=266
                'aro_id' => 4, // Référent Archives
                'aco_id' => 101, // root/controllers/Counters/view
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=267
                'aro_id' => 4, // Référent Archives
                'aco_id' => 103, // root/controllers/Deposits/add1
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=268
                'aro_id' => 4, // Référent Archives
                'aco_id' => 106, // root/controllers/Deposits/cancel
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=269
                'aro_id' => 4, // Référent Archives
                'aco_id' => 108, // root/controllers/Deposits/delete
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=270
                'aro_id' => 4, // Référent Archives
                'aco_id' => 109, // root/controllers/Deposits/edit
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=271
                'aro_id' => 4, // Référent Archives
                'aco_id' => 112, // root/controllers/Deposits/indexAll
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=272
                'aro_id' => 4, // Référent Archives
                'aco_id' => 113, // root/controllers/Deposits/indexInProgress
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=273
                'aro_id' => 4, // Référent Archives
                'aco_id' => 114, // root/controllers/Deposits/indexMyEntity
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=274
                'aro_id' => 4, // Référent Archives
                'aco_id' => 116, // root/controllers/Deposits/resend
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=275
                'aro_id' => 4, // Référent Archives
                'aco_id' => 117, // root/controllers/Deposits/send
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=276
                'aro_id' => 4, // Référent Archives
                'aco_id' => 119, // root/controllers/Deposits/view
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=277
                'aro_id' => 4, // Référent Archives
                'aco_id' => 175, // root/controllers/Forms/activate
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=278
                'aro_id' => 4, // Référent Archives
                'aco_id' => 176, // root/controllers/Forms/add
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=279
                'aro_id' => 4, // Référent Archives
                'aco_id' => 181, // root/controllers/Forms/deactivate
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=280
                'aro_id' => 4, // Référent Archives
                'aco_id' => 182, // root/controllers/Forms/delete
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=281
                'aro_id' => 4, // Référent Archives
                'aco_id' => 184, // root/controllers/Forms/duplicate
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=282
                'aro_id' => 4, // Référent Archives
                'aco_id' => 185, // root/controllers/Forms/edit
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=283
                'aro_id' => 4, // Référent Archives
                'aco_id' => 189, // root/controllers/Forms/indexAll
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=284
                'aro_id' => 4, // Référent Archives
                'aco_id' => 190, // root/controllers/Forms/indexEditing
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=285
                'aro_id' => 4, // Référent Archives
                'aco_id' => 191, // root/controllers/Forms/indexPublished
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=286
                'aro_id' => 4, // Référent Archives
                'aco_id' => 194, // root/controllers/Forms/publish
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=287
                'aro_id' => 4, // Référent Archives
                'aco_id' => 195, // root/controllers/Forms/test
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=288
                'aro_id' => 4, // Référent Archives
                'aco_id' => 196, // root/controllers/Forms/unpublish
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=289
                'aro_id' => 4, // Référent Archives
                'aco_id' => 197, // root/controllers/Forms/version
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=290
                'aro_id' => 4, // Référent Archives
                'aco_id' => 198, // root/controllers/Forms/view
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=291
                'aro_id' => 4, // Référent Archives
                'aco_id' => 203, // root/controllers/KeywordLists/add
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=292
                'aro_id' => 4, // Référent Archives
                'aco_id' => 204, // root/controllers/KeywordLists/delete
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=293
                'aro_id' => 4, // Référent Archives
                'aco_id' => 205, // root/controllers/KeywordLists/edit
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=294
                'aro_id' => 4, // Référent Archives
                'aco_id' => 206, // root/controllers/KeywordLists/index
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=295
                'aro_id' => 4, // Référent Archives
                'aco_id' => 207, // root/controllers/KeywordLists/newVersion
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=296
                'aro_id' => 4, // Référent Archives
                'aco_id' => 210, // root/controllers/KeywordLists/view
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=297
                'aro_id' => 4, // Référent Archives
                'aco_id' => 229, // root/controllers/Ldaps/importUsers
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=298
                'aro_id' => 4, // Référent Archives
                'aco_id' => 241, // root/controllers/OrgEntities/add
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=299
                'aro_id' => 4, // Référent Archives
                'aco_id' => 242, // root/controllers/OrgEntities/delete
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=300
                'aro_id' => 4, // Référent Archives
                'aco_id' => 244, // root/controllers/OrgEntities/edit
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=301
                'aro_id' => 4, // Référent Archives
                'aco_id' => 249, // root/controllers/OrgEntities/index
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=302
                'aro_id' => 4, // Référent Archives
                'aco_id' => 256, // root/controllers/Roles/add
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=303
                'aro_id' => 4, // Référent Archives
                'aco_id' => 257, // root/controllers/Roles/delete
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=304
                'aro_id' => 4, // Référent Archives
                'aco_id' => 258, // root/controllers/Roles/edit
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=305
                'aro_id' => 4, // Référent Archives
                'aco_id' => 259, // root/controllers/Roles/index
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=306
                'aro_id' => 4, // Référent Archives
                'aco_id' => 260, // root/controllers/Roles/view
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=307
                'aro_id' => 4, // Référent Archives
                'aco_id' => 264, // root/controllers/Tasks/admin
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=308
                'aro_id' => 4, // Référent Archives
                'aco_id' => 265, // root/controllers/Tasks/admin/adminJobInfo
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=309
                'aro_id' => 4, // Référent Archives
                'aco_id' => 266, // root/controllers/Tasks/ajaxCancel
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=310
                'aro_id' => 4, // Référent Archives
                'aco_id' => 267, // root/controllers/Tasks/ajaxPause
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=311
                'aro_id' => 4, // Référent Archives
                'aco_id' => 268, // root/controllers/Tasks/ajaxResume
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=312
                'aro_id' => 4, // Référent Archives
                'aco_id' => 292, // root/controllers/Users/add
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=313
                'aro_id' => 4, // Référent Archives
                'aco_id' => 296, // root/controllers/Users/delete
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=314
                'aro_id' => 4, // Référent Archives
                'aco_id' => 298, // root/controllers/Users/editByAdmin
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=315
                'aro_id' => 4, // Référent Archives
                'aco_id' => 302, // root/controllers/Users/index
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=316
                'aro_id' => 4, // Référent Archives
                'aco_id' => 307, // root/controllers/Users/view
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=317
                'aro_id' => 5, // Versant
                'aco_id' => 98, // root/controllers/Counters/edit
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=318
                'aro_id' => 5, // Versant
                'aco_id' => 99, // root/controllers/Counters/index
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=319
                'aro_id' => 5, // Versant
                'aco_id' => 101, // root/controllers/Counters/view
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=320
                'aro_id' => 5, // Versant
                'aco_id' => 103, // root/controllers/Deposits/add1
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=321
                'aro_id' => 5, // Versant
                'aco_id' => 106, // root/controllers/Deposits/cancel
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=322
                'aro_id' => 5, // Versant
                'aco_id' => 108, // root/controllers/Deposits/delete
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=323
                'aro_id' => 5, // Versant
                'aco_id' => 109, // root/controllers/Deposits/edit
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=324
                'aro_id' => 5, // Versant
                'aco_id' => 112, // root/controllers/Deposits/indexAll
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=325
                'aro_id' => 5, // Versant
                'aco_id' => 113, // root/controllers/Deposits/indexInProgress
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=326
                'aro_id' => 5, // Versant
                'aco_id' => 114, // root/controllers/Deposits/indexMyEntity
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=327
                'aro_id' => 5, // Versant
                'aco_id' => 116, // root/controllers/Deposits/resend
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=328
                'aro_id' => 5, // Versant
                'aco_id' => 117, // root/controllers/Deposits/send
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=329
                'aro_id' => 5, // Versant
                'aco_id' => 119, // root/controllers/Deposits/view
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=330
                'aro_id' => 5, // Versant
                'aco_id' => 175, // root/controllers/Forms/activate
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=331
                'aro_id' => 5, // Versant
                'aco_id' => 176, // root/controllers/Forms/add
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=332
                'aro_id' => 5, // Versant
                'aco_id' => 181, // root/controllers/Forms/deactivate
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=333
                'aro_id' => 5, // Versant
                'aco_id' => 182, // root/controllers/Forms/delete
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=334
                'aro_id' => 5, // Versant
                'aco_id' => 184, // root/controllers/Forms/duplicate
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=335
                'aro_id' => 5, // Versant
                'aco_id' => 185, // root/controllers/Forms/edit
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=336
                'aro_id' => 5, // Versant
                'aco_id' => 189, // root/controllers/Forms/indexAll
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=337
                'aro_id' => 5, // Versant
                'aco_id' => 190, // root/controllers/Forms/indexEditing
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=338
                'aro_id' => 5, // Versant
                'aco_id' => 191, // root/controllers/Forms/indexPublished
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=339
                'aro_id' => 5, // Versant
                'aco_id' => 194, // root/controllers/Forms/publish
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=340
                'aro_id' => 5, // Versant
                'aco_id' => 195, // root/controllers/Forms/test
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=341
                'aro_id' => 5, // Versant
                'aco_id' => 196, // root/controllers/Forms/unpublish
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=342
                'aro_id' => 5, // Versant
                'aco_id' => 197, // root/controllers/Forms/version
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=343
                'aro_id' => 5, // Versant
                'aco_id' => 198, // root/controllers/Forms/view
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=344
                'aro_id' => 5, // Versant
                'aco_id' => 203, // root/controllers/KeywordLists/add
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=345
                'aro_id' => 5, // Versant
                'aco_id' => 204, // root/controllers/KeywordLists/delete
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=346
                'aro_id' => 5, // Versant
                'aco_id' => 205, // root/controllers/KeywordLists/edit
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=347
                'aro_id' => 5, // Versant
                'aco_id' => 206, // root/controllers/KeywordLists/index
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=348
                'aro_id' => 5, // Versant
                'aco_id' => 207, // root/controllers/KeywordLists/newVersion
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=349
                'aro_id' => 5, // Versant
                'aco_id' => 210, // root/controllers/KeywordLists/view
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=350
                'aro_id' => 5, // Versant
                'aco_id' => 229, // root/controllers/Ldaps/importUsers
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=351
                'aro_id' => 5, // Versant
                'aco_id' => 241, // root/controllers/OrgEntities/add
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=352
                'aro_id' => 5, // Versant
                'aco_id' => 242, // root/controllers/OrgEntities/delete
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=353
                'aro_id' => 5, // Versant
                'aco_id' => 244, // root/controllers/OrgEntities/edit
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=354
                'aro_id' => 5, // Versant
                'aco_id' => 249, // root/controllers/OrgEntities/index
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=355
                'aro_id' => 5, // Versant
                'aco_id' => 256, // root/controllers/Roles/add
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=356
                'aro_id' => 5, // Versant
                'aco_id' => 257, // root/controllers/Roles/delete
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=357
                'aro_id' => 5, // Versant
                'aco_id' => 258, // root/controllers/Roles/edit
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=358
                'aro_id' => 5, // Versant
                'aco_id' => 259, // root/controllers/Roles/index
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=359
                'aro_id' => 5, // Versant
                'aco_id' => 260, // root/controllers/Roles/view
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=360
                'aro_id' => 5, // Versant
                'aco_id' => 264, // root/controllers/Tasks/admin
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=361
                'aro_id' => 5, // Versant
                'aco_id' => 265, // root/controllers/Tasks/admin/adminJobInfo
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=362
                'aro_id' => 5, // Versant
                'aco_id' => 266, // root/controllers/Tasks/ajaxCancel
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=363
                'aro_id' => 5, // Versant
                'aco_id' => 267, // root/controllers/Tasks/ajaxPause
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=364
                'aro_id' => 5, // Versant
                'aco_id' => 268, // root/controllers/Tasks/ajaxResume
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=365
                'aro_id' => 5, // Versant
                'aco_id' => 292, // root/controllers/Users/add
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=366
                'aro_id' => 5, // Versant
                'aco_id' => 296, // root/controllers/Users/delete
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=367
                'aro_id' => 5, // Versant
                'aco_id' => 298, // root/controllers/Users/editByAdmin
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=368
                'aro_id' => 5, // Versant
                'aco_id' => 302, // root/controllers/Users/index
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=369
                'aro_id' => 5, // Versant
                'aco_id' => 307, // root/controllers/Users/view
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=370
                'aro_id' => 1, // Archiviste
                'aco_id' => 316, // root/controllers/Forms/export
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=371
                'aro_id' => 1, // Archiviste
                'aco_id' => 317, // root/controllers/Forms/import
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=372
                'aro_id' => 4, // Référent Archives
                'aco_id' => 316, // root/controllers/Forms/export
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=373
                'aro_id' => 4, // Référent Archives
                'aco_id' => 317, // root/controllers/Forms/import
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=374
                'aro_id' => 5, // Versant
                'aco_id' => 316, // root/controllers/Forms/export
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=375
                'aro_id' => 5, // Versant
                'aco_id' => 317, // root/controllers/Forms/import
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=376
                'aro_id' => 1, // Archiviste
                'aco_id' => 319, // root/controllers/ArchivingSystems/api
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=377
                'aro_id' => 2, // admin
                'aco_id' => 319, // root/controllers/ArchivingSystems/api
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=378
                'aro_id' => 3, // ldap_user
                'aco_id' => 319, // root/controllers/ArchivingSystems/api
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=379
                'aro_id' => 4, // Référent Archives
                'aco_id' => 319, // root/controllers/ArchivingSystems/api
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=380
                'aro_id' => 5, // Versant
                'aco_id' => 319, // root/controllers/ArchivingSystems/api
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=381
                'aro_id' => 6, // archiviste
                'aco_id' => 319, // root/controllers/ArchivingSystems/api
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=382
                'aro_id' => 7, // user_sa2
                'aco_id' => 319, // root/controllers/ArchivingSystems/api
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=383
                'aro_id' => 1, // Archiviste
                'aco_id' => 323, // root/controllers/Users/api
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=384
                'aro_id' => 2, // admin
                'aco_id' => 323, // root/controllers/Users/api
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=385
                'aro_id' => 3, // ldap_user
                'aco_id' => 323, // root/controllers/Users/api
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=386
                'aro_id' => 4, // Référent Archives
                'aco_id' => 323, // root/controllers/Users/api
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=387
                'aro_id' => 5, // Versant
                'aco_id' => 323, // root/controllers/Users/api
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=388
                'aro_id' => 6, // archiviste
                'aco_id' => 323, // root/controllers/Users/api
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=389
                'aro_id' => 7, // user_sa2
                'aco_id' => 323, // root/controllers/Users/api
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=390
                'aro_id' => 1, // Archiviste
                'aco_id' => 322, // root/controllers/Users/acceptUser
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=391
                'aro_id' => 1, // Archiviste
                'aco_id' => 324, // root/controllers/Users/changeEntity
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ],
            [// id=392
                'aro_id' => 4, // Référent Archives
                'aco_id' => 322, // root/controllers/Users/acceptUser
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=393
                'aro_id' => 4, // Référent Archives
                'aco_id' => 324, // root/controllers/Users/changeEntity
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=394
                'aro_id' => 5, // Versant
                'aco_id' => 322, // root/controllers/Users/acceptUser
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
            [// id=395
                'aro_id' => 5, // Versant
                'aco_id' => 324, // root/controllers/Users/changeEntity
                '_create' => '-1',
                '_read' => '-1',
                '_update' => '-1',
                '_delete' => '-1',
            ],
        ];
        parent::init();
    }
}
