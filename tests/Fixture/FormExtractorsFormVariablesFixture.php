<?php

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FormExtractorsFormVariablesFixture
 */
class FormExtractorsFormVariablesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'form_extractor_id' => 1,
                'form_variable_id' => 1,
            ],
        ];
        parent::init();
    }
}
