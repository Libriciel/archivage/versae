<?php

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FormCalculatorsFormVariablesFixture
 */
class FormCalculatorsFormVariablesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'form_calculator_id' => 1,
                'form_variable_id' => 1,
            ],
        ];
        parent::init();
    }
}
