<?php

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;
use Versae\Test\Mock\FakeWorker;

/**
 * BeanstalkWorkersFixture
 */
class BeanstalkWorkersFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'name' => 'test',
                'tube' => 'test',
                'path' => FakeWorker::class,
                'pid' => 987654321,
                'last_launch' => '2018-06-13 17:25:09',
                'hostname' => 'fake',
            ],
        ];
        parent::init();
    }
}
