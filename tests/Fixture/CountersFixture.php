<?php

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CountersFixture
 */
class CountersFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'org_entity_id' => 2,
                'identifier' => 'ArchiveTransfer',
                'name' => 'ArchiveTransfer',
                'description' => 'Utilisé pour générer le code TransferIdentifier des bordereaux de transfert.',
                'type' => 'interne',
                'definition_mask' => 'sa_AT_#s#',
                'sequence_reset_mask' => null,
                'sequence_reset_value' => null,
                'sequence_id' => 1,
                'active' => true,
                'created' => '2018-07-26T10:47:22',
                'modified' => '2018-07-26T10:47:22',
            ],
        ];
        parent::init();
    }
}
