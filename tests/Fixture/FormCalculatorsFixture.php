<?php

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FormCalculatorsFixture
 */
class FormCalculatorsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'form_id' => 1,
                'name' => 'var_test',
                'description' => 'Lorem ipsum',
                'form_variable_id' => 1,
                'multiple' => false,
            ],
            [// id=2
                'form_id' => 8,
                'name' => 'var_test_deletable',
                'description' => 'Lorem ipsum',
                'form_variable_id' => 6,
                'multiple' => false,
            ],
        ];
        parent::init();
    }
}
