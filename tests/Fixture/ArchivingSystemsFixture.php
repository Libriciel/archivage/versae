<?php

declare(strict_types=1);

namespace Versae\Test\Fixture;

use AsalaeCore\Test\Fixture\ArchivingSystemsFixture as CoreArchivingSystemsFixture;

/**
 * ArchivingSystemsFixture
 */
class ArchivingSystemsFixture extends CoreArchivingSystemsFixture
{
}
