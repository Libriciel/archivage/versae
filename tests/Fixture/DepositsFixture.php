<?php

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DepositsFixture
 */
class DepositsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'archival_agency_id' => 2,
                'transferring_agency_id' => 2,
                'form_id' => 1,
                'name' => 'editing',
                'state' => 'editing',
                'last_state_update' => '2021-09-20T14:27:04',
                'states_history' => '',
                'created_user_id' => 1,
                'created' => '2021-09-20T14:27:04',
                'path_token' => '12345678',
                'error_message' => '',
                'request_data' => null,
                'originating_agency_id' => null,
                'bypass_validation' => false,
            ],
            [// id=2
                'archival_agency_id' => 2,
                'transferring_agency_id' => 2,
                'form_id' => 1,
                'name' => 'sending',
                'state' => 'ready_to_prepare',
                'last_state_update' => '2021-09-20T14:27:04',
                'states_history' => '',
                'created_user_id' => 1,
                'created' => '2021-09-20T14:27:04',
                'path_token' => '12345678',
                'error_message' => '',
                'request_data' => null,
                'originating_agency_id' => null,
                'bypass_validation' => false,
            ],
            [// id=3
                'archival_agency_id' => 2,
                'transferring_agency_id' => 2,
                'form_id' => 1,
                'name' => 'received',
                'state' => 'received',
                'last_state_update' => '2021-09-20T14:27:04',
                'states_history' => '',
                'created_user_id' => 1,
                'created' => '2021-09-20T14:27:04',
                'path_token' => '12345678',
                'error_message' => '',
                'request_data' => null,
                'originating_agency_id' => null,
                'bypass_validation' => false,
            ],
            [// id=4
                'archival_agency_id' => 2,
                'transferring_agency_id' => 2,
                'form_id' => 1,
                'name' => 'accepted',
                'state' => 'accepted',
                'last_state_update' => '2021-09-20T14:27:04',
                'states_history' => '',
                'created_user_id' => 1,
                'created' => '2021-09-20T14:27:04',
                'path_token' => '12345678',
                'error_message' => '',
                'request_data' => null,
                'originating_agency_id' => null,
                'bypass_validation' => false,
            ],
            [// id=5
                'archival_agency_id' => 2,
                'transferring_agency_id' => 2,
                'form_id' => 1,
                'name' => 'rejected',
                'state' => 'rejected',
                'last_state_update' => '2021-09-20T14:27:04',
                'states_history' => '',
                'created_user_id' => 1,
                'created' => '2021-09-20T14:27:04',
                'path_token' => '12345678',
                'error_message' => '',
                'request_data' => null,
                'originating_agency_id' => null,
                'bypass_validation' => false,
            ],
            [// id=6
                'archival_agency_id' => 2,
                'transferring_agency_id' => 2,
                'form_id' => 1,
                'name' => 'preparing_error',
                'state' => 'preparing_error',
                'last_state_update' => '2021-09-20T14:27:04',
                'states_history' => '',
                'created_user_id' => 1,
                'created' => '2021-09-20T14:27:04',
                'path_token' => '12345678',
                'error_message' => 'erreur',
                'request_data' => null,
                'originating_agency_id' => null,
                'bypass_validation' => false,
            ],
            [// id=7
                'archival_agency_id' => 2,
                'transferring_agency_id' => 2,
                'form_id' => 5,
                'name' => 'editing',
                'state' => 'editing',
                'last_state_update' => '2021-09-20T14:27:04',
                'states_history' => '',
                'created_user_id' => 1,
                'created' => '2021-09-20T14:27:04',
                'path_token' => '12345678',
                'error_message' => '',
                'request_data' => null,
                'originating_agency_id' => null,
                'bypass_validation' => false,
            ],
            [// id=8
                'archival_agency_id' => 2,
                'transferring_agency_id' => 2,
                'form_id' => 5,
                'name' => 'editing',
                'state' => 'sending_error',
                'last_state_update' => '2021-09-20T14:27:04',
                'states_history' => '',
                'created_user_id' => 1,
                'created' => '2021-09-20T14:27:04',
                'path_token' => '12345678',
                'error_message' => '',
                'request_data' => null,
                'originating_agency_id' => null,
                'bypass_validation' => false,
            ],
        ];
        parent::init();
    }
}
