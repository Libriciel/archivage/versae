<?php

declare(strict_types=1);

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ChangeEntityRequestsFixture
 */
class ChangeEntityRequestsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'base_archival_agency_id' => 3, // sa2
                'target_archival_agency_id' => 2, // sa
                'user_id' => 1,
                'reason' => null,
                'created' => '2022-04-13T00:00:00',
            ],
        ];
        parent::init();
    }
}
