<?php

namespace Versae\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FormExtractorsFixture
 */
class FormExtractorsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'form_id' => 1,
                'name' => 'extractor_test',
                'description' => 'Lorem ipsum dolor sit amet',
                'type' => 'archivefile',
                'form_input_id' => 1,
                'file_selector' => '*',
                'data_format' => 'csv',
                'data_path' => '1,1',
                'app_meta' => '[]',
                'multiple' => false,
            ],
            [// id=2
                'form_id' => 1,
                'name' => 'extractor_test_without_input',
                'description' => 'Lorem ipsum dolor sit amet',
                'type' => 'Lorem ipsum dolor sit amet',
                'form_input_id' => null,
                'file_selector' => 'Lorem ipsum dolor sit amet',
                'data_format' => 'Lorem ipsum dolor sit amet',
                'data_path' => 'Lorem ipsum dolor sit amet',
                'app_meta' => '[]',
                'multiple' => false,
            ],
        ];
        parent::init();
    }
}
