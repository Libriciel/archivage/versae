<?php
namespace Versae\Test\Fixture;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\Fixture\TestFixture;

/**
 * AcosFixture
 */
class AcosFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// id=1
                'parent_id' => null,
                'model' => '',
                'foreign_key' => null,
                'alias' => 'root',
                'lft' => 1,
                'rght' => 656,
            ],
            [// id=2
                'parent_id' => 1, // root
                'model' => 'root',
                'foreign_key' => null,
                'alias' => 'controllers',
                'lft' => 2,
                'rght' => 641,
            ],
            [// id=3
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Home',
                'lft' => 3,
                'rght' => 6,
            ],
            [// id=4
                'parent_id' => 3, // root/controllers/Home
                'model' => 'Home',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 4,
                'rght' => 5,
            ],
            [// id=5
                'parent_id' => 1, // root
                'model' => 'root',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 642,
                'rght' => 655,
            ],
            [// id=6
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Admins',
                'lft' => 7,
                'rght' => 162,
            ],
            [// id=7
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'addLdap',
                'lft' => 8,
                'rght' => 9,
            ],
            [// id=8
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'addSae',
                'lft' => 10,
                'rght' => 11,
            ],
            [// id=9
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'addServiceArchive',
                'lft' => 12,
                'rght' => 13,
            ],
            [// id=10
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'addUser',
                'lft' => 14,
                'rght' => 15,
            ],
            [// id=11
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'addWorker',
                'lft' => 16,
                'rght' => 17,
            ],
            [// id=12
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxCheck',
                'lft' => 18,
                'rght' => 19,
            ],
            [// id=13
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxCheckDisk',
                'lft' => 20,
                'rght' => 21,
            ],
            [// id=14
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxCheckTreeSanity',
                'lft' => 22,
                'rght' => 23,
            ],
            [// id=15
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxCheckWebsocket',
                'lft' => 24,
                'rght' => 25,
            ],
            [// id=16
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxCheckWorkers',
                'lft' => 26,
                'rght' => 27,
            ],
            [// id=17
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxEditConf',
                'lft' => 28,
                'rght' => 29,
            ],
            [// id=18
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxInterrupt',
                'lft' => 30,
                'rght' => 31,
            ],
            [// id=19
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxLogin',
                'lft' => 32,
                'rght' => 33,
            ],
            [// id=20
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxLogs',
                'lft' => 34,
                'rght' => 35,
            ],
            [// id=21
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxNotify',
                'lft' => 36,
                'rght' => 37,
            ],
            [// id=22
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxTasks',
                'lft' => 38,
                'rght' => 39,
            ],
            [// id=23
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxToggleDebug',
                'lft' => 40,
                'rght' => 41,
            ],
            [// id=24
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'deleteLdap',
                'lft' => 42,
                'rght' => 43,
            ],
            [// id=25
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'deleteLogo',
                'lft' => 44,
                'rght' => 45,
            ],
            [// id=26
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'deleteSae',
                'lft' => 46,
                'rght' => 47,
            ],
            [// id=27
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'deleteSession',
                'lft' => 48,
                'rght' => 49,
            ],
            [// id=28
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'deleteTube',
                'lft' => 50,
                'rght' => 51,
            ],
            [// id=29
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'downloadLog',
                'lft' => 52,
                'rght' => 53,
            ],
            [// id=30
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'editAdmin',
                'lft' => 54,
                'rght' => 55,
            ],
            [// id=31
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'editCron',
                'lft' => 56,
                'rght' => 57,
            ],
            [// id=32
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'editLdap',
                'lft' => 58,
                'rght' => 59,
            ],
            [// id=33
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'editSae',
                'lft' => 60,
                'rght' => 61,
            ],
            [// id=34
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'editServiceArchive',
                'lft' => 62,
                'rght' => 63,
            ],
            [// id=35
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'editServiceExploitation',
                'lft' => 64,
                'rght' => 65,
            ],
            [// id=36
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'getCronState',
                'lft' => 66,
                'rght' => 67,
            ],
            [// id=37
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'getLdapUsers',
                'lft' => 68,
                'rght' => 69,
            ],
            [// id=38
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'getWorkerStats',
                'lft' => 70,
                'rght' => 71,
            ],
            [// id=39
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'getWsRolesOptions',
                'lft' => 72,
                'rght' => 73,
            ],
            [// id=40
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'importLdapUser',
                'lft' => 74,
                'rght' => 75,
            ],
            [// id=41
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 76,
                'rght' => 77,
            ],
            [// id=42
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'indexArchivingSystems',
                'lft' => 78,
                'rght' => 79,
            ],
            [// id=43
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'indexCrons',
                'lft' => 80,
                'rght' => 81,
            ],
            [// id=44
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'indexJobs',
                'lft' => 82,
                'rght' => 83,
            ],
            [// id=45
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'indexLdaps',
                'lft' => 84,
                'rght' => 85,
            ],
            [// id=46
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'indexSaes',
                'lft' => 86,
                'rght' => 87,
            ],
            [// id=47
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'indexServicesArchives',
                'lft' => 88,
                'rght' => 89,
            ],
            [// id=48
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'indexSessions',
                'lft' => 90,
                'rght' => 91,
            ],
            [// id=49
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'jobCancel',
                'lft' => 92,
                'rght' => 93,
            ],
            [// id=50
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'jobEdit',
                'lft' => 94,
                'rght' => 95,
            ],
            [// id=51
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'jobInfo',
                'lft' => 96,
                'rght' => 97,
            ],
            [// id=52
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'jobPause',
                'lft' => 98,
                'rght' => 99,
            ],
            [// id=53
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'jobResume',
                'lft' => 100,
                'rght' => 101,
            ],
            [// id=54
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'killWorker',
                'lft' => 102,
                'rght' => 103,
            ],
            [// id=55
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'login',
                'lft' => 104,
                'rght' => 105,
            ],
            [// id=56
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'logout',
                'lft' => 106,
                'rght' => 107,
            ],
            [// id=57
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'notifyAll',
                'lft' => 108,
                'rght' => 109,
            ],
            [// id=58
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'notifyUser',
                'lft' => 110,
                'rght' => 111,
            ],
            [// id=59
                'parent_id' => 244, // root/controllers/OrgEntities/edit
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'paginateUsers',
                'lft' => 501,
                'rght' => 502,
            ],
            [// id=60
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'pauseTube',
                'lft' => 112,
                'rght' => 113,
            ],
            [// id=61
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'phpInfo',
                'lft' => 114,
                'rght' => 115,
            ],
            [// id=62
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'phpinfo',
                'lft' => 116,
                'rght' => 117,
            ],
            [// id=63
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ping',
                'lft' => 118,
                'rght' => 119,
            ],
            [// id=64
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'probe',
                'lft' => 120,
                'rght' => 121,
            ],
            [// id=65
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'restartAllWorkers',
                'lft' => 122,
                'rght' => 123,
            ],
            [// id=66
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'restartWorker',
                'lft' => 124,
                'rght' => 125,
            ],
            [// id=67
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'resumeAllTube',
                'lft' => 126,
                'rght' => 127,
            ],
            [// id=68
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'resumeTube',
                'lft' => 128,
                'rght' => 129,
            ],
            [// id=69
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'runCron',
                'lft' => 130,
                'rght' => 131,
            ],
            [// id=70
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'runWorkerManager',
                'lft' => 132,
                'rght' => 133,
            ],
            [// id=71
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'sedageneratorPing',
                'lft' => 134,
                'rght' => 135,
            ],
            [// id=72
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'sendTestMail',
                'lft' => 136,
                'rght' => 137,
            ],
            [// id=73
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'setMainArchivalAgency',
                'lft' => 138,
                'rght' => 139,
            ],
            [// id=74
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'stopAllWorkers',
                'lft' => 140,
                'rght' => 141,
            ],
            [// id=75
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'stopService',
                'lft' => 142,
                'rght' => 143,
            ],
            [// id=76
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'stopWorker',
                'lft' => 144,
                'rght' => 145,
            ],
            [// id=77
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'tail',
                'lft' => 146,
                'rght' => 147,
            ],
            [// id=78
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'tailService',
                'lft' => 148,
                'rght' => 149,
            ],
            [// id=79
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'testSae',
                'lft' => 150,
                'rght' => 151,
            ],
            [// id=80
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'testWorker',
                'lft' => 152,
                'rght' => 153,
            ],
            [// id=81
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'viewCron',
                'lft' => 154,
                'rght' => 155,
            ],
            [// id=82
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'viewSae',
                'lft' => 156,
                'rght' => 157,
            ],
            [// id=83
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'workerLog',
                'lft' => 158,
                'rght' => 159,
            ],
            [// id=84
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'ArchivingSystems',
                'lft' => 163,
                'rght' => 178,
            ],
            [// id=85
                'parent_id' => 84, // root/controllers/ArchivingSystems
                'model' => 'ArchivingSystems',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 164,
                'rght' => 165,
            ],
            [// id=86
                'parent_id' => 84, // root/controllers/ArchivingSystems
                'model' => 'ArchivingSystems',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 166,
                'rght' => 167,
            ],
            [// id=87
                'parent_id' => 84, // root/controllers/ArchivingSystems
                'model' => 'ArchivingSystems',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 168,
                'rght' => 169,
            ],
            [// id=88
                'parent_id' => 84, // root/controllers/ArchivingSystems
                'model' => 'ArchivingSystems',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 170,
                'rght' => 171,
            ],
            [// id=89
                'parent_id' => 84, // root/controllers/ArchivingSystems
                'model' => 'ArchivingSystems',
                'foreign_key' => null,
                'alias' => 'testConnection',
                'lft' => 172,
                'rght' => 173,
            ],
            [// id=90
                'parent_id' => 84, // root/controllers/ArchivingSystems
                'model' => 'ArchivingSystems',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 174,
                'rght' => 175,
            ],
            [// id=91
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Assets',
                'lft' => 179,
                'rght' => 186,
            ],
            [// id=92
                'parent_id' => 91, // root/controllers/Assets
                'model' => 'Assets',
                'foreign_key' => null,
                'alias' => 'configuredBackground',
                'lft' => 180,
                'rght' => 181,
            ],
            [// id=93
                'parent_id' => 91, // root/controllers/Assets
                'model' => 'Assets',
                'foreign_key' => null,
                'alias' => 'configuredLogoClient',
                'lft' => 182,
                'rght' => 183,
            ],
            [// id=94
                'parent_id' => 91, // root/controllers/Assets
                'model' => 'Assets',
                'foreign_key' => null,
                'alias' => 'css',
                'lft' => 184,
                'rght' => 185,
            ],
            [// id=95
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'AuthUrls',
                'lft' => 187,
                'rght' => 190,
            ],
            [// id=96
                'parent_id' => 95, // root/controllers/AuthUrls
                'model' => 'AuthUrls',
                'foreign_key' => null,
                'alias' => 'activate',
                'lft' => 188,
                'rght' => 189,
            ],
            [// id=97
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Counters',
                'lft' => 191,
                'rght' => 200,
            ],
            [// id=98
                'parent_id' => 97, // root/controllers/Counters
                'model' => 'Counters',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 192,
                'rght' => 195,
            ],
            [// id=99
                'parent_id' => 97, // root/controllers/Counters
                'model' => 'Counters',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 196,
                'rght' => 197,
            ],
            [// id=100
                'parent_id' => 98, // root/controllers/Counters/edit
                'model' => 'Counters',
                'foreign_key' => null,
                'alias' => 'initCounters',
                'lft' => 193,
                'rght' => 194,
            ],
            [// id=101
                'parent_id' => 97, // root/controllers/Counters
                'model' => 'Counters',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 198,
                'rght' => 199,
            ],
            [// id=102
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Deposits',
                'lft' => 201,
                'rght' => 242,
            ],
            [// id=103
                'parent_id' => 102, // root/controllers/Deposits
                'model' => 'Deposits',
                'foreign_key' => null,
                'alias' => 'add1',
                'lft' => 202,
                'rght' => 209,
            ],
            [// id=104
                'parent_id' => 103, // root/controllers/Deposits/add1
                'model' => 'Deposits',
                'foreign_key' => null,
                'alias' => 'add2',
                'lft' => 205,
                'rght' => 206,
            ],
            [// id=105
                'parent_id' => 103, // root/controllers/Deposits/add1
                'model' => 'Deposits',
                'foreign_key' => null,
                'alias' => 'addAndSend',
                'lft' => 203,
                'rght' => 204,
            ],
            [// id=106
                'parent_id' => 102, // root/controllers/Deposits
                'model' => 'Deposits',
                'foreign_key' => null,
                'alias' => 'cancel',
                'lft' => 210,
                'rght' => 211,
            ],
            [// id=107
                'parent_id' => 109, // root/controllers/Deposits/edit
                'model' => 'Deposits',
                'foreign_key' => null,
                'alias' => 'checkNewFormVersion',
                'lft' => 219,
                'rght' => 220,
            ],
            [// id=108
                'parent_id' => 102, // root/controllers/Deposits
                'model' => 'Deposits',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 212,
                'rght' => 213,
            ],
            [// id=109
                'parent_id' => 102, // root/controllers/Deposits
                'model' => 'Deposits',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 214,
                'rght' => 225,
            ],
            [// id=110
                'parent_id' => 109, // root/controllers/Deposits/edit
                'model' => 'Deposits',
                'foreign_key' => null,
                'alias' => 'editAndSend',
                'lft' => 215,
                'rght' => 216,
            ],
            [// id=111
                'parent_id' => 119, // root/controllers/Deposits/view
                'model' => 'Deposits',
                'foreign_key' => null,
                'alias' => 'getXml',
                'lft' => 239,
                'rght' => 240,
            ],
            [// id=112
                'parent_id' => 102, // root/controllers/Deposits
                'model' => 'Deposits',
                'foreign_key' => null,
                'alias' => 'indexAll',
                'lft' => 226,
                'rght' => 227,
            ],
            [// id=113
                'parent_id' => 102, // root/controllers/Deposits
                'model' => 'Deposits',
                'foreign_key' => null,
                'alias' => 'indexInProgress',
                'lft' => 228,
                'rght' => 229,
            ],
            [// id=114
                'parent_id' => 102, // root/controllers/Deposits
                'model' => 'Deposits',
                'foreign_key' => null,
                'alias' => 'indexMyEntity',
                'lft' => 230,
                'rght' => 231,
            ],
            [// id=115
                'parent_id' => 109, // root/controllers/Deposits/edit
                'model' => 'Deposits',
                'foreign_key' => null,
                'alias' => 'reedit',
                'lft' => 221,
                'rght' => 222,
            ],
            [// id=116
                'parent_id' => 102, // root/controllers/Deposits
                'model' => 'Deposits',
                'foreign_key' => null,
                'alias' => 'resend',
                'lft' => 232,
                'rght' => 233,
            ],
            [// id=117
                'parent_id' => 102, // root/controllers/Deposits
                'model' => 'Deposits',
                'foreign_key' => null,
                'alias' => 'send',
                'lft' => 234,
                'rght' => 237,
            ],
            [// id=118
                'parent_id' => 109, // root/controllers/Deposits/edit
                'model' => 'Deposits',
                'foreign_key' => null,
                'alias' => 'upgradeFormVersion',
                'lft' => 223,
                'rght' => 224,
            ],
            [// id=119
                'parent_id' => 102, // root/controllers/Deposits
                'model' => 'Deposits',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 238,
                'rght' => 241,
            ],
            [// id=120
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Devs',
                'lft' => 243,
                'rght' => 254,
            ],
            [// id=121
                'parent_id' => 120, // root/controllers/Devs
                'model' => 'Devs',
                'foreign_key' => null,
                'alias' => 'debugMail',
                'lft' => 244,
                'rght' => 245,
            ],
            [// id=122
                'parent_id' => 120, // root/controllers/Devs
                'model' => 'Devs',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 246,
                'rght' => 247,
            ],
            [// id=123
                'parent_id' => 120, // root/controllers/Devs
                'model' => 'Devs',
                'foreign_key' => null,
                'alias' => 'permissions',
                'lft' => 248,
                'rght' => 249,
            ],
            [// id=124
                'parent_id' => 120, // root/controllers/Devs
                'model' => 'Devs',
                'foreign_key' => null,
                'alias' => 'setPermission',
                'lft' => 250,
                'rght' => 251,
            ],
            [// id=125
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Download',
                'lft' => 255,
                'rght' => 262,
            ],
            [// id=126
                'parent_id' => 125, // root/controllers/Download
                'model' => 'Download',
                'foreign_key' => null,
                'alias' => 'file',
                'lft' => 256,
                'rght' => 257,
            ],
            [// id=127
                'parent_id' => 125, // root/controllers/Download
                'model' => 'Download',
                'foreign_key' => null,
                'alias' => 'open',
                'lft' => 258,
                'rght' => 259,
            ],
            [// id=128
                'parent_id' => 125, // root/controllers/Download
                'model' => 'Download',
                'foreign_key' => null,
                'alias' => 'thumbnailImage',
                'lft' => 260,
                'rght' => 261,
            ],
            [// id=129
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Fileuploads',
                'lft' => 263,
                'rght' => 266,
            ],
            [// id=130
                'parent_id' => 190, // root/controllers/Forms/indexEditing
                'model' => 'Fileuploads',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 383,
                'rght' => 384,
            ],
            [// id=131
                'parent_id' => 129, // root/controllers/Fileuploads
                'model' => 'Fileuploads',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 264,
                'rght' => 265,
            ],
            [// id=132
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Filters',
                'lft' => 267,
                'rght' => 278,
            ],
            [// id=133
                'parent_id' => 132, // root/controllers/Filters
                'model' => 'Filters',
                'foreign_key' => null,
                'alias' => 'ajaxDeleteSave',
                'lft' => 268,
                'rght' => 269,
            ],
            [// id=134
                'parent_id' => 132, // root/controllers/Filters
                'model' => 'Filters',
                'foreign_key' => null,
                'alias' => 'ajaxGetFilters',
                'lft' => 270,
                'rght' => 271,
            ],
            [// id=135
                'parent_id' => 132, // root/controllers/Filters
                'model' => 'Filters',
                'foreign_key' => null,
                'alias' => 'ajaxNewSave',
                'lft' => 272,
                'rght' => 273,
            ],
            [// id=136
                'parent_id' => 132, // root/controllers/Filters
                'model' => 'Filters',
                'foreign_key' => null,
                'alias' => 'ajaxOverwrite',
                'lft' => 274,
                'rght' => 275,
            ],
            [// id=137
                'parent_id' => 132, // root/controllers/Filters
                'model' => 'Filters',
                'foreign_key' => null,
                'alias' => 'ajaxRedirectUrl',
                'lft' => 276,
                'rght' => 277,
            ],
            [// id=138
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'FormCalculators',
                'lft' => 279,
                'rght' => 280,
            ],
            [// id=139
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormCalculators',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 291,
                'rght' => 292,
            ],
            [// id=140
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormCalculators',
                'foreign_key' => null,
                'alias' => 'addEditContentUnit',
                'lft' => 293,
                'rght' => 294,
            ],
            [// id=141
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormCalculators',
                'foreign_key' => null,
                'alias' => 'addEditHeaderUnit',
                'lft' => 295,
                'rght' => 296,
            ],
            [// id=142
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormCalculators',
                'foreign_key' => null,
                'alias' => 'addEditHeaderVar',
                'lft' => 297,
                'rght' => 298,
            ],
            [// id=143
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormCalculators',
                'foreign_key' => null,
                'alias' => 'addEditKeywordDetail',
                'lft' => 299,
                'rght' => 300,
            ],
            [// id=144
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormCalculators',
                'foreign_key' => null,
                'alias' => 'addEditManagementUnit',
                'lft' => 301,
                'rght' => 302,
            ],
            [// id=145
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormCalculators',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 303,
                'rght' => 304,
            ],
            [// id=146
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormCalculators',
                'foreign_key' => null,
                'alias' => 'deleteKeywordDetail',
                'lft' => 305,
                'rght' => 306,
            ],
            [// id=147
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormCalculators',
                'foreign_key' => null,
                'alias' => 'deleteTransferHeader',
                'lft' => 307,
                'rght' => 308,
            ],
            [// id=148
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormCalculators',
                'foreign_key' => null,
                'alias' => 'deleteUnitContent',
                'lft' => 309,
                'rght' => 310,
            ],
            [// id=149
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormCalculators',
                'foreign_key' => null,
                'alias' => 'deleteUnitHeader',
                'lft' => 311,
                'rght' => 312,
            ],
            [// id=150
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormCalculators',
                'foreign_key' => null,
                'alias' => 'deleteUnitManagement',
                'lft' => 313,
                'rght' => 314,
            ],
            [// id=151
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormCalculators',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 315,
                'rght' => 316,
            ],
            [// id=152
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormCalculators',
                'foreign_key' => null,
                'alias' => 'getData',
                'lft' => 317,
                'rght' => 318,
            ],
            [// id=153
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'FormExtractors',
                'lft' => 281,
                'rght' => 282,
            ],
            [// id=154
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormExtractors',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 319,
                'rght' => 320,
            ],
            [// id=155
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormExtractors',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 321,
                'rght' => 322,
            ],
            [// id=156
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormExtractors',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 323,
                'rght' => 324,
            ],
            [// id=157
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormExtractors',
                'foreign_key' => null,
                'alias' => 'extractCsvPath',
                'lft' => 325,
                'rght' => 326,
            ],
            [// id=158
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormExtractors',
                'foreign_key' => null,
                'alias' => 'extractJsonPath',
                'lft' => 327,
                'rght' => 328,
            ],
            [// id=159
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormExtractors',
                'foreign_key' => null,
                'alias' => 'extractXpaths',
                'lft' => 329,
                'rght' => 330,
            ],
            [// id=160
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormExtractors',
                'foreign_key' => null,
                'alias' => 'testFilePath',
                'lft' => 331,
                'rght' => 332,
            ],
            [// id=161
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormExtractors',
                'foreign_key' => null,
                'alias' => 'testWebserviceFile',
                'lft' => 333,
                'rght' => 334,
            ],
            [// id=162
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'FormUnitKeywords',
                'lft' => 283,
                'rght' => 284,
            ],
            [// id=163
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormUnitKeywords',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 335,
                'rght' => 336,
            ],
            [// id=164
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormUnitKeywords',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 337,
                'rght' => 338,
            ],
            [// id=165
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormUnitKeywords',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 339,
                'rght' => 340,
            ],
            [// id=166
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'FormUnits',
                'lft' => 285,
                'rght' => 286,
            ],
            [// id=167
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormUnits',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 341,
                'rght' => 342,
            ],
            [// id=168
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormUnits',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 343,
                'rght' => 344,
            ],
            [// id=169
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormUnits',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 345,
                'rght' => 346,
            ],
            [// id=170
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormUnits',
                'foreign_key' => null,
                'alias' => 'getOrderOptions',
                'lft' => 347,
                'rght' => 348,
            ],
            [// id=171
                'parent_id' => 198, // root/controllers/Forms/view
                'model' => 'FormUnits',
                'foreign_key' => null,
                'alias' => 'getTree',
                'lft' => 401,
                'rght' => 402,
            ],
            [// id=172
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'FormUnits',
                'foreign_key' => null,
                'alias' => 'move',
                'lft' => 349,
                'rght' => 350,
            ],
            [// id=173
                'parent_id' => 198, // root/controllers/Forms/view
                'model' => 'FormUnits',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 403,
                'rght' => 404,
            ],
            [// id=174
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Forms',
                'lft' => 287,
                'rght' => 410,
            ],
            [// id=175
                'parent_id' => 174, // root/controllers/Forms
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'activate',
                'lft' => 288,
                'rght' => 289,
            ],
            [// id=176
                'parent_id' => 174, // root/controllers/Forms
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 290,
                'rght' => 367,
            ],
            [// id=177
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'addFieldset',
                'lft' => 351,
                'rght' => 352,
            ],
            [// id=178
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'addInput',
                'lft' => 353,
                'rght' => 354,
            ],
            [// id=179
                'parent_id' => 195, // root/controllers/Forms/test
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'afterTest',
                'lft' => 391,
                'rght' => 392,
            ],
            [// id=180
                'parent_id' => 174, // root/controllers/Forms
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 368,
                'rght' => 369,
            ],
            [// id=181
                'parent_id' => 174, // root/controllers/Forms
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'deactivate',
                'lft' => 370,
                'rght' => 371,
            ],
            [// id=182
                'parent_id' => 174, // root/controllers/Forms
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 372,
                'rght' => 373,
            ],
            [// id=183
                'parent_id' => 195, // root/controllers/Forms/test
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'displayXmlTest',
                'lft' => 393,
                'rght' => 394,
            ],
            [// id=184
                'parent_id' => 174, // root/controllers/Forms
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'duplicate',
                'lft' => 374,
                'rght' => 375,
            ],
            [// id=185
                'parent_id' => 174, // root/controllers/Forms
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 376,
                'rght' => 379,
            ],
            [// id=186
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'editFieldset',
                'lft' => 355,
                'rght' => 356,
            ],
            [// id=187
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'editInput',
                'lft' => 357,
                'rght' => 358,
            ],
            [// id=188
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'generateInput',
                'lft' => 359,
                'rght' => 360,
            ],
            [// id=189
                'parent_id' => 174, // root/controllers/Forms
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'indexAll',
                'lft' => 380,
                'rght' => 381,
            ],
            [// id=190
                'parent_id' => 174, // root/controllers/Forms
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'indexEditing',
                'lft' => 382,
                'rght' => 385,
            ],
            [// id=191
                'parent_id' => 174, // root/controllers/Forms
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'indexPublished',
                'lft' => 386,
                'rght' => 387,
            ],
            [// id=192
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'keywordListContent',
                'lft' => 361,
                'rght' => 362,
            ],
            [// id=193
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'preview',
                'lft' => 363,
                'rght' => 364,
            ],
            [// id=194
                'parent_id' => 174, // root/controllers/Forms
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'publish',
                'lft' => 388,
                'rght' => 389,
            ],
            [// id=195
                'parent_id' => 174, // root/controllers/Forms
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'test',
                'lft' => 390,
                'rght' => 395,
            ],
            [// id=196
                'parent_id' => 174, // root/controllers/Forms
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'unpublish',
                'lft' => 396,
                'rght' => 397,
            ],
            [// id=197
                'parent_id' => 174, // root/controllers/Forms
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'version',
                'lft' => 398,
                'rght' => 399,
            ],
            [// id=198
                'parent_id' => 174, // root/controllers/Forms
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 400,
                'rght' => 405,
            ],
            [// id=199
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Install',
                'lft' => 411,
                'rght' => 416,
            ],
            [// id=200
                'parent_id' => 199, // root/controllers/Install
                'model' => 'Install',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 412,
                'rght' => 413,
            ],
            [// id=201
                'parent_id' => 199, // root/controllers/Install
                'model' => 'Install',
                'foreign_key' => null,
                'alias' => 'sendTestMail',
                'lft' => 414,
                'rght' => 415,
            ],
            [// id=202
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'KeywordLists',
                'lft' => 417,
                'rght' => 454,
            ],
            [// id=203
                'parent_id' => 202, // root/controllers/KeywordLists
                'model' => 'KeywordLists',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 418,
                'rght' => 419,
            ],
            [// id=204
                'parent_id' => 202, // root/controllers/KeywordLists
                'model' => 'KeywordLists',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 420,
                'rght' => 421,
            ],
            [// id=205
                'parent_id' => 202, // root/controllers/KeywordLists
                'model' => 'KeywordLists',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 422,
                'rght' => 423,
            ],
            [// id=206
                'parent_id' => 202, // root/controllers/KeywordLists
                'model' => 'KeywordLists',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 424,
                'rght' => 427,
            ],
            [// id=207
                'parent_id' => 202, // root/controllers/KeywordLists
                'model' => 'KeywordLists',
                'foreign_key' => null,
                'alias' => 'newVersion',
                'lft' => 428,
                'rght' => 449,
            ],
            [// id=208
                'parent_id' => 207, // root/controllers/KeywordLists/newVersion
                'model' => 'KeywordLists',
                'foreign_key' => null,
                'alias' => 'publishVersion',
                'lft' => 429,
                'rght' => 430,
            ],
            [// id=209
                'parent_id' => 207, // root/controllers/KeywordLists/newVersion
                'model' => 'KeywordLists',
                'foreign_key' => null,
                'alias' => 'removeVersion',
                'lft' => 431,
                'rght' => 432,
            ],
            [// id=210
                'parent_id' => 202, // root/controllers/KeywordLists
                'model' => 'KeywordLists',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 450,
                'rght' => 453,
            ],
            [// id=211
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Keywords',
                'lft' => 455,
                'rght' => 456,
            ],
            [// id=212
                'parent_id' => 207, // root/controllers/KeywordLists/newVersion
                'model' => 'Keywords',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 433,
                'rght' => 434,
            ],
            [// id=213
                'parent_id' => 207, // root/controllers/KeywordLists/newVersion
                'model' => 'Keywords',
                'foreign_key' => null,
                'alias' => 'addMultiple',
                'lft' => 435,
                'rght' => 436,
            ],
            [// id=214
                'parent_id' => 207, // root/controllers/KeywordLists/newVersion
                'model' => 'Keywords',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 437,
                'rght' => 438,
            ],
            [// id=215
                'parent_id' => 207, // root/controllers/KeywordLists/newVersion
                'model' => 'Keywords',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 439,
                'rght' => 440,
            ],
            [// id=216
                'parent_id' => 207, // root/controllers/KeywordLists/newVersion
                'model' => 'Keywords',
                'foreign_key' => null,
                'alias' => 'getData',
                'lft' => 441,
                'rght' => 442,
            ],
            [// id=217
                'parent_id' => 207, // root/controllers/KeywordLists/newVersion
                'model' => 'Keywords',
                'foreign_key' => null,
                'alias' => 'getImportFileInfo',
                'lft' => 443,
                'rght' => 444,
            ],
            [// id=218
                'parent_id' => 207, // root/controllers/KeywordLists/newVersion
                'model' => 'Keywords',
                'foreign_key' => null,
                'alias' => 'import',
                'lft' => 445,
                'rght' => 446,
            ],
            [// id=219
                'parent_id' => 206, // root/controllers/KeywordLists/index
                'model' => 'Keywords',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 425,
                'rght' => 426,
            ],
            [// id=220
                'parent_id' => 207, // root/controllers/KeywordLists/newVersion
                'model' => 'Keywords',
                'foreign_key' => null,
                'alias' => 'populateSelect',
                'lft' => 447,
                'rght' => 448,
            ],
            [// id=221
                'parent_id' => 210, // root/controllers/KeywordLists/view
                'model' => 'Keywords',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 451,
                'rght' => 452,
            ],
            [// id=222
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Ldaps',
                'lft' => 457,
                'rght' => 488,
            ],
            [// id=223
                'parent_id' => 222, // root/controllers/Ldaps
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 458,
                'rght' => 461,
            ],
            [// id=224
                'parent_id' => 222, // root/controllers/Ldaps
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 462,
                'rght' => 463,
            ],
            [// id=225
                'parent_id' => 222, // root/controllers/Ldaps
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 464,
                'rght' => 465,
            ],
            [// id=226
                'parent_id' => 222, // root/controllers/Ldaps
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'getCount',
                'lft' => 466,
                'rght' => 467,
            ],
            [// id=227
                'parent_id' => 222, // root/controllers/Ldaps
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'getFiltered',
                'lft' => 468,
                'rght' => 469,
            ],
            [// id=228
                'parent_id' => 222, // root/controllers/Ldaps
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'getRandomEntry',
                'lft' => 470,
                'rght' => 471,
            ],
            [// id=229
                'parent_id' => 222, // root/controllers/Ldaps
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'importUsers',
                'lft' => 472,
                'rght' => 475,
            ],
            [// id=230
                'parent_id' => 222, // root/controllers/Ldaps
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 476,
                'rght' => 477,
            ],
            [// id=231
                'parent_id' => 223, // root/controllers/Ldaps/add
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'listAvailableAttributes',
                'lft' => 459,
                'rght' => 460,
            ],
            [// id=232
                'parent_id' => 222, // root/controllers/Ldaps
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'listLdapEntries',
                'lft' => 478,
                'rght' => 479,
            ],
            [// id=233
                'parent_id' => 222, // root/controllers/Ldaps
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'ping',
                'lft' => 480,
                'rght' => 481,
            ],
            [// id=234
                'parent_id' => 222, // root/controllers/Ldaps
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'testConnection',
                'lft' => 482,
                'rght' => 483,
            ],
            [// id=235
                'parent_id' => 222, // root/controllers/Ldaps
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'testLogin',
                'lft' => 484,
                'rght' => 485,
            ],
            [// id=236
                'parent_id' => 222, // root/controllers/Ldaps
                'model' => 'Ldaps',
                'foreign_key' => null,
                'alias' => 'viewEntry',
                'lft' => 486,
                'rght' => 487,
            ],
            [// id=237
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Notifications',
                'lft' => 489,
                'rght' => 494,
            ],
            [// id=238
                'parent_id' => 237, // root/controllers/Notifications
                'model' => 'Notifications',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 490,
                'rght' => 491,
            ],
            [// id=239
                'parent_id' => 237, // root/controllers/Notifications
                'model' => 'Notifications',
                'foreign_key' => null,
                'alias' => 'test',
                'lft' => 492,
                'rght' => 493,
            ],
            [// id=240
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'OrgEntities',
                'lft' => 495,
                'rght' => 516,
            ],
            [// id=241
                'parent_id' => 240, // root/controllers/OrgEntities
                'model' => 'OrgEntities',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 496,
                'rght' => 497,
            ],
            [// id=242
                'parent_id' => 240, // root/controllers/OrgEntities
                'model' => 'OrgEntities',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 498,
                'rght' => 499,
            ],
            [// id=243
                'parent_id' => 244, // root/controllers/OrgEntities/edit
                'model' => 'OrgEntities',
                'foreign_key' => null,
                'alias' => 'deleteBackground',
                'lft' => 503,
                'rght' => 504,
            ],
            [// id=244
                'parent_id' => 240, // root/controllers/OrgEntities
                'model' => 'OrgEntities',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 500,
                'rght' => 513,
            ],
            [// id=245
                'parent_id' => 244, // root/controllers/OrgEntities/edit
                'model' => 'OrgEntities',
                'foreign_key' => null,
                'alias' => 'editConfig',
                'lft' => 505,
                'rght' => 506,
            ],
            [// id=246
                'parent_id' => 244, // root/controllers/OrgEntities/edit
                'model' => 'OrgEntities',
                'foreign_key' => null,
                'alias' => 'editConversions',
                'lft' => 507,
                'rght' => 508,
            ],
            [// id=247
                'parent_id' => 244, // root/controllers/OrgEntities/edit
                'model' => 'OrgEntities',
                'foreign_key' => null,
                'alias' => 'editParentId',
                'lft' => 509,
                'rght' => 510,
            ],
            [// id=248
                'parent_id' => 229, // root/controllers/Ldaps/importUsers
                'model' => 'OrgEntities',
                'foreign_key' => null,
                'alias' => 'getAvailablesRoles',
                'lft' => 473,
                'rght' => 474,
            ],
            [// id=249
                'parent_id' => 240, // root/controllers/OrgEntities
                'model' => 'OrgEntities',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 514,
                'rght' => 515,
            ],
            [// id=250
                'parent_id' => 244, // root/controllers/OrgEntities/edit
                'model' => 'OrgEntities',
                'foreign_key' => null,
                'alias' => 'paginateUsers',
                'lft' => 511,
                'rght' => 512,
            ],
            [// id=251
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Permissions',
                'lft' => 517,
                'rght' => 518,
            ],
            [// id=252
                'parent_id' => 259, // root/controllers/Roles/index
                'model' => 'Permissions',
                'foreign_key' => null,
                'alias' => 'ajaxGetPermission',
                'lft' => 527,
                'rght' => 528,
            ],
            [// id=253
                'parent_id' => 259, // root/controllers/Roles/index
                'model' => 'Permissions',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 529,
                'rght' => 530,
            ],
            [// id=254
                'parent_id' => 259, // root/controllers/Roles/index
                'model' => 'Permissions',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 531,
                'rght' => 532,
            ],
            [// id=255
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Roles',
                'lft' => 519,
                'rght' => 542,
            ],
            [// id=256
                'parent_id' => 255, // root/controllers/Roles
                'model' => 'Roles',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 520,
                'rght' => 521,
            ],
            [// id=257
                'parent_id' => 255, // root/controllers/Roles
                'model' => 'Roles',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 522,
                'rght' => 523,
            ],
            [// id=258
                'parent_id' => 255, // root/controllers/Roles
                'model' => 'Roles',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 524,
                'rght' => 525,
            ],
            [// id=259
                'parent_id' => 255, // root/controllers/Roles
                'model' => 'Roles',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 526,
                'rght' => 537,
            ],
            [// id=260
                'parent_id' => 255, // root/controllers/Roles
                'model' => 'Roles',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 538,
                'rght' => 539,
            ],
            [// id=261
                'parent_id' => 259, // root/controllers/Roles/index
                'model' => 'Roles',
                'foreign_key' => null,
                'alias' => 'viewGlobal',
                'lft' => 533,
                'rght' => 534,
            ],
            [// id=262
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Tasks',
                'lft' => 543,
                'rght' => 576,
            ],
            [// id=263
                'parent_id' => 262, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 544,
                'rght' => 545,
            ],
            [// id=264
                'parent_id' => 262, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'admin',
                'lft' => 546,
                'rght' => 549,
            ],
            [// id=265
                'parent_id' => 264, // root/controllers/Tasks/admin
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'adminJobInfo',
                'lft' => 547,
                'rght' => 548,
            ],
            [// id=266
                'parent_id' => 262, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'ajaxCancel',
                'lft' => 550,
                'rght' => 551,
            ],
            [// id=267
                'parent_id' => 262, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'ajaxPause',
                'lft' => 552,
                'rght' => 553,
            ],
            [// id=268
                'parent_id' => 262, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'ajaxResume',
                'lft' => 554,
                'rght' => 555,
            ],
            [// id=269
                'parent_id' => 262, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'cancel',
                'lft' => 556,
                'rght' => 557,
            ],
            [// id=270
                'parent_id' => 262, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'deleteTube',
                'lft' => 558,
                'rght' => 559,
            ],
            [// id=271
                'parent_id' => 262, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 560,
                'rght' => 561,
            ],
            [// id=272
                'parent_id' => 262, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'getTubesInfo',
                'lft' => 562,
                'rght' => 563,
            ],
            [// id=273
                'parent_id' => 262, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 564,
                'rght' => 567,
            ],
            [// id=274
                'parent_id' => 273, // root/controllers/Tasks/index
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'myJobInfo',
                'lft' => 565,
                'rght' => 566,
            ],
            [// id=275
                'parent_id' => 262, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'pause',
                'lft' => 568,
                'rght' => 569,
            ],
            [// id=276
                'parent_id' => 262, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'pauseTube',
                'lft' => 570,
                'rght' => 571,
            ],
            [// id=277
                'parent_id' => 262, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'resume',
                'lft' => 572,
                'rght' => 573,
            ],
            [// id=278
                'parent_id' => 262, // root/controllers/Tasks
                'model' => 'Tasks',
                'foreign_key' => null,
                'alias' => 'resumeTube',
                'lft' => 574,
                'rght' => 575,
            ],
            [// id=279
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'TestError',
                'lft' => 577,
                'rght' => 582,
            ],
            [// id=280
                'parent_id' => 279, // root/controllers/TestError
                'model' => 'TestError',
                'foreign_key' => null,
                'alias' => 'code',
                'lft' => 578,
                'rght' => 579,
            ],
            [// id=281
                'parent_id' => 279, // root/controllers/TestError
                'model' => 'TestError',
                'foreign_key' => null,
                'alias' => 'throwException',
                'lft' => 580,
                'rght' => 581,
            ],
            [// id=282
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Transfers',
                'lft' => 583,
                'rght' => 586,
            ],
            [// id=283
                'parent_id' => 282, // root/controllers/Transfers
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 584,
                'rght' => 585,
            ],
            [// id=284
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Upload',
                'lft' => 587,
                'rght' => 598,
            ],
            [// id=285
                'parent_id' => 284, // root/controllers/Upload
                'model' => 'Upload',
                'foreign_key' => null,
                'alias' => 'archivingSystemCert',
                'lft' => 588,
                'rght' => 589,
            ],
            [// id=286
                'parent_id' => 284, // root/controllers/Upload
                'model' => 'Upload',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 590,
                'rght' => 591,
            ],
            [// id=287
                'parent_id' => 176, // root/controllers/Forms/add
                'model' => 'Upload',
                'foreign_key' => null,
                'alias' => 'formCert',
                'lft' => 365,
                'rght' => 366,
            ],
            [// id=288
                'parent_id' => 284, // root/controllers/Upload
                'model' => 'Upload',
                'foreign_key' => null,
                'alias' => 'formFile',
                'lft' => 592,
                'rght' => 593,
            ],
            [// id=289
                'parent_id' => 284, // root/controllers/Upload
                'model' => 'Upload',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 594,
                'rght' => 595,
            ],
            [// id=290
                'parent_id' => 284, // root/controllers/Upload
                'model' => 'Upload',
                'foreign_key' => null,
                'alias' => 'sedaGeneratorCert',
                'lft' => 596,
                'rght' => 597,
            ],
            [// id=291
                'parent_id' => 2, // root/controllers
                'model' => 'controllers',
                'foreign_key' => null,
                'alias' => 'Users',
                'lft' => 599,
                'rght' => 640,
            ],
            [// id=292
                'parent_id' => 291, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'add',
                'lft' => 600,
                'rght' => 605,
            ],
            [// id=293
                'parent_id' => 292, // root/controllers/Users/add
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'addWithSelectOrgEntity',
                'lft' => 601,
                'rght' => 602,
            ],
            [// id=294
                'parent_id' => 291, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'ajaxLogin',
                'lft' => 606,
                'rght' => 607,
            ],
            [// id=295
                'parent_id' => 291, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'ajaxSaveMenu',
                'lft' => 608,
                'rght' => 609,
            ],
            [// id=296
                'parent_id' => 291, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'delete',
                'lft' => 610,
                'rght' => 611,
            ],
            [// id=297
                'parent_id' => 291, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'edit',
                'lft' => 612,
                'rght' => 613,
            ],
            [// id=298
                'parent_id' => 291, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'editByAdmin',
                'lft' => 614,
                'rght' => 615,
            ],
            [// id=299
                'parent_id' => 291, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'forgottenPassword',
                'lft' => 616,
                'rght' => 617,
            ],
            [// id=300
                'parent_id' => 291, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'forgottenPasswordMail',
                'lft' => 618,
                'rght' => 619,
            ],
            [// id=301
                'parent_id' => 292, // root/controllers/Users/add
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'getRolesOptions',
                'lft' => 603,
                'rght' => 604,
            ],
            [// id=302
                'parent_id' => 291, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'index',
                'lft' => 620,
                'rght' => 621,
            ],
            [// id=303
                'parent_id' => 291, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'initializePassword',
                'lft' => 622,
                'rght' => 623,
            ],
            [// id=304
                'parent_id' => 291, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'login',
                'lft' => 624,
                'rght' => 625,
            ],
            [// id=305
                'parent_id' => 291, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'logout',
                'lft' => 626,
                'rght' => 627,
            ],
            [// id=306
                'parent_id' => 291, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'resetSession',
                'lft' => 628,
                'rght' => 629,
            ],
            [// id=307
                'parent_id' => 291, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'view',
                'lft' => 630,
                'rght' => 631,
            ],
            [// id=308
                'parent_id' => 5, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'Forms',
                'lft' => 643,
                'rght' => 646,
            ],
            [// id=309
                'parent_id' => 308, // root/api/Forms
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'testExtractor',
                'lft' => 644,
                'rght' => 645,
            ],
            [// id=310
                'parent_id' => 5, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'Transfers',
                'lft' => 647,
                'rght' => 650,
            ],
            [// id=311
                'parent_id' => 310, // root/api/Transfers
                'model' => 'Transfers',
                'foreign_key' => null,
                'alias' => 'zip',
                'lft' => 648,
                'rght' => 649,
            ],
            [// id=312
                'parent_id' => 255, // root/controllers/Roles
                'model' => 'Roles',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 540,
                'rght' => 541,
            ],
            [// id=313
                'parent_id' => 259, // root/controllers/Roles/index
                'model' => 'Roles',
                'foreign_key' => null,
                'alias' => 'viewWebservice',
                'lft' => 535,
                'rght' => 536,
            ],
            [// id=314
                'parent_id' => 5, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'Roles',
                'lft' => 651,
                'rght' => 652,
            ],
            [// id=315
                'parent_id' => 120, // root/controllers/Devs
                'model' => 'Devs',
                'foreign_key' => null,
                'alias' => 'icons',
                'lft' => 252,
                'rght' => 253,
            ],
            [// id=316
                'parent_id' => 174, // root/controllers/Forms
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'export',
                'lft' => 406,
                'rght' => 407,
            ],
            [// id=317
                'parent_id' => 174, // root/controllers/Forms
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'import',
                'lft' => 408,
                'rght' => 409,
            ],
            [// id=318
                'parent_id' => 103, // root/controllers/Deposits/add1
                'model' => 'Deposits',
                'foreign_key' => null,
                'alias' => 'generating',
                'lft' => 207,
                'rght' => 208,
            ],
            [// id=319
                'parent_id' => 84, // root/controllers/ArchivingSystems
                'model' => 'ArchivingSystems',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 176,
                'rght' => 177,
            ],
            [// id=320
                'parent_id' => 117, // root/controllers/Deposits/send
                'model' => 'Deposits',
                'foreign_key' => null,
                'alias' => 'summarize',
                'lft' => 235,
                'rght' => 236,
            ],
            [// id=321
                'parent_id' => 185, // root/controllers/Forms/edit
                'model' => 'Forms',
                'foreign_key' => null,
                'alias' => 'previewFormTree',
                'lft' => 377,
                'rght' => 378,
            ],
            [// id=322
                'parent_id' => 291, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'acceptUser',
                'lft' => 632,
                'rght' => 635,
            ],
            [// id=323
                'parent_id' => 291, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'api',
                'lft' => 636,
                'rght' => 637,
            ],
            [// id=324
                'parent_id' => 291, // root/controllers/Users
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'changeEntity',
                'lft' => 638,
                'rght' => 639,
            ],
            [// id=325
                'parent_id' => 322, // root/controllers/Users/acceptUser
                'model' => 'Users',
                'foreign_key' => null,
                'alias' => 'refuseUser',
                'lft' => 633,
                'rght' => 634,
            ],
            [// id=326
                'parent_id' => 5, // root/api
                'model' => 'api',
                'foreign_key' => null,
                'alias' => 'Users',
                'lft' => 653,
                'rght' => 654,
            ],
            [// id=327
                'parent_id' => 109, // root/controllers/Deposits/edit
                'model' => 'Deposits',
                'foreign_key' => null,
                'alias' => 'checkGenerating',
                'lft' => 217,
                'rght' => 218,
            ],
            [// id=328
                'parent_id' => 6, // root/controllers/Admins
                'model' => 'Admins',
                'foreign_key' => null,
                'alias' => 'ajaxCheckArchivingSystem',
                'lft' => 160,
                'rght' => 161,
            ],
        ];
        TableRegistry::getTableLocator()->clear();
        parent::init();
    }
}
