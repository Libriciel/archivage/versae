<?php

declare(strict_types=1);

namespace Versae\Test\Fixture;

use AsalaeCore\Test\Fixture\AuthSubUrlsFixture as CoreAuthSubUrlsFixture;

/**
 * AuthSubUrlsFixture
 */
class AuthSubUrlsFixture extends CoreAuthSubUrlsFixture
{
}
