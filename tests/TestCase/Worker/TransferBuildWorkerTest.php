<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Worker;

use AsalaeCore\Factory\Utility;
use AsalaeCore\ORM\Entity;
use AsalaeCore\TestSuite\SaveBufferTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\TestSuite\VolumeSample;
use AsalaeCore\Utility\Exec;
use Cake\Console\ConsoleIo;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\Stub\ConsoleInput;
use Cake\TestSuite\Stub\ConsoleOutput;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\MockObject\MockObject;
use Versae\Model\Entity\Mail;
use Versae\Model\Table\DepositsTable;
use Versae\Test\Mock\FakeAntivirus;
use Versae\Worker\TransferBuildWorker;

class TransferBuildWorkerTest extends TestCase
{
    use SaveBufferTrait;

    public $fixtures = [
        'app.Deposits',
        'app.Transfers',
    ];

    /**
     * @var TransferBuildWorker|MockObject $worker
     */
    public $worker;
    /**
     * @var Mail|MockObject
     */
    public $mailer;

    public function setUp(): void
    {
        parent::setUp();
        Configure::write('Beanstalk.disable_check_ttr', true);

        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $this->worker = $this->getMockBuilder(TransferBuildWorker::class)
            ->setConstructorArgs([new Entity(), $io])
            ->onlyMethods(['emit'])
            ->getMock();
        $this->originalAntivirus = Configure::read('Antivirus.classname');
        Configure::write('Antivirus.classname', FakeAntivirus::class);
        VolumeSample::init();
        Configure::write('App.paths.data', TMP_TESTDIR);
    }

    public function tearDown(): void
    {
        Utility::reset();
        Exec::waitUntilAsyncFinish();
        Configure::write('Antivirus.classname', $this->originalAntivirus);
        VolumeSample::destroy();
        parent::tearDown();
    }

    public function testWork()
    {
        VolumeSample::copySample('sample_seda10.xml', 'deposits/5_12345678/transfer.xml');
        Filesystem::mkdir(TMP_TESTDIR . '/deposits/5_12345678/tmp/management_data');
        $Deposits = TableRegistry::getTableLocator()->get('Deposits');
        $Deposits->updateAll(['state' => DepositsTable::S_READY_TO_PREPARE], ['id' => 5]);
        $this->worker->work(['transfer_id' => 1]);
        $this->assertEquals(
            DepositsTable::S_READY_TO_SEND,
            $Deposits->get(5)->get('state')
        );
    }
}
