<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Worker;

use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\Client;
use AsalaeCore\ORM\Entity;
use AsalaeCore\TestSuite\SaveBufferTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\TestSuite\VolumeSample;
use AsalaeCore\Utility\Exec;
use Beanstalk\Exception\CantWorkException;
use Cake\Console\ConsoleIo;
use Cake\Core\Configure;
use Cake\Http\Client\Response;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\Stub\ConsoleInput;
use Cake\TestSuite\Stub\ConsoleOutput;
use PHPUnit\Framework\MockObject\MockObject;
use Versae\Exception\GenericException;
use Versae\Model\Entity\Mail;
use Versae\Model\Table\DepositsTable;
use Versae\Test\Mock\ClientMock;
use Versae\Test\Mock\FakeAntivirus;
use Versae\Worker\MailWorker;
use Versae\Worker\TransferSendWorker;

class TransferSendWorkerTest extends TestCase
{
    use SaveBufferTrait;

    public $fixtures = [
        'app.ArchivingSystems',
        'app.AuthUrls',
        'app.Deposits',
        'app.Forms',
        'app.Transfers',
    ];

    /**
     * @var MailWorker|MockObject $worker
     */
    public $worker;
    /**
     * @var Mail|MockObject
     */
    public $mailer;
    /**
     * @var string
     */
    public $originalAntivirus;

    public function setUp(): void
    {
        parent::setUp();
        Configure::write('Beanstalk.disable_check_ttr', true);
        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $this->worker = $this->getMockBuilder(TransferSendWorker::class)
            ->setConstructorArgs([new Entity(), $io])
            ->onlyMethods(['emit'])
            ->getMock();
        $this->originalAntivirus = Configure::read('Antivirus.classname');
        Configure::write('Antivirus.classname', FakeAntivirus::class);
        VolumeSample::init();
        Configure::write('App.paths.data', TMP_TESTDIR);

        $response = $this->createMock(Response::class);
        $response->method('getJson')->willReturn(['success' => true]);
        $client = $this->getMockBuilder(Client::class)
            ->setConstructorArgs([])
            ->onlyMethods(['get', 'post'])
            ->getMock();
        $client->method('get')->willReturn($response);
        $client->method('post')->willReturn($response);
        ClientMock::$mock = $client;
        Utility::set(Client::class, new ClientMock());
    }

    public function tearDown(): void
    {
        Utility::reset();
        Exec::waitUntilAsyncFinish();
        Configure::write('Antivirus.classname', $this->originalAntivirus);
        VolumeSample::destroy();
        parent::tearDown();
    }

    public function testWork()
    {
        $zip = VolumeSample::copySample('sample_transfer10.zip', 'deposits/5_12345678/transfer.tar.gz');
        $Deposits = TableRegistry::getTableLocator()->get('Deposits');
        $Deposits->updateAll(['state' => DepositsTable::S_READY_TO_SEND], ['id' => 5]);
        $this->worker->work(['transfer_id' => 1]);
        $this->assertEquals(
            DepositsTable::S_SENT,
            $Deposits->get(5)->get('state')
        );

        // no data
        $e = null;
        try {
            $this->worker->work(['transfer_id' => 0]);
        } catch (CantWorkException $e) {
        }
        $this->assertInstanceOf(CantWorkException::class, $e);

        // state != ready_to_send
        $e = null;
        try {
            $this->worker->work(['transfer_id' => 1]);
        } catch (CantWorkException $e) {
        }
        $this->assertInstanceOf(CantWorkException::class, $e);

        // zip n'existe pas
        rename($zip, $zip . '.bak');
        $Deposits->updateAll(['state' => DepositsTable::S_READY_TO_SEND], ['id' => 5]);
        $e = null;
        try {
            $this->worker->work(['transfer_id' => 1]);
        } catch (GenericException $e) {
        }
        $this->assertInstanceOf(GenericException::class, $e);
        rename($zip . '.bak', $zip);
        $this->assertEquals(
            DepositsTable::S_SENDING_ERROR,
            $Deposits->get(5)->get('state')
        );
    }
}
