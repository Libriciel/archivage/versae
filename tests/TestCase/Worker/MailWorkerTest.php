<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Worker;

use AsalaeCore\Factory\Utility;
use AsalaeCore\ORM\Entity;
use AsalaeCore\TestSuite\SaveBufferTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use Cake\Console\ConsoleIo;
use Cake\Core\Configure;
use Cake\Mailer\Mailer;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\Stub\ConsoleInput;
use Cake\TestSuite\Stub\ConsoleOutput;
use PHPUnit\Framework\MockObject\MockObject;
use Versae\Model\Entity\Mail;
use Versae\Worker\MailWorker;

class MailWorkerTest extends TestCase
{
    use SaveBufferTrait;

    public $fixtures = [
        'app.Mails',
    ];

    /**
     * @var MailWorker|MockObject $worker
     */
    public $worker;
    /**
     * @var Mail|MockObject
     */
    public $mailer;

    public function setUp(): void
    {
        parent::setUp();
        Configure::write('Beanstalk.disable_check_ttr', true);
        $this->mailer = $this->getMockBuilder(Mailer::class)
            ->onlyMethods(['send'])
            ->getMock();

        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        $this->worker = $this->getMockBuilder(MailWorker::class)
            ->setConstructorArgs([new Entity(), $io])
            ->onlyMethods(['emit'])
            ->getMock();
    }

    public function tearDown(): void
    {
        Utility::reset();
        Exec::waitUntilAsyncFinish();
        parent::tearDown();
    }

    public function testWork()
    {
        $this->mailer->setSubject('testunit');
        $Mails = TableRegistry::getTableLocator()->get('Mails');
        $Mails->updateAll(
            ['serialized' => serialize($this->mailer)],
            ['id' => 1]
        );
        $this->worker->work(['mail_id' => 1]);
        $this->worker->afterWork(['mail_id' => 1]);
        $this->assertStringContainsString('testunit', $this->mailer->getMessage()->getSubject());
    }
}
