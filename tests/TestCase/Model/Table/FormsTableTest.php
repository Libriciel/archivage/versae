<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Model\Table;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\TestSuite\VolumeSample;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Versae\Model\Table\FormExtractorsTable;
use Versae\Model\Table\FormsTable;
use Versae\Model\Table\FormUnitsTable;

/**
 * Versae\Model\Table\FormVariablesTable Test Case
 */
class FormsTableTest extends TestCase
{
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_FORMS,
        'app.ArchivingSystems',
        'app.Deposits',
        'app.OrgEntities',
        'app.Users',
    ];
    /**
     * @var array|EntityInterface|null
     */
    private $original;
    /**
     * @var array|EntityInterface|null
     */
    private $copy;

    public function testGetNewerForm()
    {
        /** @var FormsTable $Forms */
        $Forms = TableRegistry::getTableLocator()->get('Forms');
        $Forms->saveOrFail(
            $originalForm = $Forms->newEntity(
                [
                    'org_entity_id' => 2,
                    'name' => 'testNewForm',
                    'description' => 'Lorem ipsum dolor sit amet',
                    'archiving_system_id' => 1,
                    'identifier' => 'testNewForm',
                    'version_number' => 1,
                    'version_note' => 'Lorem ipsum dolor sit amet',
                    'state' => FormsTable::S_EDITING,
                    'transferring_agencies' => ['_ids' => [2]],
                ]
            ),
            [
                'associated' => [
                    'TransferringAgencies' => ['fields' => ['_ids']],
                ],
            ]
        );

        $newForm = $Forms->version($originalForm->get('id'), 'newVersion');
        $newForm->set('state', FormsTable::S_PUBLISHED);
        $Forms->saveOrFail($newForm);
        $result = $Forms->getNewerForm($originalForm);
        $this->assertEquals($newForm->get('id'), $result->get('id'));

        // deuxième passe
        $newForm = $Forms->version($newForm->get('id'), 'newVersion');
        $newForm->set('state', FormsTable::S_PUBLISHED);
        $Forms->saveOrFail($newForm);
        $result = $Forms->getNewerForm($originalForm);
        $this->assertEquals($newForm->get('id'), $result->get('id'));
    }

    public function testDeletingNewVersionWillDeactivateThePreviousOne()
    {
        /** @var FormsTable $Forms */
        $Forms = TableRegistry::getTableLocator()->get('Forms');

        $oldFormId = 6;
        $Forms->publish($oldFormId);
        $newForm = $Forms->version($oldFormId, 'new');

        $Forms->publish($newForm->get('id'));

        $oldForm = $Forms->get($oldFormId);
        $this->assertEquals(FormsTable::S_DEPRECATED, $oldForm->get('state'));

        $Forms->delete($newForm);

        $oldForm = $Forms->get($oldFormId);
        $this->assertEquals(FormsTable::S_DEACTIVATED, $oldForm->get('state'));
    }

    public function testDuplicate(): void
    {
        /** @var FormsTable $Forms */
        $Forms = TableRegistry::getTableLocator()->get('Forms');
        $contains = [
            'TransferringAgencies',
            'FormTransferHeaders',
            'FormFieldsets' => ['FormInputs'],
            'FormCalculators' => [
                'ManyFormVariables'
            ],
            'FormExtractors' => [
                'FormVariables',
            ],
            'FormVariables' => [
                'ManyFormCalculators',
                'FormInputs',
                'FormExtractors',
            ],
            'FormUnits' => [
                'ParentFormUnits',
                'FormUnitContents',
                'FormUnitKeywords' => 'FormUnitKeywordDetails',
                'FormUnitHeaders',
                'FormUnitManagements',
            ],
            'FormInputs' => [
                'FormExtractors',
                'FormVariables' => [
                    'FormTransferHeaders',
                    'FormUnitHeaders',
                    'FormUnitContents',
                    'FormUnitManagements',
                ],
            ],
        ];
        $original = $Forms->find()
            ->where(['id' => 1])
            ->contain($contains)
            ->first();
        $copy = $Forms->duplicate(1);
        $this->assertEmpty($copy->getErrors());
        $copy = $Forms->find()
            ->where(['id' => $copy->get('id')])
            ->contain($contains)
            ->first();

        $this->original = $original;
        $this->copy = $copy;

        /**
         * Form
         */
        $this->assertEquals($original->get('name') . '_copy_', substr($copy->get('name'), 0, -strlen(uniqid())));
        $this->assertNotEquals($original->get('identifier'), $copy->get('identifier'));
        $this->assertEquals($original->get('archiving_system_id'), $copy->get('archiving_system_id'));
        $this->checkEquality('transferring_agencies.{n}.id');

        /**
         * FormFieldsets
         */
        $this->checkEquality('form_fieldsets.{n}.legend');
        $this->checkEquality('form_fieldsets.{n}.form_inputs.{n}.name');

        /**
         * FormInputs
         */
        $this->checkEquality('form_inputs.{n}.name');
        $this->checkEquality('form_inputs.{n}.app_meta');
        $this->checkEquality('form_inputs.{n}.form_extractors.{n}.name');
        $this->checkEquality('form_inputs.{n}.form_extractors.{n}.app_meta');
        $this->checkEquality('form_inputs.{n}.form_variables.{n}.type');
        $this->checkEquality('form_inputs.{n}.form_variables.{n}.twig');

        // FIXME
//        $this->checkEquality('form_inputs.{n}.form_variables.{n}.form_transfer_header.name');
//        $this->checkEquality('form_inputs.{n}.form_variables.{n}.form_unit_header.name');
//        $this->checkEquality('form_inputs.{n}.form_variables.{n}.form_unit_content.name');
//        $this->checkEquality('form_inputs.{n}.form_variables.{n}.form_unit_management.name');

        /**
         * FormExtractors
         */
        $this->checkEquality('form_extractors.{n}.name');
        $this->checkEquality('form_extractors.{n}.app_meta');
        $this->checkEquality('form_extractors.{n}.form_variables.{n}.type');
        $this->checkEquality('form_extractors.{n}.form_variables.{n}.twig');

        /**
         * FormCalculators
         */
        $this->checkEquality('form_calculators.{n}.name');
        $this->checkEquality('form_calculators.{n}.many_form_variables.{n}.type');
        $this->checkEquality('form_calculators.{n}.many_form_variables.{n}.twig');

        /**
         * FormTransferHeaders
         */
        $this->checkEquality('form_transfer_headers.{n}.name');

        /**
         * Form Units
         */
        $this->checkEquality('form_units.{n}.name');
        $this->checkEquality('form_units.{n}.parent_form_unit.name');
        $this->checkEquality('form_units.{n}.form_unit_headers.{n}.name');
        $this->checkEquality('form_units.{n}.form_unit_contents.{n}.name');
        $this->checkEquality('form_units.{n}.form_unit_managements.{n}.name');
        $this->checkEquality('form_units.{n}.form_unit_keywords.{n}.name');
        $this->checkEquality('form_units.{n}.form_unit_keywords.{n}.form_unit_keyword_details.{n}.name');
    }

    /**
     * @param string $path
     */
    protected function checkEquality(string $path)
    {
        $this->assertEquals(
            $results = Hash::extract($this->original, $path),
            Hash::extract($this->copy, $path)
        );
        $this->assertNotEmpty($results);
    }

    public function testTestedChange()
    {
        $Forms = TableRegistry::getTableLocator()->get('Forms');
        $Forms->saveOrFail(
            $form = $Forms->newEntity(
                [
                    'name' => 'testTested',
                    'identifier' => uuid_create(UUID_TYPE_TIME),
                    'archiving_system_id' => 1,
                    'version_number' => 1,
                    'version_note' => 'creation',
                    'state' => FormsTable::S_EDITING,
                    'transferring_agencies' => [2],
                    'org_entity_id' => 2,
                ]
            )
        );
        $formId = $form->get('id');
        $this->assertFalse($this->getFormTested($formId));
        $this->setFormTested($formId);

        // form_units
        $FormUnits = TableRegistry::getTableLocator()->get('FormUnits');
        $FormUnits->saveOrFail(
            $formUnit = $FormUnits->newEntity(
                [
                    'form_id' => $form->get('id'),
                    'name' => 'root',
                    'type' => FormUnitsTable::TYPE_SIMPLE,
                ]
            )
        );
        $this->assertFalse($this->getFormTested($formId));
        $this->setFormTested($formId);

        // form_variables
        $FormVariables = TableRegistry::getTableLocator()->get('FormVariables');
        $FormVariables->saveOrFail(
            $formVariable = $FormVariables->newEntity(
                [
                    'form_id' => $form->get('id'),
                    'type' => 'Lorem ipsum dolor sit amet',
                    'twig' => 'Lorem ipsum dolor sit amet, aliquet feugiat.',
                    'app_meta' => '[]',
                ]
            )
        );
        $this->assertFalse($this->getFormTested($formId));
        $this->setFormTested($formId);

        // form_calculators
        $FormCalculators = TableRegistry::getTableLocator()->get('FormCalculators');
        $FormCalculators->saveOrFail(
            $formCalculator = $FormCalculators->newEntity(
                [
                    'form_id' => $form->get('id'),
                    'name' => 'var_test',
                    'description' => 'Lorem ipsum',
                    'form_variable_id' => $formVariable->get('id'),
                ]
            )
        );
        $this->assertFalse($this->getFormTested($formId));
        $this->setFormTested($formId);

        // form_calculators_form_variables
        $FormCalculatorsFormVariables = TableRegistry::getTableLocator()->get('FormCalculatorsFormVariables');
        $FormCalculatorsFormVariables->saveOrFail(
            $FormCalculatorsFormVariables->newEntity(
                [
                    'form_calculator_id' => $formCalculator->get('id'),
                    'form_variable_id' => $formVariable->get('id'),
                ]
            )
        );
        $this->assertFalse($this->getFormTested($formId));
        $this->setFormTested($formId);

        // form_fieldsets
        $FormFieldsets = TableRegistry::getTableLocator()->get('FormFieldsets');
        $FormFieldsets->saveOrFail(
            $formFieldset = $FormFieldsets->newEntity(
                [
                    'form_id' => $form->get('id'),
                    'ord' => 1,
                ]
            )
        );
        $this->assertFalse($this->getFormTested($formId));
        $this->setFormTested($formId);

        // form_inputs
        $FormInputs = TableRegistry::getTableLocator()->get('FormInputs');
        $FormInputs->saveOrFail(
            $formInput = $FormInputs->newEntity(
                [
                    'form_id' => $form->get('id'),
                    'form_fieldset_id' => $formFieldset->get('id'),
                    'name' => 'foo',
                    'type' => 'text',
                    'ord' => 1,
                    'label' => 'Lorem ipsum dolor sit amet',
                    'required' => 0,
                    'readonly' => 0,
                ]
            )
        );
        $this->assertFalse($this->getFormTested($formId));
        $this->setFormTested($formId);

        // form_inputs_form_variables
        $FormInputsFormVariables = TableRegistry::getTableLocator()->get('FormInputsFormVariables');
        $FormInputsFormVariables->saveOrFail(
            $FormInputsFormVariables->newEntity(
                [
                    'form_input_id' => $formInput->get('id'),
                    'form_variable_id' => $formVariable->get('id'),
                ]
            )
        );
        $this->assertFalse($this->getFormTested($formId));
        $this->setFormTested($formId);

        // form_extractors
        $FormExtractors = TableRegistry::getTableLocator()->get('FormExtractors');
        $FormExtractors->saveOrFail(
            $formExtractor = $FormExtractors->newEntity(
                [
                    'form_id' => $form->get('id'),
                    'form_input_id' => $formInput->get('id'),
                    'name' => 'foo',
                    'type' => FormExtractorsTable::TYPE_FILE,
                    'data_format' => 'Lorem ipsum dolor sit amet',
                    'data_path' => 'Lorem ipsum dolor sit amet',
                ]
            )
        );
        $this->assertFalse($this->getFormTested($formId));
        $this->setFormTested($formId);

        // form_extractors_form_variables
        $FormExtractorsFormVariables = TableRegistry::getTableLocator()->get('FormExtractorsFormVariables');
        $FormExtractorsFormVariables->saveOrFail(
            $FormExtractorsFormVariables->newEntity(
                [
                    'form_extractor_id' => $formExtractor->get('id'),
                    'form_variable_id' => $formVariable->get('id'),
                ]
            )
        );
        $this->assertFalse($this->getFormTested($formId));
        $this->setFormTested($formId);

        // form_transfer_headers
        $FormTransferHeaders = TableRegistry::getTableLocator()->get('FormTransferHeaders');
        $FormTransferHeaders->saveOrFail(
            $FormTransferHeaders->newEntity(
                [
                    'form_id' => $form->get('id'),
                    'name' => 'foo',
                    'form_variable_id' => $formVariable->get('id'),
                ]
            )
        );
        $this->assertFalse($this->getFormTested($formId));
        $this->setFormTested($formId);

        // form_unit_keywords
        $FormUnitKeywords = TableRegistry::getTableLocator()->get('FormUnitKeywords');
        $FormUnitKeywords->saveOrFail(
            $formUnitKeyword = $FormUnitKeywords->newEntity(
                [
                    'form_unit_id' => $formUnit->get('id'),
                    'name' => 'foo',
                ]
            )
        );
        $this->assertFalse($this->getFormTested($formId));
        $this->setFormTested($formId);

        // form_unit_keyword_details
        $FormUnitKeywordDetails = TableRegistry::getTableLocator()->get('FormUnitKeywordDetails');
        $FormUnitKeywordDetails->saveOrFail(
            $FormUnitKeywordDetails->newEntity(
                [
                    'form_unit_keyword_id' => $formUnitKeyword->get('id'),
                    'name' => 'foo',
                    'form_variable_id' => $formVariable->get('id'),
                ]
            )
        );
        $this->assertFalse($this->getFormTested($formId));
        $this->setFormTested($formId);

        // form_unit_contents
        $FormUnitContents = TableRegistry::getTableLocator()->get('FormUnitContents');
        $FormUnitContents->saveOrFail(
            $FormUnitContents->newEntity(
                [
                    'form_unit_id' => $formUnit->get('id'),
                    'name' => 'foo',
                    'form_variable_id' => $formVariable->get('id'),
                ]
            )
        );
        $this->assertFalse($this->getFormTested($formId));
        $this->setFormTested($formId);

        // form_unit_headers
        $FormUnitHeaders = TableRegistry::getTableLocator()->get('FormUnitHeaders');
        $FormUnitHeaders->saveOrFail(
            $FormUnitHeaders->newEntity(
                [
                    'form_unit_id' => $formUnit->get('id'),
                    'name' => 'foo',
                    'form_variable_id' => $formVariable->get('id'),
                ]
            )
        );
        $this->assertFalse($this->getFormTested($formId));
        $this->setFormTested($formId);

        // form_unit_managements
        $FormUnitManagements = TableRegistry::getTableLocator()->get('FormUnitManagements');
        $FormUnitManagements->saveOrFail(
            $FormUnitManagements->newEntity(
                [
                    'form_unit_id' => $formUnit->get('id'),
                    'name' => 'foo',
                    'form_variable_id' => $formVariable->get('id'),
                ]
            )
        );
        $this->assertFalse($this->getFormTested($formId));
        $this->setFormTested($formId);
    }

    protected function getFormTested(int $formId): bool
    {
        return TableRegistry::getTableLocator()->get('Forms')
            ->find()
            ->select('tested')
            ->where(['id' => $formId])
            ->firstOrFail()
            ->get('tested');
    }

    protected function setFormTested(int $formId): void
    {
        TableRegistry::getTableLocator()->get('Forms')
            ->updateAll(
                ['tested' => true],
                ['id' => $formId]
            );
    }

    public function testExport()
    {
        /** @var FormsTable $Forms */
        $Forms = $this->fetchTable('Forms');
        $export = $Forms->export(1);
        $this->assertEquals('testunit_editing', Hash::get($export, '0.identifier'));
        $this->assertEquals('concat', Hash::get($export, '0.form_vars.0.type'));
        $this->assertEquals('input_test', Hash::get($export, '0.form_fieldsets.0.form_inputs.0.name'));
        $this->assertEquals('extractor_test', Hash::get($export, '0.form_extractors.0.name'));
        $this->assertEquals('root', Hash::get($export, '0.form_units.0.name'));
        $this->assertEquals('simple', Hash::get($export, '0.form_units.1.name'));
        $this->assertEquals('Comment', Hash::get($export, '0.form_transfer_headers.0.name'));
    }

    public function testImport()
    {
        /** @var FormsTable $Forms */
        $Forms = $this->fetchTable('Forms');
        $initialCount = $Forms->find()
            ->where(['identifier' => 'testunit_editing'])
            ->count();
        $importData = VolumeSample::getSampleContent('export_form_fixtures.json');
        $importData = json_decode($importData, true);
        $importData = end($importData);
        $importData += [
            'transferring_agencies' => [2],
            'archiving_system_id' => 1,
            'state' => 'test',
            'org_entity_id' => 2,
        ];
        $success = $Forms->import($importData);
        $this->assertTrue($success);
        $this->assertCount(
            $initialCount + 1,
            $Forms->find()
                ->where(['identifier' => 'testunit_editing'])
        );

        /**
         * Test disable_expression
         */
        $initialCount = $this->fetchTable('FormFieldsets')
            ->find()
            ->where(['disable_expression like' => '%test%foo%bar%'])
            ->count();
        $importData['form_fieldsets'][0]['disable_expression'] = ['test' => ['foo' => ['bar']]];
        $success = $Forms->import($importData);
        $this->assertTrue($success);
        $this->assertCount(
            $initialCount + 1,
            $this->fetchTable('FormFieldsets')
                ->find()
                ->where(['disable_expression like' => '%test%foo%bar%'])
        );
    }
}
