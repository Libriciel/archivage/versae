<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Model\Table;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Versae\Model\Table\FormUnitsTable;

/**
 * Versae\Model\Table\FormUnitsTableTest Test Case
 */
class FormUnitsTableTest extends TestCase
{
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_FORMS,
    ];

    public function testGetTypesOptions(): void
    {
        /** @var FormUnitsTable $FormUnits */
        $FormUnits = TableRegistry::getTableLocator()->get('FormUnits');
        $options = $FormUnits->getTypesOptions();
        $this->assertArrayHasKey(FormUnitsTable::TYPE_SIMPLE, $options);

        $parent = $FormUnits->newEntity(
            [
                'form_id' => 1,
                'lft' => 1,
                'rght' => 2,
            ]
        );
        $parent->set('type', FormUnitsTable::TYPE_TREE_ROOT);
        $options = $FormUnits->getTypesOptions($parent);
        $this->assertArrayNotHasKey(FormUnitsTable::TYPE_SIMPLE, $options);
        $this->assertArrayHasKey(FormUnitsTable::TYPE_TREE_SEARCH, $options);

        $parent->set('type', FormUnitsTable::TYPE_SIMPLE);
        $options = $FormUnits->getTypesOptions($parent);
        $this->assertArrayHasKey(FormUnitsTable::TYPE_SIMPLE, $options);
        $this->assertArrayNotHasKey(FormUnitsTable::TYPE_TREE_SEARCH, $options);

        $parent->set('type', FormUnitsTable::TYPE_TREE_SEARCH);
        $options = $FormUnits->getTypesOptions($parent);
        $this->assertCount(0, $options);
    }
}
