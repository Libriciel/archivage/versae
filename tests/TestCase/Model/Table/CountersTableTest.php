<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Model\Table;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use DateTime;
use Versae\Model\Table\CountersTable;

class CountersTableTest extends TestCase
{
    use AutoFixturesTrait;

    public $appAutoFixtures = [
        'Sequences',
        'Counters',
        'OrgEntities',
    ];
    public $appAppendFixtures = [
    ];

    public function testGenerate()
    {
        /** @var CountersTable $Counters */
        $Counters = TableRegistry::getTableLocator()->get('Counters');

        $counter = $Counters->generate(125, 'TEST_#s#');
        $this->assertEquals('TEST_125', $counter);

        $counter = $Counters->generate(133, '#AAAA#-#MM#-#JJ#_TEST_#s#');
        $this->assertEquals(date('Y-m-d') . '_TEST_133', $counter);

        $counter = $Counters->generate(246, '#J##M##AA#_TEST_#s#');
        $this->assertEquals(date('jny') . '_TEST_246', $counter);

        $date = new DateTime('2010-01-01');
        $counter = $Counters->generate(246, '#J#/#M#/#AA#_TEST_#s#', $date);
        $this->assertEquals('1/1/10_TEST_246', $counter);

        $counter = $Counters->generate(389, 'TEST#SS#');
        $this->assertEquals('TEST89', $counter);
        $counter = $Counters->generate(389, 'TEST#SSS#');
        $this->assertEquals('TEST389', $counter);
        $counter = $Counters->generate(389, 'TEST#SSSS#');
        $this->assertEquals('TEST_389', $counter);
        $counter = $Counters->generate(389, 'TEST#SSSSS#');
        $this->assertEquals('TEST__389', $counter);

        $counter = $Counters->generate(389, 'TEST#00#');
        $this->assertEquals('TEST89', $counter);
        $counter = $Counters->generate(389, 'TEST#000#');
        $this->assertEquals('TEST389', $counter);
        $counter = $Counters->generate(389, 'TEST#0000#');
        $this->assertEquals('TEST0389', $counter);
        $counter = $Counters->generate(389, 'TEST#00000#');
        $this->assertEquals('TEST00389', $counter);
    }

    public function testNext()
    {
        /** @var CountersTable $Counters */
        $Counters = TableRegistry::getTableLocator()->get('Counters');
        $counter = $Counters->get(1);
        $counter->set(
            'definition_mask',
            'TEST_#TestString#_#AAAA#-#MM#-#JJ#_#AA#-#M#-#J#x#S#x#SS#x#SSS#xxx#0#x#00#x#000#xxx#s#'
        );
        $counter->set(
            'sequence_reset_mask',
            '#AAAA#'
        );
        $Counters->saveOrFail($counter);
        $Sequences = TableRegistry::getTableLocator()->get('Sequences');
        $params = [
            'date' => new DateTime('2010-01-01'),
            'replace' => ['TestString' => 'Foo'],
        ];
        $output = $Counters->next(2, 'ArchiveTransfer', $params);
        $this->assertEquals("TEST_Foo_2010-01-01_10-1-1x1x_1x__1xxx1x01x001xxx1", $output);

        $sequenceValue = $Sequences->find()
            ->innerJoinWith('Counters')
            ->where(['Counters.identifier' => 'ArchiveTransfer'])
            ->first()
            ->get('value');
        $this->assertEquals(1, $sequenceValue);

        $output = $Counters->next(2, 'ArchiveTransfer', $params);
        $this->assertEquals("TEST_Foo_2010-01-01_10-1-1x2x_2x__2xxx2x02x002xxx2", $output);

        $sequenceValue = $Sequences->find()
            ->innerJoinWith('Counters')
            ->where(['Counters.identifier' => 'ArchiveTransfer'])
            ->first()
            ->get('value');
        $this->assertEquals(2, $sequenceValue);

        $params['value'] = 101;
        $output = $Counters->next(2, 'ArchiveTransfer', $params);
        $this->assertEquals("TEST_Foo_2010-01-01_10-1-1x1x01x101xxx1x01x101xxx101", $output);

        $sequenceValue = $Sequences->find()
            ->innerJoinWith('Counters')
            ->where(['Counters.identifier' => 'ArchiveTransfer'])
            ->first()
            ->get('value');
        $this->assertEquals(101, $sequenceValue);

        $params['value'] = null;
        $params['date'] = new DateTime('2012-02-15'); // sequence_reset_mask = #AAAA#
        $output = $Counters->next(2, 'ArchiveTransfer', $params);
        $this->assertEquals("TEST_Foo_2012-02-15_12-2-15x1x_1x__1xxx1x01x001xxx1", $output);
    }
}
