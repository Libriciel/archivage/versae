<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Model\Table;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Versae\Model\Table\FormTransferHeadersTable;

/**
 * Versae\Model\Table\FormTransferHeadersTable Test Case
 */
class FormTransferHeadersTableTest extends TestCase
{
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_FORMS,
    ];

    /**
     * Test subject
     *
     * @var \Versae\Model\Table\FormTransferHeadersTable
     */
    protected $FormTransferHeaders;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('FormTransferHeaders')
            ? []
            : ['className' => FormTransferHeadersTable::class];
        $this->FormTransferHeaders = $this->getTableLocator()->get('FormTransferHeaders', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->FormTransferHeaders);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \Versae\Model\Table\FormTransferHeadersTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $entity = $this->FormTransferHeaders->find()
            ->where(['FormTransferHeaders.id' => 1])
            ->contain(
                [
                    'Forms',
                    'FormVariables',
                ]
            )
            ->first();
        $this->assertFalse($entity->hasErrors());
    }
}
