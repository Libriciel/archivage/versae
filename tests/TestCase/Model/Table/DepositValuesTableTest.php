<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Model\Table;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Versae\Model\Table\DepositValuesTable;
use Versae\Model\Table\FormInputsTable;

class DepositValuesTableTest extends TestCase
{
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_FORMS,
        'app.DepositValues',
        'app.Deposits',
    ];

    public function testRequiredFormInputMeanRequiredDepositValue()
    {

        /** @var FormInputsTable $FormInputs */
        $FormInputs = TableRegistry::getTableLocator()->get('FormInputs');
        /** @var DepositValuesTable $DepositValues */
        $DepositValues = TableRegistry::getTableLocator()->get('DepositValues');
        $ord = $FormInputs->find()
                ->where(['form_fieldset_id' => 1])
                ->orderDesc('ord')
                ->firstOrFail()
                ->get('ord') + 1;

        $FormInputs->saveOrFail(
            $formInput = $FormInputs->newEntity(
                [
                    'form_id' => 1,
                    'form_fieldset_id' => 1,
                    'name' => 'input',
                    'type' => FormInputsTable::TYPE_TEXT,
                    'required' => true,
                    'readonly' => false,
                    'ord' => $ord,
                    'label' => 'foo',
                ]
            )
        );

        $success = $DepositValues->save(
            $entity = $DepositValues->newEntity(
                [
                    'form_input_id' => $formInput->get('id'),
                    'deposit_id' => 1,
                    'input_value' => '',
                ]
            )
        );
        $this->assertFalse($success);
        $this->assertEquals(
            ['input_value' => ['custom' => 'Le champ est obligatoire']],
            $entity->getErrors()
        );

        $DepositValues->save(
            $entity = $DepositValues->newEntity(
                [
                    'form_input_id' => $formInput->get('id'),
                    'deposit_id' => 1,
                    'input_value' => 'bar',
                ]
            )
        );
        $this->assertEmpty($entity->getErrors());

        $FormInputs->updateAll(
            ['required' => false],
            ['id' => $formInput->get('id')]
        );

        $DepositValues->save(
            $entity = $DepositValues->newEntity(
                [
                    'form_input_id' => $formInput->get('id'),
                    'deposit_id' => 1,
                    'input_value' => '',
                ]
            )
        );
        $this->assertEmpty($entity->getErrors());
    }
}
