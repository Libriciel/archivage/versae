<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Model\Table;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Versae\Model\Table\FormVariablesTable;

/**
 * Versae\Model\Table\FormCalculatorsTable Test Case
 */
class FormCalculatorsTableTest extends TestCase
{
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_FORMS,
    ];

    public function testAfterSave(): void
    {
        $FormVariables = TableRegistry::getTableLocator()->get('FormVariables');
        $FormCalculators = TableRegistry::getTableLocator()->get('FormCalculators');
        $entity = $FormVariables->newEmptyEntity();
        $FormVariables->patchEntity(
            $entity,
            [
                'form_calculator' => [
                    'name' => 'test_replace_cond',
                    'form_id' => 1,
                ],
                'form_id' => 1,
                'type' => FormVariablesTable::TYPE_CONDITIONS,
                'twig' => '{% if var.var_test != null %}foo{% else %}bar{% endif %}', // fixture id=1
                'conditions_if_value' => 'foo',
                'conditions' => [
                    'if' => [
                        [
                            'part' => [
                                [
                                    'type' => 'field',
                                    'field' => 'var.var_test',
                                    'field_tolower' => true,
                                    'value' => '',
                                    'operator' => '!=',
                                ],
                                [
                                    'type' => 'null',
                                    'field' => '',
                                    'value' => '',
                                ],
                            ]
                        ],
                    ],
                ],
                'conditions_else_value' => 'bar',
            ]
        );
        $FormVariables->saveOrFail($entity);
        $this->assertEquals('var.var_test', Hash::get($entity->get('conditions'), 'if.0.part.0.field'));

        $FormCalculatorsFormVariables = TableRegistry::getTableLocator()->get('FormCalculatorsFormVariables');
        $FormCalculatorsFormVariables->deleteAll([]);
        $FormCalculatorsFormVariables->findOrCreate(['form_variable_id' => $entity->id, 'form_calculator_id' => 1]);

        $e1 = $FormCalculators->get(1);
        $e1->set('name', 'foo');
        $FormCalculators->saveOrFail($e1);

        $entity = $FormVariables->get($entity->id);
        $this->assertEquals('{% if var.foo != null %}foo{% else %}bar{% endif %}', $entity->get('twig'));
        $this->assertEquals('var.foo', Hash::get($entity->get('conditions'), 'if.0.part.0.field'));
    }
}
