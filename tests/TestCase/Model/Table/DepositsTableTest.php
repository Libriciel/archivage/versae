<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Model\Table;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\I18n\FrozenTime;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Libriciel\Filesystem\Utility\Filesystem;
use Versae\Model\Entity\DepositValue;
use Versae\Model\Table\DepositsTable;
use Versae\Model\Table\DepositValuesTable;
use Versae\Model\Table\FileuploadsTable;
use Versae\Model\Table\FormFieldsetsTable;
use Versae\Model\Table\FormInputsTable;
use Versae\Model\Table\FormsTable;
use Versae\Model\Table\TransfersTable;

class DepositsTableTest extends TestCase
{
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_FORMS,
        'app.ArchivingSystems',
        'app.DepositValues',
        'app.Deposits',
        'app.Fileuploads',
        'app.OrgEntities',
        'app.Transfers',
        'app.Users',
    ];

    public function setUp(): void
    {
        parent::setUp();
        Configure::write('App.paths.data', TMP_TESTDIR);
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Filesystem::mkdir(TMP_TESTDIR);
        Filesystem::dumpFile(TMP_TESTDIR . '/deposits/5_12345678/transfer.xml', 'test');
        Filesystem::dumpFile(TMP_TESTDIR . '/deposits/5_12345678/transfer.tar.gz', 'test');
    }

    public function tearDown(): void
    {
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        parent::tearDown();
    }

    public function testReeditWithNewForm()
    {
        /** @var FormsTable $Forms */
        $Forms = TableRegistry::getTableLocator()->get('Forms');
        /** @var DepositsTable $Deposits */
        $Deposits = TableRegistry::getTableLocator()->get('Deposits');
        /** @var FormInputsTable $FormInputs */
        $FormInputs = TableRegistry::getTableLocator()->get('FormInputs');
        /** @var FormFieldsetsTable $FormFieldsets */
        $FormFieldsets = TableRegistry::getTableLocator()->get('FormFieldsets');
        /** @var DepositValuesTable $DepositValues */
        $DepositValues = TableRegistry::getTableLocator()->get('DepositValues');
        /** @var TransfersTable $Transfers */
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');

        $initialDepositValuesCount = $DepositValues->find()->all()->count();

        $initialForm = $Forms->newEntity(
            [
                'name' => 'initial_form',
                'identifier' => uuid_create(UUID_TYPE_TIME),
                'archiving_system_id' => 1,
                'version_number' => 1,
                'version_note' => 'initial',
                'state' => $Forms::S_EDITING,
                'title' => 'initial',
                'org_entity_id' => 2,
                'transferring_agencies' => [2],
            ]
        );
        $Forms->saveOrFail($initialForm);

        $FormFieldsets->saveOrFail(
            $fieldset = $FormFieldsets->newEntity(
                [
                    'form_id' => $initialForm->get('id'),
                    'ord' => 0,
                ]
            )
        );

        $inputs = $FormInputs->newEntities(
            [
                [
                    'form_id' => $initialForm->get('id'),
                    'name' => 'a1',
                    'type' => 'text',
                    'ord' => 0,
                    'label' => '1',
                    'required' => true,
                    'readonly' => false,
                    'form_fieldset_id' => $fieldset->get('id'),
                ],
                [
                    'form_id' => $initialForm->get('id'),
                    'name' => 'a2',
                    'type' => 'text',
                    'ord' => 1,
                    'label' => '2',
                    'required' => true,
                    'readonly' => false,
                    'form_fieldset_id' => $fieldset->get('id'),
                ],
            ]
        );
        $FormInputs->saveManyOrFail($inputs);

        $newForm = $Forms->saveOrFail(
            $Forms->newEntity(
                [
                    'name' => 'initial_form',
                    'identifier' => $initialForm->get('identifier'),
                    'archiving_system_id' => 1,
                    'version_number' => 2,
                    'version_note' => 'initial',
                    'state' => $Forms::S_EDITING,
                    'title' => 'initial',
                    'org_entity_id' => 2,
                    'transferring_agencies' => [2],
                ]
            )
        );
        $FormFieldsets->saveOrFail(
            $newFieldset = $FormFieldsets->newEntity(
                [
                    'form_id' => $newForm->get('id'),
                    'ord' => 0,
                ]
            )
        );
        $newInputs = $FormInputs->newEntities(
            [
                [
                    'form_id' => $newForm->get('id'),
                    'name' => 'a2',
                    'type' => 'number',
                    'ord' => 0,
                    'label' => '2',
                    'required' => true,
                    'readonly' => false,
                    'form_fieldset_id' => $newFieldset->get('id'),
                ],
                [
                    'form_id' => $newForm->get('id'),
                    'name' => 'a3',
                    'type' => 'number',
                    'ord' => 1,
                    'label' => '3',
                    'required' => true,
                    'readonly' => false,
                    'form_fieldset_id' => $newFieldset->get('id'),
                ],
            ]
        );
        $FormInputs->saveManyOrFail($newInputs);

        $deposit = $Deposits->newEntity(
            [
                'form_id' => $initialForm->get('id'),
                'name' => 'deposit',
                'state' => $Deposits::S_PREPARING_ERROR,
                'archival_agency_id' => 2,
                'transferring_agency_id' => 2,
                'created_user_id' => 1,
                'bypass_validation' => true,
            ]
        );
        $Deposits->saveOrFail($deposit);
        foreach ($inputs as $input) {
            $DepositValues->saveOrFail(
                $DepositValues->newEntity(
                    [
                        'deposit_id' => $deposit->get('id'),
                        'form_input_id' => $input->get('id'),
                        'input_value' => 'lorem',
                    ]
                )
            );
        }

        $transfer = $Transfers->newEntity(
            [
                'deposit_id' => $deposit->get('id'),
                'identifier' => 'identifier',
                'created_user_id' => 1,
            ]
        );
        $Transfers->saveOrFail($transfer);

        $success = $Deposits->reeditWithNewForm($deposit, $newForm);
        $this->assertEmpty($deposit->getErrors());
        $this->assertTrue($success);
        $this->assertEquals($newForm->get('id'), $deposit->get('form_id'));

        $depositValues = $DepositValues->find()
            ->select(['FormInputs.name'])
            ->contain(['FormInputs'])
            ->where(['DepositValues.deposit_id' => $deposit->get('id')])
            ->order(['DepositValues.id'])
            ->all()
            ->map(
                function (DepositValue $entity) {
                    return Hash::get($entity, 'form_input.name');
                }
            )
            ->toArray();

        // il ne doit rester plus qu'une deposit_value pour ce deposit
        $this->assertEquals(['a2'], $depositValues);
        $this->assertEquals($initialDepositValuesCount + 1, $DepositValues->find()->all()->count());
    }

    public function testReeditWithNewFormWithSameInputs()
    {
        /** @var FormsTable $Forms */
        $Forms = TableRegistry::getTableLocator()->get('Forms');
        /** @var DepositsTable $Deposits */
        $Deposits = TableRegistry::getTableLocator()->get('Deposits');
        /** @var FormInputsTable $FormInputs */
        $FormInputs = TableRegistry::getTableLocator()->get('FormInputs');
        /** @var FormFieldsetsTable $FormFieldsets */
        $FormFieldsets = TableRegistry::getTableLocator()->get('FormFieldsets');
        /** @var DepositValuesTable $DepositValues */
        $DepositValues = TableRegistry::getTableLocator()->get('DepositValues');
        /** @var TransfersTable $Transfers */
        $Transfers = TableRegistry::getTableLocator()->get('Transfers');

        $initialDepositValuesCount = $DepositValues->find()->all()->count();

        $initialForm = $Forms->newEntity(
            [
                'name' => 'initial_form',
                'identifier' => uuid_create(UUID_TYPE_TIME),
                'archiving_system_id' => 1,
                'version_number' => 1,
                'version_note' => 'initial',
                'state' => $Forms::S_EDITING,
                'title' => 'initial',
                'org_entity_id' => 2,
                'transferring_agencies' => [2],
            ]
        );
        $Forms->saveOrFail($initialForm);

        $FormFieldsets->saveOrFail(
            $fieldset = $FormFieldsets->newEntity(
                [
                    'form_id' => $initialForm->get('id'),
                    'ord' => 0,
                ]
            )
        );

        $inputs = $FormInputs->newEntities(
            [
                [
                    'form_id' => $initialForm->get('id'),
                    'name' => 'a1',
                    'type' => 'text',
                    'ord' => 0,
                    'label' => '1',
                    'required' => true,
                    'readonly' => false,
                    'form_fieldset_id' => $fieldset->get('id'),
                ],
                [
                    'form_id' => $initialForm->get('id'),
                    'name' => 'a2',
                    'type' => 'text',
                    'ord' => 1,
                    'label' => '2',
                    'required' => true,
                    'readonly' => false,
                    'form_fieldset_id' => $fieldset->get('id'),
                ],
            ]
        );
        $FormInputs->saveManyOrFail($inputs);

        $newForm = $Forms->saveOrFail(
            $Forms->newEntity(
                [
                    'name' => 'initial_form',
                    'identifier' => $initialForm->get('identifier'),
                    'archiving_system_id' => 1,
                    'version_number' => 2,
                    'version_note' => 'initial',
                    'state' => $Forms::S_EDITING,
                    'title' => 'initial',
                    'org_entity_id' => 2,
                    'transferring_agencies' => [2],
                ]
            )
        );
        $FormFieldsets->saveOrFail(
            $newFieldset = $FormFieldsets->newEntity(
                [
                    'form_id' => $newForm->get('id'),
                    'ord' => 0,
                ]
            )
        );
        $newInputs = $FormInputs->newEntities(
            [
                [
                    'form_id' => $newForm->get('id'),
                    'name' => 'a1',
                    'type' => 'text',
                    'ord' => 0,
                    'label' => '1',
                    'required' => true,
                    'readonly' => false,
                    'form_fieldset_id' => $newFieldset->get('id'),
                ],
                [
                    'form_id' => $newForm->get('id'),
                    'name' => 'a2',
                    'type' => 'text',
                    'ord' => 1,
                    'label' => '2',
                    'required' => true,
                    'readonly' => false,
                    'form_fieldset_id' => $newFieldset->get('id'),
                ],
            ]
        );
        $FormInputs->saveManyOrFail($newInputs);

        $deposit = $Deposits->newEntity(
            [
                'form_id' => $initialForm->get('id'),
                'name' => 'deposit',
                'state' => $Deposits::S_PREPARING_ERROR,
                'archival_agency_id' => 2,
                'transferring_agency_id' => 2,
                'created_user_id' => 1,
                'bypass_validation' => true,
            ]
        );
        $Deposits->saveOrFail($deposit);
        foreach ($inputs as $input) {
            $DepositValues->saveOrFail(
                $DepositValues->newEntity(
                    [
                        'deposit_id' => $deposit->get('id'),
                        'form_input_id' => $input->get('id'),
                        'input_value' => 'lorem',
                    ]
                )
            );
        }

        $transfer = $Transfers->newEntity(
            [
                'deposit_id' => $deposit->get('id'),
                'identifier' => 'identifier',
                'created_user_id' => 1,
            ]
        );
        $Transfers->saveOrFail($transfer);

        $success = $Deposits->reeditWithNewForm($deposit, $newForm);
        $this->assertEmpty($deposit->getErrors());
        $this->assertTrue($success);
        $this->assertEquals($newForm->get('id'), $deposit->get('form_id'));

        $depositValues = $DepositValues->find()
            ->select(['FormInputs.name'])
            ->contain(['FormInputs'])
            ->where(['DepositValues.deposit_id' => $deposit->get('id')])
            ->order(['DepositValues.id'])
            ->all()
            ->map(
                function (DepositValue $entity) {
                    return Hash::get($entity, 'form_input.name');
                }
            )
            ->toArray();

        $this->assertEquals(['a1', 'a2'], $depositValues);
        $this->assertEquals($initialDepositValuesCount + 2, $DepositValues->find()->all()->count());
    }

    public function testDelete()
    {
        $this->assertFileExists(TMP_TESTDIR . DS . 'deposits' . DS . '5_12345678' . DS . 'transfer.xml');
        $this->assertFileExists(TMP_TESTDIR . DS . 'deposits/5_12345678' . DS . 'transfer.tar.gz');

        /** @var DepositsTable $Deposits */
        $Deposits = TableRegistry::getTableLocator()->get('Deposits');
        $Deposits->deleteOrFail($Deposits->get(5));
        $this->assertFileDoesNotExist(TMP_TESTDIR . DS . 'deposits' . DS . '5_12345678' . DS . 'transfer.xml');
        $this->assertFileDoesNotExist(TMP_TESTDIR . DS . 'deposits/5_12345678' . DS . 'transfer.tar.gz');
    }

    public function testRepeatableFieldsets()
    {
        $data = [
            'form_id' => 7,
            'name' => 'deposit_test',
            'transferring_agency_id' => 2,
            'archival_agency_id' => 2,
            'state' => DepositsTable::S_EDITING,
            'created_user_id' => 1,
            'last_state_update' => new FrozenTime(),
            'bypass_validation' => true,
        ];
        /** @var DepositsTable $Deposits */
        $Deposits = $this->fetchTable('Deposits');
        $entity = $Deposits->newEntity($data);
        $Deposits->saveOrFail($entity);

        /** @var FileuploadsTable $Fileuploads */
        $Fileuploads = $this->fetchTable('Fileuploads');
        Filesystem::dumpFile(TMP_TESTDIR . DS . 'test1.txt', 'test');
        $file1 = $Fileuploads->newUploadedEntity(
            'test1.txt',
            TMP_TESTDIR . DS . 'test1.txt',
            1
        );
        $Fileuploads->saveOrFail($file1);
        Filesystem::dumpFile(TMP_TESTDIR . DS . 'test2.txt', 'test');
        $file2 = $Fileuploads->newUploadedEntity(
            'test2.txt',
            TMP_TESTDIR . DS . 'test2.txt',
            1
        );
        $Fileuploads->saveOrFail($file2);

        $Deposits->updateDeposit(
            $entity,
            [
                'archival_agency_id' => 2,
                'state' => DepositsTable::S_EDITING,
                'created_user_id' => 1,
                'inputs' => [
                    'Titre' => 'test',
                    'section-1' => [
                        0 => [
                            'Commentaire' => 'test 1',
                            'fichier' => $file1->id,
                        ],
                        1 => [
                            'Commentaire' => 'test 2',
                            'fichier' => $file2->id,
                        ],
                    ],
                ]
            ]
        );
        $DepositValues = $this->fetchTable('DepositValues');
        $result = [];
        $DepositValues->find()
            ->where(['deposit_id' => $entity->id])
            ->order('form_input_id')
            ->disableHydration()
            ->all()
            ->map(
                function ($v) use (&$result) {
                    $result[$v['form_input_id']][] = $v['input_value'];
                }
            )
            ->toArray();
        $expected = [
            5 => [
                0 => 'test'
            ],
            6 => [
                0 => 'test 1',
                1 => 'test 2',
            ],
            7 => [
                0 => (string)$file1->id,
                1 => (string)$file2->id,
            ],
        ];
        $this->assertEquals($expected, $result);
    }
}
