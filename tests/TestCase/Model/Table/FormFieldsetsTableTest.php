<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Model\Table;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;

/**
 * Versae\Model\Table\FormFieldsetsTable Test Case
 */
class FormFieldsetsTableTest extends TestCase
{
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_FORMS,
        'app.ArchivingSystems',
        'app.Deposits',
        'app.OrgEntities',
        'app.Users',
    ];

    public function testNewEntity()
    {
        // newEntity avec disable_expression en array
        $FormFieldsets = $this->fetchTable('FormFieldsets');
        $fieldset = $FormFieldsets->newEntity(
            [
                'form_id' => 1,
                'legend' => 'test',
                'disable_expression' => [
                    'cond_display_select' => 'checkbox_not_checked',
                    'cond_display_input' => 'test1',
                ],
                'ord' => 990,
            ]
        );
        $FormFieldsets->saveOrFail($fieldset);
        $this->assertEquals(
            <<<JSON
{
    "cond_display_select": "checkbox_not_checked",
    "cond_display_input": "test1"
}
JSON,
            (string)$FormFieldsets->get($fieldset->id)->get('disable_expression')
        );

        // newEntity avec disable_expression en string
        $FormFieldsets = $this->fetchTable('FormFieldsets');
        $fieldset = $FormFieldsets->newEntity(
            [
                'form_id' => 1,
                'legend' => 'test',
                'disable_expression' => json_encode(
                    [
                        'cond_display_select' => 'checkbox_not_checked',
                        'cond_display_input' => 'test2',
                    ]
                ),
                'ord' => 991,
            ]
        );
        $FormFieldsets->saveOrFail($fieldset);
        $this->assertEquals(
            <<<JSON
{
    "cond_display_select": "checkbox_not_checked",
    "cond_display_input": "test2"
}
JSON,
            (string)$FormFieldsets->get($fieldset->id)->get('disable_expression')
        );

        // newEntity avec disable_expression par champs
        $FormFieldsets = $this->fetchTable('FormFieldsets');
        $fieldset = $FormFieldsets->newEntity(
            [
                'form_id' => 1,
                'legend' => 'test',
                'cond_display_select' => 'checkbox_not_checked',
                'cond_display_input' => 'test3',
                'ord' => 992,
            ]
        );
        $this->assertTrue($fieldset->isDirty('disable_expression'));
        $FormFieldsets->saveOrFail($fieldset);
        $this->assertEquals(
            <<<JSON
{
    "cond_display_select": "checkbox_not_checked",
    "cond_display_input": "test3"
}
JSON,
            (string)$FormFieldsets->get($fieldset->id)->get('disable_expression')
        );

        // newEntity avec retrait du disable_expression
        $FormFieldsets = $this->fetchTable('FormFieldsets');
        $fieldset = $FormFieldsets->newEntity(
            [
                'form_id' => 1,
                'legend' => 'test',
                'ord' => 993,
            ]
        );
        $this->assertTrue($fieldset->isDirty('disable_expression'));
        $FormFieldsets->saveOrFail($fieldset);
        $this->assertEquals(
            '[]',
            (string)$FormFieldsets->get($fieldset->id)->get('disable_expression')
        );
    }

    public function testPatchEntity()
    {
        // patchEntity avec disable_expression en array
        $FormFieldsets = $this->fetchTable('FormFieldsets');
        $fieldset = $FormFieldsets->get(1);
        $data = [
            'legend' => 'test',
            'disable_expression' => [
                'cond_display_select' => 'checkbox_not_checked',
                'cond_display_input' => 'test1',
            ],
        ];
        $FormFieldsets->patchEntity($fieldset, $data);
        $FormFieldsets->saveOrFail($fieldset);
        $this->assertEquals(
            <<<JSON
{
    "cond_display_select": "checkbox_not_checked",
    "cond_display_input": "test1"
}
JSON,
            (string)$FormFieldsets->get($fieldset->id)->get('disable_expression')
        );

        // patchEntity avec disable_expression en string
        $FormFieldsets = $this->fetchTable('FormFieldsets');
        $fieldset = $FormFieldsets->get(1);
        $data = [
            'legend' => 'test',
            'disable_expression' => json_encode(
                [
                    'cond_display_select' => 'checkbox_not_checked',
                    'cond_display_input' => 'test2',
                ]
            ),
        ];
        $FormFieldsets->patchEntity($fieldset, $data);
        $FormFieldsets->saveOrFail($fieldset);
        $this->assertEquals(
            <<<JSON
{
    "cond_display_select": "checkbox_not_checked",
    "cond_display_input": "test2"
}
JSON,
            (string)$FormFieldsets->get($fieldset->id)->get('disable_expression')
        );

        // patchEntity avec disable_expression par champs
        $FormFieldsets = $this->fetchTable('FormFieldsets');
        $fieldset = $FormFieldsets->get(1);
        $data = [
            'legend' => 'test',
            'cond_display_select' => 'checkbox_not_checked',
            'cond_display_input' => 'test3',
        ];
        $FormFieldsets->patchEntity($fieldset, $data);
        $this->assertTrue($fieldset->isDirty('disable_expression'));
        $FormFieldsets->saveOrFail($fieldset);
        $this->assertEquals(
            <<<JSON
{
    "cond_display_select": "checkbox_not_checked",
    "cond_display_input": "test3"
}
JSON,
            (string)$FormFieldsets->get($fieldset->id)->get('disable_expression')
        );

        // patchEntity avec retrait du disable_expression
        $FormFieldsets = $this->fetchTable('FormFieldsets');
        $fieldset = $FormFieldsets->get(1);
        $FormFieldsets->patchEntity($fieldset, $data);
        $this->assertTrue($fieldset->isDirty('disable_expression'));
        $data = [
            'legend' => 'test',
            'cond_display_select' => null,
            'cond_display_input' => null,
        ];
        $FormFieldsets->patchEntity($fieldset, $data);
        $this->assertTrue($fieldset->isDirty('disable_expression'));
        $FormFieldsets->saveOrFail($fieldset);
        $this->assertEquals(
            '[]',
            (string)$FormFieldsets->get($fieldset->id)->get('disable_expression')
        );
    }

    public function testSetEntity()
    {
        // newEntity avec disable_expression en array
        $FormFieldsets = $this->fetchTable('FormFieldsets');
        $fieldset = $FormFieldsets->get(1);
        $fieldset->set('cond_display_select', 'checkbox_not_checked');
        $fieldset->set('cond_display_input', 'test1');
        $FormFieldsets->saveOrFail($fieldset);
        $this->assertEquals(
            <<<JSON
{
    "cond_display_select": "checkbox_not_checked",
    "cond_display_input": "test1"
}
JSON,
            (string)$FormFieldsets->get($fieldset->id)->get('disable_expression')
        );

        // newEntity avec disable_expression en string
        $FormFieldsets = $this->fetchTable('FormFieldsets');
        $fieldset = $FormFieldsets->get(1);
        $fieldset->set('cond_display_select', 'checkbox_not_checked');
        $fieldset->set('cond_display_input', 'test2');
        $FormFieldsets->saveOrFail($fieldset);
        $this->assertEquals(
            <<<JSON
{
    "cond_display_select": "checkbox_not_checked",
    "cond_display_input": "test2"
}
JSON,
            (string)$FormFieldsets->get($fieldset->id)->get('disable_expression')
        );

        // newEntity avec disable_expression par champs
        $FormFieldsets = $this->fetchTable('FormFieldsets');
        $fieldset = $FormFieldsets->get(1);
        $fieldset->set('cond_display_select', 'checkbox_not_checked');
        $fieldset->set('cond_display_input', 'test3');
        $this->assertTrue($fieldset->isDirty('disable_expression'));
        $FormFieldsets->saveOrFail($fieldset);
        $this->assertEquals(
            <<<JSON
{
    "cond_display_select": "checkbox_not_checked",
    "cond_display_input": "test3"
}
JSON,
            (string)$FormFieldsets->get($fieldset->id)->get('disable_expression')
        );
    }
}
