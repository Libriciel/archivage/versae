<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Model\Table;

use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Exception;

class AllTableTest extends TestCase
{
    public function testAssocs()
    {
        $loc = TableRegistry::getTableLocator();
        $basePath = Configure::read('Tree.model_path', APP . 'Model' . DS . 'Table');
        foreach (glob($basePath . DS . '*Table.php') as $filename) {
            $modelname = basename($filename, 'Table.php');
            $Model = $loc->get($modelname);
            /** @var Table $assoc */
            foreach ($Model->associations() as $assoc) {
                if ($Model->getAlias() === $assoc->getAlias()) {
                    continue;
                }
                try {
                    $Model->find()
                        ->select(['existing' => 1])
                        ->innerJoinWith($assoc->getAlias())
                        ->first();
                    $success = true;
                } catch (Exception $e) {
                    $success = false;
                }
                $this->assertTrue(
                    $success,
                    sprintf(
                        'Error in Table->assoc: %s -> %s : %s',
                        $modelname,
                        $assoc->getAlias(),
                        (isset($e) ? $e->getMessage() : '')
                    )
                );
            }
        }
    }
}
