<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Model\Table;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Versae\Model\Table\FormInputsTable;
use Versae\Model\Table\FormVariablesTable;

/**
 * Versae\Model\Table\FormInputsTable Test Case
 */
class FormInputsTableTest extends TestCase
{
    use AutoFixturesTrait;

    const DATA = [
        'form_fieldset_id' => 1,
        'form_id' => 1,
        'name' => 'test',
        'type' => FormInputsTable::TYPE_TEXT,
        'disable_expression' => [
            'cond_display_select' => 'checkbox_not_checked',
            'cond_display_input' => 'test1',
        ],
        'app_meta' => [
            'empty' => 'foo',
        ],
        'ord' => 990,
        'label' => 'test',
        'default_value' => '',
        'required' => false,
        'readonly' => false,
        'multiple' => false,
    ];

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_FORMS,
    ];

    public function testAfterSave(): void
    {
        $FormVariables = TableRegistry::getTableLocator()->get('FormVariables');
        $FormInputs = TableRegistry::getTableLocator()->get('FormInputs');
        $entity = $FormVariables->newEntity(
            [
                'form_id' => 1,
                'name' => 'test_replace_cond',
                'type' => FormVariablesTable::TYPE_CONDITIONS,
                'twig' => '{% if input.input_test != null %}foo{% else %}bar{% endif %}', // fixture id=1
                'conditions_if_value' => 'foo',
                'conditions' => [
                    'if' => [
                        [
                            'part' => [
                                [
                                    'type' => 'field',
                                    'field' => 'input.input_test',
                                    'field_tolower' => true,
                                    'value' => '',
                                    'operator' => '!=',
                                ],
                                [
                                    'type' => 'null',
                                    'field' => '',
                                    'value' => '',
                                ],
                            ]
                        ],
                    ],
                ],
                'conditions_else_value' => 'bar',
            ]
        );
        $FormVariables->saveOrFail($entity);
        $this->assertEquals('input.input_test', Hash::get($entity->get('conditions'), 'if.0.part.0.field'));

        $FormInputsFormVariables = TableRegistry::getTableLocator()->get('FormInputsFormVariables');
        $FormInputsFormVariables->deleteAll([]);
        $FormInputsFormVariables->findOrCreate(['form_variable_id' => $entity->id, 'form_input_id' => 1]);

        $e1 = $FormInputs->get(1);
        $e1->set('name', 'foo');
        $FormInputs->saveOrFail($e1);

        $entity = $FormVariables->get($entity->id);
        $this->assertEquals('{% if input.foo != null %}foo{% else %}bar{% endif %}', $entity->get('twig'));
        $this->assertEquals('input.foo', Hash::get($entity->get('conditions'), 'if.0.part.0.field'));
    }

    public function testRequiredAndReadonlyIncompatible()
    {
        $FormInputs = TableRegistry::getTableLocator()->get('FormInputs');
        $FormInputs->save(
            $input = $FormInputs->newEntity(
                [
                    'required' => 1,
                    'readonly' => 1,
                ]
            )
        );

        $this->assertContains(['custom' => 'Un champ ne peut être obligatoire et en lecture seule'], $input->getErrors());
    }

    public function testNewEntity()
    {
        // newEntity avec disable_expression en array
        $FormInputs = $this->fetchTable('FormInputs');
        $defaultData = self::DATA;
        $fieldset = $FormInputs->newEntity($defaultData);
        $FormInputs->saveOrFail($fieldset);
        $this->assertEquals(
            <<<JSON
{
    "cond_display_select": "checkbox_not_checked",
    "cond_display_input": "test1"
}
JSON,
            (string)$FormInputs->get($fieldset->id)->get('disable_expression')
        );
        $this->assertEquals(
            <<<JSON
{
    "empty": "foo"
}
JSON,
            (string)$FormInputs->get($fieldset->id)->get('app_meta')
        );

        // newEntity avec disable_expression en string
        $FormInputs = $this->fetchTable('FormInputs');
        $defaultData['name'] = uniqid('test');
        $defaultData['ord']++;
        $defaultData['disable_expression'] = json_encode(
            [
                'cond_display_select' => 'checkbox_not_checked',
                'cond_display_input' => 'test2',
            ]
        );
        $fieldset = $FormInputs->newEntity($defaultData);
        $FormInputs->saveOrFail($fieldset);
        $this->assertEquals(
            <<<JSON
{
    "cond_display_select": "checkbox_not_checked",
    "cond_display_input": "test2"
}
JSON,
            (string)$FormInputs->get($fieldset->id)->get('disable_expression')
        );

        // newEntity avec disable_expression par champs
        $FormInputs = $this->fetchTable('FormInputs');
        unset($defaultData['disable_expression']);
        $defaultData['cond_display_select'] = 'checkbox_not_checked';
        $defaultData['cond_display_input'] = 'test3';
        $defaultData['name'] = uniqid('test');
        $defaultData['ord']++;
        $fieldset = $FormInputs->newEntity($defaultData);
        $this->assertTrue($fieldset->isDirty('disable_expression'));
        $FormInputs->saveOrFail($fieldset);
        $this->assertEquals(
            <<<JSON
{
    "cond_display_select": "checkbox_not_checked",
    "cond_display_input": "test3"
}
JSON,
            (string)$FormInputs->get($fieldset->id)->get('disable_expression')
        );

        // newEntity avec retrait du disable_expression
        $FormInputs = $this->fetchTable('FormInputs');
        unset($defaultData['cond_display_select']);
        unset($defaultData['cond_display_input']);
        $defaultData['name'] = uniqid('test');
        $defaultData['ord']++;
        $fieldset = $FormInputs->newEntity($defaultData);
        $this->assertTrue($fieldset->isDirty('disable_expression'));
        $FormInputs->saveOrFail($fieldset);
        $this->assertEquals(
            '[]',
            (string)$FormInputs->get($fieldset->id)->get('disable_expression')
        );
    }

    public function testPatchEntity()
    {
        // patchEntity avec disable_expression en array
        $FormInputs = $this->fetchTable('FormInputs');
        $fieldset = $FormInputs->get(1);
        $data = [
            'legend' => 'test',
            'disable_expression' => [
                'cond_display_select' => 'checkbox_not_checked',
                'cond_display_input' => 'test1',
            ],
        ];
        $FormInputs->patchEntity($fieldset, $data);
        $FormInputs->saveOrFail($fieldset);
        $this->assertEquals(
            <<<JSON
{
    "cond_display_select": "checkbox_not_checked",
    "cond_display_input": "test1"
}
JSON,
            (string)$FormInputs->get($fieldset->id)->get('disable_expression')
        );

        // patchEntity avec disable_expression en string
        $FormInputs = $this->fetchTable('FormInputs');
        $fieldset = $FormInputs->get(1);
        $data = [
            'legend' => 'test',
            'disable_expression' => json_encode(
                [
                    'cond_display_select' => 'checkbox_not_checked',
                    'cond_display_input' => 'test2',
                ],
                JSON_PRETTY_PRINT
            ),
        ];
        $FormInputs->patchEntity($fieldset, $data);
        $FormInputs->saveOrFail($fieldset);
        $this->assertEquals(
            <<<JSON
{
    "cond_display_select": "checkbox_not_checked",
    "cond_display_input": "test2"
}
JSON,
            (string)$FormInputs->get($fieldset->id)->get('disable_expression')
        );

        // patchEntity avec disable_expression par champs
        $FormInputs = $this->fetchTable('FormInputs');
        $fieldset = $FormInputs->get(1);
        $data = [
            'legend' => 'test',
            'cond_display_select' => 'checkbox_not_checked',
            'cond_display_input' => 'test3',
        ];
        $FormInputs->patchEntity($fieldset, $data);
        $this->assertTrue($fieldset->isDirty('disable_expression'));
        $FormInputs->saveOrFail($fieldset);
        $this->assertEquals(
            <<<JSON
{
    "cond_display_select": "checkbox_not_checked",
    "cond_display_input": "test3"
}
JSON,
            (string)$FormInputs->get($fieldset->id)->get('disable_expression')
        );

        // patchEntity avec retrait du disable_expression
        $FormInputs = $this->fetchTable('FormInputs');
        $fieldset = $FormInputs->get(1);
        $FormInputs->patchEntity($fieldset, $data);
        $this->assertTrue($fieldset->isDirty('disable_expression'));
        $data = [
            'legend' => 'test',
            'cond_display_select' => null,
            'cond_display_input' => null,
        ];
        $FormInputs->patchEntity($fieldset, $data);
        $this->assertTrue($fieldset->isDirty('disable_expression'));
        $FormInputs->saveOrFail($fieldset);
        $this->assertEquals(
            '[]',
            (string)$FormInputs->get($fieldset->id)->get('disable_expression')
        );
    }

    public function testSetEntity()
    {
        // newEntity avec disable_expression en array
        $FormInputs = $this->fetchTable('FormInputs');
        $fieldset = $FormInputs->get(1);
        $fieldset->set('cond_display_select', 'checkbox_not_checked');
        $fieldset->set('cond_display_input', 'test1');
        $FormInputs->saveOrFail($fieldset);
        $this->assertEquals(
            <<<JSON
{
    "cond_display_select": "checkbox_not_checked",
    "cond_display_input": "test1"
}
JSON,
            (string)$FormInputs->get($fieldset->id)->get('disable_expression')
        );

        // newEntity avec disable_expression en string
        $FormInputs = $this->fetchTable('FormInputs');
        $fieldset = $FormInputs->get(1);
        $fieldset->set('cond_display_select', 'checkbox_not_checked');
        $fieldset->set('cond_display_input', 'test2');
        $FormInputs->saveOrFail($fieldset);
        $this->assertEquals(
            <<<JSON
{
    "cond_display_select": "checkbox_not_checked",
    "cond_display_input": "test2"
}
JSON,
            (string)$FormInputs->get($fieldset->id)->get('disable_expression')
        );

        // newEntity avec disable_expression par champs
        $FormInputs = $this->fetchTable('FormInputs');
        $fieldset = $FormInputs->get(1);
        $fieldset->set('cond_display_select', 'checkbox_not_checked');
        $fieldset->set('cond_display_input', 'test3');
        $this->assertTrue($fieldset->isDirty('disable_expression'));
        $FormInputs->saveOrFail($fieldset);
        $this->assertEquals(
            <<<JSON
{
    "cond_display_select": "checkbox_not_checked",
    "cond_display_input": "test3"
}
JSON,
            (string)$FormInputs->get($fieldset->id)->get('disable_expression')
        );
    }
}
