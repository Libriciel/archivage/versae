<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Model\Entity;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use Cake\Core\Configure;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use AsalaeCore\TestSuite\TestCase;
use Versae\Model\Entity\Fileupload;

class FileuploadTest extends TestCase
{
    use AutoFixturesTrait;

    public $appAutoFixtures = [
        'Fileuploads',
    ];
    public $appAppendFixtures = [
    ];

    public function setUp(): void
    {
        parent::setUp();
        Configure::write('App.paths.data', TMP_TESTDIR);
        Configure::write('OrgEntities.public_dir', TMP_TESTDIR . DS . 'webroot');
    }

    public function testDeletable()
    {
        $upload = new Fileupload();
        $this->assertTrue($upload->get('deletable'));

        $upload->id = 1;
        $this->assertTrue((bool)$upload->get('deletable'));
    }

    public function testCopyInEntityDirectory()
    {
        $configuration = new Entity(
            [
                'org_entity_id' => 2,
                'name' => 'logo-client',
                'setting' => 'test.png'
            ]
        );
        /** @var Fileupload $upload */
        $upload = TableRegistry::getTableLocator()->get('Fileuploads')->get(1);
        if (!is_dir(dirname($upload->get('path')))) {
            mkdir(dirname($upload->get('path')), 0777, true);
        }
        file_put_contents($upload->get('path'), 'test');
        $upload->copyInEntityDirectory($configuration);
        $this->assertFileExists(TMP_TESTDIR . DS . 'webroot' . DS . $configuration->setting);
    }
}
