<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Model\Entity;

use Cake\ORM\TableRegistry;
use AsalaeCore\TestSuite\TestCase;
use Libriciel\Filesystem\Utility\Filesystem;
use Versae\Model\Table\DepositsTable;

class DepositTest extends TestCase
{
    public $fixtures = [
        'app.Deposits',
        'app.DepositValues',
        'app.Forms',
        'app.FormFieldsets',
        'app.FormInputs',
    ];

    public function testGetSendable()
    {
        $Deposits = TableRegistry::getTableLocator()->get('Deposits');
        $deposit = $Deposits->get(1);
        Filesystem::createDummyFile($deposit->get('path') . '/transfer.xml', 1);
        $this->assertTrue((bool)$deposit->get('sendable'));
        $this->assertFalse($Deposits->get(2)->get('sendable'));

        $deposit = $Deposits->newEntity(['form_id' => 1]);
        Filesystem::createDummyFile($deposit->get('path') . '/transfer.xml', 1);
        $this->assertFalse($deposit->get('sendable'));

        $deposit = $Deposits->get(1);
        $deposit->set('bypass_validation', true);
        $this->assertFalse($deposit->get('sendable'));
    }

    public function testGetReeditable()
    {
        $Deposits = TableRegistry::getTableLocator()->get('Deposits');
        $this->assertFalse($Deposits->get(1)->get('reeditable'));
        $this->assertFalse($Deposits->get(2)->get('reeditable'));
        $this->assertFalse($Deposits->get(3)->get('reeditable'));
        $this->assertFalse($Deposits->get(4)->get('reeditable'));
        $this->assertTrue((bool)$Deposits->get(6)->get('reeditable'));
    }

    public function testGetDeletable()
    {
        $Deposits = TableRegistry::getTableLocator()->get('Deposits');
        $this->assertTrue((bool)$Deposits->get(1)->get('deletable'));
        $this->assertFalse($Deposits->get(2)->get('deletable'));
        $this->assertFalse($Deposits->get(3)->get('deletable'));
        $this->assertFalse($Deposits->get(4)->get('deletable'));
        $this->assertTrue((bool)$Deposits->get(5)->get('deletable'));

        $deposit = $Deposits->newEntity(['state' => DepositsTable::S_SENDING_ERROR]);
        $this->assertTrue($deposit->get('deletable'));
    }

    public function testGetBasedir()
    {
        $deposit = TableRegistry::getTableLocator()->get('Deposits')
            ->get(1);
        $this->assertEquals('deposits/1_12345678', $deposit->get('basedir'));
    }
}
