<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Model\Entity;

use AsalaeCore\DataType\JsonString;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Versae\Model\Entity\FormCalculator;
use Versae\Model\Entity\FormVariable;
use Versae\Model\Table\FormVariablesTable;

class FormCalculatorTest extends TestCase
{
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_FORMS,
    ];

    public function testAll()
    {
        /** @var FormVariablesTable $FormVariables */
        $FormVariables = TableRegistry::getTableLocator()->get('FormVariables');
        /** @var FormVariable $formInput */
        $formInput = $FormVariables->newEntity(
            [
                'form_id' => 1,
                'name' => 'test',
                'description' => 'test',
                'type' => FormVariablesTable::TYPE_TWIG,
                'twig' => '{{input.test}}',
            ]
        );
        $this->assertNotFalse($FormVariables->save($formInput));

        $appMeta = $formInput->get('app_meta');
        $this->assertInstanceOf(JsonString::class, $appMeta);
        $this->assertEquals('[]', (string)$appMeta);

        $optionsType = $FormVariables->options('type');
        $this->assertArrayHasKey(FormVariablesTable::TYPE_TWIG, $optionsType);

        $FormVariables->patchEntity(
            $formInput,
            [
                'type' => FormVariablesTable::TYPE_CONDITIONS,
                'conditions_if_value' => 'test',
                'conditions' => [
                    'if' => [
                        0 => [
                            'part' => [
                                0 => [
                                    'type' => 'field',
                                    'field' => 'input.test',
                                    'field_tolower' => '1',
                                    'operator' => '==',
                                ],
                                1 => [
                                    'type' => 'value',
                                    'value' => 'test',
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        );
        $formInput->setDirty('app_meta');
        $FormVariables->saveOrFail($formInput);
        $formInput = $FormVariables->get($formInput->id);

        $this->assertEquals(
            'test',
            Hash::get($formInput, 'conditions.if.0.part.1.value')
        );
    }

    public function testDeletable()
    {
        $calculator = new FormCalculator(['id' => 1]);
        $this->assertFalse($calculator->get('deletable'));

        $calculator->id = 1000000;
        $this->assertTrue((bool)$calculator->get('deletable'));

        $FormCalculators = TableRegistry::getTableLocator()->get('FormCalculators');

        $this->assertFalse((bool)$FormCalculators->get(1)->get('deletable'));
    }
}
