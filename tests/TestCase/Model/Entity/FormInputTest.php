<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Model\Entity;

use AsalaeCore\DataType\JsonString;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use Cake\ORM\TableRegistry;
use AsalaeCore\TestSuite\TestCase;
use Versae\Model\Table\FormInputsTable;

class FormInputTest extends TestCase
{
    use AutoFixturesTrait;

    public $appAutoFixtures = [
        'FormInputs',
    ];
    public $appAppendFixtures = [
        FIXTURES_FORMS,
    ];

    public function testAll()
    {
        $FormInputs = TableRegistry::getTableLocator()->get('FormInputs');
        $ord = $FormInputs->find()
            ->where(['form_fieldset_id' => 1])
            ->orderDesc('ord')
            ->firstOrFail()
            ->get('ord') + 1;
        $formInput = $FormInputs->newEntity(
            [
                'name' => 'testunit',
                'type' => 'test',
                'ord' => $ord,
                'label' => 'test',
                'form_fieldset_id' => 1,
                'cond_display_select' => 'test',
                'cond_display_input' => 'test',
                'cond_display_input_type' => 'text',
                'cond_display_value_select' => 'test',
                'cond_display_value_file' => 'test',
                'checked' => '1',
                'color' => 'alert-info',
                'formats' => 'all',
                'max' => '10',
                'min' => '1',
                'multiple' => '1',
                'options' => '{"foo": "bar"}',
                'p' => 'test',
                'select2' => '1',
                'step' => '1',
                'value' => 'test',
                'required' => '1',
                'readonly' => '0',
                'date_format' => FormInputsTable::DATE_FORMAT_FR,
                'form_id' => 1,
            ]
        );
        $appMeta = $formInput->get('app_meta');
        $this->assertInstanceOf(JsonString::class, $appMeta);
        $this->assertEquals(
            <<<JSON
{
    "checked": "1",
    "color": "alert-info",
    "formats": "all",
    "max": "10",
    "min": "1",
    "options": "{\"foo\": \"bar\"}",
    "p": "test",
    "select2": "1",
    "step": "1",
    "value": "test"
}
JSON,
            (string)$appMeta
        );

        $disableExpression = $formInput->get('disable_expression');
        $this->assertInstanceOf(JsonString::class, $disableExpression);
        $this->assertEquals(
            <<<JSON
{
    "cond_display_select": "test",
    "cond_display_input": "test",
    "cond_display_input_type": "text",
    "cond_display_value_select": "test",
    "cond_display_value_file": "test"
}
JSON,
            (string)$disableExpression
        );

        foreach (['required', 'select2', 'multiple', 'checked'] as $boolField) {
            $this->assertEquals(1, $formInput->get($boolField));
        }
        $this->assertEquals(0, $formInput->get('readonly'));

        $this->assertNotEmpty($formInput->get('date_formattrad'));
        $formInput->set('date_format', FormInputsTable::DATE_FORMAT_FR_LONG);
        $this->assertNotEmpty($formInput->get('date_formattrad'));
        $formInput->set('date_format', FormInputsTable::DATE_FORMAT_ISO);
        $this->assertNotEmpty($formInput->get('date_formattrad'));
        $formInput->set('date_format', 'bad_format');
        $this->assertEmpty($formInput->get('date_formattrad'));

        $formInput->set('app_meta', '{"p": "bar"}');
        $appMeta = $formInput->get('app_meta');
        $this->assertInstanceOf(JsonString::class, $appMeta);

        $FormInputs->saveOrFail($formInput);
        $formInput = $FormInputs->get($formInput->id);
        /** @var JsonString $appMeta */
        $appMeta = $formInput->get('app_meta');
        $this->assertInstanceOf(JsonString::class, $appMeta);
        $this->assertEquals(
            <<<JSON
{
    "p": "bar"
}
JSON,
            (string)$appMeta
        );
        $appMeta->setFilter(null);
        $this->assertStringContainsString(
            '"checked": null',
            (string)$appMeta
        );
    }

    public function testNewEntity()
    {
        $FormInputs = $this->fetchTable('FormInputs');
        $formInput = $FormInputs->newEntity(['app_meta' => ['value' => '1']]);
        $this->assertEquals('1', $formInput->get('value'));
        $this->assertEquals('{"value":"1"}', (string)$formInput->get('app_meta'));

        $formInput = $FormInputs->newEntity(['app_meta' => ['value' => false]]);
        $this->assertEquals('[]', (string)$formInput->get('app_meta'));
    }
}
