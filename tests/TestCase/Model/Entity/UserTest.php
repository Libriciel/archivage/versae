<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Model\Entity;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use Cake\ORM\TableRegistry;
use AsalaeCore\TestSuite\TestCase;

class UserTest extends TestCase
{
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        'app.Aros',
        'app.Users',
    ];

    public function testGetDeletable()
    {
        $Users = TableRegistry::getTableLocator()->get('Users');
        $Users->saveOrFail(
            $user = $Users->newEmptyEntity()
        );
        $this->assertTrue($user->get('deletable'));
    }
}
