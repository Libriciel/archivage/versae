<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Model\Entity;

use Cake\ORM\TableRegistry;
use AsalaeCore\TestSuite\TestCase;
use Versae\Model\Entity\ArchivingSystem;

class ArchivingSystemTest extends TestCase
{
    public $fixtures = [
        'app.ArchivingSystems',
        'app.Forms',
        'app.FormsTransferringAgencies',
    ];

    public function testGetRemovable()
    {
        $ArchivingSystems = TableRegistry::getTableLocator()->get('ArchivingSystems');
        $this->assertFalse($ArchivingSystems->get(1)->get('removable'));

        $this->assertTrue((new ArchivingSystem())->get('removable'));

        $archivingSystem = $ArchivingSystems->newEntity(
            [
                'name' => 'testRemovable',
                'url' => 'https://localhost2',
                'username' => 'testunit',
                'password' => 'testunit',
                'use_proxy' => false,
                'active' => true,
            ]
        );
        $ArchivingSystems->saveOrFail($archivingSystem);
        $this->assertTrue($archivingSystem->get('removable'));
    }
}
