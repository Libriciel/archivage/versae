<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Model\Entity;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Versae\Model\Entity\FormFieldset;
use Versae\Model\Table\FormFieldsetsTable;

class FormFieldsetTest extends TestCase
{
    use AutoFixturesTrait;

    public $appAutoFixtures = [
        'FormInputs',
    ];

    public function test_getCondDisplaySelecttrad()
    {
        $entity = new FormFieldset();
        $conditions = [
            FormFieldsetsTable::COND_DISPLAY_INPUT_NOT_EMPTY,
            FormFieldsetsTable::COND_DISPLAY_INPUT_EMPTY,
            FormFieldsetsTable::COND_DISPLAY_INPUT_NOT_VALUE,
            FormFieldsetsTable::COND_DISPLAY_INPUT_VALUE,
            FormFieldsetsTable::COND_DISPLAY_CHECKBOX_NOT_CHECKED,
            FormFieldsetsTable::COND_DISPLAY_CHECKBOX_CHECKED,
        ];
        foreach ($conditions as $condition) {
            $entity->set('cond_display_select', $condition);
            $this->assertNotEmpty($entity->get('cond_display_selecttrad'));
        }
        $entity->set('cond_display_select', 'not defined');
        $this->assertEquals('not defined', $entity->get('cond_display_selecttrad'));
    }

    public function test_getDisableExpressionTrad()
    {
        $entity = new FormFieldset(['disable_expression' => '']);
        $this->assertEmpty($entity->get('disable_expressiontrad'));

        $entity = new FormFieldset(['form_id' => 1]);
        $conditions = [
            FormFieldsetsTable::COND_DISPLAY_INPUT_NOT_EMPTY,
            FormFieldsetsTable::COND_DISPLAY_INPUT_EMPTY,
            FormFieldsetsTable::COND_DISPLAY_INPUT_NOT_VALUE,
            FormFieldsetsTable::COND_DISPLAY_INPUT_VALUE,
            FormFieldsetsTable::COND_DISPLAY_CHECKBOX_NOT_CHECKED,
            FormFieldsetsTable::COND_DISPLAY_CHECKBOX_CHECKED,
        ];
        foreach ($conditions as $condition) {
            $entity->set(
                'disable_expression',
                json_encode(
                    [
                        'cond_display_select' => $condition,
                        'cond_display_input' => 'input_test',
                    ]
                )
            );
            $this->assertNotEmpty($entity->get('readable_disable_expression'));
        }
        $entity->set(
            'disable_expression',
            json_encode(
                [
                    'cond_display_select' => 'invalid',
                    'cond_display_input' => 'input_test',
                ]
            )
        );
        $this->assertEmpty($entity->get('readable_disable_expression'));
    }
}
