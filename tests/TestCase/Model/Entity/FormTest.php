<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Model\Entity;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use Cake\ORM\TableRegistry;
use AsalaeCore\TestSuite\TestCase;
use Versae\Model\Table\DepositsTable;
use Versae\Model\Table\FormInputsTable;
use Versae\Model\Table\FormsTable;

class FormTest extends TestCase
{
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_FORMS,
        'app.Deposits',
        'app.OrgEntities',
        'app.ArchivingSystems',
        'app.Users',
    ];

    public function testGetVersionable()
    {
        $Forms = TableRegistry::getTableLocator()->get('Forms');
        $form = $Forms->newEntity(
            [
                'org_entity_id' => 1,
                'name' => 'testGetVersionable',
                'description' => 'Lorem ipsum dolor sit amet',
                'archiving_system_id' => 1,
                'identifier' => 'testGetVersionable',
                'version_number' => 2,
                'version_note' => 'Lorem ipsum dolor sit amet',
                'state' => FormsTable::S_EDITING,
            ]
        );

        $this->assertFalse($form->get('versionable'));

        $Forms->patchEntity($form, ['state' => FormsTable::S_PUBLISHED]);
        $this->assertTrue($form->get('versionable'));

        $Forms->saveOrFail(
            $newForm = $Forms->newEntity(
                [
                    'org_entity_id' => 1,
                    'name' => 'testGetVersionable',
                    'description' => 'Lorem ipsum dolor sit amet',
                    'archiving_system_id' => 1,
                    'identifier' => 'testGetVersionable',
                    'version_number' => 1,
                    'version_note' => 'Lorem ipsum dolor sit amet',
                    'state' => FormsTable::S_EDITING,
                    'transferring_agencies' => [2],
                ]
            )
        );
        $this->assertFalse($form->get('versionable'));

        $Forms->delete($newForm);
        $this->assertTrue($form->get('versionable'));

        $Forms->saveOrFail(
            $Forms->newEntity(
                [
                    'org_entity_id' => 1,
                    'name' => 'testGetVersionable',
                    'description' => 'Lorem ipsum dolor sit amet',
                    'archiving_system_id' => 1,
                    'identifier' => 'testGetVersionable',
                    'version_number' => 3,
                    'version_note' => 'Lorem ipsum dolor sit amet',
                    'state' => FormsTable::S_PUBLISHED,
                    'transferring_agencies' => [2],
                ]
            )
        );
        $this->assertFalse($form->get('versionable'));
    }

    public function testGetPublishable()
    {
        $Forms = TableRegistry::getTableLocator()->get('Forms');

        $Forms->saveOrFail(
            $form = $Forms->newEntity(
                [
                    'org_entity_id' => 1,
                    'name' => 'testGetVersionable',
                    'description' => 'Lorem ipsum dolor sit amet',
                    'archiving_system_id' => 1,
                    'identifier' => 'testGetVersionable',
                    'version_number' => 2,
                    'version_note' => 'Lorem ipsum dolor sit amet',
                    'state' => FormsTable::S_EDITING,
                    'transferring_agencies' => [2],
                ]
            )
        );

        $this->assertFalse($form->get('publishable'));
        $this->assertEquals(
            'Le Formulaire ne contient aucun champ Fichier ou ZIP',
            $form->get('not_publishable_reason')
        );

        $FormFieldsets = TableRegistry::getTableLocator()->get('FormFieldsets');
        $FormFieldsets->saveOrFail(
            $fieldset = $FormFieldsets->newEntity(
                [
                    'form_id' => $form->get('id'),
                    'legend' => 'Lorem ipsum dolor sit amet',
                    'ord' => 1,
                ]
            )
        );

        $FormInputs = TableRegistry::getTableLocator()->get('FormInputs');
        $FormInputs->saveOrFail(
            $FormInputs->newEntity(
                [
                    'form_fieldset_id' => $fieldset->get('id'),
                    'name' => 'test',
                    'type' => FormInputsTable::TYPE_FILE,
                    'ord' => 1,
                    'label' => 'Lorem ipsum dolor sit amet',
                    'required' => 1,
                    'readonly' => 0,
                    'form_id' => $fieldset->get('form_id'),
                ]
            )
        );
        $form = $Forms->get($form->get('id'));
        $this->assertFalse($form->get('publishable'));
        $this->assertStringContainsString(
            __("Le Formulaire ne contient aucune unité d'archives"),
            $form->get('not_publishable_reason')
        );

        $FormUnits = TableRegistry::getTableLocator()->get('FormUnits');
        $FormUnits->saveOrFail(
            $FormUnits->newEntity(
                [
                    'form_id' => $form->get('id'),
                    'name' => 'unit',
                    'type' => 'document',
                ]
            )
        );
        $form = $Forms->get($form->get('id'));
        $this->assertTrue((bool)$form->get('publishable'));
        $this->assertEquals('', $form->get('not_publishable_reason'));
    }

    public function testUnpublishable()
    {
        $Forms = TableRegistry::getTableLocator()->get('Forms');
        $this->assertFalse((bool)$Forms->get(1)->get('unpublishable'));
        $this->assertTrue((bool)$Forms->get(9)->get('unpublishable'));
        $this->assertFalse($Forms->get(3)->get('unpublishable'));
        $this->assertFalse($Forms->get(4)->get('unpublishable'));

        $Deposits = TableRegistry::getTableLocator()->get('Deposits');
        $Deposits->saveOrFail(
            $Deposits->newEntity(
                [
                    'form_id' => 2,
                    'archival_agency_id' => 1,
                    'transferring_agency_id' => 1,
                    'name' => 'editing',
                    'state' => DepositsTable::S_READY_TO_PREPARE,
                    'created_user_id' => 1,
                    'path_token' => 'loremips',
                    'bypass_validation' => true,
                ]
            )
        );
        $this->assertFalse($Forms->get(2)->get('unpublishable'));
    }
}
