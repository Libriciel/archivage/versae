<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Utility;

use AsalaeCore\DataType\JsonString;
use AsalaeCore\TestSuite\TestCase;
use DateTime;
use Twig\Environment;
use Versae\Utility\Twig;

class TwigTest extends TestCase
{
    public function testGetTwig()
    {
        $tmpl = 'Test {{test|upper|trim}}';
        $this->assertInstanceOf(Environment::class, Twig::getTwig($tmpl));
    }

    public function testRender()
    {
        $tmpl = 'Test {{test|upper|trim}}';
        $vars = ['test' => '  foo  '];
        $expected = 'Test FOO';
        $this->assertEquals($expected, Twig::render($tmpl, $vars, false));

        $tmpl = '{{test|date("Y-m-d")}}';
        $vars = ['test' => new DateTime('2021-06-21')];
        $expected = '2021-06-21';
        $this->assertEquals($expected, Twig::render($tmpl, $vars, false));

        $tmpl = '{{test}}';
        $vars = ['test' => new JsonString(['foo', 'bar'])];
        $expected = '[&quot;foo&quot;,&quot;bar&quot;]';
        $this->assertEquals($expected, Twig::render($tmpl, $vars, false));

        $tmpl = '{% if test == "foo" %}bar{% endif %}';
        $vars = ['test' => 'foo'];
        $expected = 'bar';
        $this->assertEquals($expected, Twig::render($tmpl, $vars));
        $vars = ['test' => 'baz'];
        $expected = '';
        $this->assertEquals($expected, Twig::render($tmpl, $vars));

        $tmpl = '{{foo.bar}}';
        $vars = ['foo' => ['bar' => 'baz']];
        $expected = 'baz';
        $this->assertEquals($expected, Twig::render($tmpl, $vars));

        $tmpl = <<<EOT
{%
if foo not in bar
    %}true_value{%
endif
%}
EOT;
        $vars = ['foo' => ['foo' => 'bar', 'bar' => 'baz']];
        $expected = 'true_value';
        $this->assertEquals($expected, Twig::render($tmpl, $vars));

        $tmpl = '{% if (test == "foo" and test != "biz") or (test == null) %}bar{% endif %}';
        $vars = ['test' => 'foo'];
        $expected = 'bar';
        $this->assertEquals($expected, Twig::render($tmpl, $vars));
    }

    public function testValidate()
    {
        $tmpl = 'Test {{test|upper|trim}}';
        $this->assertTrue(
            Twig::validate(
                $tmpl,
                true,
                [Twig::KEY_FILTERS => array_merge(Twig::SANDBOX_FILTERS, ['upper', 'trim', 'escape'])]
            )
        );

        $tmpl = 'Test {test|upper|trim}}';
        $this->assertFalse(Twig::validate($tmpl));

        $tmpl = '{% set foo = 1 %}{{ foo }}'; // interdit par le sandbox
        $this->assertFalse(Twig::validate($tmpl));
    }

    public function testTokenize()
    {
        $tmpl = <<<EOT
{%
if foo not in bar
    %}true_value{%
endif
%}
EOT;
        $result = Twig::tokenize($tmpl);
        $this->assertCount(11, $result);
        $this->assertEquals('BLOCK_START_TYPE', $result[0]['name']);
        $this->assertEquals('if', $result[1]['value']);
        $this->assertEquals('VAR', $result[2]['type']);
        $this->assertEquals('foo', $result[2]['value']);
        $this->assertEquals('OPERATOR_TYPE', $result[3]['name']);
        $this->assertEquals('VAR', $result[4]['type']);
        $this->assertEquals('BLOCK_END_TYPE', $result[5]['name']);
    }

    public function testExtractVars()
    {
        $tmpl = <<<EOT
{%
if foo not in bar
    %}true_value{%
endif
%}
EOT;
        $result = Twig::extractVars($tmpl);
        $this->assertEquals(['foo', 'bar'], $result);

        $result = Twig::extractVars('{{ test1|lower or test2[0] or toto or foo.bar }}');
        $this->assertEquals(['test1', 'test2', 'toto', 'foo.bar'], $result);
    }

    public function testReplaceVar()
    {
        $tmpl = '{% if foo.ba not in foo.bar %}1{% elseif foo.ba is null %}{{ foo.bar }}{% endif %}';
        $result = Twig::replaceVar($tmpl, 'foo.ba', 'foo.test');
        $this->assertEquals(
            '{% if foo.test not in foo.bar %}1{% elseif foo.test is null %}{{ foo.bar }}{% endif %}',
            $result
        );
    }
}
