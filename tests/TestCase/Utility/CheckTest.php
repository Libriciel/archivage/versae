<?php
/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Utility;

use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\Client;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\Http\Client\Response;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Exception;
use Versae\Test\Mock\CheckWithSetter;
use Versae\Test\Mock\ClientMock;
use Versae\Utility\Check;

class CheckTest extends TestCase
{
    use AutoFixturesTrait;

    public $appAutoFixtures = [
        'ArchivingSystems',
    ];
    public $appAppendFixtures = [
    ];

    public function setUp(): void
    {
        parent::setUp();
        Check::reset();

        $response = $this->createMock(Response::class);
        $response->method('getStringBody')
            ->willReturn(json_encode(['success' => true]));
        $client = $this->getMockBuilder(Client::class)
            ->setConstructorArgs([])
            ->onlyMethods(['get'])
            ->getMock();
        $client->method('get')->willReturn($response);
        ClientMock::$mock = $client;
        Utility::set(Client::class, new ClientMock());
    }

    public function tearDown(): void
    {
        Utility::reset();
        parent::tearDown();
    }

    public function testSedaGeneratorStatus()
    {
        Configure::delete('SedaGenerator.url');
        $seda = Check::sedaGeneratorStatus();
        $this->assertTrue($seda['warning']);

        $response = $this->createMock(Response::class);
        $response->method('getStringBody')
            ->willReturn(json_encode(['success' => true]));
        $client = $this->getMockBuilder(Client::class)
            ->setConstructorArgs([])
            ->onlyMethods(['get'])
            ->getMock();
        $client->method('get')->willReturn($response);
        ClientMock::$mock = $client;
        Utility::set(Client::class, new ClientMock());
        Configure::write('SedaGenerator.url', 'https://test');
        $seda = Check::sedaGeneratorStatus();
        $this->assertTrue($seda['success']);

        $client = $this->getMockBuilder(Client::class)
            ->setConstructorArgs([])
            ->onlyMethods(['get'])
            ->getMock();
        $client->method('get')->willThrowException(new Exception('message exception'));
        ClientMock::$mock = $client;
        Utility::set(Client::class, new ClientMock());
        $seda = Check::sedaGeneratorStatus();
        $this->assertFalse($seda['success']);
        $this->assertEquals('message exception', $seda['info']);
    }

    public function testarchivingSystems()
    {
        $this->assertNotEmpty(Check::archivingSystems());

        // les sae inactifs ne sont pas vérifiés
        TableRegistry::getTableLocator()->get('ArchivingSystems')
            ->updateAll(['active' => false], ['true']);
        Check::reset();
        $this->assertEmpty(Check::archivingSystems());
    }

    public function testarchivingSystemsOk()
    {
        $archivingSystems = [
            new Entity(['success' => true]),
            new Entity(['success' => true]),
            new Entity(['success' => true]),
        ];
        CheckWithSetter::setArchivingSystems($archivingSystems);
        $this->assertTrue(Check::archivingSystemsOK());

        $archivingSystems = [
            new Entity(['success' => true]),
            new Entity(['success' => false]),
            new Entity(['success' => true]),
        ];
        CheckWithSetter::setArchivingSystems($archivingSystems);
        $this->assertFalse(Check::archivingSystemsOK());
    }
}
