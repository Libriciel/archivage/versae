<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Utility;

use AsalaeCore\TestSuite\TestCase;
use Libriciel\Filesystem\Utility\Filesystem;
use Versae\Utility\Csv;

class CsvTest extends TestCase
{
    public function testCoord()
    {
        $this->assertEquals([0, 0], Csv::coords('A1'));
        $this->assertEquals([2, 17], Csv::coords('C18'));
        $this->assertEquals([26, 26], Csv::coords('AA27'));
        $this->assertEquals([27, 27], Csv::coords('AB28'));
        $this->assertEquals([730, 247], Csv::coords('abc248'));
        $this->assertEquals([0, 0], Csv::coords('1,1'));
        $this->assertEquals([124, 244], Csv::coords('125,245'));
    }

    public function testColRowToCoords()
    {
        $this->assertEquals('A1', Csv::colRowToCoords(0, 0));
        $this->assertEquals('B1', Csv::colRowToCoords(0, 1));
        $this->assertEquals('D7', Csv::colRowToCoords(6, 3));
        $this->assertEquals('AA27', Csv::colRowToCoords(26, 26));
        $this->assertEquals('AB28', Csv::colRowToCoords(27, 27));
        $this->assertEquals('BA28', Csv::colRowToCoords(27, 52));
        $this->assertEquals('ABC248', Csv::colRowToCoords(247, 730));
    }

    public function testCoordRange()
    {
        $this->assertEquals([[0, 0], [0, 1], [0, 2]], Csv::coordRange('A1:A3'));
        $this->assertEquals([[1, 1], [1, 2], [1, 3]], Csv::coordRange('B2:B4'));
        $this->assertEquals([[1, 1], [2, 1], [3, 1]], Csv::coordRange('B2:D2'));
    }

    public function testGet()
    {
        $csv = "foo,3.14,\"3,14\"\n"
            . "bar,toto,\"vilain petit canard\"\n"
            . "test,,test";
        $Csv = new Csv($csv);
        $this->assertEquals('foo', $Csv->get('A1'));
        $this->assertEquals('bar', $Csv->get('a2'));
        $this->assertEquals('test', $Csv->get('c3'));
        $this->assertEquals('', $Csv->get('aaa999'));

        $tmpFile = TMP_TESTDIR . DS . 'testfile.csv';
        Filesystem::dumpFile($tmpFile, $csv);
        $Csv = new Csv($tmpFile);
        $this->assertEquals('3,14', $Csv->get('C1'));
        $this->assertEquals('test', $Csv->get('C3'));
        $this->assertEquals('', $Csv->get('B3'));
        $this->assertEquals('', $Csv->get('aaa999'));

        // test plage de coordonnées
        $this->assertEquals(['3,14', 'vilain petit canard', 'test'], $Csv->get('C1:C3'));
        $this->assertEquals(['toto', 'vilain petit canard'], $Csv->get('B2:D2'));
    }
}
