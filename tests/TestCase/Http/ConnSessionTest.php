<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Http;

use AsalaeCore\TestSuite\TestCase;
use Versae\Http\ConnSession;

class ConnSessionTest extends TestCase
{
    public function testTimeout()
    {
        $defaultSession = [
            'cookie' => 'PHPSESSID',
            'ini' => [
                'session.use_trans_sid' => 0,
                'session.cookie_secure' => 1,
                'session.name' => 'PHPSESSID',
                'session.cookie_httponly' => 1
            ],
            'defaults' => 'php',
            'cookiePath' => '/'
        ];
        $session = new ConnSession($defaultSession);
        $this->assertFalse($session->timedOut());

        $session->write('Config.time', (int)ini_get('session.gc_maxlifetime') + 600);
        $this->assertTrue($session->timedOut());
    }

    public function testTimeleft()
    {
        $defaultSession = [
            'cookie' => 'PHPSESSID',
            'ini' => [
                'session.use_trans_sid' => 0,
                'session.cookie_secure' => 1,
                'session.name' => 'PHPSESSID',
                'session.cookie_httponly' => 1
            ],
            'defaults' => 'php',
            'cookiePath' => '/'
        ];
        $session = new ConnSession($defaultSession);
        $session->write('Config.time', time() - (int)ini_get('session.gc_maxlifetime') + 300);
        $this->assertWithinRange(300, $session->timeLeft(), 1);
    }
}
