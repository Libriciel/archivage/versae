<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Http\Exception;

use AsalaeCore\TestSuite\TestCase;
use Versae\Exception\KilledException;

class KilledExceptionTest extends TestCase
{
    public function testConstruct()
    {
        $e = new KilledException();
        $this->assertEquals(499, $e->getCode());
    }
}
