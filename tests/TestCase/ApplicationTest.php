<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase;

use Versae\Application;
use Cake\Core\Configure;
use Cake\Core\Exception\MissingPluginException;
use AsalaeCore\TestSuite\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

class ApplicationTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('debug', true);
    }

    public function testBootstrap()
    {
        $app = new Application(ROOT . DS . 'config');
        $app->bootstrap();
        $this->assertTrue($app->getPlugins()->has('AsalaeCore'));

        /** @var MockObject|Application $app */
        $app = $this->getMockBuilder(Application::class)
            ->onlyMethods(['addPlugin'])
            ->setConstructorArgs([ROOT . DS . 'config'])
            ->getMock();
        $app->method('addPlugin')->will(
            $this->returnCallback(
                function ($plugin) {
                    if (in_array($plugin, ['DebugKit', 'Bake'])) {
                        throw new MissingPluginException();
                    }
                }
            )
        );
        $app->bootstrap();
        $this->assertFalse($app->getPlugins()->has('DebugKit'));
    }
}
