<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\View\Helper;

use Cake\Core\Configure;
use AsalaeCore\TestSuite\TestCase;
use Cake\View\View;
use Versae\View\Helper\MultiStepFormHelper;

/**
 * Versae\View\Helper\MultiStepFormHelper Test Case
 */
class MultiStepFormHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var MultiStepFormHelper
     */
    public $MultiStepForm;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->MultiStepForm = new MultiStepFormHelper($view);
        Configure::write('App.defaultLocale', 'fr_FR');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->MultiStepForm);

        parent::tearDown();
    }

    public function testrender()
    {
        $render = $this->MultiStepForm->template('MultiStepForm/add_deposit')->step(1);
        $expected = '<li class="active"><div class="bar"></div><span class="title">'
            . __("Choix du formulaire") . '</span></li>';
        $this->assertStringContainsString($expected, $render);

        $render = $this->MultiStepForm->template('MultiStepForm/add_deposit')->step(2);

        // etape 1 ne doit pas être 'actif' ici
        $expected = '<li class="active"><div class="bar"></div><span class="title">'
            . __("Choix du formulaire") . '</span></li>';
        $this->assertStringNotContainsString($expected, $render);

        // etape 2 doit être actif
        $expected = '<li class="active"><div class="bar"></div><span class="title">'
            . __("Saisie du versement") . '</span></li>';
        $this->assertStringContainsString($expected, $render);
    }
}
