<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\View\Helper;

use Cake\Core\Configure;
use AsalaeCore\TestSuite\TestCase;
use Cake\View\View;
use Versae\View\Helper\HtmlHelper;

/**
 * Versae\View\Helper\HtmlHelperTest Test Case
 */
class HtmlHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var HtmlHelper
     */
    public $HtmlHelper;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->HtmlHelper = new HtmlHelper($view);
        Configure::write('App.defaultLocale', 'fr_FR');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->HtmlHelper);

        parent::tearDown();
    }

    public function testtag()
    {
        $content = 'mon contenu';
        $div = $this->HtmlHelper->tag('div#monid.maclass1.maclass2', $content);
        $this->assertStringContainsString($content, $div);
        $this->assertStringContainsString('class="maclass1 maclass2"', $div);
        $this->assertStringContainsString('id="monid"', $div);
    }
}
