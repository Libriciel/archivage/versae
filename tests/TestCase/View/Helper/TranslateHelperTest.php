<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\View\Helper;

use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\View\View;
use Exception;
use Versae\View\Helper\TranslateHelper;

/**
 * Versae\View\Helper\TranslateHelper Test Case
 */
class TranslateHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var TranslateHelper
     */
    public $TranslateHelper;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->TranslateHelper = new TranslateHelper($view);
        Configure::write('App.defaultLocale', 'fr_FR');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->TranslateHelper);

        parent::tearDown();
    }

    /**
     * Test permission method
     *
     * @return void
     * @throws Exception
     */
    public function testPermission()
    {
        $controllers = json_decode(
            file_get_contents(RESOURCES . 'controllers.json'),
            true
        );
        foreach ($controllers as $controller => $actions) {
            $trad = $this->TranslateHelper->permission($controller);
            $this->assertNotEmpty($trad);
            foreach ($actions as $action => $params) {
                if ($action !== 'api') {
                    $trad = $this->TranslateHelper->permission($controller, $action);
                    $this->assertNotEmpty($trad);
                    continue;
                }
                foreach (['create', 'read', 'update', 'delete'] as $crud) {
                    $trad = $this->TranslateHelper->permission($controller, $action . '.' . $crud);
                    $this->assertNotEmpty($trad);
                }
            }
        }
    }
}
