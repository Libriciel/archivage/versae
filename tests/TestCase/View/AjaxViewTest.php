<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\View;

use AsalaeCore\Http\Response;
use AsalaeCore\TestSuite\TestCase;
use Versae\View\AjaxView;

class AjaxViewTest extends TestCase
{
    public $fixtures = [
        'app.Acos',
    ];

    public function testConstruct()
    {
        $response = new Response();
        $view = new AjaxView(null, $response);
        $this->assertEquals('text/html', $view->getResponse()->getType());
    }
}
