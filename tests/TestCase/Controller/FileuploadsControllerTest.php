<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Versae\Controller\FileuploadsController Test Case
 *
 * @uses \Versae\Controller\DownloadController
 */
class FileuploadsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        'app.Fileuploads',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Filesystem::reset();
    }

    public function tearDown(): void
    {
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        parent::tearDown();
    }

    public function testDelete()
    {
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $path = $Fileuploads->get(1)->get('path');
        if (substr($path, 0, strlen(sys_get_temp_dir())) !== sys_get_temp_dir()) {
            $this->fail("path '$path' != " . sys_get_temp_dir());
        }

        Filesystem::dumpFile($path, 'test');
        $this->assertFileExists($path);
        $this->httpDelete('/fileuploads/delete/1');
        $this->assertResponseCode(200);
        $this->assertFileDoesNotExist($path);
    }
}
