<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;

class CountersControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;

    public $fixtures = [
        'app.Acos',
        'app.Aros',
        'app.ArosAcos',
        'app.Counters',
        'app.Filters',
        'app.OrgEntities',
        'app.SavedFilters',
        'app.Sequences',
        'app.Users',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
    }

    public function testIndex()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $response = $this->httpGet('/counters/index', true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['data']);
    }

    public function testInitCounters()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->get('/counters/initCounters');
        $this->assertResponseCode(400);

        $Counters = TableRegistry::getTableLocator()->get('Counters');
        $Counters->deleteAll([]);

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/counters/initCounters');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response['success']));
        $this->assertTrue($response['success']);
        $this->assertGreaterThan(0, $Counters->find()->count());
    }

    public function testEdit()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/counters/edit/1');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response['entity']));
        $data = $response['entity'];
        $data['description'] = "Ceci n'est pas un test";

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPut('/counters/edit/1', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertEquals($data['name'], $response['name']);

        $Counters = TableRegistry::getTableLocator()->get('Counters');
        $this->assertEquals("Ceci n'est pas un test", $Counters->get(1)->get('description'));
    }

    public function testView()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $response = $this->httpGet('/counters/view/1', true);
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response['entity']));
    }
}
