<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use AsalaeCore\TestSuite\TestCase;
use Authentication\AuthenticationService;
use Authentication\Authenticator\SessionAuthenticator;
use Cake\Core\Configure;
use Cake\Http\ServerRequest;
use Cake\TestSuite\IntegrationTestTrait;
use Exception;
use Versae\Controller\AppController;

/**
 * Asalae\Controller\AppController Test Case
 */
class AppControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Aros',
        'app.OrgEntities',
        'app.TypeEntities',
        'app.Users',
    ];

    /**
     * Test initialize method
     *
     * @return void
     * @throws Exception
     */
    public function testInitialize()
    {
        Configure::write('Auth.disabled', true);
        $service = new AuthenticationService();
        $service->loadAuthenticator(
            'Session',
            [
                'className' => SessionAuthenticator::class,
            ]
        );
        $request = (new ServerRequest())->withAttribute('authentication', $service);
        $app = new AppController($request);
        $app->components()->reset();
        $app->initialize();
        $app->components()->reset();
        $user = $app->getRequest()->getSession()->read('Auth');
        unset($app);
        $this->assertEquals(1, $user['id']);
    }
}
