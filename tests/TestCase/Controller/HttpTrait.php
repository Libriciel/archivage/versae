<?php

namespace Versae\Test\TestCase\Controller;

use AsalaeCore\TestSuite\TestCase;
use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Http\Response;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Inflector;
use ErrorException;
use Exception;
use Laminas\Diactoros\CallbackStream;
use PHPUnit\Exception as PHPUnitException;
use Throwable;

/**
 * Trait Http permet d'effectuer des appels speudo Http avec debug automatique
 * des exception
 *
 * @package Versae\Test\TestCase\Controller
 * @mixin IntegrationTestTrait
 * @mixin TestCase
 * @property array _request
 * @property Response|null _response
 * @property Controller|null _controller
 */
trait HttpTrait
{
    /**
     * @var bool Transforme les erreurs en exception
     */
    private $errorToException = true;

    /**
     * Performs a GET request using the current request data.
     *
     * The response of the dispatched request will be stored as
     * a property. You can use various assert methods to check the
     * response.
     *
     * @param string|array $url   The URL to request.
     * @param bool         $assoc When <b>TRUE</b>, returned objects will be converted into
     *                            associative arrays.
     * @return mixed
     * @throws ErrorException
     * @throws PHPUnitException
     * @throws Throwable
     */
    public function httpGet(string $url, bool $assoc = false)
    {
        return $this->httpSendRequest($url, 'GET', [], $assoc);
    }

    /**
     * Performs a POST request using the current request data.
     *
     * The response of the dispatched request will be stored as
     * a property. You can use various assert methods to check the
     * response.
     *
     * @param string|array $url   The URL to request.
     * @param array        $data  The data for the request.
     * @param bool         $assoc When <b>TRUE</b>, returned objects will be converted into
     *                            associative arrays.
     * @return mixed
     * @throws ErrorException
     * @throws PHPUnitException
     * @throws Throwable
     */
    public function httpPost(string $url, array $data = [], bool $assoc = false)
    {
        return $this->httpSendRequest($url, 'POST', $data, $assoc);
    }

    /**
     * Performs a PATCH request using the current request data.
     *
     * The response of the dispatched request will be stored as
     * a property. You can use various assert methods to check the
     * response.
     *
     * @param string|array $url   The URL to request.
     * @param array        $data  The data for the request.
     * @param bool         $assoc When <b>TRUE</b>, returned objects will be converted into
     *                            associative arrays.
     * @return mixed
     * @throws ErrorException
     * @throws PHPUnitException
     * @throws Throwable
     */
    public function httpPatch(string $url, array $data = [], bool $assoc = false)
    {
        return $this->httpSendRequest($url, 'PATCH', $data, $assoc);
    }

    /**
     * Performs a PUT request using the current request data.
     *
     * The response of the dispatched request will be stored as
     * a property. You can use various assert methods to check the
     * response.
     *
     * @param string|array $url   The URL to request.
     * @param array        $data  The data for the request.
     * @param bool         $assoc When <b>TRUE</b>, returned objects will be converted into
     *                            associative arrays.
     * @return mixed
     * @throws PHPUnitException
     */
    public function httpPut(string $url, array $data = [], bool $assoc = false)
    {
        return $this->httpSendRequest($url, 'PUT', $data, $assoc);
    }

    /**
     * Performs a DELETE request using the current request data.
     *
     * The response of the dispatched request will be stored as
     * a property. You can use various assert methods to check the
     * response.
     *
     * @param string|array $url   The URL to request.
     * @param array        $data
     * @param bool         $assoc When <b>TRUE</b>, returned objects will be converted into
     *                            associative arrays.
     * @return mixed
     * @throws ErrorException
     * @throws PHPUnitException
     * @throws Throwable
     */
    public function httpDelete(string $url, array $data = [], bool $assoc = false)
    {
        return $this->httpSendRequest($url, 'DELETE', $data, $assoc);
    }

    /**
     * Envoi une commande http et retourne la réponse. Met en forme les erreurs
     * si applicable
     * @param string $method
     * @param string $url
     * @param int    $expectedCode
     * @param array  $data
     * @return array
     * @throws ErrorException
     * @throws PHPUnitException
     * @throws Throwable
     */
    public function http(
        string $method,
        string $url,
        int $expectedCode,
        array $data = []
    ): array {
        return $this->httpSendRequestCode($url, strtoupper($method), $expectedCode, $data);
    }

    /**
     * Creates and send the request into a Dispatcher instance.
     *
     * Receives and stores the response for future inspection.
     *
     * @param string|array $url    The URL
     * @param string       $method The HTTP method
     * @param array|null   $data   The request data.
     * @param bool         $assoc  When <b>TRUE</b>, returned objects will be converted into
     *                             associative arrays.
     * @return mixed
     * @throws ErrorException
     * @throws Throwable
     * @throws PHPUnitException
     */
    protected function httpSendRequest($url, $method, $data = [], bool $assoc = false)
    {
        if (empty($this->_request['headers']['Accept'])) {
            $this->_request['headers']['Accept'] = 'application/json';
        }
        if ($this->errorToException) {
            set_error_handler(
                function ($errno, $errstr, $errfile, $errline) {
                    if ($errno & Configure::read('Error.errorLevel')) {
                        throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
                    }
                }
            );
            try {
                $this->_sendRequest($url, $method, $data);
            } catch (Throwable $e) {
                restore_error_handler();
                throw $e;
            }
            restore_error_handler();
        } else {
            $this->_sendRequest($url, $method, $data);
        }
        if (($code = $this->_response->getStatusCode()) !== 200) {
            $body = $this->getResponseAsString();
            $output = json_decode($body) ?: substr($body, 0, 500);
            ob_start();
            $out = [];
            if (
                is_object($output)
                && isset($output->message)
                && preg_match('/Cannot describe ([\w]+)/', $output->message, $match)
            ) {
                $out['Fixtures'] = 'Missing fixture: app.' . Inflector::camelize($match[1]);
            }
            $out['Error code ' . $code] = $output;
            debug($out);
            $debug = ob_get_clean();
            throw new Exception('HTTP Status code != 200: ' . PHP_EOL . $debug);
        }
        return json_decode($this->getResponseAsString(), $assoc);
    }

    /**
     * Donne le body de la réponse sous forme de string
     * @return string
     */
    protected function getResponseAsString(): string
    {
        $body = $this->_response->getBody();
        if (!$body instanceof CallbackStream) {
            $body->rewind();
        }
        return (string)$body->getContents();
    }

    /**
     * Tente de trouver une erreur dans la réponse
     * @return string
     */
    protected function extractResponseError(): string
    {
        $errorVar = $this->_controller->viewBuilder()->getVar('error');
        if ($errorVar instanceof Throwable) {
            $errorVar = $errorVar->getMessage();
        }
        $flash = $this->_controller->getRequest()->getSession()->read('Flash.flash');
        $flashError = null;
        foreach ((array)$flash as $msgArray) {
            if ($msgArray['element'] ?? null === 'Flash/error') {
                $flashError = $msgArray['message'] ?? 'flash_error';
            }
        }
        $jsonError = null;
        $jsonFlash = null;
        $body = $this->_response->getBody();
        $body->rewind();
        $content = $body->getContents();
        if ($this->_response->getType() === 'application/json') {
            $json = json_decode($content, true);
            if ($json && !empty($json['error'])) {
                $jsonError = $json['error'];
            }
            if ($json && !empty($json['flash'])) {
                $jsonFlash = $json['flash'];
            }
        } elseif (
            $this->_response->getType() === 'text/html'
            && ($pos = strpos($content, '<section class="container container-flash">'))
        ) {
            $sub = substr($content, $pos + 43);
            $pos = strpos($sub, '<div class="alert alert-danger">');
            $msg = substr($sub, $pos + 32);
            $msg = trim(substr($msg, 0, strpos($msg, '</div>')));
            if ($msg) {
                $jsonFlash = $msg;
            }
        }
        $errorMsg = array_filter(
            [
                $errorVar,
                $flashError,
                $jsonError,
                $jsonFlash,
            ]
        );
        return implode(' - ', $errorMsg);
    }

    /**
     * Assure le retour 200 avec header X-Asalae-Success=true d'une requête
     */
    protected function assertAsalaeSuccess()
    {
        $errorMsg = $this->extractResponseError();
        $this->assertResponseCode(200, $errorMsg);
        $this->assertHeader('X-Asalae-Success', 'true', $errorMsg);
    }

    /**
     * Méthode générique d'envoi http
     * @param string $url
     * @param string $method
     * @param int    $expectedCode
     * @param array  $data
     * @return array
     * @throws ErrorException
     * @throws PHPUnitException
     * @throws Throwable
     */
    private function httpSendRequestCode(
        string $url,
        string $method,
        int $expectedCode,
        array $data = []
    ): array {
        if (empty($this->_request['headers']['Accept'])) {
            $this->_request['headers']['Accept'] = 'application/json';
        }
        if ($this->errorToException) {
            set_error_handler(
                function ($errno, $errstr, $errfile, $errline) {
                    if ($errno & Configure::read('Error.errorLevel')) {
                        throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
                    }
                }
            );
            try {
                $this->_sendRequest($url, $method, $data);
            } catch (Throwable $e) {
                restore_error_handler();
                throw $e;
            }
            restore_error_handler();
        } else {
            $this->_sendRequest($url, $method, $data);
        }
        $body = $this->getResponseAsString();
        if (($code = $this->_response->getStatusCode()) !== $expectedCode) {
            $output = json_decode($body, true) ?: substr($body, 0, 500);
            $out = [];
            if (
                is_object($output)
                && isset($output->message)
                && preg_match('/Cannot describe ([\w]+)/', $output->message, $match)
            ) {
                $out['Fixtures'] = 'Missing fixture: app.' . Inflector::camelize($match[1]);
            }
            $out['expected: ' . $expectedCode . ' ; returned code ' . $code] = $output;

            $debug = debuglr($out);
            $this->assertResponseCode(
                $expectedCode,
                sprintf("HTTP Status code %d != %d: %s\n", $code, $expectedCode, $debug)
            );
        } else {
            $this->assertResponseCode($expectedCode); // toujours vrais
        }
        return json_decode($body, true);
    }
}
