<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\Client;
use AsalaeCore\TestSuite\TestCase;
use Cake\Http\Client\Response;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Versae\Test\Mock\ClientMock;

/**
 * Versae\Controller\ArchivingSystemsController Test Case
 *
 * @uses \Versae\Controller\ArchivingSystemsController
 */
class ArchivingSystemsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;

    public $fixtures = [
        'app.Acos',
        'app.ArchivingSystems',
        'app.Aros',
        'app.ArosAcos',
        'app.Configurations',
        'app.Filters',
        'app.Ldaps',
        'app.Notifications',
        'app.OrgEntities',
        'app.Roles',
        'app.SavedFilters',
        'app.Sessions',
        'app.TypeEntities',
        'app.Users',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->genericSessionData['Auth']->set(
            'admin',
            [
                'tech' => true,
                'data' => [
                    'username' => 'testunit'
                ]
            ]
        );
        $this->session($this->genericSessionData);
        $response = $this->createMock(Response::class);
        $response->method('getJson')->willReturn([]);
        $client = $this->getMockBuilder(Client::class)
            ->setConstructorArgs([])
            ->onlyMethods(['get', 'post'])
            ->getMock();
        $client->method('get')->willReturn($response);
        $client->method('post')->willReturn($response);
        ClientMock::$mock = $client;
        Utility::set(Client::class, new ClientMock());
    }

    public function tearDown(): void
    {
        Utility::reset();
        parent::tearDown();
    }

    public function testIndex(): void
    {
        $this->httpGet('/archiving-systems/index');
        $this->assertResponseCode(200);
    }

    public function testAdd(): void
    {
        $this->httpGet('/archiving-systems/add');
        $this->assertResponseCode(200);

        $ArchivingSystems = TableRegistry::getTableLocator()->get('ArchivingSystems');
        $ArchivingSystems->deleteAll([]);
        $data = [
            'org_entity_id' => 2,
            'name' => 'testunit',
            'description' => 'test',
            'url' => 'https://libriciel.fr',
            'username' => 'pastell',
            'use_proxy' => false,
            'active' => true,
            'password' => 'foo',
            'ssl_verify_peer' => true,
            'ssl_verify_peer_name' => true,
            'ssl_verify_depth' => 5,
            'ssl_verify_host' => true,
            'ssl_cafile' => '',
            'chunk_size' => null,
        ];
        $this->httpPost('/archiving-systems/add', $data);
        $this->assertResponseCode(200);
        $this->assertCount(1, $ArchivingSystems->find());
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testEdit(): void
    {
        $this->httpGet('/archiving-systems/edit/1');
        $this->assertResponseCode(200);

        $data = [
            'org_entity_id' => 2,
            'name' => 'testunit',
            'description' => 'test',
            'url' => 'https://libriciel.fr',
            'username' => 'pastell',
            'use_proxy' => false,
            'active' => true,
            'password' => 'foo',
            'ssl_verify_peer' => true,
            'ssl_verify_peer_name' => true,
            'ssl_verify_depth' => 5,
            'ssl_verify_host' => true,
            'ssl_cafile' => '',
            'chunk_size' => null,
        ];
        $this->httpPut('/archiving-systems/edit/1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testDelete(): void
    {
        $this->httpDelete('/archiving-systems/delete/1');
        $this->assertResponseCode(200);
        $this->assertResponseNotContains('done');

        $ArchivingSystems = TableRegistry::getTableLocator()->get('ArchivingSystems');
        $ArchivingSystems->updateAll(['org_entity_id' => null], []);
        $this->httpDelete('/archiving-systems/delete/1');
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
    }

    public function testView(): void
    {
        $this->httpGet('/archiving-systems/view/1');
        $this->assertResponseCode(200);
    }

    public function testTestConnection(): void
    {
        $data = [
            'name' => 'test',
            'url' => 'https://localhost',
            'username' => 'admin',
            'password' => 'admin',
            'use_proxy' => false,
            'active' => true,
            'ssl_verify_peer' => false,
            'ssl_verify_peer_name' => false,
            'ssl_verify_depth' => 5,
            'ssl_verify_host' => false,
            'ssl_cafile' => '',
            'chunk_size' => null,
        ];
        $this->httpPost('/archiving-systems/test-connection', $data);
        $this->assertResponseCode(200);
    }
}
