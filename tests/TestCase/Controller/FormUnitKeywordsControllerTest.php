<?php

declare(strict_types=1);

namespace Versae\Test\TestCase\Controller;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Throwable;

/**
 * Versae\Controller\FormUnitKeywordsController Test Case
 *
 * @uses \Versae\Controller\FormUnitKeywordsController
 */
class FormUnitKeywordsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        'app.Forms',
        'app.FormUnitKeywords',
        'app.FormUnits',
        'app.FormUnitKeywordDetails',
        'app.FormVariables',
    ];

    /**
     * Setup the test case, backup the static object values so they can be restored.
     * Specifically backs up the contents of Configure and paths in App if they have
     * not already been backed up.
     *
     * @return void
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Filesystem::reset();
    }

    /**
     * teardown any static object changes and restore them.
     *
     * @return void
     * @throws Exception
     */
    public function tearDown(): void
    {
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        parent::tearDown();
    }

    /**
     * Test add method
     *
     * @return void
     * @throws Throwable
     * @uses \Versae\Controller\FormUnitKeywordsController::add()
     */
    public function testAdd(): void
    {
        $this->httpGet('/form-unit-keywords/add/1');
        $this->assertResponseCode(200);

        $FormUnitKeywords = TableRegistry::getTableLocator()->get('FormUnitKeywords');
        $count = $FormUnitKeywords->find()->count();
        $data = ['name' => 'test_add_keyword'];
        $this->httpPost('/form-unit-keywords/add/1', $data);
        $this->assertResponseCode(200);
        $this->assertCount($count + 1, $FormUnitKeywords->find());
    }

    /**
     * Test edit method
     *
     * @return void
     * @throws Throwable
     * @uses \Versae\Controller\FormUnitKeywordsController::edit()
     */
    public function testEdit(): void
    {
        $this->httpGet('/form-unit-keywords/edit/1');
        $this->assertResponseCode(200);

        $FormUnitKeywords = TableRegistry::getTableLocator()->get('FormUnitKeywords');
        $data = ['name' => 'test_edit_keyword'];
        $this->httpPut('/form-unit-keywords/edit/1', $data);
        $this->assertResponseCode(200);
        $this->assertCount(1, $FormUnitKeywords->find()->where(['name' => 'test_edit_keyword']));
    }

    /**
     * Test delete method
     *
     * @return void
     * @throws Throwable
     * @uses \Versae\Controller\FormUnitKeywordsController::delete()
     */
    public function testDelete(): void
    {
        $FormUnitKeywords = TableRegistry::getTableLocator()->get('FormUnitKeywords');
        $count = $FormUnitKeywords->find()->count();
        $this->httpDelete('/form-unit-keywords/delete/1');
        $this->assertResponseCode(200);
        $this->assertCount($count - 1, $FormUnitKeywords->find());
    }
}
