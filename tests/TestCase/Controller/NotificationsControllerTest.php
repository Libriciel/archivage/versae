<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use AsalaeCore\Factory;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Versae\Test\Mock\FakeClass;

class NotificationsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;

    public $fixtures = [
        'app.Acos',
        'app.Aros',
        'app.ArosAcos',
        'app.Notifications',
        'app.OrgEntities',
        'app.Roles',
        'app.Sessions',
        'app.TypeEntities',
        'app.Users',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        Factory\Utility::set('Notify', FakeClass::class);
        Factory\Utility::set('Beanstalk', FakeClass::class);
    }

    public function tearDown(): void
    {
        Factory\Utility::reset();
        parent::tearDown();
    }

    public function testTest()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $data = [
            'msg' => 'test du test'
        ];
        $this->httpPost('/notifications/test', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response['result']));
        $this->assertTrue(!empty($response['result']['id']));
        $this->assertTrue(!empty($response['result']['text']));
        $this->assertEquals('test du test', $response['result']['text']);

        $Notifications = TableRegistry::getTableLocator()->get('Notifications');
        $notif = $Notifications->find()->where(['id' => $response['result']['id']])->first();
        $this->assertTrue((bool)$notif);
    }

    public function testDelete()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpDelete('/notifications/delete/1');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response['report']));
        $this->assertEquals('done', $response['report']);
    }
}
