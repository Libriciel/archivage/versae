<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Test\Mock\StaticGenericUtility;
use Beanstalk\Utility\Beanstalk;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Security;
use Cake\View\ViewBuilder;
use DateInterval;
use DateTime;
use Versae\Controller\Component\LoginComponent;
use Versae\Controller\UsersController;
use Versae\Model\Table\DepositsTable;

/**
 * Versae\Controller\UsersController Test Case
 */
class UsersControllerTest extends TestCase
{
    use AutoFixturesTrait;
    use InvokePrivateTrait;
    use IntegrationTestTrait;
    use HttpTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_FORMS,
        'app.Acos',
        'app.ArchivingSystems',
        'app.Aros',
        'app.ArosAcos',
        'app.AuthSubUrls',
        'app.AuthUrls',
        'app.BeanstalkJobs', // FIXME - on ne devrait pas en avoir besoin
        'app.Configurations',
        'app.Deposits',
        'app.Fileuploads',
        'app.Filters',
        'app.Ldaps',
        'app.Mails',
        'app.Notifications',
        'app.OrgEntities',
        'app.Roles',
        'app.RolesTypeEntities',
        'app.SavedFilters',
        'app.Sessions',
        'app.Transfers',
        'app.TypeEntities',
        'app.Users',
    ];

    public function setUp(): void
    {
        parent::setUp();
        Configure::write('Password.complexity', 1);
        $beanstalk = $this->createMock(Beanstalk::class);
        $beanstalk->method('emit')->willReturn(true);
        $beanstalk->method('setTube')->willReturnSelf();
        Utility::set('Beanstalk', $beanstalk);
        Configure::write('Keycloak.enabled', false);
        Configure::write('OpenIDConnect.enabled', false);
    }

    public function tearDown(): void
    {
        Utility::reset();
        parent::tearDown();
    }

    private function logged()
    {
        $this->session($this->genericSessionData);
        Utility::reset();
    }

    public function testLogin()
    {
        $this->get('/users/login');
        $this->assertResponseCode(200);

        $data = [
            'username' => 'testunit',
            'password' => 'bad-password',
        ];
        $this->httpPost('/users/login', $data);
        $this->assertResponseCode(200);

        LoginComponent::$attempts = [];
        $data = [
            'username' => 'admin',
            'password' => 'admin',
        ];
        $this->post('/users/login', $data);
        $this->assertRedirect('/');

        $this->logged();
        $this->get('/users/login');
        $this->assertRedirect('/');
    }

    public function testLogout()
    {
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $file = $Fileuploads->find()->firstOrFail();
        $file->set('path', TMP . 'testunit.tmpfile');
        $file->set('locked', false);
        $Fileuploads->save($file);
        file_put_contents($file->get('path'), 'Lorem ipsum dolor sit amet');

        $this->cookie('test', 'foo');
        $this->logged();
        $this->get('/users/logout');
        if (is_file($file->get('path'))) {
            unlink($file->get('path'));
            $this->fail("le fichier doit être supprimé au logout");
        }
        $this->assertRedirect('/');
    }

    public function testEdit()
    {
        $this->logged();
        $this->configRequest(
            $req = [
                'headers' => [
                    'Accept' => 'application/json',
                    'X-Requested-With' => 'XMLHttpRequest'
                ]
            ]
        );
        $this->httpGet('/users/edit');
        $this->assertResponseCode(200);

        $data = [
            'username' => 'test',
            'name' => 'foo bar',
            'email' => 'foo.bar@libriciel.fr',
            'high_contrast' => false,
            'password' => 'testunit',
            'confirm-password' => 'error',
        ];
        $this->configRequest($req);
        $this->put('/users/edit', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'false');

        $data['confirm-password'] = $data['password'];
        $this->configRequest($req);
        $this->put('/users/edit', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testEditByAdmin()
    {
        $this->logged();
        $this->configRequest(
            $req = [
                'headers' => [
                    'Accept' => 'application/json',
                    'X-Requested-With' => 'XMLHttpRequest'
                ]
            ]
        );
        $this->get('/users/edit-by-admin/1');
        $this->assertResponseCode(200);

        $data = [
            'username' => 'test',
            'name' => 'foo bar',
            'email' => 'foo.bar@libriciel.fr',
            'high_contrast' => false,
            'role_id' => 1,
            'active' => false,
        ];
        $this->configRequest($req);
        $this->put('/users/edit-by-admin/1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testAdd()
    {
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $AuthUrls->deleteAll([]);
        $this->logged();
        $this->configRequest(
            $req = [
                'headers' => [
                    'Accept' => 'application/json',
                    'X-Requested-With' => 'XMLHttpRequest'
                ]
            ]
        );
        $this->get('/users/add/2');
        $this->assertResponseCode(200);

        $this->configRequest($req);
        $this->post('/users/add/2');
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'false');

        $data = [
            'role_id' => 1,
            'username' => 'testunit2',
            'name' => 'testunit2 testunit2',
            'email' => 'testunit2@test.fr',
            'high_contrast' => false,
        ];
        $this->configRequest($req);
        $this->post('/users/add/2', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testLoadCookies()
    {
        $this->cookie('test', 'foo');
        $Users = TableRegistry::getTableLocator()->get('Users');
        $user = $Users->get(1);
        $user->set('cookies', json_encode(['test' => 'bar']));
        $Users->saveOrFail($user);

        $data = [
            'prev_archival_agency_id' => '2',
            'prev_username' => 'admin',
            'username' => 'admin',
            'password' => 'admin'
        ];
        $this->post('/users/ajax-login', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testAjaxLogin()
    {
        $this->get('/users/ajax-login');
        $this->assertResponseCode(200);

        LoginComponent::$attempts = [];
        $data = [
            'prev_archival_agency_id' => '2',
            'prev_username' => 'admin',
            'name' => 'testunit',
            'password' => 'badpassword'
        ];
        $this->post('/users/ajax-login', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'false');

        LoginComponent::$attempts = [];
        $data = [
            'prev_archival_agency_id' => '2',
            'prev_username' => 'admin',
            'username' => 'admin',
            'password' => 'admin'
        ];
        $this->post('/users/ajax-login', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testAjaxSaveMenu()
    {
        $this->logged();
        $Users = TableRegistry::getTableLocator()->get('Users');
        $menu = $Users->get(1)->get('menu');
        $data = [
            'menu' => json_encode(
                [
                    [
                        "href" => "#",
                        "class" => "navbar-link",
                        "@" => "Foo",
                        "@close" => true,
                    ],
                ]
            )
        ];
        $this->configRequest(
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'X-Requested-With' => 'XMLHttpRequest'
                ]
            ]
        );
        $this->post('/users/ajax-save-menu', $data);
        $this->assertResponseCode(200);
        $this->assertNotEquals($menu, $Users->get(1)->get('menu'));
    }

    public function testDelete()
    {
        $Transfers = $this->fetchTable('Transfers');
        $Deposits = $this->fetchTable('Deposits');

        $Users = $this->fetchTable('Users');
        $user = $Users->newEntity(
            [
                'id' => '',
                'username' => uniqid('test-'),
            ] + $Users->get(1)->toArray(),
            ['validate' => false]
        );
        $this->assertNotFalse((bool)$Users->save($user));

        $Deposits->saveOrFail(
            $editingDeposit = $Deposits->newEntity(
                [
                    'form_id' => 2,
                    'archival_agency_id' => 1,
                    'transferring_agency_id' => 1,
                    'name' => 'editing',
                    'state' => DepositsTable::S_EDITING,
                    'created_user_id' => $user->id,
                    'bypass_validation' => true,
                ]
            )
        );
        $Deposits->saveOrFail(
            $Deposits->newEntity(
                [
                    'form_id' => 2,
                    'archival_agency_id' => 1,
                    'transferring_agency_id' => 1,
                    'name' => 'not_editing',
                    'state' => DepositsTable::S_READY_TO_PREPARE,
                    'created_user_id' => $user->id,
                    'bypass_validation' => true,
                ]
            )
        );
        $Transfers->saveOrFail(
            $Transfers->newEntity(
                [
                    'identifier' => 'testDelete',
                    'deposit_id' => 1,
                    'created_user_id' => $user->id,
                    'validated' => true,
                ]
            )
        );

        $this->logged();
        $this->_request['headers']['X-Versae-Force-Delete'] = true;
        $this->httpDelete('/users/delete/' . $user->id);
        $response = json_decode((string)$this->_response);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response->report);
        $this->assertEquals('done', $response->report);

        $this->assertCount(0, $Transfers->find()->where(['created_user_id' => $user->id]));
        $this->assertCount(0, $Deposits->find()->where(['created_user_id' => $user->id]));
        $this->assertCount(0, $Deposits->find()->where(['id' => $editingDeposit->id]));
    }

    public function testResetSession()
    {
        $this->logged();
        $this->configRequest(
            $req = [
                'headers' => [
                    'Accept' => 'application/json',
                    'X-Requested-With' => 'XMLHttpRequest'
                ]
            ]
        );
        StaticGenericUtility::$methods['doReset'] = true;
        Utility::set('Session', StaticGenericUtility::class);
        $message = base64_encode(
            Security::encrypt(
                json_encode(
                    [
                        'id' => 1, // user_id
                        'key' => $key = hash('sha256', 'testunit'),
                        'expire' => (new DateTime())->add(new DateInterval('P1Y')),
                    ]
                ),
                hash('sha256', 'testunit')
            )
        );
        $data = [
            'message' => $message
        ];
        $this->post('/users/reset-session', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');

        $this->session(
            $this->genericSessionData + [
                'reset_keys' => [$key]
            ]
        );
        $this->configRequest($req);
        StaticGenericUtility::$methods['doReset'] = false;
        $this->post('/users/reset-session', $data);
        $this->assertResponseCode(200);
        $this->assertTrue(!$this->_response->hasHeader('X-Asalae-Success'));
    }

    public function testInitializePassword()
    {
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $AuthUrls->deleteAll([]);
        $code1 = $AuthUrls->newEntity(['url' => '/users/initialize-password/1']);
        $code2 = $AuthUrls->newEntity(['url' => '/users/forgotten-password/1']);
        $initialCount = $AuthUrls->find()->count();
        $AuthUrls->saveOrFail($code1);
        $AuthUrls->saveOrFail($code2);
        $data = [
            'password' => 'testunit',
            'confirm-password' => 'testunit',
        ];

        // syntaxe url incorrecte
        $this->session(
            [
                'Auth' => [
                    'code' => $code2->get('code') . 'a',
                    'url' => '/users/badurl'
                ]
            ]
        );
        $this->put('/users/initialize-password/1', $data);
        $this->assertResponseContains(__("La page demandée n'est pas/plus valide. La durée d'accès peut être dépassée ou le traitement déjà effectué."));

        // get
        $this->session(
            [
                'Auth' => [
                    'code' => $code1->get('code'),
                    'url' => $code1->get('url')
                ]
            ]
        );
        $this->get('/users/initialize-password/1');
        $this->assertResponseCode(200);

        // Réussite
        $this->put('/users/initialize-password/1', $data);
        $this->assertRedirect('/');
        // il reste $code2
        $this->assertCount($initialCount + 1, $AuthUrls->find());
    }

    public function testIndex()
    {
        $this->logged();
        $this->httpGet('/users/index');
        $this->assertResponseCode(200);
    }

    public function testSetAvailablesRoles()
    {
        $UsersController = new UsersController();
        $this->invokeMethod($UsersController, 'setAvailablesRoles', [null]);
        $this->assertEquals([], $UsersController->immutableViewVars['roles']);
    }

    public function testForgottenPassword()
    {
        $loc = TableRegistry::getTableLocator();
        $AuthUrls = $loc->get('AuthUrls');
        $AuthUrls->deleteAll([]);
        $viewBuilder = $this->createMock(ViewBuilder::class);
        $viewBuilder->method('setTemplate')->willReturnSelf();
        $viewBuilder->method('setLayout')->willReturnSelf();

        $this->httpGet('/users/forgottenPassword');
        $this->assertResponseCode(200);
    }
}
