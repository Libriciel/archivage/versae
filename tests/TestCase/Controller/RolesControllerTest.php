<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Hash;

class RolesControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;

    public $fixtures = [
        'app.Acos',
        'app.Aros',
        'app.ArosAcos',
        'app.OrgEntities',
        'app.Roles',
        'app.RolesTypeEntities',
        'app.TypeEntities',
        'app.Users',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        TableRegistry::getTableLocator()->clear();
        TableRegistry::getTableLocator()->get('Aros');
        TableRegistry::getTableLocator()->get('Roles');
        TableRegistry::getTableLocator()->get('ArosAcos');
    }

    public function testIndex()
    {
        $this->fetchTable('Roles')->updateAll(['org_entity_id' => 2], ['id' => 1]);
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/roles/index/1');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response['roles']));
    }

    public function testAdd()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/roles/add/2');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response['entity']));

        $data = [
            'name' => 'test_2',
            'active' => true,
            'hierarchical_view' => false,
        ];

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPost('/roles/add/2', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response['id']));
    }

    public function testEdit()
    {
        $this->fetchTable('Roles')->updateAll(['org_entity_id' => 2], ['id' => 1]);
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/roles/edit/1');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response['entity']));

        $data = [
            'name' => 'test_2',
            'active' => true,
            'hierarchical_view' => false,
        ];

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPut('/roles/edit/1', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response['id']));
        $this->assertEquals('test_2', $response['name']);
    }

    public function testDelete()
    {
        $this->fetchTable('Roles')->updateAll(['org_entity_id' => 2], ['id' => 1]);
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpDelete('/roles/delete/1');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response['report']));
        $this->assertEquals('done', $response['report']);
    }

    public function testView()
    {
        $response = $this->httpGet('/roles/view/1', true);
        $this->assertResponseCode(200);
        $this->assertTrue(Hash::get($response, 'accesses.controllers.Roles.action.view'));
    }
}
