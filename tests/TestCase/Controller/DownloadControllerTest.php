<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\TestSuite\IntegrationTestTrait;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Versae\Controller\DownloadController Test Case
 *
 * @uses \Versae\Controller\DownloadController
 */
class DownloadControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        'app.Fileuploads',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Filesystem::reset();
        Filesystem::dumpFile(TMP_TESTDIR . DS . 'testfile.txt', 'test');
        Filesystem::dumpFile(TMP_TESTDIR . DS . 'testfile.pdf', 'test');
    }

    public function tearDown(): void
    {
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        parent::tearDown();
    }

    public function testFile()
    {
        $this->httpGet('/download/file/1');
        $this->assertResponseCode(200);
    }

    public function testOpen()
    {
        $this->httpGet('/download/open/2');
        $this->assertResponseCode(200);
    }

    public function testThumbnailImage()
    {
        $this->httpGet('/download/thumbnailImage/1');
        $this->assertResponseCode(200);
    }
}
