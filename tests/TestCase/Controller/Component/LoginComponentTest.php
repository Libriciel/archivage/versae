<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller\Component;

use Adldap\Adldap;
use Adldap\Auth\Guard;
use Adldap\Connections\Provider;
use Adldap\Models\Model;
use Adldap\Query\Builder;
use Adldap\Query\Factory;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\TestCase;
use Authentication\Controller\Component\AuthenticationComponent;
use Cake\Controller\ComponentRegistry;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use PHPUnit\Framework\MockObject\MockObject;
use Versae\Controller\AppController;
use Versae\Controller\Component\LoginComponent;

class LoginComponentTest extends TestCase
{
    public $fixtures = [
        'app.Configurations',
        'app.Ldaps',
        'app.OrgEntities',
        'app.Roles',
        'app.TypeEntities',
        'app.Users',
        'app.Versions',
    ];

    /**
     * @var LoginComponent $component
     */
    public $component = null;
    /**
     * @var MockObject|AppController
     */
    public $controller = null;

    public function setUp(): void
    {
        parent::setUp();
        $request = new ServerRequest();
        $response = new Response();
        $this->controller = $this->getMockBuilder('Cake\Controller\Controller')
            ->setConstructorArgs([$request, $response])
            ->onlyMethods([])
            ->getMock();
        $registry = new ComponentRegistry($this->controller);
        $this->component = new LoginComponent($registry);
        $this->controller->Authentication = $this->createMock(AuthenticationComponent::class);
        $this->controller->Authentication->method('setIdentity');
    }

    public function tearDown(): void
    {
        Utility::reset();
        parent::tearDown();
    }

    public function testLogUser()
    {
        $this->assertTrue($this->component->logUser('admin', 'admin'));
        LoginComponent::$attempts = [];
        $this->assertFalse($this->component->logUser('testunit', 'bad_password'));

        $adldap = $this->createMock(Adldap::class);
        $adldap->method('addProvider');
        $provider = $this->createMock(Provider::class);
        $adldap->method('connect')->willReturn($provider);
        $guard = $this->createMock(Guard::class);
        $provider->method('auth')->willReturn($guard);
        $guard->method('attempt')->willReturn(true);
        $factory = $this->createMock(Factory::class);
        $provider->method('search')->willReturn($factory);
        $users = $this->createMock(Builder::class);
        $factory->method('users')->willReturn($users);
        $ldapUser = $this->createMock(Model::class);
        $users->method('findBy')->willReturn($ldapUser);
        $ldapUser->method('getAttribute')->willReturn(['foo'], ['bar@baz.fr']);
        Utility::set(Adldap::class, $adldap);
        $this->assertTrue($this->component->logUser('ldap_user', 'test_ldap'));
    }
}
