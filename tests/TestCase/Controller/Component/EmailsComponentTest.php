<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller\Component;

use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\TestCase;
use Beanstalk\Utility\Beanstalk;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\Mailer\Mailer;
use Cake\Mailer\Message;
use Cake\ORM\TableRegistry;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\MockObject\MockObject;
use Versae\Controller\AppController;
use Versae\Controller\Component\EmailsComponent;
use Versae\Model\Entity\AuthUrl;
use Versae\Model\Entity\User;

class EmailsComponentTest extends TestCase
{
    public $fixtures = [
        'app.AuthUrls',
        'app.Configurations',
        'app.Ldaps',
        'app.Mails',
        'app.OrgEntities',
        'app.Roles',
        'app.TypeEntities',
        'app.Users',
        'app.Versions',
    ];

    /**
     * @var EmailsComponent $component
     */
    public $component = null;
    /**
     * @var MockObject|AppController
     */
    public $controller = null;

    public function setUp(): void
    {
        parent::setUp();
        $request = new ServerRequest();
        $response = new Response();
        $this->controller = $this->getMockBuilder('Cake\Controller\Controller')
            ->setConstructorArgs([$request, $response])
            ->onlyMethods([])
            ->getMock();
        $registry = new ComponentRegistry($this->controller);
        $this->component = new EmailsComponent($registry);
        /** @var Mailer|MockObject $email */
        $email = $this->getMockBuilder(Mailer::class)
            ->onlyMethods(['send'])
            ->getMock();
        $email->method('send')->willReturn(['the mail content']);
        Utility::set(Mailer::class, $email);
        $beanstalk = $this->createMock(Beanstalk::class);
        $beanstalk->method('emit')->willReturn(true);
        $beanstalk->method('setTube')->willReturnSelf();
        Utility::set('Beanstalk', $beanstalk);
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Configure::write('debug_mails_basepath', TMP_TESTDIR);
    }

    public function tearDown(): void
    {
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Utility::reset();
        parent::tearDown();
    }

    public function testLogUser()
    {
        // email = @test.fr
        $this->assertInstanceOf(
            AuthUrl::class,
            $this->component->submitEmailNewUser(new User(['id' => 1]))
        );

        TableRegistry::getTableLocator()->get('Users')
            ->updateAll(['email' => 'toto@tata.fr'], ['id' => 1]);
        $this->assertTrue($this->component->submitEmailNewUser(new User(['id' => 1])));
    }

    public function testdebug()
    {
        $message = $this->getMockBuilder(Message::class)
            ->onlyMethods(['getBodyHtml'])
            ->getMock();
        $message->method('getBodyHtml')->willReturn('test');
        $email = $this->getMockBuilder(Mailer::class)
            ->onlyMethods(['getMessage'])
            ->getMock();
        $email->method('getMessage')->willReturn($message);

        Configure::write('debug_mails', false);
        $this->assertNull($this->component->debug($email));
        Configure::write('debug_mails', true);
        $this->assertInstanceOf(
            Response::class,
            $this->component->debug($email)
        );
    }
}
