<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\SaveBufferTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Libriciel\Filesystem\Utility\Filesystem;

class TransfersControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;
    use SaveBufferTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        'app.Deposits',
        'app.Transfers',
    ];

    public function setUp(): void
    {
        parent::setUp();
        Configure::write('App.paths.data', TMP_TESTDIR);
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Filesystem::mkdir(TMP_TESTDIR);
        Filesystem::dumpFile(TMP_TESTDIR . '/deposits/5_12345678/transfer.tar.gz', 'test');
    }

    public function tearDown(): void
    {
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        parent::tearDown();
    }

    public function testzip()
    {
        $this->get('/api/transfers/zip/1');
        $this->assertResponseCode(401);

        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $code = $AuthUrls->newEntity([], ['validate' => false]);
        $AuthUrls->patchEntity($code, ['url' => '/api/transfers/zip/1']);
        $AuthUrls->save($code);
        $this->session(
            [
                'Auth.code' => $code->get('code'),
                'Auth.url' => $code->get('url'),
            ]
        );
        $this->preserveBuffer(
            [$this, 'httpGet'],
            '/api/transfers/zip/1'
        );
        $this->assertResponseOk();
        $this->assertHeader('Content-Disposition', 'attachment; filename="transfer.tar.gz"');
    }
}
