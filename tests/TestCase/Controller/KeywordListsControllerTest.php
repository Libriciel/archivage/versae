<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use Cake\ORM\TableRegistry;
use AsalaeCore\TestSuite\TestCase;
use Cake\TestSuite\IntegrationTestTrait;

class KeywordListsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;

    public $fixtures = [
        'app.Acos',
        'app.Aros',
        'app.ArosAcos',
        'app.Configurations',
        'app.Counters',
        'app.CronExecutions',
        'app.Crons',
        'app.Filters',
        'app.KeywordLists',
        'app.Keywords',
        'app.Ldaps',
        'app.Notifications',
        'app.OrgEntities',
        'app.Roles',
        'app.SavedFilters',
        'app.Sessions',
        'app.TypeEntities',
        'app.Users',
        'app.Versions',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
    }

    public function testIndex()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/keyword-lists/index');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response['data']));

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/keyword-lists/index?SaveFilterSelect=&name[0]=%3Fes*&identifier[0]=%3Fes*&active[0]=1&sort=identifier&direction=asc');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response['data']));

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/keyword-lists/index?SaveFilterSelect=&favoris[0]=1&sort=favorite&direction=desc');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(empty($response['data']));
    }

    public function testAdd()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/keyword-lists/add');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response['entity']));

        $data = [
            'identifier' => 'test-add',
            'name' => 'Test add',
            'description' => "test d'un ajout de liste de mots clés",
            'active' => true
        ];
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPost('/keyword-lists/add', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response['id']));
    }

    public function testEdit()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/keyword-lists/edit/1');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response['entity']));

        $data = [
            'identifier' => 'test-edit',
            'name' => 'Test edit',
            'description' => "test d'édition d'une liste de mots clés",
            'active' => true
        ];
        $this->httpPut('/keyword-lists/edit/1', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response['identifier']));
        $this->assertEquals('test-edit', $response['identifier']);
    }

    public function testDelete()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpDelete('/keyword-lists/delete/1');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response['report']));
        $this->assertEquals('done', $response['report']);
    }

    public function testView()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $response = $this->httpGet('/keyword-lists/view/1', true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['entity']);
    }

    public function testNewVersion()
    {
        $KeywordLists = TableRegistry::getTableLocator()->get('KeywordLists');
        $Keywords = TableRegistry::getTableLocator()->get('Keywords');
        $KeywordLists->updateAll(['version' => 1], []);
        $Keywords->updateAll(['version' => 1], []);

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->get('/keyword-lists/new-version/1');
        $this->assertRedirect('/Keywords/index/1/0');

        $keyword = $Keywords->find()->where(
            [
                'keyword_list_id' => 1,
                'version' => 0
            ]
        )->first();
        $this->assertTrue((bool)$keyword);
    }

    public function testPublishVersion()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->get('/keyword-lists/publish-version/1');
        $this->assertRedirect('/KeywordLists/index');

        $Keywords = TableRegistry::getTableLocator()->get('Keywords');
        $keyword = $Keywords->find()->where(
            [
                'keyword_list_id' => 1,
                'version' => 1
            ]
        )->first();
        $this->assertTrue((bool)$keyword);
    }

    public function testRemoveVersion()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->get('/keyword-lists/remove-version/1');
        $this->assertRedirect('/KeywordLists/index');

        $Keywords = TableRegistry::getTableLocator()->get('Keywords');
        $keyword = $Keywords->find()->where(
            [
                'keyword_list_id' => 1
            ]
        )->first();
        $this->assertFalse((bool)$keyword);
    }
}
