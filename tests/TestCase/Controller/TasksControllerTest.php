<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use AsalaeCore\Factory;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Beanstalk\Utility\Beanstalk;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Versae\Model\Table\BeanstalkJobsTable;
use Versae\Test\Mock\FakeEmitter;

class TasksControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        'app.BeanstalkJobs',
        'app.BeanstalkWorkers',
    ];

    const TUBE = 'testunit-tasks';

    public function setUp(): void
    {
        parent::setUp();
        $this->genericSessionData['Auth']->set(
            'admin',
            [
                'tech' => true,
                'data' => [
                    'username' => 'testunit'
                ]
            ]
        );
        $this->session($this->genericSessionData);

        Beanstalk::$instance = $this->createMock(Beanstalk::class);
        Factory\Utility::set('Notify', FakeEmitter::class);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Factory\Utility::reset();
    }

    public function testIndex()
    {
        $response = $this->httpGet('/Tasks/index');
        $this->assertResponseCode(200);
        $this->assertGreaterThanOrEqual(
            1,
            count(
                $response->all
            )
        );
    }

    public function testAdmin()
    {
        $response = $this->httpGet('/Tasks/admin');
        $this->assertResponseCode(200);
        $this->assertGreaterThanOrEqual(
            1,
            count(
                $response->all
            )
        );
    }

    public function testResume()
    {
        $loc = TableRegistry::getTableLocator();
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'controllers', 'alias' => 'Tasks'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);

        $Jobs = $loc->get('BeanstalkJobs');
        $Jobs->saveOrFail(
            $job = $Jobs->newEntity(
                [
                    'tube' => 'archive',
                    'priority' => 1024,
                    'job_state' => BeanstalkJobsTable::S_FAILED,
                    'user_id' => 1,
                    'delay' => 0,
                    'ttr' => 60,
                    'errors' => null,
                ]
            )
        );
        $response = $this->httpGet('/Tasks/resume/' . $job->id);
        $this->assertEquals(BeanstalkJobsTable::S_PENDING, $response->job_state);
    }

    public function testPause()
    {
        $loc = TableRegistry::getTableLocator();
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'controllers', 'alias' => 'Tasks'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);

        $Jobs = $loc->get('BeanstalkJobs');
        $Jobs->saveOrFail(
            $job = $Jobs->newEntity(
                [
                    'id' => 6063,
                    'tube' => 'archive',
                    'priority' => 1024,
                    'job_state' => BeanstalkJobsTable::S_PENDING,
                    'user_id' => 1,
                    'delay' => 0,
                    'ttr' => 60,
                    'errors' => null,
                ]
            )
        );

        $response = $this->httpGet('/Tasks/pause/' . $job->id);
        $this->assertEquals(BeanstalkJobsTable::S_PAUSED, $response->job_state);
    }

    public function testCancel()
    {
        $loc = TableRegistry::getTableLocator();
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'controllers', 'alias' => 'Tasks'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);

        $Jobs = $loc->get('BeanstalkJobs');
        $response = $this->httpGet('/Tasks/cancel/1');
        $this->assertEquals('done', $response->report);
        $this->assertNull($Jobs->find()->where(['id' => 1])->first());
    }

    public function testAjaxCancel()
    {
        $Jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $this->configRequest(
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'X-Requested-With' => 'XMLHttpRequest',
                ]
            ]
        );
        $this->httpGet('/Tasks/ajax-cancel/1');
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
        $this->assertNull($Jobs->find()->where(['id' => 1])->first());
    }

    public function testAjaxPause()
    {
        $Jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $this->configRequest(
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'X-Requested-With' => 'XMLHttpRequest',
                ]
            ]
        );
        $this->get('/Tasks/ajax-pause/1');
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
        $this->assertEquals(BeanstalkJobsTable::S_PAUSED, $Jobs->get(1)->get('job_state'));
    }

    public function testAjaxResume()
    {
        $Jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $Jobs->updateAll(
            ['job_state' => BeanstalkJobsTable::S_PAUSED],
            ['id' => 1]
        );
        $this->configRequest(
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'X-Requested-With' => 'XMLHttpRequest',
                ]
            ]
        );
        $this->get('/Tasks/ajax-resume/1');
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
        $this->assertEquals(BeanstalkJobsTable::S_DELAYED, $Jobs->get(1)->get('job_state'));
    }

    public function testDeleteTube()
    {
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'controllers', 'alias' => 'Tasks'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);
        $this->get('/Tasks/delete-tube/test');
        $this->assertRedirect('/');
    }

    public function testAdd()
    {
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'controllers', 'alias' => 'Tasks'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);
        $this->configRequest(
            $req = [
                'headers' => [
                    'Accept' => 'application/json',
                    'X-Requested-With' => 'XMLHttpRequest',
                ]
            ]
        );
        $this->httpGet('/Tasks/add');
        $this->assertResponseCode(200);

        $data = [
            'tube' => 'test',
            'priority' => 1024,
            'user_id' => 1,
            'delay' => 0,
            'ttr' => 60,
            'data' => ['foo'],
        ];
        $this->configRequest($req);
        $this->httpPost('/Tasks/add', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testEdit()
    {
        $this->configRequest(
            $req = [
                'headers' => [
                    'Accept' => 'application/json',
                    'X-Requested-With' => 'XMLHttpRequest'
                ]
            ]
        );
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'Tasks', 'alias' => 'edit'])
            ->orderDesc('lft')
            ->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);
        $this->get('/Tasks/edit/1');
        $response = json_decode((string)$this->_response->getBody());
        $this->assertResponseCode(200);
        $this->assertTrue($response && isset($response->entity) && isset($response->entity->job_state));

        $this->configRequest($req);
        $data = [
            'data' => ['foo' => 'bar', 'user_id' => 1],
            'mix' => [['key' => 'bar', 'value' => 'baz']],
        ];
        $this->put('/Tasks/edit/1', $data);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testMyJobInfo()
    {
        $response = $this->httpGet('/Tasks/my-job-info/1');
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response->id);
        $this->assertEquals(1, $response->id);
    }

    public function testAdminJobInfo()
    {
        $response = $this->httpGet('/Tasks/admin-job-info/1');
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response->id);
        $this->assertEquals(1, $response->id);
    }
}
