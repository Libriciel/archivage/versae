<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Hash;
use Libriciel\Filesystem\Utility\Filesystem;

class DevsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;

    public $fixtures = [
        'app.Acos',
        'app.Aros',
        'app.ArosAcos',
        'app.Configurations',
        'app.Filters',
        'app.Ldaps',
        'app.Notifications',
        'app.OrgEntities',
        'app.Roles',
        'app.SavedFilters',
        'app.Sessions',
        'app.TypeEntities',
        'app.Users',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        Filesystem::reset();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        $rulesPath = TMP_TESTDIR . DS . uniqid('controllers-') . '.json';
        Filesystem::copy(Configure::read('App.paths.controllers_rules'), $rulesPath);
        Configure::write('App.paths.controllers_rules', $rulesPath);
        Configure::write('Devs.export_roles_path', $f = TMP_TESTDIR . DS . 'export_roles.json');
        Filesystem::copy(RESOURCES . 'export_roles.json', $f, true);

        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'admin'])->firstOrFail();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()->where(['model' => 'controllers', 'alias' => 'Devs'])->firstOrFail();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id,
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);
    }

    public function tearDown(): void
    {
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Filesystem::reset();
        parent::tearDown();
    }

    public function testPermissions()
    {
        $this->httpGet('/devs/permissions');
        $this->assertResponseCode(200);
    }

    public function testSetPermission()
    {
        $data = [
            'role' => 'Archiviste',
            'path' => 'root/controllers/Devs/setPermission',
            'value' => 'true',
        ];
        $this->httpPost('/devs/set-permission', $data);
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
        $export = json_decode(file_get_contents(TMP_TESTDIR . DS . 'export_roles.json'), true);
        $this->assertEquals('1111', Hash::get($export, 'Archiviste.root/controllers/Devs/setPermission'));
    }
}
