<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\Client;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\TestSuite\VolumeSample;
use Beanstalk\Utility\Beanstalk;
use Cake\Core\Configure;
use Cake\Http\Client\Response;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Datacompressor\Utility\DataCompressor;
use Libriciel\Filesystem\Utility\Filesystem;
use Versae\Model\Table\DepositsTable;
use Versae\Model\Table\FormsTable;
use Versae\Test\Mock\ClientMock;

/**
 * Versae\Controller\DepositsController Test Case
 *
 * @uses \Versae\Controller\DepositsController
 */
class DepositsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_FORMS,
        'app.ArchivingSystems',
        'app.Counters',
        'app.DepositValues',
        'app.Deposits',
        'app.Fileuploads',
        'app.Sequences',
        'app.Transfers',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        VolumeSample::init();
        Filesystem::reset();
        Configure::write('App.paths.data', TMP_TESTDIR);
        Filesystem::dumpFile(TMP_TESTDIR . DS . 'testfile.txt', 'test');
        Filesystem::dumpFile(TMP_TESTDIR . DS . 'testfileinput.txt', 'test');
        Filesystem::mkdir(TMP_TESTDIR . DS . 'deposits/1_12345678');

        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('setTube')->willReturnSelf();
        $beanstalk->method('isConnected')->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);
    }

    public function tearDown(): void
    {
        Utility::reset();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        VolumeSample::destroy();
        parent::tearDown();
    }

    public function testIndexInProgress()
    {
        $response = $this->httpGet('/deposits/index-in-progress');
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response->data);
        foreach ($response->data as $deposit) {
            $this->assertTrue(
                in_array(
                    $deposit->state,
                    [
                        DepositsTable::S_EDITING,
                        DepositsTable::S_READY_TO_PREPARE,
                        DepositsTable::S_PREPARING_ERROR,
                        DepositsTable::S_READY_TO_SEND,
                        DepositsTable::S_SENDING_ERROR,
                        DepositsTable::S_SENT,
                        DepositsTable::S_ACKNOWLEDGING_ERROR,
                        DepositsTable::S_RECEIVED,
                        DepositsTable::S_ANSWERING_ERROR,
                    ]
                )
            );
        }
    }

    public function testIndexAll()
    {
        $response = $this->httpGet('/deposits/index-all');
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response->data);
    }

    public function testIndexMyEntity()
    {
        $response = $this->httpGet('/deposits/index-my-entity');
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response->data);

        $session = $this->getSession()->read();
        $typeEntity = $session['Auth']['org_entity']->get('type_entity');
        $typeEntity->set('code', 'SV');
        $session['Auth']['org_entity']->set('type_entity', $typeEntity);
        $this->session($session);

        $response = $this->httpGet('/deposits/index-my-entity');
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response->data);
    }

    public function testSend()
    {
        $loc = TableRegistry::getTableLocator();
        $Transfers = $loc->get('Transfers');
        $Transfers->deleteAll([]);

        $this->post('/deposits/send/1');
        $this->assertResponseCode(200);
        $this->assertCount(1, $Transfers->find());
    }

    public function testResend()
    {
        $loc = TableRegistry::getTableLocator();
        $Transfers = $loc->get('Transfers');
        $Transfers->deleteAll([]);

        $this->post('/deposits/send/8');
        $this->assertResponseCode(200);
        $this->assertCount(1, $Transfers->find());
    }

    public function testReedit()
    {
        $loc = TableRegistry::getTableLocator();
        $Deposits = $loc->get('Deposits');

        $this->get('/deposits/reedit/6?choice=old');
        $this->assertResponseCode(200);
        $deposit = $Deposits->get(6);
        $this->assertEquals(DepositsTable::S_EDITING, $deposit->get('state'));

        $this->get('/deposits/reedit/4');
        $this->assertResponseCode(404);
    }

    public function testAdd1()
    {
        $this->get('/deposits/add1');
        $this->assertResponseCode(200);

        // form not published
        $data = [
            'form_id' => 1,
            'name' => 'deposit_test',
            'transferring_agency_id' => 2,
        ];
        $this->post('/deposits/add1', $data);
        $this->assertResponseCode(400);

        // wrong transferrring agency
        $data = [
            'form_id' => 9,
            'name' => 'deposit_test',
            'transferring_agency_id' => 1,
        ];
        $this->post('/deposits/add1', $data);
        $this->assertResponseCode(400);

        $data = [
            'form_id' => 9,
            'name' => 'deposit_test',
            'transferring_agency_id' => 3,
            'bypass_validation' => true,
        ];
        $this->post('/deposits/add1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testAdd2()
    {
        $targetFile = VolumeSample::copySample('sample.zip');
        DataCompressor::uncompress($targetFile, TMP_TESTDIR . DS . 'deposits/1_12345678/uncompressed/1/1');
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $Fileuploads->updateAll(['path' => $targetFile], ['id' => 1]);
        $client = $this->getMockBuilder(Client::class)
            ->setConstructorArgs([])
            ->onlyMethods(['get', 'post'])
            ->getMock();

        $responseGet = $this->createMock(Response::class);
        $responseGet->method('getStatusCode')->willReturn(200);
        $responseGet->method('getStringBody')->willReturn('test');
        $client->method('get')->willReturn($responseGet);

        $responsePost = $this->createMock(Response::class);
        $responsePost->method('getStatusCode')->willReturn(200);
        $responsePost->method('getStringBody')->willReturn(
            <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<ArchiveTransfer xmlns="fr:gouv:culture:archivesdefrance:seda:v2.1">
</ArchiveTransfer>
XML
        );
        $client->method('post')->willReturn($responsePost);
        ClientMock::$mock = $client;
        Utility::set(Client::class, new ClientMock());

        // deposit not editing
        $this->post('/deposits/add2/2');
        $this->assertResponseCode(404);

        $data = [
            'inputs' => [
                'input_test' => '1',
                'input_test_file' => 3,
            ]
        ];
        $this->post('/deposits/add2/1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testCheckNewFormVersion()
    {
        $client = $this->getMockBuilder(Client::class)
            ->setConstructorArgs([])
            ->onlyMethods(['get', 'post'])
            ->getMock();

        $responseGet = $this->createMock(Response::class);
        $responseGet->method('getStatusCode')->willReturn(200);
        $responseGet->method('getStringBody')->willReturn('test');
        $client->method('get')->willReturn($responseGet);

        $responsePost = $this->createMock(Response::class);
        $responsePost->method('getStatusCode')->willReturn(200);
        $responsePost->method('getStringBody')->willReturn(
            <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<ArchiveTransfer xmlns="fr:gouv:culture:archivesdefrance:seda:v2.1">
</ArchiveTransfer>
XML
        );
        $client->method('post')->willReturn($responsePost);
        ClientMock::$mock = $client;
        Utility::set(Client::class, new ClientMock());

        $loc = TableRegistry::getTableLocator();
        /** @var FormsTable $Forms */
        $Forms = $loc->get('Forms');
        $Forms->saveOrFail(
            $originalForm = $Forms->newEntity(
                [
                    'org_entity_id' => 2,
                    'name' => 'testNewForm',
                    'description' => 'Lorem ipsum dolor sit amet',
                    'archiving_system_id' => 1,
                    'identifier' => 'testNewForm',
                    'version_number' => 1,
                    'version_note' => 'Lorem ipsum dolor sit amet',
                    'state' => FormsTable::S_EDITING,
                    'transferring_agencies' => ['_ids' => [2]],
                ]
            ),
            [
                'associated' => [
                    'TransferringAgencies' => ['fields' => ['_ids']],
                ],
            ]
        );

        /** @var DepositsTable $Deposits */
        $Deposits = $loc->get('Deposits');
        $Deposits->saveOrFail(
            $deposit = $Deposits->newEntity(
                [
                    'form_id' => $originalForm->get('id'),
                    'name' => 'depositNewForm',
                    'state' => $Deposits::S_EDITING,
                    'archival_agency_id' => 2,
                    'transferring_agency_id' => 2,
                    'created_user_id' => 1,
                    'bypass_validation' => true,
                ]
            )
        );
        
        // get without new form
        $response = $this->httpGet('deposits/checkNewFormVersion/' . $deposit->get('id'), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response);
        $this->assertArrayHasKey('new_form', $response);
        $this->assertFalse($response['new_form']);


        $newForm = $Forms->version($originalForm->get('id'), 'newVersion');
        $newForm->set('state', FormsTable::S_PUBLISHED);
        $Forms->saveOrFail($newForm);
        // get with new form
        $response = $this->httpGet('deposits/checkNewFormVersion/' . $deposit->get('id'), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response);
        $this->assertArrayHasKey('new_form', $response);
        $this->assertTrue($response['new_form']);
    }

    public function testView()
    {
        $response = $this->httpGet('/deposits/view/1', true);
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response['entity']));
    }

    public function testDelete()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpDelete('/deposits/delete/1');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['report']);
        $this->assertEquals('done', $response['report']);
    }
}
