<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use Adldap\Adldap;
use Adldap\Connections\Ldap;
use Adldap\Connections\Provider;
use Adldap\Models\Entry;
use Adldap\Query\Paginator;
use ArrayIterator;
use AsalaeCore\Factory;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\CascadeDelete;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Utility\Object\CommandResult;
use Beanstalk\Test\Mock\MockedPheanstalk;
use Beanstalk\Utility\Beanstalk;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;
use Cake\Mailer\Mailer;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Hash;
use Libriciel\Filesystem\Utility\Filesystem;
use Pheanstalk\Connection;
use Pheanstalk\Job;
use PHPUnit\Framework\MockObject\MockObject;
use stdClass;
use Versae\Model\Table\BeanstalkJobsTable;
use Versae\Model\Table\BeanstalkWorkersTable;
use Versae\Test\Mock\FakeAntivirus;
use Versae\Test\Mock\FakeEmitter;

class AdminsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;

    public $fixtures = [
        'app.Acos',
        'app.ArchivingSystems',
        'app.Aros',
        'app.ArosAcos',
        'app.AuthSubUrls',
        'app.AuthUrls',
        'app.BeanstalkJobs',
        'app.BeanstalkWorkers',
        'app.Configurations',
        'app.Counters',
        'app.CronExecutions',
        'app.Crons',
        'app.Fileuploads',
        'app.Forms',
        'app.Ldaps',
        'app.Notifications',
        'app.OrgEntities',
        'app.Phinxlog',
        'app.RgpdInfos',
        'app.Roles',
        'app.RolesTypeEntities',
        'app.Sequences',
        'app.Sessions',
        'app.TypeEntities',
        'app.Users',
    ];
    public $originalAntivirus;

    public function setUp(): void
    {
        parent::setUp();
        Filesystem::reset();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Filesystem::dumpFile(TMP_TESTDIR . "test_local.json", '{}');
        $hasher = new DefaultPasswordHasher();
        $admins = [['username' => 'admin', 'password' => $hasher->hash('admin')]];
        $administators = TMP_TESTDIR . DS . 'administrateurs.json';
        Configure::write('App.paths.administrators_json', $administators);
        Filesystem::dumpFile($administators, json_encode($admins, JSON_UNESCAPED_SLASHES));
        Filesystem::dumpFile(TMP_TESTDIR . 'path_to_local.php', '<?php return TMP_TESTDIR."test_local.json";?>');
        Configure::write('App.paths.path_to_local_config', TMP_TESTDIR . 'path_to_local.php');
        Configure::write('App.paths.data', TMP_TESTDIR);
        Configure::write('OrgEntities.public_dir', TMP_TESTDIR . DS . 'webroot');
        Configure::write('App.defaultLocale', 'fr_FR');
        Configure::write('Beanstalk.disable_check_ttr', true);
        Configure::write('Beanstalk.table_jobs', 'BeanstalkJobs');
        Configure::write('Beanstalk.table_workers', 'BeanstalkWorkers');
        $this->genericSessionData['Auth']->set(
            'admin',
            [
                'tech' => true,
                'data' => [
                    'username' => 'testunit'
                ]
            ]
        );
        $this->session($this->genericSessionData);

        $Exec = $this->createMock(Exec::class);
        $Exec->method('async')->willReturnSelf();
        $Exec->method('command')->willReturn(
            new CommandResult(['success' => true, 'code' => 0, 'stdout' => '', 'stderr' => ''])
        );
        Factory\Utility::set('Exec', $Exec);
        /** @var BeanstalkJobsTable $model */
        $model = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $model->sync = true;
        $model->setEntityClass(Entity::class);
        /** @var BeanstalkWorkersTable $model */
        $model = TableRegistry::getTableLocator()->get('BeanstalkWorkers');
        $model->sync = true;
        $model->setEntityClass(Entity::class);
        Configure::write('Ip.enable_whitelist', false);
        Configure::write('debug_mails', false);
        Notify::$instance = $this->createMock(Notify::class);
        $beanstalk = $this->createMock(Beanstalk::class);
        $beanstalk->method('isConnected')->willReturn(true);
        Beanstalk::$instance = $beanstalk;
        $this->originalAntivirus = Configure::read('Antivirus.classname');
        Configure::write('Antivirus.classname', FakeAntivirus::class);
    }

    private function setIsConnected($bool): void
    {
        $Connection = $this->createMock(Connection::class);
        $Connection->method('isServiceListening')->willReturn($bool);
        MockedPheanstalk::$MockedConnection = $Connection;
        Factory\Utility::reset();
        if (is_file(TMP . 'test_local.json')) {
            unlink(TMP . 'test_local.json');
        }
    }

    /**
     * Adds additional event spies to the controller/view event manager.
     *
     * @return void
     */
    public function controllerSpy(): void
    {
        if (isset($this->_controller)) {
            $registry = new ComponentRegistry($this->_controller);
            $tasks = $this->getMockBuilder('AsalaeCore\\Controller\\Component\\TasksComponent')
                ->setConstructorArgs([$registry])
                ->onlyMethods(['deleteTube'])
                ->getMock();
            $tasks->expects($this->any())
                ->method('deleteTube')
                ->will($this->returnValue(true));
            $this->_controller->components()->set('Tasks', $tasks);
        }
    }

    public function tearDown(): void
    {
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Filesystem::setNamespace();
        Filesystem::reset();
        Factory\Utility::reset();
        Exec::waitUntilAsyncFinish();
        Configure::write('Antivirus.classname', $this->originalAntivirus);
        parent::tearDown();
    }

    public function testIndex()
    {
        $response = $this->httpGet('/admins/index', true);
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response['system']));
        $this->assertTrue(is_bool($response['system']));
    }

    public function testDeleteTube()
    {
        $job = $this->createMock(Job::class);
        $job->method('getId')->willReturn(1);
        MockedPheanstalk::$MockedJob = $job;
        $this->httpGet('/admins/deleteTube/testunit');
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testAjaxCheck()
    {
        $this->setIsConnected(false);
        $this->httpGet('/admins/ajaxCheck');
        $this->assertResponseCode(200);
    }

    public function testAjaxLogs()
    {
        Filesystem::dumpFile(Configure::read('App.paths.logs', LOGS) . "test_ajax_logs.log", $expected = 'test');
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $response = $this->httpGet('/admins/ajaxLogs', true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response);
        $this->assertNotEmpty($response['logs']);
        $this->assertNotEmpty($response['logs']['test_ajax_logs.log']);
        $this->assertEquals($expected, $response['logs']['test_ajax_logs.log']);
    }

    public function testAjaxTasks()
    {
        $this->setIsConnected(true);
        /** @var BeanstalkJobsTable $jobs */
        $jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $jobs->sync = true;
        $jobs->setEntityClass('Versae\Test\Mock\EmptyEntity');
        $jobs->deleteAll([]);
        $jobs->save(
            $jobs->newEntity(
                [
                    'jobid' => 0,
                    'job_state' => 'ready',
                    'tube' => 'testunit',
                ]
            )
        );

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/admins/ajaxTasks');
        json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
    }

    public function testAjaxCheckDisk()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/admins/ajaxCheckDisk');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response);
        $this->assertNotEmpty($response['disks']);
    }

    public function testAjaxToggleDebug()
    {
        file_put_contents(TMP_TESTDIR . "test_local.json", json_encode(['debug' => true]));
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPost('/admins/ajaxToggleDebug', ['state' => 'false']);
        $this->assertResponseCode(200);
        $conf = json_decode(file_get_contents(TMP_TESTDIR . "test_local.json"));
        $this->assertNotEmpty($conf);
        $this->assertFalse($conf->debug);

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPost('/admins/ajaxToggleDebug', ['state' => 'true']);
        $this->assertResponseCode(200);
        $conf = json_decode(file_get_contents(TMP_TESTDIR . "test_local.json"));
        $this->assertNotEmpty($conf);
        $this->assertTrue($conf->debug);
    }

    public function testNotifyAll()
    {
        $this->httpGet('/admins/notifyAll');
        $this->assertResponseCode(200);
        $this->httpPost('/admins/notifyAll', ['activeusers' => true, 'msg' => 'test', 'color' => 'alert-info']);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testLogin()
    {
        $this->session(
            [
                'Admin' => ['tech' => false],
                'Auth' => null,
            ]
        );
        $this->httpGet('/admins/login');
        $this->assertResponseCode(200);

        $this->post('/admins/login', ['username' => 'admin', 'password' => 'admin']);
        $this->assertRedirect('/admins');
    }

    public function testLogout()
    {
        $this->get('/admins/logout');
        $this->assertRedirect('/users/login');
    }

    public function testAjaxEditConf()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/admins/ajaxEditConf');
        $this->assertResponseCode(200);

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPost(
            '/admins/ajaxEditConf',
            [
                'config' => '{}',
                'app_fullbaseurl' => 'https://libriciel.fr',
                'ignore_invalid_fullbaseurl' => true,
                'password_complexity' => '',
                'datacompressor_encodefilename' => '',
                'filesystem_useshred' => '',
                'tokens-duration_code' => '',
                'tokens-duration_access-token' => '',
                'tokens-duration_refresh-token' => '',
                'beanstalk_tests_timeout' => '',
                'downloads_asyncfilecreationtimelimit' => '',
                'downloads_tempfileconservationtimelimit' => '',
                'proxy_host' => '',
                'proxy_port' => '',
                'proxy_username' => '',
                'proxy_password' => '',
                'paginator_limit' => 20,
                'ajaxpaginator_limit' => 100,
            ]
        );
        $this->assertResponseCode(200);
        $conf = json_decode(file_get_contents(TMP_TESTDIR . "test_local.json"));
        $this->assertNotEmpty($conf);
        $this->assertNotEmpty($conf->App);
        $this->assertEquals('https://libriciel.fr', $conf->App->fullBaseUrl);
    }

    public function testDeleteLogo()
    {
        $Configurations = TableRegistry::getTableLocator()->get('Configurations');
        $Configurations->deleteAll([]);
        $conf = $Configurations->newEntity(
            [
                'org_entity_id' => 1,
                'name' => 'logo-client',
                'setting' => '/testunit/deletelogo.jpg'
            ]
        );
        $Configurations->saveOrFail($conf);
        $publicDir = Configure::read('App.paths.data');
        $webrootDir = rtrim(Configure::read('OrgEntities.public_dir', WWW_ROOT), DS);
        Filesystem::createDummyFile($f1 = $publicDir . '/testunit/deletelogo.jpg', 1024);
        Filesystem::copy($f1, $f2 = $webrootDir . '/testunit/deletelogo.jpg');
        $this->assertFileExists($f1);
        $this->assertFileExists($f2);
        $response = $this->httpDelete('/admins/deleteLogo', [], true);
        $this->assertResponseCode(200);
        $this->assertContains('done', $response);
        $this->assertFileDoesNotExist($f1);
        $this->assertFileDoesNotExist($f2);
    }

    public function testAjaxLogin()
    {
        $this->session(['Admin.tech' => false]);
        $this->httpGet('/admins/ajaxLogin');
        $this->assertResponseCode(200);

        $this->httpPost('/admins/ajaxLogin', ['name' => 'admin', 'password' => 'admin']);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testAjaxInterrupt()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/admins/ajaxInterrupt');
        $this->assertResponseCode(200);

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPost(
            '/admins/ajaxInterrupt',
            [
                'enabled' => true,
                'scheduled__begin' => '01/01/1950 11:20:00',
                'scheduled__end' => '01/01/2500 13:20:00',
                'periodic__begin' => '11:00:00',
                'periodic__end' => '12:00:00',
                'enable_whitelist' => true,
                'whitelist_headers_inline' => 'X-Asalae-Webservice, Foo-Bar',
                'message' => 'test',
                'message_periodic' => 'test',
                'enable_workers' => false,
            ]
        );
        $this->assertResponseCode(200);
        $conf = json_decode(file_get_contents(TMP_TESTDIR . "test_local.json"));
        $this->assertNotEmpty($conf);
        $this->assertNotEmpty($conf->Interruption);
        $this->assertTrue($conf->Interruption->enabled);
    }

    public function testKillWorker()
    {
        /** @var BeanstalkWorkersTable $workers */
        $workers = TableRegistry::getTableLocator()->get('BeanstalkWorkers');
        $workers->sync = true;
        Configure::write('Beanstalk.workers.test.kill', 'echo testunit');
        $this->httpGet('/admins/kill-worker/1');
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testEditServiceExploitation()
    {
        $this->setAjaxRequest();
        $this->httpGet('/admins/edit-service-exploitation');
        $this->assertResponseCode(200);

        $data = [
            'name' => 'testunit',
            'identifier' => 'testunit',
        ];
        $this->setAjaxRequest();
        $this->httpPut('/admins/edit-service-exploitation', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testIndexServicesArchives()
    {
        $this->setAjaxRequest();
        $this->httpGet('/admins/index-services-archives');
        $this->assertResponseCode(200);
    }

    public function testAddServiceArchive()
    {
        $this->setAjaxRequest();
        $this->httpGet('/admins/add-service-archive');
        $this->assertResponseCode(200);

        $this->setAjaxRequest();
        $this->httpPost('/admins/add-service-archive');
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'false');

        $data = [
            'name' => 'testunit',
            'identifier' => 'testunit',
            'max_disk_usage_conv' => 1024,
        ];
        $this->setAjaxRequest();
        $this->httpPost('/admins/add-service-archive', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testEditServiceArchive()
    {
        $this->setAjaxRequest();
        $this->httpGet('/admins/edit-service-archive/2');
        $this->assertResponseCode(200);

        $this->setAjaxRequest();
        $this->httpPut('/admins/edit-service-archive/2', ['_tab' => 'main']);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'false');

        $data = [
            '_tab' => 'main',
            'name' => 'testunit',
            'identifier' => 'testunit',
            'max_disk_usage_conv' => 1024,
        ];
        $this->setAjaxRequest();
        $this->httpPut('/admins/edit-service-archive/2', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testImportLdapUser()
    {
        $ldap = $this->createMock(Ldap::class);
        $ldap->method('search')->willReturn('');

        $entry = $this->createMock(Entry::class);
        $entry->method('getAttribute')->will(
            $this->returnCallback(
                function ($attr) {
                    $return = [
                        'sAMAccountName' => 'testldap',
                        'displayname' => 'ldapuser',
                        'mail' => 'ldap@testunit.org',
                        'cn' => 'cn',
                    ];
                    return isset($return[$attr]) ? [$return[$attr]] : [];
                }
            )
        );
        $paginator = $this->createMock(Paginator::class);
        $paginator->method('count')->willReturn(1);
        $paginator->method('getIterator')->willReturn(new ArrayIterator([$entry]));
        $search = $this->createMock(\Adldap\Query\Factory::class);
        $search->method('__call')
            ->will(
                $this->returnCallback(
                    function ($name) use ($entry, $paginator) {
                        switch ($name) {
                            case 'paginate':
                                return $paginator;
                            case 'firstOrFail':
                                return $entry;
                        }
                    }
                )
            );
        $search->method('newQuery')->willReturnSelf();

        $provider = $this->createMock(Provider::class);
        $provider->method('search')->willReturn($search);

        $ad = $this->createMock(Adldap::class);
        $ad->method('addProvider')->willReturnSelf();
        $ad->method('connect')->willReturn($provider);
        Utility::set(Adldap::class, $ad);

        $response = $this->httpGet('/admins/get-ldap-users/1', true);
        $this->assertResponseCode(200);
        $this->assertEquals([['id' => 'cn', 'text' => 'ldapuser']], $response['results']);

        $this->httpGet('/admins/import-ldap-user/2');
        $this->assertResponseCode(200);

        $data = [
            'ldap_id' => 1,
            'ldap_user' => 'ldapuser',
            'role_id' => 1,
        ];

        $response = $this->httpPost('admins/importLdapUser/2', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
        $this->assertEquals('testldap', $response->username);
    }

    public function testAddUser()
    {
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $AuthUrls->deleteAll([]);
        $this->setAjaxRequest();
        $this->httpGet('/admins/add-user/2');
        $this->assertResponseCode(200);

        $this->setAjaxRequest();
        $this->httpPost('/admins/add-user/2');
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'false');

        $data = [
            'username' => 'test',
            'name' => 'testunit',
            'email' => 'testunit@test.fr',
            'high_contrast' => false,
        ];
        $this->setAjaxRequest();
        $this->httpPost('/admins/add-user/2', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testTail()
    {
        file_put_contents(Configure::read('App.paths.logs', LOGS) . 'testunit.log', 'test');
        ob_start();
        $this->httpGet('/admins/tail/testunit.log/0.1');
        $partialEmit = ob_get_clean();
        $this->assertResponseCode(200);
        $this->assertEquals('test', $partialEmit);
        unlink(Configure::read('App.paths.logs', LOGS) . 'testunit.log');
    }

    public function testIndexCrons()
    {
        $this->httpGet('/admins/index-crons');
        $this->assertResponseCode(200);
    }

    public function testEditCrons()
    {
        $this->httpGet('/admins/edit-cron/1');
        $this->assertResponseCode(200);

        $data = [
            'next' => '2100-01-01 12:00:00',
            'frequency_build' => '1',
            'frequency_build_unit' => 'P_D',
            'active' => true,
            'hidden_locked' => true,
            'locked' => false,
            'use_proxy' => false,
            'update_url' => '',
            'update_md5' => '',
        ];
        $response = $this->httpPut('/admins/edit-cron/1', $data);
        $this->assertResponseCode(200);
        $this->assertInstanceOf(stdClass::class, $response);
        $this->assertTrue(isset($response->id));
        $this->assertTrue(isset($response->next));
        $this->assertTextContains('2100', $response->next);

        $data['next'] = '01/01/2100 12:00:00';
        $response = $this->httpPut('/admins/edit-cron/1', $data);
        $this->assertResponseCode(200);
        $this->assertInstanceOf(stdClass::class, $response);
        $this->assertTrue(isset($response->id));
        $this->assertTrue(isset($response->next));
        $this->assertTextContains('2100', $response->next);
    }

    public function testViewCrons()
    {
        $this->httpGet('/admins/view-cron/1');
        $this->assertResponseCode(200);
    }

    public function testRunCron()
    {
        $this->httpGet('/admins/run-cron/1');
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
    }

    public function testGetCronState()
    {
        $response = $this->httpGet('/admins/get-cron-state/1');
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response) && !empty($response->id));
    }

    public function testWorkerLog()
    {
        $exec = $this->createMock(Exec::class);
        $cmdOutput = new CommandResult(
            [
                'success' => true,
                'code' => 0,
                'stdout' => 'the log value',
                'stderr' => ''
            ]
        );
        $exec->method('command')->willReturn($cmdOutput);
        Factory\Utility::set('Exec', $exec);
        $this->httpGet('/admins/worker-log/1');
        $this->assertResponseCode(200);
        $this->assertResponseContains('the log value');
    }

    public function testPhpInfo()
    {
        $this->httpGet('/admins/php-info');
        $this->assertResponseCode(200);
        $this->assertResponseContains('upload_max_filesize');
    }

    public function testIndexLdaps()
    {
        $this->httpGet('/admins/index-ldaps');
        $this->assertResponseCode(200);
    }

    public function testAddLdap()
    {
        $this->httpGet('/admins/add-ldap');
        $this->assertResponseCode(200);

        $data = [
            'org_entity_id' => 2,
            'name' => 'test',
            'host' => '127.0.0.1',
            'port' => 389,
            'user_query_login' => 'test@adullact.win',
            'user_query_password' => 'test',
            'ldap_root_search' => 'dc=adullact,dc=win',
            'user_login_attribute' => 'sAMAccountName',
            'user_username_attribute' => 'sAMAccountName',
            'ldap_users_filter' => '(memberOf=cn=versae,OU=Groupes,dc=adullact,dc=win)',
            'account_prefix' => '',
            'account_suffix' => '@adullact.win',
            'description' => 'Pour tester la connexion LDAP',
            'use_proxy' => false,
            'use_ssl' => false,
            'use_tls' => false,
            'user_name_attribute' => 'displayname',
            'user_mail_attribute' => 'mail',
        ];
        $this->httpPost('/admins/add-ldap', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testEditLdap()
    {
        $this->httpGet('/admins/edit-ldap/1');
        $this->assertResponseCode(200);

        $data = [
            'name' => 'foo',
        ];
        $this->httpPut('/admins/edit-ldap/1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testDeleteLdap()
    {
        CascadeDelete::truncate(TableRegistry::getTableLocator()->get('Users'));
        $this->httpDelete('/admins/delete-ldap/1');
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
    }

    public function testSetMainArchivalAgency()
    {
        $this->httpGet('/admins/set-main-archival-agency/1');
        $this->assertResponseCode(200);

        $data = [];
        $this->httpPut('/admins/set-main-archival-agency/1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testSendTestMail()
    {
        /** @var Mailer|MockObject $email */
        $email = $this->getMockBuilder(Mailer::class)
            ->onlyMethods(['send'])
            ->getMock();
        $email->method('send')->willReturn(['the mail content']);
        Factory\Utility::set(Mailer::class, $email);

        $data = [
            'email' => 'test@test.fr',
        ];
        $response = $this->httpPost('/admins/send-test-mail', $data, true);
        $this->assertResponseCode(200);
        $this->assertTrue(Hash::get($response, 'success'));

        $beanstalk = $this->getMockBuilder(Beanstalk::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isConnected', 'setTube', 'emit'])
            ->getMock();
        $beanstalk->method('isConnected')->willReturn(true);
        Utility::set('Beanstalk', $beanstalk);
    }

    public function testIndexSessions()
    {
        $this->httpGet('/admins/index-sessions');
        $this->assertResponseCode(200);
    }

    public function testDeleteSessions()
    {
        TableRegistry::getTableLocator()->get('Sessions')->updateAll(
            ['expires' => (int)date('U') + 10],
            ['id' => 1]
        );
        $this->httpDelete('/admins/delete-session/f45df7be-ca83-44ef-960c-1856a8893eb6');
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
    }

    public function testNotifyUser()
    {
        TableRegistry::getTableLocator()->get('Sessions')->updateAll(
            ['expires' => (int)date('U') + 10],
            ['id' => 1]
        );
        $this->httpGet('/admins/notify-user/f45df7be-ca83-44ef-960c-1856a8893eb6');
        $this->assertResponseCode(200);

        Factory\Utility::set('Notify', FakeEmitter::class);

        $data = [
            'msg' => 'foo',
            'color' => 'danger'
        ];
        $response = $this->httpPost(
            '/admins/notify-user/f45df7be-ca83-44ef-960c-1856a8893eb6',
            $data,
            true
        );
        $this->assertResponseCode(200);
        $this->assertTrue(Hash::get($response, 'success'));
    }

    public function testIndexJobs()
    {
        $Jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $job = $Jobs->newEntity(
            [
                'jobid' => 1,
                'tube' => 'test',
                'user_id' => 1,
            ]
        );
        $this->assertNotFalse($Jobs->save($job));

        $this->get('/admins/indexJobs/test');
        $this->assertResponseCode(200);

        MockedPheanstalk::$statsJob['state'] = 'ready';
        $response = $this->httpGet('/admins/indexJobs/test?job_state[]=ready');
        $this->assertResponseCode(200);
        $this->assertGreaterThanOrEqual(1, $response);
    }

    public function testJobInfo()
    {
        $response = $this->httpGet('/admins/jobInfo/1', true);
        $this->assertResponseCode(200);
        $this->assertArrayHasKey('id', $response);
    }

    public function testJobCancel()
    {
        $Jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $initialCount = $Jobs->find()->count();
        $response = $this->httpGet('/admins/jobCancel/1', true);
        $this->assertResponseCode(200);
        $this->assertArrayHasKey('report', $response);
        $this->assertEquals('done', $response['report']);
        $this->assertCount($initialCount - 1, $Jobs->find());
    }

    public function testJobResume()
    {
        $Jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $Jobs->updateAll(['job_state' => BeanstalkJobsTable::S_FAILED], ['id' => 1]);
        $query = $Jobs->find()->where(['job_state' => BeanstalkJobsTable::S_PENDING]);
        $initialCount = $query->count();
        $response = $this->httpGet('/admins/jobResume/1', true);
        $this->assertResponseCode(200);
        $this->assertArrayHasKey('report', $response);
        $this->assertEquals('done', $response['report']);
        $this->assertCount($initialCount + 1, $query);
    }

    public function testJobPause()
    {
        $Jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $Jobs->updateAll(['job_state' => BeanstalkJobsTable::S_PENDING], ['id' => 1]);
        $response = $this->httpGet('/admins/jobPause/1', true);
        $this->assertResponseCode(200);
        $this->assertArrayHasKey('report', $response);
        $this->assertEquals('done', $response['report']);
    }

    /**
     * Requete type ajax
     */
    private function setAjaxRequest(): void
    {
        $this->configRequest(
            [
                'headers' => [
                    'X-Requested-With' => 'XMLHttpRequest'
                ]
            ]
        );
    }

    public function testPingSedaGenerator()
    {
        Configure::write('SedaGenerator.url', 'foo');
        $this->httpPost('/admins/sedagenerator-ping', [], true);
        $this->assertResponseCode(200);
        $this->assertResponseContains('The source URI string appears to be malformed');
    }
}
