<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\Http\Client;
use Cake\Http\Client\Response;
use Cake\TestSuite\IntegrationTestTrait;
use Libriciel\Filesystem\Utility\Filesystem;
use Versae\Test\Mock\ClientMock;

class FormExtractorsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_FORMS,
        'app.Fileuploads',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Configure::write('Upload.dir', TMP_TESTDIR . DS . 'testunit-upload-dir');
        Configure::write('App.paths.data', TMP_TESTDIR);
    }

    public function tearDown(): void
    {
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        parent::tearDown();
    }

    public function testAdd()
    {
        $this->httpGet('/form-extractors/add/1');
        $this->assertResponseOk();

        $data = [
            'name' => 'extract_1file',
            'description' => 'test',
            'type' => 'file',
            'form_input_id' => 1,
            'file_selector' => '*',
            'data_format' => 'xml',
            'data_path' => '/catalog/book[1]/author',
        ];
        $this->httpPost('/form-extractors/add/1', $data);
        $this->assertResponseOk();
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testEdit()
    {
        $this->httpGet('/form-extractors/edit/1');
        $this->assertResponseOk();

        $data = [
            'name' => 'extract_1file',
            'description' => 'test',
            'type' => 'file',
            'form_input_id' => 1,
            'file_selector' => '*',
            'data_format' => 'xml',
            'data_path' => '/catalog/book[1]/author',
        ];
        $this->httpPut('/form-extractors/edit/1', $data);
        $this->assertResponseOk();
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testextractXpaths()
    {
        Filesystem::dumpFile(TMP_TESTDIR . DS . 'testfile.txt', '<root><test>test</test></root>');
        $this->httpGet('/form-extractors/extract-xpaths/1');
        $this->assertResponseOk();
    }

    public function testextractJsonPath()
    {
        Filesystem::dumpFile(TMP_TESTDIR . DS . 'testfile.txt', '{"test": "test"}');
        $this->httpGet('/form-extractors/extract-json-path/1');
        $this->assertResponseOk();
    }

    public function testextractCsvPath()
    {
        Filesystem::dumpFile(TMP_TESTDIR . DS . 'testfile.txt', 'foo,bar');
        $this->httpGet('/form-extractors/extract-csv-path/1');
        $this->assertResponseOk();
    }

    public function testtestFilePath()
    {
        $Fileuploads = $this->fetchTable('Fileuploads');
        $Fileuploads->updateAll(['name' => 'testfile.xml'], ['name' => 'testfile.txt']);
        Filesystem::dumpFile(TMP_TESTDIR . DS . 'testfile.txt', '<root><test>testunit</test></root>');
        $this->httpGet('/form-extractors/test-file-path/1?path=test');
        $this->assertResponseOk();
        $this->assertResponseContains('testunit');

        Filesystem::dumpFile(
            TMP_TESTDIR . DS . 'testfile.txt',
            '<root><test>test1</test><test>test2</test></root>'
        );
        $this->httpGet('/form-extractors/test-file-path/1?path=%2F%2Ftest');
        $this->assertResponseOk();
        $this->assertResponseContains('test1');
        $this->assertResponseContains('test2');
    }

    public function testdelete()
    {
        $this->httpDelete('/form-extractors/delete/1');
        $this->assertResponseOk();
    }

    public function testtestWebserviceFile()
    {
        $client = $this->getMockBuilder(Client::class)
            ->setConstructorArgs([])
            ->onlyMethods(['get', 'post'])
            ->getMock();

        $responseGet = $this->createMock(Response::class);
        $responseGet->method('getStatusCode')->willReturn(200);
        $responseGet->method('getStringBody')->willReturn('test');
        $client->method('get')->willReturn($responseGet);

        $responsePost = $this->createMock(Response::class);
        $responsePost->method('getStatusCode')->willReturn(200);
        $responsePost->method('getStringBody')->willReturn(
            '<?xml version="1.0" encoding="UTF-8"?>
<ArchiveTransfer xmlns="fr:gouv:culture:archivesdefrance:seda:v2.1">
</ArchiveTransfer>'
        );
        $client->method('post')->willReturn($responsePost);
        ClientMock::$mock = $client;
        Utility::set(Client::class, new ClientMock());

        $data = [
            'url' => 'https://localhost',
            'data_format' => 'json',
            'username' => 'test',
            'password' => 'test',
            'ssl_verify_peer' => false,
            'ssl_verify_peer_name' => false,
            'ssl_verify_host' => false,
            'ssl_cafile' => '',
            'use_proxy' => true,
        ];
        $this->httpPost('/form-extractors/test-webservice-file/1', $data);
        $this->assertResponseOk();
    }
}
