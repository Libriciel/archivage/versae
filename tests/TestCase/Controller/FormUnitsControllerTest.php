<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Hash;
use Versae\Model\Table\FormUnitsTable;

/**
 * Versae\Controller\FormUnitsController Test Case
 *
 * @uses \Versae\Controller\FormUnitsController
 */
class FormUnitsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        'app.FormUnits',
        'app.Forms',
        'app.FormInputs',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
    }

    public function testGetTree(): void
    {
        $response = $this->httpGet('/form-units/get-tree/1', true);
        $this->assertEquals(
            'jstree-node-6',
            Hash::get($response, '0.children.1.children.1.id')
        );
        $this->assertEquals(
            'jstree-btn-add-root',
            Hash::get($response, '1.id')
        );
    }

    public function testadd()
    {
        $this->_request['headers']['Accept'] = 'text/html';
        $this->httpGet('/form-units/add/1/1', true);
        $this->assertResponseOk();

        $data = [
            'parent_id' => 1,
            'name' => 'testunit',
            'type' => FormUnitsTable::TYPE_DOCUMENT,
            'form_input_id' => 1,
            'presence_condition_id' => null,
            'search_expression' => null,
            'presence_required' => null,
            'order' => 1, // note: > 0
            'append_addables' => 'true',
        ];
        $this->httpPost('/form-units/add/1/1', $data, true);
        $this->assertResponseOk();
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testedit()
    {
        $this->_request['headers']['Accept'] = 'text/html';
        $this->httpGet('/form-units/edit/1', true);
        $this->assertResponseOk();

        $data = [
            'parent_id' => 1,
            'name' => 'testunit',
            'type' => FormUnitsTable::TYPE_DOCUMENT,
            'form_input_id' => 1,
            'presence_condition_id' => null,
            'search_expression' => null,
            'presence_required' => null,
            'order' => null,
            'append_addables' => 'true',
        ];
        $this->httpPut('/form-units/edit/2', $data, true);
        $this->assertResponseOk();
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testview()
    {
        $this->_request['headers']['Accept'] = 'text/html';
        $this->httpGet('/form-units/view/1', true);
        $this->assertResponseOk();
    }

    public function testgetOrderOptions()
    {
        $response = $this->httpPost('/form-units/get-order-options/1', [], true);
        $this->assertResponseOk();
        $this->assertEquals(1, Hash::get($response, '0.value'));
    }

    public function testmove()
    {
        $this->httpPost('/form-units/move/2/1', [], true);
        $this->assertResponseOk();
    }

    public function testdelete()
    {
        $this->httpDelete('/form-units/delete/1');
        $this->assertResponseOk();
    }
}
