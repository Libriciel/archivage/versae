<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Versae\Model\Table\FormVariablesTable;

class FormCalculatorsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_FORMS,
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
    }

    public function testAdd()
    {
        $loc = TableRegistry::getTableLocator();
        $FormCalculators = $loc->get('FormCalculators');
        $FormVariables = $loc->get('FormVariables');
        $FormInputsFormVariables = $loc->get('FormInputsFormVariables');
        $FormCalculatorsFormVariables = $loc->get('FormCalculatorsFormVariables');
        $initialCalculatorsCount = $FormCalculators->find()->count();
        $initialVariablesCount = $FormVariables->find()->count();
        $initialLinksInputsCount = $FormInputsFormVariables->find()->count();
        $initialLinksVarsCount = $FormCalculatorsFormVariables->find()->count();

        $this->httpGet('/form-calculators/add/1');
        $this->assertResponseOk();

        $data = [
            'form_calculator' => [
                'name' => 'test_calculator',
                'description' => 'test',
            ],
            'type' => FormVariablesTable::TYPE_CONCAT,
            'twig' => 'test {{input.input_test}}',
        ];
        $this->httpPost('/form-calculators/add/1', $data);
        $this->assertResponseOk();
        $this->assertHeader('X-Asalae-Success', 'true');
        $this->assertCount($initialCalculatorsCount + 1, $FormCalculators->find());
        $this->assertCount($initialVariablesCount + 1, $FormVariables->find());
        $this->assertCount($initialLinksInputsCount + 1, $FormInputsFormVariables->find());
        $this->assertCount($initialLinksVarsCount, $FormCalculatorsFormVariables->find());

        $data = [
            'form_calculator' => [
                'name' => 'test_calculator2',
                'description' => 'test',
            ],
            'type' => FormVariablesTable::TYPE_CONCAT,
            'twig' => 'test {{input.input_test}} & {{var.test_calculator}}',
        ];
        $this->httpPost('/form-calculators/add/1', $data);
        $this->assertResponseOk();
        $this->assertHeader('X-Asalae-Success', 'true');
        $this->assertCount($initialCalculatorsCount + 2, $FormCalculators->find());
        $this->assertCount($initialVariablesCount + 2, $FormVariables->find());
        $this->assertCount($initialLinksInputsCount + 2, $FormInputsFormVariables->find());
        $this->assertCount($initialLinksVarsCount + 1, $FormCalculatorsFormVariables->find());
    }

    public function testEdit()
    {
        $this->httpGet('/form-calculators/edit/1');
        $this->assertResponseOk();

        $data = [
            'form_calculator' => [
                'name' => 'test_calculator',
                'description' => 'test',
            ],
            'type' => FormVariablesTable::TYPE_CONCAT,
            'twig' => 'test {{input.input_test}}',
        ];
        $this->httpPut('/form-calculators/edit/1', $data);
        $this->assertResponseOk();
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testDelete()
    {
        $this->httpDelete('/form-calculators/delete/2');
        $this->assertResponseOk();
        $this->assertResponseContains('done');
    }

    public function testDeleteTransferHeader()
    {
        $this->httpDelete('/form-calculators/delete-transfer-header/1');
        $this->assertResponseOk();
    }

    public function testDeleteUnitHeader()
    {
        $this->httpDelete('/form-calculators/delete-unit-header/1');
        $this->assertResponseOk();
    }

    public function testDeleteUnitManagement()
    {
        $this->httpDelete('/form-calculators/delete-unit-management/1');
        $this->assertResponseOk();
    }

    public function testDeleteUnitContent()
    {
        $this->httpDelete('/form-calculators/delete-unit-content/1');
        $this->assertResponseOk();
    }

    public function testDeleteKeywordDetail()
    {
        $this->httpDelete('/form-calculators/delete-keyword-detail/1');
        $this->assertResponseOk();
    }

    public function testaddEditHeaderVar()
    {
        $this->httpGet('/form-calculators/add-edit-header-var/1/1');
        $this->assertResponseOk();

        $data = [
            'twig' => 'testunit'
        ];
        $this->httpPost('/form-calculators/add-edit-header-var/1/1', $data);
        $this->assertResponseOk();
        $this->assertResponseContains('testunit');
    }

    public function testaddEditHeaderUnit()
    {
        $this->httpGet('/form-calculators/add-edit-header-unit/1/1');
        $this->assertResponseOk();

        $data = [
            'twig' => 'testunit'
        ];
        $this->httpPost('/form-calculators/add-edit-header-unit/1/1', $data);
        $this->assertResponseOk();
        $this->assertResponseContains('testunit');
    }

    public function testaddEditManagementUnit()
    {
        $this->httpGet('/form-calculators/add-edit-management-unit/1/1');
        $this->assertResponseOk();

        $data = [
            'twig' => 'testunit'
        ];
        $this->httpPost('/form-calculators/add-edit-management-unit/1/1', $data);
        $this->assertResponseOk();
        $this->assertResponseContains('testunit');
    }

    public function testaddEditContentUnit()
    {
        $this->httpGet('/form-calculators/add-edit-content-unit/1/1');
        $this->assertResponseOk();

        $data = [
            'twig' => 'testunit'
        ];
        $this->httpPost('/form-calculators/add-edit-content-unit/1/1', $data);
        $this->assertResponseOk();
        $this->assertResponseContains('testunit');
    }

    public function testaddEditKeywordDetail()
    {
        $this->httpGet('/form-calculators/add-edit-keyword-detail/1/1');
        $this->assertResponseOk();

        $data = [
            'twig' => 'testunit'
        ];
        $this->httpPost('/form-calculators/add-edit-keyword-detail/1/1', $data);
        $this->assertResponseOk();
        $this->assertResponseContains('testunit');
    }
}
