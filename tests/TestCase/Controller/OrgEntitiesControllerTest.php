<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Object\CommandResult;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Hash;
use Libriciel\Filesystem\Utility\Filesystem;

class OrgEntitiesControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_FORMS,
        'app.Acos',
        'app.ArchivingSystems',
        'app.Aros',
        'app.ArosAcos',
        'app.Configurations',
        'app.Counters',
        'app.CronExecutions',
        'app.Crons',
        'app.Deposits',
        'app.Fileuploads',
        'app.Filters',
        'app.KeywordLists',
        'app.Ldaps',
        'app.Notifications',
        'app.OrgEntities',
        'app.Roles',
        'app.RolesTypeEntities',
        'app.SavedFilters',
        'app.Sequences',
        'app.Sessions',
        'app.TypeEntities',
        'app.Users',
        'app.Versions',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Configure::write('App.paths.data', TMP_TESTDIR);
        Configure::write('OrgEntities.public_dir', TMP_TESTDIR . DS . 'webroot');
        Filesystem::reset();
        $Exec = $this->createMock(Exec::class);
        $Exec->method('async')->willReturnSelf();
        $Exec->method('command')->willReturn(
            new CommandResult(['success' => true, 'code' => 0, 'stdout' => '', 'stderr' => ''])
        );
        Utility::set('Exec', $Exec);
    }

    public function tearDown(): void
    {
        Filesystem::setNamespace();
        Filesystem::reset();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Utility::set('Exec', Exec::class);
        Exec::waitUntilAsyncFinish();
        parent::tearDown();
    }

    public function testIndex()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/OrgEntities/index');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty(Hash::get($response, 'chartDatasource'));
    }

    public function testAdd()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/OrgEntities/add/3');
        $this->assertResponseCode(200);

        $data = [
            'name' => 'test',
            'identifier' => 'test',
            'type_entity_id' => 4,
        ];
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPost('/OrgEntities/add/3', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty(Hash::get($response, 'id'));
    }

    public function testEdit()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/OrgEntities/edit/4');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty(Hash::get($response, 'entity'));

        $data = [
            'name' => 'Un test',
            'identifier' => 'test766',
            'type_entity_id' => 4,
            'parent_id' => 5,
            'active' => true,
        ];
        $this->httpPut('/OrgEntities/edit/4', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty(Hash::get($response, 'parent_id'));
        $this->assertEquals(5, $response['parent_id']);
        $this->assertHeaderContains('X-Asalae-Reload', 'true');

        // test edit SA (pas de parent_id)
        $data = [
            'name' => $newName = 'SA new name',
            'identifier' => 'sa_new_name',
            'type_entity_id' => 2,
            'active' => true,
        ];
        $this->httpPut('/OrgEntities/edit/2', $data);
        $this->assertResponseCode(200);
        $this->assertHeaderContains('X-Asalae-Reload', 'true');
        $this->assertEquals($newName, $this->fetchTable('OrgEntities')->get(2)->get('name'));
    }

    public function testDelete()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpDelete('/OrgEntities/delete/6');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty(Hash::get($response, 'report'));
        $this->assertEquals('done', $response['report']);

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->delete('/OrgEntities/delete/1');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(403);
        $this->assertNotEmpty(Hash::get($response, 'message'));
        $this->assertEquals(__("Tentative de suppression d'une entité protégée"), $response['message']);
    }

    public function testEditParentId()
    {
        // NOTE : 3 et 4 cote à cote -> 3 deviens enfant de 4
        $data = [
            'parent_id' => 4,
        ];
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPut('/OrgEntities/editParentId/3', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty(Hash::get($response, 'success'));

        // NOTE : 3 est enfant de 4 -> 4 deviens enfant de 3 (impossible)
        $data = [
            'parent_id' => 3,
        ];
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPut('/OrgEntities/editParentId/4', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertEmpty($response['success'] ?? null);
    }

    public function testDeleteBackground()
    {
        $publicDir = Configure::read('App.paths.data');
        $webrootDir = rtrim(Configure::read('OrgEntities.public_dir', WWW_ROOT), DS);
        Filesystem::dumpFile($publicDir . DS . 'org-entity-data/4/background.css', '* { background: red; }');
        Filesystem::dumpFile($webrootDir . DS . 'org-entity-data/4/background.css', '* { background: red; }');
        Filesystem::dumpFile($file1 = $publicDir . DS . 'org-entity-data/4/testunit.png', 'testfile');
        Filesystem::dumpFile($file2 = $webrootDir . DS . 'org-entity-data/4/testunit.png', 'testfile');
        $this->assertFileExists($file1);
        $this->assertFileExists($file2);

        $this->httpDelete('/OrgEntities/deleteBackground/4');
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
        $this->assertFileDoesNotExist($file1);
        $this->assertFileDoesNotExist($file2);
    }

    public function testGetAvailablesRoles()
    {
        $this->httpGet('/OrgEntities/getAvailablesRoles/2');
        $this->assertResponseCode(200);
        $this->assertResponseContains('Archiviste');
    }

    public function testGetAvailablesRolesPerson()
    {
        $this->httpGet('/OrgEntities/getAvailablesRoles/2?agent_type=person');
        $this->assertResponseCode(200);
        $this->assertResponseContains('Archiviste');
    }

    public function testEditConfig()
    {
        $Configurations = TableRegistry::getTableLocator()->get('Configurations');
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $conf = $Configurations->get(1);
        Filesystem::dumpFile($conf->get('data_filename'), 'testfile');
        Filesystem::dumpFile($conf->get('webroot_filename'), 'testfile');

        $file = TMP_TESTDIR . $conf->get('setting');
        Filesystem::createDummyFile($file, 1024);
        Filesystem::createDummyFile($Fileuploads->get(1)->get('path'), 1024);

        $data = [
            'fileuploads' => [
                'id' => 1
            ],
            'Configurations' => array_values(
                [
                    'background-position' => ['setting' => '20px 20px'],
                    'header-title' => ['setting' => 'test'],
                    'mail-title-prefix' => ['setting' => 'test'],
                    'mail-html-signature' => ['setting' => 'test'],
                    'mail-text-signature' => ['setting' => 'test'],
                ]
            )
        ];
        $this->httpPut('/OrgEntities/editConfig/4', $data);
        $this->assertResponseCode(200);
        unlink($Fileuploads->get(1)->get('path'));
    }
}
