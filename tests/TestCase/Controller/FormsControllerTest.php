<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Hash;
use Versae\Model\Table\FormInputsTable;
use Versae\Model\Table\FormsTable;

class FormsControllerTest extends TestCase
{
    use AutoFixturesTrait;
    use IntegrationTestTrait;
    use HttpTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_FORMS,
        'app.Acos',
        'app.ArchivingSystems',
        'app.Aros',
        'app.ArosAcos',
        'app.Deposits',
        'app.Filters',
        'app.KeywordLists',
        'app.Keywords',
        'app.OrgEntities',
        'app.SavedFilters',
        'app.TypeEntities',
        'app.Users',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
    }

    public function testIndexEditing()
    {
        $this->httpGet('/forms/indexEditing', true);
        $this->assertResponseCode(200);
    }

    public function testIndexPublished()
    {
        $response = $this->httpGet('/forms/indexPublished', true);
        $this->assertResponseCode(200);
        foreach ($response['data'] as $form) {
            $this->assertEquals(FormsTable::S_PUBLISHED, $form['state']);
        }
    }

    public function testIndexAllLastVersionFilter(): void
    {
        $Forms = TableRegistry::getTableLocator()->get('Forms');
        $Forms->saveOrFail(
            $Forms->newEntity(
                [
                    'org_entity_id' => 2,
                    'name' => 'test_version_filter',
                    'archiving_system_id' => 1,
                    'identifier' => 'foo',
                    'version_number' => 1,
                    'version_note' => 'bar',
                    'state' => FormsTable::S_EDITING,
                    'transferring_agencies' => [2],
                ]
            )
        );
        $Forms->saveOrFail(
            $v2 = $Forms->newEntity(
                [
                    'org_entity_id' => 2,
                    'name' => 'test_version_filter',
                    'archiving_system_id' => 1,
                    'identifier' => 'foo',
                    'version_number' => 2,
                    'version_note' => 'bar',
                    'state' => FormsTable::S_EDITING,
                    'transferring_agencies' => [2],
                ]
            )
        );

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $response = $this->httpGet('/forms/index-all?last_version[0]=1&name[0]=test_version_filter', true);
        $this->assertResponseCode(200);
        $this->assertEquals(1, count($response['data']));
        $this->assertEquals($v2->get('version_number'), $response['data'][0]['version_number']);
    }

    public function testAdd1()
    {
        $data = ['foo' => 'bar'];
        $this->httpPost('/forms/add', $data);
        $this->assertHeader('X-Asalae-Success', 'false');

        $data = [
            'name' => 'foo',
            'description' => 'bar',
            'archiving_system_id' => 1,
            'user_guide' => 'foobar',
            'seda_version' => 'seda2.1',
            'transferring_agencies' => [2],
        ];
        $response = $this->httpPost('/forms/add', $data, true);
        $this->assertHeader('X-Asalae-Success', 'true');
        $this->assertEquals(1, Hash::get($response, 'version_number'));
    }

    public function testAddInput()
    {
        $response = $this->httpPost('/forms/addInput', [], true);
        $this->assertArrayHasKey('form', $response);

        $data = [
            'is_submit' => 1,
            'type' => FormInputsTable::TYPE_TEXT,
        ];
        $this->httpPost('/forms/addInput', $data);
        $this->assertHeader('X-Asalae-Step', 'addFormInput2("text")');
        $this->assertHeader('X-Asalae-Success', 'true');

        // test 2
        $data = [
            'is_submit' => 1,
            'required' => 0,
            'readonly' => 0,
            'select2' => 0,
            'multiple' => 0,
            'checked' => 0,
        ];
        $this->httpPost('/forms/addInput/text', $data);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testEditInput()
    {
        $data = [
            'field' => [
                'id' => 4,
                'name' => 'input_test_file',
                'type' => FormInputsTable::TYPE_FILE,
                'disable_expression' => json_encode(
                    [
                        'cond_display_select' => 'input_empty',
                        'cond_display_input' => 'foo',
                        'cond_display_input_type' => 'text',
                    ]
                )
            ],
            'form' => [
                'fieldsets' => [
                    [
                        'inputs' => [
                            json_encode(
                                [
                                    'id' => 4,
                                    'name' => 'input_test_file',
                                    'type' => FormInputsTable::TYPE_FILE,
                                ]
                            ),
                            json_encode(
                                [
                                    'name' => 'input_test_file_2',
                                    'type' => FormInputsTable::TYPE_FILE,
                                ]
                            )
                        ]
                    ]
                ]
            ]
        ];
        $response = $this->httpPost('/forms/edit-input', $data, true);
        $this->assertEquals(['input_test_file_2' => 'input_test_file_2'], $response['targets']);

        $data = [
            'is_submit' => 1,
            'type' => FormInputsTable::TYPE_TEXT,
        ];
        $this->httpPost('/forms/edit-input', $data);
        $this->assertHeader('X-Asalae-Success', 'true');

        $data = [
            'id' => 1,
            'is_submit' => 1,
            'type' => FormInputsTable::TYPE_TEXT,
            'required' => '1',
        ];
        $response = $this->httpPost('/forms/edit-input', $data, true);
        $this->assertHeader('X-Asalae-Success', 'true');
        $this->assertEquals('1', $response['required']);
    }

    public function testGenerateInput()
    {
        $data = ['foo' => 'bar'];
        $response = $this->httpPost('/forms/generateInput', $data, true);
        $this->assertArrayHasKey('inputData', $response);
        $this->assertEquals($data, $response['inputData']);

        $data = ['foo' => 'bar', 'id' => 1];
        $response = $this->httpPost('/forms/generateInput', $data, true);
        $this->assertArrayHasKey('inputData', $response);
        $this->assertEquals($data, $response['inputData']);
        $this->assertNotEmpty($response['formInput']);
    }

    public function testAddFieldset()
    {
        $response = $this->httpPost('/forms/add-fieldset', [], true);
        $this->assertArrayHasKey('entity', $response);

        $data = [
            'is_submit' => 1,
            'type' => FormInputsTable::TYPE_TEXT,
        ];
        $this->httpPost('/forms/add-fieldset', $data);
        $this->assertHeader('X-Asalae-Success', 'true');

        // test 2
        $data = [
            'is_submit' => 1,
            'legend' => 'foo',
        ];
        $this->httpPost('/forms/add-fieldset', $data);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testEditFieldset()
    {
        $data = [
            'disable_expression' => json_encode(
                [
                    'cond_display_select' => 'input_empty',
                    'cond_display_input' => 'foo',
                    'cond_display_input_type' => 'text',
                ]
            ),
            'form' => [
                'fieldsets' => [
                    [
                        'inputs' => [
                            json_encode(
                                [
                                    'id' => 4,
                                    'name' => 'input_test_file',
                                    'type' => FormInputsTable::TYPE_FILE,
                                ]
                            )
                        ]
                    ]
                ]
            ]
        ];
        $response = $this->httpPost('/forms/edit-fieldset', $data, true);
        $this->assertEquals(
            [
                'cond_display_select' => 'input_empty',
                'cond_display_input' => 'foo',
                'cond_display_input_type' => 'text',
                'cond_display_value_select' => null,
                'cond_display_value_file' => null,
                'cond_display_value' => null,
                'disable_expression' => [
                    'cond_display_select' => 'input_empty',
                    'cond_display_input' => 'foo',
                    'cond_display_input_type' => 'text',
                    'cond_display_value_select' => null,
                    'cond_display_value_file' => null,
                    'cond_display_value' => null,
                    'cond_display_way' => null,
                    'cond_display_field' => null,
                ],
                'ord' => -1,
                'cond_display_way' => null,
                'cond_display_field' => null,
            ],
            $response['entity']
        );

        $data = [
            'is_submit' => 1,
            'name' => 'foo',
        ];
        $this->httpPost('/forms/edit-fieldset', $data);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    public function testPreview()
    {
        $data = ['fieldsets' => [['legend' => 'test', 'inputs' => [json_encode(['foo' => 'bar'])]]]];
        $response = $this->httpPost('/forms/preview/1', $data, true);
        $this->assertArrayHasKey('formEntity', $response);
        $this->assertArrayHasKey('form_fieldsets', $response['formEntity']);
        $this->assertEquals('bar', Hash::get($response, 'formEntity.form_fieldsets.0.form_inputs.0.foo'));

        $data = ['is_submit' => 1];
        $response = $this->httpPost('/forms/preview/1', $data, true);
        $this->assertEquals('done', $response);
    }

    public function testKeywordListContent()
    {
        $response = $this->httpGet('/forms/keywordListContent/1', true); // version = 0
        $this->assertEmpty($response);

        $response = $this->httpGet('/forms/keywordListContent/2', true); // version = 1
        $this->assertNotEmpty($response);
    }

    public function testView()
    {
        $Forms = TableRegistry::getTableLocator()->get('Forms');
        $Forms->saveOrFail(
            $form = $Forms->newEntity(
                [
                    'org_entity_id' => 2,
                    'name' => 'editing',
                    'archiving_system_id' => 1,
                    'identifier' => 'foo',
                    'version_number' => 1,
                    'version_note' => 'bar',
                    'state' => FormsTable::S_EDITING,
                    'transferring_agencies' => [2],
                ]
            )
        );
        $response = $this->httpGet('/forms/view/' . $form->get('id'), true);
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response['entity']));
    }

    public function testDelete(): void
    {
        $Forms = TableRegistry::getTableLocator()->get('Forms');
        $Forms->saveOrFail(
            $editing = $Forms->newEntity(
                [
                    'org_entity_id' => 2,
                    'name' => 'editing',
                    'archiving_system_id' => 1,
                    'identifier' => 'foo',
                    'version_number' => 1,
                    'version_note' => 'bar',
                    'state' => FormsTable::S_EDITING,
                    'transferring_agencies' => [2],
                ]
            )
        );

        $response = $this->httpDelete('/forms/delete/' . $editing->get('id'), [], true);
        $this->assertResponseCode(200);
        $this->assertContains('done', $response);

        $Forms = TableRegistry::getTableLocator()->get('Forms');
        $Forms->saveOrFail(
            $published = $Forms->newEntity(
                [
                    'org_entity_id' => 2,
                    'name' => 'published',
                    'archiving_system_id' => 1,
                    'identifier' => 'foo',
                    'version_number' => 1,
                    'version_note' => 'bar',
                    'state' => FormsTable::S_PUBLISHED,
                    'transferring_agencies' => [2],
                ]
            )
        );

        $response = $this->httpDelete('/forms/delete/' . $published->get('id'), [], true);
        $this->assertResponseCode(200);
        $this->assertContains('Erreur lors de la suppression', $response);
    }

    public function testDeactivate()
    {
        $Forms = TableRegistry::getTableLocator()->get('Forms');
        $Forms->saveOrFail(
            $form = $Forms->newEntity(
                [
                    'name' => 'foo',
                    'identifier' => 'foo',
                    'archiving_system_id' => 1,
                    'version_number' => 1,
                    'version_note' => 'bar',
                    'org_entity_id' => 2,
                    'state' => FormsTable::S_PUBLISHED,
                    'transferring_agencies' => [2],
                ]
            )
        );

        $this->get('/forms/deactivate/' . $form->get('id'));
        $this->assertResponseCode(200);
        $form = $Forms->get($form->get('id'));

        $this->assertEquals(FormsTable::S_DEACTIVATED, $form->get('state'));
    }

    public function testActivate()
    {
        $Forms = TableRegistry::getTableLocator()->get('Forms');
        $Forms->saveOrFail(
            $form = $Forms->newEntity(
                [
                    'name' => 'foo',
                    'identifier' => 'foo',
                    'archiving_system_id' => 1,
                    'version_number' => 1,
                    'version_note' => 'bar',
                    'org_entity_id' => 2,
                    'state' => FormsTable::S_DEACTIVATED,
                    'transferring_agencies' => [2],
                ]
            )
        );

        $this->get('/forms/activate/' . $form->get('id'));
        $this->assertResponseCode(200);
        $form = $Forms->get($form->get('id'));

        $this->assertEquals(FormsTable::S_PUBLISHED, $form->get('state'));
    }

    public function testVersion()
    {
        $this->get('/forms/version/1'); // non versionnable
        $this->assertResponseCode(403);

        /** @var EntityInterface $firstVersion */
        $firstVersion = TableRegistry::getTableLocator()->get('Forms')->get(9);
        $resp = $this->httpPut('/forms/version/9', ['version_note' => 'lorem']);
        $newVersion = TableRegistry::getTableLocator()->get('Forms')->get($resp->id);
        $this->assertEquals($firstVersion->get('version_number') + 1, $newVersion->get('version_number'));
        $this->assertEquals($firstVersion->get('identifier'), $newVersion->get('identifier'));
        $this->assertEquals('lorem', $newVersion->get('version_note'));
    }

    public function testPublish()
    {
        $loc = TableRegistry::getTableLocator();
        $Forms = $loc->get('Forms');
        /** @var EntityInterface $originalForm */
        $originalForm = $Forms->get($originalId = 2); // state published

        $Forms->saveOrFail(
            $form = $Forms->newEntity(
                [
                    'name' => 'foo',
                    'identifier' => $originalForm->get('identifier'),
                    'archiving_system_id' => 1,
                    'version_number' => $originalForm->get('version_number') + 1,
                    'version_note' => 'bar',
                    'org_entity_id' => 2,
                    'state' => FormsTable::S_EDITING,
                    'transferring_agencies' => [2],
                ]
            )
        );
        $Fieldsets = $loc->get('FormFieldsets');
        $Fieldsets->saveOrFail(
            $fieldset = $Fieldsets->newEntity(
                [
                    'form_id' => $form->get('id'),
                    'ord' => 1,
                ]
            )
        );

        $Inputs = $loc->get('FormInputs');
        $Inputs->saveOrFail(
            $Inputs->newEntity(
                [
                    'form_fieldset_id' => $fieldset->get('id'),
                    'name' => 'foo',
                    'type' => 'file',
                    'ord' => 1,
                    'label' => 'bar',
                    'required' => false,
                    'readonly' => false,
                    'form_id' => $form->get('id'),
                ]
            )
        );

        $FormUnits = TableRegistry::getTableLocator()->get('FormUnits');
        $FormUnits->saveOrFail(
            $FormUnits->newEntity(
                [
                    'form_id' => $form->get('id'),
                    'name' => 'unit',
                    'type' => 'document',
                ]
            )
        );

        $this->get('/forms/publish/' . $form->get('id'));
        $this->assertResponseCode(200);
        $form = $Forms->get($form->get('id'));

        $this->assertEquals(FormsTable::S_PUBLISHED, $form->get('state'));
        $originalForm = $Forms->get($originalId);
        $this->assertEquals(FormsTable::S_DEPRECATED, $originalForm->get('state'));
    }

    public function testUnpublish()
    {
        $loc = TableRegistry::getTableLocator();
        $Forms = $loc->get('Forms');
        $Forms->saveOrFail(
            $form = $Forms->newEntity(
                [
                    'name' => 'foo',
                    'identifier' => 'foo',
                    'archiving_system_id' => 1,
                    'version_number' => 1,
                    'version_note' => 'bar',
                    'org_entity_id' => 2,
                    'state' => FormsTable::S_PUBLISHED,
                    'transferring_agencies' => [2],
                ]
            )
        );

        $this->get('/forms/unpublish/' . $form->get('id'));
        $this->assertResponseCode(200);
        $form = $Forms->get($form->get('id'));

        $this->assertEquals(FormsTable::S_EDITING, $form->get('state'));
    }

    public function testDuplicate(): void
    {
        $Forms = TableRegistry::getTableLocator()->get('Forms');
        $count = $Forms->find()->count();

        $this->httpGet('/forms/duplicate/1');

        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
        $this->assertEquals($count + 1, $Forms->find()->count());
    }
}
