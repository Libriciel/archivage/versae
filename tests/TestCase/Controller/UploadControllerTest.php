<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\VolumeSample;
use AsalaeCore\TestSuite\SaveBufferTrait;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use AsalaeCore\TestSuite\TestCase;
use Cake\TestSuite\IntegrationTestTrait;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Versae\Controller\UploadController Test Case
 */
class UploadControllerTest extends TestCase
{
    use HttpTrait;
    use IntegrationTestTrait;
    use SaveBufferTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        'app.Aros',
        'app.Acos',
        'app.ArosAcos',
        'app.Fileuploads',
        'app.Roles',
        'app.Users',
    ];

    public $server = [];
    public $files = [];

    public function setUp(): void
    {
        parent::setUp();
        $this->files = $_FILES;
        $this->server = $_SERVER;
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $_FILES = [
            'file' => [
                'name' => 'logo.png',
                'type' => 'image/png',
                'tmp_name' => TMP_TESTDIR . DS . uniqid('php'),
                'error' => 0,
                'size' => 7215
            ]
        ];
        Configure::write('Upload.dir', TMP_TESTDIR . DS . 'testunit-upload-dir');
        Configure::write('App.paths.data', TMP_TESTDIR);
        $this->session($this->genericSessionData);
        VolumeSample::init();
        VolumeSample::copySample('logo.png', basename($_FILES['file']['tmp_name']));
    }

    public function tearDown(): void
    {
        parent::tearDown();
        if (isset($_FILES['file']['tmp_name']) && is_file($_FILES['file']['tmp_name'])) {
            unlink($_FILES['file']['tmp_name']);
        }
        $_FILES = $this->files;
        $_SERVER = $this->server;
        VolumeSample::destroy();
    }

    public function testIndex()
    {
        $this->postChunk(
            '/upload',
            TMP_TESTDIR . DS . 'testunit-upload-dir' . DS . 'user_1'
        );
    }

    public function testFormFile()
    {
        $this->postChunk(
            '/upload/form-file/1',
            TMP_TESTDIR . DS . 'testunit-upload-dir/upload/user_1/1'
        );
    }

    public function testDelete()
    {
        $entity = TableRegistry::getTableLocator()->get('Fileuploads')->get(1);
        Filesystem::reset();
        Filesystem::dumpFile($entity->get('path'), 'test');
        $this->assertFileExists($entity->get('path'));
        $this->httpDelete('/upload/delete/1');
        $this->assertResponseCode(200);
        $this->assertFileDoesNotExist($entity->get('path'));
    }

    public function testArchivingSystemCert()
    {
        $this->genericSessionData['Auth']->set(
            'admin',
            [
                'tech' => true,
                'data' => [
                    'username' => 'testunit'
                ]
            ]
        );
        $this->session($this->genericSessionData);
        $this->postChunk(
            '/upload/archiving-system-cert',
            TMP_TESTDIR . '/ssl/archiving-system'
        );
    }

    public function testSedaGeneratorCert()
    {
        $this->genericSessionData['Auth']->set(
            'admin',
            [
                'tech' => true,
                'data' => [
                    'username' => 'testunit'
                ]
            ]
        );
        $this->session($this->genericSessionData);
        $this->postChunk(
            '/upload/seda-generator-cert',
            TMP_TESTDIR . '/ssl/seda-generator'
        );
    }

    public function testFormCert()
    {
        $this->postChunk(
            '/upload/form-cert',
            TMP_TESTDIR . '/ssl/seda-generator-2'
        );
    }

    private function postChunk(string $url, $path)
    {
        $chunkPath = $path . '/' . sha1('7215-logopng') . '_1';
        Filesystem::copy($_FILES['file']['tmp_name'], $chunkPath);
        $data = [
            'flowChunkNumber' => '1',
            'flowChunkSize' => '1048570',
            'flowCurrentChunkSize' => 7215,
            'flowTotalSize' => '7215',
            'flowIdentifier' => '7215-logopng',
            'flowFilename' => 'logo.png',
            'flowRelativePath' => 'logo.png',
            'flowTotalChunks' => '1'
        ];
        $this->saveBuffer();
        $this->httpPost($url, $data);
        $partialEmit = $this->restoreBuffer();
        $response = json_decode($partialEmit . ((string)$this->_response->getBody()), true);
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response['report']['id']));
    }
}
