<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use Adldap\Adldap;
use Adldap\Connections\Ldap;
use Adldap\Connections\Provider;
use Adldap\Models\Entry;
use Adldap\Query\Factory;
use Adldap\Query\Paginator;
use ArrayIterator;
use AsalaeCore\Factory\Utility;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use AsalaeCore\TestSuite\TestCase;
use ErrorException;
use PHPUnit\Exception;
use Throwable;

/**
 * Versae\Controller\LdapsController Test Case
 */
class LdapsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;

    public $fixtures = [
        'app.Acos',
        'app.Aros',
        'app.ArosAcos',
        'app.Configurations',
        'app.Counters',
        'app.CronExecutions',
        'app.Crons',
        'app.Filters',
        'app.KeywordLists',
        'app.Ldaps',
        'app.Notifications',
        'app.OrgEntities',
        'app.Roles',
        'app.RolesTypeEntities',
        'app.SavedFilters',
        'app.Sessions',
        'app.TypeEntities',
        'app.Users',
        'app.Versions',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->genericSessionData['Auth']->set(
            'admin',
            [
                'tech' => true,
                'data' => [
                    'username' => 'testunit'
                ]
            ]
        );
        $this->session($this->genericSessionData);
    }

    public function tearDown(): void
    {
        Utility::reset();
        parent::tearDown();
    }

    /**
     * Test index method
     * @throws Exception
     * @throws ErrorException
     * @throws Throwable
     */
    public function testIndex()
    {
        $this->httpGet('/ldaps/index');
        $this->assertResponseCode(200);
    }

    /**
     * Test add method
     * @throws Exception
     * @throws ErrorException
     * @throws Throwable
     */
    public function testAdd()
    {
        $this->httpGet('/ldaps/add');
        $this->assertResponseCode(200);

        $data = [
            'name' => 'LDAP de test2',
            'description' => 'Pour tester la connexion LDAP',
            'host' => '192.168.2.139',
            'port' => '389',
            'use_proxy' => '1',
            'use_ssl' => '0',
            'use_tls' => '0',
            'account_prefix' => '',
            'account_suffix' => '@adullact.win',
            'user_query_login' => 'test@adullact.win',
            'user_query_password' => 'test',
            'ldap_root_search' => 'dc=adullact,dc=win',
            'ldap_users_filter' => '(memberOf=cn=asalae,OU=Groupes,dc=adullact,dc=win)',
            'user_login_attribute' => 'sAMAccountName',
            'user_username_attribute' => 'sAMAccountName',
            'user_name_attribute' => 'displayname',
            'user_mail_attribute' => 'mail'
        ];
        $this->httpPost('/ldaps/add', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * Test edit method
     */
    public function testEdit()
    {
        $this->httpGet('/ldaps/edit/1');
        $this->assertResponseCode(200);

        $data = [
            'name' => 'LDAP de test',
            'description' => 'Pour tester la connexion LDAP',
            'host' => '192.168.2.139',
            'port' => '389',
            'use_proxy' => '1',
            'use_ssl' => '0',
            'use_tls' => '0',
            'account_prefix' => '',
            'account_suffix' => '@adullact.win',
            'user_query_login' => 'test@adullact.win',
            'user_query_password' => 'test',
            'ldap_root_search' => 'dc=adullact,dc=win',
            'ldap_users_filter' => '(memberOf=cn=asalae,OU=Groupes,dc=adullact,dc=win)',
            'user_login_attribute' => 'sAMAccountName',
            'user_username_attribute' => 'sAMAccountName',
            'user_name_attribute' => 'displayname',
            'user_mail_attribute' => 'mail',
        ];
        $this->httpPut('/ldaps/edit/1', $data);
        $this->assertResponseCode(200);
        $this->assertHeader('X-Asalae-Success', 'true');
    }

    /**
     * Test delete method
     */
    public function testDelete()
    {
        $Users = TableRegistry::getTableLocator()->get('Users');
        $Users->updateAll(['ldap_id' => null], []);
        $this->httpDelete('/ldaps/delete/1');
        $this->assertResponseCode(200);
        $this->assertResponseContains('done');
    }

    /**
     * Test ping method
     */
    public function testPing()
    {
        $this->mockPaginator();
        $this->httpPost('/ldaps/ping', ['host' => '127.0.0.1', 'port' => 389]);
        $this->assertResponseCode(200);
        $this->assertResponseContains('pong');
    }

    private function mockPaginator()
    {
        $ldap = $this->createMock(Ldap::class);
        $ldap->method('search')->willReturn('');
        $ldap->method('countEntries')->willReturn(1);

        $entry = $this->createMock(Entry::class);
        $entry->method('getAttributes')->will(
            $this->returnCallback(
                function () {
                    return [
                        'cn' => ['foo'],
                        'mail' => ['foo@bar.baz'],
                    ];
                }
            )
        );
        $entry->method('getAttribute')->will(
            $this->returnCallback(
                function ($attr) {
                    $return = [
                        'sAMAccountName' => 'testunit',
                        'displayname' => 'test',
                        'mail' => 'test@testunit.org',
                    ];
                    return isset($return[$attr]) ? [$return[$attr]] : [];
                }
            )
        );
        $paginator = $this->createMock(Paginator::class);
        $paginator->method('count')->willReturn(1);
        $paginator->method('getIterator')->willReturn(new ArrayIterator([$entry]));
        $search = $this->createMock(Factory::class);
        $search->method('__call')
            ->will(
                $this->returnCallback(
                    function ($name) use ($entry, $ldap, $paginator) {
                        switch ($name) {
                            case 'paginate':
                                return $paginator;
                            case 'getConnection':
                                return $ldap;
                            case 'getDn':
                            case 'getQuery':
                            case 'getSelects':
                            case 'rawFilter':
                            case 'where':
                                return [];
                        }
                    }
                )
            ); // getDn(), getQuery(), getSelects() et surtout paginate()
        $search->method('newQuery')->willReturnSelf();

        $provider = $this->createMock(Provider::class);
        $provider->method('search')->willReturn($search);

        $ad = $this->createMock(Adldap::class);
        $ad->method('addProvider')->willReturnSelf();
        $ad->method('connect')->willReturn($provider);
        Utility::set(Adldap::class, $ad);
    }

    /**
     * Test getRandomEntry method
     * @throws Exception
     * @throws ErrorException
     * @throws Throwable
     */
    public function testGetRandomEntry()
    {
        $this->mockPaginator();
        $this->httpPost('/ldaps/get-random-entry', ['host' => '127.0.0.1', 'port' => 389]);
        $this->assertResponseCode(200);
        $this->assertResponseContains('foo@bar.baz');
    }

    /**
     * Test getCount method
     */
    public function testGetCount()
    {
        $this->mockPaginator();
        $this->httpPost('/ldaps/get-count', ['host' => '127.0.0.1', 'port' => 389]);
        $this->assertResponseCode(200);
        $this->assertEquals('1', (string)$this->_response->getBody());
    }

    /**
     * Test importUsers method
     */
    public function testImportUsers()
    {
        $this->mockPaginator();
        $this->get('/ldaps/import-users/1?filter[foo]=bar');
        $this->assertResponseCode(200);
        $this->assertResponseContains('test@testunit.org');

        $data = [
            'form' => [
                'entity' => 2,
                'role' => 1,
            ],
            'selected' => [
                [
                    'conf-login' => 'foo',
                    'conf-username' => 'foo',
                    'conf-name' => 'bar',
                    'conf-email' => 'foo@bar.baz',
                ]
            ]
        ];
        $Users = TableRegistry::getTableLocator()->get('Users');
        $Users->deleteAll(['ldap_id' => 1]);
        $this->httpPost('/ldaps/import-users/1', $data);
        $this->assertResponseCode(200);
        $user = $Users->find()->where(['ldap_id' => 1])->first();
        $this->assertNotFalse((bool)$user);
        $this->assertEquals('foo', $user->get('username'));
        $this->assertEquals('bar', $user->get('name'));
        $this->assertEquals('foo@bar.baz', $user->get('email'));

        // echec de l'import: un utilisateur de meme nom existe sur un autre SA
        $Users->updateAll(['org_entity_id' => 5], ['ldap_id' => 1]);
        $this->httpPost('/ldaps/import-users/1', $data);
        $this->assertResponseCode(200);
        $this->assertResponseContains(
            json_encode(
                __dn(
                    'ldap',
                    "USER_CONFLICT_DIFFERENT_ARCHIVAL_AGENCY_SINGULAR",
                    "USER_CONFLICT_DIFFERENT_ARCHIVAL_AGENCY_PLURAL",
                    1,
                    "foo (Service d'archives 2)"
                )
            )
        );

        // echec de l'import: un utilisateur importé de meme nom existe sur un autre LDAP
        $Users->updateAll(['org_entity_id' => 2, 'ldap_id' => 2], ['ldap_id' => 1]);
        $this->httpPost('/ldaps/import-users/1', $data);
        $this->assertResponseCode(200);
        $this->assertResponseContains(
            json_encode(
                __dn(
                    'ldap',
                    "USER_CONFLICT_DIFFERENT_LDAP_SINGULAR",
                    "USER_CONFLICT_DIFFERENT_LDAP_PLURAL",
                    1,
                    "foo (LDAP de test 2)"
                )
            )
        );

        // echec de l'import: un utilisateur non LDAP de meme nom existe (liaison possible)
        $Users->updateAll(['ldap_id' => null], ['id' => $user->id]);
        $this->httpPost('/ldaps/import-users/1', $data);
        $this->assertResponseCode(200);
        $this->assertResponseContains(
            json_encode(
                __dn(
                    'ldap',
                    "USER_CONFLICT_SAME_ARCHIVAL_AGENCY_SINGULAR",
                    "USER_CONFLICT_SAME_ARCHIVAL_AGENCY_PLURAL",
                    1,
                    "foo (bar)"
                )
            )
        );

        // bind de l'utilisateur au ldap
        $user = $Users->get($user->id);
        $this->assertNull($user->get('ldap_id'));
        $data['bind_users_to_ldap'] = true;
        $this->httpPost('/ldaps/import-users/1', $data);
        $user = $Users->get($user->id);
        $this->assertNotNull($user->get('ldap_id'));
    }

    /**
     * Test getFiltered method
     */
    public function testGetFiltered()
    {
        $this->mockPaginator();
        $data = [
            'user_login_attribute' => 'sAMAccountName',
            'user_username_attribute' => 'sAMAccountName',
            'user_name_attribute' => 'displayname',
            'user_mail_attribute' => 'mail',
        ];
        $this->httpPost('/ldaps/get-filtered', $data);
        $this->assertResponseCode(200);
        $this->assertResponseContains('test@testunit.org');
    }
}
