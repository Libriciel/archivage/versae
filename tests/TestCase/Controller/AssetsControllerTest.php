<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\TestSuite\IntegrationTestTrait;

class AssetsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;

    public $fixtures = [
        'app.Acos',
        'app.Configurations',
        'app.Ldaps',
        'app.OrgEntities',
        'app.Roles',
        'app.TypeEntities',
        'app.Users',
    ];

    const TEST_IMG = TMP . 'test-assets.png';
    const TEST_IMG2 = TMP . 'test-assets2.png';

    public function setUp(): void
    {
        parent::setUp();
        $handle = imagecreatetruecolor(50, 50);
        $white = imagecolorallocate($handle, 255, 255, 255);
        imagefilledrectangle($handle, 0, 0, 49, 49, $white);
        imagepng($handle, self::TEST_IMG);
        imagedestroy($handle);
        $handle = imagecreatetruecolor(50, 50);
        $red = imagecolorallocate($handle, 255, 0, 0);
        imagefilledrectangle($handle, 0, 0, 49, 49, $red);
        imagepng($handle, self::TEST_IMG2);
        imagedestroy($handle);
        Configure::write(
            'Assets',
            [
                'login' => [
                    'logo-client' => self::TEST_IMG2,
                ],
                'global-background' => self::TEST_IMG,
                'global-background-position' => '50px 30px',
            ]
        );
        Configure::write('App.fullBaseUrl', false);
    }

    public function tearDown(): void
    {
        if (is_file(self::TEST_IMG)) {
            unlink(self::TEST_IMG);
        }
        if (is_file(self::TEST_IMG2)) {
            unlink(self::TEST_IMG2);
        }
        parent::tearDown();
    }

    public function testConfiguredBackground()
    {
        $this->httpGet('/assets/configuredBackground');
        $this->assertResponseCode(200);
        $body = $this->_response->getBody();
        $body->rewind();
        $response = md5($body->getContents());
        $this->assertHeader('Content-type', 'image/png');
        $this->assertEquals('858cb21a4087e839e21ae3c6c26bfa48', $response);
    }

    public function testConfiguredBackground404()
    {
        Configure::delete('Assets.global-background');
        $this->get('/assets/configuredBackground');
        $this->assertResponseCode(404);
    }

    public function testConfiguredLogoClient()
    {
        $this->httpGet('/assets/configuredLogoClient');
        $this->assertResponseCode(200);
        $response = md5($this->_response->getBody());
        $this->assertHeader('Content-type', 'image/png');
        $this->assertEquals('d9534b6771aede98dc7bdbe2cfc0ea73', $response);
    }

    public function testConfiguredLogoClient404()
    {
        Configure::delete('Assets.login.logo-client');
        $this->get('/assets/configuredLogoClient');
        $this->assertResponseCode(404);
    }

    public function testCss()
    {
        $this->httpGet('/assets/css');
        $this->assertResponseCode(200);
        $body = $this->_response->getBody();
        $body->rewind();
        $response = md5($body->getContents());
        $this->assertHeader('Content-type', 'text/css');
        $this->assertEquals('d85337344c0e40d664d2ab5d8ec0ba30', $response);
    }
}
