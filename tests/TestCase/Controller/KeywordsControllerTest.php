<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;

class KeywordsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;

    const TEST_SKOS = ASALAE_CORE_TEST_DATA . DS . 'continents-skos.rdf';
    const TEST_CSV = ASALAE_CORE_TEST_DATA . DS . 'mots_clefs.csv';

    public $fixtures = [
        'app.Acos',
        'app.Aros',
        'app.ArosAcos',
        'app.Configurations',
        'app.Fileuploads',
        'app.Filters',
        'app.KeywordLists',
        'app.Keywords',
        'app.Ldaps',
        'app.Notifications',
        'app.OrgEntities',
        'app.Roles',
        'app.SavedFilters',
        'app.Sessions',
        'app.TypeEntities',
        'app.Users',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        Configure::write('App.defaultLocale', 'fr_FR');
    }

    public function testIndex()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/keywords/index/1');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response['data']));

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/keywords/index/1?SaveFilterSelect=&name[0]=%3Fode*&code[0]=%3Fode*&sort=name&direction=asc');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response['data']));

        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/keywords/index/1?SaveFilterSelect=&favoris[0]=0&favoris[0]=1&sort=favorite&direction=desc');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(empty($response['data']));
    }

    public function testAdd()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/keywords/add/1');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response['entity']));

        $data = [
            'code' => 'test',
            'name' => 'test'
        ];
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPost('/keywords/add/1', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response['id']));

        $Keywords = TableRegistry::getTableLocator()->get('Keywords');
        $key = $Keywords->find()->where(['code' => 'test'])->first();
        $this->assertTrue((bool)$key);
        $this->assertEquals($response['id'], $key->get('id'));
    }

    public function testAddMultiple()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/keywords/addMultiple/1');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response['entities']));

        $data = [
            0 => [
                'code' => 'test1',
                'name' => 'test1'
            ],
            1 => [
                'code' => 'test2',
                'name' => 'test2'
            ],
        ];
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPost('/keywords/addMultiple/1', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response[0]));
        $this->assertTrue(!empty($response[0]['id']));
        $this->assertTrue(!empty($response[1]));
        $this->assertTrue(!empty($response[1]['id']));

        $Keywords = TableRegistry::getTableLocator()->get('Keywords');
        $key = $Keywords->find()->where(['code' => 'test1'])->first();
        $this->assertTrue((bool)$key);
        $this->assertEquals($response[0]['id'], $key->get('id'));
        $key = $Keywords->find()->where(['code' => 'test2'])->first();
        $this->assertTrue((bool)$key);
        $this->assertEquals($response[1]['id'], $key->get('id'));
    }

    public function testEdit()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpGet('/keywords/edit/1');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response['entity']));

        $data = [
            'code' => 'test',
            'name' => 'test'
        ];
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpPut('/keywords/edit/1', $data);
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response['code']));
        $this->assertEquals('test', $response['code']);

        $Keywords = TableRegistry::getTableLocator()->get('Keywords');
        $key = $Keywords->find()->where(['code' => 'test'])->first();
        $this->assertTrue((bool)$key);
    }

    public function testDelete()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->httpDelete('/keywords/delete/1');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response['report']));
        $this->assertEquals('done', $response['report']);

        $Keywords = TableRegistry::getTableLocator()->get('Keywords');
        $key = $Keywords->newEntity(
            [
                'keyword_list_id' => 1,
                'code' => 'test',
                'name' => 'test',
                'version' => 1,
            ]
        );
        $this->assertNotFalse($Keywords->save($key));
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->delete('/keywords/delete/' . $key->get('id'));
        $this->assertResponseCode(404);
    }

    public function testView()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $response = $this->httpGet('/keywords/view/1', true);
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response['entity']));
    }

    private function appendSkos(): EntityInterface
    {
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $file = $Fileuploads->newEntity(
            [
                'name' => basename(self::TEST_SKOS),
                'path' => self::TEST_SKOS,
                'locked' => 1
            ]
        );
        $this->assertNotFalse($Fileuploads->save($file, ['silent' => true]));
        return $file;
    }

    public function testImport()
    {
        $Keywords = TableRegistry::getTableLocator()->get('Keywords');
        $Keywords->deleteAll([]);
        $file = $this->appendSkos();

        $this->httpGet('/keywords/import/1');
        $this->assertResponseCode(200);

        $data = [
            'fileupload_id' => $file->get('id'),
        ];
        $this->httpPost('/keywords/import/1', $data);
        $this->assertResponseCode(200);
        $this->assertEquals(1, $Keywords->find()->where(['name' => 'Europe'])->count());

        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $file = $Fileuploads->newEntity(
            [
                'name' => basename(self::TEST_CSV),
                'path' => self::TEST_CSV,
                'locked' => 1
            ]
        );
        $this->assertNotFalse($Fileuploads->save($file, ['silent' => true]));

        $data = [
            'fileupload_id' => $file->get('id'),
            'Csv' => [
                'separator' => ';',
                'delimiter' => '"',
                'way' => 'row',
                'pos_label_col' => '2',
                'pos_label_row' => '1',
                'auto_code' => '0',
                'pos_code_col' => '1',
                'pos_code_row' => '1'
            ]
        ];
        $this->httpPost('/keywords/import/1', $data);
        $this->assertResponseCode(200);
        $this->assertEquals(1, $Keywords->find()->where(['name' => 'Chambéry'])->count());
    }

    public function testGetImportFileInfo()
    {
        $file = $this->appendSkos();
        $this->httpPost('/keywords/get-import-file-info/1/' . $file->get('id'), []);
        $this->assertResponseCode(200);

        // injection d'un futur doublon
        $Keywords = TableRegistry::getTableLocator()->get('Keywords');
        $keyword = $Keywords->newEntity(
            [
                'keyword_list_id' => 1,
                'version' => 0,
                'code' => 'europe',
                'name' => 'Europe',
            ]
        );
        $this->assertNotFalse($Keywords->save($keyword));

        $file = $this->appendSkos();
        $this->httpPost(
            '/keywords/get-import-file-info/1/' . $file->get('id'),
            [
                'conflicts' => 'overwrite',
            ]
        );
        $this->assertResponseCode(200);

        $file = $this->appendSkos();
        $this->httpPost(
            '/keywords/get-import-file-info/1/' . $file->get('id'),
            [
                'conflicts' => 'ignore',
            ]
        );
        $this->assertResponseCode(200);

        $file = $this->appendSkos();
        $this->httpPost(
            '/keywords/get-import-file-info/1/' . $file->get('id'),
            [
                'conflicts' => 'rename',
            ]
        );
        $this->assertResponseCode(200);
    }

    public function testPopulateSelect()
    {
        $data = [
            'term' => 'test',
        ];
        $response = $this->httpPost('/keywords/populate-select', $data);
        $this->assertResponseCode(200);
        $this->assertTrue(!empty($response->results));

        $data = [
            'term' => 'test',
            'page' => 2
        ];
        $this->httpPost('/keywords/populate-select', $data);
        $this->assertResponseCode(200);
    }

    public function testGetData()
    {
        $response = $this->httpGet('/keywords/get-data/1');
        $this->assertResponseCode(200);
        $this->assertTrue(isset($response->version));
    }
}
