<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\TestSuite\IntegrationTestTrait;

/**
 * Versae\Controller\HomeController Test Case
 *
 * @uses \Versae\Controller\DownloadController
 */
class HomeControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_AUTH,
        'app.Forms',
        'app.Transfers',
        'app.Deposits',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
    }

    public function testindex()
    {
        $this->get('/');
        $this->assertResponseOk();
    }
}
