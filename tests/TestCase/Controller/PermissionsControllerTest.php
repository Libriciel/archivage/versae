<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Controller;

use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Hash;
use Libriciel\Filesystem\Utility\Filesystem;

class PermissionsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;
    use AutoFixturesTrait;

    private $debugFixtures = false;
    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        FIXTURES_APP_BEFORE_FILTER,
        FIXTURES_AUTH,
    ];

    public $originalControllersRules;

    public function setUp(): void
    {
        parent::setUp();
        $this->session($this->genericSessionData);
        Configure::write('Roles.global_is_editable', true);
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        $rulesPath = TMP_TESTDIR . DS . uniqid('controllers-') . '.json';
        $this->originalControllersRules = Configure::read('App.paths.controllers_rules');
        Filesystem::copy($this->originalControllersRules, $rulesPath);
        Configure::write('App.paths.controllers_rules', $rulesPath);
        $ctls = json_decode(file_get_contents($rulesPath), true);
        // forte diminution du temps d'execution des tests
        file_put_contents(
            $rulesPath,
            json_encode(
                [
                    'OrgEntities' => $ctls['OrgEntities'],
                    'Roles' => $ctls['Roles'],
                    'Permissions' => $ctls['Permissions'],
                    'Ldaps' => $ctls['Ldaps'],
                ]
            )
        );
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()
            ->where(['alias' => 'admin'])
            ->first();
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $aco = $Acos->find()
            ->where(['model' => 'controllers', 'alias' => 'Roles'])
            ->first();
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $perm = $ArosAcos->newEntity(
            [
                'aro_id' => $aro->id,
                'aco_id' => $aco->id, // controllers/Roles (commeDroits)
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $ArosAcos->saveOrFail($perm);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Configure::write('App.paths.controllers_rules', $this->originalControllersRules);
    }

    public function testIndex()
    {
        $this->httpGet('/Permissions/index');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response['aros']);
    }

    public function testAjaxGetPermission()
    {
        $this->configRequest(
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'X-Requested-With' => 'XMLHttpRequest'
                ]
            ]
        );
        $this->get('/Permissions/ajaxGetPermission/1/OrgEntities/index');
        $response = json_decode((string)$this->_response->getBody(), true);
        $this->assertResponseCode(200);
        $this->assertEquals('1', $response['access']);
        $this->assertFalse($response['accessParent']); // heritage
    }

    public function testEdit()
    {
        $response = $this->httpGet('/Permissions/edit/1');
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response->entity);

        $data = Hash::expand(
            [
                'api.Forms.create' => '1',
                'api.Forms.read' => '0',
                'controllers.OrgEntities.action.index' => '1',
                'controllers.OrgEntities.action.add' => '-1',
            ]
        );
        $response = $this->httpPost('/Permissions/edit/1', $data);
        $this->assertResponseCode(200);
        $this->assertNotEmpty($response->entity);

        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $controllers = $Acos->find()
            ->where(['alias' => 'controllers', 'model' => 'root'])
            ->firstOrFail();
        $aco = $Acos->find()
            ->where(
                [
                    'alias' => 'add',
                    'model' => 'OrgEntities',
                    'lft >=' => $controllers->get('lft'),
                    'rght <=' => $controllers->get('rght'),
                ]
            )
            ->first();
        $this->assertTrue((bool)$aco);
        $perm = $ArosAcos->find()->where(
            [
                'aro_id' => 1,
                'aco_id' => $aco->get('id'),
            ]
        )->first();
        $this->assertEquals('-1', $perm->get('_create'));

        $aco = $Acos->find()
            ->where(
                [
                    'alias' => 'index',
                    'model' => 'OrgEntities',
                    'lft >=' => $controllers->get('lft'),
                    'rght <=' => $controllers->get('rght'),
                ]
            )
            ->first();
        $this->assertTrue((bool)$aco);
        $perm = $ArosAcos->find()->where(
            [
                'aro_id' => 1,
                'aco_id' => $aco->get('id'),
            ]
        )->first();
        $this->assertTrue((bool)$perm);
        $this->assertEquals('1', $perm->get('_read'));
    }
}
