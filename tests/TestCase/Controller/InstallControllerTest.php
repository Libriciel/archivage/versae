<?php

/** @noinspection SqlDialectInspection PhpCSValidationInspection */
namespace Versae\Test\TestCase\Controller;

use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\CascadeDelete;
use AsalaeCore\Utility\Config;
use AsalaeCore\Utility\Exec;
use Cake\Core\Configure;
use Cake\Database\Connection;
use Cake\Database\Driver\Sqlite;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Mailer;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Utility\Hash;
use Libriciel\Filesystem\Utility\Filesystem;
use PDOException;

/**
 * Versae\Controller\InstallController Test Case
 */
class InstallControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use HttpTrait;

    public $fixtures = [
        'app.Roles',
        'app.TypeEntities',
        'app.OrgEntities',
    ];

    private $originalPathToLocal;

    public function setUp(): void
    {
        parent::setUp();
        $dir = sys_get_temp_dir() . DS . 'testinstall' . getenv('TEST_TOKEN');
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
        mkdir($dir);
        Configure::write('App.paths.data', $dir);
        Filesystem::reset();
        $email = $this->getMockBuilder(Mailer::class)
            ->onlyMethods(['send'])
            ->getMock();
        $email->method('send')->willReturn(['email content']);
        Utility::set(Mailer::class, $email);
        $this->originalPathToLocal = Configure::read('App.paths.path_to_local_config');
        $Exec = $this->createMock(Exec::class);
        $Exec->method('async')->willReturnSelf();
        Utility::set('Exec', $Exec);
        Utility::set(Exec::class, $Exec);
        Config::reset();
        Configure::write('debug_mails', false);
    }

    public function tearDown(): void
    {
        $dir = sys_get_temp_dir() . DS . 'testinstall' . getenv('TEST_TOKEN');
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
        Filesystem::setNamespace();
        Filesystem::reset();
        Configure::write('App.paths.path_to_local_config', $this->originalPathToLocal);
        Utility::reset();
        Config::reset();
        parent::tearDown();
    }

    public function testIndex()
    {
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        CascadeDelete::all($OrgEntities);

        // cas début d'installation (1er get)
        Configure::write(
            'App.paths.path_to_local_config',
            sys_get_temp_dir() . DS . 'testinstall' . getenv('TEST_TOKEN') . DS . 'path_to_local.php'
        );
        $this->get('/install/index');
        $this->assertResponseCode(200);

        // 1er post (initialisation de la conf)
        $file = sys_get_temp_dir() . DS . 'testinstall' . getenv('TEST_TOKEN') . DS . 'app_local.json';
        $data = [
            'local_config_file' => $file,
            'Config__App__paths__data' => sys_get_temp_dir() . DS . 'testinstall' . getenv('TEST_TOKEN') ,
        ];
        if (is_file($file)) {
            unlink($file);
        }
        $this->post('/install/index', $data);
        $this->assertResponseCode(200);
        $this->assertFileExists($file);

        // 2e post (config de base)
        $data = [
            'Config__App__defaultLocale' => 'fr_FR',
            'Config__App__timezone' => 'Europe/Paris',
            'Config__App__fullBaseUrl' => 'https://www.libriciel.fr',
            'ignore_invalid_fullbaseurl' => false,
        ];
        $this->assertStringNotContainsString('https:\/\/www.libriciel.fr', file_get_contents($file));
        $this->post('/install/index', $data);
        $this->assertResponseCode(200);
        $this->assertStringContainsString('https:\/\/www.libriciel.fr', file_get_contents($file));

        // 3e post (ratchet)
        $data = [
            'Config__Ratchet__connect' => 'wss://libriciel.fr/wss',
        ];
        $this->post('/install/index', $data);
        $this->assertResponseCode(200);

        // 4e post (datasource)
        $data = [
            'Config__Datasources__default__driver' => Sqlite::class,
            'Config__Datasources__default__host' => 'localhost',
            'Config__Datasources__default__username' => 'fakeuser',
            'Config__Datasources__default__password' => 'fakepassword',
            'confirm_database_password' => 'fakepassword',
            'Config__Datasources__default__database'
                => sys_get_temp_dir() . DS . 'testinstall' . getenv('TEST_TOKEN') . DS . 'database.sqlite',
        ];
        $this->assertStringNotContainsString('database.sqlite', file_get_contents($file));
        $this->post('/install/index', $data);
        $this->assertResponseCode(200);
        $this->assertStringContainsString('database.sqlite', file_get_contents($file));

        // 5e post (Security.salt)
        $data = [
            'security_salt_method' => 'hash',
            'security_salt_method_hash' => 'testunit',
        ];
        $hash = 'e4690848b6417abf1978ccf9190c7cbf44dd66e8805618dfbe18f14915400cf3';
        $this->assertStringNotContainsString($hash, file_get_contents($file));
        $this->post('/install/index', $data);
        $this->assertResponseCode(200);
        $this->assertStringContainsString($hash, file_get_contents($file));

        // 6e post email
        $data = [
            'Config__Email__default__from' => 'no-reply@versae.fr',
            'Config__EmailTransport__default__className' => 'Mail',
            'Config__EmailTransport__default__host' => 'localhost',
            'Config__EmailTransport__default__port' => 25,
            'Config__EmailTransport__default__username' => 'username',
            'Config__EmailTransport__default__password' => 'password',
        ];
        $this->assertStringNotContainsString('no-reply@versae.fr', file_get_contents($file));
        $this->post('/install/index', $data);
        $this->assertResponseCode(200);
        $this->assertStringContainsString('no-reply@versae.fr', file_get_contents($file));

        // patch database (migration migrate) (note: datasource crée lors du 4e post)
        /** @var Connection $conn */
        $conn = ConnectionManager::get('validate_datasource');
        try {
            $conn->query('select id from users limit 1');
            $success = true;
        } catch (PDOException $e) {
            $success = false;
        }
        $this->assertFalse($success);

        $Exec = $this->createMock(Exec::class);
        $Exec->method('rawCommand')->will(
            $this->returnCallback(
                function ($command, array &$output = null, &$return_var = null) {
                    $output = [];
                    $return_var = 0;
                    return '';
                }
            )
        );
        Utility::set('Exec', $Exec);
        $data = [
            'initializeDatabase' => true
        ];
        $this->httpPatch('/install/index', $data);
        Filesystem::rollback();
        $this->assertResponseCode(200);

        // 7e post (administrateur technique)
        $data = [
            'Admins__username' => 'testunit',
            'Admins__email' => 'testunit@libriciel.fr',
            'Admins__password' => 'testunit',
            'Admins__confirm-password' => 'testunit',
        ];
        $admins = sys_get_temp_dir() . DS . 'testinstall' . getenv('TEST_TOKEN')  . DS . 'administrateurs.json';
        Configure::write('App.paths.administrators_json', $admins);
        $this->assertFileDoesNotExist($admins);
        $this->post('/install/index', $data);
        $this->assertResponseCode(200);
        $this->assertFileExists($admins);

        // 9e post
        $data = [
            'ServiceExploitation__name' => 'SE-Testunit',
            'ServiceExploitation__identifier' => 'se-Testunit',
        ];
        $this->assertEquals(0, $OrgEntities->find()->count());
        $this->post('/install/index', $data);
        $this->assertRedirect('/admins');
        $this->assertEquals(1, $OrgEntities->find()->count());
    }

    public function testSendTestMail()
    {
        TableRegistry::getTableLocator()->get('OrgEntities')->deleteAll([]);
        $data = [
            'Config__EmailTransport__default__className' => 'Mail',
            'Config__EmailTransport__default__host' => 'localhost',
            'Config__EmailTransport__default__port' => '25',
            'Config__EmailTransport__default__username' => 'test',
            'Config__EmailTransport__default__password' => 'test',
            'email' => 'test@test.fr',
            'Config__Email__default__from' => 'test@test.fr',
        ];
        $response = $this->httpPost('/install/send-test-mail', $data, true);
        $this->assertResponseCode(200);
        $this->assertTrue(Hash::get($response, 'success'));
    }
}
