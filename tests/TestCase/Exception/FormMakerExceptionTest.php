<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Exception;

use AsalaeCore\TestSuite\TestCase;
use Versae\Exception\FormMakerException;

class FormMakerExceptionTest extends TestCase
{
    public function testMe()
    {
        $e = FormMakerException::create('myfield', 'test');
        $this->assertEquals('myfield', $e->field);
        $this->assertEquals('test', $e->getMessage());
    }
}
