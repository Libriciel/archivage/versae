<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Form;

use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\Utility\CascadeDelete;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Object\CommandResult;
use Cake\Core\Configure;
use AsalaeCore\TestSuite\TestCase;
use Cake\Database\Expression\QueryExpression;
use Cake\Datasource\ConnectionManager as CakeConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Libriciel\Filesystem\Utility\Filesystem;
use Versae\Form\InstallConfigForm;
use Versae\Test\Mock\ConnectionManager;

class InstallConfigFormTest extends TestCase
{
    use AutoFixturesTrait;

    public $appAutoFixtures = [
    ];
    public $appAppendFixtures = [
        'app.Acos',
        'app.ArchivingSystems',
        'app.Aros',
        'app.Roles',
        'app.RolesTypeEntities',
        'app.TypeEntities',
        'app.Counters',
        'app.OrgEntities',
        'app.Users',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->originalPathToLocal = Configure::read('App.paths.path_to_local_config');
        $dir = TMP_TESTDIR . DS . 'testinstallconfig';
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Configure::write('App.defaultLocale', 'fr_FR');
        Configure::write('App.paths.path_to_local_config', $dir . DS . 'path_to_local.php');
        Filesystem::reset();
        Filesystem::dumpFile($dir . DS . 'path_to_local.php', "<?php return '$dir/app_local.json';?>");
        Filesystem::dumpFile($dir . DS . 'app_local.json', "{}");
        Utility::set(CakeConnectionManager::class, new ConnectionManager());
        ConnectionManager::$outputs = [
            'drop' => true,
            'setConfig' => true,
        ];
        $exec = $this->getMockBuilder(Exec::class)
            ->onlyMethods(['command', 'rawCommand', 'async'])
            ->getMock();
        $exec->method('async');
        $exec->method('command')->willReturn(
            new CommandResult(['success' => true, 'code' => 0, 'stdout' => 'OK', 'stderr' => ''])
        );
        $exec->method('rawCommand')->will(
            $this->returnCallback(
                function ($command, array &$output = null, &$return_var = null) {
                    $output = [];
                    $return_var = 0;
                    return '';
                }
            )
        );
        Utility::set('Exec', $exec);
    }

    public function tearDown(): void
    {
        Configure::write('App.paths.path_to_local_config', $this->originalPathToLocal);
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Utility::reset();
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        CascadeDelete::all($OrgEntities); // tearDown n'arrive pas à delete une ref circulaire
        parent::tearDown();
    }

    public function testOut()
    {
        $form = new InstallConfigForm();
        $out = '';
        $form->stdOut = function ($v) use (&$out) {
            $out .= $v;
        };
        $form->out('test');
        $this->assertEquals('test', $out);
    }

    public function testRuleIsWritable()
    {
        $form = new InstallConfigForm();
        $rule = $form->ruleIsWritable();
        $this->assertTrue($rule['rule'](TMP_TESTDIR . DS . 'testnotexists'));
        file_put_contents(TMP_TESTDIR . DS . 'testexists', 'foo');
        $this->assertTrue($rule['rule'](TMP_TESTDIR . DS . 'testexists'));
    }

    public function testExecute()
    {
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        CascadeDelete::all($OrgEntities);
        $OrgEntities->updateAll(['type_entity_id' => null], []);
        $OrgEntities->query()
            ->update()
            ->set(['identifier' => new QueryExpression("identifier  || '_test'")])
            ->execute();

        $Users = TableRegistry::getTableLocator()->get('Users');
        $Users->query()
            ->update()
            ->set(['username' => new QueryExpression("username  || '_test'")])
            ->execute();

        $form = new InstallConfigForm();
        $out = '';
        $form->stdOut = function ($v) use (&$out) {
            $out .= $v;
        };
        $this->assertFalse($form->execute([]));
        $dir = TMP_TESTDIR . DS . 'testinstallconfig';
        $config = parse_ini_file(CONFIG . 'install_default.ini', true);
        $config = Hash::merge(
            $config,
            [
                'config' => [
                    'config_file' => $dir . DS . 'app_local.json',
                    'data_dir' => $dir,
                    'admin_file' => $dir . DS . 'administrators.json',
                ]
            ]
        );
        $result = $form->execute(Hash::flatten($config, '__'));
        Configure::write('debug', true);
        $this->assertTrue($result);
        $this->assertCount(3, $OrgEntities->find());
    }
}
