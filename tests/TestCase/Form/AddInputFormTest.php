<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Form;

use AsalaeCore\TestSuite\TestCase;
use Versae\Form\AddInputForm;

class AddInputFormTest extends TestCase
{
    public $fixtures = [];

    public function testSchema()
    {
        $form = new AddInputForm();
        $fields = $form->getSchema()->fields();
        $this->assertContains('type', $fields);
    }

    public function testExecute()
    {
        $form = new AddInputForm();

        $result = $form->execute(
            [
                'type' => null,
            ]
        );
        $this->assertFalse($result);

        $result = $form->execute(
            [
                'type' => 'text',
                'label' => null,
            ]
        );
        $this->assertFalse($result);
    }
}
