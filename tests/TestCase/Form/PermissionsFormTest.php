<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Form;

use Acl\Controller\Component\AclComponent;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use AsalaeCore\TestSuite\TestCase;
use Versae\Form\PermissionsForm;

class PermissionsFormTest extends TestCase
{
    public $fixtures = [
        'app.Acos',
        'app.Aros',
        'app.ArosAcos',
        'app.Users',
    ];

    public function testSchema()
    {
        $form = new PermissionsForm();
        $fields = $form->getSchema()->fields();
        $this->assertContains('controllers.Permissions.action.edit', $fields);
    }

    public function testList()
    {
        $registry = new ComponentRegistry();
        $Acl = new AclComponent($registry);
        $form = new PermissionsForm();
        $list = $form->list(1);
        $this->assertArrayHasKey('controllers.Roles.action.edit', $list);
        $this->assertEquals('1', $list['controllers.Roles.action.edit']);
        $Acl->deny(1, 'controllers/Roles/edit');
        $list = $form->list(1);
        $this->assertEquals('-1', $list['controllers.Roles.action.edit']);
        $Acl->allow(1, 'controllers/Roles/edit');
        $list = $form->list(1);
        $this->assertEquals('1', $list['controllers.Roles.action.edit']);
        $Acl->allow(1, 'controllers/Roles', 'read');
        $list = $form->list(1);
        $this->assertArrayHasKey('controllers.Roles.read', $list);
        $this->assertEquals('1', $list['controllers.Roles.read']);
    }

    public function testExecute()
    {
        $registry = new ComponentRegistry();
        $Acl = new AclComponent($registry);
        $form = new PermissionsForm();
        $aro = TableRegistry::getTableLocator()->get('Aros')->get(1);

        // test validation
        $result = $form->execute(
            [
                'aro' => $aro,
                'controllers' => ['Roles' => ['action' => ['edit' => 'bad_value']]],
            ]
        );
        $this->assertFalse($result);

        // test grant
        $result = $form->execute(
            [
                'aro' => $aro,
                'controllers' => ['Roles' => ['action' => ['edit' => '1']]],
            ]
        );
        $this->assertTrue($result);
        $this->assertTrue($Acl->check($aro->get('id'), 'controllers/Roles/edit'));

        // test deny
        $result = $form->execute(
            [
                'aro' => $aro,
                'controllers' => ['Roles' => ['action' => ['edit' => '-1']]],
            ]
        );
        $this->assertTrue($result);
        $this->assertFalse($Acl->check($aro->get('id'), 'controllers/Roles/edit'));
    }
}
