<?php

namespace Versae\Test\TestCase\Form;

use Versae\Form\ChangeEntityForm;
use AsalaeCore\TestSuite\TestCase;
use Cake\Error\Debugger;

class ChangeEntityFormTest extends TestCase
{
    public $fixtures = [
        'app.Aros',
        'app.ChangeEntityRequests',
        'app.OrgEntities',
        'app.Roles',
        'app.RolesTypeEntities',
        'app.TypeEntities',
        'app.Users',
    ];

    public function testschema()
    {
        $form = new ChangeEntityForm();
        $archivalAgencyIdField = $form->getSchema()->field('archival_agency_id');
        $this->assertArrayHasKey('type', $archivalAgencyIdField);
        $this->assertEquals('integer', $archivalAgencyIdField['type']);
    }

    public function testvalidation()
    {
        $form = new ChangeEntityForm();

        // ok - changement de service d'archives
        $validation = $form->validate(
            [
                'base_archival_agency_id' => 3,
                'archival_agency_id' => 2,
                'user_id' => 4,
                'reason' => 'test',
                'disable_user' => true,
            ]
        );
        $this->assertTrue($validation, Debugger::exportVarAsPlainText($form->getErrors()));

        // ok - changement d'entité
        $validation = $form->validate(
            [
                'base_archival_agency_id' => 2,
                'archival_agency_id' => 2,
                'org_entity_id' => 4,
                'user_id' => 4,
                'role_id' => 2,
            ]
        );
        $this->assertTrue($validation, Debugger::exportVarAsPlainText($form->getErrors()));

        // ko - valeurs manquantes
        $form->validate(
            [
                'base_archival_agency_id' => null,
                'archival_agency_id' => null,
                'org_entity_id' => null,
                'user_id' => null,
                'role_id' => null,
            ]
        );
        $this->assertCount(5, $form->getErrors());

        // ko - org_entity_id manquant malgrès un non changement de service d'archives

        $form->validate(
            [
                'base_archival_agency_id' => 2,
                'archival_agency_id' => 2,
                'org_entity_id' => null,
                'user_id' => 4,
                'role_id' => 2,
            ]
        );
        $this->assertCount(1, $form->getErrors());

        // ok - car changement de service d'archives
        $validation = $form->validate(
            [
                'base_archival_agency_id' => 2,
                'archival_agency_id' => 3,
                'org_entity_id' => null,
                'user_id' => 4,
            ]
        );
        $this->assertTrue($validation, Debugger::exportVarAsPlainText($form->getErrors()));
    }

    public function testexecute()
    {
        $form = new ChangeEntityForm();
        $ChangeEntityRequests = $this->fetchTable('ChangeEntityRequests');
        $ChangeEntityRequests->deleteAll([]);

        // changement de service d'archives
        $validation = $form->execute(
            [
                'base_archival_agency_id' => 3,
                'archival_agency_id' => 2,
                'user_id' => 4,
                'reason' => 'test',
                'disable_user' => true,
            ]
        );
        $this->assertTrue($validation, Debugger::exportVarAsPlainText($form->getErrors()));

        $request = $ChangeEntityRequests->find()
            ->select(
                [
                    'base_archival_agency_id',
                    'target_archival_agency_id',
                    'user_id',
                    'reason',
                ]
            )
            ->disableHydration()
            ->firstOrFail();
        $expected = [
            'base_archival_agency_id' => 3,
            'target_archival_agency_id' => 2,
            'user_id' => 4,
            'reason' => 'test',
        ];
        $this->assertEquals($expected, $request);

        $Users = $this->fetchTable('Users');
        $user = $Users->get(4);
        $this->assertFalse($user->get('active'));

        // changement d'entité au sein d'un même service d'archives
        $validation = $form->execute(
            [
                'base_archival_agency_id' => 2,
                'archival_agency_id' => 2,
                'org_entity_id' => 4,
                'user_id' => 4,
                'role_id' => 2,
            ]
        );
        $this->assertTrue($validation, Debugger::exportVarAsPlainText($form->getErrors()));
        $user = $Users->get(4);
        $this->assertEquals(4, $user->get('org_entity_id'));
    }
}
