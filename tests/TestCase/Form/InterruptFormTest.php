<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Form;

use Cake\Core\Configure;
use AsalaeCore\TestSuite\TestCase;
use Libriciel\Filesystem\Utility\Filesystem;
use Versae\Form\InterruptForm;

class InterruptFormTest extends TestCase
{
    public $fixtures = [
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->originalPathToLocal = Configure::read('App.paths.path_to_local_config');
        $dir = TMP_TESTDIR . DS . 'testinterrupt';
        Configure::write('App.defaultLocale', 'fr_FR');
        Configure::write('App.paths.path_to_local_config', $dir . DS . 'path_to_local.php');
        Filesystem::reset();
        Filesystem::dumpFile($dir . DS . 'path_to_local.php', "<?php return '$dir/app_local.json';?>");
        Filesystem::dumpFile($dir . DS . 'app_local.json', "{}");
    }

    public function tearDown(): void
    {
        Configure::write('App.paths.path_to_local_config', $this->originalPathToLocal);
        $dir = TMP_TESTDIR . DS . 'testinterrupt';
        Filesystem::remove($dir);
        parent::tearDown();
    }

    public function testSchema()
    {
        $form = new InterruptForm();
        $fields = $form->getSchema()->fields();
        $this->assertContains('message', $fields);
    }

    public function testExecute()
    {
        $data = [
            'enabled' => true,
            'scheduled__begin' => '01/01/1950 11:20:00',
            'scheduled__end' => '01/01/2500 13:20:00',
            'periodic__begin' => '11:00:00',
            'periodic__end' => '12:00:00',
            'enable_whitelist' => true,
            'whitelist_headers_inline' => 'X-Asalae-Webservice, Foo-Bar',
            'message' => 'test',
            'message_periodic' => 'test',
        ];
        $form = new InterruptForm();
        $form->execute($data);
        $this->assertTrue($form->execute($data));
        $dir = TMP_TESTDIR . DS . 'testinterrupt';
        $result = json_decode(file_get_contents($dir . DS . 'app_local.json'), true);
        $expected = [
            'Interruption' => [
                'enabled' => true,
                'scheduled' => [
                    'begin' => '1950-01-01T11:20:00+01:00',
                    'end' => '2500-01-01T13:20:00+01:00'
                ],
                'periodic' => [
                    'begin' => '11:00:00',
                    'end' => '12:00:00'
                ],
                'enable_whitelist' => true,
                'whitelist_headers' => [
                    'X-Asalae-Webservice' => true,
                    'Foo-Bar' => true
                ],
                'message' => 'test',
                'message_periodic' => 'test',
            ]
        ];
        $this->assertEquals($expected, $result);
    }
}
