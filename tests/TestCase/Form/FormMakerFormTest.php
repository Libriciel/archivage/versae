<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Form;

use AsalaeCore\Factory\Utility;
use AsalaeCore\Form\MessageSchema\Seda10Schema;
use AsalaeCore\Http\Client;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\TestSuite\VolumeSample;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Object\CommandResult;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Http\Client\Response;
use Cake\Http\Exception\HttpException;
use Cake\I18n\FrozenTime;
use Cake\I18n\I18n;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Datacompressor\Utility\DataCompressor;
use Exception;
use Laminas\Diactoros\Stream;
use Libriciel\Filesystem\Utility\Filesystem;
use Psr\Http\Message\StreamInterface;
use SplFileInfo;
use Versae\Exception\FormMakerException;
use Versae\Exception\GenericException;
use Versae\Form\FormMakerForm;
use Versae\Model\Table\DepositsTable;
use Versae\Model\Table\FileuploadsTable;
use Versae\Model\Table\FormExtractorsTable;
use Versae\Model\Table\FormInputsTable;
use Versae\Model\Table\FormUnitsTable;
use Versae\Model\Table\FormVariablesTable;
use Versae\Test\Mock\ClientMock;

class FormMakerFormTest extends TestCase
{
    use InvokePrivateTrait;

    public function setUp(): void
    {
        parent::setUp();
        VolumeSample::init();
        Configure::write('App.paths.data', TMP_TESTDIR);

        $stdout = json_encode(
            [
                'files' => [
                    0 => [
                        'filename' => 'test.txt',
                        'matches' => [
                            0 => [
                                'id' => 'UNKNOWN',
                                'mime' => 'application/octet-stream',
                            ],
                        ],
                    ],
                ],
            ]
        );
        $exec = $this->getMockBuilder(Exec::class)
            ->onlyMethods(['command'])
            ->getMock();
        $exec->method('command')->willReturn(
            new CommandResult(['success' => true, 'code' => 0, 'stdout' => $stdout, 'stderr' => ''])
        );
        Utility::set('Exec', $exec);
    }

    public $fixtures = [
        'app.Counters',
        'app.DepositValues',
        'app.Deposits',
        'app.Fileuploads',
        'app.FormCalculators',
        'app.FormExtractors',
        'app.FormFieldsets',
        'app.FormInputs',
        'app.FormTransferHeaders',
        'app.FormUnitContents',
        'app.FormUnitHeaders',
        'app.FormUnitKeywordDetails',
        'app.FormUnitKeywords',
        'app.FormUnitManagements',
        'app.FormUnits',
        'app.FormVariables',
        'app.Forms',
        'app.OrgEntities',
        'app.Pronoms',
        'app.Sequences',
        'app.Siegfrieds',
        'app.TypeEntities',
        'app.Users',
    ];

    public function tearDown(): void
    {
        Utility::reset();
        VolumeSample::destroy();
        parent::tearDown();
    }

    public function testPingSedaGenerator()
    {
        $response = $this->createMock(Response::class);
        $response->method('getStatusCode')->willReturn(200);
        $client = $this->getMockBuilder(Client::class)
            ->onlyMethods(['get'])
            ->getMock();
        $client->method('get')->willReturn($response);
        ClientMock::$mock = $client;
        Utility::set(Client::class, new ClientMock());
        $this->assertTrue(FormMakerForm::pingSedaGenerator());
    }

    public function testValidationDefault()
    {
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(1, 1, $deposit);
        $this->assertTrue($form->validate(['input_test' => 'foo']));
        $this->assertFalse($form->validate(['input_test' => (array)'foo']));

        I18n::setLocale('FR_fr');
        $FormInputs = TableRegistry::getTableLocator()->get('FormInputs');
        $types = [
            FormInputsTable::TYPE_CHECKBOX => '1',
            FormInputsTable::TYPE_DATE => '01/01/2000',
            FormInputsTable::TYPE_DATETIME => '01/01/2000 00:00',
            FormInputsTable::TYPE_MULTI_CHECKBOX => '',
            FormInputsTable::TYPE_NUMBER => 1,
            FormInputsTable::TYPE_PARAGRAPH => 'test',
            FormInputsTable::TYPE_SELECT => 'test',
            FormInputsTable::TYPE_TEXTAREA => 'test',
            FormInputsTable::TYPE_TEXT => 'test',
        ];
        foreach ($types as $type => $value) {
            $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
            $form = FormMakerForm::create(1, 1, $deposit);
            $FormInputs->updateAll(['type' => $type], ['id' => 1]);
            $form->getSchema(); // juste pour la converture de code
            $this->assertTrue($form->validate(['input_test' => $value]), 'error on ' . $type);
        }
        $FormInputs->updateAll(
            [
                'required' => true,
                'pattern' => '.*',
                'multiple' => true,
            ],
            ['id' => 1]
        );
        $FormInputs->updateAll(['type' => FormInputsTable::TYPE_FILE], ['id' => 1]);
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(1, 1, $deposit);
        $form->getSchema(); // juste pour la converture de code
        $this->assertTrue($form->validate(['input_test' => 'foo']));

        $FormInputs->updateAll(['type' => FormInputsTable::TYPE_EMAIL], ['id' => 1]);
        $form = FormMakerForm::create(1, 1, $deposit);
        $form->getSchema(); // juste pour la converture de code
        $this->assertTrue($form->validate(['input_test' => 'foo@bar.com']));
    }

    public function testValidateDate()
    {
        I18n::setLocale('FR_fr');
        $form = new FormMakerForm();
        $this->assertTrue($form->validateDate('01/01/1950'));
        $this->assertFalse($form->validateDate('not a date'));
    }

    public function testValidateDatetime()
    {
        I18n::setLocale('FR_fr');
        $form = new FormMakerForm();
        $this->assertTrue($form->validateDatetime('01/01/1950 12:00'));
        $this->assertFalse($form->validateDatetime('not a datetime'));
    }

    public function testIsValidForm()
    {
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(1, 1, $deposit);
        $this->assertTrue($form->isValidForm());

        // communicabilité partielle (1)
        $FormUnitManagements = TableRegistry::getTableLocator()->get('FormUnitManagements');
        $FormUnitManagements->query()
            ->insert(['form_unit_id', 'name', 'form_variable_id'])
            ->values(
                [
                    'form_unit_id' => 1,
                    'name' => 'AppraisalRule_Code',
                    'form_variable_id' => 1,
                ]
            )
            ->execute();
        $form->setErrors([]);
        $this->assertFalse($form->isValidForm());
        $FormUnitManagements->query()
            ->insert(['form_unit_id', 'name', 'form_variable_id'])
            ->values(
                [
                    'form_unit_id' => 2,
                    'name' => 'AppraisalRule_Code',
                    'form_variable_id' => 1,
                ]
            )
            ->execute();
        $form->setErrors([]);
        $this->assertFalse($form->isValidForm());

        // communicabilité partielle (2)
        $FormUnitManagements->query()
            ->insert(['form_unit_id', 'name', 'form_variable_id'])
            ->values(
                [
                    'form_unit_id' => 1,
                    'name' => 'AppraisalRule_Duration',
                    'form_variable_id' => 2,
                ]
            )
            ->execute();
        $form->setErrors([]);
        $this->assertFalse($form->isValidForm());
        $FormUnitManagements->query()
            ->insert(['form_unit_id', 'name', 'form_variable_id'])
            ->values(
                [
                    'form_unit_id' => 2,
                    'name' => 'AppraisalRule_Duration',
                    'form_variable_id' => 2,
                ]
            )
            ->execute();
        $form->setErrors([]);
        $this->assertFalse($form->isValidForm());

        // communicabilité complète
        $FormUnitManagements->query()
            ->insert(['form_unit_id', 'name', 'form_variable_id'])
            ->values(
                [
                    'form_unit_id' => 1,
                    'name' => 'AppraisalRule_StartDate',
                    'form_variable_id' => 3,
                ]
            )
            ->values(
                [
                    'form_unit_id' => 2,
                    'name' => 'AppraisalRule_StartDate',
                    'form_variable_id' => 3,
                ]
            )
            ->execute();
        $form->setErrors([]);
        $this->assertTrue($form->isValidForm());

        // restriction d'accès partielle (1)
        $FormUnitManagements = TableRegistry::getTableLocator()->get('FormUnitManagements');
        $FormUnitManagements->query()
            ->insert(['form_unit_id', 'name', 'form_variable_id'])
            ->values(
                [
                    'form_unit_id' => 1,
                    'name' => 'AccessRule_Rule',
                    'form_variable_id' => 1,
                ]
            )
            ->execute();
        $form->setErrors([]);
        $this->assertFalse($form->isValidForm());
        $FormUnitManagements->query()
            ->insert(['form_unit_id', 'name', 'form_variable_id'])
            ->values(
                [
                    'form_unit_id' => 2,
                    'name' => 'AccessRule_Rule',
                    'form_variable_id' => 1,
                ]
            )
            ->execute();
        $form->setErrors([]);
        $this->assertFalse($form->isValidForm());
    }

    public function testGetSchema()
    {
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(1, 1, $deposit);
        $this->assertArrayHasKey('type', $form->getSchema()->field('input_test'));
    }

    public function testExecute()
    {
        $response = $this->createMock(Response::class);
        $response->method('getStatusCode')->willReturn(200);
        $util = DOMUtility::loadXML(VolumeSample::getSampleContent('sample_seda21.xml'));
        $util->dom->getElementsByTagName('Filename')
            ->item(0)
            ->nodeValue = 'firstdir/testfile.txt';
        $au = $util->dom->getElementsByTagName('ArchiveUnit')->item(0);
        $dor = $util->createElement('DataObjectReference');
        $dorId = $util->createElement('DataObjectReferenceId', 'ID1');
        $util->appendChild($au, $dor);
        $util->appendChild($dor, $dorId);

        $response->method('getStringBody')
            ->willReturn($util->dom->saveXML());
        $client = $this->getMockBuilder(Client::class)
            ->onlyMethods(['post'])
            ->getMock();
        $client->method('post')->willReturn($response);
        ClientMock::$mock = $client;
        Utility::set(Client::class, new ClientMock());
        Filesystem::dumpFile($file = TMP_TESTDIR . '/firstdir/testfile.txt', 'test');
        DataCompressor::compress(dirname($file), $zip = TMP_TESTDIR . DS . 'testfile.tar.gz');
        $loc = TableRegistry::getTableLocator();
        $Fileuploads = $loc->get('Fileuploads');
        $Fileuploads->updateAll(['name' => 'testfile.zip', 'path' => $zip, 'locked' => false], ['id' => 1]);
        $FormInputs = $loc->get('FormInputs');
        $FormInputs->updateAll(['type' => FormInputsTable::TYPE_ARCHIVE_FILE], ['id' => 1]);
        $FormUnits = $loc->get('FormUnits');
        $FormUnits->updateAll(['search_expression' => '*'], ['id' => 5]);
        $FormUnitKeywords = $loc->get('FormUnitKeywords');
        $FormUnitKeywords->updateAll(['multiple_with' => null], ['id' => 1]);

        $deposit = new Entity(['path' => TMP_TESTDIR . '/testExportFiles']);
        $form = FormMakerForm::create(1, 1, $deposit);
        $form->execute(['input_test' => '1']);
        $this->assertTrue(true);

        // cas d'erreur 1
        $response = $this->createMock(Response::class);
        $response->method('getStatusCode')->willReturn(400);
        $body = $this->createMock(StreamInterface::class);
        $body->method('getContents')->willReturn('test message');
        $response->method('getBody')->willReturn($body);

        $client = $this->getMockBuilder(Client::class)
            ->onlyMethods(['post'])
            ->getMock();
        $client->method('post')->willReturn($response);
        ClientMock::$mock = $client;
        $form = FormMakerForm::create(1, 1, $deposit);
        $this->assertFalse($form->execute(['input_test' => '1']));
        $this->assertStringContainsString(
            'test message',
            var_export($form->getErrors(), true)
        );

        // cas d'erreur 2 (validation)
        $response = $this->createMock(Response::class);
        $response->method('getStatusCode')->willReturn(400);
        $response->method('getStringBody')->willReturn(
            json_encode(
                [
                    'xml' => '<xml>test</xml>',
                    'validation-errors' => [0 => ['line' => 1, 'column' => 1, 'message' => 'test message']],
                ]
            )
        );
        $client = $this->getMockBuilder(Client::class)
            ->onlyMethods(['post'])
            ->getMock();
        $client->method('post')->willReturn($response);
        ClientMock::$mock = $client;
        $form = FormMakerForm::create(1, 1, $deposit);
        $this->assertFalse($form->execute(['input_test' => '1']));
        $this->assertStringContainsString(
            'test message',
            var_export($form->getErrors(), true)
        );
    }

    public function testprepareData()
    {
        I18n::setLocale('FR_fr');
        $FormInputs = TableRegistry::getTableLocator()->get('FormInputs');
        $FormInputs->updateAll(['type' => FormInputsTable::TYPE_DATE], ['id' => 1]);
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(1, 1, $deposit);
        $data = $this->invokeMethod($form, 'prepareData', [['input_test' => '01/01/2000']]);
        $this->assertEquals('2000-01-01', $data['input_test']);

        $FormInputs->updateAll(['type' => FormInputsTable::TYPE_DATETIME], ['id' => 1]);
        $form = FormMakerForm::create(1, 1, $deposit);
        $data = $this->invokeMethod($form, 'prepareData', [['input_test' => '01/01/2000 08:00']]);
        $this->assertEquals('2000-01-01T08:00:00+01:00', $data['input_test']);
    }

    public function testextractData()
    {
        /**
         * XML
         */
        $targetFile = VolumeSample::copySample('sample-eaccpf.xml');
        $FormExtractors = TableRegistry::getTableLocator()->get('FormExtractors');
        $formExtractor = $FormExtractors->newEntity(
            [
                'name' => 'test',
                'data_format' => FormExtractorsTable::DATA_FORMAT_XML,
                'namespace' => '[{"prefix": "ns", "namespace": "urn:isbn:1-931666-33-4"}]',
                'data_path' => 'ns:control/ns:recordId',
                'form_input' => [
                    'id' => 1,
                    'name' => 'test',
                ],
            ]
        );
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(1, 1, $deposit);
        $data = $this->invokeMethod(
            $form,
            'extractData',
            [$targetFile, $formExtractor]
        );
        $this->assertEquals('test-import', $data); // contenu du sample-eaccpf.xml (balise recordId)

        // file not found
        $e = null;
        try {
            $this->invokeMethod(
                $form,
                'extractData',
                ['not exists', $formExtractor]
            );
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(FormMakerException::class, $e);

        // invalid data_path
        $formExtractor->set('data_path', 'invalid path');
        $e = null;
        try {
            $this->invokeMethod(
                $form,
                'extractData',
                [$targetFile, $formExtractor]
            );
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(FormMakerException::class, $e);

        // data_path donnant plusieurs noeuds
        $formExtractor->set('data_path', '//ns:*');
        $formExtractor->set('multiple', true);
        $data = $this->invokeMethod(
            $form,
            'extractData',
            [$targetFile, $formExtractor]
        );
        $this->assertIsArray($data);

        // data_path ne donnant aucun noeuds
        $formExtractor->set('data_path', 'notexists');
        try {
            $this->invokeMethod(
                $form,
                'extractData',
                [$targetFile, $formExtractor]
            );
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(FormMakerException::class, $e);

        /**
         * JSON
         */
        $formExtractor->set('multiple', false);
        $formExtractor->set('data_format', FormExtractorsTable::DATA_FORMAT_JSON);
        $formExtractor->set('data_path', '0.username');
        $targetFile = VolumeSample::copySample('administrateurs.json');
        $data = $this->invokeMethod(
            $form,
            'extractData',
            [$targetFile, $formExtractor]
        );
        $this->assertEquals('admin', $data);

        // data_path donnant plusieurs noeuds
        $formExtractor->set('data_path', '..*');
        $formExtractor->set('multiple', true);
        $data = $this->invokeMethod(
            $form,
            'extractData',
            [$targetFile, $formExtractor]
        );
        $this->assertIsArray($data);

        // data_path ne donnant aucun noeuds
        $formExtractor->set('data_path', 'notexists');
        try {
            $this->invokeMethod(
                $form,
                'extractData',
                [$targetFile, $formExtractor]
            );
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(FormMakerException::class, $e);

        // invalid data_path
        $formExtractor->set('data_path', 'invalid path');
        try {
            $this->invokeMethod(
                $form,
                'extractData',
                [$targetFile, $formExtractor]
            );
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(FormMakerException::class, $e);

        /**
         * CSV
         */
        $formExtractor->set('multiple', false);
        $formExtractor->set('data_format', FormExtractorsTable::DATA_FORMAT_CSV);
        $formExtractor->set('data_path', 'B3'); // 3e ligne, 2e colonne
        $targetFile = VolumeSample::copySample('mots_clefs.csv');
        $data = $this->invokeMethod(
            $form,
            'extractData',
            [$targetFile, $formExtractor]
        );
        $this->assertEquals('Brest', $data);

        // invalid data_path
        $formExtractor->set('data_path', 'invalid path');
        try {
            $this->invokeMethod(
                $form,
                'extractData',
                [$targetFile, $formExtractor]
            );
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(FormMakerException::class, $e);

        // data_path ne donnant aucun noeuds
        $formExtractor->set('data_path', 'ZZ255');
        try {
            $this->invokeMethod(
                $form,
                'extractData',
                [$targetFile, $formExtractor]
            );
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(FormMakerException::class, $e);

        /**
         * Autre
         */
        $formExtractor->set('data_format', 'invalid format');
        try {
            $this->invokeMethod(
                $form,
                'extractData',
                [$targetFile, $formExtractor]
            );
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(FormMakerException::class, $e);
    }

    public function testgetExtractors()
    {
        $FormExtractors = TableRegistry::getTableLocator()->get('FormExtractors');
        $FormExtractors->updateAll(
            [
                'type' => FormExtractorsTable::TYPE_WEBSERVICE,
                'data_format' => FormExtractorsTable::DATA_FORMAT_JSON,
                'data_path' => '0.username',
                'app_meta' => json_encode(
                    [
                        'url' => 'https://test',
                        'username' => 'test',
                        'password' => 'test',
                        'use_proxy' => false,
                        'ssl_verify_peer' => false,
                        'ssl_verify_peer_name' => false,
                        'ssl_verify_depth' => false,
                        'ssl_verify_host' => false,
                        'ssl_cafile' => false,
                    ]
                ),
            ],
            ['id' => 1]
        );
        $targetFile = VolumeSample::copySample('administrateurs.json');
        $response = $this->createMock(Response::class);
        $response->method('getStatusCode')->willReturn(200);
        $handle = fopen($targetFile, 'r');
        $response->method('getBody')->willReturn(new Stream($handle));
        $client = $this->getMockBuilder(Client::class)
            ->onlyMethods(['get'])
            ->getMock();
        $client->method('get')->willReturn($response);
        ClientMock::$mock = $client;
        Utility::set(Client::class, new ClientMock());
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(1, 1, $deposit);
        $data = $this->invokeMethod(
            $form,
            'getExtractors',
            [[]]
        );
        fclose($handle);
        $this->assertEquals('admin', $data['extractor_test']);

        // test requete en erreur
        Configure::write('Proxy', ['host' => 'localhost', 'port' => 123]);
        $response = $this->createMock(Response::class);
        $response->method('getStatusCode')->willReturn(403);
        $client = $this->getMockBuilder(Client::class)
            ->onlyMethods(['get'])
            ->getMock();
        $client->method('get')->willReturn($response);
        ClientMock::$mock = $client;
        $FormExtractors->updateAll(
            [
                'type' => FormExtractorsTable::TYPE_WEBSERVICE,
                'data_format' => FormExtractorsTable::DATA_FORMAT_CSV,
                'data_path' => 'B3',
                'app_meta' => json_encode(
                    [
                        'url' => 'https://test',
                        'username' => 'test',
                        'password' => 'test',
                        'use_proxy' => true,
                        'ssl_verify_peer' => false,
                        'ssl_verify_peer_name' => false,
                        'ssl_verify_depth' => false,
                        'ssl_verify_host' => false,
                        'ssl_cafile' => false,
                    ]
                ),
            ],
            ['id' => 1]
        );
        $e = null;
        try {
            $this->invokeMethod(
                $form,
                'getExtractors',
                [[]]
            );
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(HttpException::class, $e);
    }

    public function testextractFromFiles()
    {
        $targetFile = VolumeSample::copySample('sample-eaccpf.xml');
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $Fileuploads->updateAll(['path' => $targetFile], ['id' => 1]);
        $FormExtractors = TableRegistry::getTableLocator()->get('FormExtractors');
        $formExtractor = $FormExtractors->newEntity(
            [
                'name' => 'test',
                'data_format' => FormExtractorsTable::DATA_FORMAT_XML,
                'namespace' => '[{"prefix": "ns", "namespace": "urn:isbn:1-931666-33-4"}]',
                'data_path' => 'ns:control/ns:recordId',
                'form_input' => [
                    'id' => 1,
                    'name' => 'input_test',
                ],
            ]
        );
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(1, 1, $deposit);
        $data = $this->invokeMethod(
            $form,
            'extractFromFiles',
            [['input_test' => '1'], $formExtractor]
        );
        $this->assertEquals('test-import', $data['test']);

        $formExtractor->set('multiple', false);
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(1, 1, $deposit);
        $data = $this->invokeMethod(
            $form,
            'extractFromFiles',
            [['input_test' => '1'], $formExtractor]
        );
        $this->assertEquals('test-import', $data['test']);
    }

    public function testextractFromSingleFile()
    {
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $targetFile = VolumeSample::copySample(
            'sample_transfer10.zip',
            'exported/upload/1/1/sample_transfer10.zip'
        );
        $FormInputs = $this->fetchTable('FormInputs');
        $FormInputs->updateAll(['type' => FormInputsTable::TYPE_ARCHIVE_FILE], ['id' => 1]);
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $Fileuploads->updateAll(
            [
                'name' => basename($targetFile),
                'path' => $targetFile,
                'locked' => false,
                'mime' => mime_content_type($targetFile),
            ],
            ['id' => 1]
        );
        $fileupload = $Fileuploads->get(1);
        $FormExtractors = TableRegistry::getTableLocator()->get('FormExtractors');
        $formExtractor = $FormExtractors->newEntity(
            [
                'name' => 'test',
                'type' => FormExtractorsTable::TYPE_ARCHIVE_FILE,
                'data_format' => FormExtractorsTable::DATA_FORMAT_XML,
                'file_selector' => 'message/*.xml',
                'namespace' => '[{"prefix": "ns", "namespace": "fr:gouv:culture:archivesdefrance:seda:v1.0"}]',
                'data_path' => 'ns:Archive/ns:Name',
                'form_input' => [
                    'id' => 1,
                    'name' => 'input_test',
                ],
            ]
        );
        $form = FormMakerForm::create(1, 1, $deposit);
        $this->invokeMethod(
            $form,
            'lockFiles',
            [['input_test' => 1]]
        );
        $data = $this->invokeMethod(
            $form,
            'extractFromSingleFile',
            [$fileupload, $formExtractor]
        );
        $this->assertEquals('53556ab4-bda8-4842-8841-7e7ca69f4490', $data['test']);

        // file_selector -> not found
        $form = FormMakerForm::create(1, 1, $deposit);
        $formExtractor->set('file_selector', 'test');
        $data = $this->invokeMethod(
            $form,
            'extractFromSingleFile',
            [$fileupload, $formExtractor]
        );
        $this->assertEmpty($data);

        // 1 fichiers, plusieurs données
        $targetFile = VolumeSample::copySample('sample-eaccpf.xml');
        $Fileuploads->patchEntity(
            $fileupload,
            [
                'path' => $targetFile,
                'locked' => true,
                'mime' => mime_content_type($targetFile),
            ]
        );
        $form = FormMakerForm::create(1, 1, $deposit);
        $formExtractor = $FormExtractors->newEntity(
            [
                'name' => 'test',
                'type' => FormExtractorsTable::TYPE_FILE,
                'data_format' => FormExtractorsTable::DATA_FORMAT_XML,
                'namespace' => '[{"prefix": "ns", "namespace": "urn:isbn:1-931666-33-4"}]',
                'data_path' => '//ns:*',
                'multiple' => true,
                'form_input' => [
                    'id' => 1,
                    'name' => 'input_test',
                    'multiple' => true, // FIXME, bug dans l'UI sur le multiple x2²
                ],
            ]
        );
        $data = $this->invokeMethod(
            $form,
            'extractFromSingleFile',
            [$fileupload, $formExtractor]
        );
        $this->assertIsArray($data);
    }

    public function testgetVariables()
    {
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(1, 1, $deposit);
        $data = $this->invokeMethod(
            $form,
            'getVariables',
            [[]]
        );
        $this->assertArrayHasKey('var_test', $data);

        $FormVariables = TableRegistry::getTableLocator()->get('FormVariables');
        $FormVariables->updateAll(['twig' => '{{input.input_test}}'], ['id' => 1]);
        $form = FormMakerForm::create(1, 1, $deposit);
        $data = $this->invokeMethod(
            $form,
            'getVariables',
            [['input' => ['input_test' => 'testunit']]]
        );
        $this->assertArrayHasKey('var_test', $data);
        $this->assertEquals('testunit', $data['var_test']);

        $FormCalculators = TableRegistry::getTableLocator()->get('FormCalculators');
        $FormCalculators->updateAll(['multiple' => true], ['id' => 1]);
        $form = FormMakerForm::create(1, 1, $deposit);
        $data = $this->invokeMethod(
            $form,
            'getVariables',
            [['input' => ['input_test' => 'testunit'], 'var' => []]]
        );
        $this->assertNull($data['var_test']);

        $FormVariables->updateAll(['twig' => '{{multiple.value}}'], ['id' => 1]);
        $FormVariables->updateAll(['app_meta' => json_encode(['multiple_with' => 'input.input_test'])], ['id' => 1]);
        $form = FormMakerForm::create(1, 1, $deposit);
        $data = $this->invokeMethod(
            $form,
            'getVariables',
            [['input' => ['input_test' => ['foo', 'bar']], 'var' => []]]
        );
        $this->assertArrayHasKey('var_test', $data);
        $this->assertEquals(['foo', 'bar'], $data['var_test']);
    }

    public function testgetRequestData()
    {
        $FormUnits = TableRegistry::getTableLocator()->get('FormUnits');
        $FormUnits->deleteAll(['id <=' => 4]);
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(1, 1, $deposit);
        $puids = array_map(
            function ($v) {
                return $v['value'];
            },
            Seda10Schema::getXsdOptions('filetype')
        );
        $mimes = array_map(
            function ($v) {
                return $v['value'];
            },
            Seda10Schema::getXsdOptions('mime')
        );
        $this->invokeProperty($form, 'puids', 'set', $puids);
        $this->invokeProperty($form, 'mimes', 'set', $mimes);
        $data = $this->invokeMethod(
            $form,
            'getRequestData',
            [[]]
        );
        $this->assertArrayNotHasKey('ArchiveUnits', $data);

        $this->insertFormUnits();
        $data = $this->invokeMethod(
            $form,
            'getRequestData',
            [
                [
                    'input' => [
                        'root' => 'noeud archive',
                        'first_au' => '1er archive_unit',
                        'input_test' => '1',
                    ],
                ],
            ]
        );
        $this->assertEquals('noeud archive', Hash::get($data, 'ArchiveUnits.0.Name'));
        $this->assertEquals('1er archive_unit', Hash::get($data, 'ArchiveUnits.0.ArchiveUnits.0.Name'));
        $this->assertEquals(
            'noeud archive/1er archive_unit/testfile.txt',
            Hash::extract($data, 'ArchiveUnits.0.ArchiveUnits.0.Files.{*}.Filename')[0]
        );
    }

    /**
     * N'est pas un test
     *
     * Création d'une structure :
     * root - (name = {{input.root}})
     *      \
     *       first AU - (name = {{input.first}}, keyword = test)
     *               \
     *                document - input.document
     */
    private function insertFormUnits()
    {
        $FormUnits = TableRegistry::getTableLocator()->get('FormUnits');
        $FormVariables = TableRegistry::getTableLocator()->get('FormVariables');
        $FormUnitHeaders = TableRegistry::getTableLocator()->get('FormUnitHeaders');
        $FormUnitKeywords = TableRegistry::getTableLocator()->get('FormUnitKeywords');

        /**
         * root
         */
        $root = $FormUnits->newEntity(
            [
                'form_id' => 1,
                'name' => 'root',
                'type' => FormUnitsTable::TYPE_SIMPLE,
                'parent_id' => null,
                'presence_required' => true,
                'storage_calculation' => FormUnitsTable::STORAGE_CALCULATION_ARCHIVE_UNIT_NAME,
            ]
        );
        $FormUnits->saveOrFail($root);
        $var = $FormVariables->newEntity(
            [
                'form_id' => 1,
                'type' => FormVariablesTable::TYPE_CONCAT,
                'twig' => '{{input.root}}',
            ]
        );
        $FormVariables->saveOrFail($var);
        $header = $FormUnitHeaders->newEntity(
            [
                'form_id' => 1,
                'form_unit_id' => $root->id,
                'name' => 'Name',
                'form_variable_id' => $var->id,
            ]
        );
        $FormUnitHeaders->saveOrFail($header);

        /**
         * first AU
         */
        $firstAU = $FormUnits->newEntity(
            [
                'form_id' => 1,
                'name' => 'first AU',
                'type' => FormUnitsTable::TYPE_SIMPLE,
                'parent_id' => $root->id,
                'presence_required' => true,
                'storage_calculation' => FormUnitsTable::STORAGE_CALCULATION_ARCHIVE_UNIT_NAME,
            ]
        );
        $FormUnits->saveOrFail($firstAU);
        $var = $FormVariables->newEntity(
            [
                'form_id' => 1,
                'type' => FormVariablesTable::TYPE_CONCAT,
                'twig' => '{{input.first_au}}',
            ]
        );
        $FormVariables->saveOrFail($var);
        $header = $FormUnitHeaders->newEntity(
            [
                'form_id' => 1,
                'form_unit_id' => $firstAU->id,
                'name' => 'Name',
                'form_variable_id' => $var->id,
            ]
        );
        $FormUnitHeaders->saveOrFail($header);
        $var = $FormVariables->newEntity(
            [
                'form_id' => 1,
                'type' => FormVariablesTable::TYPE_CONCAT,
                'twig' => 'test',
            ]
        );
        $FormVariables->saveOrFail($var);
        $keyword = $FormUnitKeywords->newEntity(
            [
                'form_unit_id' => $firstAU->id,
                'name' => 'Name',
                'form_variable_id' => $var->id,
            ]
        );
        $FormUnitKeywords->saveOrFail($keyword);

        /**
         * document
         */
        $document = $FormUnits->newEntity(
            [
                'form_id' => 1,
                'name' => 'document',
                'type' => FormUnitsTable::TYPE_DOCUMENT,
                'parent_id' => $firstAU->id,
                'form_input_id' => 1,
                'presence_required' => true,
            ]
        );
        $FormUnits->saveOrFail($document);
    }

    public function testgetTransferHeadersRequestData()
    {
        $targetFile = VolumeSample::copySample(
            'sample_transfer10.zip',
            'exported/upload/1/1/sample_transfer10.zip'
        );
        DataCompressor::uncompress($targetFile, TMP_TESTDIR . '/exported/uncompressed/1/1');
        $FormTransferHeaders = TableRegistry::getTableLocator()->get('FormTransferHeaders');
        $FormTransferHeaders->updateAll(
            ['name' => 'ArchivalAgency_Identification'],
            ['id' => 1]
        );
        $targetFile = VolumeSample::copySample(
            'sample_transfer10.zip',
            'exported/upload/1/1/sample_transfer10.zip'
        );
        $loc = TableRegistry::getTableLocator();
        $Fileuploads = $loc->get('Fileuploads');
        $Fileuploads->updateAll(
            [
                'path' => $targetFile,
                'locked' => false,
                'mime' => mime_content_type($targetFile),
            ],
            ['id' => 1]
        );
        $FormInputs = $loc->get('FormInputs');
        $FormInputs->updateAll(['type' => FormInputsTable::TYPE_ARCHIVE_FILE], ['id' => 1]);
        $FormInputs->updateAll(['type' => FormInputsTable::TYPE_ARCHIVE_FILE], ['id' => 1]);
        $FormUnits = $loc->get('FormUnits');
        $FormUnits->updateAll(['search_expression' => '*'], ['id' => 5]);
        $FormUnitKeywords = $loc->get('FormUnitKeywords');
        $FormUnitKeywords->updateAll(['multiple_with' => null], ['id' => 1]);
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(1, 1, $deposit);

        $puids = array_map(
            function ($v) {
                return $v['value'];
            },
            Seda10Schema::getXsdOptions('filetype')
        );
        $mimes = array_map(
            function ($v) {
                return $v['value'];
            },
            Seda10Schema::getXsdOptions('mime')
        );
        $this->invokeProperty($form, 'puids', 'set', $puids);
        $this->invokeProperty($form, 'mimes', 'set', $mimes);

        $data = $this->invokeMethod(
            $form,
            'getRequestData',
            [
                [
                    'input' => [
                        'input_test' => '1',
                    ],
                ],
            ]
        );
        $this->assertIsArray($data['ArchiveUnits']);

        $FormTransferHeaders->updateAll(
            ['name' => 'Date'],
            ['id' => 1]
        );
        $e = null;
        try {
            $this->invokeMethod(
                $form,
                'getRequestData',
                [
                    [
                        'input' => [
                            'input_test' => '1',
                        ],
                    ],
                ]
            );
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(FormMakerException::class, $e);

        $FormTransferHeaders->updateAll(
            ['name' => 'ArchivalAgency_Identification'],
            ['id' => 1]
        );
        $FormUnits->deleteAll(['form_id' => 1]);
        $data = $this->invokeMethod(
            $form,
            'getRequestData',
            [
                [
                    'input' => [
                        'input_test' => '1',
                    ],
                ],
            ]
        );
        $this->assertArrayNotHasKey('ArchiveUnits', $data);
    }

    public function testarchiveUnitsRequestData()
    {
        $data = [];
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(1, 1, $deposit);
        $formUnit = new Entity(['form_variable' => ['form_calculator' => ['name' => 'not exists']]]);
        $this->invokeMethod(
            $form,
            'archiveUnitsRequestData',
            [
                [$formUnit],
                [],
                &$data
            ]
        );
        $this->assertEmpty($data);

        // document multiple
        $formUnit = new Entity(['type' => FormUnitsTable::TYPE_DOCUMENT_MULTIPLE]);
        $this->invokeMethod(
            $form,
            'archiveUnitsRequestData',
            [
                [$formUnit],
                [],
                &$data
            ]
        );
        $this->assertNotEmpty($data);
    }

    public function testgetArchiveUnitSatRequestData()
    {
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(1, 1, $deposit);
        $formUnit = new Entity(
            [
                'form_unit_headers' => [
                    new Entity(
                        [
                            'name' => 'AccessRestrictionRule_Code',
                            'form_variable' => ['twig' => '{{input.test}}']
                        ]
                    ),
                    new Entity(
                        [
                            'name' => 'AccessRestrictionRule_StartDate',
                            'form_variable' => ['twig' => '2001-01-01']
                        ]
                    ),
                ]
            ]
        );
        $data = $this->invokeMethod(
            $form,
            'getArchiveUnitSatRequestData',
            [
                'form_unit_headers',
                $formUnit,
                ['input' => ['test' => 'testunit']]
            ]
        );
        $this->assertNotEmpty($data);
        $this->assertEquals('testunit', $data['AccessRestrictionRule']['Code']);
    }

    public function testgetArchiveUnitContentRequestData()
    {
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(1, 1, $deposit);
        $formUnit = new Entity(
            [
                'form_unit_contents' => [
                    new Entity(
                        [
                            'name' => 'CustodialHistory_CustodialHistoryItem',
                            'form_variable' => ['twig' => '{{input.test}}']
                        ]
                    )
                ]
            ]
        );
        $data = $this->invokeMethod(
            $form,
            'getArchiveUnitContentRequestData',
            [
                $formUnit,
                ['input' => ['test' => 'testunit']]
            ]
        );
        $this->assertNotEmpty($data);
        $this->assertEquals(
            'testunit',
            $data['ContentDescription']['CustodialHistory']['CustodialHistoryItem'][0]['value']
        );

        $formUnit = new Entity(
            [
                'form_unit_contents' => [
                    new Entity(
                        [
                            'name' => 'CustodialHistory_CustodialHistoryItem',
                            'form_variable' => ['twig' => '{{input.test}}']
                        ]
                    ),
                    new Entity(
                        [
                            'name' => 'CustodialHistory_CustodialHistoryItem@when',
                            'form_variable' => ['twig' => '{{input.test}}']
                        ]
                    ),
                ]
            ]
        );
        $data = $this->invokeMethod(
            $form,
            'getArchiveUnitContentRequestData',
            [
                $formUnit,
                ['input' => ['test' => 'testunit']]
            ]
        );
        $this->assertNotEmpty($data);
        $this->assertEquals(
            'testunit',
            $data['ContentDescription']['CustodialHistory']['CustodialHistoryItem'][0]['@when']
        );

        $formUnit = new Entity(
            [
                'form_unit_contents' => [
                    new Entity(
                        [
                            'name' => 'OriginatingAgency_Identification',
                            'form_variable' => ['twig' => '{{input.test}}']
                        ]
                    )
                ]
            ]
        );
        $data = $this->invokeMethod(
            $form,
            'getArchiveUnitContentRequestData',
            [
                $formUnit,
                ['input' => ['test' => 'testunit']]
            ]
        );
        $this->assertNotEmpty($data);
        $this->assertEquals(
            'testunit',
            $data['ContentDescription']['OriginatingAgency']['Identification']
        );

        $formUnit = new Entity(
            [
                'form_unit_contents' => [
                    new Entity(
                        [
                            'name' => 'OriginatingAgency_Name',
                            'form_variable' => ['twig' => '{{input.test}}']
                        ]
                    ),
                    new Entity(
                        [
                            'name' => 'OriginatingAgency_Identification',
                            'form_variable' => ['twig' => '{{input.test}}']
                        ]
                    ),
                ]
            ]
        );
        $data = $this->invokeMethod(
            $form,
            'getArchiveUnitContentRequestData',
            [
                $formUnit,
                ['input' => ['test' => 'testunit']]
            ]
        );
        $this->assertNotEmpty($data);
        $this->assertEquals(
            'testunit',
            $data['ContentDescription']['OriginatingAgency']['Name'] ?? null
        );
    }

    public function testgetArchiveUnitKeywordsRequestData()
    {
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(1, 1, $deposit);
        $formUnit = new Entity(
            [
                'form_unit_keywords' => [
                    new Entity(
                        [
                            'multiple_with' => 'input.tests',
                            'form_unit_keyword_details' => [
                                new Entity(
                                    [
                                        'name' => 'KeywordContent',
                                        'form_variable' => ['twig' => '{{multiple.value}}'],
                                    ]
                                ),
                            ],
                        ]
                    ),
                ],
            ]
        );
        $data = $this->invokeMethod(
            $form,
            'getArchiveUnitKeywordsRequestData',
            [
                $formUnit,
                ['input' => ['tests' => ['foo', 'bar']]]
            ]
        );
        $this->assertNotEmpty($data);
        $this->assertEquals(
            'foo',
            $data['ContentDescription']['Keyword'][0]['KeywordContent']
        );
        $this->assertEquals(
            'bar',
            $data['ContentDescription']['Keyword'][1]['KeywordContent']
        );

        $formUnit = new Entity(
            [
                'form_unit_keywords' => [
                    new Entity(
                        [
                            'multiple_with' => 'input.tests',
                            'form_unit_keyword_details' => [
                                new Entity(
                                    [
                                        'name' => 'KeywordContent',
                                        'form_variable' => ['twig' => '{{multiple.value}}'],
                                    ]
                                ),
                                new Entity(
                                    [
                                        'name' => 'AccessRestrictionRule_Code',
                                        'form_variable' => ['twig' => 'AR038'],
                                    ]
                                ),
                                new Entity(
                                    [
                                        'name' => 'AccessRestrictionRule_StartDate',
                                        'form_variable' => ['twig' => '2001-01-01']
                                    ]
                                ),
                            ],
                        ]
                    ),
                ],
            ]
        );
        $data = $this->invokeMethod(
            $form,
            'getArchiveUnitKeywordsRequestData',
            [
                $formUnit,
                ['input' => ['tests' => ['foo', 'bar']]]
            ]
        );
        $this->assertNotEmpty($data);
        $this->assertEquals(
            'AR038',
            $data['ContentDescription']['Keyword'][0]['AccessRestrictionRule']['Code']
        );
        $this->assertEquals(
            'AR038',
            $data['ContentDescription']['Keyword'][1]['AccessRestrictionRule']['Code']
        );
    }

    public function testappendArchiveUnitsDefaultValuesToRequestData()
    {
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(2, 1, $deposit);
        $currentData = [];
        $formUnit = new Entity(
            [
                'name' => 'title',
                'parent_id' => null, // initialise AccessRule et Content
            ]
        );
        $this->invokeMethod(
            $form,
            'appendArchiveUnitsDefaultValuesToRequestData',
            [
                &$currentData,
                $formUnit,
            ]
        );
        $this->assertNotEmpty($currentData['Id']);
        $this->assertNotEmpty($currentData['Content']);
        $this->assertNotEmpty($currentData['Content']['Title']);
        $this->assertNotEmpty($currentData['AccessRule']);
        $this->assertNotEmpty($currentData['AccessRule']['Rule']);

        $currentData = ['Content' => []];
        $formUnit = new Entity(
            [
                'name' => 'title',
                'parent_id' => 123, // initialise DescriptionLevel
            ]
        );
        $this->invokeMethod(
            $form,
            'appendArchiveUnitsDefaultValuesToRequestData',
            [
                &$currentData,
                $formUnit,
            ]
        );
        $this->assertArrayHasKey('DescriptionLevel', $currentData['Content']);
    }

    public function testdocumentFormUnitRequestData()
    {
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(2, 1, $deposit);
        $requestData = [];
        $formUnit = new Entity(['presence_required' => true]);
        $e = null;
        try {
            $this->invokeMethod(
                $form,
                'documentFormUnitRequestData',
                [
                    $formUnit,
                    [],
                    &$requestData,
                ]
            );
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(FormMakerException::class, $e); // presence_required

        $puids = array_map(
            function ($v) {
                return $v['value'];
            },
            Seda10Schema::getXsdOptions('filetype')
        );
        $mimes = array_map(
            function ($v) {
                return $v['value'];
            },
            Seda10Schema::getXsdOptions('mime')
        );
        $this->invokeProperty($form, 'puids', 'set', $puids);
        $this->invokeProperty($form, 'mimes', 'set', $mimes);
        $targetFile = VolumeSample::copySample('sample.pdf');
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $Fileuploads->updateAll(['path' => $targetFile, 'hash' => null, 'mime' => null], ['id' => 1]);
        $Siegfrieds = TableRegistry::getTableLocator()->get('Siegfrieds');
        $Siegfrieds->updateAll(
            [
                'pronom' => 'fmt/18', // Acrobat PDF 1.4 - Portable Document Format
            ],
            ['id' => 1]
        );
        $requestData = [];
        $formUnit = new Entity(
            [
                'form_input' => [
                    'id' => 1,
                    'name' => 'file_uploader',
                ],
                'form_unit_headers' => [
                    new Entity(
                        [
                            'name' => 'DescriptionLevel',
                            'form_variable' => ['twig' => '{{input.test}}']
                        ]
                    )
                ],
            ]
        );
        $this->invokeMethod(
            $form,
            'documentFormUnitRequestData',
            [
                $formUnit,
                ['input' => ['file_uploader' => '1', 'test' => 'item']],
                &$requestData,
            ]
        );
        $this->assertEquals('fmt/18', Hash::extract($requestData, 'Files.{*}.FormatId')[0]);
        $this->assertEquals('item', Hash::extract($requestData, 'Files.{*}.DescriptionLevel')[0]);
    }

    public function testdocumentMultipleFormUnitRequestData()
    {
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(2, 1, $deposit);
        $requestData = [];
        $formUnit = new Entity(['presence_required' => true]);
        $e = null;
        try {
            $this->invokeMethod(
                $form,
                'documentMultipleFormUnitRequestData',
                [
                    $formUnit,
                    [],
                    &$requestData,
                ]
            );
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(FormMakerException::class, $e); // presence_required

        $puids = array_map(
            function ($v) {
                return $v['value'];
            },
            Seda10Schema::getXsdOptions('filetype')
        );
        $mimes = array_map(
            function ($v) {
                return $v['value'];
            },
            Seda10Schema::getXsdOptions('mime')
        );
        $this->invokeProperty($form, 'puids', 'set', $puids);
        $this->invokeProperty($form, 'mimes', 'set', $mimes);
        $targetFile = VolumeSample::copySample('sample.pdf');
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $Fileuploads->updateAll(['path' => $targetFile, 'hash' => null, 'mime' => null], ['id' => 1]);
        $Siegfrieds = TableRegistry::getTableLocator()->get('Siegfrieds');
        $Siegfrieds->updateAll(
            [
                'pronom' => 'fmt/18', // Acrobat PDF 1.4 - Portable Document Format
            ],
            ['id' => 1]
        );
        $requestData = [];
        $formUnit = new Entity(
            [
                'form_input' => [
                    'id' => 1,
                    'name' => 'file_uploader',
                ],
                'form_unit_headers' => [
                    new Entity(
                        [
                            'name' => 'DescriptionLevel',
                            'form_variable' => ['twig' => '{{input.test}}']
                        ]
                    )
                ],
            ]
        );
        $this->invokeMethod(
            $form,
            'documentMultipleFormUnitRequestData',
            [
                $formUnit,
                ['input' => ['file_uploader' => ['1'], 'test' => 'item']], // file_uploader en array
                &$requestData,
            ]
        );
        $this->assertEquals('fmt/18', Hash::extract($requestData, 'Files.{*}.FormatId')[0]);
        $this->assertEquals('item', Hash::extract($requestData, 'Files.{*}.DescriptionLevel')[0]);

        // test en seda 1.0
        $form = FormMakerForm::create(1, 1, $deposit);
        $this->invokeProperty($form, 'puids', 'set', $puids);
        $this->invokeProperty($form, 'mimes', 'set', $mimes);
        $this->invokeMethod(
            $form,
            'documentMultipleFormUnitRequestData',
            [
                $formUnit,
                ['input' => ['file_uploader' => ['1'], 'test' => 'item']], // file_uploader en array
                &$requestData,
            ]
        );
        $this->assertEquals('sha256', Hash::extract($requestData, 'Files.{*}.algorithme')[0]);
    }

    public function testtreeRootFormUnitRequestData()
    {
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(1, 1, $deposit);
        $requestData = [];
        $formUnit = new Entity(['form_input' => ['id' => 1, 'name' => 'archive_uploader']]);
        $e = null;
        try {
            $this->invokeMethod(
                $form,
                'treeRootFormUnitRequestData',
                [
                    $formUnit,
                    ['input' => ['archive_uploader' => '1']],
                    &$requestData,
                ]
            );
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(FormMakerException::class, $e);
    }

    public function testtreeMakeDir()
    {
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(2, 1, $deposit);
        $puids = array_map(
            function ($v) {
                return $v['value'];
            },
            Seda10Schema::getXsdOptions('filetype')
        );
        $mimes = array_map(
            function ($v) {
                return $v['value'];
            },
            Seda10Schema::getXsdOptions('mime')
        );
        $this->invokeProperty($form, 'puids', 'set', $puids);
        $this->invokeProperty($form, 'mimes', 'set', $mimes);
        $requestData = [];
        $formUnit = new Entity(['full_name' => 'testunit']);
        $filename = VolumeSample::copySample(
            'sample_transfer10.zip',
            'exported/upload/1/1/sample_transfer10.zip'
        );
        DataCompressor::uncompress($filename, TMP_TESTDIR . '/exported/uncompressed/1/1');
        $fileupload = new Entity(['path' => $filename]);
        $this->invokeMethod(
            $form,
            'treeMakeDir',
            [
                'attachments',
                TMP_TESTDIR . '/exported/uncompressed/1/1',
                $fileupload,
                $formUnit,
                &$requestData,
            ]
        );
        $this->assertEquals(
            'testunit/attachments/file1.txt',
            Hash::extract($requestData, 'ArchiveUnits.0.Files.{*}.Filename')[0]
        );
    }

    public function testtreeMakeFile()
    {
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(2, 1, $deposit);
        $puids = array_map(
            function ($v) {
                return $v['value'];
            },
            Seda10Schema::getXsdOptions('filetype')
        );
        $mimes = array_map(
            function ($v) {
                return $v['value'];
            },
            Seda10Schema::getXsdOptions('mime')
        );
        $this->invokeProperty($form, 'puids', 'set', $puids);
        $this->invokeProperty($form, 'mimes', 'set', $mimes);
        $requestData = [];
        $formUnit = new Entity(['full_name' => 'testunit']);
        $filename = VolumeSample::copySample(
            'sample.pdf',
            'exported/uncompressed/1/1/testunit/sample.pdf'
        );
        $fileupload = new Entity(['path' => dirname($filename, 2) . DS . 'sample.zip']);
        $item = new SplFileInfo($filename);
        $sf = [
            $item->getPathname() => [
                'format' => 'fmt/18',
                'mime' => 'application/pdf',
            ],
        ];
        $this->invokeProperty($form, 'sf', 'set', $sf);
        $this->invokeMethod(
            $form,
            'treeMakeFile',
            [
                $item,
                $fileupload,
                $formUnit,
                &$requestData,
            ]
        );
        $this->assertEquals(
            'testunit/testunit/sample.pdf',
            Hash::extract($requestData, 'Files.{*}.Filename')[0]
        );
    }

    public function testalterArchiveUnitByPath()
    {
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(1, 1, $deposit);
        $formUnit = new Entity(
            [
                'name' => 'testunit',
                'search_expression' => '/foo/*/',
                'form_unit_headers' => [
                    new Entity(
                        [
                            'name' => 'AccessRestrictionRule_Code',
                            'form_variable' => ['twig' => '{{input.test}}']
                        ]
                    ),
                    new Entity(
                        [
                            'name' => 'AccessRestrictionRule_StartDate',
                            'form_variable' => ['twig' => '2001-01-01']
                        ]
                    ),
                ],
            ]
        );
        $requestData = [
            '_node_path' => 'foo',
            'Name' => 'foo',
            'ArchiveUnits' => [
                [
                    '_tree_path' => 'bar',
                    'Name' => 'bar',
                    'Files' => [
                        'UUID-2b90c8be-72e8-11ec-91ea-f11b47a3f26b' => [
                            'Filename' => 'foo/bar/sample.pdf'
                        ],
                    ],
                ],
            ],
        ];
        $this->invokeMethod(
            $form,
            'alterArchiveUnitByPath',
            [
                $formUnit,
                ['input' => ['test' => 'test_alter']],
                &$requestData,
                ['foo/bar/', 'foo/bar/sample.pdf'],
            ]
        );
        $this->assertEquals(
            'test_alter',
            Hash::get($requestData, 'ArchiveUnits.0.AccessRestrictionRule.Code')
        );
    }

    public function testalterArchiveUnitBySearch()
    {
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(1, 1, $deposit);
        $formUnit = new Entity(
            [
                'name' => 'testunit',
                'search_expression' => '/foo/*',
                'form_unit_keywords' => [
                    new Entity(
                        [
                            'form_unit_keyword_details' => [
                                new Entity(
                                    [
                                        'name' => 'KeywordContent',
                                        'form_variable' => ['twig' => 'mon mot-clé'],
                                    ]
                                ),
                            ],
                        ]
                    ),
                ],
            ]
        );
        $requestData = [
            '_node_path' => 'foo',
            'Name' => 'foo',
            'ArchiveUnits' => [
                [
                    '_tree_path' => 'bar',
                    'Name' => 'bar',
                    'Files' => [
                        'UUID-2b90c8be-72e8-11ec-91ea-f11b47a3f26b' => [
                            'Filename' => 'foo/bar/sample.pdf'
                        ],
                    ],
                ],
                [
                    '_tree_path' => 'biz',
                    'Name' => 'biz',
                    'Files' => [
                        'UUID-2b90c8be-72e8-11ec-91ea-f11b47a3f27b' => [
                            'Filename' => 'foo/biz/sample.pdf'
                        ],
                    ],
                ],
            ],
        ];
        $this->invokeMethod(
            $form,
            'alterArchiveUnitBySearch',
            [
                $formUnit,
                [],
                &$requestData,
                ['foo/bar/', 'foo/bar/sample.zip', 'foo/biz/', 'foo/biz/sample.zip'],
            ]
        );
        $this->assertEquals(
            'mon mot-clé',
            Hash::get($requestData, 'ArchiveUnits.0.ContentDescription.Keyword.0.KeywordContent')
        );
        $this->assertEquals(
            'mon mot-clé',
            Hash::get($requestData, 'ArchiveUnits.1.ContentDescription.Keyword.0.KeywordContent')
        );
    }

    public function testexportFiles()
    {
        $xml = VolumeSample::copySample('sample_seda10.xml');
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(1, 1, $deposit);
        $form->xml = file_get_contents($xml);

        $e = null;
        try {
            $form->exportFiles();
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(GenericException::class, $e);

        $attachments = [
            'sample.rng' => VolumeSample::copySample('sample.rng'),
            'sample.pdf' => VolumeSample::copySample('sample.pdf'),
            'sample.odt' => VolumeSample::copySample('sample.odt'),
        ];
        $this->invokeProperty($form, 'attachments', 'set', $attachments);
        $form->exportFiles();
        $this->assertFileExists(
            TMP_TESTDIR . DS . 'exported/tmp/original_data/sample.odt'
        );

        $xml = VolumeSample::copySample('sample_seda21_valid.xml');
        $form = FormMakerForm::create(2, 1, $deposit);
        $form->xml = file_get_contents($xml);

        $e = null;
        try {
            $form->exportFiles();
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(GenericException::class, $e);

        $attachments = [
            'sample.pdf' => VolumeSample::copySample('sample.pdf'),
        ];
        $this->invokeProperty($form, 'attachments', 'set', $attachments);
        $form->exportFiles();
        $this->assertFileExists(
            TMP_TESTDIR . DS . 'exported/tmp/original_data/sample.pdf'
        );
    }

    /**
     * Créé un fichier $name
     * @param string $name
     * @return EntityInterface
     * @throws Exception
     */
    private function createFile(string $name): EntityInterface
    {
        Filesystem::dumpFile(TMP_TESTDIR . DS . $name, 'test');
        /** @var FileuploadsTable $Fileuploads */
        $Fileuploads = $this->fetchTable('Fileuploads');
        $file = $Fileuploads->newUploadedEntity(
            $name,
            TMP_TESTDIR . DS . $name,
            1
        );
        $Fileuploads->saveOrFail($file);
        return $file;
    }

    public function testRepeatableFieldsets()
    {
        // On créer un deposit avec sections multiples
        $data = [
            'form_id' => 7,
            'name' => 'deposit_test',
            'transferring_agency_id' => 2,
            'archival_agency_id' => 2,
            'state' => DepositsTable::S_EDITING,
            'created_user_id' => 1,
            'last_state_update' => new FrozenTime(),
            'validated' => true,
        ];
        /** @var DepositsTable $Deposits */
        $Deposits = $this->fetchTable('Deposits');
        $entity = $Deposits->newEntity($data);
        $Deposits->saveOrFail($entity);

        $file1 = $this->createFile('test1.txt');
        $file2 = $this->createFile('test2.txt');
        $file3 = $this->createFile('test3.txt');
        $file4 = $this->createFile('test4.txt');
        $file5 = $this->createFile('test5.txt');
        $file6 = $this->createFile('test6.txt');
        $data = [
            'archival_agency_id' => 2,
            'state' => DepositsTable::S_EDITING,
            'created_user_id' => 1,
            'inputs' => [
                'Titre' => 'test',
                'section-1' => [
                    0 => [
                        'Commentaire' => 'test 1',
                        'fichier' => $file1->id,
                        'fichier_multi' => [
                            0 => $file3->id,
                            1 => $file4->id,
                        ],
                    ],
                    1 => [
                        'Commentaire' => 'test 2',
                        'fichier' => $file2->id,
                        'fichier_multi' => [
                            0 => $file5->id,
                            1 => $file6->id,
                        ],
                    ],
                ],
            ]
        ];
        $Deposits->updateDeposit(
            $entity,
            $data
        );

        // on mock sedaGenerator, on récupère le request data qu'on lui envoi
        $requestData = [];
        $client = $this->getMockBuilder(Client::class)
            ->onlyMethods(['post'])
            ->getMock();
        $client->method('post')->will(
            $this->returnCallback(
                function ($url, $data) use (&$requestData) {
                    $requestData = Hash::flatten(json_decode($data, true));
                    throw new Exception();
                }
            )
        );
        ClientMock::$mock = $client;
        Utility::set(Client::class, new ClientMock());
        $form = FormMakerForm::create(7, 1, $entity);
        try {
            $form->execute($data['inputs']);
        } catch (Exception $e) {
        }

        $this->assertArrayHasKey(
            'ArchiveUnits.0.ArchiveUnits.0.Content.Title',
            $requestData
        );
        $this->assertArrayHasKey(
            'ArchiveUnits.0.ArchiveUnits.1.Content.Title',
            $requestData
        );
        $this->assertArrayHasKey(
            'ArchiveUnits.0.ArchiveUnits.0.Content.Description',
            $requestData
        );
        $this->assertEquals(
            'test 1',
            $requestData['ArchiveUnits.0.ArchiveUnits.0.Content.Description']
        );
        $this->assertArrayHasKey(
            'ArchiveUnits.0.ArchiveUnits.1.Content.Description',
            $requestData
        );
        $this->assertEquals(
            'test 2',
            $requestData['ArchiveUnits.0.ArchiveUnits.1.Content.Description']
        );
        $expandedRequest = Hash::expand($requestData);
        $this->assertCount(
            3,
            Hash::get($expandedRequest, 'ArchiveUnits.0.ArchiveUnits.0.Files', [])
        );
        $this->assertCount(
            3,
            Hash::get($expandedRequest, 'ArchiveUnits.0.ArchiveUnits.1.Files', [])
        );
    }

    public function testShortUUID()
    {
        $exempleShortUUID = 'q.MLZhQPS0QAixHuQvIZ-g';
        $shortUUID = FormMakerForm::shortUUID();
        $this->assertEquals(strlen($exempleShortUUID), strlen($shortUUID));

        $decodedShortUUID = FormMakerForm::decodeShortUUID($exempleShortUUID);
        $this->assertEquals('42f219fe-008b-11ee-abe3-0b66140f4b44', $decodedShortUUID);

        $uuid = uuid_create(UUID_TYPE_TIME);
        $shortUUID = FormMakerForm::shortUUID($uuid);
        $decodedShortUUID = FormMakerForm::decodeShortUUID($shortUUID);
        $this->assertEquals($uuid, $decodedShortUUID);
    }

    public function testValidationRepeatable()
    {
        $deposit = new Entity(['path' => TMP_TESTDIR . '/exported']);
        $form = FormMakerForm::create(7, 1, $deposit);
        $form->validate(['Titre' => 'test', 'section-1' => [0 => ['number' => '12']]]);
        $this->assertTrue($form->validate(['Titre' => 'test', 'section-1' => [0 => ['number' => '12']]]));
        $this->assertFalse($form->validate(['Titre' => 'test', 'section-1' => [0 => ['number' => 'foo']]]));
    }
}
