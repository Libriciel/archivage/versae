<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Command;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\ConsoleIntegrationTestTrait;
use AsalaeCore\TestSuite\TestCase;

/**
 * Versae\Command\SessionCommand Test Case
 */
class SessionCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public $fixtures = [
        'app.OrgEntities',
        'app.Sessions',
        'app.Users',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();
    }

    public function testOrgentities()
    {
        $Sessions = TableRegistry::getTableLocator()->get('Sessions');
        $original = stream_get_contents(
            $Sessions->find()
                ->where(['id' => 'f45df7be-ca83-44ef-960c-1856a8893eb6'])
                ->first()
                ->get('data')
        );
        $this->exec('session orgentities');

        $new = stream_get_contents(
            $Sessions->find()
                ->where(['id' => 'f45df7be-ca83-44ef-960c-1856a8893eb6'])
                ->first()
                ->get('data')
        );
        $this->assertNotEquals($original, $new);
    }

    public function testList()
    {
        $this->exec('session list');
        $this->assertOutputContains('f45df7be-ca83-44ef-960c-1856a8893eb6');
    }
}
