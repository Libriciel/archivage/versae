<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Command;

use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\CascadeDelete;
use AsalaeCore\Utility\Config;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Object\CommandResult;
use Cake\Core\Configure;
use Cake\Database\Connection;
use Cake\Database\Exception\MissingConnectionException;
use Cake\Datasource\ConnectionManager as CakeConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\ConsoleIntegrationTestTrait;
use Cake\Utility\Security;
use Libriciel\Filesystem\Utility\Filesystem;
use Versae\Test\Mock\ConnectionManager;

class InstallConfigCommandTest extends TestCase
{
    public $fixtures = [
        'app.ArchivingSystems',
        'app.Aros',
        'app.Counters',
        'app.OrgEntities',
        'app.Roles',
        'app.RolesTypeEntities',
        'app.Sequences',
        'app.TypeEntities',
        'app.Users',
    ];

    use ConsoleIntegrationTestTrait;

    public function setUp(): void
    {
        parent::setUp();
        $this->originalPathToLocal = Configure::read('App.paths.path_to_local_config');
        $dir = TMP_TESTDIR . DS . 'testinstallconfig';
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Configure::write('App.defaultLocale', 'fr_FR');
        Configure::write('App.paths.path_to_local_config', $dir . DS . 'path_to_local.php');
        Filesystem::reset();
        Filesystem::dumpFile($dir . DS . 'path_to_local.php', "<?php return '$dir/app_local.json';?>");
        Filesystem::dumpFile($dir . DS . 'app_local.json', "{}");
        Utility::set(CakeConnectionManager::class, new ConnectionManager());
        $conn = $this->createMock(Connection::class);
        $conn->method('query')->willThrowException(new MissingConnectionException());
        ConnectionManager::$outputs = [
            'drop' => true,
            'setConfig' => true,
            'get' => $conn,
        ];
        $exec = $this->getMockBuilder(Exec::class)
            ->onlyMethods(['command', 'rawCommand', 'async'])
            ->getMock();
        $exec->method('async');
        $exec->method('command')->willReturn(
            new CommandResult(['success' => true, 'code' => 0, 'stdout' => 'OK', 'stderr' => ''])
        );
        $exec->method('rawCommand')->will(
            $this->returnCallback(
                function ($command, array &$output = null, &$return_var = null) {
                    $output = [];
                    $return_var = 0;
                    return '';
                }
            )
        );
        Utility::set('Exec', $exec);
        $this->useCommandRunner();
    }

    public function tearDown(): void
    {
        Configure::write('App.paths.path_to_local_config', $this->originalPathToLocal);
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Utility::reset();
        Config::reset();
        Security::setSalt(Config::readAll('Security.salt'));
        parent::tearDown();
    }

    public function testExecute()
    {
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $Users = TableRegistry::getTableLocator()->get('Users');
        CascadeDelete::all($OrgEntities);
        $dir = TMP_TESTDIR . DS . 'testinstallconfig';
        $ini = file_get_contents(CONFIG . 'install_default.ini');
        $ini = preg_replace('/(config_file = ).*/', '$1' . $dir . DS . 'app_local.json', $ini);
        $ini = preg_replace('/(data_dir = ).*/', "$1$dir", $ini);
        $ini = preg_replace('/(admin_file = ).*/', '$1' . $dir . DS . 'administrators.json', $ini);
        $ini = preg_replace('/(debug = ).*/', '${1}1', $ini);
        $iniFile = $dir . DS . 'install.ini';
        file_put_contents($dir . DS . 'install.ini', $ini);

        $this->exec("install config --force $iniFile");
        $this->assertOutputContains(__("Installation terminée"));
        $this->assertCount(3, $OrgEntities->find());
        $userCount = $Users->find()->count();

        // install repetable
        Configure::write('App.paths.path_to_local_config', $dir . DS . 'path_to_local.php');
        $ini = preg_replace('/(user1\[username] = ).*/', "$1test", $ini);
        file_put_contents($dir . DS . 'install.ini', $ini);
        $this->exec("install config --force $iniFile");
        $this->assertOutputContains(__("Installation terminée"));
        $this->assertCount(3, $OrgEntities->find());
        // on a changé username, du coup ça a créé un nouvel utilisateur
        $this->assertCount($userCount + 1, $Users->find());
    }
}
