<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Command;

use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\Client;
use AsalaeCore\TestSuite\TestCase;
use Cake\Http\Client\Response;
use Cake\TestSuite\ConsoleIntegrationTestTrait;
use Versae\Test\Mock\ClientMock;

/**
 * Versae\Command\DockerListCommandTest Test Case
 */
class DockerListCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public $fixtures = [
        'app.BeanstalkWorkers',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();
        $response = $this->createMock(Response::class);
        $response->method('getStatusCode')->willReturn(204);
        $response->method('getStringBody')
            ->willReturn(
                json_encode(['jwt' => 'test_token']),
                json_encode([['Id' => 'fake', 'Names' => ['uuid']]])
            );
        $client = $this->getMockBuilder(Client::class)
            ->setConstructorArgs([])
            ->onlyMethods(['get', 'post'])
            ->getMock();
        $client->method('get')->willReturn($response);
        $client->method('post')->willReturn($response);
        ClientMock::$mock = $client;
        Utility::set(Client::class, new ClientMock());
    }

    public function tearDown(): void
    {
        Utility::reset();
        parent::tearDown();
    }

    public function testMain()
    {
        $this->exec('docker list');
        $this->assertOutputContains(
            sprintf("Worker %s (id=%d): %s", 'test', 1, 'uuid')
        );
    }
}
