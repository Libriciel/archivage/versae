<?php

namespace Versae\Test\TestCase\Command;

use Versae\Command\CronsProbeCommand;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\ConsoleIntegrationTestTrait;
use DateTime;

/**
 * Versae\Command\CronsProbeCommand Test Case
 */
class CronsProbeCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public $fixtures = [
        'app.Crons',
        'app.CronExecutions',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();
    }

    public function testMain()
    {
        $CronExecutions = TableRegistry::getTableLocator()->get('CronExecutions');
        $CronExecutions->deleteAll([]);

        $this->exec('cronsProbe');
        $this->assertEquals(0, $this->_exitCode);

        $execution = $CronExecutions->newEntity(
            [
                'cron_id' => 3,
                'date_begin' => new DateTime(),
                'date_end' => new DateTime(),
                'state' => 'error',
                'report' => "Version php <span class=\"console success\">OK</span>\n"
                    . "Extension X <span class=\"console error\">Fail</span>",
            ]
        );
        $CronExecutions->saveOrFail($execution);
        $execution = $CronExecutions->newEntity(
            [
                'cron_id' => 2,
                'date_begin' => new DateTime(),
                'date_end' => new DateTime(),
                'state' => 'warning',
                'report' => $warning = 'This is a warning',
            ]
        );
        $CronExecutions->saveOrFail($execution);

        $this->exec('cronsProbe');
        $this->assertEquals(CronsProbeCommand::CODE_PROBE_ERROR, $this->_exitCode);
        $this->assertErrorContains('Extension X');
        $this->assertOutputContains($warning);
    }
}
