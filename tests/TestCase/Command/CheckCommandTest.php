<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Command;

use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\ConsoleIntegrationTestTrait;
use AsalaeCore\TestSuite\TestCase;
use Versae\Test\Mock\CheckMock;

class CheckCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();
    }

    public function tearDown(): void
    {
        Utility::reset();
        parent::tearDown();
    }

    public function testExecute()
    {
        Utility::set('Check', CheckMock::class);
        CheckMock::$outputs = [
            'missings' => [],
            'missingsExtPhp' => [],
            'database' => true,
            'beanstalkd' => true,
            'ratchet' => true,
            'phpOk' => true,
        ];

        $this->exec('check');
        $this->assertExitCode(0);

        CheckMock::$outputs = [
            'missings' => ['testunit'],
            'missingsExtPhp' => ['php-testunit'],
            'database' => false,
            'beanstalkd' => false,
            'ratchet' => false,
            'phpOk' => false,
        ];
        $this->exec('check');
        $this->assertExitCode(1);
    }
}
