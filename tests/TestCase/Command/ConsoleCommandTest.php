<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Command;

use AsalaeCore\Factory\Utility;
use Cake\TestSuite\ConsoleIntegrationTestTrait;
use AsalaeCore\TestSuite\TestCase;
use Psy\Shell as PsyShell;

/**
 * Versae\Command\ConsoleCommand Test Case
 */
class ConsoleCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();
    }

    public function tearDown(): void
    {
        Utility::reset();
        parent::tearDown();
    }

    public function testMain()
    {
        $psy = $this->createMock(PsyShell::class);
        $psy->method('run');
        Utility::set(PsyShell::class, $psy);
        $this->exec('console');
        $this->assertOutputContains('You can exit with');
    }
}
