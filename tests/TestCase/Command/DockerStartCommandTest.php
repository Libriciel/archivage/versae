<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Command;

use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\Client;
use AsalaeCore\TestSuite\TestCase;
use Cake\Http\Client\Response;
use Cake\TestSuite\ConsoleIntegrationTestTrait;
use Versae\Test\Mock\ClientMock;

/**
 * Versae\Command\DockerStartCommandTest Test Case
 */
class DockerStartCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public $fixtures = [];

    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();
        $response = $this->createMock(Response::class);
        $response->method('getStatusCode')->willReturn(204);
        $response->method('getStringBody')
            ->willReturn(json_encode(['jwt' => 'test_token']));
        $client = $this->getMockBuilder(Client::class)
            ->setConstructorArgs([])
            ->onlyMethods(['get', 'post'])
            ->getMock();
        $client->method('get')->willReturn($response);
        $client->method('post')->willReturn($response);
        ClientMock::$mock = $client;
        Utility::set(Client::class, new ClientMock());
    }

    public function tearDown(): void
    {
        Utility::reset();
        parent::tearDown();
    }

    public function testMain()
    {
        $this->exec('docker start test');
        $this->assertOutputContains(
            __("{0} démarré avec succès", 'test')
        );
    }
}
