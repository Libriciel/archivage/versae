<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Command;

use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\GetShellTrait;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\CascadeDelete;
use AsalaeCore\Utility\Exec;
use Cake\Console\Exception\StopException;
use Cake\Core\Configure;
use Cake\Database\Exception;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\ConsoleIntegrationTestTrait;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\MockObject\MockObject;
use Versae\Command\InstallCommand;

/**
 * Versae\Command\InstallCommand Test Case
 */
class InstallCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;
    use InvokePrivateTrait;
    use GetShellTrait;

    public $fixtures = [
        'app.Acos',
        'app.Aros',
        'app.ArosAcos',
        'app.OrgEntities',
        'app.Phinxlog',
        'app.Sessions',
        'app.TypeEntities',
    ];

    private $deleteMigration = false;
    private $initialpathToLocale;

    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();
        if (!is_file(CONFIG . 'Migrations' . DS . 'schema-dump-default.lock')) {
            touch(CONFIG . 'Migrations' . DS . 'schema-dump-default.lock');
            $this->deleteMigration = true;
        }
        $this->initialpathToLocale = Configure::read('App.paths.path_to_local_config');
        TableRegistry::getTableLocator()->clear();
        $Exec = $this->createMock(Exec::class);
        $Exec->method('async')->willReturnSelf();
        Utility::set('Exec', $Exec);
        $dir = TMP_TESTDIR;
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
    }

    public function tearDown(): void
    {
        if ($this->deleteMigration) {
            unlink(CONFIG . 'Migrations' . DS . 'schema-dump-default.lock');
        }
        Configure::write('App.paths.path_to_local_config', $this->initialpathToLocale);
        Utility::reset();
        $dir = TMP_TESTDIR;
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
        parent::tearDown();
    }

    public function testMain()
    {
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        CascadeDelete::all($OrgEntities);
        $Permissions = TableRegistry::getTableLocator()->get('AsalaeCore.ArosAcos');
        $Permissions->deleteAll([]);
        $Acos = TableRegistry::getTableLocator()->get('AsalaeCore.Acos');
        $Acos->deleteAll([]);

        set_time_limit(60); // boucle infinie possible
        $this->exec(
            'install',
            [
                'se',
                'identifier',
                'myname',
            ]
        );
        set_time_limit(0);
        $this->assertEquals(1, $OrgEntities->find()->count());
        $this->assertGreaterThan(0, $Acos->find()->count());
        $this->assertGreaterThan(0, $Permissions->find()->count());
    }

    public function testCheckConfig()
    {
        /** @var InstallCommand $installCommand */
        $installCommand = $this->getShell(InstallCommand::class);
        $this->invokeMethod($installCommand, 'checkConfig');
        $this->assertOutputContains(__d('install', "Fichier de configuration trouvé."));

        $dir = TMP_TESTDIR;
        if (!is_dir($dir)) {
            mkdir($dir);
        }
        $pathToLocal = tempnam($dir, 'testunit-');
        file_put_contents($pathToLocal, '<?php return "fakepath"; ?>');
        Configure::write('App.paths.path_to_local_config', $pathToLocal);
        try {
            $this->invokeMethod($installCommand, 'checkConfig');
            throw new Exception;
        } catch (\Exception $e) {
            $this->assertInstanceOf(StopException::class, $e);
            $this->assertEquals(
                __d('install', "Erreur, impossible de trouver le fichier de configuration !"),
                $e->getMessage()
            );
        }

        Configure::write('App.paths.path_to_local_config', 'fakepath');
        try {
            $this->invokeMethod($installCommand, 'checkConfig');
            throw new Exception;
        } catch (\Exception $e) {
            $this->assertInstanceOf(StopException::class, $e);
            $this->assertEquals(
                __d(
                    'install',
                    "Il semble que l'application n'a pas été configurée. "
                    . "Merci de lancer le shell `Configuration` avant de lancer l'installation."
                ),
                $e->getMessage()
            );
        }
    }

    public function testCheckDatabase()
    {
        /** @var InstallCommand $installCommand */
        $installCommand = $this->getShell(InstallCommand::class);
        $this->invokeMethod($installCommand, 'checkDatabase');
        $this->assertOutputContains(__d('install', "Migration ok"));

        /** @var Table|MockObject $phinxlog */
        $phinxlog = $this->createMock(Table::class);
        $phinxlog->method('find')->willThrowException(new Exception());
        $i = 0;
        $table = TableRegistry::getTableLocator()->get('Phinxlog');
        $find = $table->find();

        $phinxlog->method('find')->will(
            $this->returnCallback(
                function () use (&$i, $find) {
                    $i++;
                    if ($i === 1) {
                        new Exception();
                    }
                    return $find;
                }
            )
        );

        TableRegistry::getTableLocator()->set('Phinxlog', $phinxlog);
        $installCommand = $this->getShell(InstallCommand::class, ['OK']);
        $this->invokeMethod($installCommand, 'checkDatabase');
        $this->assertOutputContains(__d('install', "Vous devez effectuer la migration pour continuer."));
    }
}
