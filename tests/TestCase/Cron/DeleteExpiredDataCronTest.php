<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Cron;

use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\SaveBufferTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use DateTime;
use Libriciel\Filesystem\Utility\Filesystem;
use Versae\Cron\DeleteExpiredData;

class DeleteExpiredDataCronTest extends TestCase
{
    use IntegrationCronTrait;
    use SaveBufferTrait;

    public $fixtures = [
        'app.Crons',
        'app.CronExecutions',
        'app.AuthUrls'
    ];

    public function setUp(): void
    {
        parent::setUp();
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    public function testMain()
    {
        $CronExecutions = TableRegistry::getTableLocator()->get('CronExecutions');
        $Crons = TableRegistry::getTableLocator()->get('Crons');
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');

        // test aucune url à supprimer
        $CronExecutions->deleteAll([]);
        $Crons->deleteAll([]);
        $AuthUrls->deleteAll([]);
        $this->runCron(DeleteExpiredData::class);
        $this->assertOutputContains(__("Aucune url d'authentification à supprimer"));

        // test une url à supprimer
        $CronExecutions->deleteAll([]);
        $Crons->deleteAll([]);
        $AuthUrls->deleteAll([]);
        $AuthUrls->save(
            $AuthUrls->newEntity(
                [
                    'code' => '4aff47e6b0edf013b9c15cb58d888fef9',
                    'url' => '/users/initialize-password/1',
                    'expire' => new DateTime(('-1 day')),
                    'created' => new DateTime()
                ]
            )
        );
        $this->runCron(DeleteExpiredData::class);
        $this->assertOutputContains(__("Suppression de l'url d'authentification /users/initialize-password/1"));

        Configure::write('App.paths.data', TMP_TESTDIR);
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        $folder = TMP_TESTDIR . DS . 'upload' . DS . 'foo';
        Filesystem::mkdir($folder);
        exec('touch -d "2 days ago" '. $folder);
        $this->runCron(DeleteExpiredData::class);
        $this->assertOutputContains(__("Suppression du dossier vide {0}", $folder));
    }
}
