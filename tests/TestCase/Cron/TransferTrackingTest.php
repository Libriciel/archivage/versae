<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Cron;

use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\Client;
use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\TestSuite\VolumeSample;
use Cake\Http\Client\Response;
use Cake\ORM\TableRegistry;
use Psr\Http\Message\StreamInterface;
use Versae\Cron\TransferTracking;
use Versae\Model\Table\DepositsTable;
use Versae\Test\Mock\ClientMock;

/**
 * Versae\Cron\TransferTracking Test Case
 */
class TransferTrackingTest extends TestCase
{
    use IntegrationCronTrait;

    public $fixtures = [
        'app.ArchivingSystems',
        'app.Deposits',
        'app.Forms',
        'app.OrgEntities',
        'app.Transfers',
    ];

    public function setUp(): void
    {
        parent::setUp();
        VolumeSample::init();

        $body = $this->createMock(StreamInterface::class);
        $body->method('getContents')->willReturn('test');
        $response = $this->createMock(Response::class);
        $response->method('getBody')->willReturn($body);
        $response->method('getStringBody')
            ->willReturn(VolumeSample::getSampleContent('sample_transfer_reply.xml'));
        $client = $this->getMockBuilder(Client::class)
            ->setConstructorArgs([])
            ->onlyMethods(['get', 'post'])
            ->getMock();
        $client->method('get')->willReturn($response);
        $client->method('post')->willReturn($response);
        ClientMock::$mock = $client;
        Utility::set(Client::class, new ClientMock());
    }

    public function tearDown(): void
    {
        Utility::reset();
        VolumeSample::destroy();
        parent::tearDown();
    }

    public function testWork()
    {
        $Deposits = TableRegistry::getTableLocator()->get('Deposits');
        $Deposits->updateAll(['state' => DepositsTable::S_SENT], []);
        $this->runCron(TransferTracking::class);
        $count = $Deposits->find()
            ->where(['state' => DepositsTable::S_ACCEPTED])
            ->count();
        $this->assertGreaterThanOrEqual(2, $count, implode(PHP_EOL, $this->_out->messages()));
    }
}
