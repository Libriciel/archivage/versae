<?php

/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Cron;

use Adldap\Adldap;
use Adldap\Auth\Guard;
use Adldap\Connections\Provider;
use Adldap\Models\Model;
use Adldap\Query\Builder;
use Adldap\Query\Factory;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AutoFixturesTrait;
use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Versae\Cron\UpdateLDAPUsers;

/**
 * Versae\Cron\UpdateLDAPUsersTest Test Case
 */
class UpdateLDAPUsersTest extends TestCase
{
    use IntegrationCronTrait;
    use AutoFixturesTrait;

    public $appAutoFixtures = [
        'Users',
    ];
    public $appAppendFixtures = [
        'app.Aros',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $adldap = $this->createMock(Adldap::class);
        $adldap->method('addProvider');
        $provider = $this->createMock(Provider::class);
        $adldap->method('connect')->willReturn($provider);
        $guard = $this->createMock(Guard::class);
        $provider->method('auth')->willReturn($guard);
        $guard->method('attempt')->willReturn(true);
        $factory = $this->createMock(Factory::class);
        $provider->method('search')->willReturn($factory);
        $users = $this->createMock(Builder::class);
        $factory->method('users')->willReturn($users);
        $ldapUser = $this->createMock(Model::class);
        $users->method('findBy')->willReturn($ldapUser);
        $ldapUser->method('getAttribute')->willReturn(['foo'], ['bar@baz.fr']);
        Utility::set(Adldap::class, $adldap);
    }

    public function tearDown(): void
    {
        Utility::reset();
        parent::tearDown();
    }

    public function testWork()
    {
        $Users = TableRegistry::getTableLocator()->get('Users');
        $Users->updateAll(['ldap_id' => 1], ['id' => 1]);
        $this->runCron(UpdateLDAPUsers::class);
        $this->assertEquals('bar@baz.fr', $Users->get(1)->get('email'));
    }
}
