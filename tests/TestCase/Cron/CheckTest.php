<?php
/** @noinspection PhpCSValidationInspection */

namespace Versae\Test\TestCase\Cron;

use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Object\CommandResult;
use Cake\Console\Command;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use DateTime;
use Libriciel\Filesystem\Utility\Filesystem;
use Versae\Controller\AdminsController;
use Versae\Cron\Check;
use Versae\Test\Mock\FakeEmitter;
use Versae\Test\Mock\GenericCaller;

/**
 * Versae\Cron\Check Test Case
 */
class CheckTest extends TestCase
{
    use IntegrationCronTrait;
    use InvokePrivateTrait;

    public $fixtures = [
        'app.CronExecutions',
        'app.Crons',
        'app.OrgEntities',
        'app.Phinxlog',
    ];

    public function setUp(): void
    {
        parent::setUp();
        if (is_dir($dir = TMP_TESTDIR . DS . 'check-test')) {
            Filesystem::remove($dir);
        }
        Filesystem::mkdir($dir);

        file_put_contents(
            $localpath = $dir . DS . 'path_to_local.php',
            sprintf('<?php return "%s";?>', addcslashes($localjson = $dir . DS . 'local.json', '"'))
        );
        file_put_contents(
            $localjson,
            json_encode(
                [
                    'Datasources' => [
                        'default' => ConnectionManager::get('default')->config()
                    ]
                ]
            )
        );
        Configure::write('App.paths.path_to_local_config', $localpath);

        $Phinxlog = TableRegistry::getTableLocator()->get('Phinxlog');
        $Phinxlog->deleteAll([]);
        $Phinxlogs = TableRegistry::getTableLocator()->get('Phinxlog');
        $migrations = glob(CONFIG . 'Migrations/*.php');
        foreach ($migrations as $path) {
            $filename = basename($path, '.php');
            [$version, $migration_name] = explode('_', $filename, 2);
            $phinx = $Phinxlogs->newEntity(
                [
                    'version' => $version,
                    'migration_name' => $migration_name,
                    'start_time' => $time = new DateTime(),
                    'end_time' => $time,
                    'breakpoint' => false
                ]
            );
            $Phinxlogs->saveOrFail($phinx);
        }
    }

    public function tearDown(): void
    {
        if (is_dir($dir = TMP_TESTDIR . DS . 'check-test')) {
            Filesystem::remove($dir);
        }
        Utility::reset();
        Filesystem::setNamespace();
        Filesystem::reset();
        Exec::waitUntilAsyncFinish();
        parent::tearDown();
    }

    public function testWork()
    {
        $check = $this->createMock(FakeEmitter::class);
        $check->method('__call')->will(
            $this->returnCallback(
                function ($name) {
                    switch ($name) {
                        case 'extPhp':
                        case 'missings':
                        case 'php':
                            return ['test' => true];
                        default:
                            return true;
                    }
                }
            )
        );

        Utility::set('Check', $check);
        $exec = $this->createMock(Exec::class);
        $exec->method('command')->willReturn(
            new CommandResult(['success' => true, 'code' => Command::CODE_SUCCESS, 'stdout' => '', 'stderr' => ''])
        );
        Utility::set('Exec', $exec);
        GenericCaller::$methods['ping'] = true;

        $Crons = TableRegistry::getTableLocator()->get('Crons');
        $cron = $Crons->newEntity(
            [
                'name' => 'Vérification du fonctionnement de versae',
                'description' => 'test',
                'classname' => Check::class,
                'active' => true,
                'locked' => false,
                'frequency' => 'PT1S',
                'next' => new DateTime('yesterday'),
                'app_meta' => null,
                'parallelisable' => true
            ]
        );
        $Crons->saveOrFail($cron);

        $this->runCron(Check::class);
        $this->assertOutputContains('success');
    }

    public function testGetDriverArgs()
    {
        Configure::write('Some.random.config', 'foo');
        $fields = [
            'one' => [
                'read_config' => 'Some.random.config',
            ],
            'two' => [
                'default' => 'will_not_be_used'
            ],
            'three' => [
                'default' => 'baz'
            ],
            'four' => [],
        ];
        $data = (object)[
            'two' => 'bar'
        ];
        $expected = [
            'one' => 'foo',
            'two' => 'bar',
            'three' => 'baz',
            'four' => '',
        ];
        $check = new Check();
        $args = $this->invokeMethod($check, 'getDriverArgs', [$fields, $data]);
        $this->assertEquals($expected, $args);

        $decryptKey = hash('sha256', AdminsController::DECRYPT_KEY);
        $fields = ['five' => ['type' => 'password']];
        $data = (object)['five' => base64_encode(Security::encrypt('test', $decryptKey))];
        $args = $this->invokeMethod($check, 'getDriverArgs', [$fields, $data]);
        $this->assertEquals('test', $args['five']);
    }
}
